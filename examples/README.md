# **hawen: time-HArmonic Wave modEling and INversion using Hybridizable Discontinuous Galerkin Discretization.**

___

> ### Dedicated website with more infos and tutorials: [https://ffaucher.gitlab.io/hawen-website/](https://ffaucher.gitlab.io/hawen-website/).
>
> 
> ### This code is used to solve time-harmonic wave problem using Hybridizable Discontinuous Galerkin (HDG) discretization. It can be used for the forward problem (modeling) and the inverse one (quantitative parameter identification).
> ### For comments or additional information, contact <florian.faucher@univie.ac.at>.

___

## Examples folder
>
> This folder contains an acoustic and elastic modeling benchmarks to illustrate the code. It assumes that the executables for forward problem in acoustic and elastic are compiled, respectively `helmholtz_acoustic-iso` and `helmholtz_elastic-iso`. As indicated in the README of the root directory, do not forget to use `make clean` between the compilations.
> Start by untaring the benchmark:
>
>		tar -Jxf benchmark.tar.xz
>
> Navigate to the folder
>
>		cd benchmark
>
> There are two parameter files in the folder: 
> - `par.modeling_acoustic` for the acoustic case.
> - `par.modeling_elastic` for the elastic case.
>
> You can open the files to see the parameters (e.g., the values of the wave speeds, the frequency solved, etc). For more information on the parameter files, we refer to the [User's documentation](https://ffaucher.gitlab.io/hawen-website), which is also in `doc` folder.
>

## Launching the code
>
> You can launch the code with or without parallelism, we refer to ${HAWEN_BIN_PATH} for the path where the executables have been compiled (which should be `../../bin`)
> The acoustic case launches with
>
>		mpirun -np 4 -x OMP_NUM_THREADS=2 ${HAWEN_BIN_PATH}/forward_helmholtz_acoustic-iso_hdg.out parameter=par.modeling_acoustic
>
> using 4 mpi and 2 threads per mpi. The user can select the combination depending on its architecture.
>
> The elastic test-case runs with
>
>		mpirun -np 4 -x OMP_NUM_THREADS=1 ${HAWEN_BIN_PATH}/forward_helmholtz_elastic-iso_hdg.out parameter=par.modeling_elastic
>
> using 4 mpi and 1 thread per mpi. 
>
> Upon running, several information are printed on screen, which follows the computational operations.
>

## Visualization of the results
>
> After the computations are done, the `results' folder contains the time-harmonic wave solutions. One can visualize these solutions using [ParaView](https://www.paraview.org/). For instance, for the elastic test-case, one can see the solution at 0.4Hz with
>
>		paraview results_elastic/wavefield/wavefield_frequency_0.00000E+00_4.00000E-01Hz_src000001.pvtu
>
> It is recommended to adjust the scale (colorbar) to better see the wave. Note that all solutions (velocities and stress components, real and imaginary parts) are encompassed in one file.

___

## Inversion benchmark
>
> A benchmark to run the inverse problem can also be downloaded from the [hawen website](https://ffaucher.gitlab.io/hawen-website), in the section **Tutorial**. 

___

