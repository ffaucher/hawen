# **hawen: time-HArmonic Wave modEling and INversion using Hybridizable Discontinuous Galerkin Discretization.**

___

## version 1.4.0

___

> ### Dedicated website with more infos and tutorials: [https://ffaucher.gitlab.io/hawen-website/](https://ffaucher.gitlab.io/hawen-website/).
>
> ### Journal of Open-Source Software paper: [![DOI](https://joss.theoj.org/papers/10.21105/joss.02699/status.svg)](https://doi.org/10.21105/joss.02699)
>
> ### The software solves time-harmonic wave problems for acoustic and elastic media using the Hybridizable Discontinuous Galerkin (HDG) method for discretization. It combines mpi and OpenMP parallelism to solve large-scale applications such as Earth's imaging and helioseismology.
> ### It can handle the forward problem (propagation of waves) as well as the inverse problem (parameter identification).
> ### For comments or additional information, contact <florian.faucher@inria.fr>.



___

## License
> ### The code is distributed with no warranty, under the GNU General Public License v3.0, see the LICENSE file.

## Folder structure
> Once the repository is cloned/downloaded, the following items must be in the folder:
> - **CHANGELOG** 
> - **cmake**         &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; for CMake installation 
> - **CMakeLists.txt**&emsp;&emsp;&nbsp; for CMake installation 
> - **code**          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&nbsp; contains the code source
> - **doc**           &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&ensp;&nbsp; contains the documentation
> - **examples**      &emsp;&emsp;&emsp;&emsp;&ensp; contains toy-problems
> - **LICENSE**
> - **README.md**
> - **tests**         &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&nbsp; contains test-cases to validate CMake installation

## Installation
### Mandatory Dependencies
> The following dependencies are necessary before the compilation of hawen:
>   - **make**, **MPI** libraries and **Fortran90** compiler with mpi.
>   - **BLAS**, **LAPACK** and **scaLAPACK** libraries (to link with MUMPS). It is also possible to use **Openblas** instead of the two first.
>
>   - [**MUMPS**](http://mumps.enseeiht.fr/) to solve the linear system.
>   - [**Metis**](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) is used for mesh partition.

> ___

>> **NOTE FOR LINUX USERS**: All of the packages can be retrieved from the official repositories, on `Ubuntu 20.04` these are obtained with
>>
>> 	sudo apt-get install  build-essential
>> 	sudo apt-get install  libopenmpi-dev
>> 	sudo apt-get install  libblas-dev libscalapack-openmpi-dev liblapack-dev
>> 	sudo apt-get install  libmetis-dev
>> 	sudo apt-get install  libmumps-dev
>>
>> **REMARK**: as of July 2020, latest versions of MUMPS (higher than 5.3.3) include more options than the available one in the default Linux repository. We recommend to install the latest version to benefit from the latest features.

> ___

### Optional Dependencies
> We refer to the [User's documentation](https://ffaucher.gitlab.io/hawen-website) for the information on the optional dependencies, which include
>  - [**arb**](http://arblib.org/) toolbox for the efficient computation of special functions.
>  - [**ARPACK/pARPACK**](https://www.caam.rice.edu/software/ARPACK/)  for the computation of eigenvalues and eigenvectors.


### Installation option 1: using make

#### Edit file **config/make.version_config**
> Once the dependencies are installed, you can move the the `code` repository: 
>
>		cd code
>
> Before to run `make`, one needs to create the file **make.version_config** in the config/ folder. Templates are given in the folder **config/config-files_make/**, for different architectures. This file contains the path towards the dependencies, that is, where to find the LAPACK, scaLAPACK, MUMPS and metis libraries. Note that any dependencies that has been added to MUMPS must be added in hawen (e.g., scotch partitioner, etc.).
> The variable `ARCH` is used to indicate the architecture: it must be one of the following: `GNU`,`INTEL` or `PGI`, respectively for GNU, Intel or PGI based compilation. All have been successfully tested on supercomputers.
>
>> - For instance, the template file **config/config-files_make/make.version_config__GNU.default** can be used directly if one has installed the dependencies from the linux repository as given above.
>>
>>			cp config/config-files_make/make.version_config__GNU.default config/make.version_config
>>
> The main keywords are described below for the `config/config-files_make/make.version_config` file.
> Other keywords are for the non-mandatory libraries and can remain blank if these are not linked.
>		
>		# F90 indicates the compiler, e.g., mpif90 or mpiifort.
>		F90  := mpif90
>		# ARCH indicates the architecture, it can either be
>		# GNU, INTEL or PGI
>		ARCH := GNU
>			
>		# metis is a mandatory dependency, both the library
>		# and include folder must be linked.
>		METISDIR  = /usr/lib
>		LMETIS    = -L$(METISDIR) -lmetis
>		IMETIS    = -I/usr/include/
>		
>		# MUMPS is a mandatory dependency, both the library
>		# -lcmumps -lzmumps -lmumps_common and -lpord must
>		# be linked, as well as the include folder
>		MUMPSDIR= /usr/lib
>		LMUMPS  = -L$(MUMPSDIR) -lcmumps -lzmumps
>		LMUMPS += -L$(MUMPSDIR) -lmumps_common -lpord
>		IMUMPS  = -I/usr/include/
>		
>		# additional dependencies for Mumps for linear algebra
>		# are required with LAPACK and SCALAPACK
>		LMUMPS += -L/usr/lib -lblas -lscalapack-openmpi -llapack
>


#### (Optional) Edit file **config/make.version**
>
> The file  **config/make.version** contains information on the compilation, such as the choice of optimization or debug flags, as well as the choice of method to be compiled. The choice of problem is made prior to the compilation, such that one executable is linked with one problem (forward or inverse, acoustic or elastic medium, etc). By default, the forward problem associated with acoustic media is compiled.
>
> 		vi config/make.version
> 
> You can choose the propagator that is compiled and the problem, by default, we have the acoustic isotropic forward problem:
>		# PROBLEM indicates the type of problem: this can be `forward` or `inversion`.
>		PROBLEM        := forward
>
>		# The different choices are propagator are currently:
>		#   - `helmholtz_acoustic-iso`              for the acoustic isotropic case.
>		#   - `helmholtz_elastic-iso_Su`            for the elastic isotropic case.
>		#   - `helio_scalar-conjugated_spherical1d` for the 1D helioseismology ODE under spherical symmetry.
>		#   - `helio_scalar-conjugated`             for the helioseismology scalar wave problem. 
>		PROPAGATOR     := helmholtz_acoustic-iso
>
>		# MUMPS_LATEST can be set to 1 if you have compiled MUMPS version 5.3.3 or higher.
>		MUMPS_LATEST=0
>		# DEPENDENCY_ARB can be set to 1 if you have linked arb library.
>		DEPENDENCY_ARB=0
>		# DEPENDENCY_ARPACK can be set to 1 if you have linked ARPACK library.
>		DEPENDENCY_ARPACK=0
>		# DEPENDENCY_PARPACK can be set to 1 if you have linked PARPACK library.
>		DEPENDENCY_PARPACK=0
>
> We refer to the [User's documentation](https://ffaucher.gitlab.io/hawen-website) for more information on the corresponding equations and options.
> Note that the dependencies are **not** activated by default, and one must change the value to **1** is  it is linked with the compilation, it also requires that the proper libraries are linked via the `config/make.version_config` file.
>

#### (Optional) Edit file **src/m_define_precision.f90**
> The file  **src/m_define_precision.f90** contains the choice of arithmetric precision, which has to be selected prior to the compilation (for instance, simple or double precision matrix operations). It is detailed in the [User's documentation](https://ffaucher.gitlab.io/hawen-website)

#### Compilation

> ___
>> **WARNING**: in order to compile several methods (`helmholtz_acoustic-iso` or `helmholtz_elastic-iso_Su`, `forward` or `inversion`), you always have to run
>>
>>		make clean
>> 
>> **in between sucessive compilation.**

> ___

> Once you have
> 1. Create the file `config/make.version_config` to link the mandatory libraries,
> 2. Possibly adjusted the compilation in the file `config/make.version`
> 3. Possibly adjusted the arithmetic precision in the file `src/m_define_precision.f90`
>
> you can compile with (still in the `code` repository):
>
>		make
>
> Upon successful operation, the generated executable is created in the folder `bin` at root directory:
>
>		ls ../bin
>
> To compile the different methods you have to:
> 1. compile using `make`
> 2. run `make clean`
> 3. choose the method in the `config/make.version` file (e.g. elastic, acoustic, forward, inversion)
> 4. go to step 1 and repeat steps 1--4 for all methods you need.

### Installation option 2: using CMake

#### Specify your compilation options
> With dependencies installed, compiling with CMake is done in two step. 
> First to retrieve the links to the dependencies with
>
>		cmake -B build
>
> where `build` is the name of the folder where CMake installation files 
> will be generated. By default, the propagator compiled is `helmholtz acoustic-iso` 
> and the problem is forward. These can be changed using `-D` options of CMake, 
> e.g., run
>
>		cmake -B build -DPROPAGATOR=helmholtz elastic-iso_Su -DPROBLEM=inversion
>
> One can also specify the path to the dependencies in the case they have been manually
> installed. For instance using a file preload.cmake to indicate the path (see below) and then by
>
>		cmake -B build -C preload.cmake
>
> A template of a `preload` file can be found in `cmake/preload.cmake.Template`

#### CMake compilation
> Once the command `CMake -B build` has been successfully run with your options), 
> the compilation can be done in parallel, e.g., using 5 cores:
>
>		make -C build -j5
>
> The executable files are generated in folder `build/bin/`

#### CMake validation tests
> To verify that the CMake compilation is successful and the code properly
> installed, one can use the `ctest` command in the build directory: 
>
>		cd build/
>		ctest
>
> It then should indicate success in the run.
> Note that validation test are possible for forward and inversion.

## Utilization
> Once the executable(s) have been successfully compiled, in addition to 
> the test validation provided with CMake installation, you can also try 
> some toy problems in the folder `examples`. Assuming you are in the `code` folder:
>
>		cd ../examples
>
> Note that the code works with an **input parameter files** that provides all information. These are detailed in the dedicated section of the [User's documentation](https://ffaucher.gitlab.io/hawen-website).
>
> A benchmark to run the inverse problem can also be downloaded from the [hawen website](https://ffaucher.gitlab.io/hawen-website), in the section **Tutorial**.
