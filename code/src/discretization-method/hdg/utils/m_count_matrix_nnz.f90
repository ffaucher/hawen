!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_count_matrix_nnz.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to estimate the number of non-zero in the matrix
!
!------------------------------------------------------------------------------
module m_count_matrix_nnz

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_define_precision,       only: IKIND_MESH, IKIND_MAT
  use m_allreduce_sum,          only: allreduce_sum
  !! domain, equation and matrix types
  use m_ctx_domain,             only: t_domain
  use m_tag_bc,                 only: tag_ghost_cell
  use m_ctx_discretization,     only: t_discretization
  

  !! -------------------------------------------------------------------
  implicit none

  private
  public :: count_matrix_nnz

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute matrix size by approximating number of non-zeros in it
  !
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[out]   n_nz           : approximate number of non-zeros
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine count_matrix_nnz(ctx_paral,ctx_grid,ctx_dg,flag_symmetry,  &
                              n_nz_loc,n_nz_gb,ctx_err)
  
    implicit none
    
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    logical                          ,intent(in)   :: flag_symmetry
    integer(kind=8)                  ,intent(out)  :: n_nz_loc,n_nz_gb
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MAT)  :: ntemp
    integer                  :: i_neigh,nmat_diag,nmat_upper
    integer                  :: i_order,order
    
    ctx_err%ierr  = 0
    
    !! Count for HDG
    !! -----------------------------------------------------------------
    !! The number of sub-matrices is given by the number of 
    !! variables:
    !!   e.g., if dim_var = 2, we have 
    !! 
    !! A = [Block1][Block2]   Block2 is full, Block1 and Block4
    !!     [Block3][Block4]   only have the upper part...
    !!
    !! to count the nnz we need to separate 
    !! - the block on the diagonal (only fill the upper sub-matrix)
    !! - the upperblock            (fill everything)
    !! - the lowerblock            (nothing because of symmetry)
    !! To count the nnz we have: 
    !!   (Block1 + Block2) in symmetry
    !!   (Block2)          full
    !!  
    nmat_diag   = (ctx_dg%dim_var)
    nmat_upper  = (ctx_dg%dim_var+1)*(ctx_dg%dim_var)/2 - nmat_diag
    n_nz_loc = 0
    do i_cell = 1, ctx_grid%n_cell_loc
      ntemp      = 0          
      do i_neigh = 1, ctx_grid%n_neigh_per_cell
        order   = ctx_grid%order_face(ctx_grid%cell_face(i_neigh,i_cell))
        i_order = ctx_dg%order_to_index(order)
        ntemp   = ntemp + ctx_dg%n_dof_face_per_order(i_order)
      end do
      !! cf. the comment above
      n_nz_loc = n_nz_loc + nmat_upper*(ntemp*ntemp)     & !! full block(s)
                          + nmat_diag *(ntemp*(ntemp+1))/2 !! sym  block(s)
      ! ----------------------------------------------------------------
      !! if we are not symmetric, we modify the nz 
      ! ----------------------------------------------------------------
      if(.not. flag_symmetry) then
        n_nz_loc = n_nz_loc + nmat_diag *(ntemp*(ntemp+1))/2 & !! another 
                            + nmat_upper*(ntemp*ntemp)
      end if
    
    end do
    
    !! sum all mpi processors
    call allreduce_sum(n_nz_loc,n_nz_gb,1,ctx_paral%communicator,ctx_err)
    
    return

  end subroutine count_matrix_nnz

end module m_count_matrix_nnz
