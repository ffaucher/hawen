!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_init_geometry.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module initializes the geometrical informations for the mesh 
!>     - determinant of the jacobian of all cell
!>     - normal of the faces
!>     
!
!------------------------------------------------------------------------------
module m_dg_init_geometry

  use omp_lib
  use m_raise_error,         only : t_error, raise_error
  use m_define_precision,    only : IKIND_MESH
  use m_ctx_domain,          only : t_domain
  use m_mesh_simplex_interface, &
                             only : interface_2D_triangle,              &
                                    interface_3D_tetrahedron
  use m_ctx_discretization,  only : t_discretization

  implicit none

  private
  public :: dg_init_geometry_simplex
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init mesh geometrical infos: for instance, face normals and jacobians 
  !
  !> @param[inout] ctx_dg          : DG context
  !> @param[inout] ctx_mesh        : mesh context
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine dg_init_geometry_simplex(ctx_dg,ctx_mesh,ctx_err)
    type(t_discretization) ,intent(inout):: ctx_dg
    type(t_domain)         ,intent(in)   :: ctx_mesh
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH) :: i_cell,i_node,node,i_face
    integer(kind=8):: real_size=8 ! double
    !! for 1d
    real(kind=8)   :: xseg(1,2)!,ptvect(1,2),jac1d
    !! for 2d
    real(kind=8)   :: xtri(2,3),edgevect(2,3),scaling,jac2d(2,2),det
    !! for 3d
    real(kind=8)   :: xtet(3,4),facevect1(3),facevect2(3),crossprod(3)
    real(kind=8)   :: jac3d(3,3)

    ctx_err%ierr  = 0

    !! allocate and initialize array with memory update
    ctx_dg%memory_loc = ctx_dg%memory_loc + &
          (ctx_mesh%dim_domain*ctx_mesh%n_neigh_per_cell   * &
           ctx_mesh%n_cell_loc)*real_size + &  !>  !! normals
          (ctx_mesh%n_cell_loc)*real_size + &  !>  !! det jac
          (ctx_mesh%dim_domain*ctx_mesh%dim_domain         * &
           ctx_mesh%n_cell_loc)*real_size + &  !>  !! inv jac      
          (ctx_mesh%dim_domain*ctx_mesh%dim_domain         * &
           ctx_mesh%n_cell_loc)*real_size + &  !>  !! jac      
          (ctx_mesh%n_neigh_per_cell * ctx_mesh%n_cell_loc)* &
           real_size                           !! det jac face
    allocate(ctx_dg%normal(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell, &
                           ctx_mesh%n_cell_loc))
    allocate(ctx_dg%det_jac(ctx_mesh%n_cell_loc))
    allocate(ctx_dg%inv_jac(ctx_mesh%dim_domain,ctx_mesh%dim_domain,    &
                            ctx_mesh%n_cell_loc))
    allocate(ctx_dg%jac    (ctx_mesh%dim_domain,ctx_mesh%dim_domain,    &
                            ctx_mesh%n_cell_loc))
    allocate(ctx_dg%det_jac_face(ctx_mesh%n_neigh_per_cell,ctx_mesh%n_cell_loc))
    ctx_dg%normal      =0.d0
    ctx_dg%det_jac_face=0.d0
    ctx_dg%inv_jac     =0.d0
    ctx_dg%jac         =0.d0
    ctx_dg%det_jac     =0.d0
    
    select case(ctx_dg%dim_domain)
      ! ----------------------------------------------------------------
      !! One-dimensional domain
      ! ----------------------------------------------------------------
      case(1)
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,xseg,i_node,node)
        !$OMP DO
        do i_cell=1,ctx_mesh%n_cell_loc
          
          !! get coordinates of the nodes of the cell
          do i_node=1,ctx_mesh%n_node_per_cell
            node           = ctx_mesh%cell_node(i_node,i_cell)
            xseg(:,i_node) = dble(ctx_mesh%x_node(:,node))
          end do 

          !! compute the normals: always. -1 on the left and +1 on the right
          if(xseg(1,1) < xseg(1,2)) then
            ctx_dg%normal(1,1,i_cell) =-1
            ctx_dg%normal(1,2,i_cell) =+1
          else
            ctx_dg%normal(1,1,i_cell) =+1
            ctx_dg%normal(1,2,i_cell) =-1          
          end if
          
          !! compute face jacobian for the 2 faces
          ctx_dg%det_jac_face(1,i_cell) = 1
          ctx_dg%det_jac_face(2,i_cell) = 1
          ctx_dg%det_jac(i_cell)        = abs(xseg(1,1)-xseg(1,2))
          !! compute inverse of jacobian
          ctx_dg%jac    (1,1,i_cell) =    abs(xseg(1,1)-xseg(1,2))
          ctx_dg%inv_jac(1,1,i_cell) = 1./abs(xseg(1,1)-xseg(1,2))
        
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      ! ----------------------------------------------------------------
      !! Two-dimensional domain
      ! ----------------------------------------------------------------
      case(2)
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_face,i_node,node,xtri), &
        !$OMP&         PRIVATE(edgevect,scaling,jac2d,det)
        !$OMP DO
        do i_cell=1,ctx_mesh%n_cell_loc
          
          !! get coordinates of the nodes of the cell
          do i_node=1,ctx_mesh%n_node_per_cell
            node           = ctx_mesh%cell_node(i_node,i_cell)
            xtri(:,i_node) = real(ctx_mesh%x_node(:,node),kind=8)
          end do 
          
          !! compute the 3 normals using the face opposite to node
          do i_face=1, ctx_mesh%n_neigh_per_cell !! it is the number of face
            edgevect(:,i_face) = xtri(:,interface_2D_triangle(2,i_face)) - &
                                 xtri(:,interface_2D_triangle(1,i_face))
            !! reorder for actual normal and normalization
            scaling = dsqrt(edgevect(1,i_face)**2 + edgevect(2,i_face)**2)
            ctx_dg%normal(1,i_face,i_cell) = edgevect(2,i_face) / scaling
            ctx_dg%normal(2,i_face,i_cell) =-edgevect(1,i_face) / scaling
          end do
                   
          !! compute jacobian for the 3 faces
          ctx_dg%det_jac_face(1,i_cell) = dsqrt((                           &
                                         (edgevect(1,3)+edgevect(1,2))**2 + &
                                         (edgevect(2,3)+edgevect(2,2))**2))
          ctx_dg%det_jac_face(2,i_cell)=dsqrt(edgevect(1,2)**2+edgevect(2,2)**2)
          ctx_dg%det_jac_face(3,i_cell)=dsqrt(edgevect(1,3)**2+edgevect(2,3)**2)

          !! ***********************************************************
          !! WARNING: should be the transposed of the JACOBIAN Instead
          !! ***********************************************************
          jac2d(1,:) = edgevect(:,3)
          jac2d(2,:) =-edgevect(:,2)
          ! -------------------------------------------------------
          !! WE HAVE: 
          !!       jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)_local
          ! -------------------------------------------------------
          ctx_dg%jac(:,:,i_cell) = transpose(jac2d)
          !! ***********************************************************

          det = jac2d(1,1)*jac2d(2,2) - jac2d(1,2)*jac2d(2,1)
          ctx_dg%det_jac(i_cell) = det
          if(abs(det) > 1.e-30) then
            ctx_dg%inv_jac(1,1,i_cell) = 1./(det) * jac2d(2,2)
            ctx_dg%inv_jac(2,2,i_cell) = 1./(det) * jac2d(1,1)
            ctx_dg%inv_jac(1,2,i_cell) =-1./(det) * jac2d(2,1)
            ctx_dg%inv_jac(2,1,i_cell) =-1./(det) * jac2d(1,2)
          else
            ctx_err%msg   = "** ERROR: zero determinant for jacobian " // &
                            "in [dg_init_geo] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
        
        end do
        !$OMP END DO
        !$OMP END PARALLEL

      ! ----------------------------------------------------------------
      !! Three-dimensional domain
      ! ----------------------------------------------------------------
      case(3)
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_face,i_node,node,xtet),&
        !$OMP&         PRIVATE(facevect1,facevect2,crossprod,scaling)  ,&
        !$OMP&         PRIVATE(jac3d,det)
        !$OMP DO
        do i_cell=1,ctx_mesh%n_cell_loc

          !! get coordinates of the nodes of the cell
          do i_node=1,ctx_mesh%n_node_per_cell
            node           = ctx_mesh%cell_node(i_node,i_cell)
            xtet(:,i_node) = dble(ctx_mesh%x_node(:,node))
          end do          

          !! compute the 4 normals using the face opposite to node
          do i_face=1, ctx_mesh%n_neigh_per_cell !! it is the number of face
            facevect1(:) = xtet(:,interface_3D_tetrahedron(2,i_face))-&
                           xtet(:,interface_3D_tetrahedron(1,i_face))
            facevect2(:) = xtet(:,interface_3D_tetrahedron(3,i_face))-&
                           xtet(:,interface_3D_tetrahedron(1,i_face))
            !! compute cross-product between 2 facevect
            crossprod(1) = facevect1(2)*facevect2(3)-facevect2(2)*facevect1(3)
            crossprod(2) = facevect1(3)*facevect2(1)-facevect2(3)*facevect1(1)
            crossprod(3) = facevect1(1)*facevect2(2)-facevect2(1)*facevect1(2)
            !! check outward normal ?
            !! if (sum(crossprod*(XXXXXXX)) < 0) then
            !!   crossprod = -crossprod
            !! endif
            !! reorder for actual normal and normalization
            scaling = dsqrt(crossprod(1)**2 + crossprod(2)**2 + crossprod(3)**2)
            ctx_dg%normal(1,i_face,i_cell) =-crossprod(1) / scaling
            ctx_dg%normal(2,i_face,i_cell) =-crossprod(2) / scaling
            ctx_dg%normal(3,i_face,i_cell) =-crossprod(3) / scaling
          end do

          !! compute jacobian for the 4 tetrahedron faces
          !! Face 1
          facevect1(:) =(xtet(:,3)-xtet(:,1)) - (xtet(:,2)-xtet(:,1))
          facevect2(:) =(xtet(:,4)-xtet(:,1)) - (xtet(:,2)-xtet(:,1))
          crossprod(1) = facevect1(2)*facevect2(3)-facevect2(2)*facevect1(3)
          crossprod(2) = facevect1(3)*facevect2(1)-facevect2(3)*facevect1(1)
          crossprod(3) = facevect1(1)*facevect2(2)-facevect2(1)*facevect1(2)
          ctx_dg%det_jac_face(1,i_cell)=dsqrt(sum(crossprod**2))
          !! Face 2
          facevect1(:) =(xtet(:,3)-xtet(:,1))
          facevect2(:) =(xtet(:,4)-xtet(:,1))
          crossprod(1) = facevect1(2)*facevect2(3)-facevect2(2)*facevect1(3)
          crossprod(2) = facevect1(3)*facevect2(1)-facevect2(3)*facevect1(1)
          crossprod(3) = facevect1(1)*facevect2(2)-facevect2(1)*facevect1(2)
          ctx_dg%det_jac_face(2,i_cell)=dsqrt(sum(crossprod**2))
          !! Face 3
          facevect1(:) =(xtet(:,2)-xtet(:,1))
          facevect2(:) =(xtet(:,4)-xtet(:,1))
          crossprod(1) = facevect1(2)*facevect2(3)-facevect2(2)*facevect1(3)
          crossprod(2) = facevect1(3)*facevect2(1)-facevect2(3)*facevect1(1)
          crossprod(3) = facevect1(1)*facevect2(2)-facevect2(1)*facevect1(2)
          ctx_dg%det_jac_face(3,i_cell)=dsqrt(sum(crossprod**2))
          !! Face 4
          facevect1(:) =(xtet(:,2)-xtet(:,1))
          facevect2(:) =(xtet(:,3)-xtet(:,1))
          crossprod(1) = facevect1(2)*facevect2(3)-facevect2(2)*facevect1(3)
          crossprod(2) = facevect1(3)*facevect2(1)-facevect2(3)*facevect1(1)
          crossprod(3) = facevect1(1)*facevect2(2)-facevect2(1)*facevect1(2)
          ctx_dg%det_jac_face(4,i_cell)=dsqrt(sum(crossprod**2))

          !! compute inverse of jacobian for tetrahedron
          
          !! ********** WARNING ****************************************
          !! the jacobian should be by column instead of line to have
          !! jac3d * x_ref + xloc(node1) = x_loc
          !! instead we have a transposed here........
          !! ********** WARNING ****************************************
          jac3d(1,:) = (xtet(:,2)-xtet(:,1))
          jac3d(2,:) = (xtet(:,3)-xtet(:,1))
          jac3d(3,:) = (xtet(:,4)-xtet(:,1))
          ctx_dg%jac(:,:,i_cell) = transpose(jac3d)
          !! ********** WARNING ****************************************

          !! 3x3 determinant
          det = jac3d(1,1)*jac3d(2,2)*jac3d(3,3) +                      &
                jac3d(1,2)*jac3d(2,3)*jac3d(3,1) +                      &
                jac3d(1,3)*jac3d(2,1)*jac3d(3,2) -                      &
                jac3d(1,3)*jac3d(2,2)*jac3d(3,1) -                      &
                jac3d(1,2)*jac3d(2,1)*jac3d(3,3) -                      &
                jac3d(1,1)*jac3d(2,3)*jac3d(3,2)
          ctx_dg%det_jac(i_cell) = det
          !! inverse of 3x3 matrix
          if(abs(det) > tiny(1.0)) then
            ctx_dg%inv_jac(1,1,i_cell) = 1./(det) * & 
                          (jac3d(2,2)*jac3d(3,3) - jac3d(2,3)*jac3d(3,2))
            ctx_dg%inv_jac(1,2,i_cell) =-1./(det) * & 
                          (jac3d(2,1)*jac3d(3,3) - jac3d(2,3)*jac3d(3,1))
            ctx_dg%inv_jac(1,3,i_cell) = 1./(det) * & 
                          (jac3d(2,1)*jac3d(3,2) - jac3d(2,2)*jac3d(3,1))

            ctx_dg%inv_jac(2,1,i_cell) =-1./(det) * & 
                          (jac3d(1,2)*jac3d(3,3) - jac3d(1,3)*jac3d(3,2))
            ctx_dg%inv_jac(2,2,i_cell) = 1./(det) * & 
                          (jac3d(1,1)*jac3d(3,3) - jac3d(1,3)*jac3d(3,1))
            ctx_dg%inv_jac(2,3,i_cell) =-1./(det) * & 
                          (jac3d(1,1)*jac3d(3,2) - jac3d(1,2)*jac3d(3,1))

            ctx_dg%inv_jac(3,1,i_cell) = 1./(det) * & 
                          (jac3d(1,2)*jac3d(2,3) - jac3d(1,3)*jac3d(2,2))
            ctx_dg%inv_jac(3,2,i_cell) =-1./(det) * & 
                          (jac3d(1,1)*jac3d(2,3) - jac3d(1,3)*jac3d(2,1))
            ctx_dg%inv_jac(3,3,i_cell) = 1./(det) * & 
                          (jac3d(1,1)*jac3d(2,2) - jac3d(1,2)*jac3d(2,1))

          else
            ctx_err%msg   = "** ERROR: zero determinant for jacobian " // &
                            "in [dg_init_geo] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL

      case default
        ctx_err%msg   = "** ERROR: dimension for geometrical "   // &
                        "informations not recognized [dg_init_geo] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end select
    return

  end subroutine dg_init_geometry_simplex

end module m_dg_init_geometry
