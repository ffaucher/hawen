!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_dg.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for DG informations
!
!------------------------------------------------------------------------------
module m_print_dg

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_allreduce_max,    only : allreduce_max
  use m_barrier,          only : barrier
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  use m_define_precision, only : IKIND_MAT

  implicit none
  
  private
  public :: print_dg, print_info_io_dg_cartesian,print_info_dg_refmat
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print info for the discretization using DG
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   dim_domain    : dimension of the domain
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_dg(ctx_paral,format_cell,format_basis,                 &
                      memory_loc,dim_domain,dim_sol,dim_var, n_dofsol_gb, &
                      n_dofmat_gb, n_dofsol_loc, n_dofmat_loc,            &
                      n_different_order, order_max_gb, ncell_per_order,   &
                      penal, flag_quad, quad_npts, quad_formula_vol,      &
                      quad_npts_face, quad_formula_face,                  &
                      time_lag, time_geo, verb_lvl, ctx_err)
  
    type(t_parallelism)     ,intent(in) :: ctx_paral
    character(len=*)        ,intent(in) :: format_cell,format_basis
    integer(kind=8)         ,intent(in) :: memory_loc
    real(kind=8)            ,intent(in) :: time_lag, time_geo
    integer(kind=IKIND_MAT) ,intent(in) :: n_dofsol_gb,n_dofmat_gb
    integer(kind=IKIND_MAT) ,intent(in) :: n_dofsol_loc,n_dofmat_loc
    integer                 ,intent(in) :: dim_domain,dim_sol,dim_var
    integer                 ,intent(in) :: order_max_gb,n_different_order
    integer                 ,intent(in) :: verb_lvl
    integer,allocatable     ,intent(in) :: ncell_per_order(:)
    real                    ,intent(in) :: penal
    logical                 ,intent(in) :: flag_quad
    integer                 ,intent(in) :: quad_npts
    integer                 ,intent(in) :: quad_npts_face
    character(len=*)        ,intent(in) :: quad_formula_vol
    character(len=*)        ,intent(in) :: quad_formula_face
    type(t_error)           ,intent(out):: ctx_err
    !!
    integer(kind=IKIND_MAT):: ndof_max_on_core_sol
    integer(kind=IKIND_MAT):: ndof_max_on_core_mat
    integer                :: k
    integer(kind=8)        :: memory_loc_max
    character(len=64)      :: strtemp
    character(len=64)      :: str
    character(len=7)       :: frmt
    
    ctx_err%ierr  = 0
    
    !! -----------------------------------------------------------------
    !! Note that it all depends on the level of verbose.
    !!  in the case where we have 0, we only print what can be different
    !!  from frequency to frequency: 
    !!  -> the number of different order and cells
    !!  -> Ndof per solution 
    !!  -> Ndof for matrix
    !!  -> memory
    !!  -> time
    !!
    !! -----------------------------------------------------------------
    frmt='(a,i7)'
    if(dble(n_dofsol_gb) > 1E6)  frmt='(a,i12)'
    if(dble(n_dofsol_gb) > 1E11) frmt='(a,i20)'
    
    call allreduce_max(n_dofmat_loc,ndof_max_on_core_mat,1,             &
                       ctx_paral%communicator,ctx_err)
    call allreduce_max(n_dofsol_loc,ndof_max_on_core_sol,1,             &
                       ctx_paral%communicator,ctx_err)
    call allreduce_max(memory_loc,memory_loc_max,1,                     &
                       ctx_paral%communicator,ctx_err)
    
    if(ctx_paral%master) then
    
      !! General information >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      if(verb_lvl > 0) then
        write(6,'(a)')  separator
        str="HDG "     
        write(6,'(2a)') indicator,                                 &
                      " DG method matrices initialization "//trim(adjustl(str))
        if(dim_domain.eq.1) strtemp = "- one-dimensional "
        if(dim_domain.eq.2) strtemp = "- two-dimensional "
        if(dim_domain.eq.3) strtemp = "- three-dimensional "
        select case (trim(adjustl(format_cell)))
          case('simplex')
            strtemp = trim(adjustl(strtemp)) // " SIMPLEX domain" 
          case default
            strtemp = trim(adjustl(strtemp)) //" "//&
                      trim(adjustl(format_cell))  //" domain" 
        end select
        write(6,'(a)') trim(adjustl(strtemp))
        
        !! format basis --------------------------------------------------
        select case (trim(adjustl(format_basis)))
          case('lagrange')
            write(6,'(a)') "- LAGRANGE basis functions"
          case default
            write(6,'(a)') "- "//trim(adjustl(format_basis))//" basis functions"
        end select
      end if
      !! end only if verbose > 0 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      !! Dof information
      if(n_different_order .ne. 1) then
        write(6,frmt) "- maximum order of polynomial: ", order_max_gb
        write(6,frmt) "- number of different order  : ", n_different_order
        do k=1,order_max_gb
          if(ncell_per_order(k) > 0) then
            write(6,'(a,i3,a,i7)') "-    ncells of order ",k,"     :",&
                                   ncell_per_order(k)
          end if 
        end do
      else
        write(6,frmt) "- constant polynomial order  : ", order_max_gb
      end if
      
      !! ---------------------------------------------------------------
      if(verb_lvl > 0) then
        if(penal < 0) then
          write(6,'(a)') "- default DG penalization criterion"
        else
          write(6,'(a,es15.3)') "- constant DG penalization   : ",penal
        end if
        
        !! how to evaluate the integral
        if(flag_quad) then
          write(6,'(3a,i0,a)') "- Volume integrale using ",             &
                               trim(adjustl(quad_formula_vol )),        &
                               " quadratures with ",quad_npts     ," points"
          if(dim_domain > 1) then
            write(6,'(3a,i0,a)') "- Face   integrale using ",           &
                                 trim(adjustl(quad_formula_face)),      &
                                 " quadratures with ",quad_npts_face," points"
          end if
        end if
        
      end if
      ! ----------------------------------------------------------------
        
      if(verb_lvl > 0) write(6,frmt) "- number of solution(s)      : ", dim_sol
      write(6,frmt) "- Ndof per solution          : ", n_dofsol_gb
      if(ctx_paral%nproc .ne. 1 .and. verb_lvl > 0) then
        write(6,frmt)     "- max solution ndof on 1 core: ", ndof_max_on_core_sol
      end if
        
      if(dim_sol .ne. dim_var) then
        if(verb_lvl>0) write(6,frmt) "- number of matrix variables : ", dim_var
        write(6,frmt) "- total Ndof for the matrix  : ", n_dofmat_gb*dim_var
        if(ctx_paral%nproc .ne. 1 .and. verb_lvl > 0) then
          write(6,frmt)     "- max matrix ndof on 1 core  : ", ndof_max_on_core_mat
        end if
      end if
        
      !! memory and time
      call cast_memory(memory_loc_max,str)
      if(ctx_paral%nproc .ne. 1) then
        write(6,'(2a)') "- memory max on 1 proc for DG: ",trim(adjustl(str))
      else
        write(6,'(2a)') "- memory used for DG         : ",trim(adjustl(str))
      end if
      if(verb_lvl > 0) then
        call cast_time  (time_lag,str)
        write(6,'(2a)') "- time to compute DG matrices: ",trim(adjustl(str))
        call cast_time  (time_geo,str)
        write(6,'(2a)') "- time to compute Geo infos  : ",trim(adjustl(str))
      else
        call cast_time  (time_lag+time_geo,str)
        write(6,'(2a)') "- time for DG pre-processing : ",trim(adjustl(str))
      endif

    end if
    call barrier(ctx_paral%communicator,ctx_err)
    
    return
  end subroutine print_dg

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print information regarding cartesian mapping for DG mesh
  !
  !> @param[in]    ctx_paral         : context parallelism
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine print_info_io_dg_cartesian(ctx_paral,nx,ny,nz,time,mem,ctx_err)
  
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    integer                     ,intent(in)   :: nx,ny,nz
    real(kind=8)                ,intent(in)   :: time
    integer(kind=8)             ,intent(in)   :: mem
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    character(len=512) :: str

    ctx_err%ierr  = 0
    str=''

    !! print infos
    if(ctx_paral%master) then
      write(6,'(a,i6,a,i6,a,i6)') "- cartesian map for DG mesh  : ",    &
                                     nx," x",ny," x",nz
      call cast_time(time,str)
      write(6,'(2a)')  "    - mapping time           : ",trim(adjustl(str))
      
      call cast_memory(mem,str)
      if(ctx_paral%nproc.ne.1) then
        write(6,'(2a)')"    - max memory on 1 core   : ",trim(adjustl(str))
      else
        write(6,'(2a)')"    - mapping memory         : ",trim(adjustl(str))
      end if
    endif
    
    return
    
  end subroutine print_info_io_dg_cartesian    

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print information regarding reference matrices memory
  !
  !> @param[in]    ctx_paral         : context parallelism
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine print_info_dg_refmat(ctx_paral,mem,ctx_err)
  
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    integer(kind=8)             ,intent(in)   :: mem
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    character(len=512) :: str

    ctx_err%ierr  = 0
    str=''

    !! print infos
    if(ctx_paral%master) then     
      call cast_memory(mem,str)
      write(6,'(2a)')"- mem/proc for ref matrices  : ",trim(adjustl(str))
    endif
    
    return
    
  end subroutine print_info_dg_refmat    

end module m_print_dg
