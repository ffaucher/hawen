!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_block_create.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to generate the block informations for the matrix.
!> This is used for the blkptr options in solver mumps for instance.
!
!------------------------------------------------------------------------------
module m_matrix_block_create

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_define_precision,       only: IKIND_MAT,IKIND_MESH
  use m_allreduce_sum,          only: allreduce_sum
  use m_barrier,                only: barrier
  !! domain, equation and matrix types
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  

  !! -------------------------------------------------------------------
  implicit none

  private
  public :: matrix_block_create

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the matrix block from the faces and order
  !
  !> @param[in]    ctx_paral      : parallelism type
  !> @param[in]    ctx_grid       : mesh type
  !> @param[in]    ctx_dg         : DG type
  !> @param[out]   n_blk          : number of blocks
  !> @param[out]   n_blkptr       : list the blocks
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine matrix_block_create(ctx_paral,ctx_grid,ctx_dg,n_blk,blkptr,ctx_err)
  
    implicit none
    
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_domain)                     ,intent(in)   :: ctx_grid
    type(t_discretization)             ,intent(in)   :: ctx_dg
    integer(kind=IKIND_MAT)            ,intent(out)  :: n_blk
    integer(kind=IKIND_MAT),allocatable,intent(inout):: blkptr(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local to assemble
    integer(kind=IKIND_MESH)            :: i_face,i_face_gb,offset_face
    integer                             :: i_order,order,ivar
    integer(kind=IKIND_MAT),allocatable :: blkptr_gb   (:,:)
    integer(kind=IKIND_MAT),allocatable :: blkptr_count(:,:)
    
    ctx_err%ierr  = 0
    
    !! Count for HDG, nblk is the number of faces times the number 
    !! of variables.
    n_blk = int(ctx_grid%n_face_gb*ctx_dg%dim_var,kind=IKIND_MAT)

    allocate(blkptr_gb   (2,ctx_grid%n_face_gb))
    allocate(blkptr_count(2,ctx_grid%n_face_gb))
    blkptr_gb    =0
    blkptr_count =0
    !! loop over all cells
    !! ----------------------------------------------------------------- 
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_face,order,i_order)
    !$OMP DO
    do i_face = 1, ctx_grid%n_face_loc
      order   = ctx_grid%order_face(i_face)
      i_order = ctx_dg%order_to_index(order)
      !! We must count the number of times this face appears to avoid
      !! counting twice the faces that are shared between mpi.
      blkptr_count(1,ctx_grid%index_loctoglob_face(i_face))=1
      blkptr_count(2,ctx_grid%index_loctoglob_face(i_face))=            &
                                     ctx_dg%n_dof_face_per_order(i_order)
    end do
    !$OMP END DO
    !$OMP END PARALLEL

    !! sum all mpi processors
    call allreduce_sum(blkptr_count,blkptr_gb,int(2*ctx_grid%n_face_gb),&
                       ctx_paral%communicator,ctx_err)
    deallocate(blkptr_count)

    !! move to an incremental array, only on master processor.
    if(ctx_paral%master) then
      allocate(blkptr(n_blk+1))
      blkptr(1) = 1
      !! loop over all variable too
      do ivar = 1, ctx_dg%dim_var
        offset_face = (ivar-1)*ctx_grid%n_face_gb
        do i_face = 1, ctx_grid%n_face_gb
          i_face_gb = i_face + offset_face
          blkptr(i_face_gb + 1) = blkptr(i_face_gb) +                   &
                                  blkptr_gb(2,i_face)/blkptr_gb(1,i_face)
        end do
      end do
    end if
    deallocate(blkptr_gb)


    call barrier(ctx_paral%communicator,ctx_err)
    return

  end subroutine matrix_block_create

end module m_matrix_block_create
