!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_pml_work.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to deal with the PML area by identifying the
!> zone: it is normal to the boundary.
!
!------------------------------------------------------------------------------
module m_pml_work

  use omp_lib  
  use m_define_precision,     only : IKIND_MESH
  use m_raise_error,          only : t_error, raise_error
  use m_ctx_parallelism,      only : t_parallelism
  use m_distribute_error,     only : distribute_error
  use m_allreduce_sum,        only : allreduce_sum
  use m_allreduce_min,        only : allreduce_min
  use m_allreduce_max,        only : allreduce_max
  use m_ctx_domain,           only : t_domain
  use m_ctx_discretization,   only : t_discretization
  use m_tag_bc,               only : tag_pml, tag_ghost_cell
  use m_mesh_simplex_interface, &
                              only : interface_2D_triangle,             &
                                     interface_3D_tetrahedron

  implicit none

  private  
  public :: initialize_pml_normals, initialize_pml_coeff
    
  contains  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine identify the normals of each of the PML
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine initialize_pml_normals(ctx_paral,ctx_mesh,ctx_dg,ctx_err)

    implicit none
    type(t_parallelism)       ,intent(in)        :: ctx_paral
    type(t_domain)            ,intent(inout)     :: ctx_mesh
    type(t_discretization)    ,intent(in)        :: ctx_dg
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    integer(kind=IKIND_MESH)  :: i_cell, face
    integer                   :: i_face
    integer                   :: i_tag,i_dim
    real(kind=8)              :: xloc
    real(kind=8),allocatable  :: xmin(:),xmax(:),zmin(:)
    real(kind=8),allocatable  :: zmax(:),ymin(:),ymax(:)
    real(kind=8),allocatable  :: pml_normals   (:,:)
    real(kind=8),allocatable  :: pml_normals_gb(:,:)
    integer     ,allocatable  :: pml_mpi_loc(:), pml_mpi_gb(:)

    ctx_err%ierr = 0


    allocate(ctx_mesh%pml_normals(ctx_mesh%pml_nbc,ctx_mesh%dim_domain))
    ctx_mesh%pml_normals = 0.d0 
    !! init with max
    allocate(xmin(ctx_mesh%pml_nbc)) ; allocate(xmax(ctx_mesh%pml_nbc))
    allocate(ymin(ctx_mesh%pml_nbc)) ; allocate(ymax(ctx_mesh%pml_nbc))
    allocate(zmin(ctx_mesh%pml_nbc)) ; allocate(zmax(ctx_mesh%pml_nbc))
    xmax(:) = dble(ctx_mesh%bounds_xmin)
    xmin(:) = dble(ctx_mesh%bounds_xmax)
    ymax(:) = dble(ctx_mesh%bounds_ymin)
    ymin(:) = dble(ctx_mesh%bounds_ymax)
    zmax(:) = dble(ctx_mesh%bounds_zmin)
    zmin(:) = dble(ctx_mesh%bounds_zmax)

    !! 1. for each of PML boundaries.
    !! -----------------------------------------------------------------
    if (ctx_mesh%tag_bc_pml(1) .ne. 0) then
      !! the normals of each surface.   
      allocate(pml_normals(ctx_mesh%pml_nbc,ctx_mesh%dim_domain))   
      allocate(pml_mpi_loc(ctx_mesh%pml_nbc))   
      pml_normals = 0.d0
      pml_mpi_loc = 0
      
      !! 2.
      !! loop over all cells to get the normals of each face that
      !! have the pml tag. IT MUST BE THE SAME NORMAL FOR EACH 
      !! OF THE CONCERNED CELL
      do i_cell = 1, ctx_mesh%n_cell_loc
        if(ctx_err%ierr .ne. 0) cycle !! non-shared error

        do i_face = 1, ctx_mesh%n_neigh_per_cell
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
          face = ctx_mesh%cell_neigh(i_face,i_cell)
        
          !! check with existing tags. 
          do i_tag = 1, ctx_mesh%pml_nbc
            if(ctx_err%ierr .ne. 0) cycle !! non-shared error
            
            if(face .eq. ctx_mesh%tag_bc_pml(i_tag)) then
              !! if already found it must be the SAME Normal
              if(pml_mpi_loc(i_tag) .ne. 0) then
                do i_dim = 1,ctx_mesh%dim_domain
                  if( abs(ctx_dg%normal(i_dim,i_face,i_cell) -          &
                          pml_normals(i_tag,i_dim)) > tiny(1.0) ) then
                    ctx_err%msg   = "** ERROR: all faces of the PML " //&
                                    "must have the SAME normal [pml_init] **"
                    ctx_err%ierr  = -1 
                    cycle !! non-shared error
                  end if
                end do
                !! update min/max --------------------------------------
                !! we compare the values of the node coo. for the face 
                !! where we have a pml.
                !! -----------------------------------------------------
                select case (ctx_mesh%dim_domain)
                  case(1) !! 1D
                    xloc=minval(ctx_mesh%x_node(1,ctx_mesh%cell_node(:,i_cell)))
                    xmin(i_tag) = min(xmin(i_tag),xloc)
                    xloc=maxval(ctx_mesh%x_node(1,ctx_mesh%cell_node(:,i_cell)))
                    xmax(i_tag) = max(xmax(i_tag),xloc)
                  case(2) !! 2D
                    xloc = minval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                    xmin(i_tag) = min(xmin(i_tag),xloc)
                    xloc = maxval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                    xmax(i_tag) = max(xmax(i_tag),xloc)
                    xloc = minval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                    zmin(i_tag) = min(zmin(i_tag),xloc)
                    xloc = maxval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                    zmax(i_tag) = max(zmax(i_tag),xloc)
                  case(3) !! 3D 
                    xloc = minval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    xmin(i_tag) = min(xmin(i_tag),xloc)
                    xloc = maxval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    xmax(i_tag) = max(xmax(i_tag),xloc)
                    xloc = minval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    ymin(i_tag) = min(ymin(i_tag),xloc)
                    xloc = maxval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    ymax(i_tag) = max(ymax(i_tag),xloc)
                    xloc = minval(ctx_mesh%x_node(3,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    zmin(i_tag) = min(zmin(i_tag),xloc)
                    xloc = maxval(ctx_mesh%x_node(3,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))                 
                    zmax(i_tag) = max(zmax(i_tag),xloc)
                  case default 
                    ctx_err%msg   = "** ERROR: Unrecognized dimension"//&
                                    " [pml_init] **"
                    ctx_err%ierr  = -1 
                    cycle !! non-shared error
                end select

              else !! save the normal for the first time
                pml_mpi_loc(i_tag)   = 1
                pml_normals(i_tag,:) = ctx_dg%normal(:,i_face,i_cell)
                !! get min/max -----------------------------------------
                !! we compare the values of the node coo. for the face 
                !! where we have a pml.
                !! -----------------------------------------------------
                select case (ctx_mesh%dim_domain)
                  case(1) !! 1D
                    xmin(i_tag)=minval(ctx_mesh%x_node(1,               &
                                       ctx_mesh%cell_node(:,i_cell)))
                    xmax(i_tag)=maxval(ctx_mesh%x_node(1,               &
                                       ctx_mesh%cell_node(:,i_cell)))
                  case(2) !! 2D
                    xmin(i_tag) = minval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                    xmax(i_tag) = maxval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                    zmin(i_tag) = minval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                    zmax(i_tag) = maxval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_2D_triangle(:,i_face),i_cell)))
                  case(3) !! 3D 
                    xmin(i_tag) = minval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    xmax(i_tag) = maxval(ctx_mesh%x_node(1,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    ymin(i_tag) = minval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    ymax(i_tag) = maxval(ctx_mesh%x_node(2,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    zmin(i_tag) = minval(ctx_mesh%x_node(3,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))
                    zmax(i_tag) = maxval(ctx_mesh%x_node(3,ctx_mesh%cell_node( &
                                  interface_3D_tetrahedron(:,i_face),i_cell)))                 
                  case default 
                    ctx_err%msg   = "** ERROR: Unrecognized dimension"//&
                                    " [pml_init] **"
                    ctx_err%ierr  = -1 
                    cycle !! non-shared error
                end select
                  
              end if 
            
              !! replace tag by Generic PML
              ctx_mesh%cell_neigh(i_face,i_cell) = tag_pml
            end if
          end do 
        end do
      end do
      !! possibility for non-shared error
      call distribute_error(ctx_err,ctx_paral%communicator)
      
      !! 3. check that the normals is consistent between all mpi
      !! ---------------------------------------------------------------
      allocate(pml_mpi_gb    (ctx_mesh%pml_nbc))
      allocate(pml_normals_gb(ctx_mesh%pml_nbc,ctx_mesh%dim_domain))
      call allreduce_sum(pml_mpi_loc,pml_mpi_gb,ctx_mesh%pml_nbc,       &
                         ctx_paral%communicator,ctx_err)
      call allreduce_sum(pml_normals,pml_normals_gb,ctx_mesh%pml_nbc*   &
                         ctx_mesh%dim_domain,ctx_paral%communicator,ctx_err)

      do i_tag = 1, ctx_mesh%pml_nbc
        if(ctx_err%ierr .ne. 0) cycle

        if(pml_mpi_gb(i_tag) <= 0) then
          ctx_err%msg   = "** ERROR: one of the tag for PML is NOT " // &
                          "an actual tag in the mesh [pml_init] **"
          ctx_err%ierr  = -1 
          cycle !! non-shared error        
        end if     
      
        pml_normals_gb(i_tag,:)=pml_normals_gb(i_tag,:)/dble(pml_mpi_gb(i_tag))
        if(pml_mpi_loc(i_tag) > 0) then
          !! must be the same <!
          do i_dim = 1,ctx_mesh%dim_domain
            if( abs(pml_normals_gb(i_tag,i_dim) -          &
                    pml_normals   (i_tag,i_dim)) > tiny(1.0) ) then
              ctx_err%msg   = "** ERROR: all faces of the PML " //&
                              "must have the SAME normal [pml_init] **"
              ctx_err%ierr  = -1 
              cycle !! non-shared error
            end if
          end do
        end if
      end do
      !! possibility for non-shared error
      call distribute_error(ctx_err,ctx_paral%communicator)
      deallocate(pml_normals   )
      deallocate(pml_mpi_loc   )
      deallocate(pml_mpi_gb    )
      
      !! save normals in mesh context
      ctx_mesh%pml_normals = pml_normals_gb
      deallocate(pml_normals_gb)
   
      !! save min/max values
      allocate(ctx_mesh%pml_xmin(ctx_mesh%pml_nbc))
      allocate(ctx_mesh%pml_ymin(ctx_mesh%pml_nbc))
      allocate(ctx_mesh%pml_zmin(ctx_mesh%pml_nbc))
      allocate(ctx_mesh%pml_xmax(ctx_mesh%pml_nbc))
      allocate(ctx_mesh%pml_ymax(ctx_mesh%pml_nbc))
      allocate(ctx_mesh%pml_zmax(ctx_mesh%pml_nbc))
      call allreduce_min(xmin,ctx_mesh%pml_xmin,ctx_mesh%pml_nbc,ctx_paral%communicator,ctx_err)
      call allreduce_min(ymin,ctx_mesh%pml_ymin,ctx_mesh%pml_nbc,ctx_paral%communicator,ctx_err)
      call allreduce_min(zmin,ctx_mesh%pml_zmin,ctx_mesh%pml_nbc,ctx_paral%communicator,ctx_err)
      call allreduce_max(xmax,ctx_mesh%pml_xmax,ctx_mesh%pml_nbc,ctx_paral%communicator,ctx_err)
      call allreduce_max(ymax,ctx_mesh%pml_ymax,ctx_mesh%pml_nbc,ctx_paral%communicator,ctx_err)
      call allreduce_max(zmax,ctx_mesh%pml_zmax,ctx_mesh%pml_nbc,ctx_paral%communicator,ctx_err)
      deallocate(xmin)
      deallocate(ymin)
      deallocate(zmin)
      deallocate(xmax)
      deallocate(ymax)
      deallocate(zmax)

    end if
    !! -----------------------------------------------------------------


    !! -----------------------------------------------------------------
    !! Circular PML ----------------------------------------------------
    !! only the cell detection should be dealt with, nothing special 
    !! to do here actually ?
    !! -----------------------------------------------------------------

    return
  end subroutine initialize_pml_normals


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine identify the cells that belong to each of the PML
  !>  and fill in the coefficient in all directions, for the cell, but
  !>  also for the faces, which take the max(abs(imag(coeff))) between
  !>  the adjacent cells.
  !
  !> @param[in]    ctx_mesh      : mesh context
  !> @param[inout] ctx_dg        : dg   context
  !> @param[inout] angular_freq  : frequency for the coefficient computation
  !> @param[out]   ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine initialize_pml_coeff(ctx_mesh,ctx_dg,angular_freq,ctx_err)

    implicit none
    type(t_domain)            ,intent(in)        :: ctx_mesh
    type(t_discretization)    ,intent(inout)     :: ctx_dg
    complex(kind=8)           ,intent(in)        :: angular_freq
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    real   (kind=8)              :: pml_size
    integer(kind=IKIND_MESH)     :: i_cell
    integer                      :: i_tag
    real(kind=8)                 :: mypos
    real(kind=8),allocatable     :: xmin(:),xmax(:),zmin(:)
    real(kind=8),allocatable     :: zmax(:),ymin(:),ymax(:)
    character(len=1),allocatable :: flat_pml(:)
    real(kind=8),parameter       :: pi = 4.d0*datan(1.d0)
    complex(kind=8)              :: coeff_loc,temp
    !! to compute the face 
    integer(kind=IKIND_MESH)     :: i_ghost, neigh, face
    integer                      :: i_neigh, i_dim
    complex(kind=8),allocatable  :: coeff_face(:)

    ctx_err%ierr = 0
    
    !! reset PML infos cell
    ctx_dg%pml_cell_coeff = dcmplx(1.d0,0.d0)

    !! -----------------------------------------------------------------
    !! the size depends on the format 
    pml_size = 0.d0 
    select case(trim(adjustl(ctx_mesh%pml_size_format)))
      case('wavelength')
        pml_size =  ctx_mesh%pml_size * ctx_dg%wavelength_min
        if(pml_size < tiny(1.0)) then
          ctx_err%msg   = "** ERROR: the PML size is too small "//      &
                          "because of the wavelength, you should use"// & 
                          "a 'fixed' size [pml_cells] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
      case('fixed')
        pml_size =  ctx_mesh%pml_size
      case default
        ctx_err%msg   = "** ERROR: Unknown size format for the PML, " //&
                        " should be 'fixed' or per 'wavelength' [pml_cells] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! NORMAL PML boundaries.
    !! -----------------------------------------------------------------
    if (ctx_mesh%tag_bc_pml(1) .ne. 0) then

      !! WE ONLY ALLOW FLAT PML for NOW >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      allocate(xmin(ctx_mesh%pml_nbc)) ; allocate(xmax(ctx_mesh%pml_nbc))
      allocate(ymin(ctx_mesh%pml_nbc)) ; allocate(ymax(ctx_mesh%pml_nbc))
      allocate(zmin(ctx_mesh%pml_nbc)) ; allocate(zmax(ctx_mesh%pml_nbc))
      allocate(flat_pml(ctx_mesh%pml_nbc))
      flat_pml = ''
      xmax=ctx_mesh%pml_xmax ; ymax=ctx_mesh%pml_ymax ; zmax=ctx_mesh%pml_zmax
      xmin=ctx_mesh%pml_xmin ; ymin=ctx_mesh%pml_ymin ; zmin=ctx_mesh%pml_zmin
      

      do i_tag = 1, ctx_mesh%pml_nbc

        if(any(abs(abs(ctx_mesh%pml_normals(i_tag,:))-1.d0)<tiny(1.0))) then

          !! OK: we have a flat surface with one normal to 1.0
          !! check if it is X, Y or Z and create xmin, xmax, ...
          select case(ctx_mesh%dim_domain)
            case(1) !! One-Dimension -----------------------------------
              !! either max or min 
              if(abs(ctx_mesh%pml_normals(i_tag,1)+1.d0)<tiny(1.)) then !! LEFT
                xmax(i_tag) = ctx_mesh%pml_xmax(i_tag) + pml_size
              else !! RIGHT
                xmin(i_tag) = ctx_mesh%pml_xmin(i_tag) - pml_size
              end if
              flat_pml(i_tag) = 'x'

            case(2) !! Two-Dimensions ----------------------------------
              !! either X or Z
              if    (abs(ctx_mesh%pml_normals(i_tag,1)+1.d0)<tiny(1.)) then !! LEFT X
                xmax(i_tag) = ctx_mesh%pml_xmax(i_tag) + pml_size
                flat_pml(i_tag) = 'x'
              elseif(abs(ctx_mesh%pml_normals(i_tag,1)-1.d0)<tiny(1.)) then !! RIGHT X
                xmin(i_tag) = ctx_mesh%pml_xmin(i_tag) - pml_size
                flat_pml(i_tag) = 'x'
              elseif(abs(ctx_mesh%pml_normals(i_tag,2)+1.d0)<tiny(1.)) then !! LEFT Z
                zmax(i_tag) = ctx_mesh%pml_zmax(i_tag) + pml_size
                flat_pml(i_tag) = 'z'
              elseif(abs(ctx_mesh%pml_normals(i_tag,2)-1.d0)<tiny(1.)) then !! RIGHT Z
                zmin(i_tag) = ctx_mesh%pml_zmin(i_tag) - pml_size               
                flat_pml(i_tag) = 'z'
              end if

            case(3)
              !! either X, Y or Z
              if    (abs(ctx_mesh%pml_normals(i_tag,1)+1.d0)<tiny(1.)) then !! LEFT X
                xmax(i_tag) = ctx_mesh%pml_xmax(i_tag) + pml_size
                flat_pml(i_tag) = 'x'
              elseif(abs(ctx_mesh%pml_normals(i_tag,1)-1.d0)<tiny(1.)) then !! RIGHT X
                xmin(i_tag) = ctx_mesh%pml_xmin(i_tag) - pml_size
                flat_pml(i_tag) = 'x'
              elseif(abs(ctx_mesh%pml_normals(i_tag,2)+1.d0)<tiny(1.)) then !! LEFT Y
                ymax(i_tag) = ctx_mesh%pml_ymax(i_tag) + pml_size
                flat_pml(i_tag) = 'y'
              elseif(abs(ctx_mesh%pml_normals(i_tag,2)-1.d0)<tiny(1.)) then !! RIGHT Y
                ymin(i_tag) = ctx_mesh%pml_ymin(i_tag) - pml_size               
                flat_pml(i_tag) = 'y'
              elseif(abs(ctx_mesh%pml_normals(i_tag,3)+1.d0)<tiny(1.)) then !! LEFT Z
                zmax(i_tag) = ctx_mesh%pml_zmax(i_tag) + pml_size
                flat_pml(i_tag) = 'z'
              elseif(abs(ctx_mesh%pml_normals(i_tag,3)-1.d0)<tiny(1.)) then !! LEFT Z
                zmin(i_tag) = ctx_mesh%pml_zmin(i_tag) - pml_size               
                flat_pml(i_tag) = 'z'
              end if
          end select

        else
          ctx_err%msg   = "** ERROR: only FLAT PML is supported for"//&
                          " now [pml_init] **"
          ctx_err%ierr  = -1 
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
      end do 

      ! ----------------------------------------------------------------
      !! loop over all cells to see if it is in, the coeff depends 
      !! on the format of the pml: can be cosine or default (=constant)
      !! ---------------------------------------------------------------
      !! PML based upon cosine formula >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !! [1 - i coeff/freq * cos²(pi/2*X)]
      !! using cos²(alpha/2) = 1/2*(1 + cos(alpha)) we end up with
      !! [1 - i coeff/freq * 1/2*(1 + cos(pi*X))]
      !! because we have angular frequency it ends up to
      !! [1 + coeff / \omega * 1/2*(1 + cos(pi*X)) ]
      !! all of this is ^{-1} by the way.
      ! ---------------------------------------------------------
      select case(trim(adjustl(ctx_mesh%pml_format)))
        case('cosine')

          coeff_loc = dble(2.d0*pi*ctx_mesh%pml_coeff)

          !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,i_tag,mypos)
          !$OMP DO
          !! We loop on the ghost cell too, in order to have the 
          !! max between adjacent cells on the face >>>>>>>>>>>>>>>>>>>>
          do i_cell = 1, ctx_mesh%n_cell_loc+ctx_mesh%n_ghost_loc
            do i_tag = 1, ctx_mesh%pml_nbc
              select case (flat_pml(i_tag))
                case('x') !! X ---------------------------------------------
                  if( ctx_mesh%cell_bary(1,i_cell) <=xmax(i_tag) .and. &
                      ctx_mesh%cell_bary(1,i_cell) > xmin(i_tag)) then 
                    !! position in the pml depends on the direction of 
                    !! the normal --------------------------------------
                    mypos = dble(xmax(i_tag) - ctx_mesh%cell_bary(1,i_cell))/&
                            (xmax(i_tag) - xmin(i_tag))
                    if(ctx_mesh%pml_normals(i_tag,1) < 0) mypos = 1 - mypos
                    temp = 1.d0 + coeff_loc/conjg(angular_freq)*        &
                          (1.d0 + dcos(mypos*pi))/2.d0
                    ctx_dg%pml_cell_coeff(1,i_cell) = 1.d0/temp
                  end if
                case('y') !! Y ---------------------------------------------
                  if( ctx_mesh%cell_bary(2,i_cell) <=ymax(i_tag) .and. &
                      ctx_mesh%cell_bary(2,i_cell) > ymin(i_tag)) then 
                    !! position in the pml depends on the direction of 
                    !! the normal --------------------------------------
                    mypos = dble(ymax(i_tag) - ctx_mesh%cell_bary(2,i_cell))/&
                                (ymax(i_tag) - ymin(i_tag))
                    if(ctx_mesh%pml_normals(i_tag,2) < 0) mypos = 1 - mypos
                    temp = 1.d0 + coeff_loc/conjg(angular_freq)*        &
                          (1.d0 + dcos(mypos*pi))/2.d0
                    ctx_dg%pml_cell_coeff(2,i_cell) = 1.d0/temp
                  end if

                case('z') !! Z ---------------------------------------------
                  select case (ctx_mesh%dim_domain)
                    case(2) !! 2D
                      if( ctx_mesh%cell_bary(2,i_cell) <=zmax(i_tag) .and.  &
                          ctx_mesh%cell_bary(2,i_cell) > zmin(i_tag)) then 
                        !! position in the pml depends on the direction of 
                        !! the normal ------------------------------------
                        mypos = dble(zmax(i_tag) - ctx_mesh%cell_bary(2,i_cell))/&
                                    (zmax(i_tag) - zmin(i_tag))
                        if(ctx_mesh%pml_normals(i_tag,2) < 0) mypos = 1 - mypos
                        temp = 1.d0 + coeff_loc/conjg(angular_freq)*        &
                              (1.d0 + dcos(mypos*pi))/2.d0
                        ctx_dg%pml_cell_coeff(2,i_cell) = 1.d0/temp
                      end if
                    case(3)  !! 3D
                      if( ctx_mesh%cell_bary(3,i_cell) <=zmax(i_tag) .and. &
                          ctx_mesh%cell_bary(3,i_cell) > zmin(i_tag)) then 
                        !! position in the pml depends on the direction of 
                        !! the normal ----------------------------------
                        mypos = dble(zmax(i_tag) - ctx_mesh%cell_bary(3,i_cell))/&
                                    (zmax(i_tag) - zmin(i_tag))
                        if(ctx_mesh%pml_normals(i_tag,3) < 0) mypos = 1 - mypos
                        temp = 1.d0 + coeff_loc/conjg(angular_freq)*        &
                              (1.d0 + dcos(mypos*pi))/2.d0
                        ctx_dg%pml_cell_coeff(3,i_cell) = 1.d0/temp
                      end if
                  
                  end select
      
                !! only flat for now ---------------------------------------
                case default
                  ctx_err%msg   = "** ERROR: only FLAT PML is supported for"//&
                                  " now [pml_cells] **"
                  ctx_err%ierr  = -1 
                  ctx_err%critic=.true.
                  call raise_error(ctx_err)
      
              end select
            end do
          end do
          !$OMP END DO
          !$OMP END PARALLEL
          ! -------------------------------------------------------
          
        !! default PML with constant coefficient >>>>>>>>>>>>>>>>>>>>>>>
        !! will replace derivative with 
        !!  [1 - i coeff / freq]^{-1}, 
        !! that is, using the complex frequency:
        !!  [1 + coeff / (i\omega)]^{-1}
        ! ---------------------------------------------------------
        case('default')

          coeff_loc = ctx_mesh%pml_coeff / conjg(angular_freq)
          !coeff_loc = ctx_mesh%pml_coeff / dcmplx(0.d0,-1.d0)
          
          !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,i_tag,mypos)
          !$OMP DO
          !! We loop on the ghost cell too, in order to have the 
          !! max between adjacent cells on the face >>>>>>>>>>>>>>>>>>>>
          do i_cell = 1, ctx_mesh%n_cell_loc+ctx_mesh%n_ghost_loc
            do i_tag = 1, ctx_mesh%pml_nbc
              select case (flat_pml(i_tag))
                case('x') !! X ---------------------------------------------
                  if( ctx_mesh%cell_bary(1,i_cell) <=xmax(i_tag) .and. &
                      ctx_mesh%cell_bary(1,i_cell) > xmin(i_tag)) then 
                    !! position in the pml 
                    ctx_dg%pml_cell_coeff(1,i_cell) = 1.d0 / (1.d0 + coeff_loc)
                  end if
                  
                case('y') !! Y ---------------------------------------------
                  if( ctx_mesh%cell_bary(2,i_cell) <=ymax(i_tag) .and. &
                      ctx_mesh%cell_bary(2,i_cell) > ymin(i_tag)) then 
                    ctx_dg%pml_cell_coeff(2,i_cell) = 1.d0 / (1.d0 + coeff_loc)
                  end if
 
                case('z') !! Z ---------------------------------------------
                  select case (ctx_mesh%dim_domain)
                    case(2) !! 2D
                      if( ctx_mesh%cell_bary(2,i_cell) <=zmax(i_tag) .and.  &
                          ctx_mesh%cell_bary(2,i_cell) > zmin(i_tag)) then 
                        ctx_dg%pml_cell_coeff(2,i_cell) = 1.d0 / (1.d0 + coeff_loc)
                      end if
                      
                    case(3)  !! 3D
                      if( ctx_mesh%cell_bary(3,i_cell) <=zmax(i_tag) .and. &
                          ctx_mesh%cell_bary(3,i_cell) > zmin(i_tag)) then 
                        ctx_dg%pml_cell_coeff(3,i_cell) = 1.d0 / (1.d0 + coeff_loc)
                      end if
                  
                  end select
      
                !! only flat for now ---------------------------------------
                case default
                  ctx_err%msg   = "** ERROR: only FLAT PML is supported for"//&
                                  " now [pml_cells] **"
                  ctx_err%ierr  = -1 
                  ctx_err%critic=.true.
                  call raise_error(ctx_err)
      
              end select
            end do
          end do
          !$OMP END DO
          !$OMP END PARALLEL
          ! ----------------------------------------------------------------
        
        case default
          ctx_err%msg   = "** ERROR: the pml format must be either " // &
                          " 'cosine' or 'default' [pml_cells] **"
          ctx_err%ierr  = -1 
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    
    
    end if
    !! -----------------------------------------------------------------


    !! -----------------------------------------------------------------
    !! CIRCULAR PML.
    !! -----------------------------------------------------------------
    if (ctx_mesh%tag_bc_pml_radius(1) .ne. 0) then
      !! only the cell detection should be dealt with, nothing special 
      !! to do here actually ?
      ctx_err%msg   = "** ERROR: circular PML not ready yet [pml_init] **"
      ctx_err%ierr  = -1 
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if


    !! -----------------------------------------------------------------
    !! Now that we have the coeff on the cells, we extract the ones 
    !! on the face: they take the value of the cell which has the 
    !! maximal absolute value of the imaginary part of the PML coeff.
    !! -----------------------------------------------------------------
    allocate(coeff_face(ctx_mesh%dim_domain))
    !! because ghost are at the end, not safe to use OMP here.
    i_ghost = ctx_mesh%n_cell_loc
    do i_cell = 1, ctx_mesh%n_cell_loc
      do i_neigh = 1, ctx_mesh%n_neigh_per_cell

        !! neighbor cell number
        neigh = ctx_mesh%cell_neigh(i_neigh,i_cell)

        !! carefull with ghost index that are put at the end.
        if(neigh .eq. tag_ghost_cell) then
          i_ghost = i_ghost + 1 !! already init with ncell_loc
            neigh = i_ghost     !! index cell
        end if

        !! non-boundary: check adjacent cell ---------------------------
        !! take the value by default and only change if the imaginary
        !! part in the neighbor is larger.
        coeff_face(:) = ctx_dg%pml_cell_coeff(:,i_cell)
        if(neigh>0) then !! not a boundary face
          do i_dim = 1, ctx_mesh%dim_domain
            if(abs(aimag(ctx_dg%pml_cell_coeff(i_dim,neigh ))) >        &
               abs(aimag(ctx_dg%pml_cell_coeff(i_dim,i_cell))) ) then
              coeff_face(i_dim) = ctx_dg%pml_cell_coeff(i_dim,neigh)
            end if
          end do
        end if

        !! local face number
        face  = ctx_mesh%cell_face(i_neigh,i_cell)
        ctx_dg%pml_face_coeff(:,face) = coeff_face(:)

      end do !! end loop over neighbors
    end do  !! end loop over cells
    deallocate(coeff_face)
    !! -----------------------------------------------------------------

    return
  end subroutine initialize_pml_coeff

end module m_pml_work
