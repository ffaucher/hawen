!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_discretization_field.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as API to create the fields after resolution
!> of the linear system, for HDG we need to find the wavefields 
!> from the Lagrange multipliers.
!
!------------------------------------------------------------------------------
module m_discretization_field

  !! module used -------------------------------------------------------
  use m_raise_error,                  only: raise_error, t_error
  use m_ctx_parallelism,              only: t_parallelism
  use m_ctx_domain,                   only: t_domain
  use m_ctx_discretization,           only: t_discretization
  use m_define_precision,             only: RKIND_MAT
  use m_dg_lagrange_field_construct,  only: dg_field_construct_lagrange
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: discretization_field

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate the wavefields from the monoprocessor solution 
  !> in HDG we need to work with Lagrange multipliers to extract
  !> the wavefields.
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    ctx_mesh       : mesh grid type
  !> @param[in]    n_src          : number of source
  !> @param[in]    n_field        : number of field
  !> @param[out]   n_dofsol_loc   : number of dof per sol locally
  !> @param[out]   n_dofvar_loc   : number of dof per var locally
  !> @param[inout] field          : wavefields
  !> @param[inout] multiplier     : Lagrange multiplier
  !> @param[inout] n_mult         : number of lagrange multiplier
  !> @param[in]    flag_back      : indicates if backward field to assemble
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine discretization_field(ctx_paral,ctx_dg,ctx_mesh,            &
                                  global_solution,n_src,n_mult,n_field, &
                                  n_dofsol_loc,n_dofvar_loc,multiplier, &
                                  field,flag_backward,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_discretization)             ,intent(inout):: ctx_dg
    type(t_domain)                     ,intent(in)   :: ctx_mesh    
    integer                            ,intent(in)   :: n_src,n_field
    integer                            ,intent(out)  :: n_mult
    integer                            ,intent(out)  :: n_dofsol_loc
    integer                            ,intent(out)  :: n_dofvar_loc
    complex(kind=RKIND_MAT),allocatable,intent(inout):: multiplier(:,:)
    complex(kind=RKIND_MAT),allocatable,intent(inout):: field(:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)   :: global_solution(:)
    logical                            ,intent(in)   :: flag_backward
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0
    n_mult       = ctx_dg%dim_var

    select case (trim(adjustl(ctx_dg%format_basis)))
      case('lagrange')
        call dg_field_construct_lagrange(ctx_paral,ctx_dg,ctx_mesh,     &
                                         global_solution,n_src,n_mult,  &
                                         n_field,n_dofsol_loc,          &
                                         n_dofvar_loc,multiplier,       &
                                         field,flag_backward,ctx_err)
        
      case default
        ctx_err%msg   = "** ERROR: Unrecognized basis function format "// &
                        "(need LAGRANGE) in [discretization_field] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return 
  end subroutine discretization_field
end module m_discretization_field
