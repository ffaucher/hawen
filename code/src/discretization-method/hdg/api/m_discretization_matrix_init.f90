!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_discretization_matrix_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module is used to initialize the discretization matrix
!> for HDG discretization, independently of the operator.
!> - we count the number of non-zeros.
!
!------------------------------------------------------------------------------
module m_discretization_matrix_init

  use m_raise_error,              only : t_error, raise_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_define_precision,         only : IKIND_MAT
  use m_ctx_discretization,       only : t_discretization
  use m_ctx_domain,               only : t_domain
  use m_count_matrix_nnz,         only : count_matrix_nnz
  use m_matrix_block_create,      only : matrix_block_create

  implicit none
  private
  public :: discretization_matrix_init
  
  contains
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init HDG discretization matrix: count number of non-zeros 
  !
  !> @param[inout] ctx_disc        : context discretization
  !> @param[in]    ctx_domain      : context domain (mesh)
  !> @param[in]    dim_solution    : dimension solution
  !> @param[out]   size_mat_loc    : local processors matrix size
  !> @param[out]   size_mat_gb     : global matrix size
  !> @param[out]   nnz_loc         : local processors matrix nnz
  !> @param[out]   nnz_gb          : global matrix nnz
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine discretization_matrix_init(ctx_paral,ctx_discretization,   &
                                        ctx_domain,dim_solution,        &
                                        flag_symmetric_matrix,          &
                                        size_mat_loc,size_mat_gb,       &
                                        nnz_loc,nnz_gb,                 &
                                        flag_block,nblk,blkptr,         &
                                        flag_external_pml,size_pml_ix,  &
                                        size_pml_iy,size_pml_iz,ctx_err,&
                                        o_wavelength)

    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(in)   :: ctx_discretization
    type(t_domain)         ,intent(inout):: ctx_domain
    integer                ,intent(in)   :: dim_solution
    logical                ,intent(in)   :: flag_symmetric_matrix
    integer(kind=8)        ,intent(out)  :: size_mat_loc,size_mat_gb
    logical                ,intent(in)   :: flag_block
    integer(kind=IKIND_MAT),intent(out)  :: nblk
    integer(kind=IKIND_MAT),allocatable, intent(inout)  :: blkptr(:)
    integer(kind=8)        ,intent(out)  :: nnz_loc,nnz_gb
    logical                ,intent(out)  :: flag_external_pml
    integer                ,intent(out)  :: size_pml_ix(2)
    integer                ,intent(out)  :: size_pml_iy(2)
    integer                ,intent(out)  :: size_pml_iz(2)
    type(t_error)          ,intent(out)  :: ctx_err
    real(kind=8), optional ,intent(in)   :: o_wavelength

    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! Impossible to have external pml for now at least 
    flag_external_pml = .false.
    size_pml_ix       = 0
    size_pml_iy       = 0
    size_pml_iz       = 0
    if(present(o_wavelength)) then
    end if
    !! -----------------------------------------------------------------

    !! count number of nnz from dof per cell
    call count_matrix_nnz(ctx_paral,ctx_domain,ctx_discretization,      &
                          flag_symmetric_matrix,nnz_loc,nnz_gb,ctx_err)

    size_mat_loc = ctx_discretization%n_dofloc_pervar                  *&
                   ctx_discretization%dim_var
    size_mat_gb  = ctx_discretization%n_dofgb_pervar                   *&
                   ctx_discretization%dim_var


    !! -----------------------------------------------------------------
    !! create the block infos if it is needed
    !! -----------------------------------------------------------------
    if(flag_block) then
      call matrix_block_create(ctx_paral,ctx_domain,ctx_discretization, &
                               nblk,blkptr,ctx_err)
    
    end if


    !! quick consitency check
    if(dim_solution .ne. ctx_discretization%dim_sol) then
      ctx_err%msg   = "** ERROR: dimensions for solution do not "    // &
                      "match [discretization_matrix_init] for HDG**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 
    return

  end subroutine discretization_matrix_init
  
end module m_discretization_matrix_init
