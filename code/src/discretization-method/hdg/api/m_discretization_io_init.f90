!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_discretization_io_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module is the API for the IO using DG, in particular, we may 
!> need to initialize the cartesian mapping from 
!> Discontinuous Galerkin infos, it depends on the type of elements 
!> (simplex, quadrangles)
!
!------------------------------------------------------------------------------
module m_discretization_io_init

  use m_raise_error,               only : t_error, raise_error
  use m_time,                      only : time
  use m_define_precision,          only : IKIND_MESH
  use m_ctx_parallelism,           only : t_parallelism
  use m_allreduce_min,             only : allreduce_min
  use m_allreduce_max,             only : allreduce_max
  use m_allreduce_sum,             only : allreduce_sum
  use m_ctx_domain,                only : t_domain
  use m_ctx_discretization,        only : t_discretization
  use m_find_cell,                 only : find_cell
  use m_dg_lagrange_map_cartesian, only : dg_cartesian_map_weight_lagrange
  use m_print_dg,                  only : print_info_io_dg_cartesian

  implicit none
  private
  public  :: discretization_io_init
  private :: dg_init_cartesian_map
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> check consistency of IO and create the cartesian map if needed
  !
  !> @param[in]    ctx_paral        : parallelism context
  !> @param[inout] ctx_dg           : DG context including cartesian mapping
  !> @param[in]    ctx_mesh         : mesh context
  !> @param[in]    dx,dy,dz         : discretization step
  !> @param[in]    save_structured  : flag to know what to do
  !> @param[in]    save_unstructured: flag to know what to do
  !> @param[out]   ctx_err          : error context
  !> @param[in]    o_verb_lvl       : optional verbose level
  !----------------------------------------------------------------------------
  subroutine discretization_io_init(ctx_paral,ctx_dg,ctx_mesh,dx,dy,dz, &
                                    save_structured,save_unstructured,  &
                                    ctx_err,o_verb_lvl)
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(inout):: ctx_dg
    type(t_domain)         ,intent(in)   :: ctx_mesh
    real                   ,intent(in)   :: dx,dy,dz
    logical                ,intent(in)   :: save_structured
    logical                ,intent(in)   :: save_unstructured
    type(t_error)          ,intent(out)  :: ctx_err
    integer,    optional   ,intent(in)   :: o_verb_lvl 
    !! local
    integer                :: verb_lvl
    real(kind=8)           :: time0, time1
    
    ctx_err%ierr  = 0

    verb_lvl = 1
    if(present(o_verb_lvl)) verb_lvl=o_verb_lvl
    
    if(save_unstructured) then
      !! nothing to do actually...
    end if 

    if(save_structured) then
      !! extract cartesian mapping infos
      call time(time0)
      call dg_init_cartesian_map(ctx_paral,ctx_dg,ctx_mesh,dx,dy,dz,ctx_err)
      call time(time1)
      if(verb_lvl > 0) then
        call print_info_io_dg_cartesian(ctx_paral,                      &
                                        ctx_dg%cartesian_map%nx_gb,     &
                                        ctx_dg%cartesian_map%ny_gb,     &
                                        ctx_dg%cartesian_map%nz_gb,     &
                                        time1-time0,                    &
                                        ctx_dg%cartesian_map%mem_loc,ctx_err)
      end if
    end if
    
    
    return
  end subroutine discretization_io_init
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init cartesian mapping infos for DG context
  !
  !> @param[in]    ctx_paral       : parallelism context
  !> @param[inout] ctx_dg          : DG context including cartesian mapping
  !> @param[in]    ctx_mesh        : mesh context
  !> @param[in]    dx,dy,dz        : discretization step
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine dg_init_cartesian_map(ctx_paral,ctx_dg,ctx_mesh,dx,dy,dz,ctx_err)
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(inout):: ctx_dg
    type(t_domain)         ,intent(in)   :: ctx_mesh
    real                   ,intent(in)   :: dx,dy,dz
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    real(kind=8)                           :: xmin_loc,xmax_loc,ymin_loc
    real(kind=8)                           :: ymax_loc,zmin_loc,zmax_loc
    real(kind=8)                           :: xmin_gb,xmax_gb,ymin_gb
    real(kind=8)                           :: ymax_gb,zmin_gb,zmax_gb
    integer                                :: ixmin,ixmax,iymin,iymax
    integer                                :: izmin,izmax,nx,ny,nz,count_cell
    integer                                :: ix,iy,iz
    integer(kind=IKIND_MESH)               :: local_cell
    integer(kind=IKIND_MESH),allocatable   :: overlap_loc(:,:,:)
    integer(kind=IKIND_MESH),allocatable   :: overlap_gb (:,:,:)
    real(kind=8)                           :: coo2d(2),coo3d(3),coo1d(1)
    real(kind=8)                           :: mytol
    
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! we have two parts, first we identify the cell associated to 
    !! each grid point, this is done only once,then we find the weight
    !! of each node, this must be done everytime we change the 
    !! orders
    !!
    !! -----------------------------------------------------------------
    
    !! test if we compute the cells and grid infos
    if(.not. ctx_dg%cartesian_map%flag_cell_found) then
      !! get the min and max of the DG grid 
      xmin_loc = dble(minval(ctx_mesh%x_node(1,:)))
      xmax_loc = dble(maxval(ctx_mesh%x_node(1,:)))
      select case(ctx_mesh%dim_domain) 
        case(1)
          zmin_loc = 0.
          zmax_loc = 0.
          ymin_loc = 0.
          ymax_loc = 0.
        case(2)
          zmin_loc = dble(minval(ctx_mesh%x_node(2,:)))
          zmax_loc = dble(maxval(ctx_mesh%x_node(2,:)))
          ymin_loc = 0.
          ymax_loc = 0.
        case(3)
          ymin_loc = dble(minval(ctx_mesh%x_node(2,:)))
          ymax_loc = dble(maxval(ctx_mesh%x_node(2,:)))
          zmin_loc = dble(minval(ctx_mesh%x_node(3,:)))
          zmax_loc = dble(maxval(ctx_mesh%x_node(3,:)))
        case default
          ctx_err%msg   = "** ERROR: Unrecognized domain dimension "//  &
                          "[dg_init_cartesian_map] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      !! global values
      call allreduce_min(xmin_loc,xmin_gb,1,ctx_paral%communicator,ctx_err)
      call allreduce_min(ymin_loc,ymin_gb,1,ctx_paral%communicator,ctx_err)
      call allreduce_min(zmin_loc,zmin_gb,1,ctx_paral%communicator,ctx_err)
      call allreduce_max(xmax_loc,xmax_gb,1,ctx_paral%communicator,ctx_err)
      call allreduce_max(ymax_loc,ymax_gb,1,ctx_paral%communicator,ctx_err)
      call allreduce_max(zmax_loc,zmax_gb,1,ctx_paral%communicator,ctx_err)
      
      ctx_dg%cartesian_map%nx_gb  = nint((xmax_gb -xmin_gb )/dx) + 1
      ctx_dg%cartesian_map%ny_gb  = nint((ymax_gb -ymin_gb )/dy) + 1
      ctx_dg%cartesian_map%nz_gb  = nint((zmax_gb -zmin_gb )/dz) + 1
      
      ctx_dg%cartesian_map%ixmin_loc=nint((xmin_loc-xmin_gb)/dx) + 1
      ctx_dg%cartesian_map%ixmax_loc=nint((xmax_loc-xmin_gb)/dx) + 1
      ctx_dg%cartesian_map%iymin_loc=nint((ymin_loc-ymin_gb)/dy) + 1
      ctx_dg%cartesian_map%iymax_loc=nint((ymax_loc-ymin_gb)/dy) + 1
      ctx_dg%cartesian_map%izmin_loc=nint((zmin_loc-zmin_gb)/dz) + 1
      ctx_dg%cartesian_map%izmax_loc=nint((zmax_loc-zmin_gb)/dz) + 1
      
      ctx_dg%cartesian_map%nx_loc = ctx_dg%cartesian_map%ixmax_loc - &
                                    ctx_dg%cartesian_map%ixmin_loc + 1
      ctx_dg%cartesian_map%ny_loc = ctx_dg%cartesian_map%iymax_loc - &
                                    ctx_dg%cartesian_map%iymin_loc + 1
      ctx_dg%cartesian_map%nz_loc = ctx_dg%cartesian_map%izmax_loc - &
                                    ctx_dg%cartesian_map%izmin_loc + 1
      ctx_dg%cartesian_map%dx     = dx
      ctx_dg%cartesian_map%dy     = dy
      ctx_dg%cartesian_map%dz     = dz
      ctx_dg%cartesian_map%ox     = dble(xmin_gb)
      ctx_dg%cartesian_map%oy     = dble(ymin_gb)
      ctx_dg%cartesian_map%oz     = dble(zmin_gb)

      !! count how many nodes are really in the subgrid, at most one per node
      !! it can be much less if we have circle shaped domain
      nx   =ctx_dg%cartesian_map%nx_loc
      ny   =ctx_dg%cartesian_map%ny_loc
      nz   =ctx_dg%cartesian_map%nz_loc
      ixmin=ctx_dg%cartesian_map%ixmin_loc
      iymin=ctx_dg%cartesian_map%iymin_loc
      izmin=ctx_dg%cartesian_map%izmin_loc
      ixmax=ctx_dg%cartesian_map%ixmax_loc
      iymax=ctx_dg%cartesian_map%iymax_loc
      izmax=ctx_dg%cartesian_map%izmax_loc
      
      !! local memory to store all of those
      !! the weight mem is updated later
      ctx_dg%cartesian_map%mem_loc = ( 2*8)                + & !!  2 real*8
                                     (13*4)                + & !! 13 int
                                     ( 6*4)                + & !!  6 real
                                     (nx*ny*nz*IKIND_MESH) + & !! i_cell 
              !! cell array can be 2x size_global when saving !
              (2*ctx_dg%cartesian_map%nx_gb*ctx_dg%cartesian_map%ny_gb* &
                 ctx_dg%cartesian_map%nz_gb)
      
      !! overlapping check
      allocate(overlap_loc(ctx_dg%cartesian_map%nx_gb,                  &
                           ctx_dg%cartesian_map%ny_gb,                  &
                           ctx_dg%cartesian_map%nz_gb))
      overlap_loc = ctx_mesh%n_cell_gb + 10 !! we reduce_min later on
      allocate(ctx_dg%cartesian_map%i_cell(nx,ny,nz))
      ctx_dg%cartesian_map%i_cell = 0 
      count_cell=0
      select case(ctx_mesh%dim_domain)
        case(1)

          mytol = min(1.d-12, dx/1.d3)
          do ix=ixmin,ixmax
            coo1d(1) = dble(ctx_dg%cartesian_map%ox) + dble((ix-1)*dx)
            !! round-off, make sure we are still in 
            coo1d(1) = min(coo1d(1),xmax_gb)

            !! move slightly in to make sure we have a point.
            if(ix==1                         ) coo1d(1) = coo1d(1)+mytol
            if(ix==ctx_dg%cartesian_map%nx_gb) coo1d(1) = coo1d(1)-mytol

            !! *********************************************************
            !! ** WARNING ******* THIS ROUTINE USES OMP  ***************
            !! *********************************************************
            call find_cell(ctx_paral,coo1d,local_cell,ctx_mesh%x_node,  &
                     ctx_mesh%cell_node,ctx_mesh%index_loctoglob_cell,  &
                     ctx_mesh%n_cell_loc,ctx_mesh%n_cell_gb,            &
                     ctx_mesh%dim_domain,ctx_mesh%cell_neigh,           &
                     ctx_dg%format_cell,.false.,ctx_err)
            if(local_cell > 0) then
              count_cell = count_cell + 1
              ctx_dg%cartesian_map%i_cell(ix-ixmin+1,1,1)=local_cell
              overlap_loc(ix,1,1) = ctx_mesh%index_loctoglob_cell(local_cell)
            end if
          end do 

        case(2)

          mytol = min(1.d-12, dx/1.d3)
          mytol = min(mytol , dz/1.d3)
          do ix=ixmin,ixmax ; do iz=izmin,izmax
            coo2d(1) = dble(ctx_dg%cartesian_map%ox) + dble((ix-1)*dx)
            coo2d(2) = dble(ctx_dg%cartesian_map%oz) + dble((iz-1)*dz)
            !! round-off, make sure we are still in 
            coo2d(1) = min(coo2d(1),xmax_gb)
            coo2d(2) = min(coo2d(2),zmax_gb)

            !! move slightly in to make sure we have a point.
            if(ix==1                         ) coo2d(1) = coo2d(1)+mytol
            if(iz==1                         ) coo2d(2) = coo2d(2)+mytol
            if(ix==ctx_dg%cartesian_map%nx_gb) coo2d(1) = coo2d(1)-mytol
            if(iz==ctx_dg%cartesian_map%nz_gb) coo2d(2) = coo2d(2)-mytol

            !! *******************************************************
            !! ** WARNING ******* THIS ROUTINE USES OMP  *************
            !! *******************************************************
            call find_cell(ctx_paral,coo2d,local_cell,ctx_mesh%x_node,&
                     ctx_mesh%cell_node,ctx_mesh%index_loctoglob_cell,&
                     ctx_mesh%n_cell_loc,ctx_mesh%n_cell_gb,          &
                     ctx_mesh%dim_domain,ctx_mesh%cell_neigh,         &
                     ctx_dg%format_cell,.false., ctx_err)

            if(local_cell > 0) then
              count_cell = count_cell + 1
              ctx_dg%cartesian_map%i_cell(ix-ixmin+1,1,iz-izmin+1)=local_cell
              overlap_loc(ix,1,iz) = ctx_mesh%index_loctoglob_cell(local_cell)
            end if
          end do ; end do

        case(3)

          mytol = min(1.d-12, dx/1.d3)
          mytol = min(mytol , dy/1.d3)
          mytol = min(mytol , dz/1.d3)
          do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
            coo3d(1) = dble(ctx_dg%cartesian_map%ox) + dble((ix-1)*dx)
            coo3d(2) = dble(ctx_dg%cartesian_map%oy) + dble((iy-1)*dy)
            coo3d(3) = dble(ctx_dg%cartesian_map%oz) + dble((iz-1)*dz)
            !! round-off, make sure we are still in 
            coo3d(1) = min(coo3d(1),xmax_gb)
            coo3d(2) = min(coo3d(2),ymax_gb)
            coo3d(3) = min(coo3d(3),zmax_gb)

            !! move slightly in to make sure we have a point.
            if(ix==1                         ) coo3d(1) = coo3d(1)+mytol
            if(iy==1                         ) coo3d(2) = coo3d(2)+mytol
            if(iz==1                         ) coo3d(3) = coo3d(3)+mytol
            if(ix==ctx_dg%cartesian_map%nx_gb) coo3d(1) = coo3d(1)-mytol
            if(iy==ctx_dg%cartesian_map%ny_gb) coo3d(2) = coo3d(2)-mytol
            if(iz==ctx_dg%cartesian_map%nz_gb) coo3d(3) = coo3d(3)-mytol


            !! *********************************************************
            !! ** WARNING ******* THIS ROUTINE USES OMP  ***************
            !! *********************************************************
            call find_cell(ctx_paral,coo3d,local_cell,ctx_mesh%x_node,  &
                     ctx_mesh%cell_node,ctx_mesh%index_loctoglob_cell,  &
                     ctx_mesh%n_cell_loc,ctx_mesh%n_cell_gb,            &
                     ctx_mesh%dim_domain,ctx_mesh%cell_neigh,           &
                     ctx_dg%format_cell,.false.,ctx_err)
            if(local_cell > 0) then
              count_cell = count_cell + 1
              ctx_dg%cartesian_map%i_cell(ix-ixmin+1,iy-iymin+1,        &
                                          iz-izmin+1) = local_cell
              overlap_loc(ix,iy,iz)=ctx_mesh%index_loctoglob_cell(local_cell)
            end if
          end do ; end do ; end do
        
        case default
          ctx_err%msg   = "** ERROR: Unrecognized dimension [init_dg_cart] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      
      !! ---------------------------------------------------------------
      !! Verify there is no overlaping in the nodes
      !! not sure it is optimal, we just divide the weight
      !! ---------------------------------------------------------------
      allocate(overlap_gb(ctx_dg%cartesian_map%nx_gb,                   &
                          ctx_dg%cartesian_map%ny_gb,                   &
                          ctx_dg%cartesian_map%nz_gb))
      overlap_gb = 0
      call allreduce_min(overlap_loc,overlap_gb,                        &
                         ctx_dg%cartesian_map%nx_gb*                    &
                         ctx_dg%cartesian_map%ny_gb*                    &
                         ctx_dg%cartesian_map%nz_gb,                    &
                         ctx_paral%communicator,ctx_err)
      !! adjust in case we have not the right one
      do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
        local_cell=ctx_dg%cartesian_map%i_cell(ix-ixmin+1,iy-iymin+1,iz-izmin+1)
        if(local_cell > 0) then
          if(ctx_mesh%index_loctoglob_cell(local_cell) .ne.             &
             overlap_gb(ix,iy,iz)) then
            count_cell = count_cell - 1
            ctx_dg%cartesian_map%i_cell(ix-ixmin+1,iy-iymin+1,iz-izmin+1)=0
          end if
        end if
      end do ; end do ; end do
      deallocate(overlap_loc)
      deallocate(overlap_gb)
      !! ---------------------------------------------------------------
      
      ctx_dg%cartesian_map%ncell_loc = count_cell
      allocate(ctx_dg%cartesian_map%weight(nx,ny,nz))
    end if
    !! end if we compute the cell positions
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_dg%cartesian_map%flag_cell_found = .true.

    !! -----------------------------------------------------------------
    !! initialize the weights depending on the type of element and 
    !! basis functions (only simplex lagrange for now)
    !! -----------------------------------------------------------------    
    select case (trim(adjustl(ctx_dg%format_cell)))
      case('simplex')
        call dg_cartesian_map_weight_lagrange(ctx_mesh,ctx_dg,          &
         ctx_dg%cartesian_map%ixmin_loc,ctx_dg%cartesian_map%ixmax_loc, &
         ctx_dg%cartesian_map%iymin_loc,ctx_dg%cartesian_map%iymax_loc, & 
         ctx_dg%cartesian_map%izmin_loc,ctx_dg%cartesian_map%izmax_loc, &
         ctx_dg%cartesian_map%dx,ctx_dg%cartesian_map%dy,               &
         ctx_dg%cartesian_map%dz,ctx_dg%cartesian_map%ox,               &
         ctx_dg%cartesian_map%oy,ctx_dg%cartesian_map%oz,               &
         ctx_dg%cartesian_map%i_cell,ctx_dg%cartesian_map%weight,       &
         ctx_dg%cartesian_map%mem_loc,ctx_err)
      case default
        ctx_err%msg   = "** ERROR: only SIMPLEX cells supported " // &
                        "[dg_cartesian_map] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    call allreduce_sum(ctx_dg%cartesian_map%mem_loc,                    &
                       ctx_dg%cartesian_map%mem_gb,1,                   &
                       ctx_paral%communicator,ctx_err)
    return

  end subroutine dg_init_cartesian_map
  
end module m_discretization_io_init
