!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_discretization_acquisition.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize acquisition infos for DG discretization
!> of sources
!
!------------------------------------------------------------------------------
module m_discretization_acquisition

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_acquisition,        only: t_acquisition,SRC_PW,SRC_ANALYTIC
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_dg_lagrange_src_rcv_api,only: dg_src_rcv_init_lagrange
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: discretization_acquisition

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of sources:
  !> - the cell(s) which are involved
  !> - the weight for the integral 
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_mesh       : mesh grid type
  !> @param[inout] ctx_dg         : type discretization DG
  !> @param[in]    nrhs           : current number of rhs
  !> @param[in]    i_src_first    : first source index in acquisition
  !> @param[in]    i_src_last     : last source index in acquisition
  !> @param[in]    tag_scale_rhs  : indicates if scaling the RHS
  !> @param[in]    rhs_format     : indicates the format dense or sparse
  !> @param[in]    freq           : frequency for plane-wave only
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine discretization_acquisition(ctx_paral,ctx_aq,ctx_mesh,      &
                                        ctx_dg,nrhs,i_src_first,        &
                                        i_src_last,tag_scale_rhs,       &
                                        rhs_format,freq,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_mesh
    type(t_discretization)           ,intent(inout):: ctx_dg
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: nrhs,i_src_first
    integer                          ,intent(in)   :: i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    character(len=*)                 ,intent(inout):: rhs_format
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! rhs format depends on the typ of source:
    !!  - sparse for dirac
    !!  - dense for planewave
    rhs_format = ''

    !! consitency: if we have a planewave, there must be a planewave
    !! boundary condition somewhere.
    if(ctx_aq%src_type == SRC_PW .and.                                  &
      ( (.not. ctx_mesh%flag_bc_planewave)       .and.                  &
        (.not. ctx_mesh%flag_bc_planewave_scatt) .and.                  &
        (.not. ctx_mesh%flag_bc_planewave_isoP)  .and.                  &
        (.not. ctx_mesh%flag_bc_planewave_isoS)  .and.                  &
        (.not. ctx_mesh%flag_bc_planewave_vtiYSH).and.                  &
        (.not. ctx_mesh%flag_bc_planewave_vtiYSV).and.                  &
        (.not. ctx_mesh%flag_bc_planewave_vtiYqP))) then
      ctx_err%msg   = "** ERROR: the planewave source requires at "  // &
                      "least 1 planewave boundary condition "        // &
                      "[dg_source_init] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(ctx_aq%src_type == SRC_ANALYTIC .and.                            &
      ((.not.ctx_mesh%flag_bc_analytic_orthorhombic) .and.              &
       (.not.ctx_mesh%flag_bc_analytic_elasticiso))) then
      ctx_err%msg   = "** ERROR: the analytic function condition "  //  &
                      "requires at least 1 analytic "               //  &
                      "boundary condition [dg_source_init] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(ctx_mesh%flag_bc_planewave .and.                                 &
      (ctx_aq%src_type .ne. SRC_PW)) then
      ctx_err%msg   = "** ERROR: there is a planewave boundary " //     &
                      "condition while the source is a dirac, it " //   &
                      "is not supported [dg_source_init] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if    
    !! -----------------------------------------------------------------
    
    select case (trim(adjustl(ctx_dg%format_basis)))
      case('lagrange')
        call dg_src_rcv_init_lagrange(ctx_paral,ctx_aq,ctx_mesh,ctx_dg, & 
                                      nrhs,i_src_first,i_src_last,      &
                                      tag_scale_rhs,rhs_format,freq,    &
                                      ctx_err)
     
      case default
        ctx_err%msg   = "** ERROR: Unrecognized basis function format "// &
                        "(need LAGRANGE) in [dg_source_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return 
  end subroutine discretization_acquisition
end module m_discretization_acquisition
