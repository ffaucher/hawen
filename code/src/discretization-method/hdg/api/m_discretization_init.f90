!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_discretization_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module is the API to initialize the context for Discontinuous
!> Galerkin infos, it could depend on the type of elements at the 
!! end (simplex, quadrangles) for now we only have simplex.
!
!------------------------------------------------------------------------------
module m_discretization_init

  use m_raise_error,                  only : t_error, raise_error
  use m_distribute_error,             only : distribute_error
  use m_define_precision,             only : IKIND_MESH
  use m_ctx_parallelism,              only : t_parallelism
  use m_allreduce_sum,                only : allreduce_sum
  use m_time,                         only : time
  use m_ctx_domain,                   only : t_domain
  use m_ctx_discretization,           only : t_discretization
  use m_quadrature_init,              only : quadrature_init
  use m_print_dg,                     only : print_dg
  use m_dg_init_geometry,             only : dg_init_geometry_simplex
  use m_dg_lagrange_simplex_init_ctx, only : dg_init_lagrange_simplex
  use m_pml_work,                     only : initialize_pml_normals

  implicit none
  private
  public :: discretization_init
  
  contains
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init General DG context, in particular from the mesh infos
  !
  !> @param[in]    ctx_paral       : parallelism context
  !> @param[inout] ctx_dg          : DG context
  !> @param[in]    ctx_mesh        : mesh context
  !> @param[in]    dim_var         : dimension matrix variables
  !> @param[in]    dim_solution    : dimension solution
  !> @param[in]    name_var        : name of the matrix variables
  !> @param[in]    name_solution   : name of the volume solutions
  !> @param[in]    tag_method      : method
  !> @param[in]    flag_inv        : indicates if inversion
  !> @param[out]   ctx_err         : error context
  !> @param[in]    o_verb_lvl      : optional verbose level
  !----------------------------------------------------------------------------
  subroutine discretization_init(ctx_paral,ctx_dg,ctx_mesh,dim_var,     &
                                 dim_solution,name_var,name_sol,        &
                                 flag_inv,ctx_err,o_verb_lvl)

    implicit none

    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(inout):: ctx_dg
    type(t_domain)         ,intent(inout):: ctx_mesh
    integer                ,intent(in)   :: dim_var,dim_solution
    character(len=3)       ,intent(in)   :: name_var(:)
    character(len=3)       ,intent(in)   :: name_sol(:)
    logical                ,intent(in)   :: flag_inv
    type(t_error)          ,intent(out)  :: ctx_err
    integer,    optional   ,intent(in)   :: o_verb_lvl
    !! local
    integer(kind=IKIND_MESH)         :: ic
    integer(kind=4)                  :: verb_lvl,k,j
    integer     ,allocatable         :: ncell_order_loc(:)
    integer     ,allocatable         :: ncell_order_gb (:)
    real(kind=8)                     :: time1,time2,time_lagrange,time_geo

    ctx_err%ierr  = 0

    verb_lvl = 1
    if(present(o_verb_lvl)) verb_lvl=o_verb_lvl
    !! -----------------------------------------------------------------
    !! initialize the context depending on the type of element and 
    !! basis functions (only simplex lagrange for now)
    !! -----------------------------------------------------------------
    ctx_dg%format_basis = 'lagrange'
    ctx_dg%dim_domain   = ctx_mesh%dim_domain
    ctx_dg%penalization = ctx_mesh%penalization
    ctx_dg%dim_sol      = dim_solution
    ctx_dg%dim_var      = dim_var
    ctx_dg%hdg%n_interface_combi = ctx_mesh%n_interface_combi
    ctx_dg%flag_eval_int_quadrature = ctx_mesh%flag_eval_int_quadrature
    ctx_dg%memory_loc   = 0
    allocate(ctx_dg%name_sol(dim_solution))
    allocate(ctx_dg%name_var(dim_var     ))
    ctx_dg%name_sol(:) = name_sol
    ctx_dg%name_var(:) = name_var
    !! find the index of the trace variable in the global solutions.
    allocate(ctx_dg%ind_var_to_sol(dim_var))
    ctx_dg%ind_var_to_sol = 0
    do k = 1,dim_var
      do j = 1,dim_solution
        if(trim(adjustl(name_var(k))) .eq. trim(adjustl(name_sol(j)))) then
          ctx_dg%ind_var_to_sol(k) = j 
        end if
      end do
    end do

    select case (trim(adjustl(ctx_mesh%format_element)))
      case('triangle','tetrahedron','segment')
        ctx_dg%format_cell  = 'simplex'
        
        !! hdg informations --------------------------------------------
        call time(time1)
        call dg_init_lagrange_simplex(ctx_paral,ctx_dg,ctx_mesh,flag_inv, &
                                      ctx_err)

        !! initialize quadrature rules if it is needed -----------------
        !! for now we have a fixed number of quadrature points. This 
        !! should change to be adapted to the maximal order for the 
        !! problem, that is, it should change at each frequency, depending
        !! on the pre-computed orders. 
        !! -------------------------------------------------------------
        if(ctx_dg%flag_eval_int_quadrature) then
          call quadrature_init(ctx_dg%dim_domain,                       &
                               ctx_dg%quadGL_npts,ctx_dg%quadGL_weight, &
                               ctx_dg%quadGL_pts,ctx_dg%order_max_gb,   &
                               ctx_dg%quad_formula_volumic,ctx_err)
          !! for the faces as well .....................................
          if(ctx_dg%dim_domain > 1) then
            call quadrature_init(ctx_dg%dim_domain-1,                   &
                                 ctx_dg%quadGL_face_npts,               &
                                 ctx_dg%quadGL_face_weight,             &
                                 ctx_dg%quadGL_face_pts,                &
                                 ctx_dg%order_max_gb,                   &
                                 ctx_dg%quad_formula_face,ctx_err)
          elseif(ctx_dg%dim_domain == 1) then
            !! work with only the end-point
            ctx_dg%quad_formula_face= ''
            ctx_dg%quadGL_face_npts = 1
            allocate(ctx_dg%quadGL_face_pts (1,1))
            allocate(ctx_dg%quadGL_face_weight(1))
            ctx_dg%quadGL_face_weight = 1.0
            !! only the points in 0.0 (which makes sense from the 
            !! polynomial_integrate function which gives only the 
            !! coeff(1) if dim=0, that is, the constant
            ctx_dg%quadGL_face_pts    = 0.0 
          else
            ctx_err%msg   = "** ERROR: invalid dimension [dg_init] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
        end if
        
        call time(time_lagrange)
        time_lagrange=time_lagrange-time1
        
        !! geometrical informations ------------------------------------
        call time(time1)
        call dg_init_geometry_simplex(ctx_dg,ctx_mesh,ctx_err)      
        call time(time_geo)
        time_geo=time_geo-time1

      case default
        ctx_err%msg   = "** ERROR: only SIMPLEX cells supported [dg_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    !! initialize PML informations if any ------------------------------
    if(ctx_mesh%pml_flag) then
      call time(time1)
      !! initialization of basic informations is done once only
      if(.not. ctx_mesh%pml_flag_init) then
        call initialize_pml_normals(ctx_paral,ctx_mesh,ctx_dg,ctx_err) 
        !! possibility for non-shared error
        call distribute_error(ctx_err,ctx_paral%communicator)
        ctx_mesh%pml_flag_init = .true.
      end if
      call time(time2)
      time_geo=time_geo + (time2-time1)
    end if
    !! -----------------------------------------------------------------
    
    !! count how many cell per order
    allocate(ncell_order_loc(ctx_dg%order_max_gb))
    allocate(ncell_order_gb (ctx_dg%order_max_gb))
    ncell_order_loc=0
    ncell_order_gb =0
    do ic=1,ctx_mesh%n_cell_loc
      ncell_order_loc(ctx_mesh%order(ic)) = ncell_order_loc(ctx_mesh%order(ic))+1
    enddo
    call allreduce_sum(ncell_order_loc,ncell_order_gb,                  &
                       ctx_dg%order_max_gb,ctx_paral%communicator,ctx_err)
    deallocate(ncell_order_loc)

    !! print infos on screen
    call print_dg(ctx_paral,ctx_dg%format_cell,                           &
                  ctx_dg%format_basis,ctx_dg%memory_loc,ctx_dg%dim_domain,&
                  ctx_dg%dim_sol,ctx_dg%dim_var,ctx_dg%n_dofgb_persol,    &
                  ctx_dg%n_dofgb_pervar,ctx_dg%n_dofloc_persol,           &
                  ctx_dg%n_dofloc_pervar,ctx_dg%n_different_order,        &
                  ctx_dg%order_max_gb,ncell_order_gb,ctx_dg%penalization, &
                  !! quadrature infos
                  ctx_dg%flag_eval_int_quadrature,                        &
                  ctx_dg%quadGL_npts,ctx_dg%quad_formula_volumic,         &
                  ctx_dg%quadGL_face_npts,ctx_dg%quad_formula_face,       &
                  !! time
                  time_lagrange,time_geo,verb_lvl,ctx_err)
    deallocate(ncell_order_gb )

    return

  end subroutine discretization_init
  
end module m_discretization_init
