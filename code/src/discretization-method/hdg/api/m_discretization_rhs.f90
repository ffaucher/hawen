!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_discretization_rhs.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used as API to create the righ-hand side for DG propagators
!
!------------------------------------------------------------------------------
module m_discretization_rhs

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MAT, RKIND_MAT, IKIND_MESH
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_domain,             only: t_domain
  use m_ctx_acquisition,        only: t_acquisition
  use m_ctx_discretization,     only: t_discretization
  use m_dg_lagrange_rhs_sparse, only: dg_rhs_create_lagrange_sparse,    &
                                      dg_rhs_backward_create_lagrange_sparse
  !! for the full source: to represent the model and the routine
  use m_ctx_model_representation, &
                                only: t_model_param
  use m_dg_lagrange_source_field, &
                                only: dg_source_init_field,             &
                                      dg_source_backward_init_field
  use m_dg_lagrange_rhs_dense,  only: dg_rhs_create_lagrange_dense,     &
                                      dg_rhs_backward_create_lagrange_dense
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: discretization_rhs_sparse
  public :: discretization_rhs_backward_sparse
  public :: discretization_rhs_field_full
  public :: discretization_rhs_backward_field_full

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate the righ-hand side for DG in a sparse representation
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] ctx_dg         : type DG
  !> @param[inout] ctx_mesh       : type mesh
  !> @param[in]    nrhs           : number of rhs
  !> @param[in]    n_rhs_comp     : number of rhs component
  !> @param[in]    rhs_i_comp     : index  of rhs component
  !> @param[in]    rhs_i_comp_bc  : index  of rhs component for boundary
  !> @param[in]    source_value   : source value
  !> @param[inout] csri           : CSR matrix I
  !> @param[inout] icol           : CSR matrix col
  !> @param[inout] Rval           : CSR matrix values
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine discretization_rhs_sparse(ctx_paral,ctx_dg,ctx_mesh,       &
                                       nrhs,n_rhs_component,            &
                                       rhs_i_component,                 &
                                       rhs_i_component_bc,source_value, &
                                       csri,icol,Rval,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_discretization)             ,intent(inout) :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    integer                            ,intent(in)    :: nrhs
    integer                            ,intent(in)    :: n_rhs_component
    integer                ,allocatable,intent(in)    :: rhs_i_component   (:)
    integer                ,allocatable,intent(in)    :: rhs_i_component_bc(:)
    complex(kind=8)                    ,intent(in)    :: source_value
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: csri(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: icol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: Rval(:)
    type(t_error)                      ,intent(inout) :: ctx_err

    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case (trim(adjustl(ctx_dg%format_basis)))
      case('lagrange')
        call dg_rhs_create_lagrange_sparse(ctx_paral,source_value,      &
                                           ctx_dg,ctx_mesh,nrhs,        &
                                           n_rhs_component,             &
                                           rhs_i_component,             &
                                           rhs_i_component_bc,csri,icol,&
                                           Rval,ctx_err)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized basis function format "// &
                        "(need LAGRANGE) in [discretization_rhs] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine discretization_rhs_sparse

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate the righ-hand side for DG in a sparse representation
  !> for the backward RHS in FWI
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] ctx_dg         : type DG
  !> @param[inout] ctx_mesh       : type mesh
  !> @param[in]    nrhs           : number of rhs
  !> @param[in]    n_comp         : number of component
  !> @param[in]    rhs_i_comp     : index  of rhs component
  !> @param[in]    source_residu  : array with rhs val(n_rcv,n_comp,n_rhs)
  !> @param[inout] csri           : CSR matrix I
  !> @param[inout] icol           : CSR matrix col
  !> @param[inout] Rval           : CSR matrix values
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine discretization_rhs_backward_sparse(ctx_paral,ctx_dg,       &
                                       ctx_mesh,nrhs,source_residuals,  &
                                       csri,icol,Rval,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_discretization)             ,intent(inout) :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    integer                            ,intent(in)    :: nrhs
    complex(kind=8)        ,allocatable,intent(in)    :: source_residuals(:,:,:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: csri(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: icol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: Rval(:)
    type(t_error)                      ,intent(inout) :: ctx_err

    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case (trim(adjustl(ctx_dg%format_basis)))
      case('lagrange')
        call dg_rhs_backward_create_lagrange_sparse(ctx_paral,ctx_dg,   &
                             ctx_mesh,nrhs,source_residuals,csri,icol,  &
                             Rval,ctx_err)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized basis function format "// &
                        "(need LAGRANGE) in [discretization_rhs] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine discretization_rhs_backward_sparse


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate the righ-hand side for DG in the case where we have
  !> a full RHS based upon an input wavefield.
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] ctx_dg         : type DG
  !> @param[inout] ctx_mesh       : type mesh
  !> @param[inout] ctx_model_param: type model parameter
  !> @param[in]    field          : field array used for RHS
  !> @param[in]    n_rhs_component: number of component concerned
  !> @param[in]    rhs_i_component: list of rhs component 
  !> @param[in]    format_model   : format of the model
  !> @param[in]    i_src_first    : first source
  !> @param[in]    i_src_last     : last source 
  !> @param[inout] tag_scale_rhs  : rhs scaling
  !> @param[inout] icol           : number of RHS
  !> @param[inout] Rval           : RHS full
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine discretization_rhs_field_full(ctx_paral,ctx_dg,ctx_mesh,   &
                                           ctx_model_param_real,        &
                                           ctx_model_param_imag,field,  &
                                           n_rhs_component,             &
                                           rhs_i_component,             &
                                           format_model,                &
                                           i_src_first,i_src_last,      &
                                           tag_scale_rhs,nrhs,Rval,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_domain)                     ,intent(in)   :: ctx_mesh
    type(t_discretization)             ,intent(inout):: ctx_dg
    type(t_model_param)                ,intent(in)   :: ctx_model_param_real
    type(t_model_param)                ,intent(in)   :: ctx_model_param_imag
    complex(kind=RKIND_MAT)            ,intent(in)   :: field(:,:)
    integer                            ,intent(in)   :: n_rhs_component
    integer                            ,intent(in)   :: rhs_i_component(:)  
    character(len=*)                   ,intent(in)   :: format_model
    integer                            ,intent(in)   :: i_src_first, i_src_last
    integer                            ,intent(in)   :: tag_scale_rhs
    integer                            ,intent(in)   :: nrhs
    complex(kind=RKIND_MAT),allocatable,intent(inout):: Rval(:)
    type(t_error)                   ,intent(inout)   :: ctx_err
    !! -----------------------------------------------------------------
    logical             :: flag_boundary_involved = .false.
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case (trim(adjustl(ctx_dg%format_basis)))
      case('lagrange')
        call dg_source_init_field(ctx_mesh,ctx_dg,ctx_model_param_real, &
                                  ctx_model_param_imag,field,           &
                                  n_rhs_component,rhs_i_component,      &
                                  format_model,i_src_first,             &
                                  i_src_last,tag_scale_rhs,ctx_err)
        
        call dg_rhs_create_lagrange_dense(ctx_paral,ctx_dg,ctx_mesh,    &
                                          nrhs,flag_boundary_involved,  &
                                          Rval,ctx_err)



      case default
        ctx_err%msg   = "** ERROR: Unrecognized basis function format "// &
                        "(need LAGRANGE) in [discretization_rhs] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine discretization_rhs_field_full

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate the righ-hand side for DG in the case where we have
  !> a full RHS based upon an input wavefield for backward problem.
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] ctx_dg         : type DG
  !> @param[inout] ctx_mesh       : type mesh
  !> @param[inout] ctx_model_param: type model parameter
  !> @param[in]    field          : field array used for RHS
  !> @param[in]    n_rhs_component: number of component concerned
  !> @param[in]    rhs_i_component: list of rhs component 
  !> @param[in]    format_model   : format of the model
  !> @param[in]    i_src_first    : first source
  !> @param[in]    i_src_last     : last source 
  !> @param[inout] tag_scale_rhs  : rhs scaling
  !> @param[inout] icol           : number of RHS
  !> @param[inout] Rval           : RHS full
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine discretization_rhs_backward_field_full(ctx_paral,ctx_dg,   &
                                           ctx_mesh,                    &
                                           ctx_model_param_real,        &
                                           ctx_model_param_imag,field,  &
                                           n_rhs_component,             &
                                           rhs_i_component,             &
                                           format_model,                &
                                           i_src_first,i_src_last,      &
                                           tag_scale_rhs,nrhs,Rval,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_domain)                     ,intent(in)   :: ctx_mesh
    type(t_discretization)             ,intent(inout):: ctx_dg
    type(t_model_param)                ,intent(in)   :: ctx_model_param_real
    type(t_model_param)                ,intent(in)   :: ctx_model_param_imag
    complex(kind=RKIND_MAT)            ,intent(in)   :: field(:,:)
    integer                            ,intent(in)   :: n_rhs_component
    integer                            ,intent(in)   :: rhs_i_component(:)  
    character(len=*)                   ,intent(in)   :: format_model
    integer                            ,intent(in)   :: i_src_first, i_src_last
    integer                            ,intent(in)   :: tag_scale_rhs
    integer                            ,intent(in)   :: nrhs
    complex(kind=RKIND_MAT),allocatable,intent(inout):: Rval(:)
    type(t_error)                   ,intent(inout)   :: ctx_err
    !! -----------------------------------------------------------------
    logical             :: flag_boundary_involved = .false.
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case (trim(adjustl(ctx_dg%format_basis)))
      case('lagrange')
        call dg_source_backward_init_field(ctx_mesh,ctx_dg,             &
                                  ctx_model_param_real,                 &
                                  ctx_model_param_imag,field,           &
                                  n_rhs_component,rhs_i_component,      &
                                  format_model,i_src_first,             &
                                  i_src_last,tag_scale_rhs,ctx_err)
        
        call dg_rhs_backward_create_lagrange_dense(ctx_paral,ctx_dg,     &
                                    ctx_mesh,nrhs,flag_boundary_involved,&
                                    Rval,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Unrecognized basis function format "// &
                        "(need LAGRANGE) in [discretization_rhs_backward] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine discretization_rhs_backward_field_full

end module m_discretization_rhs
