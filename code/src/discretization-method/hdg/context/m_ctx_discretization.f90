!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_discretization.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the type for discontinuous Galerkin methods
!> Some specificities are implemented depending on the choice of method:
!>   - Internal Penalty Discontinuous Galerkin (IPDG)
!>   - Hybridizable     Discontinuous Galerkin (HDG)
!> This context only addresses HDG discreitzation.
!> Note that:
!> Some array are saved here to avoid their recomputation during the
!> processes (particularly important during inversion as it will be
!> repeated many times).
! ----------------------------
!> The main types are defined here:
!>  (o) type t_dg_cartmap: provides infos for a cartesian map associated
!>                         with a mesh. It may be important for I/O when
!>                         saving solutions
!>  (o) type t_dg_rcv    : provides infos for ONE receivers: we assume
!>                         only one cell is concerned, we save the
!>                         dof weights, etc...
!>  (o) type t_dg_src    : provides infos for ONE source.
!>  (o) type t_hdg       : provides sepecific infos of HDG
!>  (o) type t_discretization: contains all infos for the discretization
!>
!------------------------------------------------------------------------------
module m_ctx_discretization

  use m_raise_error,      only : t_error, raise_error
  use m_array_types,      only : t_array2d_int, t_array2d_real,         &
                                 t_array1d_int, t_array1d_complex,      &
                                 t_array1d_real,t_array3d_real,         &
                                 t_array2d_complex,                     &
                                 t_array2d_complex_kindmat,             &
                                 t_array4d_real, array_deallocate,array_allocate
  use m_define_precision, only : IKIND_MAT,RKIND_MAT,IKIND_MESH,RKIND_POL

  implicit none

  private
  public :: t_discretization, discretization_clean_context
  public :: discretization_clean_context_src
  public :: discretization_clean_context_rcv
  public :: discretization_reset_context_io
  public :: discretization_clean_context_io,dg_symmat_reset_context
  public :: dg_source_copy_context,dg_reset_face_param
  public :: t_dg_source,dg_source_clean_context

  !! -------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_dg_cartmap contains the infos on a structured grid related
  !>      to the mesh. It is used for visualization when the results
  !>      are projected onto a cartesian-grid representation.
  !
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  type t_dg_cartmap
    !!  memory
    integer(kind=8)                       :: mem_loc, mem_gb
    !!  Number of local and global node
    integer                               :: nx_loc,ny_loc,nz_loc
    integer                               :: nx_gb, ny_gb, nz_gb
    !! steps
    real(kind=8)                          :: dx,dy,dz
    !!  geometrical origins
    real(kind=8)                          :: ox,oy,oz
    !! index of the first and last node on the subgrid
    integer                               :: ixmin_loc,ixmax_loc
    integer                               :: iymin_loc,iymax_loc
    integer                               :: izmin_loc,izmax_loc
    !! total number of nodes that are concerned
    integer                               :: ncell_loc
    !! for all nodes the corresponding cell: 3D array of size (nx,ny,nz)
    integer(kind=IKIND_MESH),allocatable  :: i_cell(:,:,:)
    !! for all cells: the weight
    type(t_array1d_real),    allocatable  :: weight(:,:,:)
    !! logical to indicate if the cells associated with each
    !! point of the cartesian grid already solved. It has to
    !! be done only once if we do not change the mesh
    logical                               :: flag_cell_found=.false.
  end type t_dg_cartmap

  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_dg_rcv contains the infos on ONE receivers, e.g., the cell(s)
  !> in which they belong, if the current subdomain is affected, weights.
  !> receivers are assumed to affect only ONE cell
  !
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  type t_dg_rcv

    !!  we assume one cell per receivers, otherwise, we have
    !!  to follow the type t_dg_src
    integer(kind=IKIND_MESH)              :: icell_loc
    !!  Weight for the unique cells => weight(ndof)
    !!  depends on the order of the current cell
    real   (kind=RKIND_POL), allocatable  :: weight(:)
    !!  number of dof, face and vol
    integer                               :: ndof_vol
    integer                               :: ndof_face
    !! HDG infos for the receivers, only required in the
    !! context of inversion...
    complex(kind=RKIND_POL), allocatable  :: hdg_R(:)
  end type t_dg_rcv
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_dg_source contains the infos on ONE source, e.g., the cell(s)
  !> in which they belong, if the current subdomain is affected, weights, ...
  !
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  type t_dg_source

    !! The type of source, according to the acquisition 
    !! (e.g., Dirac, Planewave, etc.)
    integer                               :: src_type
    !! We also need the component on which they are applied
    !! for instance if need a directional gradient in the 
    !! source function.
    integer                               :: ncomponent_src
    character(len=64),allocatable         :: src_component(:)

    !!  Number of local cells concerned
    integer                               :: ncell_loc
    !!  Cell index that are concerned
    integer(kind=IKIND_MESH),allocatable  :: icell_loc(:)
    !!  indicates if boundary source or not, and on which face index
    !!  of the corresponding simplex
    logical                 ,allocatable  :: flag_on_bdry(:)
    !!  Weight for each of the cells (multi cells only if simultaneous sources)
    !!  => weight(icomp,icell)%array(ndof)
    !!  depends on the order of the current cell, complex because 
    !!  of planewave sources
    type(t_array1d_complex), allocatable  :: weight(:,:)
    !!  Weight for the derivative of the basis functions: possible
    !!  in the case where we use a gradient or divergence. On each 
    !!  of the cells => weight_dbasis(icomp,icell)%array(dimension,ndof)
    !!  depends on the order of the current cell, complex because 
    !!  of planewave sources
    type(t_array2d_complex), allocatable  :: weight_dbasis(:,:)
    
    !! how many volumic dof for this cell, note that ndof_vol=0
    !! if the source is on the boundary.
    !! on the other hand, the ndof_face is always > 0 if we have a rhs
    integer                , allocatable  :: ndof_vol (:)
    integer                , allocatable  :: ndof_face(:)
    !! for boundary source, the face concerned 
    integer,                 allocatable  :: iface_index(:) 
    !! HDG infos for the sources, one per source
    type(t_array1d_complex), allocatable  :: hdg_S(:)
    !! receivers associated with current source
    integer                               :: nrcv
    type(t_dg_rcv),          allocatable  :: rcv(:)

  end type t_dg_source

  !! -------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_dg_symmat contains the infos in order to make the matrix
  !> symmetric, even when we have a Dirichlet boundary conditions that
  !> breaks the symmetry. It gives the infos in order to adapt the 
  !> right-hand side to maintain the symmetry.
  !
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  type t_dg_symmat

    !! every cell gives the number of lines/columns that are affected.
    !! if it is 0, it means that this cell does not influence the 
    !! symmetry of the matrix
    integer,   allocatable  :: cell_nval(:)
    
    !! each cell has a possibly allocated array, for the line, the 
    !! column and the values. Those three arrays are of size 
    !! cell_nval(i_cell)
    type(t_array1d_int),       allocatable  :: c_line(:)
    type(t_array1d_int),       allocatable  :: c_col (:)
    type(t_array1d_complex),   allocatable  :: c_val (:)
    
    !! in the case of a function applied on the boundary, e.g., with 
    !! planewaves, we have to save the entire local block to replace
    !! it with the identity later on.
    !! the size is the number of (faces, components), and then in each 
    !! we have a squared matrix of size the number of dof on the face.
    !!                                      (i_face, i_component)
    type(t_array2d_complex),   allocatable  :: f_matbc(:,:)
    !! indicates if the linear system has to be solved, this is not 
    !! necessary if the matrix is not symmetric anyway
    logical                                 :: flag_to_symmetrize
  end type t_dg_symmat
  
  
  !! -------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_hdg contains specific matrices for HDG method
  !
  !! -------------------------------------------------------------------
  type t_hdg

    !!  A^-1, size (ncells)%array(ndof_cell,ndof_cell)
    type(t_array2d_complex_kindmat),allocatable :: Ainv(:)
    !! complex(kind=RKIND_MAT),allocatable   :: Ainv(:,:,:)
    !!  Q = A^-1 * C
    type(t_array2d_complex_kindmat),allocatable :: Q(:)
    !! complex(kind=RKIND_MAT),allocatable   :: Q(:,:,:)
    !!  X = B * A^-1
    type(t_array2d_complex_kindmat),allocatable :: X(:)
    !! complex(kind=RKIND_MAT),allocatable   :: X(:,:,:)

    !!  W = adjoint(C)*adjoint(A)^-1  (FWI gradient only)
    type(t_array2d_complex_kindmat),allocatable :: W(:)
    !!  D = adjoint(A^-1)*adjoint(B)  (FWI gradient only)
    type(t_array2d_complex_kindmat),allocatable :: D(:)

    !! for all face order, we create a indexing array to have the local
    !! dof. the number of possibilities depends on the dimension of the
    !! face and the geometry of elements.
    !!   - example 1: for 2D triangle mesh; the interface is a segment,
    !!                there are two possible combinations to ensure that
    !!                the normal of each triangle that share a segment
    !!                is outward: at order 2, we have 3 dof per face (line)
    !!                1          3          2   outward
    !!                x----------x----------x  INTERFACE
    !!                2          3          1   inward
    !!
    !!   - example 2: for 3D tetrahedral mesh; the interface is a triangle,
    !!                there are four possible combinations to ensure that
    !!                the outward normal on both side:
    !!                - we fixe one of the two neighbors  (1 case)
    !!                - for the other to have outward normal, one of the
    !!                  three line of the interface (i.e. triangle) must
    !!                  be reverted; i.e. 3 more cases.
    !!                There is a total of 4 cases.
    !! > ORDER1 >>
    !!  3               2            3            1
    !!  | \             | \          | \          | \
    !!  |  \            |  \         |  \         |  \
    !!  1---2           1---3        2---1        3---2
    !!  main         invert[2-3]  invert[1-2]  invert[1-3]
    !!
    !! > ORDER3 >>
    !!  3                 2               3               1
    !!  | \               | \             | \             | \
    !!  8   7             5   6           7   8           9   4
    !!  |     \           |     \         |     \         |     \
    !!  9  10   6         4  10   7       6  10   9       8  10   5
    !!  |         \       |         \     |         \     |         \
    !!  1---4---5---2     1---9---8---3   2---5---4---1   3---7---6---2
    !!     main          invert[2-3]     invert[1-2]     invert[1-3]
    !!
    !! For higher order, careful with interior node ordering as well...
    !! -----------------------------------------------------------------
    !! array of size (n_interface_combi, n_different_order)
    type(t_array1d_int),  allocatable :: idof_face (:,:)

    !! number of possible combination for interface
    !! between element (i.e. rotation of face)
    integer                           :: n_interface_combi

  end type t_hdg
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_dg contains info on the DG method:
  !> General infos are stored and specificities for IPDG or HDG
  !
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  type t_discretization

    !! source infos
    type(t_dg_source),allocatable :: src(:)
    !! Specific hdg
    type(t_hdg)                   :: hdg
    !! possibility to use a constant penalization for DG method
    real                          :: penalization

    !! -----------------------------------------------------------------
    !! General, shared, informations >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    character(len=16)        :: format_cell
    character(len=16)        :: format_basis
    integer(kind=8)          :: memory_loc
    !! dimension of the problem and solution
    integer                  :: dim_domain
    integer                  :: dim_sol !! dimension of the solution
    integer                  :: dim_var !! dimension of the matrix variable
    !! global and local number of dof, for the matrix and the solution
    !! (these are different in HDG)
    integer(kind=IKIND_MAT)  :: n_dofgb_persol , n_dofloc_persol
    integer(kind=IKIND_MAT)  :: n_dofgb_pervar , n_dofloc_pervar

    !! current min/max wavelength
    real(kind=8)             :: wavelength_min
    real(kind=8)             :: wavelength_max
    !! name of the numerical traces and of the volume solutions 
    !! for the current PDE.
    character(len=3)  ,allocatable :: name_sol(:)
    character(len=3)  ,allocatable :: name_var(:)
    !! convert the index of the trace to the volume solution
    integer,           allocatable :: ind_var_to_sol(:)

    !! the array contains the GLOBAL index of the first dof
    !! for the LOCAL cell,
    !! for solution   :: array of size (n_cell_loc)
    !! for matrix var :: array of size n_cell_loc or n_face_loc for ipdg/hdg
    !! global (for the matrix creation)
    !! and
    !! local  (for the distributed solution)
    integer(kind=IKIND_MAT), allocatable :: offsetdof_sol_gb (:)
    integer(kind=IKIND_MAT), allocatable :: offsetdof_mat_gb (:)
    integer(kind=IKIND_MAT), allocatable :: offsetdof_sol_loc(:)
    integer(kind=IKIND_MAT), allocatable :: offsetdof_mat_loc(:)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! information on the geometry
    !! normal on every face of every cell (dim,n_face_per_cell,n_cell)
    real(kind=8),            allocatable :: normal(:,:,:)
    !! determinant of the jacobian (n_cell)
    real(kind=8),            allocatable :: det_jac(:)
    !! inverse of the jacobian (dim,dim,n_cell) and jacobian(dim,dim,n_cell)
    real(kind=8),            allocatable :: inv_jac(:,:,:)
    real(kind=8),            allocatable :: jac(:,:,:)
    !! determinant of the jacobian on the face (n_face_per_cell,n_cell)
    real(kind=8),            allocatable :: det_jac_face(:,:)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! mpi partitioning infos
    !! ndof  for variables and nfaces per procs: size(n_procs)
    integer,                 allocatable :: mpi_ndofvar_count (:)
    integer,                 allocatable :: mpi_nface_count   (:)
    !! index of dof (ONLY MASTER)
    !! for HDG ==> mpi_dofvar_list(i_proc)%(i_face,1) = index_dof_global
    !!             mpi_dofvar_list(i_proc)%(i_face,2) = ndof for this face
    type(t_array2d_int),     allocatable :: mpi_dofvar_list   (:)
    !! -----------------------------------------------------------------


    !! -----------------------------------------------------------------
    !! the matrices on the reference element are stored here
    !! not all will be allocated, it shall depends on the actual
    !! equation to solve, and order of derivative needed
    !! how many different order are involved order
    !! list of involved order
    integer                  :: n_different_order
    integer                  :: order_max_gb
    integer                  :: order_min_gb
    !! convert order to local index in matrix order array
    !! e.g. if there is only one order, say 2,
    !! then order_list_index(2) = 1
    !! so we will work with mass_matrix_vol(1)% for order 2 mass matrices
    !! array from 0 to order_max
    integer,allocatable      :: order_to_index(:)
    !! this is the opposite, from index i it gives you the actual order
    integer,allocatable      :: index_to_order(:)

    !! ndof vol and on face for all order (size n_diff_order)
    integer,allocatable      :: n_dof_per_order     (:)
    integer,allocatable      :: n_dof_face_per_order(:)
    !! the map from the face degrees of freedom indices to the volume
    !! degrees of freedom indices, we have
    !!      face_map(i_order)%array(dof_face,face) = dof_vol
    type(t_array2d_int),  allocatable :: face_map(:)

    !! These are the matrices on the reference element, we have 
    !! - mass matrix face \phi_i \phi_j
    !! - mass matrix vol  \phi_i \phi_j
    !! - stiffness   vol  \partial(\phi_i) \phi_j
    !! - stiffness   vol  \phi_i \partial(\phi_j)
    !! - stiffness   vol  \partial(\phi_i) \partial(\phi_j)
    !! REMARK ==> only created for piecewise-constant model (i.e.
    !!            it gets out of the integral) AND for some equation
    !!            (i.e. the one without infos in the integral)
    !! indicates if the reference matrix are already saved or not
    logical                           :: flag_refmatrix
    !! the mass matrix on the reference element, for
    !! which corresponds to \integeral \phi_i \phi_j for all dof and order
    !! vol_matrix_mass(i_order)%array(dof,dof)
    type(t_array2d_real), allocatable :: vol_matrix_mass(:)
    !! this is the stiffness matrix where phi_i may be derived or not:
    !!  array(i,j,k) = \integral_{K} \partial_{xk} \phi_i . \phi_j
    !!  array(i,j,k) = \integral_{K} \phi_i . \partial_{xk} \phi_j
    !! type(t_array3d_real), allocatable :: vol_matrix_phi_dphi  (:)
    type(t_array3d_real), allocatable :: vol_matrix_dphi_phi(:)
    !!  array(k,l,i,j) = \integral_{K} \partial_{xk} \phi_i \partial_{xl} \phi_j
    !! unused in HDG
    !! type(t_array4d_real), allocatable :: vol_matrix_dphi_dphi (:)

    !! face_matrix_mass(i_order_local,i_order_neigh)%array(i,j,face) =
    !!        \integral_{face} \phi_i \phi_j
    type(t_array3d_real), allocatable :: face_matrix_mass(:,:)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! correspondance between the DG mesh and a cartesian grid
    type(t_dg_cartmap)      :: cartesian_map
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! In the case where we use quadrature points to evaluate the 
    !! integrals, we save, once and for all, the weights and points
    !! of the Gauss-Lobatto quadrature 
    logical                           :: flag_eval_int_quadrature
    !! the points, array of size (dim_domain,npts)
    integer                           :: quadGL_npts
    real(kind=RKIND_POL), allocatable :: quadGL_pts   (:,:)
    real(kind=RKIND_POL), allocatable :: quadGL_weight(:)
    !! the points for the face: 1 dimension less 
    integer                           :: quadGL_face_npts
    real(kind=RKIND_POL), allocatable :: quadGL_face_pts(:,:)
    real(kind=RKIND_POL), allocatable :: quadGL_face_weight(:)
    !! the formulas used for quadrature depending on the dimension
    character(len=64)                 :: quad_formula_volumic
    character(len=64)                 :: quad_formula_face

    !! for every order, we compute the value of ------------------------
    !!     \phi_i(x_k) \phi_j(x_k) weight_k
    !! d_x \phi_i(x_k) \phi_j(x_k) weight_k
    !! 
    !! quadGL_phi_phi_w(order) is of size  (n_dof, n_dof, n_pts)
    !!                                     ( i   ,   j  ,   k  )
    !!                         for phi_i(xk) * phi_j(xk)  w(xk)
    !! quadGL_dphi_phi_w(order) is of size (n_dof, n_dof, n_pts, n_dim)
    !!                                     ( i   ,   j  ,   k  ,   l )
    !!                           \patial_l phi_i(xk) * phi_j(xk)  w(xk)
    !! -----------------------------------------------------------------
    type(t_array3d_real), allocatable :: quadGL_phi_phi_w (:)
    type(t_array4d_real), allocatable :: quadGL_dphi_phi_w(:)
    
    !! in the case where the model is represented in a lagrangian
    !! basis polynomial, we may need the integral of \phi_i \phi_j \phi_k
    !! in the context of inversion only, once we derived with  respect to
    !! the model coefficient.
    !! quadGL_phi_phi__phi_w(order) size  (n_dof, n_dof, n_dof, n_pts)
    !!                                     ( i   ,   j  ,   l ,    k  )
    !! Remark that here, there is only one possible order for the model 
    !! representation, contrary the other basis i and j which may
    !! have a different order per celln therefore, the dimension of the
    !! array is the same as the other ones.
    type(t_array4d_real), allocatable :: quadGL_phi_phi_phi_w(:)

    !! for the face    
    !! 
    !! quadGL_face_phi_phi_w(order1,order2) is of size (n_dof, n_dof, n_pts, nface)
    !!                                                 ( i   ,   j  ,   k  , l   )
    !!           for \int_{face_l}  phi_i^{o1}(xk) * phi_j^{o2}(xk)  w(xk) 
    type(t_array4d_real), allocatable :: quadGL_face_phi_phi_w (:,:)
    
    !! this is used in the case of a Dirichlet boundary condition where
    !! we have one basis function and the Dirichlet function to apply
    !! quadGL_face_phi_phi_w(order1) is of size (n_dof, n_pts, nface)
    !!                                          ( i   ,   k  , l   )
    !!           for \int_{face_l}  phi_i^{o1}(xk)  w(xk) 
    type(t_array3d_real), allocatable :: quadGL_face_phi_w(:)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! In the case we have a PML, each cell will have the 
    !! specific coefficients, otherwize, those arrays are filled with 1.0
    !! for the cell, we also need the ghost to obtain the face coeff.
    !!                       size (dim_domain, n_cell_loc + n_ghost_loc)
    complex(kind=8),allocatable       :: pml_cell_coeff(:,:)
    !!                              size (dim_domain, n_face_loc)
    complex(kind=8),allocatable       :: pml_face_coeff(:,:)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! this is used to make the matrix symmetric, even with Dirichlet
    !! boundary conditions.
    type(t_dg_symmat)                  :: ctx_symmetrize_mat
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! in the case we have a planewave source, we need 
    !! to save the velocity on the faces, size(nface),
    !! there can be multiple wave speeds depending on the equations.
    type(t_array1d_complex),allocatable:: face_velocity(:)
    !! -----------------------------------------------------------------


  end type t_discretization

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> copy DG source information information
  !
  !> @param[inout] ctx_dg_src_in   : source DG context
  !> @param[inout] ctx_dg_src_out  : source DG context
  !----------------------------------------------------------------------------
  subroutine dg_source_copy_context(ctx_dg_src_in,ctx_dg_src_out,ctx_err)
    type(t_dg_source)       ,intent(in)     :: ctx_dg_src_in
    type(t_dg_source)       ,intent(inout)  :: ctx_dg_src_out
    type(t_error)           ,intent(inout)  :: ctx_err
    integer :: k,l,n,n1,n2,n3,n4

    ctx_dg_src_out%src_type       = ctx_dg_src_in%src_type
    ctx_dg_src_out%ncomponent_src = ctx_dg_src_in%ncomponent_src
    ctx_dg_src_out%ncell_loc      = ctx_dg_src_in%ncell_loc
    ctx_dg_src_out%nrcv           = ctx_dg_src_in%nrcv

    if(allocated(ctx_dg_src_in%src_component)) then
      n=size(ctx_dg_src_in%src_component)
      allocate(ctx_dg_src_out%src_component(n))
      ctx_dg_src_out%src_component(:) = ctx_dg_src_in%src_component(:)
    end if
    if(allocated(ctx_dg_src_in%icell_loc    )) then
      n=size(ctx_dg_src_in%icell_loc    )
      allocate(ctx_dg_src_out%icell_loc    (n))
      ctx_dg_src_out%icell_loc    (:) = ctx_dg_src_in%icell_loc    (:)
    end if
    if(allocated(ctx_dg_src_in%flag_on_bdry )) then
      n=size(ctx_dg_src_in%flag_on_bdry )
      allocate(ctx_dg_src_out%flag_on_bdry (n))
      ctx_dg_src_out%flag_on_bdry (:) = ctx_dg_src_in%flag_on_bdry (:)
    end if
    if(allocated(ctx_dg_src_in%ndof_vol     )) then
      n=size(ctx_dg_src_in%ndof_vol     )
      allocate(ctx_dg_src_out%ndof_vol     (n))
      ctx_dg_src_out%ndof_vol     (:) = ctx_dg_src_in%ndof_vol     (:)
    end if
    if(allocated(ctx_dg_src_in%ndof_face    )) then
      n=size(ctx_dg_src_in%ndof_face    )
      allocate(ctx_dg_src_out%ndof_face    (n))
      ctx_dg_src_out%ndof_face    (:) = ctx_dg_src_in%ndof_face    (:)
    end if
    if(allocated(ctx_dg_src_in%iface_index  )) then
      n=size(ctx_dg_src_in%iface_index  )
      allocate(ctx_dg_src_out%iface_index  (n))
      ctx_dg_src_out%iface_index  (:) = ctx_dg_src_in%iface_index  (:)
    end if
    
    if(allocated(ctx_dg_src_in%hdg_S)) then
      n=size(ctx_dg_src_in%hdg_S)
      allocate(ctx_dg_src_out%hdg_S(n))
      do k=1,n
        if(allocated(ctx_dg_src_in%hdg_S(k)%array)) then
          n1 = size(ctx_dg_src_in%hdg_S(k)%array)
          call array_allocate(ctx_dg_src_out%hdg_S(k),n1,ctx_err)
          ctx_dg_src_out%hdg_S(k)%array = ctx_dg_src_in%hdg_S(k)%array
        end if
      end do
    end if
    
    if(allocated(ctx_dg_src_in%weight)) then
      n1=size(ctx_dg_src_in%weight,1)
      n2=size(ctx_dg_src_in%weight,2)
      allocate(ctx_dg_src_out%weight(n1,n2))
      do k=1,n1 ; do l=1,n2
        if(allocated(ctx_dg_src_in%weight(k,l)%array)) then
          n  = size(ctx_dg_src_in%weight(k,l)%array)
          call array_allocate(ctx_dg_src_out%weight(k,l),n,ctx_err)
          ctx_dg_src_out%weight(k,l)%array = ctx_dg_src_in%weight(k,l)%array
        end if
      end do ; end do
    end if

    if(allocated(ctx_dg_src_in%weight_dbasis)) then
      n1=size(ctx_dg_src_in%weight_dbasis,1)
      n2=size(ctx_dg_src_in%weight_dbasis,2)
      allocate(ctx_dg_src_out%weight_dbasis(n1,n2))
      do k=1,n1 ; do l=1,n2
        if(allocated(ctx_dg_src_in%weight_dbasis(k,l)%array)) then
          n3  = size(ctx_dg_src_in%weight_dbasis(k,l)%array,1)
          n4  = size(ctx_dg_src_in%weight_dbasis(k,l)%array,2)
          call array_allocate(ctx_dg_src_out%weight_dbasis(k,l),n3,n4,ctx_err)
          ctx_dg_src_out%weight_dbasis(k,l)%array =                     &
                                    ctx_dg_src_in%weight_dbasis(k,l)%array
        end if
      end do ; end do
    end if
    !! we do not copy the receivers.
    !! type(t_dg_rcv),          allocatable  :: rcv          (:)
    if(allocated(ctx_dg_src_in%rcv)) then
      ctx_err%msg   = "** ERROR: We cannot copy the receiver " //       &
                      "array [dg_src_copy] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    return
  end subroutine dg_source_copy_context


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean DG information
  !
  !> @param[inout] ctx_dg    : DG context
  !
  !----------------------------------------------------------------------------
  subroutine discretization_clean_context(ctx_dg)
    type(t_discretization)       ,intent(inout)  :: ctx_dg
    integer :: k, l


    call dg_symmat_clean_context(ctx_dg%ctx_symmetrize_mat)

    if(allocated(ctx_dg%name_sol)) deallocate(ctx_dg%name_sol)
    if(allocated(ctx_dg%name_var)) deallocate(ctx_dg%name_var)
    if(allocated(ctx_dg%ind_var_to_sol)) deallocate(ctx_dg%ind_var_to_sol)

    if(allocated(ctx_dg%face_map)) then
      do k = 1, ctx_dg%n_different_order
        call array_deallocate(ctx_dg%face_map(k))
      end do
      deallocate(ctx_dg%face_map)
    end if
    if(allocated(ctx_dg%vol_matrix_mass)) then
      do k = 1, ctx_dg%n_different_order
        call array_deallocate(ctx_dg%vol_matrix_mass(k))
      end do
      deallocate(ctx_dg%vol_matrix_mass)
    end if
    !! if(allocated(ctx_dg%vol_matrix_phi_dphi)) then
    !!   do k = 1, ctx_dg%n_different_order
    !!     call array_deallocate(ctx_dg%vol_matrix_phi_dphi(k))
    !!   end do
    !!   deallocate(ctx_dg%vol_matrix_phi_dphi)
    !! end if
    if(allocated(ctx_dg%vol_matrix_dphi_phi)) then
      do k = 1, ctx_dg%n_different_order
        call array_deallocate(ctx_dg%vol_matrix_dphi_phi(k))
      end do
      deallocate(ctx_dg%vol_matrix_dphi_phi)
    end if
    !! unused in HDG
    !! if(allocated(ctx_dg%vol_matrix_dphi_dphi)) then
    !!   do k = 1, ctx_dg%n_different_order
    !!     call array_deallocate(ctx_dg%vol_matrix_dphi_dphi(k))
    !!   end do
    !!   deallocate(ctx_dg%vol_matrix_dphi_dphi)
    !! end if
    if(allocated(ctx_dg%face_matrix_mass)) then
      do k = 1, ctx_dg%n_different_order
        do l = 1, ctx_dg%n_different_order
          call array_deallocate(ctx_dg%face_matrix_mass(k,l))
        end do
      end do
      deallocate(ctx_dg%face_matrix_mass)
    end if

    if(allocated(ctx_dg%mpi_ndofvar_count))    deallocate(ctx_dg%mpi_ndofvar_count)
    if(allocated(ctx_dg%mpi_nface_count  ))    deallocate(ctx_dg%mpi_nface_count  )
    if(allocated(ctx_dg%mpi_dofvar_list)) then
      do k = 1, size(ctx_dg%mpi_dofvar_list)
        call array_deallocate(ctx_dg%mpi_dofvar_list(k))
      end do
      deallocate(ctx_dg%mpi_dofvar_list)
    end if

    if(allocated(ctx_dg%order_to_index      )) deallocate(ctx_dg%order_to_index      )
    if(allocated(ctx_dg%index_to_order      )) deallocate(ctx_dg%index_to_order      )
    if(allocated(ctx_dg%n_dof_per_order     )) deallocate(ctx_dg%n_dof_per_order     )
    if(allocated(ctx_dg%n_dof_face_per_order)) deallocate(ctx_dg%n_dof_face_per_order)
    if(allocated(ctx_dg%normal      ))      deallocate(ctx_dg%normal      )
    if(allocated(ctx_dg%det_jac_face))      deallocate(ctx_dg%det_jac_face)
    if(allocated(ctx_dg%inv_jac))           deallocate(ctx_dg%inv_jac)
    if(allocated(ctx_dg%jac))               deallocate(ctx_dg%jac)
    if(allocated(ctx_dg%det_jac))           deallocate(ctx_dg%det_jac)
    if(allocated(ctx_dg%offsetdof_sol_loc)) deallocate(ctx_dg%offsetdof_sol_loc)
    if(allocated(ctx_dg%offsetdof_mat_loc)) deallocate(ctx_dg%offsetdof_mat_loc)
    if(allocated(ctx_dg%offsetdof_sol_gb))  deallocate(ctx_dg%offsetdof_sol_gb)
    if(allocated(ctx_dg%offsetdof_mat_gb))  deallocate(ctx_dg%offsetdof_mat_gb)
    if(allocated(ctx_dg%pml_cell_coeff))    deallocate(ctx_dg%pml_cell_coeff)
    if(allocated(ctx_dg%pml_face_coeff))    deallocate(ctx_dg%pml_face_coeff)
    !! specific hdg
    if(allocated(ctx_dg%hdg%Ainv)) then
      do k = 1, size(ctx_dg%hdg%Ainv)
        call array_deallocate(ctx_dg%hdg%Ainv(k))
      end do
      deallocate(ctx_dg%hdg%Ainv)
    end if
    if(allocated(ctx_dg%hdg%Q)) then
      do k = 1, size(ctx_dg%hdg%Q)
        call array_deallocate(ctx_dg%hdg%Q(k))
      end do
      deallocate(ctx_dg%hdg%Q)
    end if
    if(allocated(ctx_dg%hdg%X)) then
      do k = 1, size(ctx_dg%hdg%X)
        call array_deallocate(ctx_dg%hdg%X(k))
      end do
      deallocate(ctx_dg%hdg%X)
    end if
    if(allocated(ctx_dg%hdg%W)) then
      do k = 1, size(ctx_dg%hdg%W)
        call array_deallocate(ctx_dg%hdg%W(k))
      end do
      deallocate(ctx_dg%hdg%W)
    end if
    if(allocated(ctx_dg%hdg%D)) then
      do k = 1, size(ctx_dg%hdg%D)
        call array_deallocate(ctx_dg%hdg%D(k))
      end do
      deallocate(ctx_dg%hdg%D)
    end if

    if(allocated(ctx_dg%hdg%idof_face)) then
      do k = 1, ctx_dg%n_different_order
        do l = 1, ctx_dg%hdg%n_interface_combi
          call array_deallocate(ctx_dg%hdg%idof_face (l,k))
        end do
      end do
      deallocate(ctx_dg%hdg%idof_face)
    end if
    if(allocated(ctx_dg%face_velocity)) then
      do k=1,size(ctx_dg%face_velocity)
        call array_deallocate(ctx_dg%face_velocity(k))
      enddo
      deallocate(ctx_dg%face_velocity)
    end if

    ctx_dg%flag_refmatrix   = .false.
    ctx_dg%hdg%n_interface_combi = 0
    ctx_dg%format_cell      =''
    ctx_dg%format_basis     =''
    ctx_dg%memory_loc       =0
    ctx_dg%dim_domain       =0
    ctx_dg%dim_sol          =0
    ctx_dg%dim_var          =0
    ctx_dg%n_dofloc_persol  =0
    ctx_dg%n_dofloc_pervar  =0
    ctx_dg%n_dofgb_persol   =0
    ctx_dg%n_dofgb_pervar   =0
    ctx_dg%n_different_order=0
    ctx_dg%order_max_gb     =0
    ctx_dg%order_min_gb     =0
    ctx_dg%penalization     =-1
    
    !! quadrature
    ctx_dg%flag_eval_int_quadrature = .false.
    ctx_dg%quad_formula_volumic = ''
    ctx_dg%quad_formula_face    = ''
    ctx_dg%quadGL_npts = 0
    if(allocated(ctx_dg%quadGL_pts   )) deallocate(ctx_dg%quadGL_pts   )
    if(allocated(ctx_dg%quadGL_weight)) deallocate(ctx_dg%quadGL_weight)
    if(allocated(ctx_dg%quadGL_phi_phi_w)) then
      do k = 1, ctx_dg%n_different_order
        call array_deallocate(ctx_dg%quadGL_phi_phi_w(k))
      end do
      deallocate(ctx_dg%quadGL_phi_phi_w)
    end if
    if(allocated(ctx_dg%quadGL_phi_phi_phi_w)) then
      do k = 1, ctx_dg%n_different_order
        call array_deallocate(ctx_dg%quadGL_phi_phi_phi_w(k))
      end do
      deallocate(ctx_dg%quadGL_phi_phi_phi_w)
    end if
    if(allocated(ctx_dg%quadGL_dphi_phi_w)) then
      do k = 1, ctx_dg%n_different_order
        call array_deallocate(ctx_dg%quadGL_dphi_phi_w(k))
      end do
      deallocate(ctx_dg%quadGL_dphi_phi_w)
    end if

    ctx_dg%quadGL_face_npts = 0
    if(allocated(ctx_dg%quadGL_face_pts   )) deallocate(ctx_dg%quadGL_face_pts   )
    if(allocated(ctx_dg%quadGL_face_weight)) deallocate(ctx_dg%quadGL_face_weight)
    if(allocated(ctx_dg%quadGL_face_phi_phi_w))  then
      do k = 1, ctx_dg%n_different_order
        do l = 1, ctx_dg%n_different_order
          call array_deallocate(ctx_dg%quadGL_face_phi_phi_w(k,l))
        end do
      end do
      deallocate(ctx_dg%quadGL_face_phi_phi_w)
    end if
    if(allocated(ctx_dg%quadGL_face_phi_w))  then
      do k = 1, ctx_dg%n_different_order
        call array_deallocate(ctx_dg%quadGL_face_phi_w(k))
      end do
      deallocate(ctx_dg%quadGL_face_phi_w)
    end if

    ctx_dg%n_different_order=0


    return
  end subroutine discretization_clean_context
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean DG information for IO cartesian map
  !
  !> @param[inout] ctx_dg    : DG context
  !
  !----------------------------------------------------------------------------
  subroutine discretization_clean_context_io(ctx_dg)
    type(t_discretization)       ,intent(inout)  :: ctx_dg
    integer :: k1,k2,k3

    if(allocated(ctx_dg%cartesian_map%i_cell)) then
      deallocate(ctx_dg%cartesian_map%i_cell)
    end if

    if(allocated(ctx_dg%cartesian_map%weight)) then
      do k1=1,ctx_dg%cartesian_map%nx_loc
        do k2=1,ctx_dg%cartesian_map%ny_loc
          do k3=1,ctx_dg%cartesian_map%nz_loc
            call array_deallocate(ctx_dg%cartesian_map%weight(k1,k2,k3))
          end do
        end do
      end do
      deallocate(ctx_dg%cartesian_map%weight)
    end if

    ctx_dg%cartesian_map%mem_loc  =0
    ctx_dg%cartesian_map%mem_gb   =0
    ctx_dg%cartesian_map%nx_loc   =0
    ctx_dg%cartesian_map%ny_loc   =0
    ctx_dg%cartesian_map%nz_loc   =0
    ctx_dg%cartesian_map%nx_gb    =0
    ctx_dg%cartesian_map%ny_gb    =0
    ctx_dg%cartesian_map%nz_gb    =0
    ctx_dg%cartesian_map%dx       =0.0
    ctx_dg%cartesian_map%dy       =0.0
    ctx_dg%cartesian_map%dz       =0.0
    ctx_dg%cartesian_map%ox       =0.0
    ctx_dg%cartesian_map%oy       =0.0
    ctx_dg%cartesian_map%oz       =0.0
    ctx_dg%cartesian_map%ixmin_loc=0
    ctx_dg%cartesian_map%ixmax_loc=0
    ctx_dg%cartesian_map%iymin_loc=0
    ctx_dg%cartesian_map%iymax_loc=0
    ctx_dg%cartesian_map%izmin_loc=0
    ctx_dg%cartesian_map%izmax_loc=0
    ctx_dg%cartesian_map%ncell_loc=0
    ctx_dg%cartesian_map%flag_cell_found=.false.

    return
  end subroutine discretization_clean_context_io
  
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> reset cartesian DG map: we only deallocate the weight 
  !> we do not need to recompute the cells corresponding to
  !> the nodes if we do not change the mesh but only the order.
  !
  !
  !> @param[inout] ctx_dg    : DG context
  !---------------------------------------------------------------------
  subroutine discretization_reset_context_io(ctx_dg)
    type(t_discretization)       ,intent(inout)  :: ctx_dg
    integer :: k1,k2,k3

    if(allocated(ctx_dg%cartesian_map%weight)) then
      do k1=1,ctx_dg%cartesian_map%nx_loc
        do k2=1,ctx_dg%cartesian_map%ny_loc
          do k3=1,ctx_dg%cartesian_map%nz_loc
            call array_deallocate(ctx_dg%cartesian_map%weight(k1,k2,k3))
          end do
        end do
      end do
    end if

    return
  end subroutine discretization_reset_context_io
  
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> reset information on the model parameters for the case
  !> these are involved in the RHS.
  !
  !
  !> @param[inout] ctx_dg    : DG context
  !---------------------------------------------------------------------
  subroutine dg_reset_face_param(ctx_dg)
    type(t_discretization) ,intent(inout)  :: ctx_dg
    integer :: k,n

    if(allocated(ctx_dg%face_velocity)) then
      n = size(ctx_dg%face_velocity)
      do k=1,n
        call array_deallocate(ctx_dg%face_velocity(k))
      end do
    end if

    return
  end subroutine dg_reset_face_param

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean DG information
  !
  !> @param[inout] ctx_dg    : DG context
  !----------------------------------------------------------------------------
  subroutine discretization_clean_context_src(ctx_dg,o_flag_keep_rcv)
    type(t_discretization)       ,intent(inout)  :: ctx_dg
    logical, optional            ,intent(in)     :: o_flag_keep_rcv
    !! local
    logical :: flag_keep_rcv
    integer :: k

    if(present(o_flag_keep_rcv)) then
      flag_keep_rcv = o_flag_keep_rcv
    else
      flag_keep_rcv = .false.
    end if
    
    if(allocated(ctx_dg%src)) then
      do k=1,size(ctx_dg%src)
        call dg_source_clean_context(ctx_dg%src(k),flag_keep_rcv)
      end do
      if(.not. flag_keep_rcv) deallocate(ctx_dg%src)
    end if
    return
  end subroutine discretization_clean_context_src

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean DG information
  !
  !> @param[inout] ctx_dg    : DG context
  !----------------------------------------------------------------------------
  subroutine discretization_clean_context_rcv(ctx_dg,o_flag_keep)
    type(t_discretization)       ,intent(inout)  :: ctx_dg
    logical, optional            ,intent(in)     :: o_flag_keep
    !! local
    logical :: flag_keep
    integer :: k,j

    flag_keep = .false.
    if(present(o_flag_keep)) flag_keep = o_flag_keep

    if(allocated(ctx_dg%src)) then
      do k=1,size(ctx_dg%src)
        ctx_dg%src(k)%nrcv = 0
        do j=1,size(ctx_dg%src(k)%rcv)
          ctx_dg%src(k)%rcv(j)%icell_loc = 0
          ctx_dg%src(k)%rcv(j)%ndof_vol  = 0
          ctx_dg%src(k)%rcv(j)%ndof_face = 0
          if(allocated(ctx_dg%src(k)%rcv(j)%weight)) deallocate(ctx_dg%src(k)%rcv(j)%weight)
          if(allocated(ctx_dg%src(k)%rcv(j)%hdg_R )) deallocate(ctx_dg%src(k)%rcv(j)%hdg_R )
        end do
        if(.not. flag_keep) deallocate(ctx_dg%src(k)%rcv)
      end do
    end if

    return

  end subroutine discretization_clean_context_rcv

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean sources DG information
  !
  !> @param[inout] ctx_dg    : DG context
  !
  !---------------------------------------------------------------------
  subroutine dg_source_clean_context(ctx_dg_source,flag_keep_rcv)
    type(t_dg_source)       ,intent(inout)  :: ctx_dg_source
    logical                 ,intent(in)     :: flag_keep_rcv
    integer :: k,l

    ctx_dg_source%ncell_loc=0

    if(.not. flag_keep_rcv) then
      if(allocated(ctx_dg_source%rcv)) then
        do k = 1,ctx_dg_source%nrcv
          if(allocated(ctx_dg_source%rcv(k)%weight)) &
            deallocate(ctx_dg_source%rcv(k)%weight)
          if(allocated(ctx_dg_source%rcv(k)%hdg_R))  &
            deallocate(ctx_dg_source%rcv(k)%hdg_R)
        enddo
        deallocate(ctx_dg_source%rcv)
      end if
    endif

    if(allocated(ctx_dg_source%src_component)) &
      deallocate(ctx_dg_source%src_component)
    if(allocated(ctx_dg_source%icell_loc))     &
      deallocate(ctx_dg_source%icell_loc)
    if(allocated(ctx_dg_source%flag_on_bdry))  &
      deallocate(ctx_dg_source%flag_on_bdry)
    if(allocated(ctx_dg_source%ndof_vol))      &
      deallocate(ctx_dg_source%ndof_vol)
    if(allocated(ctx_dg_source%ndof_face))     &
      deallocate(ctx_dg_source%ndof_face) 
    if(allocated(ctx_dg_source%iface_index))   &
      deallocate(ctx_dg_source%iface_index)
    if(allocated(ctx_dg_source%weight)) then
      do k=1,size(ctx_dg_source%weight,1)
      do l=1,size(ctx_dg_source%weight,2)
        call array_deallocate(ctx_dg_source%weight(k,l))
      enddo
      enddo
      deallocate(ctx_dg_source%weight)
    end if
    if(allocated(ctx_dg_source%weight_dbasis)) then
      do k=1,size(ctx_dg_source%weight_dbasis,1)
      do l=1,size(ctx_dg_source%weight_dbasis,2)
        call array_deallocate(ctx_dg_source%weight_dbasis(k,l))
      enddo
      enddo
      deallocate(ctx_dg_source%weight_dbasis)
    end if
    if(allocated(ctx_dg_source%hdg_S)) then
      do k=1,size(ctx_dg_source%hdg_S)
        call array_deallocate(ctx_dg_source%hdg_S(k))
      enddo
      deallocate(ctx_dg_source%hdg_S)
    end if

    return
  end subroutine dg_source_clean_context

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean sym mat DG information
  !
  !> @param[inout] ctx_sym_mat    : DG context
  !
  !---------------------------------------------------------------------
  subroutine dg_symmat_clean_context(ctx_sym_mat)
    
    implicit none

    type(t_dg_symmat)       ,intent(inout)  :: ctx_sym_mat
    integer :: k,n,k1,k2,n1,n2

    if(allocated(ctx_sym_mat%cell_nval)) deallocate(ctx_sym_mat%cell_nval)
    if(allocated(ctx_sym_mat%c_col)) then
      n = size(ctx_sym_mat%c_col)
      do k = 1, n
        call array_deallocate(ctx_sym_mat%c_col(k))
      end do
      deallocate(ctx_sym_mat%c_col)
    end if

    if(allocated(ctx_sym_mat%c_line)) then
      n = size(ctx_sym_mat%c_line)
      do k = 1, n
        call array_deallocate(ctx_sym_mat%c_line(k))
      end do
      deallocate(ctx_sym_mat%c_line)
    end if

    if(allocated(ctx_sym_mat%c_val)) then
      n = size(ctx_sym_mat%c_val)
      do k = 1, n
        call array_deallocate(ctx_sym_mat%c_val(k))
      end do
      deallocate(ctx_sym_mat%c_val)
    end if
    
    ctx_sym_mat%flag_to_symmetrize = .false.
    if(allocated(ctx_sym_mat%f_matbc)) then
      n1 = size(ctx_sym_mat%f_matbc,1)
      n2 = size(ctx_sym_mat%f_matbc,2)
      do k1 = 1,n1 ; do k2=1,n2
        call array_deallocate(ctx_sym_mat%f_matbc(k1,k2))
      end do ; end do
      deallocate(ctx_sym_mat%f_matbc)
    end if
    return
  end subroutine dg_symmat_clean_context

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> reset sym mat DG information: deallocate the sub-arrays only
  !
  !> @param[inout] ctx_sym_mat    : DG context
  !
  !---------------------------------------------------------------------
  subroutine dg_symmat_reset_context(ctx_sym_mat)
    
    implicit none

    type(t_dg_symmat)       ,intent(inout)  :: ctx_sym_mat
    integer :: k,k1,k2,n,n1,n2

    if(allocated(ctx_sym_mat%c_col)) then
      n = size(ctx_sym_mat%c_col)
      do k = 1, n
        call array_deallocate(ctx_sym_mat%c_col(k))
      end do
    end if
    if(allocated(ctx_sym_mat%c_line)) then
      n = size(ctx_sym_mat%c_line)
      do k = 1, n
        call array_deallocate(ctx_sym_mat%c_line(k))
      end do
    end if
    if(allocated(ctx_sym_mat%c_val)) then
      n = size(ctx_sym_mat%c_val)
      do k = 1, n
        call array_deallocate(ctx_sym_mat%c_val(k))
      end do
    end if

    if(allocated(ctx_sym_mat%f_matbc)) then
      n1 = size(ctx_sym_mat%f_matbc,1)
      n2 = size(ctx_sym_mat%f_matbc,2)
      do k1 = 1, n1; do k2 = 1, n2; 
        call array_deallocate(ctx_sym_mat%f_matbc(k1,k2))
      end do; end do
    end if
    return
  end subroutine dg_symmat_reset_context

end module m_ctx_discretization
