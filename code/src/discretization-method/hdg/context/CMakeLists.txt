add_library(ctx_discretization m_ctx_discretization.f90)
target_link_libraries(ctx_discretization hdg_polynomials raise_error)
install(TARGETS ctx_discretization
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
