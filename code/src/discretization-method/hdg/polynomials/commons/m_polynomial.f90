!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_polynomial.f90
!
!> @author
!> J. Diaz [Inria Magique 3D]
!
! DESCRIPTION:
!> @brief
!> the module is used to define the type and routines to 
!> handle polynomials functions.
!> It comes from J.Diaz body of work in software [HOU10NI]
!
!------------------------------------------------------------------------------
module m_polynomial

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: RKIND_POL
  !! -------------------------------------------------------------------
  implicit none
  
  private
  public  :: t_polynomial, t_polynomial_precomput    !! types
  public  :: polynomial_eval, polynomial_construct, polynomial_clean,     &
             polynomial_multiplication, polynomial_precomput_construct,   &
             polynomial_precomput_clean, polynomial_integrate,            &
             polynomial_dx,polynomial_dy,polynomial_dz, polynomial_trace, &
             polynomial_invert_coo, polynomial_copy, polynomial_eval_array
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !>             Structure for \f$P_k\f$ polynomials
  !> @brief 
  !>   A \f$P_k\f$ polynomial \f$P\f$ is represented by the 
  !>   structure p, where p\%dim
  !>   is the dimension of  \f$P\f$, p\%degree its degree (k), p\%size the
  !>   number of its t_coefficients and p\%coeff a vector containing all the
  !>   t_coefficients of \f$P\f$. The t_coefficients are stored in the following
  !>   way :
  !>
  !>  \li if p\%dim=0, then  \f$P=p\%coeff(1)\f$
  !>  \li if p\%dim=1, then  
  !>      \f$\displaystyle P(x)=\sum_{i=0}^{p\%degree}p\%coeff(i+1)x^i\f$
  !>  \li if p\%dim=2, then 
  !>      \f$\displaystyle P(x,y)=\sum_{i=0}^{p\%degree}
  !>      \sum_{j=0}^{i}p\%coeff(l_2(i,j))x^{i-j}y^j\f$, 
  !>      with \f$l_2(i,j)=i(i+1)/2+j+1\f$.
  !>      In other words, \f$\displaystyle P(x,y)=c_1+c_2x+c_3y+c_4x^2+c_5xy
  !>                         +c_6y^2+c_7x^3+c_8x^2y+c_9xy^2+c_{10}y^3+...\f$
  !>  \li if p\%dim=3, then \f$\displaystyle P(x,y,z)=\sum_{i=0}^{p\%degree}
  !>      \sum_{j=0}^{i}\sum_{k=0}^{j}p\%coeff(l_3(i,j,k))x^{i-j-k}y^{j-k}z^k\f$
  !>      with \f$l_3(i,j,k)={i(i+1)(i+2)/6}+j(j+1)/2+k+1\f$.
  !>      In other words,  \f$\displaystyle P(x,y,z)=c_1+c_2x+c_3y+c_4z+
  !>      c_5x^2+c_6xy+c_7xz+c_8y^2+c_9yz+c_{10}z^2+c_{11}x^3+c_{12}x^2y+
  !>      c_{13}x^2z+c_{14}xy^2+c_{15}xyz+c_{16}xz^2+c_{17}y^3+c_{18}y^2z+
  !>      c_{19}yz^2+c_{20}z^3+...\f$
  !! -------------------------------------------------------------------
  type t_polynomial
    integer                           :: dim_domain   !< the dimension(1D/2D/3D)
    integer                           :: degree       !< polynomial degree 
    integer                           :: size_coeff   !< n_coefficients 
    real(kind=RKIND_POL),allocatable  :: coeff_pol(:) !< polynomial coefficients
  end type t_polynomial


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !>             Coefficients for precomputing multiplications and 
  !>             integrals of \f$P_k\f$ polynomials.
  !>@brief 
  !>  \li if p\%dim=1,  we want to compute  \f$\displaystyle 
  !>         P_3(x)=P_1(x)P_2(x).\f$ 
  !>         Setting \f$coe(1)\%multi(i,j)=i+j-1,\f$ we have 
  !>         \f$\displaystyle P_3=\sum_k\sum_{coe(1)\%multi(i,j)=k}p_1\%coeff(i)
  !>         p_2\%coeff(j)x^k,\f$ so that \f$\displaystyle p_3\%coeff(k)=
  !>         \sum_{coe(1)\%multi(i,j)=k}p_1\%coeff(i)p_2\%coeff(j).\f$
  !>  \li if p\%dim=2,  we want to compute \f$\displaystyle P_3(x,y)=P_1(x
  !>         ,y)P_2(x,y)\f$. Setting \f$coe(2)\%multi(l_2(i_1,j_1),
  !>         l_2(i_2,j_2))
  !>         =l_2(i_1+i_2,j_1+j_2),\f$ we have  \f$\displaystyle p_3\%coeff(k)=
  !>         \sum_{coe(2)\%multi(i,j)=k}p_1\%coeff(i)p_2\%coeff(j).\f$
  !>  \li if p\%dim=3,  we want to compute \f$\displaystyle P_3(x,y,z)=P_1(x
  !>         ,y,z)P_2(x,y,z)\f$. Setting \f$coe(3)\%multi(l_2(i_1,j_1,m_1), 
  !>         l_2(i_2,j_2,m_2))
  !>         =l_2(i_1+i_2,j_1+j_2,m_1+m_2),\f$ we have  
  !>         \f$\displaystyle p_3\%coeff(k)=
  !>         \sum_{coe(3)\%multi(i,j)=k}p_1\%coeff(i)p_2\%coeff(j).\f$
  !> \li Hence, whatever the dimension, p3\%coeff can be computed by the 
  !>     following algorithm
  !>     <ul> <li>p3\%coeff=0
  !>     <li> do i=1,p1\%size
  !>     <ul><li> do j=1,p2\%size
  !>     <ul><li> p3\%coeff(coe(p3%dim)\%multi(i,j))=
  !>       p3\%coeff(coe(p3\%dim)\%multi(i,j))+p1\%coeff(i)p2\%coeff(j)
  !>     </ul><li>enddo</ul><li>enddo</ul>
  !> \li coe(1)\%integ(k) is the integral over the reference segment of 
  !>     the \f$k^{th}\f$ monomial : 
  !>     \f$coe(1)\%integ(k)=\int_0^1x^{k-1}dx=\frac 1 k \f$
  !> \li coe(2)\%integ(k) is the integral over the reference triangle of 
  !>     the \f$k^{th}\f$ monomial : \f$coe(2)\%integ(l_2(i,j))=
  !>     \int_0^1\int_0^{1-x}x^{i-1}y^{j-1}dxdy. \f$
  !> \li coe(3)\%integ(k) is the integral over the reference tetrahedron 
  !>     of the \f$k^{th}\f$ monomial : \f$coe(3)\%integ(l_3(i,j,m))=
  !>     \int_0^1\int_0^{1-x}\int_0^{1-x-y}x^{i-1}y^{j-1}z^{m-1}dxdydz. \f$
  !> \li Hence, whatever the dimension, the integral of $P$ can be 
  !>    computed by \f$\displaystyle \sum_{i=1}^{p\%size} 
  !>    coe(p\%dim)\%integ(i)p\%coeff(i)\f$
  !----------------------------------------------------------------------------
  type t_polynomial_precomput

    !! multi is computed in order to accelarate the product of 
    !! polynomials. Let K=multi(I,J), then P1\%coeff(I)*P2\%coeff(J) 
    !! is a part of P3\%coeff(K)
    integer             ,allocatable   :: multi(:,:)
    
    !! int_ref(k) is the integral over the reference element of the 
    !! \f$k^{th}\f$ monomial
    real(kind=RKIND_POL),allocatable   :: int_ref   (:)

  end type t_polynomial_precomput


  interface polynomial_eval
    module procedure polynomial_eval_interface
    module procedure polynomial_eval_1d
    module procedure polynomial_eval_2d
    module procedure polynomial_eval_3d
  end interface polynomial_eval

  interface polynomial_eval_array
    module procedure polynomial_eval_array_interface
    module procedure polynomial_eval_array_1d
    module procedure polynomial_eval_array_2d
    module procedure polynomial_eval_array_3d
  end interface polynomial_eval_array
  
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> copy a polynomial p_in to p_out
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_copy(p_in,p_out)
    implicit none
    type(t_polynomial) ,intent(in)  :: p_in 
    type(t_polynomial) ,intent(out) :: p_out

    p_out%dim_domain = p_in%dim_domain
    p_out%degree     = p_in%degree    
    p_out%size_coeff = p_in%size_coeff
    if(p_in%size_coeff > 0) then
      allocate(p_out%coeff_pol(p_in%size_coeff))
      p_out%coeff_pol = p_in%coeff_pol
    end if 
    
    return

  end subroutine polynomial_copy
  
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> construct a polynomial p1 (Pk_type) of dimension dim and of degree deg
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_construct(dim,deg,p1,ctx_err)
    implicit none 
    integer         ,intent(in)  :: deg  !< the degree
    integer         ,intent(in)  :: dim  !< the dimension
    type(t_polynomial) ,intent(out) :: p1   !< the new polynomial
    type(t_error)   ,intent(out) :: ctx_err
    
    ctx_err%ierr=0
    
    p1%degree    =deg
    p1%dim_domain=dim

    select case (dim)
    case(0)
       p1%size_coeff=1
    case(1) !! In dimension 1, the polynomial has deg+1 coeff
       p1%size_coeff=deg+1
    case(2)!! In dimension 2, the polynomial has (deg+2)*(deg+1)/2 coeff
       p1%size_coeff=(deg+2)*(deg+1)/2
    case(3)!! In dimension 3, the polynomial has (deg+3)*(deg+2)*(deg+1)/6 coeff
       p1%size_coeff=(deg+3)*(deg+2)*(deg+1)/6
    case default
      ctx_err%msg = "** ERROR: Unsupported dimension [m_polynom] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end select

    allocate(p1%coeff_pol(p1%size_coeff))
    return

  end subroutine polynomial_construct
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean a polynomial
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_clean(p1)
    implicit none 
    type(t_polynomial) ,intent(inout) :: p1 
    
    p1%degree    =0
    p1%dim_domain=0
    p1%size_coeff=0
    if (allocated(p1%coeff_pol)) deallocate(p1%coeff_pol)
    return

  end subroutine polynomial_clean
  !----------------------------------------------------------------------------

  !! ------------------------------------------------------------------->
  subroutine polynomial_eval_interface(p,x,val,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p      !< polynomial to evaluate
    real(kind=RKIND_POL),intent(in)          :: x(:)   !< nD  coordinates
    real(kind=RKIND_POL),intent(out)         :: val    !< output value 
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    
    ctx_err%ierr=0
    val=0.0
    
    if(size(x) .ne. p%dim_domain) then
      ctx_err%msg = "** ERROR: Incorrect dimension of x compared to the "// & 
                    " polynomial in [eval_poly_interface] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    
    select case(p%dim_domain) 

      case(1)
        call polynomial_eval_1d(p,x(1),val,ctx_err)

      case(2)
        call polynomial_eval_2d(p,x(1),x(2),val,ctx_err)

      case(3)
        call polynomial_eval_3d(p,x(1),x(2),x(3),val,ctx_err)

      case default
        ctx_err%msg = "** ERROR: Incorrect dimension [eval_poly_interface] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return

  end subroutine polynomial_eval_interface
  !! ------------------------------------------------------------------->
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Compute the value of a \f$P_k\f$ polynomial P at a given point \f$x_0
  !>  \f$ (in 1D), \f$(x_0,y_0)\f$ (in 2D) or \f$(x_0,y_0,z_0)\f$ (in 3D) 
  !>  \li if p\%dim=0, then  \f$P=p\%coeff(1)\f$
  !>  \li if p\%dim=1, then  \f$\displaystyle P(x_0)=\sum_{i=0}^{p\%degree}
  !>         p\%coeff(i+1)x_0^i\f$
  !>  \li if p\%dim=2, then  \f$\displaystyle P(x_0,y_0)=\sum_{i=0}^{p\%degree}
  !>         \sum_{j=0}^{i}p\%coeff(l_2(i,j))x_0^{i-j}y_0^j\f$.
  !>  \li if p\%dim=3, then  \f$\displaystyle P(x_0,y_0,z_0)=
  !>         \sum_{i=0}^{p\%degree}
  !>         \sum_{j=0}^{i}\sum_{k=0}^{j}p\%coeff(l_3(i,j,k))x_0^{i-j-k}
  !>         y_0^{j-k}z_0^k\f$. 
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_eval_3d(p,x0,y0,z0,val,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p   !< polynomial to evaluate
    real(kind=RKIND_POL),intent(in)          :: x0  !< 1st coordinates
    real(kind=RKIND_POL),intent(in)          :: y0  !< 2nd coordinates
    real(kind=RKIND_POL),intent(in)          :: z0  !< 3rd coordinates
    real(kind=RKIND_POL),intent(out)         :: val !< value of p at the point
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    integer :: I,K,L
    
    ctx_err%ierr=0
    val=0.0
    
    if(p%dim_domain .ne. 3) then
      ctx_err%msg = "** ERROR: Incorrect dimension [evalpoly_3d] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    do I=1,p%degree+1
       do K=1,I
          do L=1,K
             !!! we consider the ind1 element of F1
             val=val+p%coeff_pol((I+1)*I*(I-1)/6+K*(K-1)/2+L)*x0**(I-K) &
                    *y0**(K-L)*z0**(L-1)
          end do
       end do
    end do

    return
  end subroutine polynomial_eval_3d

  !! ------------------------------------------------------------------->
  subroutine polynomial_eval_array_interface(p,x,val,nval,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p      !< polynomial to evaluate
    real(kind=RKIND_POL),intent(in)          :: x(:,:) !< nD  coordinates
    real(kind=RKIND_POL),intent(out)         :: val(:) !< value of p at the point
    integer             ,intent(in)          :: nval
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    
    ctx_err%ierr=0
    val=0.0

    if(size(x,1) .ne. p%dim_domain) then
      ctx_err%msg = "** ERROR: Incorrect dimension of x compared to the "// & 
                    " polynomial in [eval_poly_interface_array] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    
    select case(p%dim_domain) 

      case(1)
        call polynomial_eval_array_1d(p,x(1,:),val,nval,ctx_err)

      case(2)
        call polynomial_eval_array_2d(p,x(1,:),x(2,:),val,nval,ctx_err)

      case(3)
        call polynomial_eval_array_3d(p,x(1,:),x(2,:),x(3,:),val,nval,ctx_err)

      case default
        ctx_err%msg = "** ERROR: Incorrect dimension [eval_poly_interface_array] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return

  end subroutine polynomial_eval_array_interface
  !! ------------------------------------------------------------------->

  !! ------------------------------------------------------------------->
  !! ------------------------------------------------------------------->
  subroutine polynomial_eval_array_3d(p,x0,y0,z0,val,nval,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p      !< polynomial to evaluate
    real(kind=RKIND_POL),intent(in)          :: x0(:)  !< 1st coordinates
    real(kind=RKIND_POL),intent(in)          :: y0(:)  !< 2nd coordinates
    real(kind=RKIND_POL),intent(in)          :: z0(:)  !< 3rd coordinates
    real(kind=RKIND_POL),intent(out)         :: val(:) !< value of p at the point
    integer             ,intent(in)          :: nval
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    integer :: I,K,L,j
    
    ctx_err%ierr=0
    val=0.0
    
    if(p%dim_domain .ne. 3) then
      ctx_err%msg = "** ERROR: Incorrect dimension [evalpoly_3d] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    do j = 1,nval
      do I=1,p%degree+1
         do K=1,I
            do L=1,K
               !!! we consider the ind1 element of F1
               val(j)=val(j)+p%coeff_pol((I+1)*I*(I-1)/6+K*(K-1)/2+L) * &
                             x0(j)**(I-K)*y0(j)**(K-L)*z0(j)**(L-1)
            end do
         end do
      end do
    end do

    return
  end subroutine polynomial_eval_array_3d

  !! -----------------------------------------------------------------
  subroutine polynomial_eval_2d(p,x0,y0,val,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p   !< polynomial to evaluate
    real(kind=RKIND_POL),intent(in)          :: x0  !< 1st coordinates
    real(kind=RKIND_POL),intent(in)          :: y0  !< 2nd coordinates
    real(kind=RKIND_POL),intent(out)         :: val !< value of p at the point
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    integer :: I,K
    
    ctx_err%ierr=0
    val=0.0
    
    if(p%dim_domain .ne. 2) then
      ctx_err%msg = "** ERROR: Incorrect dimension [evalpoly_2d] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
 
    do I=1,p%degree+1
      do K=1,I
        val=val+p%coeff_pol(I*(I-1)/2+K)*x0**(I-K)*y0**(K-1)
      enddo
    end do
    
    return
  end subroutine polynomial_eval_2d
  
  !! -----------------------------------------------------------------
  subroutine polynomial_eval_array_2d(p,x0,y0,val,nval,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p      !< polynomial to evaluate
    real(kind=RKIND_POL),intent(in)          :: x0(:)  !< 1st coordinates
    real(kind=RKIND_POL),intent(in)          :: y0(:)  !< 2nd coordinates
    real(kind=RKIND_POL),intent(out)         :: val(:) !< value of p at the point
    integer             ,intent(in)          :: nval
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    integer :: I,K,l
    
    ctx_err%ierr=0
    val=0.0
    
    if(p%dim_domain .ne. 2) then
      ctx_err%msg = "** ERROR: Incorrect dimension [evalpoly_2d] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
 
    do l = 1, nval
      do I=1,p%degree+1
        do K=1,I
          val(l)=val(l)+p%coeff_pol(I*(I-1)/2+K)*x0(l)**(I-K)*y0(l)**(K-1)
        enddo
      end do
    end do
    
    return
  end subroutine polynomial_eval_array_2d
  
  !! -----------------------------------------------------------------
  subroutine polynomial_eval_1d(p,x0,val,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p   !< polynomial to evaluate
    real(kind=RKIND_POL),intent(in)          :: x0  !< 1st coordinates
    real(kind=RKIND_POL),intent(out)         :: val !< value of p at the point
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    integer :: I
    
    ctx_err%ierr=0
    val=0.0
    
    if(p%dim_domain .ne. 1) then
      ctx_err%msg = "** ERROR: Incorrect dimension [evalpoly_1d] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    do I=1,p%degree+1
      val=val+p%coeff_pol(I)*x0**(I-1)
    end do
    
    return
  end subroutine polynomial_eval_1d
  !---------------------------------------------------------------------

  !! -----------------------------------------------------------------
  subroutine polynomial_eval_array_1d(p,x0,val,nval,ctx_err)
    implicit none
    type(t_polynomial)  ,intent(in)          :: p      !< polynomial to evaluate
    integer             ,intent(in)          :: nval
    real(kind=RKIND_POL),intent(in)          :: x0 (:) !< 1st coordinates
    real(kind=RKIND_POL),intent(out)         :: val(:) !< value of p at the point
    type(t_error)       ,intent(out)         :: ctx_err
    !< local
    integer :: I,k
    
    ctx_err%ierr=0
    val=0.0
    
    if(p%dim_domain .ne. 1) then
      ctx_err%msg = "** ERROR: Incorrect dimension [evalpoly_1d] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    do k = 1, nval
      do I=1,p%degree+1
        val(k) = val(k) + p%coeff_pol(I)*x0(k)**(I-1)
      end do
    end do

    return
  end subroutine polynomial_eval_array_1d
  !---------------------------------------------------------------------

  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Compute p3=p2*p1
  !
  !----------------------------------------------------------------------------
  subroutine  polynomial_multiplication(coe,p1,p2,p3,ctx_err)
    implicit none 
    type(t_polynomial_precomput),intent(in)    :: coe(3)
    type(t_polynomial)          ,intent(in)    :: p1 !< the first polynom
    type(t_polynomial)          ,intent(in)    :: p2 !< the second polynom 
    type(t_polynomial)          ,intent(out)   :: p3 !< result of the multip
    type(t_error)               ,intent(out)   :: ctx_err
    !! local
    integer :: i

    ctx_err%ierr = 0

    if (p1%dim_domain .ne. p2%dim_domain) then
      ctx_err%msg = "** ERROR: Incorrect dimension [polynom_multiplication] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    call polynomial_construct(p1%dim_domain,p1%degree+p2%degree,p3,ctx_err)
    p3%coeff_pol=0
    
    select case (p1%dim_domain)
      case(0)
         p3%coeff_pol(1)=p1%coeff_pol(1)*p2%coeff_pol(1)
      case(1)
         do I=1,p1%degree+1
            p3%coeff_pol(I:I+p2%degree) = p3%coeff_pol(I:I+p2%degree) + &
                               p1%coeff_pol(I)*p2%coeff_pol(1:p2%degree+1)
         end do
      case(2)
         do I=1,(p1%degree+2)*(p1%degree+1)/2
            p3%coeff_pol(coe(2)%multi(I,1:(p2%degree+2)*(p2%degree+1)/2))  = &
              p3%coeff_pol(coe(2)%multi(I,1:(p2%degree+2)*(p2%degree+1)/2))+ &
              p1%coeff_pol(I)*p2%coeff_pol(1:(p2%degree+2)*(p2%degree+1)/2)
         end do
      case(3)
        do I=1,(p1%degree+3)*(p1%degree+2)*(p1%degree+1)/6
          p3%coeff_pol(coe(3)%multi(I,1:(p2%degree+3) * &
              (p2%degree+2)*(p2%degree+1)/6)) = &
               p3%coeff_pol(coe(3)%multi(I,1:(p2%degree+3)*(p2%degree+2) &
              *(p2 %degree+1)/6))+p1%coeff_pol(I)*p2%coeff_pol(1:(p2%degree+3)*&
               (p2%degree +2)*(p2%degree+1)/6)
         end do
      case default
        ctx_err%msg ="** ERROR: Incorrect dimension [polynom_multiplication] **"
        ctx_err%ierr= -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return
    
  end subroutine polynomial_multiplication
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  construct the t_polynomial_precomput for precomputing 
  !>  multiplications and
  !>  integrals of \f$P_k\f$ polynomial.
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_precomput_clean(coe)
    implicit none 
    type(t_polynomial_precomput), intent(inout) :: coe(3)
    
    if(allocated(coe(1)%int_ref)) deallocate(coe(1)%int_ref)
    if(allocated(coe(2)%int_ref)) deallocate(coe(2)%int_ref)
    if(allocated(coe(3)%int_ref)) deallocate(coe(3)%int_ref)
    
    if(allocated(coe(1)%multi))   deallocate(coe(1)%multi)
    if(allocated(coe(2)%multi))   deallocate(coe(2)%multi)
    if(allocated(coe(3)%multi))   deallocate(coe(3)%multi)
    return
  end subroutine polynomial_precomput_clean
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  construct the t_polynomial_precomput for precomputing 
  !>  multiplications and
  !>  integrals of \f$P_k\f$ polynomial.
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_precomput_construct(coe,dim,deg,ctx_err)
    implicit none 
    type(t_polynomial_precomput), intent(inout) :: coe(3)
    integer        , intent(in) :: dim !< the dimension of the problem
    integer        , intent(in) :: deg !< the maximal degree of all polynomials
    type(t_error)  , intent(out):: ctx_err
    !! local
    integer :: i,j,k,l,m,n,ind
    real(kind=RKIND_POL) :: num,den
    
    
    ctx_err%ierr=0
    
    select case (dim)
    case(1)
       ind=2*deg+1
       allocate(coe(1)%multi(ind,ind))
       allocate(coe(1)%int_ref(ind))
       do I=1,deg+1
          do J=1,deg+1
             coe(1)%multi(I,J)=I+J-1
          end do
       end do
       do I=1,2*deg+1
          coe(1)%int_ref(I)=1._RKIND_POL/real(I,RKIND_POL)
       end do
    case(2)
       allocate(coe(2)%multi((deg+2)*(deg+1)/2,(deg+2)*(deg+1)/2))
       allocate(coe(2)%int_ref((2*deg+2)*(2*deg+1)/2))
       do I=1,deg+1
          do J=1,deg+1
             do K=1,I
                do L=1,J
                   coe(2)%multi((I-1)*I/2+K,(J-1)*J/2+L)=(I+J-2)*(I+J-1)/2+K &
                        &+L-1
                end do
             end do
          end do
       end do
       do I=1,2*deg+1
          do K=1,I
             ind=int(I*(I-1)/2D0+K,kind=4)
             ! we consider the ind1 element of coeffpoly
             ! it corresponds to x^(I-K)y^(K-1)
             ! the integral of x^py^q on for x in [0,1] and y in [0,1-x] is 
             ! num/dem with num=factorial(min(p,q)) and den=max(p,q)x(max(p,q)
             !+1)...(p+q+2)
             num=1
             do J=1,MIN(I-K,K-1)
                num=num*J
             end do
             den=MAX(I-K,K-1)+1
             do J=MAX(I-K,K-1)+2,I+1
                den=den*J
             end do
             coe(2)%int_ref(ind)=num/den
          end do
       end do
    case(3)
       ind=(deg+3)*(deg+2)*(deg+1)/6
       allocate(coe(3)%multi((deg+3)*(deg+2)*(deg+1)/6,(deg+3)*(deg+2)*(deg &
            &+1)/6))
       allocate(coe(3)%int_ref((2*deg+3)*(2*deg+2)*(2*deg+1)/6))
       do I=1,deg+1
          do J=1,deg+1
             do K=1,I
                do L=1,J
                   do M=1,K
                      do N=1,L
                         coe(3)%multi((I-1)*I*(I+1)/6+K*(K-1)/2+M,(J-1)*J*(J &
                              &+1)/6 +L*(L-1)/2+N)=(I+J-2)*(I+J-1)*(I+J)/6+(K+L&
                              & -2)*(K+L-1)/2+M+N-1

                      end do
                   end do
                end do
             end do
          end do
       end do
       do I=1,2*deg+1
          do K=1,I
             do L=1,K
                ind=int( (I+1)*I*(I-1)/6D0+K*(K-1)/2+L, kind=4)
                ! we consider the ind1 element of coeffpoly
                ! it corresponds to x^(I-K)y^(K-L)z^(L-1)
                ! the integral of x^py^qz^r on for x in [0,1], y in [0,1-x] and
                ! z in [0, 1-x-y-z] is 
                ! num/dem with num=factorial(p)*factorial(q) and den=(r+1)*(r
                !+2)...(r+p+q+3)
                num=1D0
                do J=1,I-K
                   num=num*J
                end do
                do J=1,K-L
                   num=num*J
                end do

                den=L
                do J=L+1,I+2
                   den=den*J
                end do
                coe(3)%int_ref(ind)=num/den
             end do
          end do
       end do
  
      case default
        ctx_err%msg ="** ERROR: Incorrect dimension [polynom_multiplication] **"
        ctx_err%ierr= -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return
  end subroutine polynomial_precomput_construct

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Compute the integral of a \f$P_k\f$ polynomial P on the reference
  !> segment (in 1D), triangle (in 2D) or tetrahedra (in 3D)
  !>\li if p\%dim=1 it computes \f$\displaystyle \int_0^1P(x)dx\f$
  !>\li if p\%dim=2 it computes \f$\displaystyle \int_0^1\int_0^{1-x}
  !>       P(x,y)dxdy. \f$
  !>\li if p\%dim=3 it computes \f$\displaystyle \int_0^1\int_0^{1-x}
  !>      \int_0^{1-x-y}P(x,y,z)dxdydz. \f$
  !>\li Whatever the dimension, the integral of P is computed by 
  !>    \f$\displaystyle \sum_{i=1}^{p\%size} coe(p\%dim)
  !>    \%integ(i)p\%coeff(i)\f$
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_integrate(coe,p,val,ctx_err)
    implicit none
    integer :: I
    type(t_polynomial_precomput), intent(in)    :: coe(3) !! precomputed infos
    type(t_polynomial)           ,intent(in)    :: p   !< the polynom to int
    real(kind=RKIND_POL)         ,intent(out)   :: val !< the value of the int
    type(t_error)                ,intent(out)   :: ctx_err
    
    ctx_err%ierr=0 
    val = 0.0d0
    select case(p%dim_domain)
    case(0)
       val=p%coeff_pol(1)
    case(1)
       do I=1,p%size_coeff
          !!! we consider the I element of F1
          !!! this is the coefficient of x^I
          val=val+p%coeff_pol(I)/real(I,RKIND_POL)
       end do
    case(2)
       val=SUM(coe(2)%int_ref(1:(p%degree+2)*(p%degree+1)/2) * &
               p%coeff_pol(1:(p%degree+2)*(p%degree+1)/2))
    case(3)
       val=SUM(coe(3)%int_ref(1:(p%degree+3)*(p%degree+2)    * &
              (p%degree+1)/6)*p%coeff_pol(1:(p%degree+3)     * &
              (p%degree+2)*(p%degree+1)/6))
    case default
      ctx_err%msg ="** ERROR: Incorrect dimension [polynom_integratie] **"
      ctx_err%ierr= -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end select
    
    return
  
  end subroutine polynomial_integrate
  !----------------------------------------------------------------------------

  !>@brief Compute \f$P_2\f$ the derivative of a 
  !>       \f$P_k\f$ polynomial \f$P_1\f$ with respect to x
  !>\li if p\%dim_domain=1 \f$\displaystyle p_2\%coeff_pol(i)=(i+1)p_2\%coeff_pol(i+1)\f$
  !>\li if p\%dim_domain=2 \f$\displaystyle p_2\%coeff_pol(l_2(i,j))
  !>                =(i+1)p_2\%coeff_pol(l_2(i+1,j))\f$
  !>\li if p\%dim_domain=3 \f$\displaystyle p_2\%coeff_pol(l_2(i,j,k))
  !>                =(i+1)p_2\%coeff_pol(l_2(i+1,j,k))\f$ 
  subroutine polynomial_dx(p1,p2,ctx_err)
    implicit none
    
    type(t_polynomial),intent(in)  :: p1 !< the polynomial to derivate
    type(t_polynomial),intent(out) :: p2 !< the derivate of the polynomial
    type(t_error)     ,intent(out)   :: ctx_err
    !! local
    integer :: I,K,L
    
    ctx_err%ierr=0
    
    call polynomial_construct(p1%dim_domain,p1%degree-1,p2,ctx_err)
    select case(p1%dim_domain)
    case(1)
       do I=1,p1%degree
          p2%coeff_pol(I)=I*p1%coeff_pol(I+1)
       end do
    case(2)
       do I=p1%degree+1,1,-1 !We run from the highest degree to the lowest
          do K=1,I-1 ! The  element I corresponds to y^I and vanishes
             p2%coeff_pol((I-2)*(I-1)/2+K)=p1%coeff_pol(((I-1)*I/2+K))*(I-K)
          end do
       end do
    case(3)
       do I=p1%degree+1,1,-1 !We run from the highest degree to the lowest
          do K=1,I-1
             do L=1,K
                ! The  elements higher than I correspond to y^kz^l and vanish
                p2%coeff_pol(I*(I-2)*(I-1)/6+(K-1)*K/2+L)= &
                      p1%coeff_pol((I-1)*I*(I+1)/6+(K-1)*K/2+L)*(I-K)
             end do
          end do
       end do
    case default
      ctx_err%msg ="** ERROR: Incorrect dimension for X derivative " // &
                   "[polynomial_dx] **"
      ctx_err%ierr= -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end select
  end subroutine polynomial_dx
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Compute \f$P_2\f$ the derivative of a \f$P_k\f$ polynomial \f$P_1\f$ 
  !>  with respect to y
  !> \li if p\%dim_domain=2 \f$\displaystyle p_2\%coeff_pol(l_2(i,j))
  !>        =(j+1)p_2\%coeff_pol(l_2(i,j+1))\f$
  !> \li if p\%dim_domain=3 \f$\displaystyle p_2\%coeff_pol(l_2(i,j,k))
  !>        =(j+1)p_2\%coeff_pol(l_2(i,j+1,k))\f$ 
  !
  !----------------------------------------------------------------------------
  subroutine polynomial_dy(p1,p2,ctx_err)
    implicit none
    
    type(t_polynomial),intent(in)  :: p1!< the polynomial to derivate
    type(t_polynomial),intent(out) :: p2!< the derivate of the polynomial
    type(t_error)     ,intent(out) :: ctx_err
    !! local
    integer :: I,K,L

    ctx_err%ierr=0
    call polynomial_construct(p1%dim_domain,p1%degree-1,p2,ctx_err)
    select case(p1%dim_domain)
    case(2)
       do I=p1%degree+1,1,-1 !We run from the highest degree to the lowest
          do K=2,I ! The  element 1 corresponds to x^I and vanishes
             p2%coeff_pol((I-2)*(I-1)/2+K-1)=p1%coeff_pol((I-1)*I/2+K)*(K-1)
          end do
       end do
    case(3)
       do I=p1%degree+1,1,-1 !We run from the highest degree to the lowest
          do K=1,I 
             do L=1,K-1
                ! The  other elements  correspond to x^iz^j and vanish
                p2%coeff_pol((I-2)*(I-1)*I/6+(K-2)*(K-1)/2+L)= &
                         p1%coeff_pol((I-1)*I*(I+1)/6+(K-1)*K/2+L)*(K-L)
             end do
          end do
       end do
    case default
      ctx_err%msg ="** ERROR: Incorrect dimension for Y derivative " // &
                   "[polynomial_dy] **"
      ctx_err%ierr= -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end select
  end subroutine polynomial_dy
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Compute \f$P_2\f$ the derivative of a \f$P_k\f$ polynomial 
  !>  \f$P_1\f$ with respect to z
  !>\li if p\%dim_domain=3 \f$\displaystyle p_2\%coeff_pol(l_2(i,j,k))
  !>       =(k+1)p_2\%coeff_pol(l_2(i,j,k+1))\f$   !
  !----------------------------------------------------------------------------
  subroutine polynomial_dz(p1,p2,ctx_err)
    implicit none
    type(t_polynomial),intent(in)  :: p1!< the polynomial to derivate
    type(t_polynomial),intent(out) :: p2!< the derivate of the polynomial
    type(t_error)     ,intent(out) :: ctx_err
    !! local
    integer :: I,K,L
    
    ctx_err%ierr=0
    call polynomial_construct(p1%dim_domain,p1%degree-1,p2,ctx_err)
    select case(p1%dim_domain)
    case(3)
       do I=p1%degree+1,1,-1 !We run from the highest degree to the lowest
          do K=1,I 
             do L=2,K
                ! The  other elements  correspond to x^iz^j and vanish
                p2%coeff_pol((I-2)*(I-1)*I/6+(K-2)*(K-1)/2+L-1)= &
                   p1%coeff_pol((I-1)*I*(I+1)/6+(K-1)*K/2+L)*(L-1)
             end do
          end do
       end do

    case default
      ctx_err%msg ="** ERROR: Incorrect dimension for Z derivative " // &
                   "[polynomial_dz] **"
      ctx_err%ierr= -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end select
  end subroutine polynomial_dz
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Invert the parametrization of a \f$P_k\f$ polynomial \f$P_1\f$ in
  !> dimension 1 or 2. The resulting polynomial is   \f$P_2\f$
  !>\li if p\%dim=1 :\f$P_2(x)=P_1(1-x)\f$
  !>\li if p\%dim=2 :<ul><li> if case=1 : \f$\displaystyle P_2(x,y)=P_1(y,x)\f$
  !><li> if case=2 (edge x=0): \f$\displaystyle P_2(x,y)=P_1(1-x-y,y)\f$
  !><li> if case=3 (edge y=0): \f$\displaystyle P_2(x,y)=P_1(x,1-x-y)\f$</ul>
  !----------------------------------------------------------------------------
  subroutine polynomial_invert_coo(p1,p2,mycase,ctx_err)
    
    implicit none

    type(t_polynomial),intent(in)  :: p1    !< the polynomial to transform
    integer, intent(in)            :: mycase !< only if p1%dim_domain=2: define the 2D parametrization
    type(t_polynomial),intent(out) :: p2   !< the resulting polynomial
    type(t_error)     ,intent(out) :: ctx_err
    !! local
    integer             :: I,K,II,JJ,index1,index2
    integer,allocatable :: coef(:),coef2(:)
    
    ctx_err%ierr=0
    
    call polynomial_construct(p1%dim_domain,p1%degree,p2,ctx_err)
    allocate(coef(p2%degree+1))
    allocate(coef2(p2%degree+1))
    p2%coeff_pol=0
    coef=0
    coef2=0
    select case(p1%dim_domain)
    case(1)
    !!!! x->1-x
       do k=1,p2%degree+1
          coef=0
          coef(1)=1
          do ii=1,k-1
             coef(2:ii)=coef(2:ii)+coef(1:ii-1)
             coef(ii+1)=1
          end do
          do ii=1,k
             p2%coeff_pol(ii)=p2%coeff_pol(ii) + &
                coef(ii)*(-1)**(ii-1)*p1%coeff_pol(k)
          end do
       end do
    case(2)
       select case(mycase)
       case(1)
       !!! x->y and y->x
          do i=1,p2%degree+1
             do k=1,i
                index1=(i-1)*i/2+k
                index2=(i-1)*i/2+i-k+1
                p2%coeff_pol(index2)=p1%coeff_pol(index1)
             end do
          end do
       case(2)
       !!! x->1-x-y and y->y
          do i=1,p2%degree+1
             do k=1,i
                index1=(i-1)*i/2+k
                coef=0
                coef(1)=1
                do ii=1,i-k
                   coef(2:ii)=coef(2:ii)+coef(1:ii-1)
                   coef(ii+1)=1
                end do
                do jj=1,i-k+1
                   coef2=0
                   coef2(1)=1
                   do ii=1,jj-1
                      coef2(2:ii)=coef2(2:ii)+coef2(1:ii-1)
                      coef2(ii+1)=1
                   end do
                   do ii=1,jj
                      index2=(jj+k-2)*(jj+k-1)/2+jj-ii+k
                      p2%coeff_pol(index2)=p2%coeff_pol(index2) + &
                         (-1)**(jj-1)*coef(jj)*coef2(ii)*p1%coeff_pol(index1)
                   end do
                end do
             end do
          end do
       case(3)
       !!! x->x and y->1-x-y
          do i=1,p2%degree+1
             do k=1,i
                index1=(i-1)*i/2+k
                coef=0
                coef(1)=1
                do ii=1,k-1
                   coef(2:ii)=coef(2:ii)+coef(1:ii-1)
                   coef(ii+1)=1
                end do
                do jj=1,k
                   coef2=0
                   coef2(1)=1
                   do ii=1,jj-1
                      coef2(2:ii)=coef2(2:ii)+coef2(1:ii-1)
                      coef2(ii+1)=1
                   end do
                   do ii=1,jj
                      index2=(i-k+jj-1)*(i-k+jj)/2+jj-ii+1
                      p2%coeff_pol(index2)=p2%coeff_pol(index2) + &
                          (-1)**(jj-1)*coef(jj)*coef2(ii) *       &
                          p1%coeff_pol(index1)
                   end do
                end do
             end do
          end do
      case default
        ctx_err%msg ="** ERROR: Incorrect choice of 2D parametrization " // &
                     "[polynomial_invert_coo] **"
        ctx_err%ierr= -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end select

    case default
      ctx_err%msg ="** ERROR: Incorrect dimension in [polynomial_invert_coo] **"
      ctx_err%ierr= -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end select
  end subroutine polynomial_invert_coo
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Compute \f$P_2\f$ the trace of a \f$P_k\f$ polynomial \f$P_1\f$ on a
  !> given face of the reference element. The dimension is decreased by one.  
  !>\li if p\%dim=1 :
  !> <ul><li> if face=1 (point x=0): \f$P_2=P_1(0)=p1\%coeff(1)\f$
  !> <li> if face=2 (point x=1): \f$\displaystyle P_2=P_1(1)
  !>            =\sum_{i=1}^{p1\%degree} p1\%coeff(i)\f$</ul>
  !>  \li if p\%dim=2 :<ul><li> if face=1 (edge x+y=1): \f$\displaystyle P_2(x)
  !>       =P_1(1-x,x)\f$
  !> <li> if face=2 (edge x=0): \f$\displaystyle P_2(x)=P_1(0,1-x)\f$
  !> <li> if face=3 (edge y=0): \f$\displaystyle P_2(x)=P_1(x,0)\f$
  !> <li> Note that the parametrization is chosen so that we run 
  !> counterclockwise on each edge.</ul>
  !> \li if p\%dim=3 :<ul><li> if face=1 (face x+y+z=1): 
  !>        \f$\displaystyle P_2(x,y)=P_1(1-x-y,y,x)\f$
  !> <li> if face=2 (face x=0): \f$\displaystyle P_2(x,y)=P_1(0,x,y)\f$
  !> <li> if face=3 (face y=0): \f$\displaystyle P_2(x,y)=P_1(y,0,x)\f$
  !> <li> if face=4 (face z=0): \f$\displaystyle P_2(x,y)=P_1(x,y,0)\f$</ul>
  !----------------------------------------------------------------------------
  subroutine polynomial_trace(p1,p2,face,ctx_err)
    
    implicit none
    
    !! The face on which the polynomial should be computed
    integer, intent(in)            :: face 
    type(t_polynomial) ,intent(in) :: p1   !< The polynomial to be reduced
    type(t_polynomial) ,intent(out):: p2   !< The trace of p1 on face face
    type(t_error)      ,intent(out):: ctx_err
    !! local
    integer :: I,J,K,L,ind1,ind2,II,JJ
    integer,allocatable ::coef(:),coef2(:)
    
    ctx_err%ierr=0
    call polynomial_construct(p1%dim_domain-1,p1%degree,p2,ctx_err)
    
    p2%coeff_pol=0
    select case(p1%dim_domain)
    case(1)
       select case(face)
       case(1)
          p2%coeff_pol(1)=p1%coeff_pol(1)
       case(2)
          p2%coeff_pol(1)=SUM(p1%coeff_pol(:))
       end select
    case(2)
       allocate(coef(p1%degree+1))
       select case(face)
       case(1)!! Edge 1, warning, this is not the edge y=0, this is the
          !! opposite edge to the first vertex : x+y=1
          !!On this edge, (x,y) are parameterized by (1-X,X)
          do I=1,p1%degree+1
             !we have to replace x by (1-y) (and not y by (1-x))
             do L=1,I
                ind2=I*(I-1)/2+L 
                !! This is the coefficient of x^(I-L)y^(L-1)
                ! With the 1D parameterization,  x^(I-L)y^(L-1) becomes
                ! (1-X)^(I-L)X^(L-1)
                ! First we compute the t_coefficients of the expansion of (1
                !-X)^(I-L)
                coef=0
                coef(1)=1
                do II=1,I-L
                   coef(2:II)=coef(2:II)+coef(1:II-1)
                   coef(II+1)=1
                end do
                !! Now, we can compute the expansion of (1-X)^(I-L)X^(L-1)
                do J=0,I-L
                   !! This is coeff1[J+1]*1^J*(-X)^(I-L-J)*X^(L-1)
                   ind1=I-J
                   p2%coeff_pol(ind1)=p2%coeff_pol(ind1)+coef(J+1)*(-1)**(I-L-J)*p1 &
                        &%coeff_pol(ind2)
                enddo
             enddo
          end do
       case(2) !! Edge 2, warning, this is not the edge x+y=1, this is the
          !! opposite edge to the second vertex : x=0
          !!On this edge, (x,y) are parameterized by (0,1-X)
          do I=1,p1%degree+1
             ind2=I*(I+1)/2 !we are only interested in the coefficient y^(i-1)
             ! With the 1D parameterization,  it becomes (1-X)^(I-1)
             ! First we compute the t_coefficients of the expansion of (1-X)^(I-1)
             coef=0
             coef(1)=1
             do II=1,I-1
                coef(2:II)=coef(2:II)+coef(1:II-1)
                coef(II+1)=1
             end do
             !! Now, we can compute the expansion of (1-X)^(I-1)
             do J=0,I-1
                !! This is coeff1[J+1]*1^J*(-X)^(I-1-J)
                ind1=I-J
                p2%coeff_pol(ind1)=p2%coeff_pol(ind1)+coef(J+1)*(-1)**(I-1-J)*p1 &
                     &%coeff_pol(ind2)
             enddo
          enddo
       case(3) !! Edge 3, warning, this is not the edge x=0, this is the
          !! opposite edge to the third vertex : y=0
          !!On this edge, (x,y) are parameterized by (X,0)
          do I=1,p1%degree+1
             ind1=I*(I-1)/2+1!we are only interested in the coefficient x^(i-1)
             p2%coeff_pol(I)=p1%coeff_pol(ind1)
          enddo
       end select
    case(3)
       allocate(coef(p1%degree+1))
       allocate(coef2(p1%degree+1))
       select case(face)
       case(1) !! Face 1, warning, this is not the face z=0, this is the
          !! opposite face to the first vertex : x+y+z=1. 
          !!On this face, (x,y,z) are parameterized by (1-X-Y,Y,X)
          do I=1,p1%degree+1
             do K=1,I 
                do L=1,K
                   ! This the coefficient of x^(I-K)y^(K-L)z^(L-1)
                   ind2=(I-1)*I*(I+1)/6+(K-1)*K/2+L
                   ! With the 2D parameterization, x^(I-K)y^(K-L)z^(L-1) becomes
                   ! (1-X-Y)^(I-K)Y^(K-L)X^(L-1)
                   ! First we compute the t_coefficients of the expansion of (1-X
                   !-Y)^(I-K)
                   coef=0
                   coef(1)=1
                   do II=1,I-K
                      coef(2:II)=coef(2:II)+coef(1:II-1)
                      coef(II+1)=1
                   end do
                   do JJ=1,I-K+1
                      coef2=0
                      coef2(1)=1
                      do II=1,JJ-1
                         coef2(2:II)=coef2(2:II)+coef2(1:II-1)
                         coef2(II+1)=1
                      end do
                      do II=1,JJ
                         ind1=(JJ+K-2)*(JJ+K-1)/2+JJ-II+K-L+1
                         p2%coeff_pol(ind1)=p2%coeff_pol(ind1)+coef(JJ)*coef2(II)*(-1) &
                              &**(JJ +1)*p1%coeff_pol(ind2)
                      end do
                   end do
                end do
             enddo
          end do
       case(2) !! Face 2, warning, this is not the face y=0, this is the
          !! opposite face to the second vertex : x=0. 
          !! On this face, (x,y,z) are parameterized by (0,X,Y).
          do I=1,p1%degree+1
             do K=1,I 
                ! This the coefficient of y^(I-K)z^(K-1)
                ind2=(I-1)*I*(I+1)/6+(I-1)*I/2+K
                ! In 2D, it becomes the coefficient X^(I-K)Y^(K-1)
                ind1=(I-1)*I/2+K
                p2%coeff_pol(ind1)=p2%coeff_pol(ind1)+p1%coeff_pol(ind2)
             end do
          enddo
       case(3) !! Face 3, warning, this is not the face x=0, this is the
          !! opposite edge to the third vertex : y=0. 
          !! On this face, (x,y,z) is parameterized by (Y,0,X) in 2D. 
          do I=1,p1%degree+1
             do K=1,I 
                ! This the coefficient of x^(I-K)z^(K-1)
                ind2=(I-1)*I*(I+1)/6+K*(K+1)/2
                ! In 2D, it becomes the coefficient X^(K-1)Y^(I-K)
                ind1=(I+1)*I/2+1-K
                p2%coeff_pol(ind1)=p2%coeff_pol(ind1)+p1%coeff_pol(ind2)
             end do
          enddo
       case(4) !! Face 4, warning, this is not the face x+y+z=1, this is the
          !! opposite edge to the fourth vertex : z=0. 
          !! On this face, (x,y,z) is parameterized by (X,Y,0). 
          do I=1,p1%degree+1
             do K=1,I 
                ! This the coefficient of x^(I-K)y^(K-1)
                ind2=(I-1)*I*(I+1)/6+(K-1)*K/2+1
                ! we consider the ind2 element of F2
                ind1=(I-1)*I/2+K
                ! After derivation with respect to y, it becomes the ind1
                ! element of F1
                p2%coeff_pol(ind1)=p2%coeff_pol(ind1)+p1%coeff_pol(ind2)
             end do
          enddo
       end select
    end select
    
    return
  end subroutine polynomial_trace
  !----------------------------------------------------------------------------

end module m_polynomial
