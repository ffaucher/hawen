!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_array_types.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to define the array types for 2D and 3D real, integer
!> and complex 
!> It comes from J.Diaz body of work in software [HOU10NI]
!
!------------------------------------------------------------------------------
module m_array_types

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: RKIND_POL, RKIND_MAT
  !! -------------------------------------------------------------------
  implicit none
  
  private
  public   :: t_array1d_int, t_array1d_real, t_array2d_int, t_array2d_real, &
              t_array3d_int, t_array3d_real, array_deallocate,              &
              array_allocate,t_array4d_real, t_array1d_complex,             &
              t_array2d_complex, t_array3d_complex, t_array4d_complex,      &
              t_array2d_complex_kindmat, t_array5d_complex,                 &
              t_array5d_real, t_array6d_real
  
  type t_array1d_int
    integer, allocatable   :: array(:)
  end type t_array1d_int

  type t_array2d_int
    integer, allocatable   :: array(:,:)
  end type t_array2d_int

  type t_array3d_int
    integer, allocatable   :: array(:,:,:)
  end type t_array3d_int
  
  type t_array1d_real
    real(kind=RKIND_POL), allocatable   :: array(:)
  end type t_array1d_real

  type t_array2d_real
    real(kind=RKIND_POL), allocatable   :: array(:,:)
  end type t_array2d_real

  type t_array3d_real
    real(kind=RKIND_POL), allocatable   :: array(:,:,:)
  end type t_array3d_real

  type t_array4d_real
    real(kind=RKIND_POL), allocatable   :: array(:,:,:,:)
  end type t_array4d_real

  type t_array5d_real
    real(kind=RKIND_POL), allocatable   :: array(:,:,:,:,:)
  end type t_array5d_real

  type t_array6d_real
    real(kind=RKIND_POL), allocatable   :: array(:,:,:,:,:,:)
  end type t_array6d_real

  type t_array1d_complex
    complex(kind=RKIND_POL), allocatable   :: array(:)
  end type t_array1d_complex

  type t_array2d_complex
    complex(kind=RKIND_POL), allocatable   :: array(:,:)
  end type t_array2d_complex

  type t_array2d_complex_kindmat
    complex(kind=RKIND_MAT), allocatable   :: array(:,:)
  end type t_array2d_complex_kindmat

  type t_array3d_complex
    complex(kind=RKIND_POL), allocatable   :: array(:,:,:)
  end type t_array3d_complex

  type t_array4d_complex
    complex(kind=RKIND_POL), allocatable   :: array(:,:,:,:)
  end type t_array4d_complex

  type t_array5d_complex
    complex(kind=RKIND_POL), allocatable   :: array(:,:,:,:,:)
  end type t_array5d_complex
  
  !! interface to clean the array
  interface array_deallocate
    module procedure array_deallocate_1d_int
    module procedure array_deallocate_2d_int
    module procedure array_deallocate_3d_int
    module procedure array_deallocate_1d_real
    module procedure array_deallocate_2d_real
    module procedure array_deallocate_3d_real
    module procedure array_deallocate_3d_complex
    module procedure array_deallocate_4d_complex
    module procedure array_deallocate_5d_complex
    module procedure array_deallocate_4d_real
    module procedure array_deallocate_5d_real
    module procedure array_deallocate_6d_real
    module procedure array_deallocate_1d_complex
    module procedure array_deallocate_2d_complex
    module procedure array_deallocate_2d_complex_kindmat
  end interface array_deallocate

  !! interface to init the array
  interface array_allocate
    module procedure array_allocate_1d_int
    module procedure array_allocate_2d_int
    module procedure array_allocate_3d_int
    module procedure array_allocate_1d_real
    module procedure array_allocate_1d_real_i8
    module procedure array_allocate_2d_real
    module procedure array_allocate_2d_real_i8
    module procedure array_allocate_3d_real
    module procedure array_allocate_3d_complex
    module procedure array_allocate_4d_complex
    module procedure array_allocate_5d_complex
    module procedure array_allocate_4d_real
    module procedure array_allocate_5d_real
    module procedure array_allocate_6d_real
    module procedure array_allocate_1d_complex
    module procedure array_allocate_2d_complex
    module procedure array_allocate_2d_complex_kindmat
  end interface array_allocate
  
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_1d_int(ctx_array)
    implicit none 
    type(t_array1d_int),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_1d_int
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_2d_int(ctx_array)
    implicit none 
    type(t_array2d_int),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_2d_int
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_3d_int(ctx_array)
    implicit none 
    type(t_array3d_int),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_3d_int

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_1d_real(ctx_array)
    implicit none 
    type(t_array1d_real),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_1d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_2d_real(ctx_array)
    implicit none 
    type(t_array2d_real),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_2d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_3d_real(ctx_array)
    implicit none 
    type(t_array3d_real),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_3d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_3d_complex(ctx_array)
    implicit none 
    type(t_array3d_complex),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_3d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_4d_complex(ctx_array)
    implicit none 
    type(t_array4d_complex),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_4d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_5d_complex(ctx_array)
    implicit none 
    type(t_array5d_complex),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_5d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_4d_real(ctx_array)
    implicit none 
    type(t_array4d_real),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_4d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_5d_real(ctx_array)
    implicit none 
    type(t_array5d_real),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_5d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_6d_real(ctx_array)
    implicit none 
    type(t_array6d_real),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_6d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_1d_complex(ctx_array)
    implicit none 
    type(t_array1d_complex),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_1d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_2d_complex(ctx_array)
    implicit none 
    type(t_array2d_complex),  intent(inout) :: ctx_array
    if(allocated(ctx_array%array)) deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_2d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  clean array
  !----------------------------------------------------------------------------
  subroutine array_deallocate_2d_complex_kindmat(ctx_array)
    implicit none 
    type(t_array2d_complex_kindmat), intent(inout) :: ctx_array
    if(allocated(ctx_array%array))   deallocate(ctx_array%array)
    return
  end subroutine array_deallocate_2d_complex_kindmat


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_1d_int(ctx_array,n1,ctx_err)
    implicit none 
    type(t_array1d_int)    ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0) then
      allocate(ctx_array%array(n1))
      ctx_array%array=0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if
    return
  end subroutine array_allocate_1d_int
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !
  !----------------------------------------------------------------------------
  subroutine array_allocate_1d_real(ctx_array,n1,ctx_err)
    implicit none 
    type(t_array1d_real)   ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0) then
      allocate(ctx_array%array(n1))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_1d_real
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !
  !----------------------------------------------------------------------------
  subroutine array_allocate_1d_real_i8(ctx_array,n1,ctx_err)
    implicit none 
    type(t_array1d_real)   ,intent(inout) :: ctx_array
    integer(kind=8)        ,intent(in)    :: n1
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0) then
      allocate(ctx_array%array(n1))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_1d_real_i8
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !
  !----------------------------------------------------------------------------
  subroutine array_allocate_2d_int(ctx_array,n1,n2,ctx_err)
    implicit none 
    type(t_array2d_int)    ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0) then
      allocate(ctx_array%array(n1,n2))
      ctx_array%array=0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_2d_int
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !
  !----------------------------------------------------------------------------
  subroutine array_allocate_2d_real(ctx_array,n1,n2,ctx_err)
    implicit none 
    type(t_array2d_real)   ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0) then
      allocate(ctx_array%array(n1,n2))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_2d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !
  !----------------------------------------------------------------------------
  subroutine array_allocate_2d_real_i8(ctx_array,n1,n2,ctx_err)
    implicit none 
    type(t_array2d_real)   ,intent(inout) :: ctx_array
    integer(kind=8)        ,intent(in)    :: n1,n2
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0) then
      allocate(ctx_array%array(n1,n2))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_2d_real_i8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !
  !----------------------------------------------------------------------------
  subroutine array_allocate_3d_int(ctx_array,n1,n2,n3,ctx_err)
    implicit none 
    type(t_array3d_int)    ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0) then
      allocate(ctx_array%array(n1,n2,n3))
      ctx_array%array=0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_3d_int
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_3d_real(ctx_array,n1,n2,n3,ctx_err)
    implicit none 
    type(t_array3d_real)    ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0) then
      allocate(ctx_array%array(n1,n2,n3))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_3d_real
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_3d_complex(ctx_array,n1,n2,n3,ctx_err)
    implicit none 
    type(t_array3d_complex),intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0) then
      allocate(ctx_array%array(n1,n2,n3))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_3d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_4d_complex(ctx_array,n1,n2,n3,n4,ctx_err)
    implicit none 
    type(t_array4d_complex),intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3,n4
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0 .and. n4>0) then
      allocate(ctx_array%array(n1,n2,n3,n4))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_4d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_5d_complex(ctx_array,n1,n2,n3,n4,n5,ctx_err)
    implicit none 
    type(t_array5d_complex),intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3,n4,n5
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0 .and. n4>0 .and. n5>0) then
      allocate(ctx_array%array(n1,n2,n3,n4,n5))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_5d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_4d_real(ctx_array,n1,n2,n3,n4,ctx_err)
    implicit none 
    type(t_array4d_real)   ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3,n4
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0 .and. n4>0) then
      allocate(ctx_array%array(n1,n2,n3,n4))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_4d_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_5d_real(ctx_array,n1,n2,n3,n4,n5,ctx_err)
    implicit none 
    type(t_array5d_real)   ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3,n4,n5
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0 .and. n4>0 .and. n5>0) then
      allocate(ctx_array%array(n1,n2,n3,n4,n5))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_5d_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_6d_real(ctx_array,n1,n2,n3,n4,n5,n6,ctx_err)
    implicit none 
    type(t_array6d_real)   ,intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2,n3,n4,n5,n6
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0 .and. n3>0 .and. n4>0 .and. n5>0 .and. n6>0) then
      allocate(ctx_array%array(n1,n2,n3,n4,n5,n6))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_6d_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_1d_complex(ctx_array,n1,ctx_err)
    implicit none 
    type(t_array1d_complex),intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0) then
      allocate(ctx_array%array(n1))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_1d_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_2d_complex(ctx_array,n1,n2,ctx_err)
    implicit none 
    type(t_array2d_complex),intent(inout) :: ctx_array
    integer                ,intent(in)    :: n1,n2
    type(t_error)          ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0) then
      allocate(ctx_array%array(n1,n2))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_2d_complex
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief  init array
  !----------------------------------------------------------------------------
  subroutine array_allocate_2d_complex_kindmat(ctx_array,n1,n2,ctx_err)
    implicit none 
    type(t_array2d_complex_kindmat),intent(inout) :: ctx_array
    integer                        ,intent(in)    :: n1,n2
    type(t_error)                  ,intent(inout) :: ctx_err
    
    if(n1 > 0 .and. n2 > 0) then
      allocate(ctx_array%array(n1,n2))
      ctx_array%array=0.0
    else
      ctx_err%msg = "** ERROR: negative size for allocation [array_allocate] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if 
    return
  end subroutine array_allocate_2d_complex_kindmat
end module m_array_types
