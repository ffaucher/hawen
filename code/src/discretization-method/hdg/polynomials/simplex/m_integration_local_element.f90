!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_integration_local_element.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the integration on a local
!> element, in particular, we allow the multiplication by 
!> the space variable to compute, e.g. 
!>   \f$ \int_{K}  x  \phi_i  \phi_j\f$
!> or 
!>   \f$ \int_{K} 1/x \phi_i  \phi_j\f$
!> 
!
!------------------------------------------------------------------------------
module m_integration_local_element

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! polynomuals
  use m_polynomial,             only: t_polynomial
  use, intrinsic                   :: ieee_arithmetic
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: eval_integral_pol_over_x, eval_integral_pol
  public :: eval_integral_pol_times_x2, eval_integral_pol_times_x

  !! -------------------------------------------------------------------
  !> to evaluate the integral of the function divided by \f$x\f$.
  interface eval_integral_pol_over_x
    module procedure eval_integral_pol_over_x_1D_real8
  end interface eval_integral_pol_over_x
  
  !> to evaluate the integral on a given subdomain
  interface eval_integral_pol
    module procedure eval_integral_pol_1D_real8
  end interface eval_integral_pol
  
  !> to evaluate the integral on a given subdomain of pol times \f$x\f$
  interface eval_integral_pol_times_x
    module procedure eval_integral_pol_times_x_1D_real8
  end interface eval_integral_pol_times_x
  
  !> to evaluate the integral on a given subdomain of pol times \f$x^2\f$
  interface eval_integral_pol_times_x2
    module procedure eval_integral_pol_times_x2_1D_real8
  end interface eval_integral_pol_times_x2
  !! -------------------------------------------------------------------

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Evaluate the integral on the given subdomain for 
  !> phi * 1/x * phi, i.e. we divide by x.
  !> the input is phi*phi polynomial
  !>
  !> @param[in]    pol             : polynomial to integrate
  !> @param[in]    node_coo        : domain of integration
  !> @param[out]   intval          : integral value
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine eval_integral_pol_over_x_1D_real8(pol,intval,node_coo,ctx_err)

    implicit none

    type(t_polynomial)              ,intent(in)    :: pol
    real(kind=8)      ,allocatable  ,intent(in)    :: node_coo(:,:)
    real(kind=8)                    ,intent(out)   :: intval
    type(t_error)                   ,intent(inout) :: ctx_err
    !! local 
    integer                          :: l,power
    !! -----------------------------------------------------------------
    
    ctx_err%ierr=0

    !! loop over all power of x,
    !! usuall => \int x^p = [x^{p+1} / (p+1)]_xmin^{xmax}
    !! now we have:
    !!  - index p > 0  
    !!      \int a x^p / x = \int a x^{p-1} = a / p [x^p]_xmin^{xmax}
    !!  - 0th order    
    !!      \int alpha/x = alpha [ln(x)]_xmin^{xmax}
    intval = 0.0
    do l=2, pol%size_coeff
      !! current order given by l-1
      power  = l-1
      intval = intval + dble(pol%coeff_pol(l) / dble(power) *  &
               !! integration : between xmax and xmin
               (node_coo(1,2)**(power) - node_coo(1,1)**power))
    end do
    !! 0th order term
    intval = intval + dble(pol%coeff_pol(1) *                       &
             (log(node_coo(1,2)) - log(node_coo(1,1))))

    !! check limit
    if(ieee_is_nan(intval) .or. intval > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: the integration gives NaN or Infinity "//&
                      "[eval_integral_pol_over_x_1D]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.   
    end if
    
    return
  end subroutine eval_integral_pol_over_x_1D_real8

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Evaluate the integral on the given subdomain 
  !>
  !> @param[in]    pol             : polynomial to integrate
  !> @param[in]    node_coo        : domain of integration
  !> @param[out]   intval          : integral value
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine eval_integral_pol_1D_real8(pol,intval,node_coo,ctx_err)

    implicit none

    type(t_polynomial)              ,intent(in)    :: pol
    real(kind=8)      ,allocatable  ,intent(in)    :: node_coo(:,:)
    real(kind=8)                    ,intent(out)   :: intval
    type(t_error)                   ,intent(inout) :: ctx_err
    !! local 
    integer                          :: l,power
    !! -----------------------------------------------------------------
    
    ctx_err%ierr=0

    !! loop over all power of x,
    !! usuall => \int x^p = [x^{p+1} / (p+1)]_xmin^{xmax}
    intval = 0.0
    do l=1, pol%size_coeff
      !! current order given by l-1
      power  = l-1
      intval = intval + dble(pol%coeff_pol(l) / dble(power+1) *  &
               !! integration : between xmax and xmin
               (node_coo(1,2)**(power+1) - node_coo(1,1)**(power+1)))
    end do

    !! check limit
    if(ieee_is_nan(intval) .or. intval > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: the integration gives NaN or Infinity "//&
                      "[eval_integral_pol_1D]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.   
    end if
    
    return
  end subroutine eval_integral_pol_1D_real8

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Evaluate the integral on the given subdomain of the 
  !> polynomial times \f$x\f$
  !>
  !> @param[in]    pol             : polynomial to integrate
  !> @param[in]    node_coo        : domain of integration
  !> @param[out]   intval          : integral value
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine eval_integral_pol_times_x_1D_real8(pol,intval,node_coo,ctx_err)

    implicit none

    type(t_polynomial)              ,intent(in)    :: pol
    real(kind=8)      ,allocatable  ,intent(in)    :: node_coo(:,:)
    real(kind=8)                    ,intent(out)   :: intval
    type(t_error)                   ,intent(inout) :: ctx_err
    !! local 
    integer                          :: l,power
    !! -----------------------------------------------------------------
    
    ctx_err%ierr=0

    !! loop over all power of x,
    !! usuall => \int x^p = [x^{p+1} / (p+1)]_xmin^{xmax}
    !! here we have 
    !! \int x^{p+1} 
    intval = 0.0
    do l=1, pol%size_coeff
      !! current order given by l-1
      power  = l-1
      intval = intval + dble(pol%coeff_pol(l) / dble(power+2) *  &
               !! integration : between xmax and xmin
               (node_coo(1,2)**(power+2) - node_coo(1,1)**(power+2)))
    end do

    !! check limit
    if(ieee_is_nan(intval) .or. intval > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: the integration gives NaN or Infinity "//&
                      "[eval_integral_pol_1D]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.   
    end if
    
    return
  end subroutine eval_integral_pol_times_x_1D_real8

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Evaluate the integral on the given subdomain of the 
  !> polynomial times \f$x^2\f$
  !>
  !> @param[in]    pol             : polynomial to integrate
  !> @param[in]    node_coo        : domain of integration
  !> @param[out]   intval          : integral value
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine eval_integral_pol_times_x2_1D_real8(pol,intval,node_coo,ctx_err)

    implicit none

    type(t_polynomial)              ,intent(in)    :: pol
    real(kind=8)      ,allocatable  ,intent(in)    :: node_coo(:,:)
    real(kind=8)                    ,intent(out)   :: intval
    type(t_error)                   ,intent(inout) :: ctx_err
    !! local 
    integer                          :: l,power
    !! -----------------------------------------------------------------
    
    ctx_err%ierr=0

    !! loop over all power of x,
    !! usuall => \int x^p = [x^{p+1} / (p+1)]_xmin^{xmax}
    !! here we have 
    !! \int x^{p+2} 
    intval = 0.0
    do l=1, pol%size_coeff
      !! current order given by l-1
      power  = l-1
      intval = intval + dble(pol%coeff_pol(l) / dble(power+3) *  &
               !! integration : between xmax and xmin
               (node_coo(1,2)**(power+3) - node_coo(1,1)**(power+3)))
    end do

    !! check limit
    if(ieee_is_nan(intval) .or. intval > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: the integration gives NaN or Infinity "//&
                      "[eval_integral_pol_1D]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.   
    end if
    
    return
  end subroutine eval_integral_pol_times_x2_1D_real8

end module m_integration_local_element
