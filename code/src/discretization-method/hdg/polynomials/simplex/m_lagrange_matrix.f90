!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_lagrange_matrix.f90
!
!> @author
!> J. Diaz [Inria Magique 3D]
!
! DESCRIPTION:
!> @brief
!> the module is used to define the type and routines to 
!> compute coefficients for variational formulations
!> It comes from J.Diaz body of work in software [HOU10NI]
!
!------------------------------------------------------------------------------
module m_lagrange_matrix

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,     only: raise_error, t_error
  use m_define_precision,only: RKIND_POL
  use m_polynomial,      only: t_polynomial, polynomial_integrate,              &
                               t_polynomial_precomput,polynomial_multiplication,&
                               polynomial_precomput_construct,                  &
                               polynomial_precomput_clean
  use m_basis_functions, only: t_basis
  !! -------------------------------------------------------------------
  
  implicit none
  
  private  

  public  :: t_lagrange_vol, t_lagrange_surf, t_lagrange_surf_neigh,           &
             lagrange_vol_clean, lagrange_surf_clean, lagrange_surfneigh_clean,&
             lagrange_vol_construct, lagrange_surf_construct,                  &
             lagrange_surfneigh_construct

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> type for Lagrange Volume matrix:
  !> \f$ \int \partial^{oi} \phi_i \partial^{oj} \phi_j d\Omega\f$
  !> where \f$\Omega\f$ is the simplex reference element: triangle in 2d and 
  !> tetrahedron in 3d.
  !> It coincides with HOU10NI type t_dphiidphij
  !
  !! --------------------------------------------------------------------------
  !> n_phi              : Number of basis functions (ndof)
  !> d_order            : order of the derivative
  !> n_derivative       : number of derivative
  !> coeff_int(i,j,k,l) = \f$\int 
  !>                      \frac{\partial^{d_{order_i}} {\phi_k}}{ \partial x_i}
  !>                      \frac{\partial^{d_{order_j}} {\phi_l}}{ \partial x_j}\f$
  !! --------------------------------------------------------------------------
  type t_lagrange_vol
    integer                           :: n_phi
    integer                           :: d_order_i
    integer                           :: d_order_j
    integer                           :: n_derivative_i
    integer                           :: n_derivative_j
    !! coeff_int(i,j,k,l) = \f$\int 
    !!                      \frac{\partial^{d_{order_i}} {\phi_k}}{ \partial x_i}
    !!                      \frac{\partial^{d_{order_j}} {\phi_l}}{ \partial x_j}\f$
    real(kind=RKIND_POL), allocatable :: coeff_int(:,:,:,:)
  end type t_lagrange_vol

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> type for Lagrange matrix on the face of reference element:
  !> \f$ \int_f \partial^{oi} \phi_i \partial^{oj} \phi_j d\Omega\f$
  !> where \f$\Omega\f$ is the simplex reference element: triangle in 2d and 
  !> tetrahedron in 3d.
  !> It coincides with hou10ni type t_dphiidphij_surf
  !
  !! --------------------------------------------------------------------------
  !> n_phi              : Number of basis functions (ndof)
  !> d_order            : order of the derivative
  !> n_derivative       : number of derivative
  !> coeff_int(i,j,k,l) = \f$ \int_{f}
  !>                      \frac{\partial^{d_{order_i}} \phi_k}{ \partial x_i}
  !>                      \frac{\partial^{d_{order_j}} \phi_l}{ \partial x_j}\f$
  !! --------------------------------------------------------------------------
  type t_lagrange_surf
    integer                           :: n_phi_i
    integer                           :: n_phi_j
    integer                           :: d_order_i
    integer                           :: d_order_j
    integer                           :: n_derivative_i
    integer                           :: n_derivative_j
    real(kind=RKIND_POL), allocatable :: coeff_int(:,:,:,:,:)
  end type t_lagrange_surf

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> type for Lagrange matrix on the face of reference element:
  !> \f$ \int_{f} \partial^{oi} \phi_i \partial^{oj} \phi_j d\Omega\f$
  !> where \f$\Omega\f$ is the simplex reference element: triangle in 2d and 
  !> tetrahedron in 3d.
  !> It coincides with HOU10NI type t_dphiidphij_surf_neigh
  !
  !! --------------------------------------------------------------------------
  !> n_phi              : Number of basis functions (ndof)
  !> d_order            : order of the derivative
  !> n_derivative       : number of derivative
  !> coeff_int(i,j,k,l,face_loc,face_neigh,case) =
  !>      \f$\int_{floc,fneigh,case} 
  !>                      \frac{\partial^{d_{order i}}\phi_k}{\partial x_i}
  !>                      \frac{\partial^{d_{order j}}\phi_l}{\partial x_j}\f$
  !> 1 case in 1D ; 1 case in 2D ; 3 case in 3D 
  !! --------------------------------------------------------------------------
  type t_lagrange_surf_neigh

    integer                           :: n_phi_i
    integer                           :: n_phi_neigh
    integer                           :: d_order_i
    integer                           :: d_order_j
    integer                           :: n_derivative_i
    integer                           :: n_derivative_j
    real(kind=RKIND_POL), allocatable :: coeff_int(:,:,:,:,:,:,:)
  end type t_lagrange_surf_neigh

  !! interface to init the array
  interface lagrange_surf_construct
    module procedure lagrange_surf_construct_1basis
    module procedure lagrange_surf_construct_2basis_full
    module procedure lagrange_surf_construct_2basis_part
  end interface lagrange_surf_construct

  !! interface to init the array
  interface lagrange_vol_construct
    module procedure lagrange_vol_construct_full
    module procedure lagrange_vol_construct_part
  end interface lagrange_vol_construct
  
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>  clean Lagrange volume matrix informations
  !
  !! --------------------------------------------------------------------------
  subroutine lagrange_vol_clean(mat_vol)
    type(t_lagrange_vol) :: mat_vol
    
    if(allocated(mat_vol%coeff_int)) deallocate(mat_vol%coeff_int)
    mat_vol%d_order_i     =0
    mat_vol%d_order_j     =0
    mat_vol%n_derivative_i=0
    mat_vol%n_derivative_j=0
    mat_vol%n_phi         =0
    return 
  end subroutine lagrange_vol_clean
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>  clean Lagrange face matrix informations
  !
  !! --------------------------------------------------------------------------
  subroutine lagrange_surf_clean(mat_surf)
    type(t_lagrange_surf) :: mat_surf
    
    if(allocated(mat_surf%coeff_int)) deallocate(mat_surf%coeff_int)
    mat_surf%d_order_i     =0
    mat_surf%d_order_j     =0
    mat_surf%n_derivative_i=0
    mat_surf%n_derivative_j=0
    mat_surf%n_phi_i       =0
    mat_surf%n_phi_j       =0
    return 
  end subroutine lagrange_surf_clean
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>  clean Lagrange neigh face matrix informations
  !
  !! --------------------------------------------------------------------------
  subroutine lagrange_surfneigh_clean(mat_surf)
    type(t_lagrange_surf_neigh) :: mat_surf
    
    if(allocated(mat_surf%coeff_int)) deallocate(mat_surf%coeff_int)
    mat_surf%d_order_i     =0
    mat_surf%d_order_j     =0
    mat_surf%n_derivative_i=0
    mat_surf%n_derivative_j=0
    mat_surf%n_phi_i       =0
    mat_surf%n_phi_neigh   =0
    return 
  end subroutine lagrange_surfneigh_clean

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>  initialize Lagrange volume matrix informations
  !  
  !! --------------------------------------------------------------------------
  subroutine lagrange_vol_construct_full(basis,mat,order1,order2,ctx_err,o_coe)
  
    implicit none  
    integer                              ,intent(in)    :: order1, order2
    type(t_basis)                        ,intent(in)    :: basis
    type(t_lagrange_vol)                 ,intent(inout) :: mat
    type(t_error)                        ,intent(inout) :: ctx_err
    type(t_polynomial_precomput),optional,intent(in)    :: o_coe(3)
    !! local     
    type(t_polynomial_precomput)   :: precomp(3)
    type(t_polynomial)             :: ptmp
    real(kind=RKIND_POL)           :: val
    integer                        :: I,J,K,L

    ctx_err%ierr=0
    !! precompute or not
    if(present(o_coe)) then
      precomp(:)   = o_coe(:)
    else 
      call polynomial_precomput_construct(precomp,basis%dim_domain,     &
                                          basis%degree,ctx_err)
    end if
    
    mat%d_order_i=order1
    mat%d_order_j=order2
    mat%n_phi    =basis%nphi
    select case(basis%dim_domain) 
      case(1)
         mat%n_derivative_i=1
         mat%n_derivative_j=1
      case(2)
         mat%n_derivative_i=order1+1
         mat%n_derivative_j=order2+1
      case(3)
         mat%n_derivative_i=(order1+1)*(order1+2)/2
         mat%n_derivative_j=(order2+1)*(order2+2)/2
      case default
         ctx_err%msg = "** ERROR: Incorrect dimension [lagrange_vol_const] **"
         ctx_err%ierr  = -1
         ctx_err%critic=.true.
         call raise_error(ctx_err)
    end select
    
    allocate(mat%coeff_int(mat%n_derivative_i,mat%n_derivative_j,       &
                           basis%nphi,basis%nphi))
    mat%coeff_int = 0.d0
    
    !! -----------------------------------------------------------------
    if(order1 .eq. 0) then
      if(order2 .eq. 0) then
        do I=1,basis%nphi
          do J=1,basis%nphi
            call polynomial_multiplication(precomp,basis%pol(I),        &
                                           basis%pol(J),ptmp,ctx_err)
            call polynomial_integrate(precomp,ptmp,val,ctx_err)
            mat%coeff_int(1,1,I,J)=val
          enddo
        enddo
      else
        do K=1,mat%n_derivative_j
          do I=1,basis%nphi
            do J=1,basis%nphi
              call polynomial_multiplication(precomp,basis%pol(I),      &
                                    basis%pol_derivative(order2)%p(J,K),&
                                    ptmp,ctx_err)
              call polynomial_integrate(precomp,ptmp,val,ctx_err)
              mat%coeff_int(1,K,I,J)=val
            enddo
          enddo
        end do
      end if
    else if (order1 > 0) then
      do L=1,mat%n_derivative_i
        do K=1,mat%n_derivative_j
          do I=1,basis%nphi
            do J=1,basis%nphi
              call polynomial_multiplication(precomp,                       &
                                      basis%pol_derivative(order1)%p(I,L),  &
                                      basis%pol_derivative(order2)%p(J,K),  &
                                      ptmp,ctx_err)
              call polynomial_integrate(precomp,ptmp,val,ctx_err)
              mat%coeff_int(L,K,I,J)=val
            enddo
          enddo
        end do
      end do
    else 
      ctx_err%msg = "** ERROR: negative order [lagrange_vol_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if
    
    call polynomial_precomput_clean(precomp)

    return
  end subroutine lagrange_vol_construct_full

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>  initialize Lagrange volume matrix informations
  !  
  !! --------------------------------------------------------------------------
  subroutine lagrange_vol_construct_part(basis,mat,order1,order2,       &
                                         list_ind,nind,ctx_err,o_coe)
  
    implicit none  
    integer                              ,intent(in)    :: order1, order2
    type(t_basis)                        ,intent(in)    :: basis
    type(t_lagrange_vol)                 ,intent(inout) :: mat
    integer                              ,intent(in)    :: list_ind(:,:)
    integer                              ,intent(in)    :: nind
    type(t_error)                        ,intent(inout) :: ctx_err
    type(t_polynomial_precomput),optional,intent(in)    :: o_coe(3)
    !! local     
    type(t_polynomial_precomput)   :: precomp(3)
    type(t_polynomial)             :: ptmp
    real(kind=RKIND_POL)           :: val
    integer                        :: I,J,K,L,ind

    ctx_err%ierr=0
   
    mat%d_order_i=order1
    mat%d_order_j=order2
    mat%n_phi    =basis%nphi
    select case(basis%dim_domain) 
      case(1)
         mat%n_derivative_i=1
         mat%n_derivative_j=1
      case(2)
         mat%n_derivative_i=order1+1
         mat%n_derivative_j=order2+1
      case(3)
         mat%n_derivative_i=(order1+1)*(order1+2)/2
         mat%n_derivative_j=(order2+1)*(order2+2)/2
      case default
         ctx_err%msg = "** ERROR: Incorrect dimension [lagrange_vol_const] **"
         ctx_err%ierr  = -1
         ctx_err%critic=.true.
         call raise_error(ctx_err)
    end select
    
    allocate(mat%coeff_int(mat%n_derivative_i,mat%n_derivative_j,       &
                           basis%nphi,basis%nphi))
    mat%coeff_int = 0.d0
    
    !! in the case where nind <= 0, we leave now.
    if(nind <= 0) then
      return
    endif

    !! precompute or not
    if(present(o_coe)) then
      precomp(:)   = o_coe(:)
    else 
      call polynomial_precomput_construct(precomp,basis%dim_domain,     &
                                          basis%degree,ctx_err)
    end if
    
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(ind,I,J,K,L,val,ptmp)
    if(order1 .eq. 0) then
      if(order2 .eq. 0) then
        !! only loop over the given indexes
        !$OMP DO
        do ind=1,nind
          I=list_ind(1,ind)
          J=list_ind(2,ind)
          call polynomial_multiplication(precomp,basis%pol(I),          &
                                         basis%pol(J),ptmp,ctx_err)
          call polynomial_integrate(precomp,ptmp,val,ctx_err)
          mat%coeff_int(1,1,I,J)=val
        enddo
        !$OMP END DO
      else
        !$OMP DO
        do ind=1,nind
          I=list_ind(1,ind)
          J=list_ind(2,ind)
          do K=1,mat%n_derivative_j
            call polynomial_multiplication(precomp,basis%pol(I),        &
                                    basis%pol_derivative(order2)%p(J,K),&
                                    ptmp,ctx_err)
            call polynomial_integrate(precomp,ptmp,val,ctx_err)
            mat%coeff_int(1,K,I,J)=val
          enddo
        end do
        !$OMP END DO
      end if
    else if (order1 > 0) then
      !$OMP DO
      do ind=1,nind
        I=list_ind(1,ind)
        J=list_ind(2,ind)
        do L=1,mat%n_derivative_i
          do K=1,mat%n_derivative_j
            call polynomial_multiplication(precomp,                     &
                                    basis%pol_derivative(order1)%p(I,L),&
                                    basis%pol_derivative(order2)%p(J,K),&
                                    ptmp,ctx_err)
            call polynomial_integrate(precomp,ptmp,val,ctx_err)
            mat%coeff_int(L,K,I,J)=val
          enddo
        end do
      end do
      !$OMP END DO
    else 
      ctx_err%msg = "** ERROR: negative order [lagrange_vol_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !$OMP END PARALLEL
    
    call polynomial_precomput_clean(precomp)

    return
  end subroutine lagrange_vol_construct_part
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !> compute lagrange on the surface 
  !
  !! --------------------------------------------------------------------------
  subroutine lagrange_surf_construct_1basis(basis,mat,order1,order2,ctx_err,o_coe)
    implicit none
    
    type(t_basis)                        ,intent(in)    :: basis
    integer                              ,intent(in)    :: order1,order2
    type(t_lagrange_surf)                ,intent(inout) :: mat   
    type(t_error)                        ,intent(inout) :: ctx_err
    type(t_polynomial_precomput),optional,intent(in)    :: o_coe(3)
    !! local
    type(t_polynomial_precomput)                        :: precomp(3)
    real(kind=RKIND_POL) :: val
    integer :: I,J,K,L,Face
    type(t_polynomial) :: ptmp

    ctx_err%ierr=0
    !! precompute or not
    if(present(o_coe)) then
      precomp(:)   = o_coe(:)
    else 
      call polynomial_precomput_construct(precomp,basis%dim_domain,     &
                                          basis%degree,ctx_err)
    end if

    mat%d_order_i=order1
    mat%d_order_j=order2
    mat%n_phi_i  =basis%nphi
    mat%n_phi_j  =basis%nphi
    SELECT CASE(basis%dim_domain) 
    CASE(1)
       mat%n_derivative_i=1
       mat%n_derivative_j=1
    CASE(2)
       mat%n_derivative_i=order1+1
       mat%n_derivative_j=order2+1
    CASE(3)
       mat%n_derivative_i=(order1+1)*(order1+2)/2
       mat%n_derivative_j=(order2+1)*(order2+2)/2
    end SELECT
    allocate(mat%coeff_int(mat%n_derivative_i,mat%n_derivative_j, &
                           basis%nphi,basis%nphi,basis%dim_domain+1))
    mat%coeff_int = 0.d0
    
    IF(order1.EQ.0) THEN
       IF(order2.EQ.0) THEN
          do face=1,basis%dim_domain+1
             do I=1,basis%nphi
                do J=1,basis%nphi
                   call polynomial_multiplication(precomp,basis%pol_face(I,Face), &
                                   basis%pol_face(J,Face),ptmp,ctx_err)
                   call polynomial_integrate(precomp,ptmp,val,ctx_err)
                   mat%coeff_int(1,1,I,J,Face)=val
                enddo
             enddo
          end do
       else
          do face=1,basis%dim_domain+1
             do K=1,mat%n_derivative_j
                do I=1,basis%nphi
                   do J=1,basis%nphi
                      call polynomial_multiplication(precomp,basis%pol_face(I,Face), &
                                      basis%pol_face_derivative(order2,Face)%p(J,K), &
                                      ptmp,ctx_err)
                      call polynomial_integrate(precomp,ptmp,val,ctx_err)
                      mat%coeff_int(1,K,I,J,Face)=val
                   enddo
                enddo
             end do
          end do
       end if
    else
       IF(order2.EQ.0) THEN
          do face=1,basis%dim_domain+1
             do K=1,mat%n_derivative_i
                do I=1,basis%nphi
                   do J=1,basis%nphi
                      call polynomial_multiplication(precomp,basis%pol_face(J,Face),basis &
                           &%pol_face_derivative(order1,Face)%p(I,K),ptmp,ctx_err)
                      call polynomial_integrate(precomp,ptmp,val,ctx_err)
                      mat%coeff_int(K,1,I,J,Face)=val
                   enddo
                enddo
             end do
          end do
       else
          do face=1,basis%dim_domain+1
             do L=1,mat%n_derivative_i
                do K=1,mat%n_derivative_j
                   do I=1,basis%nphi
                      do J=1,basis%nphi
                         call polynomial_multiplication(precomp, &
                                         basis%pol_face_derivative(order1,Face)%p(I &
                              &,L),basis %pol_face_derivative(order2,Face)%p(J,K) &
                              &,ptmp,ctx_err)
                         call polynomial_integrate(precomp,ptmp,val,ctx_err)
                         mat%coeff_int(L,K,I,J,Face)=val
                      enddo
                   end do
                end do
             end do
          end do
       end if
    end if
    
    call polynomial_precomput_clean(precomp)
    return

  end subroutine lagrange_surf_construct_1basis
  !! --------------------------------------------------------------------------

  !! -------------------------------------------------------------------
  subroutine lagrange_surf_construct_2basis_full(basis1,basis2,mat,     &
                                                 order1,order2,ctx_err)
    implicit none
    
    type(t_basis)                        ,intent(in)    :: basis1
    type(t_basis)                        ,intent(in)    :: basis2
    integer                              ,intent(in)    :: order1,order2
    type(t_lagrange_surf)                ,intent(inout) :: mat   
    type(t_error)                        ,intent(inout) :: ctx_err
    !! local
    type(t_polynomial_precomput)    :: precomp(3)
    real(kind=RKIND_POL)            :: val
    integer :: I,J,K,L,Face
    type(t_polynomial) :: ptmp

    ctx_err%ierr=0
    
    if(basis1%dim_domain .ne. basis2%dim_domain) then
      ctx_err%msg = "** ERROR: basis must have same dimension " // &
                    " [lagrange_surf_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(basis1%degree < 0 .or. basis2%degree < 0) then
      ctx_err%msg = "** ERROR: basis must have degrees > 0 " //    &
                    " [lagrange_surf_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! we need all dimensions ?
    do I=1,basis1%dim_domain
      call polynomial_precomput_construct(precomp,I,                    &
                                       max(basis1%degree,basis2%degree),&
                                       ctx_err)
    end do

    mat%d_order_i=order1
    mat%d_order_j=order2
    mat%n_phi_i  =basis1%nphi
    mat%n_phi_j  =basis2%nphi
    SELECT CASE(basis1%dim_domain) 
    CASE(1)
       mat%n_derivative_i=1
       mat%n_derivative_j=1
    CASE(2)
       mat%n_derivative_i=order1+1
       mat%n_derivative_j=order2+1
    CASE(3)
       mat%n_derivative_i=(order1+1)*(order1+2)/2
       mat%n_derivative_j=(order2+1)*(order2+2)/2
    end SELECT
    allocate(mat%coeff_int(mat%n_derivative_i,mat%n_derivative_j, &
                           basis1%nphi,basis2%nphi,basis1%dim_domain+1))
    mat%coeff_int = 0.d0
    

    !! 4 possiblities
    !! -----------------------------------------------------------------
    IF(order1.EQ.0 .and. order2.EQ.0) THEN
      do face=1,basis1%dim_domain+1
        do I=1,basis1%nphi
          do J=1,basis2%nphi
            call polynomial_multiplication(precomp,basis1%pol_face(I,Face), &
                                                   basis2%pol_face(J,Face), &
                                           ptmp,ctx_err)
            call polynomial_integrate(precomp,ptmp,val,ctx_err)
            mat%coeff_int(1,1,I,J,Face)=val
          enddo
        enddo
      end do
    !! -----------------------------------------------------------------  
    else IF (order1.EQ.0 .and. order2 > 0) THEN
      do face=1,basis1%dim_domain+1
         do K=1,mat%n_derivative_j
            do I=1,basis1%nphi
               do J=1,basis2%nphi
                  call polynomial_multiplication(precomp,basis1%pol_face(I,Face), &
                                  basis2%pol_face_derivative(order2,Face)%p(J,K), &
                                  ptmp,ctx_err)
                  call polynomial_integrate(precomp,ptmp,val,ctx_err)
                  mat%coeff_int(1,K,I,J,Face)=val
               enddo
            enddo
         end do
      end do    
    !! -----------------------------------------------------------------
    else IF (order2.EQ.0 .and. order1 > 0) THEN
      do face=1,basis1%dim_domain+1
         do K=1,mat%n_derivative_i
            do I=1,basis1%nphi
               do J=1,basis2%nphi
                  call polynomial_multiplication(precomp,basis2%pol_face(J,Face), &
                        basis1%pol_face_derivative(order1,Face)%p(I,K),ptmp,ctx_err)
                  call polynomial_integrate(precomp,ptmp,val,ctx_err)
                  mat%coeff_int(K,1,I,J,Face)=val
               enddo
            enddo
         end do
      end do
    !! -----------------------------------------------------------------
    else IF (order1 > 0 .and. order2 > 0)  THEN
      do face=1,basis1%dim_domain+1
         do L=1,mat%n_derivative_i
            do K=1,mat%n_derivative_j
               do I=1,basis1%nphi
                  do J=1,basis2%nphi
                     call polynomial_multiplication(precomp, &
                          basis1%pol_face_derivative(order1,Face)%p(I,L), &
                          basis2%pol_face_derivative(order2,Face)%p(J,K), &
                          ptmp,ctx_err)
                     call polynomial_integrate(precomp,ptmp,val,ctx_err)
                     mat%coeff_int(L,K,I,J,Face)=val
                  enddo
               end do
            end do
         end do
      end do   
    !! -----------------------------------------------------------------
    else
      ctx_err%msg = "** ERROR: order < 0 [lagrange_surf_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    call polynomial_precomput_clean(precomp)
    return

  end subroutine lagrange_surf_construct_2basis_full
  !! --------------------------------------------------------------------------

  !! -------------------------------------------------------------------
  subroutine lagrange_surf_construct_2basis_part(basis1,basis2,mat,     &
                                                 order1,order2,list_ind,&
                                                 nind,ctx_err)
    implicit none
    
    type(t_basis)                        ,intent(in)    :: basis1
    type(t_basis)                        ,intent(in)    :: basis2
    integer                              ,intent(in)    :: order1,order2
    type(t_lagrange_surf)                ,intent(inout) :: mat  
    integer                              ,intent(in)    :: list_ind(:,:)
    integer                              ,intent(in)    :: nind 
    type(t_error)                        ,intent(inout) :: ctx_err
    !! local
    type(t_polynomial_precomput)    :: precomp(3)
    real(kind=RKIND_POL)            :: val
    integer :: I,J,K,L,face,ind
    type(t_polynomial) :: ptmp

    ctx_err%ierr=0
    
    if(basis1%dim_domain .ne. basis2%dim_domain) then
      ctx_err%msg = "** ERROR: basis must have same dimension " // &
                    " [lagrange_surf_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(basis1%degree < 0 .or. basis2%degree < 0) then
      ctx_err%msg = "** ERROR: basis must have degrees > 0 " //    &
                    " [lagrange_surf_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    mat%d_order_i=order1
    mat%d_order_j=order2
    mat%n_phi_i  =basis1%nphi
    mat%n_phi_j  =basis2%nphi
    SELECT CASE(basis1%dim_domain) 
    CASE(1)
       mat%n_derivative_i=1
       mat%n_derivative_j=1
    CASE(2)
       mat%n_derivative_i=order1+1
       mat%n_derivative_j=order2+1
    CASE(3)
       mat%n_derivative_i=(order1+1)*(order1+2)/2
       mat%n_derivative_j=(order2+1)*(order2+2)/2
    end SELECT
    allocate(mat%coeff_int(mat%n_derivative_i,mat%n_derivative_j, &
                           basis1%nphi,basis2%nphi,basis1%dim_domain+1))
    mat%coeff_int = 0.d0

    !! in case nothing to do.
    if(nind <= 0) return
    
    !! we need all dimensions ?
    do I=1,basis1%dim_domain
      call polynomial_precomput_construct(precomp,I,                    &
                                       max(basis1%degree,basis2%degree),&
                                       ctx_err)
    end do

    !! 4 possiblities
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(ind,I,J,K,L,face,val,ptmp)

    IF(order1.EQ.0 .and. order2.EQ.0) THEN
      !$OMP DO
      do ind=1,nind
        I=list_ind(1,ind)
        J=list_ind(2,ind)
        do face=1,basis1%dim_domain+1
          call polynomial_multiplication(precomp,basis1%pol_face(I,Face), &
                                                 basis2%pol_face(J,Face), &
                                         ptmp,ctx_err)
          call polynomial_integrate(precomp,ptmp,val,ctx_err)
          mat%coeff_int(1,1,I,J,Face)=val
        enddo
      end do
      !$OMP END DO

    !! -----------------------------------------------------------------  
    else IF (order1.EQ.0 .and. order2 > 0) THEN
      !$OMP DO
      do ind=1,nind
        I=list_ind(1,ind)
        J=list_ind(2,ind)
        do face=1,basis1%dim_domain+1
          do K=1,mat%n_derivative_j
            call polynomial_multiplication(precomp,basis1%pol_face(I,Face), &
                            basis2%pol_face_derivative(order2,Face)%p(J,K), &
                            ptmp,ctx_err)
             call polynomial_integrate(precomp,ptmp,val,ctx_err)
             mat%coeff_int(1,K,I,J,Face)=val
          enddo
        end do
      end do
      !$OMP END DO
    !! -----------------------------------------------------------------
    else IF (order2.EQ.0 .and. order1 > 0) THEN
      !$OMP DO
      do ind=1,nind
        I=list_ind(1,ind)
        J=list_ind(2,ind)
        do face=1,basis1%dim_domain+1
          do K=1,mat%n_derivative_i
             call polynomial_multiplication(precomp,basis2%pol_face(J,Face), &
                   basis1%pol_face_derivative(order1,Face)%p(I,K),ptmp,ctx_err)
             call polynomial_integrate(precomp,ptmp,val,ctx_err)
             mat%coeff_int(K,1,I,J,Face)=val
           enddo
         end do
      end do
      !$OMP END DO
    !! -----------------------------------------------------------------
    else IF (order1 > 0 .and. order2 > 0)  THEN
      !$OMP DO
      do ind=1,nind
        I=list_ind(1,ind)
        J=list_ind(2,ind)
        do face=1,basis1%dim_domain+1
          do L=1,mat%n_derivative_i
            do K=1,mat%n_derivative_j
              call polynomial_multiplication(precomp, &
                     basis1%pol_face_derivative(order1,Face)%p(I,L), &
                     basis2%pol_face_derivative(order2,Face)%p(J,K), &
                     ptmp,ctx_err)
                call polynomial_integrate(precomp,ptmp,val,ctx_err)
                mat%coeff_int(L,K,I,J,Face)=val
              end do
            end do
         end do
      end do  
      !$OMP END DO 
    !! -----------------------------------------------------------------
    else
      ctx_err%msg = "** ERROR: order < 0 [lagrange_surf_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !$OMP END PARALLEL

    call polynomial_precomput_clean(precomp)
    return

  end subroutine lagrange_surf_construct_2basis_part
  !! --------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !> compute largrange on the surface neighbor
  !
  !! --------------------------------------------------------------------------
  subroutine lagrange_surfneigh_construct(basis1,basis2,mat,order1,order2,ctx_err,o_coe)
    implicit none
    
    type(t_basis)                        ,intent(in)    :: basis1
    type(t_basis)                        ,intent(in)    :: basis2
    integer                              ,intent(in)    :: order1,order2
    type(t_error)                        ,intent(inout) :: ctx_err
    type(t_polynomial_precomput),optional,intent(in)    :: o_coe(3)
    type(t_lagrange_surf_neigh),intent(inout) :: mat
    !! local    
    type(t_polynomial_precomput)                        :: precomp(3)
    real(kind=RKIND_POL) :: val
    integer :: I,J,K,L,Face1,face2,CASE,nbcase
    type(t_polynomial) :: ptmp

    ctx_err%ierr=0
    !! precompute or not
    if(basis1%dim_domain .ne. basis2%dim_domain) then
      ctx_err%msg = "** ERROR: basis do not have same domain dimension " // &
                    " in [lagrange_surfneigh_construct] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    end if

    if(present(o_coe)) then
      precomp(:)   = o_coe(:)
    else 
      call polynomial_precomput_construct(precomp,basis1%dim_domain,    &
                                          basis1%degree,ctx_err)
    end if
    
    mat%d_order_i  =order1
    mat%d_order_j  =order2
    mat%n_phi_i    =basis1%nphi
    mat%n_phi_neigh=basis2%nphi
    nbcase=0
    select case(basis1%dim_domain) 
    case(1)
       mat%n_derivative_i=1
       mat%n_derivative_j=1
       nbcase=1
    case(2)
       mat%n_derivative_i=order1+1
       mat%n_derivative_j=order2+1
       nbcase=1
    case(3)
       mat%n_derivative_i=(order1+1)*(order1+2)/2
       mat%n_derivative_j=(order2+1)*(order2+2)/2
       nbcase=3
    end select

    allocate(mat%coeff_int(mat%n_derivative_i,mat%n_derivative_j,basis1%nphi,basis2%nphi,basis1%dim_domain&
         & +1 ,basis2%dim_domain+1,nbcase))

    if(order1.EQ.0) THEN
       if(order2.EQ.0) THEN
          do case=1,nbcase
             do face1=1,basis1%dim_domain+1
                do face2=1,basis2%dim_domain+1
                   do I=1,basis1%nphi
                      do J=1,basis2%nphi
                         call polynomial_multiplication(precomp,basis1%pol_face(I,Face1),basis2 &
                              &%pol_face_neigh(J ,Face2,CASE),ptmp,ctx_err)    
                         
                         call polynomial_integrate(precomp,ptmp,val,ctx_err)
                         mat%coeff_int(1,1,I,J,Face1,Face2,CASE)=val
                      enddo
                   enddo
                end do
             end do
          end do
       else
          do case=1,nbcase
             do face1=1,basis1%dim_domain+1
                do face2=1,basis2%dim_domain+1
                   do K=1,mat%n_derivative_j
                      do I=1,basis1%nphi
                         do J=1,basis2%nphi
                            call polynomial_multiplication(precomp,basis1%pol_face(I,Face1),basis2 &
                                 &%pol_face_neigh_derivative(order2,Face2,CASE)%p(J &
                                 &,K),ptmp,ctx_err)
                            call polynomial_integrate(precomp,ptmp,val,ctx_err)
                            mat%coeff_int(1,K,I,J,Face1,Face2,CASE)=val
                         enddo
                      enddo
                   end do
                end do
             end do
          end do
       end if
    else
       IF(order2.EQ.0) THEN
          do case=1,nbcase
             do face1=1,basis1%dim_domain+1
                do face2=1,basis2%dim_domain+1
                   do I=1,basis1%nphi
                      do J=1,basis2%nphi
                         do K=1,mat%n_derivative_i
                            call polynomial_multiplication(precomp,basis2%pol_face(J,Face2),basis1 &
                                 &%pol_face_neigh_derivative(order1,Face1,CASE)%p(I &
                                 &,K),ptmp,ctx_err)
                            call polynomial_integrate(precomp,ptmp,val,ctx_err)
                            mat%coeff_int(K,1,I,J,Face1,Face2,CASE)=val
                         enddo
                      enddo
                   end do
                end do
             end do
          end do
       else
          do case=1,nbcase
             do face1=1,basis1%dim_domain+1
                do face2=1,basis2%dim_domain+1
                   do L=1,mat%n_derivative_i
                      do K=1,mat%n_derivative_j
                         do I=1,basis1%nphi
                            do J=1,basis2%nphi
                               call polynomial_multiplication(precomp,basis1%pol_face_derivative(order1 &
                                    &,Face1)%p(I,L),basis2 &
                                    &%pol_face_neigh_derivative(order2,Face2,CASE) &
                                    &%p(J,K),ptmp,ctx_err)
                               call polynomial_integrate(precomp,ptmp,val,ctx_err)
                               mat%coeff_int(L,K,I,J,Face1,Face2,CASE)=val
                            enddo
                         enddo
                      end do
                   end do
                end do
             end do
          end do
       end if
    end if
    
    call polynomial_precomput_clean(precomp)

    return
  end subroutine lagrange_surfneigh_construct
  !! --------------------------------------------------------------------------
  

end module m_lagrange_matrix
