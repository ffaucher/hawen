!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_basis_functions.f90
!
!> @author
!> J. Diaz [Inria Magique 3D]
!
! DESCRIPTION:
!> @brief
!> the module is used to define the type and routines to 
!> handle basis functions
!> It comes from J.Diaz body of work in software HOU10NI
!
!------------------------------------------------------------------------------
module m_basis_functions

  !! module used -------------------------------------------------------
  use m_raise_error,      only: raise_error, t_error
  use m_define_precision, only : RKIND_POL
  use m_polynomial,  only: t_polynomial,t_polynomial_precomput,             &
                           polynomial_construct, polynomial_multiplication, &
                           polynomial_eval, polynomial_clean,polynomial_dx, &
                           polynomial_dy,polynomial_dz, polynomial_trace,   &
                           polynomial_invert_coo,                           &
                           polynomial_precomput_clean,polynomial_precomput_construct
  
  !! -------------------------------------------------------------------
  implicit none
  
  private  
  public   :: t_basis, basis_construct, basis_clean,                        &
              basis_derivative_construct, basis_face_construct,             &
              basis_face_neigh_construct
  private  :: t_matrix_polynomial, basis_construct_1d, basis_construct_2d,  &
              basis_construct_3d, matrix_polynomial_clean
  
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !> type t_matrix_polynomial
  !
  !! --------------------------------------------------------------------------
  type t_matrix_polynomial
     type(t_polynomial), allocatable :: p(:,:)
  end type t_matrix_polynomial
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>  type t_basis
  !
  !! --------------------------------------------------------------------------
  type t_basis
     !! domain dimension
     integer :: dim_domain
     !! order of the highest polynomial
     integer :: degree
     !! number of basis function
     integer :: nphi
     
     ! The basis functions and derivatives
     ! In 1D, the size of pol_derivative(k+1)%p(:,:) is nphi x 1 
     ! and pol_derivative(k)%p(i,1)=d^k phi_i/dx^k
     ! In 2D, the size of pol_derivative(k+1)%p(:,:) is nphi x (k+1)
     ! and  pol_derivative(k)%p(i,j)=d^k phi_i/dx^(k-j+1)dy^(j-1)
     ! In 3D, the size of pol_derivative(k+1)%p(:,:) is nphi x (k+2)(k+1)/2
     ! and  pol_derivative(k)%p(i,(j-1)j/2+l)=d^k phi_i/dx^(k-j+1)dy^(j-l)dz^(l-1)
     type(t_polynomial)       ,allocatable :: pol(:)
     type(t_matrix_polynomial),allocatable :: pol_derivative(:)

     ! The basis functions on the faces, p(i,j) is phi_i on face j
     ! and the derivatives of the basis functions on the faces
     type(t_polynomial)       ,allocatable :: pol_face(:,:)
     type(t_matrix_polynomial),allocatable :: pol_face_derivative(:,:)

     ! The basis functions on the faces for the neighbourgh, p(i,j) is phi_i on
     ! face j and derivative
     type(t_polynomial)       ,allocatable :: pol_face_neigh(:,:,:)
     type(t_matrix_polynomial),allocatable :: pol_face_neigh_derivative(:,:,:)

  end type t_basis
  !----------------------------------------------------------------------------
  
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>   Construct the basis function
  !
  !! --------------------------------------------------------------------------
  subroutine basis_construct(dim,degree,basis,ctx_err,o_coe)
  
    implicit none  
    integer                          ,intent(in)    :: dim, degree
    type(t_basis)                    ,intent(inout) :: basis
    type(t_error)                    ,intent(inout) :: ctx_err
    type(t_polynomial_precomput),optional,intent(in):: o_coe(3)
    !! local     
    logical                        :: flag_precomp
    type(t_polynomial_precomput)   :: precomp(3)
    
    ctx_err%ierr=0
    !! recompute or not  
    flag_precomp = .false.
    if(present(o_coe)) then
      flag_precomp = .true.
      precomp(:) = o_coe(:)
    end if
    !! depends on the dimension
    select case (dim)
      case(1)
        call basis_construct_1d(precomp,degree,basis,flag_precomp,ctx_err)
      
      case(2)
        call basis_construct_2d(precomp,degree,basis,flag_precomp,ctx_err)
      
      case(3)
        call basis_construct_3d(precomp,degree,basis,flag_precomp,ctx_err)
      
      case default
        ctx_err%msg = "** ERROR: Incorrect dimension [m_basis_construct] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    call polynomial_precomput_clean(precomp)

    return
    
  end subroutine basis_construct

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>   clean the basis function
  !
  !! --------------------------------------------------------------------------
  subroutine basis_clean(basis)
  
    implicit none  
    type(t_basis)   ,intent(inout) :: basis
    integer                        :: i,j,k
    
    !! deallocate polynomials
    if (allocated(basis%pol)) then
      do i = 1, basis%nphi
        call polynomial_clean(basis%pol(i))
      end do
      deallocate(basis%pol)
    endif
    if (allocated(basis%pol_face)) then
      do i = 1, size(basis%pol_face,1)
        do j = 1, size(basis%pol_face,2)
          call polynomial_clean(basis%pol_face(i,j))
        end do
      end do
      deallocate(basis%pol_face)
    endif
    if (allocated(basis%pol_face_neigh)) then
      do i = 1, size(basis%pol_face_neigh,1)
        do j = 1, size(basis%pol_face_neigh,2)
          do k = 1, size(basis%pol_face_neigh,3)
            call polynomial_clean(basis%pol_face_neigh(i,j,k))
          end do
        end do
      end do
      deallocate(basis%pol_face_neigh)
    endif
    !! -----------------------------------------------------------------
    !! deallocate matrix_polynomial
    if (allocated(basis%pol_derivative)) then
      do i=1,size(basis%pol_derivative,1)
        call matrix_polynomial_clean(basis%pol_derivative(i))
      end do
      deallocate(basis%pol_derivative)
    end if
    if (allocated(basis%pol_face_derivative)) then
      do i=1,size(basis%pol_face_derivative,1)
        do j=1,size(basis%pol_face_derivative,2)
          call matrix_polynomial_clean(basis%pol_face_derivative(i,j))
        end do
      end do 
      deallocate(basis%pol_face_derivative)
    end if
    if (allocated(basis%pol_face_neigh_derivative)) then
      do i=1,size(basis%pol_face_neigh_derivative,1)
        do j=1,size(basis%pol_face_neigh_derivative,2)
          do k=1,size(basis%pol_face_neigh_derivative,3)
            call matrix_polynomial_clean(basis%pol_face_neigh_derivative(i,j,k))
          end do
        end do
      end do    
      deallocate(basis%pol_face_neigh_derivative)
    end if
    
    basis%dim_domain=0
    basis%degree    =0
    basis%nphi      =0
    
    return
    
  end subroutine basis_clean

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>   clean the matrix polynomial
  !
  !! --------------------------------------------------------------------------
  subroutine matrix_polynomial_clean(mat_poly)
    implicit none
    type(t_matrix_polynomial), intent(inout) :: mat_poly
    !! local
    integer :: i,j
    
    if(allocated(mat_poly%p)) then
      do i=1,size(mat_poly%p,1)
        do j=1,size(mat_poly%p,2)
          call polynomial_clean(mat_poly%p(i,j))
        end do
      end do
      deallocate(mat_poly%p)    
    end if
    
    return
  end subroutine matrix_polynomial_clean
  
  
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>   Construct the 1D basis function 
  !
  !! --------------------------------------------------------------------------
  subroutine basis_construct_1d(coe,degree,basis,flag_precomp,ctx_err)
  
    implicit none  
    integer                          ,intent(in)    :: degree
    type(t_polynomial_precomput)     ,intent(inout) :: coe(3)
    type(t_basis)                    ,intent(inout) :: basis
    logical                          ,intent(in)    :: flag_precomp
    type(t_error)                    ,intent(inout) :: ctx_err
    !! local     
    integer                        :: I,K,J,dim_domain
    type(t_polynomial),allocatable :: phibasic(:)
    type(t_polynomial)             :: phitmp
    REAL(kind=RKIND_POL)           :: val
    
    ctx_err%ierr=0    
    dim_domain  =1    !! 1D
    
    !! optional precomputation
    if(.not. flag_precomp) then
      call polynomial_precomput_construct(coe,dim_domain,degree,ctx_err)
    end if
    basis%dim_domain=dim_domain
    basis%degree    =degree 
    
    if(degree .gt. 0) then
      !!! basis%pol(i,:) contains the coefficients of the polynomial p1(i). 
      !!! They are stored in the following order :
      !!! [1,x,x^2,x^3,...]
      basis%nphi=degree+1
      allocate(basis%pol(basis%nphi))
      allocate(phibasic (basis%nphi))
      !! ? allocate(phibasic (degree))

      do i = 1, basis%nphi !!? degree
        call polynomial_construct(dim_domain,1,phibasic(i),ctx_err)
      end do
      !!! We first construct basic polynomial which we will use to construct the
      !!! phi_i1D
      !!! They correspond to x,x-1/degree, x-2/degree, ..., x-1.
      do i=1,basis%nphi
        !!! The constant
        Phibasic(I)%coeff_pol(1)=-REAL(I-1,RKIND_POL)/REAL(degree,RKIND_POL)
        !! x coeff : 
        Phibasic(I)%coeff_pol(2)=1._RKIND_POL
      end do
      
      !!! Now, we construct the basis functions
      !!! p1(k)=alpha*x(1/order-x)...((k-1)/order-x)((k+1)/order-x)...(1-x))
      basis%pol(1)=Phibasic(2)
      do I=3,degree+1
        call polynomial_multiplication(coe,basis%pol(1),Phibasic(I),phitmp,ctx_err)
        basis%pol(1)=phitmp
      end do
      
      call polynomial_eval(basis%pol(1),0._RKIND_POL,val,ctx_err)
      basis%pol(1)%coeff_pol= basis%pol(1)%coeff_pol/val !we normalize the basis
      ! function
      do K=2,degree+1
        basis%pol(K)=Phibasic(1)
        do I=2,K-1
           call polynomial_multiplication(coe,basis%pol(K),Phibasic(I),phitmp,ctx_err)
           basis%pol(K)=phitmp
        enddo
        do I=K+1,degree+1
           call polynomial_multiplication(coe,basis%pol(K),Phibasic(I),phitmp,ctx_err)
           basis%pol(K)=phitmp
        enddo
        call polynomial_eval(basis%pol(K),(K-1)/REAL(degree,RKIND_POL),val,ctx_err)
        basis%pol(K)%coeff_pol= basis%pol(K)%coeff_pol/val !we normalize the basis
        ! function
        do J=1,degree+1
           call polynomial_eval(basis%pol(K),(J-1)/REAL(degree,RKIND_POL),val,ctx_err)
        end do
      end do
    
    !! zero degree -----------------------------------------------------
    else if(degree == 0) then
      basis%nphi=1
      allocate(basis%pol(basis%nphi))
      call polynomial_construct(dim_domain,0,basis%pol(1),ctx_err)
      basis%pol(1)%coeff_pol=1
    else
      ctx_err%msg = "** ERROR: Basis degree < 0 [m_basis_construct1D] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)   
    end if
  
    return
  end subroutine basis_construct_1d
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>   Construct the 2D basis function 
  !
  !! --------------------------------------------------------------------------
  subroutine basis_construct_2d(coe,degree,basis,flag_precomp,ctx_err)
  
    implicit none  
    integer                          ,intent(in)    :: degree
    type(t_polynomial_precomput)     ,intent(inout) :: coe(3)
    type(t_basis)                    ,intent(inout) :: basis
    logical                          ,intent(in)    :: flag_precomp
    type(t_error)                    ,intent(inout) :: ctx_err
    !! local     
    integer                        :: I,K,L,Nblevel,ind1,level,dim_domain
    type(t_polynomial),allocatable :: phibasic(:),phibasic_x(:)
    type(t_polynomial),allocatable :: phibasic_y(:)
    type(t_polynomial)             :: phitmp,phiaux
    REAL(kind=RKIND_POL)           :: val
    
    ctx_err%ierr=0    
    dim_domain  =2    !! 1D
    
    !! optional precomputation
    if(.not. flag_precomp) then
      call polynomial_precomput_construct(coe,dim_domain,degree,ctx_err)
    end if
    basis%dim_domain=dim_domain
    basis%degree    =degree 

    if(degree .gt. 0) then
      basis%nphi=(degree+2)*(degree+1)/2
      allocate(basis%pol(basis%nphi))
      !!! basis%pol(i,:) contains the coefficients of the polynomial Phi_i. 
      !!! They are stored in the following order :
      !!! [1,x,y,x^2,xy,y^2,x^3,x^2y,xy^2,y^3,...]
      
      !!! We first construct basic polynomial which we will use to construct the phi_i
      !!! The first ones correspond to 1-x-y, ... 2/degree-x-y, 1/degree-x-y,...
      !!! The second ones correspond to x, 1/order-x, 3/order+x... 
      !!! The last ones correspond to y, 1/order-y, 3/order+y... 
      allocate(phibasic(1:degree))
      allocate(phibasic_x(1:degree))
      allocate(phibasic_y(1:degree))
      do i = 1, degree
        call polynomial_construct(dim_domain,1,phibasic  (i),ctx_err)
        call polynomial_construct(dim_domain,1,phibasic_x(i),ctx_err)
        call polynomial_construct(dim_domain,1,phibasic_y(i),ctx_err)
      end do
      
      do I=1,degree
         !The constant
         phibasic_x(I)%coeff_pol(1)=REAL(I-1,RKIND_POL)/REAL(degree,RKIND_POL)
         phibasic_y(I)%coeff_pol(1)=REAL(I-1,RKIND_POL)/REAL(degree,RKIND_POL)
         phibasic(I)%coeff_pol(1)=REAL((degree+1-I),RKIND_POL)/REAL(degree,RKIND_POL)
         ! x coeff
         phibasic_x(I)%coeff_pol(2)=-1._RKIND_POL
         phibasic_y(I)%coeff_pol(2)=0._RKIND_POL
         phibasic(I)%coeff_pol(2)=-1._RKIND_POL
         ! y coeff
         phibasic_x(I)%coeff_pol(3)=0._RKIND_POL
         phibasic_y(I)%coeff_pol(3)=-1._RKIND_POL
         phibasic(I)%coeff_pol(3)=-1._RKIND_POL
      end do
      !!! Now, we construct the basis functions
      !!! First, the basis functions associated to the vertices
      !!! Phi1=alpha*(1-x-y)*...*(2/degree-x-y)*(1/degree-x-y)
      basis%pol(1)=Phibasic(1)
      do I=2,degree
         call polynomial_multiplication(coe,basis%pol(1),Phibasic(I),phitmp,ctx_err)
         basis%pol(1)=phitmp
      enddo
      call polynomial_eval(basis%pol(1),0._RKIND_POL,0._RKIND_POL,val,ctx_err)
      basis%pol(1)%coeff_pol= basis%pol(1)%coeff_pol/val !we normalize the basis
      ! function
      !!! Phi2=alpha*x*(1/degree-x)*(2/degree-x)...
      basis%pol(2)=Phibasic_x(1)
      do I=2,degree
         call polynomial_multiplication(coe,basis%pol(2),Phibasic_x(I),phitmp,ctx_err)
         basis%pol(2)=phitmp
      enddo
      call polynomial_eval(basis%pol(2),1._RKIND_POL,0._RKIND_POL,val,ctx_err)
      basis%pol(2)%coeff_pol= basis%pol(2)%coeff_pol/val  !we normalize the basis
      ! function
      !!! Phi3=alpha*y*(1/degree-y)*(2/degree-y)...
      basis%pol(3)=Phibasic_y(1)
      do I=2,degree
         call polynomial_multiplication(coe,basis%pol(3),Phibasic_y(I),phitmp,ctx_err)
         basis%pol(3)=phitmp
      enddo
      call polynomial_eval(basis%pol(3),0._RKIND_POL,1._RKIND_POL,val,ctx_err)
      basis%pol(3)%coeff_pol= basis%pol(3)%coeff_pol/val  !we normalize the basis
      ! function
      if(degree.GE.2) then

      !!! Then, the basis functions associated to the edges
         !The first edge (y=0) : phik=alpha*x(x-1/degree)...(x-(k-1)/degree)(1
         !-x-y)...(1-(k+1)/degree+x+y))
         call polynomial_multiplication(coe,Phibasic(1),Phibasic_x(1),phiaux,ctx_err)
         do K=1,degree-1
            basis%pol(3+K)=phiaux
            do I=2,K
               call polynomial_multiplication(coe,basis%pol(3+K),Phibasic_x(I),phitmp,ctx_err)
               basis%pol(3+K)=phitmp
            enddo
            do I=2,degree-K
               call polynomial_multiplication(coe,basis%pol(3+K),Phibasic(I),phitmp,ctx_err)
               basis%pol(3+K)=phitmp
            enddo
            call polynomial_eval(basis%pol(3+K),K/REAL(degree,RKIND_POL),0._RKIND_POL,val,ctx_err)
            basis%pol(3+K)%coeff_pol=basis%pol(3+K)%coeff_pol/val
         end do
         !The second edge (x+y=1) phik=alpha*y(y-1/degree)...(y-(k-1)
         !/degree)x(x-1/degree)...(x-(degree-k)/degree)
         call polynomial_multiplication(coe,Phibasic_y(1),Phibasic_x(1),phiaux,ctx_err)
         do K=1,degree-1
            basis%pol(2+degree+K)=phiaux
            do I=2,K
               call polynomial_multiplication(coe,basis%pol(2+degree+K),Phibasic_y(I),phitmp,ctx_err)
               basis%pol(2+degree+K)=phitmp
            enddo
            do I=2,degree-K
               call polynomial_multiplication(coe,basis%pol(2+degree+K),Phibasic_x(I),phitmp,ctx_err)
               basis%pol(2+degree+K)=phitmp
            enddo
            call polynomial_eval(basis%pol(2+degree+K),(degree-K)/REAL(degree,RKIND_POL),K &
                 &/REAL(degree,RKIND_POL),val,ctx_err)
            basis%pol(2+degree+K)%coeff_pol= basis%pol(2+degree+K)%coeff_pol/val
         end do
         !The last edge (x=0) phik=alpha*(1-x-y)(1-1/degree-x-y)...(1-(k-1)
         !/degree-x-y)y(y-1/degree)...(y-(degree-k)/degree)
         call polynomial_multiplication(coe,Phibasic_y(1),Phibasic(1),phiaux,ctx_err)
         do K=1,degree-1
            basis%pol(1+2*degree+K)=Phiaux
            do I=2,K
               call polynomial_multiplication(coe,basis%pol(1+2*degree+K),Phibasic(I),phitmp,ctx_err)
               basis%pol(1+2*degree+K)=phitmp
            enddo
            do I=2,degree-K
               call polynomial_multiplication(coe,basis%pol(1+2*degree+K),Phibasic_y(I),phitmp,ctx_err)
               basis%pol(1+2*degree+K)=phitmp
            enddo
            call polynomial_eval(basis%pol(1+2*degree+K),0._RKIND_POL,(degree-K)/REAL(degree &
                 &,RKIND_POL),val,ctx_err)
            basis%pol(1+2*degree+K)%coeff_pol= basis%pol(1+2*degree+K)%coeff_pol/val !we
            ! normalize the basis FUNCTION
         end do
         if(degree.GE.2) then
           !!! Finally, the basis functions associated to the internal nodes.
           !!! This nodes are numbered in a "snail-way"
           !!! We compute the number of level in the snail (i.e. the number of triangles
           !!! inside)
            Nblevel=(degree-3)/3+1
            ind1=3+3*(degree-1)
            call polynomial_multiplication(coe,Phibasic_x(1),Phiaux,phitmp,ctx_err)
            phiaux=phitmp
      
            !!! We have already computed ind1 basis functions
            !!! We run over all the subtriangles 
            do Level=1,Nblevel
               !We first compute an auxiliary polynom that will be used for all
               ! basis functions
               !!!! Phi_ind1=alpha*x*y*(1-x-y)*(1/degree-x)*(1/degree-y)*(1-1/degree-x
               !!!!-y)...((level-1)/degree-x)*((level-1)/degree-y)*(1-(level-1)/degree-x-y)
               !!!!*Q(x,y)
               if(Level.GT.1) then
                  call polynomial_multiplication(coe,phiaux,Phibasic_x(Level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,Phibasic_y(Level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,Phibasic(Level),phitmp,ctx_err)
                  phiaux=phitmp
               end if
               !We run over the first edge of the triangle level 
               do L=1,degree+1-3*Level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! Now, we compute Q(x,y)
                  basis%pol(ind1)=phiaux
                  do K=1,L-1
                     call polynomial_multiplication(coe,basis%pol(ind1),Phibasic_x(Level+K),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do K=1,degree+1-3*Level-L
                     call polynomial_multiplication(coe,basis%pol(ind1),Phibasic(Level+K),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(L-1+Level)/REAL(degree,RKIND_POL), &
                                       Level/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize the
                  ! basis function
               enddo
               !We run over the second edge of the triangle level 
               do L=1,degree-3*Level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! Now, we compute Q(x,y)
                  basis%pol(ind1)=phiaux
                  do K=1,L
                     call polynomial_multiplication(coe,basis%pol(ind1),Phibasic_y(Level+K),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do K=1,degree-3*Level-L
                     call polynomial_multiplication(coe,basis%pol(ind1),Phibasic_x(Level+K),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(degree-L-2*Level)/REAL(degree,RKIND_POL)&
                       & ,(L+Level)/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize the
                  ! basis function
               end do
               !We run over the last edge of the triangle level 
               do L=1,degree-1-3*Level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! Now, we compute Q(x,y)
                  basis%pol(ind1)=phiaux
                  do K=1,L
                     call polynomial_multiplication(coe,basis%pol(ind1),Phibasic(Level+K),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do K=1,degree-3*Level-L
                     call polynomial_multiplication(coe,basis%pol(ind1),Phibasic_y(Level+K),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),Level/REAL(degree,RKIND_POL),(degree-L-2 &
                       &*Level)/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val  !we normalize the
                  ! basis function
               end do
            enddo
         end if
      end if
          
    !! 0 degree --------------------------------------------------------
    else if(degree == 0) then
      basis%nphi=1
      allocate(basis%pol(basis%nphi))
      call polynomial_construct(dim_domain,0,basis%pol(1),ctx_err)
      basis%pol(1)%coeff_pol=1
    else
      ctx_err%msg = "** ERROR: Basis degree < 0 [m_basis_construct2D] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)   
    end if
  
    return
  end subroutine basis_construct_2d
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>   Construct the 3D basis function 
  !
  !! --------------------------------------------------------------------------
  subroutine basis_construct_3d(coe,degree,basis,flag_precomp,ctx_err)
  
    implicit none  
    integer                          ,intent(in)    :: degree
    type(t_polynomial_precomput)     ,intent(inout) :: coe(3)
    type(t_basis)                    ,intent(inout) :: basis
    logical                          ,intent(in)    :: flag_precomp
    type(t_error)                    ,intent(inout) :: ctx_err
    !! local     
    integer                        :: I,J,K,L,Nblevel,ind1,level,dim_domain
    type(t_polynomial),allocatable :: phibasic(:),phibasic_x(:)
    type(t_polynomial),allocatable :: phibasic_y(:),phibasic_z(:)
    type(t_polynomial)             :: phitmp,phiaux,phiaux2,phiaux3
    REAL(kind=RKIND_POL)           :: val
    
    ctx_err%ierr=0    
    dim_domain  =3    !! 3D
    
    !! optional precomputation
    if(.not. flag_precomp) then
      call polynomial_precomput_construct(coe,dim_domain,degree,ctx_err)
    end if
    basis%dim_domain=dim_domain
    basis%degree    =degree 
    
    if(degree .gt. 0) then
      basis%nphi=(degree+3)*(degree+2)*(degree+1)/6
      allocate(basis%pol(basis%nphi))
      !!! phi(i,:) contains the coefficients of the polynomial phi_i. 
      !!! they are stored in the following order :
      !!! [1,x,y,z,x^2,xy,xz,y^2,yz,z^2,x^3,x^2y,x^2z,xy^2,xyz,xz^2,y^3,...]
      
      !!! we first construct basic polynomial which we will use to construct the phi_i
      !!! the first ones  correspond to 1-x-y-z, ... 2/order-x-y-z, 1/order-x-y-z,...
      !!! the second ones correspond to x, 1/order-x, 3/order+x... 
      !!! the third ones  correspond to y, 1/order-y, 3/order+y... 
      !!! the last ones   correspond to z, 1/order-z, 3/order+z... 
      allocate(phibasic  (1:degree))
      allocate(phibasic_x(1:degree))
      allocate(phibasic_y(1:degree))
      allocate(phibasic_z(1:degree))
      do i = 1, degree
        call polynomial_construct(dim_domain,1,phibasic  (i),ctx_err)
        call polynomial_construct(dim_domain,1,phibasic_x(i),ctx_err)
        call polynomial_construct(dim_domain,1,phibasic_y(i),ctx_err)
        call polynomial_construct(dim_domain,1,phibasic_z(i),ctx_err)
      end do
      
      do i=1,degree
         !the constant
         phibasic_x(i)%coeff_pol(1)=REAL(i-1,RKIND_POL)/REAL(degree,RKIND_POL)
         phibasic_y(i)%coeff_pol(1)=REAL(i-1,RKIND_POL)/REAL(degree,RKIND_POL)
         phibasic_z(i)%coeff_pol(1)=REAL(i-1,RKIND_POL)/REAL(degree,RKIND_POL)
         phibasic(i)%coeff_pol(1)=REAL((degree+1-i),RKIND_POL)/REAL(degree,RKIND_POL)
         ! x coeff
         phibasic_x(i)%coeff_pol(2)=-1._RKIND_POL
         phibasic_y(i)%coeff_pol(2)=0._RKIND_POL
         phibasic_z(i)%coeff_pol(2)=0._RKIND_POL
         phibasic(i)%coeff_pol(2)=-1._RKIND_POL
         ! y coeff
         phibasic_x(i)%coeff_pol(3)=0._RKIND_POL
         phibasic_y(i)%coeff_pol(3)=-1._RKIND_POL
         phibasic_z(i)%coeff_pol(3)=0._RKIND_POL
         phibasic(i)%coeff_pol(3)=-1._RKIND_POL
         ! z coeff
         phibasic_x(i)%coeff_pol(4)=0._RKIND_POL
         phibasic_y(i)%coeff_pol(4)=0._RKIND_POL
         phibasic_z(i)%coeff_pol(4)=-1._RKIND_POL
         phibasic(i)%coeff_pol(4)=-1._RKIND_POL
      end do
      !!! now, we construct the basis functions
      !!! first, the basis functions associated to the vertices
      !!! phi1=alpha*(1-x-y-z)*...*(2/order-x-y-z)*(1/order-x-y-z)
      basis%pol(1)=phibasic(1)
      do i=2,degree
         call polynomial_multiplication(coe,basis%pol(1),phibasic(i),phitmp,ctx_err)
         basis%pol(1)=phitmp
      enddo
      call polynomial_eval(basis%pol(1),0._RKIND_POL,0._RKIND_POL,0._RKIND_POL,val,ctx_err)
      basis%pol(1)%coeff_pol= basis%pol(1)%coeff_pol/val  !we normalize the basis
      ! function
      !!! phi2=alpha*x*(1/degree-x)*(2/degree-x)...
      basis%pol(2)=phibasic_x(1)
      do i=2,degree
         call polynomial_multiplication(coe,basis%pol(2),phibasic_x(i),phitmp,ctx_err)
         basis%pol(2)=phitmp
      enddo
      call polynomial_eval(basis%pol(2),1._RKIND_POL,0._RKIND_POL,0._RKIND_POL,val,ctx_err)
      basis%pol(2)%coeff_pol= basis%pol(2)%coeff_pol/val  !we normalize the basis
      ! function
      !!! phi3=alpha*y*(1/degree-y)*(2/degree-y)...
      basis%pol(3)=phibasic_y(1)
      do i=2,degree
         call polynomial_multiplication(coe,basis%pol(3),phibasic_y(i),phitmp,ctx_err)
         basis%pol(3)=phitmp
      enddo
      call polynomial_eval(basis%pol(3),0._RKIND_POL,1._RKIND_POL,0._RKIND_POL,val,ctx_err)
      basis%pol(3)%coeff_pol= basis%pol(3)%coeff_pol/val  !we normalize the basis
      ! function
      !!! phi4=alpha*z*(1/degree-z)*(2/degree-z)...
      basis%pol(4)=phibasic_z(1)
      do i=2,degree
         call polynomial_multiplication(coe,basis%pol(4),phibasic_z(i),phitmp,ctx_err)
         basis%pol(4)=phitmp
      enddo
      call polynomial_eval(basis%pol(4),0._RKIND_POL,0._RKIND_POL,1._RKIND_POL,val,ctx_err)
      basis%pol(4)%coeff_pol= basis%pol(4)%coeff_pol/val  !we normalize the basis
      ! function
      if(degree.GT.1) then
         !!$!!! then, the basis functions associated to the edges
         !!$!the first edge (y=0,z=0), phik=alpha*x(x-1/order)...(x-(k-1)/order)(1-x-y
         !!-z)...(1-(k+1)/order+x+y+z))
         ind1=4
         call polynomial_multiplication(coe,phibasic(1),phibasic_x(1),phiaux,ctx_err)
         do k=1,degree-1
            ind1=ind1+1
            basis%pol(ind1)=phiaux
            do i=2,k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            do i=2,degree-k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            call polynomial_eval(basis%pol(ind1),k/REAL(degree,RKIND_POL), &
                                 0._RKIND_POL,0._RKIND_POL,val,ctx_err)
            basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val
         end do
         !the second edge (x+y=1,z=0) phik=alpha*y(y-1/order)...(y-(k-1)
         !/order)x(x-1/order)...(x-(order-k)/order)
         call polynomial_multiplication(coe,phibasic_y(1),phibasic_x(1),phiaux,ctx_err)
         do k=1,degree-1
            ind1=ind1+1
            basis%pol(ind1)=phiaux
            do i=2,k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            do i=2,degree-k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            call polynomial_eval(basis%pol(ind1),(degree-k)/REAL(degree,RKIND_POL),k &
                 &/REAL(degree,RKIND_POL),0._RKIND_POL,val,ctx_err)
            basis%pol(ind1)%coeff_pol= basis%pol(ind1)%coeff_pol/val
         end do
         !the third edge (x=0,z=0), phik=alpha*(1-x-y-z)(1-1/order-x-y
         !-z)...(1-(k-1)
         !/order-x-y)y(y-1/order)...(y-(order-k)/order)
         call polynomial_multiplication(coe,phibasic_y(1),phibasic(1),phiaux,ctx_err)
         do k=1,degree-1
            ind1=ind1+1
            basis%pol(ind1)=phiaux
            do i=2,k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            do i=2,degree-k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            call polynomial_eval(basis%pol(ind1),0._RKIND_POL,(degree-k)/REAL(degree,RKIND_POL) &
                 &,0._RKIND_POL,val,ctx_err)
            basis%pol(ind1)%coeff_pol= basis%pol(ind1)%coeff_pol/val !we normalize the
            ! basis function
         end do
         !the fourth edge (x=0,y=0), phik=alpha*z(z-1/order)...(z-(k-1)
         !/order)(1-x-y
         !-z)...(1-(k+1)/order+x+y+z))
         call polynomial_multiplication(coe,phibasic_z(1),phibasic(1),phiaux,ctx_err)
         do k=1,degree-1
            ind1=ind1+1
            basis%pol(ind1)=phiaux
            do i=2,k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            do i=2,degree-k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            call polynomial_eval(basis%pol(ind1),0._RKIND_POL,0._RKIND_POL, & 
                                 k/REAL(degree,RKIND_POL),val,ctx_err)
            basis%pol(ind1)%coeff_pol= basis%pol(ind1)%coeff_pol/val !we normalize the
            ! basis function
         end do
         !the fiftth edge (y=0,1-x-z=0), phik=alpha*z(z-1/order)...(z-(k-1)
         !/order)x(x-1
         !/order)...(x-(order-k)/order)
         call polynomial_multiplication(coe,phibasic_z(1),phibasic_x(1),phiaux,ctx_err)
         do k=1,degree-1
            ind1=ind1+1
            basis%pol(ind1)=phiaux
            do i=2,k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            do i=2,degree-k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            call polynomial_eval(basis%pol(ind1),(degree-k)/REAL(degree,RKIND_POL),0._RKIND_POL,k&
                 & /REAL(degree,RKIND_POL),val,ctx_err)
            basis%pol(ind1)%coeff_pol= basis%pol(ind1)%coeff_pol/val !we normalize the
            ! basis function
         end do
         !the sixth edge (x=0,1-y-z=0), phik=alpha*z(z-1/order)...(z-(k-1)
         !/order)y(y-1
         !/order)...(y-(order-k)/order)
         call polynomial_multiplication(coe,phibasic_z(1),phibasic_y(1),phiaux,ctx_err)
         do k=1,degree-1
            ind1=ind1+1
            basis%pol(ind1)=phiaux
            do i=2,k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            do i=2,degree-k
               call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(i),phitmp,ctx_err)
               basis%pol(ind1)=phitmp
            enddo
            call polynomial_eval(basis%pol(ind1),0._RKIND_POL,(degree-k)/REAL(degree,RKIND_POL),k&
                 & /REAL(degree,RKIND_POL),val,ctx_err)
            basis%pol(ind1)%coeff_pol= basis%pol(ind1)%coeff_pol/val !we normalize the
            ! basis function
         end do
         if(degree.GT.2) then
            !!! the basis functions associated to the first face (1-x-y-z=0).
            !!$!!! these nodes are numbered in a "snail-way", starting from= (x,y,z)=(1-1
            !!/order,1/order,1/order) and in the direct sens.
            !!$!!! we compute the number of level in the snail (i.e. the number of
            !! triangles inside)
            nblevel=(degree-3)/3+1
            call polynomial_multiplication(coe,phibasic_z(1),phibasic_y(1),phitmp,ctx_err)
            call polynomial_multiplication(coe,phitmp,phibasic_x(1),phiaux,ctx_err)
            !!! we run over all the subtriangles
            do level=1,nblevel
               !we first compute an auxiliary polynom that will be used for
               ! all
               ! basis functions
               !!!! phi_ind1=alpha*x*y*z*(1/order-x)*(1/order-y)*(1/order-z)...((level-1)
               !!!!/order-x)*((level-1)/order-y)*(1-(level-1)/order-z)*q(x,y,z)
               if(level.GT.1) then
                  call polynomial_multiplication(coe,phiaux,phibasic_z(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic_y(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic_x(level),phitmp,ctx_err)
                  phiaux=phitmp
               end if
               !we run over the first edge of the triangle level 
               do l=1,degree+1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l-1
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree+1-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(degree-2*level-l+1) &
                       &/REAL(degree,RKIND_POL),level/REAL(degree,RKIND_POL),(level+l-1)&
                       &/REAL(degree,RKIND_POL),val,ctx_err)
      
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               end do
               !we run over the second edge of the triangle level 
               do l=1,degree-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),level/REAL(degree,RKIND_POL),(level&
                       & +l) /REAL(degree,RKIND_POL),(degree-2*level-l) &
                       &/REAL(degree ,RKIND_POL),val,ctx_err)
      
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               end do
               !we run over the last edge of the triangle level 
               do l=1,degree-1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(level+l)/REAL(degree,RKIND_POL) &
                       &,(degree-2*level-l)/REAL(degree,RKIND_POL),&
                       &level /REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val  !we
                  ! normalize the basis function
               end do
            end do
      
            !!! the basis functions associated to the second face (x=0).
            !!! these nodes are numbered in a "snail-way", starting from= (y,z)=(1/order,1
            !!!/order) and in the direct sens.
            !!! we compute the number of level in the snail (i.e. the number of triangles
            !!! inside)
            nblevel=(degree-3)/3+1
            call polynomial_multiplication(coe,phibasic_z(1),phibasic_y(1),phitmp,ctx_err)
            call polynomial_multiplication(coe,phitmp,phibasic(1),phiaux,ctx_err)
            !!! we run over all the subtriangles
            do level=1,nblevel
               !we first compute an auxiliary polynom that will be used for
               ! all
               ! basis functions
               !!!! phi_ind1=alpha*y*z*(1-x-y-z)*(1/order-z)*(1/order-y)*(1-1/order-x-y
               !!!!-z)...((level-1)/order-y)*((level-1)/order-z)*(1-(level-1)/order-x-y-z)*q(x
               !!!!,y,z)
               if(level.GT.1) then
                  call polynomial_multiplication(coe,phiaux,phibasic_z(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic_y(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic(level),phitmp,ctx_err)
                  phiaux=phitmp
               end if
               !we run over the first edge of the triangle level 
               do l=1,degree+1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l-1
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree+1-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic(level+k),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),0._RKIND_POL,(l-1+level) &
                       &/REAL(degree,RKIND_POL),level/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               end do
               !we run over the second edge of the triangle level 
               do l=1,degree-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),0._RKIND_POL,(degree-l-2*level) &
                       &/REAL(degree,RKIND_POL),(l+level)/REAL(degree,RKIND_POL),val,ctx_err)
      
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               end do
               !we run over the last edge of the triangle level 
               do l=1,degree-1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic(level+k),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),0._RKIND_POL,level/REAL(degree,RKIND_POL) &
                       &,(degree-l-2 *level)/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val  !we
                  ! normalize the basis function
               end do
            end do
            !!! the basis functions associated to the third face (y=0).
            !!! these nodes are numbered in a "snail-way", starting from= (x,z)=(1/order,1
            !!!/order) and in the direct sens.
            !!! we compute the number of level in the snail (i.e. the number of triangles
            !!! inside)
            nblevel=(degree-3)/3+1
            call polynomial_multiplication(coe,phibasic_z(1),phibasic_x(1),phitmp,ctx_err)
            call polynomial_multiplication(coe,phitmp,phibasic(1),phiaux,ctx_err)
            !!! we run over all the subtriangles
            do level=1,nblevel
               !we first compute an auxiliary polynom that will be used for
               ! all basis functions
               !!!! phi_ind1=alpha*z*x*(1-x-y-z)*(1/order-z)*(1/order-x)*(1-1/order-x-y
               !!!!-z)...((level-1)/order-z)*((level-1)/order-x)*(1-(level-1)/order-x-y-z)*q(x
               !!!!,y)
               if(level.GT.1) then
                  call polynomial_multiplication(coe,phiaux,phibasic_z(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic_x(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic(level),phitmp,ctx_err)
                  phiaux=phitmp
               end if
               !we run over the first edge of the triangle level 
               do l=1,degree+1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l-1
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree+1-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic(level+k),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),level/REAL(degree,RKIND_POL),0._RKIND_POL &
                       &,(l-1 +level)/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               end do
               !we run over the second edge of the triangle level 
               do l=1,degree-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_z(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(l+level)/REAL(degree,RKIND_POL) &
                       &,0._RKIND_POL,(degree-l-2*level)/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               end do
               !we run over the last edge of the triangle level 
               do l=1,degree-1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic(level+k),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(degree-l-2 *level) &
                       &/REAL(degree,RKIND_POL),0._RKIND_POL, &
                       level/REAL(degree,RKIND_POL),val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val  !we
                  ! normalize the basis function
               end do
            end do
      
            !!! now, the basis functions associated to the last face (z=0).
            !!! these nodes are numbered in a "snail-way", starting from= (x,y)=(1/order,1
            !!!/order) and in the direct sens.
            !!! we compute the number of level in the snail (i.e. the number of triangles
            !!! inside)
            nblevel=(degree-3)/3+1
            call polynomial_multiplication(coe,phibasic_x(1),phibasic_y(1),phitmp,ctx_err)
            call polynomial_multiplication(coe,phitmp,phibasic(1),phiaux,ctx_err)
            !!! we run over all the subtriangles
            do level=1,nblevel
      
               !we first compute an auxiliary polynom that will be used for
               ! all basis functions
               !!!! phi_ind1=alpha*x*y*(1-x-y-z)*(1/order-x)*(1/order-y)*(1-1/order-x-y
               !!!!-z)...((level-1)/order-x)*((level-1)/order-y)*(1-(level-1)/order-x-y-z)*q(x
               !!!!,y,z)
               if(level.GT.1) then
                  call polynomial_multiplication(coe,phiaux,phibasic_x(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic_y(level),phitmp,ctx_err)
                  phiaux=phitmp
                  call polynomial_multiplication(coe,phiaux,phibasic(level),phitmp,ctx_err)
                  phiaux=phitmp
               end if
               !we run over the first edge of the triangle level 
               do l=1,degree+1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l-1
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree+1-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic(level+k),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(l-1+level)/REAL(degree,RKIND_POL) &
                       &,level /REAL(degree,RKIND_POL),0._RKIND_POL,val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               enddo
               !we run over the second edge of the triangle level 
               do l=1,degree-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_x(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),(degree-l-2*level) &
                       &/REAL(degree,RKIND_POL) ,(l+level)/REAL(degree,RKIND_POL),0._RKIND_POL &
                       &,val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val !we normalize
                  ! the basis function
               end do
               !we run over the last edge of the triangle level 
               do l=1,degree-1-3*level
                  ind1=ind1+1 ! we increase the number of the basis function
                  !!! now, we compute q(x,y,z)
                  basis%pol(ind1)=phiaux
                  do k=1,l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic(level+k),phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  do k=1,degree-3*level-l
                     call polynomial_multiplication(coe,basis%pol(ind1),phibasic_y(level+k) &
                          &,phitmp,ctx_err)
                     basis%pol(ind1)=phitmp
                  end do
                  call polynomial_eval(basis%pol(ind1),level/REAL(degree,RKIND_POL) &
                       &,(degree-l-2 *level)/REAL(degree,RKIND_POL),0._RKIND_POL,val,ctx_err)
                  basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val  !we
                  ! normalize the basis function
               end do
            end do
            if(degree.GT.3) then
               !!! finally, the basis functions associated to the internal nodes.
               call polynomial_multiplication(coe,phibasic_z(1),phibasic_y(1),phitmp,ctx_err)
               call polynomial_multiplication(coe,phitmp,phibasic(1),phiaux,ctx_err)
               call polynomial_multiplication(coe,phiaux,phibasic_x(1),phitmp,ctx_err)
               phiaux=phitmp
               do i=1,degree-3
                  if(i.GE.2) then
                     call polynomial_multiplication(coe,phiaux,phibasic_x(i),phitmp,ctx_err)
                     phiaux=phitmp
                  end if
                  phiaux2=phiaux
                  do j=1,degree-2-i
                     if(j.GE.2) then
                        call polynomial_multiplication(coe,phiaux2,phibasic_y(j),phitmp,ctx_err)
                        phiaux2=phitmp
                     end if
                     phiaux3=phiaux2
                     do k=1,degree-1-i-j
                        ind1=ind1+1
                        if(k.GE.2) then
                           call polynomial_multiplication(coe,phiaux3,phibasic_z(k),phitmp,ctx_err)
                           phiaux3=phitmp
                        end if
                        basis%pol(ind1)=phiaux3
                        do l=2,degree-i-j-k
                           call polynomial_multiplication(coe,basis%pol(ind1),phibasic(l),phitmp,ctx_err)
      
                           basis%pol(ind1)=phitmp
                        end do
                        call polynomial_eval(basis%pol(ind1),i/REAL(degree,RKIND_POL),j &
                             &/REAL(degree ,RKIND_POL),k/REAL(degree,RKIND_POL),val,ctx_err)
                        basis%pol(ind1)%coeff_pol=basis%pol(ind1)%coeff_pol/val
                     end do
                  end do
               end do
            end if
         end if
      end if    
    !! 0 degree --------------------------------------------------------
    else if(degree == 0) then
      basis%nphi=1
      allocate(basis%pol(basis%nphi))
      call polynomial_construct(dim_domain,0,basis%pol(1),ctx_err)
      basis%pol(1)%coeff_pol=1
    else
      ctx_err%msg = "** ERROR: Basis degree < 0 [m_basis_construct3D] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)   
    end if
  
    return
  end subroutine basis_construct_3d


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>    construct the derivative of the basis functions
  !
  !! --------------------------------------------------------------------------
  subroutine basis_derivative_construct(basis,order,ctx_err)
    implicit none
    
    integer           ,intent(in)    :: order
    type(t_basis)     ,intent(inout) :: basis
    type(t_error)     ,intent(inout) :: ctx_err
    integer :: J,L,M,index1,index2
    
    ctx_err%ierr=0
    
    if (order .eq. 0) then
      return
    else if(order < 0) then
      ctx_err%msg = "** ERROR: derivative order < 0 in [basis_deriv_const] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    else !! order > 0
      allocate(basis%pol_derivative(order))
      select CASE(basis%dim_domain)
        CASE(1)
           allocate(basis%pol_derivative(1)%p(basis%nphi,1))
           do L=1,basis%nphi
             call polynomial_dx(basis%pol(L),basis%pol_derivative(1)%p(L,1),ctx_err)
           end do
           do j=2,order
              allocate(basis%pol_derivative(j)%p(basis%nphi,1))
              do L=1,basis%nphi   
                call polynomial_dx(basis%pol_derivative(j-1)%p(L,1), &
                                   basis%pol_derivative(j)%p(L,1),ctx_err)                
              end do
           end do
        CASE(2)
           allocate(basis%pol_derivative(1)%p(basis%nphi,2))
           do L=1,basis%nphi
             call polynomial_dx(basis%pol(L),basis%pol_derivative(1)%p(L,1),ctx_err)
             call polynomial_dy(basis%pol(L),basis%pol_derivative(1)%p(L,2),ctx_err)
           end do
           do j=2,order
              allocate(basis%pol_derivative(j)%p(basis%nphi,j+1))
              do L=1,basis%nphi   
                do M=1,j
                  call polynomial_dx(basis%pol_derivative(j-1)%p(L,M),&
                                     basis%pol_derivative(j)%p(L,M),ctx_err)
                end do           
                call polynomial_dy(basis%pol_derivative(j-1)%p(L,j),  &
                                   basis%pol_derivative(j)%p(L,j+1),ctx_err)
              end do
           end do
        CASE(3)
           allocate(basis%pol_derivative(1)%p(basis%nphi,3))
           do L=1,basis%nphi   
             call polynomial_dx(basis%pol(L),basis%pol_derivative(1)%p(L,1),ctx_err)
             call polynomial_dy(basis%pol(L),basis%pol_derivative(1)%p(L,2),ctx_err)
             call polynomial_dz(basis%pol(L),basis%pol_derivative(1)%p(L,3),ctx_err)
           end do
           do j=2,order
              allocate(basis%pol_derivative(j)%p(basis%nphi,(j+1)*(j+2)/2))
              do L=1,basis%nphi
                do M=1,j*(j+1)/2
                  call polynomial_dx(basis%pol_derivative(j-1)%p(L,M),   &
                                     basis%pol_derivative(j)%p(L,M),ctx_err) 
                end do

                do M=1,j
                  index1 = (j-1)*j/2 + M
                  index2 = j*(j+1)/2 + M
                  call polynomial_dy(basis%pol_derivative(j-1)%p(L,index1), &
                                     basis%pol_derivative(j)  %p(L,index2),ctx_err)
                end do
                call polynomial_dz(basis%pol_derivative(j-1)%p(L,j*(j+1)/2), &
                                   basis%pol_derivative(j)%p(L,j*(j+1)/2+j+1),ctx_err)
              end do
           end do
         
         case default
           ctx_err%msg = "** ERROR: Incorrect dimension [basis_deriv_const] **"
           ctx_err%ierr  = -1
           ctx_err%critic=.true.
           call raise_error(ctx_err)
      end select
    end if
  end subroutine basis_derivative_construct
  !----------------------------------------------------------------------------
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>  construct the basis functions on the face of an element
  !
  !! --------------------------------------------------------------------------
  subroutine basis_face_construct(basis,order,ctx_err)
    implicit none
    
    integer      ,intent(in)    :: order
    type(t_basis),intent(inout) :: basis
    type(t_error),intent(inout) :: ctx_err
    !! local
    integer :: I,J,K,L
    
    ctx_err%ierr=0

    allocate(basis%pol_face(basis%nphi,basis%dim_domain+1))
    do I=1,basis%dim_domain+1
      do K=1,basis%nphi
        call polynomial_trace(basis%pol(K),basis%pol_face(K,I),I,ctx_err)
      end do
    end do
    allocate(basis%pol_face_derivative(order,basis%dim_domain+1))
    select CASE(basis%dim_domain)
    CASE(1)             
       do J=1,order
          do I=1,basis%dim_domain+1
             allocate(basis%pol_face_derivative(J,I)%p(basis%nphi,1))
             do K=1,basis%nphi
               call polynomial_trace(basis%pol_derivative(J)%p(K,1),       &
                                     basis%pol_face_derivative(J,i)%p(K,1),&
                                     i,ctx_err)
             end do
          end do
       end do
    CASE(2)             
       do J=1,order
          do I=1,basis%dim_domain+1
             allocate(basis%pol_face_derivative(J,I)%p(basis%nphi,j+1))
             do k=1,j+1
                do L=1,basis%nphi
                  call polynomial_trace(basis%pol_derivative(J)%p(L,k),        &
                                        basis%pol_face_derivative(J,i)%p(L,k), &
                                        i,ctx_err)
                end do
             end do
          end do
       end do
    CASE(3)             
       do J=1,order
          do I=1,basis%dim_domain+1
             allocate(basis%pol_face_derivative(J,I)%p(basis%nphi,(j+1)*(j+2)/2))
             do k=1,(j+1)*(j+2)/2
               do L=1,basis%nphi
                 call polynomial_trace(basis%pol_derivative(J)%p(L,k),        &
                                       basis%pol_face_derivative(J,i)%p(L,k), &
                                       i,ctx_err)
               end do
             end do
          end do
       end do
    case default
       ctx_err%msg = "** ERROR: Incorrect dimension [basis_face_const] **"
       ctx_err%ierr  = -1
       ctx_err%critic=.true.
       call raise_error(ctx_err)
    end select
  end subroutine basis_face_construct

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !> construct the basis on the face neighbors
  !
  !! --------------------------------------------------------------------------
  subroutine basis_face_neigh_construct(basis,order,ctx_err)
    implicit none
    
    integer      ,intent(in)   :: order
    type(t_basis),intent(inout):: basis
    type(t_error),intent(inout):: ctx_err
    !! local
    integer :: I,face,mycase,order_diff, J
    
    ctx_err%ierr=0

    select CASE(basis%dim_domain)
    CASE(1)             
       allocate(basis%pol_face_neigh(basis%nphi,2,1))
       allocate(basis%pol_face_neigh_derivative(order,2,1))
       basis%pol_face_neigh(1:basis%nphi,1,1)=basis%pol_face(1:basis%nphi,1)
       basis%pol_face_neigh_derivative(1:order,1,1)=basis%pol_face_derivative(1:order,1)
       basis%pol_face_neigh(1:basis%nphi,2,1)=basis%pol_face(1:basis%nphi,1)
       basis%pol_face_neigh_derivative(1:order,2,1)=basis%pol_face_derivative(1:order,1)
    CASE(2)             
       allocate(basis%pol_face_neigh(basis%nphi,3,1))
       allocate(basis%pol_face_neigh_derivative(order,3,1))
       do I=1,basis%nphi ; do J=1,3
         call polynomial_invert_coo(basis%pol_face(I,J),                &
                                    basis%pol_face_neigh(I,J,1),1,ctx_err)
       end do ; end do
       do face=1,3
          do order_diff=1,order
             allocate(basis%pol_face_neigh_derivative(order_diff,Face,1)%p(basis &
                  &%nphi,order_diff+1))
             do I=1,basis%nphi ; do J=1,order_diff+1
               call polynomial_invert_coo(basis%pol_face_derivative(order_diff,Face)%p(I,J),        &
                                          basis%pol_face_neigh_derivative(order_diff,Face,1)%p(I,J),&
                                          1,ctx_err)
             end do ; end do
          end do
       end do
    CASE(3)
       allocate(basis%pol_face_neigh(basis%nphi,4,3))
       allocate(basis%pol_face_neigh_derivative(order,4,3))
       do mycase=1,3
         do I=1,basis%nphi ; do J=1,4
           call polynomial_invert_coo(basis%pol_face(I,J),              &
                                      basis%pol_face_neigh(I,J,mycase), &
                                      mycase,ctx_err)
         end do ; end do
          
         do Face=1,4
           do order_diff=1,order
             allocate(basis%pol_face_neigh_derivative(order_diff,FACE,mycase)  & 
                           %p(basis%nphi,(order_diff+1)*(order_diff+2)/2))

             do I=1,basis%nphi ; do J=1,(order_diff+1)*(order_diff+2)/2
               call polynomial_invert_coo(                                        &
                  basis%pol_face_derivative      (order_diff,Face)       %p(I,J), &
                  basis%pol_face_neigh_derivative(order_diff,Face,mycase)%p(I,J), &
                  mycase,ctx_err)
             end do ; end do
             
             
           end do
         end do
       end do
    case default
       ctx_err%msg = "** ERROR: Incorrect dimension [basis_face_neigh_const] **"
       ctx_err%ierr  = -1
       ctx_err%critic=.true.
       call raise_error(ctx_err)
    end select
    
  end subroutine basis_face_neigh_construct

end module m_basis_functions
