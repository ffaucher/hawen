!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_quadrature_init.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> The module is used to initialize the quadrature formula for simplexes.
!> It depends on the dimension (segment, triangle, or tetrahedron), and
!> on the order of the polynomial to select the total number of points.
!
!------------------------------------------------------------------------------
module m_quadrature_init

  !! module used -------------------------------------------------------
  use m_raise_error,        only : t_error, raise_error
  use m_define_precision,   only : RKIND_POL
  use m_quadrature_list_1D
  use m_quadrature_list_2D
  use m_quadrature_list_3D
  
  !! -------------------------------------------------------------------
  implicit none
  
  private  
  public  :: quadrature_init
  
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief 
  !>    Initialize the quadrature array depending on the dimension and order.
  !! --------------------------------------------------------------------------
  subroutine quadrature_init(dim_domain,npts,weight,points,order,       &
                             str_formula,ctx_err)

    implicit none
    
    integer                          , intent(in)    :: dim_domain
    integer                          , intent(out)   :: npts
    real(kind=RKIND_POL), allocatable, intent(inout) :: weight(:),points(:,:)
    integer                          , intent(in)    :: order
    character(len=*)                 , intent(inout) :: str_formula
    type(t_error)                    , intent(inout) :: ctx_err

    ctx_err%ierr = 0
    str_formula  = ''
    npts         = 0 
    !! .................................................................
    !! because we have a constant number of points, we must be careful 
    !! to the highest degree we have 
    !!  order1: 2n-3 => with 100 points, it could be up too 197, 
    !!                  assuming phi_i phi_j we divide by 2, which
    !!                  gives order 98 in theory ?
    !!
    !! not sure for 2 and 3d though, the must be a power 1/2 and 1/3...
    !!
    !! For very high order, we also ensure to have a robust evaluation of
    !! the polynomials and a robust sum of all the weighted contribution,
    !! that is, we may need quadruple precision for the polynomials.
    !! .................................................................
    !!
    !! dimension 1: 
    !!   - Gauss-Lobatto 100 points is ok, 
    !!     we need quadruple for order > 9
    !!
    !! dimension 2: we need quadruple precision when order > 9. 
    !!   - Gauss--Lobatto 91 points
    !!
    !!
    !! *****************************************************************
    if(order > 9 .and. RKIND_POL < 16) then
      ctx_err%msg   = "** ERROR: polynomial order > 9 should not be "// &
                      "used except if you allow for quadruple "      // &
                      "precision for the polynomials in the file "   // &
                      "m_define_precision.f90 [quadrature_init] **"
      ctx_err%ierr  = -201
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(order >= 18) then
      ctx_err%msg   = "** ERROR: polynomial order >= 18 has not been"// &
                      " validated with current polynomials "         // &
                      "computation. [quadrature_init] **"
      ctx_err%ierr  = -202
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 
    if(order < -1) then
      ctx_err%msg   = "** ERROR: calling quadrature with negative " //  &
                      "order [quadrature_init] **"
      ctx_err%ierr  = -203
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! end error -------------------------------------------------------

    select case(dim_domain)

      !! 1 dimension ===================================================
      case(1)
        !! --------------------------------------------------------------
        !! Reference element with POL_KIND        double   |  quadruple |
        !!                                 order           |            |
        !! --------------------------------------------------------------
        !! validation with POL_KIND               double   |  quadruple |
        !! possible options:                                            |
        !!  -> Gauss--Lobatto    :  100 points |           |            |
        !!  -> Legendre--Gauss   :   11 points |           |            |
        !!  -                        21 points |           |            |
        !!  -                        51 points |           |            |
        !!  -                       101 points |           |            |
        !!  -                       201 points |           |            |
        !!  -> Chebyshev 1st kind:   11 points |           |            |
        !!  -                        21 points |           |            |
        !!  -                        51 points |           |            |
        !!  -                       101 points |           |            |
        !!  -                       201 points |           |            |
        !!  -> Chebyshev 2nd kind:   11 points |           |            |
        !!  -                        21 points |           |            |
        !!  -                        51 points |           |            |
        !!  -                       101 points |           |            |
        !!  -                       201 points |           |            |
        !! --------------------------------------------------------------
        str_formula = 'Gauss-Lobatto'
        call quadrature_1D_GaussLobatto_order197         (npts,weight,points)
        ! call quadrature_1D_LegendreGauss_order021      (npts,weight,points)
        ! call quadrature_1D_LegendreGauss_order041      (npts,weight,points)
        ! call quadrature_1D_LegendreGauss_order101      (npts,weight,points)
        ! call quadrature_1D_LegendreGauss_order201      (npts,weight,points)
        ! call quadrature_1D_LegendreGauss_order401      (npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind1_order021(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind1_order041(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind1_order101(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind1_order201(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind1_order401(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind2_order021(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind2_order041(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind2_order101(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind2_order201(npts,weight,points)
        ! call quadrature_1D_ChebyshevGausskind2_order401(npts,weight,points)

      !! 2 dimensions ==================================================
      case(2)
        !! --------------------------------------------------------------
        !! Reference element with POL_KIND        double   |  quadruple |
        !!                                 order           |    17      |
        !! --------------------------------------------------------------
        !! validation with POL_KIND               double   |  quadruple |
        !! possible options:                                            |
        !!  -> Gauss--Lobatto    :   91 points |    9      |  ~ 12-13   |
        !!  -> Vioreanu--Rokhlin :   36 points |    8      |        9   |
        !!  -                       120 points |    9      |  ~ 12-13   |
        !!  -                       231 points |    9      |   ~   15   |
        !!  -> Grundmann--Moeller:   79 points |    8      |        9   |
        !!  -                       430 points |    7      |  ~ 13-14   |
        !! --------------------------------------------------------------
        !! --------------------------------------------------------------
        !! default is ok for p14, ko for p15 it seems
        !! VR      is ok for p17, ko for p18 it seems
        !! VR same as the reference element, therefore the inaccuracy at p18 can 
        !!    come from basis function evaluation or from MUMPS solver too dense ...
        !! The modepy indicates ok up to order 33 => p17 x p17 = 34 
        !! -------------------------------------------------------------
        ! call quadrature_2D_GaussLobatto_order024    (npts,weight,points)
        ! call quadrature_2D_GrundmannMoeller_order013(npts,weight,points)
        ! call quadrature_2D_GrundmannMoeller_order025(npts,weight,points)
        ! call quadrature_2D_VioreanuRokhlin_order012 (npts,weight,points)
        ! call quadrature_2D_VioreanuRokhlin_order024 (npts,weight,points)
        ! call quadrature_2D_VioreanuRokhlin_order033 (npts,weight,points)
        if(order <= 4) then
          str_formula = 'Vioreanu-Rokhlin'
          call quadrature_2D_VioreanuRokhlin_order012(npts,weight,points)
        else
          if(order <= 8) then
            str_formula = 'Gauss-Lobatto'
            call quadrature_2D_GaussLobatto_order024 (npts,weight,points)
          else
            if(order <= 17) then
              str_formula = 'Vioreanu-Rokhlin'
              call quadrature_2D_VioreanuRokhlin_order033(npts,weight,points)
            else
              ctx_err%msg   = "** ERROR: There is no implementation "// &
                              "of quadrauture rules for polynomials "// &
                              "order > 17 in two dimensions. "       // &
                              "[quadrature_init] **"
              ctx_err%ierr  = -202
              ctx_err%critic=.true.
              call raise_error(ctx_err)
            end if
          end if
        endif

      !! 3 dimensions ==================================================
      case(3)
        !! --------------------------------------------------------------
        !! Reference element with POL_KIND        double   |  quadruple |
        !!                                 order           |     16     |
        !! --------------------------------------------------------------
        !! validation with POL_KIND               double   |  quadruple |
        !! possible options:                                            |
        !!  -> Gauss--Lobatto    :   45 points |    4      |       4    |
        !!  -> Gauss             :  455 points | ~8-9      |   ~9-10    |
        !!  -> Vioreanu--Rokhlin :  120 points |    6      |   ~6- 7    |
        !!  -                       286 points |    9      |      10    |
        !!  -> Grundmann--Moeller:  125 points |    6      |       6    |
        !!  -                       325 points |    8      |       9    |
        !! --------------------------------------------------------------
        ! ********************************************************************************************************************************
        ! REFERENCE ELEMENT SEEMS OK UP TO P16 =================================
        ! filename = '../../quadrature-files/quadrature_VioreanuRokhlin_3D_order010_exact-to-15_00286_w'  !!! => seems ok up to order 10 ?
        ! ********************************************************************************************************************************
        ! call quadrature_3D_GaussLobatto_order008    (npts,weight,points)
        ! call quadrature_3D_Gauss                    (npts,weight,points)
        ! call quadrature_3D_VioreanuRokhlin_order015(npts,weight,points)
        ! call quadrature_3D_VioreanuRokhlin_order010(npts,weight,points)
        ! call quadrature_3D_GrundmannMoeller_order011(npts,weight,points)
        ! call quadrature_3D_GrundmannMoeller_order015(npts,weight,points)
        if(order <= 3) then
          str_formula = 'Gauss-Lobatto'
          call quadrature_3D_GaussLobatto_order008    (npts,weight,points)
        else
          if(order <= 5) then
            str_formula = 'Vioreanu-Rokhlin'
            call quadrature_3D_VioreanuRokhlin_order010(npts,weight,points)
          else
            if(order <= 10) then
              str_formula = 'Vioreanu-Rokhlin'
              call quadrature_3D_GrundmannMoeller_order015(npts,weight,points)
            else
              ctx_err%msg   = "** ERROR: There is no implementation "// &
                              "of quadrauture rules for polynomials "// &
                              "order > 10 in three dimensions. "     // &
                              "[quadrature_init] **"
              ctx_err%ierr  = -202
              ctx_err%critic=.true.
              call raise_error(ctx_err)
            end if
          end if
        endif

      case default
        ctx_err%msg   = "** ERROR: unrecognized dimension for "     //  & 
                        "quadrature formula [quadrature_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
  end subroutine quadrature_init

end module m_quadrature_init
