!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE module m_paraview_header_vtu
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save the header of a PARAVIEW VTU format:
!> every cpu writes the infos on the mesh: connectivity, list of 
!> nodes and cell, etc...
!
!------------------------------------------------------------------------------
module m_paraview_header_vtu

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_dg_lagrange_simplex_dof,only: lagrange_simplex_dof_coo,         &
                                      lagrange_simplex_paraview_connectivity
  use m_define_precision,       only: IKIND_MESH,RKIND_MESH,IKIND_MAT
  !! -------------------------------------------------------------------
  implicit none

  !! -------------------------------------------------------------------
  !! list of paraview types have been found from
  !! https://vtk.org/doc/nightly/html/vtkCellType_8h_source.html
  !! -------------------------------------------------------------------
  integer, parameter :: VTK_EMPTY_CELL                       =  0
  integer, parameter :: VTK_VERTEX                           =  1
  integer, parameter :: VTK_POLY_VERTEX                      =  2
  integer, parameter :: VTK_LINE                             =  3
  integer, parameter :: VTK_POLY_LINE                        =  4
  integer, parameter :: VTK_TRIANGLE                         =  5
  integer, parameter :: VTK_TRIANGLE_STRIP                   =  6
  integer, parameter :: VTK_POLYGON                          =  7
  integer, parameter :: VTK_PIXEL                            =  8
  integer, parameter :: VTK_QUAD                             =  9
  integer, parameter :: VTK_TETRA                            = 10
  integer, parameter :: VTK_VOXEL                            = 11
  integer, parameter :: VTK_HEXAHEDRON                       = 12
  integer, parameter :: VTK_WEDGE                            = 13
  integer, parameter :: VTK_PYRAMID                          = 14
  integer, parameter :: VTK_PENTAGONAL_PRISM                 = 15
  integer, parameter :: VTK_HEXAGONAL_PRISM                  = 16 
  !! Quadratic, isoparametric cells
  integer, parameter :: VTK_QUADRATIC_EDGE                   = 21
  integer, parameter :: VTK_QUADRATIC_TRIANGLE               = 22
  integer, parameter :: VTK_QUADRATIC_QUAD                   = 23
  integer, parameter :: VTK_QUADRATIC_POLYGON                = 36
  integer, parameter :: VTK_QUADRATIC_TETRA                  = 24
  integer, parameter :: VTK_QUADRATIC_HEXAHEDRON             = 25
  integer, parameter :: VTK_QUADRATIC_WEDGE                  = 26
  integer, parameter :: VTK_QUADRATIC_PYRAMID                = 27
  integer, parameter :: VTK_BIQUADRATIC_QUAD                 = 28
  integer, parameter :: VTK_TRIQUADRATIC_HEXAHEDRON          = 29
  integer, parameter :: VTK_QUADRATIC_LINEAR_QUAD            = 30
  integer, parameter :: VTK_QUADRATIC_LINEAR_WEDGE           = 31
  integer, parameter :: VTK_BIQUADRATIC_QUADRATIC_WEDGE      = 32
  integer, parameter :: VTK_BIQUADRATIC_QUADRATIC_HEXAHEDRON = 33
  integer, parameter :: VTK_BIQUADRATIC_TRIANGLE             = 34
  !! Cubic, isoparametric cell
  integer, parameter :: VTK_CUBIC_LINE                       = 35
  !! Special class of cells formed by convex group of points
  integer, parameter :: VTK_CONVEX_POINT_SET                 = 41
  !! Polyhedron cell (consisting of polygonal faces)
  integer, parameter :: VTK_POLYHEDRON                       = 42
  !! Higher order cells in parametric form
  integer, parameter :: VTK_PARAMETRIC_CURVE                 = 51
  integer, parameter :: VTK_PARAMETRIC_SURFACE               = 52
  integer, parameter :: VTK_PARAMETRIC_TRI_SURFACE           = 53
  integer, parameter :: VTK_PARAMETRIC_QUAD_SURFACE          = 54
  integer, parameter :: VTK_PARAMETRIC_TETRA_REGION          = 55
  integer, parameter :: VTK_PARAMETRIC_HEX_REGION            = 56
  !! Higher order cells
  integer, parameter :: VTK_HIGHER_ORDER_EDGE                = 60
  integer, parameter :: VTK_HIGHER_ORDER_TRIANGLE            = 61
  integer, parameter :: VTK_HIGHER_ORDER_QUAD                = 62
  integer, parameter :: VTK_HIGHER_ORDER_POLYGON             = 63
  integer, parameter :: VTK_HIGHER_ORDER_TETRAHEDRON         = 64
  integer, parameter :: VTK_HIGHER_ORDER_WEDGE               = 65
  integer, parameter :: VTK_HIGHER_ORDER_PYRAMID             = 66
  integer, parameter :: VTK_HIGHER_ORDER_HEXAHEDRON          = 67 
  !! Arbitrary order Lagrange elements (formulated separated from 
  !!                                    generic higher order cells)
  integer, parameter :: VTK_LAGRANGE_CURVE                   = 68
  integer, parameter :: VTK_LAGRANGE_TRIANGLE                = 69
  integer, parameter :: VTK_LAGRANGE_QUADRILATERAL           = 70
  integer, parameter :: VTK_LAGRANGE_TETRAHEDRON             = 71
  integer, parameter :: VTK_LAGRANGE_HEXAHEDRON              = 72
  integer, parameter :: VTK_LAGRANGE_WEDGE                   = 73
  integer, parameter :: VTK_LAGRANGE_PYRAMID                 = 74
  !! -------------------------------------------------------------------

  !> limit orders for Paraview so far ..................................
  integer     , parameter               :: VTK_OMAX_2D =  50 !! looks good.
  integer     , parameter               :: VTK_OMAX_3D =   5

  private
  public  :: paraview_header_vtu, paraview_close_vtu
  public  :: VTK_OMAX_2D, VTK_OMAX_3D

  contains
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save mesh infos in the vtu file (only one time per .vtu)
  !> we will save the actual arrays later because there can 
  !> be several arrays associated with a single mesh
  !
  !> @param[in]    str_representation: representation for solution (dof or cell)
  !> @param[in]    vtu_filename     : name of the vtu file
  !> @param[in]    shortname         : shortname for arrays saved
  !> @param[in]    fieldname         : name of the current field
  !> @param[in]    mesh              : mesh infos
  !----------------------------------------------------------------------------
  subroutine paraview_header_vtu(ctx_mesh,ctx_disc,str_representation,  &
                                 vtu_filename,shortname,fieldname,      &
                                 nfield,rkind_sol,flag_cx,              &
                                 flag_cste_order,cste_order,ctx_err)
    type(t_domain)              , intent(in)   :: ctx_mesh
    type(t_discretization)      , intent(in)   :: ctx_disc
    character(len=*)            , intent(in)   :: str_representation
    character(len=*),             intent(in)   :: vtu_filename,shortname
    character(len=*),allocatable, intent(in)   :: fieldname(:)
    integer                     , intent(in)   :: rkind_sol
    integer                     , intent(in)   :: nfield
    logical                     , intent(in)   :: flag_cx,flag_cste_order
    integer                     , intent(in)   :: cste_order
    type(t_error)               , intent(out)  :: ctx_err
    !! local 
    character(len=256)                    :: shortname_loc,local_name
    character(len=16)                     :: intchar1,intchar2
    character(len=16)                     :: str_data
    character(len=2)                      :: char_realmesh,char_intmesh
    character(len=2)                      :: char_realsol
    integer                               :: io,ifield,k,mem
    integer(kind=IKIND_MESH)              :: i_cell,n_val
    integer                               :: ndof_vol,order,i_order
    integer                               :: ndof_vol_cste
    integer     , parameter               :: unit_io=21
    integer     , parameter               :: ikind_mem=4 !! forced by paraview
    integer(kind=1)                       :: cell_type_vtk
    integer(kind=IKIND_MESH)              :: counter,i_k
    integer(kind=IKIND_MESH)              :: offsetloc,n_node
    integer(kind=IKIND_MESH), allocatable :: connection(:),connec_loc(:)
    integer(kind=IKIND_MESH), allocatable :: offset_connex(:)
    real   (kind=RKIND_MESH), allocatable :: coo(:,:)
    real   (kind=8)         , allocatable :: node_coo(:,:),xdof(:,:)
    character(1), parameter               :: endline = char(10) 

    ctx_err%ierr=0
  
    !! only work for simplex for now >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(trim(adjustl(ctx_disc%format_cell)) .ne. 'simplex') then
        ctx_err%msg   = "** ERROR: only simplex are supported " //      &
                        "[paraview_header_vtu] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err) 
    end if
    !! -----------------------------------------------------------------


    !! -----------------------------------------------------------------
    !! in case we have a constant order, we evaluate the ndof per cell
    ndof_vol_cste = 0
    if( flag_cste_order ) then 
      if(cste_order < 0) then
        ctx_err%msg   = "** ERROR: trying to save dof with order " //   &
                        "< 0 in [paraview_header_vtu] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err) 
      end if      
      !! compute the constant ndof
      select case(ctx_mesh%dim_domain)
        case(1)
          ndof_vol_cste = (cste_order+1)
        case(2)
          ndof_vol_cste = (cste_order+2)*(cste_order+1)/2 
        case(3)
          ndof_vol_cste = (cste_order+3)*(cste_order+2)*(cste_order+1)/6
        case default
          ctx_err%msg   = "** ERROR: unrecognized dim [paraview_header_vtu] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if
    !! -----------------------------------------------------------------
    
    !! the number of values per array and data type depend on the 
    !! representation 
    str_data=''
    n_val=0
    select case(trim(adjustl(str_representation)))
      case('cell') !! one value per cell corner
        n_node      = ctx_mesh%n_cell_loc*ctx_mesh%n_node_per_cell
        n_val       = ctx_mesh%n_cell_loc
        str_data    ='CellData'
      case('dof', 'dof-orderfix')  !! one value per dof

        !! for dof-orderfix, we must have the order.....................
        if(trim(adjustl(str_representation)) == 'dof-orderfix' .and.    &
           .not.(flag_cste_order)) then
          ctx_err%msg   = "** ERROR: representation with multiple " //  &
                          "constant per cell requires a fixed order " //&
                          "[paraview_header_vtu] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err) 
        end if

        str_data    = 'PointData'
        
        ! ---------------------------------------------------------
        ! ---------------------------------------------------------
        !! WARNING: I CANNOT HANDLE PARAVIEW 5.8 LAGRANGE FOR 
        !! DIMENSION 2 WITH ORDER > ?
        !! DIMENSION 3 WITH ORDER > 5
        !! WE FORCE ORDER 1 FOR THE CELL WITH SUCH CHARACTERISTICS
        ! ---------------------------------------------------------
        ! ---------------------------------------------------------
        if(flag_cste_order) then !! constant order check ---------------
          if    (ctx_disc%dim_domain==2 .and. cste_order > VTK_OMAX_2D) then
            n_node = 3 * ctx_mesh%n_cell_loc
          elseif(ctx_disc%dim_domain==3 .and. cste_order > VTK_OMAX_3D) then 
            n_node = 4 * ctx_mesh%n_cell_loc
          else
            n_node = ndof_vol_cste * ctx_mesh%n_cell_loc
          end if

        else !! varying orders
         
          n_node = int(ctx_disc%n_dofloc_persol) !! force kind=4
          if(ctx_disc%dim_domain == 2 .and.                             &
             ctx_disc%order_max_gb > VTK_OMAX_2D) then
            n_node=0
            do i_cell=1,ctx_mesh%n_cell_loc
              order      = ctx_mesh%order(i_cell)
              if(order > VTK_OMAX_2D) then
                n_node = n_node + 3
              else
                i_order    = ctx_disc%order_to_index (order)
                ndof_vol   = ctx_disc%n_dof_per_order(i_order)
                n_node     = n_node + ndof_vol
              end if
            end do
          end if
  
          if(ctx_disc%dim_domain == 3 .and.                             &
             ctx_disc%order_max_gb > VTK_OMAX_3D) then
            n_node=0
            do i_cell=1,ctx_mesh%n_cell_loc
              order      = ctx_mesh%order(i_cell)
              if(order > VTK_OMAX_3D) then
                n_node = n_node + 4
              else
                i_order    = ctx_disc%order_to_index (order)
                ndof_vol   = ctx_disc%n_dof_per_order(i_order)
                n_node     = n_node + ndof_vol
              end if
            end do
          end if
        end if
        n_val = n_node
        ! ---------------------------------------------------------        
        
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh array "   //       &  
                        "representation "                      //       &
                        trim(adjustl(str_representation))      //       &
                        " in [paraview_header_vtu] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)             
    end select
    !! -----------------------------------------------------------------

    !! local int and real sizes
    if(RKIND_MESH==4) char_realmesh='32'
    if(RKIND_MESH==8) char_realmesh='64'
    if(IKIND_MESH==4) char_intmesh ='32'
    if(IKIND_MESH==8) char_intmesh ='64'
    select case(rkind_sol)
      case(8)
        char_realsol='64'
      case(4)
        char_realsol='32'
    end select
    if((RKIND_MESH .ne. 4 .and. RKIND_MESH .ne. 8) .or. &
       (IKIND_MESH .ne. 4 .and. IKIND_MESH .ne. 8) .or. &
       (rkind_sol  .ne. 4 .and. rkind_sol  .ne. 8)) then
      ctx_err%msg   = "** ERROR: Unrecognized number kind  [vtu_header] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! -----------------------------------------------------------------
    !! open file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    open(unit_io,file=trim(adjustl(vtu_filename)),action='write',       &
         form='unformatted',access='stream',convert='big_endian',       &
         iostat=io,status='replace') !! (replace if existing)
    
    !! write header infos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    write(unit_io,iostat=io)'<?xml version="1.0"?>'//endline
    write(unit_io,iostat=io)'<VTKFile type="UnstructuredGrid" '     //  &
                            'version="0.1" byte_order="BigEndian">' //endline
    write(unit_io,iostat=io)'<UnstructuredGrid>'//endline
    !! number of cell and node
    write(intchar1,'(i0)') n_node
    write(intchar2,'(i0)') ctx_mesh%n_cell_loc
    write(unit_io,iostat=io)'  <Piece NumberOfPoints="'//trim(intchar1)// &
                              '" NumberOfCells="'      //trim(intchar2)// &
                            '">'//endline
    !! the dof ("point") >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    write(unit_io,iostat=io)'    <Points>'//endline
    write(unit_io,iostat=io)'      <DataArray type="Float',char_realmesh,&
                            '" NumberOfComponents="3" Name="Point" '//   &
                            'format="appended" offset="0">'//endline
    write(unit_io,iostat=io)'      </DataArray>'//endline
    write(unit_io,iostat=io)'    </Points>'     //endline
    ! Npoints x Ncoordinates (== x,y,z) x size  + 1 integer for size
    offsetloc = (n_node)*3*RKIND_MESH + ikind_mem
    
    !! >>>>>>>>> the cells infos include: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! >>>>>>>>>   - connectivity (for cell organization) >>>>>>>>>>>>>>
    !! >>>>>>>>>   - offset of cell                       >>>>>>>>>>>>>>
    !! >>>>>>>>>   - type of cell                         >>>>>>>>>>>>>>
    write(intchar1,'(i0)') offsetloc
    write(unit_io,iostat=io)'    <Cells>'//endline
    write(unit_io,iostat=io)'      <DataArray type="Int',char_intmesh,  &
                            '" Name="connectivity" format="appended" '//&
                            'offset="'//trim(intchar1)//'">'//endline
    !! Nnode x size + 1 int for size
    offsetloc = offsetloc + n_node*IKIND_MESH + ikind_mem
    write(intchar1,'(i0)') offsetloc
    write(unit_io,iostat=io)'      </DataArray>'//endline
    write(unit_io,iostat=io)'      <DataArray type="Int',char_intmesh,  &
                            '" Name="offsets" format="appended" '    // &
                            'offset="'//trim(intchar1)//'">'//endline
    write(unit_io,iostat=io)'      </DataArray>'//endline
    !! 1 offset per cell + 1 int for size 
    offsetloc = offsetloc + ctx_mesh%n_cell_loc*IKIND_MESH + ikind_mem
    write(intchar1,'(i0)') offsetloc
    !! force int8 for types of cell (cf parameter int above)
    write(unit_io,iostat=io)'      <DataArray type="Int8" Name="types"'//&
                            ' format="appended" offset="'//              &
                            trim(intchar1)//'">'//endline
    write(unit_io,iostat=io)'      </DataArray>'//endline
    write(unit_io,iostat=io)'    </Cells>'      //endline
    !! force int1, one per cell + 1 int for size
    offsetloc = offsetloc + ctx_mesh%n_cell_loc*(1)  + ikind_mem
    
    !! -----------------------------------------------------------------
    !! list of components
    !! -----------------------------------------------------------------
    write(unit_io,iostat=io)'    <'//trim(adjustl(str_data))//          &
                            ' Scalars="'//trim(adjustl(shortname))//    &
                            '">'   //endline

    !! loop over local name
    do ifield=1,nfield
      !! shortname is given by the string found before the first '_'
      local_name = trim(adjustl(fieldname(ifield)))
      shortname_loc=''
      do k=1,len_trim(adjustl(local_name))
        if(local_name(k:k) == "_") exit
      end do
      shortname_loc=''
      shortname_loc(1:k-1) = local_name(1:k-1)
      
      !! possibly real and imaginary part 
      write(intchar1,'(i0)') offsetloc
      
      if(flag_cx) then
        write(unit_io,iostat=io)'      <DataArray type="Float',         &
                                char_realsol,'" Name="Real('  //        &
                                trim(shortname_loc)//')"'     //        &
                                ' format="appended" offset="' //        &
                                trim(intchar1)//'">'  // endline
        write(unit_io,iostat=io) '      </DataArray>' // endline
        offsetloc = offsetloc + n_val*rkind_sol + ikind_mem
        write(intchar1,'(i0)') offsetloc 
        write(unit_io,iostat=io)'      <DataArray type="Float',         &
                                char_realsol,'" Name="Imag('  //        &
                                trim(shortname_loc)//')"'     //        &
                                ' format="appended" offset="' //        &
                                trim(intchar1)//'">'  // endline
        write(unit_io,iostat=io) '      </DataArray>' // endline
        offsetloc = offsetloc + n_val*rkind_sol + ikind_mem
      else
        write(unit_io,iostat=io)'      <DataArray type="Float',         &
                                char_realsol,'" Name="'      //         &
                                trim(shortname_loc)//'"'     //         &
                                ' format="appended" offset="'//         &
                                trim(intchar1)//'">'  // endline
        write(unit_io,iostat=io) '      </DataArray>' // endline
        offsetloc = offsetloc + n_val*rkind_sol + ikind_mem
      endif
      
    end do
    write(unit_io,iostat=io)'    </'//trim(adjustl(str_data))//'>'//endline
    write(unit_io,iostat=io)'  </Piece>'                          //endline
    write(unit_io,iostat=io)'</UnstructuredGrid>'                 //endline
    write(unit_io,iostat=io)'<AppendedData encoding="raw">'       //endline   
    write(unit_io,iostat=io)'_'


    !! -----------------------------------------------------------------
    !! we now need the mesh infos for paraview: 
    !!  - coordinates of all dof (or just nodes for cell visu)
    !!  - connectivity: all dof must appear
    !!  - offset: basically indicates per cell the number of dof
    !! -----------------------------------------------------------------
    !! indicate the anticipated size: 3 coordinates for all node
    mem = int(3*n_node*RKIND_MESH) !! force int4 for paraview
    write(unit_io,iostat=io) mem
    
    !! coordinates  of all dof, 
    !! connectivity where all dof must appear,
    !! offset       per cell.
    allocate(coo          (n_node,3))
    allocate(offset_connex(ctx_mesh%n_cell_loc))
    allocate(connection   (n_node))
    coo          =0.0
    offset_connex=0
    connection   =0    
    
    !! depends on the representation 
    counter = 0
    select case(trim(adjustl(str_representation)))
      case('cell')

        select case(ctx_mesh%dim_domain)
          case(1) !! 1D domain >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            do i_cell=1,ctx_mesh%n_cell_loc
              !! loop over nodes 
              do k=1,ctx_mesh%n_node_per_cell
                counter = counter + 1
                coo(counter,1) = ctx_mesh%x_node(1,ctx_mesh%cell_node(k,i_cell))
                coo(counter,2) = 0._RKIND_MESH
                coo(counter,3) = 0._RKIND_MESH
                !! WARNING PARAVIEW INDEX STARTS AT 0
                connection(counter) = counter - 1 
              end do
              offset_connex(i_cell) = 2*i_cell
            end do

          case(2) !! 2D domain >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            do i_cell=1,ctx_mesh%n_cell_loc
              !! loop over nodes 
              do k=1,ctx_mesh%n_node_per_cell
                counter = counter + 1
                coo(counter,1) = ctx_mesh%x_node(1,ctx_mesh%cell_node(k,i_cell))
                coo(counter,2) = 0._RKIND_MESH
                coo(counter,3) = ctx_mesh%x_node(2,ctx_mesh%cell_node(k,i_cell))
                !! WARNING PARAVIEW INDEX STARTS AT 0
                connection(counter) = counter - 1 
              end do
              offset_connex(i_cell) = 3*i_cell
            end do
          
          case(3) !! 3D domain >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            do i_cell=1,ctx_mesh%n_cell_loc
              !! loop over nodes 
              do k=1,ctx_mesh%n_node_per_cell
                counter = counter + 1
                coo(counter,1) = ctx_mesh%x_node(1,ctx_mesh%cell_node(k,i_cell))
                coo(counter,2) = ctx_mesh%x_node(2,ctx_mesh%cell_node(k,i_cell))
                coo(counter,3) = ctx_mesh%x_node(3,ctx_mesh%cell_node(k,i_cell))
                !! WARNING PARAVIEW INDEX STARTS AT 0
                connection(counter) = counter - 1 
              end do
              offset_connex(i_cell) = 4*i_cell
            enddo
          
          case default
            ctx_err%msg   = "** ERROR: unrecognized mesh dimension "//&
                      "[paraview_header_vtu] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
        
      !! one value per dof =============================================
      case('dof', 'dof-orderfix') 
        ! ---------------------------------------------------------
        !! One value per dof
        ! ---------------------------------------------------------
        !! depends on the cell order
        select case(ctx_mesh%dim_domain)

          case(2) !! 2D domain >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            allocate(node_coo(2,ctx_mesh%n_node_per_cell))
            do i_cell=1,ctx_mesh%n_cell_loc
              
              !! get positions of all dof and connectivity infos
              if(flag_cste_order) then
                order = cste_order
              else
                order = ctx_mesh%order(i_cell)
              end if
              ! ---------------------------------------------------
              ! ---------------------------------------------------
              !! WARNING: I CANNOT HANDLE PARAVIEW 5.8 LAGRANGE FOR 
              !! DIMENSION 2 WITH ORDER > VTK_OMAX_2D
              !! WE FORCE ORDER 1 FOR THE CELL WITH SUCH CHARACTERISTICS
              ! ---------------------------------------------------
              ! ---------------------------------------------------
              if(order > VTK_OMAX_2D) then !! force order 1
                ndof_vol   = 3
                allocate(xdof    (2,ndof_vol))
                allocate(connec_loc(ndof_vol))
                xdof(:,:) = dble(ctx_mesh%x_node(:,                     &
                                    ctx_mesh%cell_node(:,i_cell)))
                connec_loc = (/1, 2, 3/)
              else
                if(flag_cste_order) then
                  ndof_vol   = ndof_vol_cste
                else
                  i_order    = ctx_disc%order_to_index (order)
                  ndof_vol   = ctx_disc%n_dof_per_order(i_order)
                end if
                allocate(xdof    (2,ndof_vol))
                allocate(connec_loc(ndof_vol))
                node_coo(:,:)= dble(ctx_mesh%x_node(:,                  &
                                    ctx_mesh%cell_node(:,i_cell)))
                call lagrange_simplex_dof_coo(ctx_mesh%dim_domain,      &
                                              node_coo,order,ndof_vol,  &
                                              xdof,ctx_err)
                call lagrange_simplex_paraview_connectivity(            &
                                      ctx_mesh%dim_domain,order,        &
                                      connec_loc,ctx_err)
              end if

              !! we save all coordinates
              do k=1,ndof_vol
                counter = counter + 1
                coo(counter,1) = real(xdof(1,k),kind=RKIND_MESH)
                coo(counter,2) = 0._RKIND_MESH
                coo(counter,3) = real(xdof(2,k),kind=RKIND_MESH)
                !! we save the connectivity
                !! WARNING PARAVIEW INDEX STARTS AT 0
                if(i_cell==1) then
                  connection(counter) = connec_loc(k) - 1 
                else
                  connection(counter) = connec_loc(k) - 1 +             &
                                        offset_connex(i_cell-1)
                end if
              end do
              if(i_cell==1) then
                offset_connex(i_cell) = ndof_vol
              else
                offset_connex(i_cell) = offset_connex(i_cell-1) + ndof_vol
              end if
              
              deallocate(xdof)
              deallocate(connec_loc)

            end do
            deallocate(node_coo)

          case(3) !! 3D domain >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            allocate(node_coo(3,ctx_mesh%n_node_per_cell))
            
            do i_cell=1,ctx_mesh%n_cell_loc
              !! get positions of all dof and connectivity infos
              if(flag_cste_order) then
                order = cste_order
              else
                order = ctx_mesh%order(i_cell)
              end if
              ! ---------------------------------------------------
              ! ---------------------------------------------------
              !! WARNING: I CANNOT HANDLE PARAVIEW 5.8 LAGRANGE FOR
              !! DIMENSION 3 WITH ORDER > ?
              !! WE FORCE ORDER 1 FOR THE CELL WITH SUCH CHARACTERISTICS
              ! ---------------------------------------------------
              ! ---------------------------------------------------
              if(order > VTK_OMAX_3D) then !! force order 1
                ndof_vol   = 4
                allocate(xdof    (3,ndof_vol))
                allocate(connec_loc(ndof_vol))
                xdof(:,:) = dble(ctx_mesh%x_node(:,                     &
                                    ctx_mesh%cell_node(:,i_cell)))
                connec_loc = (/1, 2, 3, 4/)
              else
                if(flag_cste_order) then
                  ndof_vol   = ndof_vol_cste
                else
                  i_order    = ctx_disc%order_to_index (order)
                  ndof_vol   = ctx_disc%n_dof_per_order(i_order)
                end if

                allocate(xdof    (3,ndof_vol))
                allocate(connec_loc(ndof_vol))
                node_coo(:,:)= dble(ctx_mesh%x_node(:,                  &
                                    ctx_mesh%cell_node(:,i_cell)))

                call lagrange_simplex_dof_coo(ctx_mesh%dim_domain,      &
                                              node_coo,order,ndof_vol,  &
                                              xdof,ctx_err)
                call lagrange_simplex_paraview_connectivity(            &
                                      ctx_mesh%dim_domain,order,        &
                                      connec_loc,ctx_err)
              end if

              !! we save all coordinates
              do k=1,ndof_vol
                counter = counter + 1
                coo(counter,1) = real(xdof(1,k),kind=RKIND_MESH)
                coo(counter,2) = real(xdof(2,k),kind=RKIND_MESH)
                coo(counter,3) = real(xdof(3,k),kind=RKIND_MESH)
                !! we save the connectivity
                !! WARNING PARAVIEW INDEX STARTS AT 0
                if(i_cell==1) then
                  connection(counter) = connec_loc(k) - 1 
                else
                  connection(counter) = connec_loc(k) - 1 +             &
                                        offset_connex(i_cell-1)
                end if
              end do
              if(i_cell==1) then
                offset_connex(i_cell) = ndof_vol
              else
                offset_connex(i_cell) = offset_connex(i_cell-1) + ndof_vol
              end if
              
              deallocate(xdof)
              deallocate(connec_loc)

            end do
            deallocate(node_coo)

          case default
            ctx_err%msg   = "** ERROR: unrecognized mesh dimension "//&
                      "[paraview_header_vtu] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh array "   // & 
                        "representation in [paraview_header_vtu] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)             
    end select
    !! write in file
    write(unit_io,iostat=io) (coo(i_k,1),coo(i_k,2),coo(i_k,3),i_k=1,n_node)
    deallocate(coo)
    !! -----------------------------------------------------------------
    !! End of points coordinates
    !! -----------------------------------------------------------------


    !! -----------------------------------------------------------------
    !! Connectivity info for the current mesh: how nodes are 
    !! connected to define cells
    !! -----------------------------------------------------------------
    mem = int(n_node*IKIND_MESH)  !! force int4 for paraview
    write(unit_io,iostat=io) mem
    !! WARNING INDEX STARTS AT 0
    write(unit_io,iostat=io) connection(:)
    deallocate(connection)

    !! -----------------------------------------------------------------
    !! Offsets infos
    !! -----------------------------------------------------------------
    mem = int(ctx_mesh%n_cell_loc*IKIND_MESH) !! force int4 for paraview
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) offset_connex(:)
    deallocate(offset_connex)
    
    !! -----------------------------------------------------------------
    !! type of all elements: cf. above list at the top of the file
    !! -----------------------------------------------------------------    
    cell_type_vtk=0
    select case(trim(adjustl(ctx_mesh%format_element)))
      case('triangle')
        select case(trim(adjustl(str_representation)))
          case('cell')
            cell_type_vtk = VTK_TRIANGLE
          case('dof','dof-orderfix')
            cell_type_vtk = VTK_LAGRANGE_TRIANGLE
          case default
            ctx_err%msg   = "** ERROR: unrecognized triangle " //       &
                            "model representation [paraview_header_vtu] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select

      case('tetrahedron')
        select case(trim(adjustl(str_representation)))
          case('cell')
            cell_type_vtk = VTK_TETRA
          case('dof','dof-orderfix')
            cell_type_vtk = VTK_LAGRANGE_TETRAHEDRON
          case default
            ctx_err%msg   = "** ERROR: unrecognized tetrahedron " //    &
                            "model representation [paraview_header_vtu] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
    end select
     
    if(cell_type_vtk .eq. 0) then
      ctx_err%msg   = "** ERROR: unrecognized mesh cell type "     // & 
                      "for paraview in [paraview_header_vtu] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    endif
    !! we have forced int 1
    mem = int(ctx_mesh%n_cell_loc * 1)  !! force int4 for paraview
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) (cell_type_vtk,i_k=1,ctx_mesh%n_cell_loc)
    
    !! -----------------------------------------------------------------
    !! close file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    close(unit_io)
    return
  end subroutine paraview_header_vtu


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> closure for vtu file
  !
  !> @param[in]    filename          : name of the file
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine paraview_close_vtu(vtu_filename,ctx_err)
    character(len=*),             intent(in)   :: vtu_filename
    type(t_error)               , intent(out)  :: ctx_err
    !! local 
    integer                        :: io,unit_io=107
    character(1), parameter        :: endline = char(10) 

    ctx_err%ierr=0
    !! open file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    open(unit_io,file=trim(adjustl(vtu_filename)),action='write',       &
         form='unformatted',access='stream',convert='big_endian',       &
         iostat=io,position='append',status='old')
    write(unit_io,iostat=io) endline
    write(unit_io,iostat=io) '</AppendedData>'//endline
    write(unit_io,iostat=io) '</VTKFile>'     //endline
    !! close file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    close(unit_io)
    return
  end subroutine paraview_close_vtu

end module m_paraview_header_vtu
