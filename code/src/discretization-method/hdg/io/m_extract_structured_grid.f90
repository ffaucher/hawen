!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_extract_structured_grid.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO to extract a structured output in the case
!> we have a solution given on a mesh (per cell or per dof).
!> It basically converts the unstructured data towards the structured 
!> grid using the field cartesian_map of ctx_discretization.
!
!------------------------------------------------------------------------------
module m_extract_structured_grid

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_define_precision,         only: IKIND_MESH,RKIND_POL
  use m_ctx_parallelism,          only: t_parallelism
  use m_barrier,                  only: barrier
  use m_reduce_sum,               only: reduce_sum
  use m_ctx_domain,               only: t_domain
  use m_ctx_discretization,       only: t_discretization
  !! for model per dof
  use m_basis_functions,          only: t_basis, basis_construct, basis_clean
  use m_polynomial,               only: polynomial_eval
  !! -------------------------------------------------------------------
  implicit none

  interface extract_structured_grid
     module procedure extract_structured_grid_real4
     module procedure extract_structured_grid_real8
     module procedure extract_structured_grid_complex4
     module procedure extract_structured_grid_complex8
  end interface
  
  private
  public  :: extract_structured_grid

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Extract the structured cartesian solution from input array given
  !> by cell or by dof. 
  !> We also gives the basic infos of the grid (nx,ny,nz,dx,dy,dz).
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_disc          : type discretization
  !> @param[in]    array_in          : array to be saved of size (:,narray)
  !> @param[in]    iarray            : current array to process
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[inout] array_out         : output cartesian grid
  !> @param[out]   n{XYZ}_gb         : size of the grid
  !> @param[out]   o{XYZ}            : origin of the grid
  !> @param[out]   d{XYZ}            : step of the grid
  !> @param[in]    order_subcell     : in case of multiple points per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine extract_structured_grid_real4(ctx_paral,ctx_domain,        &
                                           ctx_disc,array_in,iarray,    &
                                           str_representation,array_out,&
                                           nx_gb,ny_gb,nz_gb,ox,oy,oz,  &
                                           dx,dy,dz,order_subcell,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real,allocatable            ,intent(in)   :: array_in (:,:)
    real,allocatable            ,intent(inout):: array_out(:,:,:)
    integer                     ,intent(in)   :: iarray
    integer                     ,intent(in)   :: order_subcell
    integer                     ,intent(out)  :: nx_gb,ny_gb,nz_gb
    real(kind=8)                ,intent(out)  :: ox,oy,oz,dx,dy,dz
    character(len=*)            ,intent(in)   :: str_representation
    type(t_error)               ,intent(inout):: ctx_err

    !! local
    !! -----------------------------------------------------------------
    integer                  :: ix,iy,iz,ixloc,iyloc,izloc
    integer(kind=IKIND_MESH) :: i_cell
    integer                  :: nx_loc,ny_loc,nz_loc
    integer                  :: ixmin,ixmax,iymin,iymax,izmin,izmax
    integer                  :: ixmin_off,iymin_off,izmin_off
    integer                  :: ixmax_off,iymax_off,izmax_off
    integer                  :: i_order,idof,ndof_vol,order,offset_dof
    real,allocatable         :: model_loc(:,:,:)
    real(kind=8)             :: coeff
    !! for model per dof
    type(t_basis)            :: basisfunc
    real(kind=8)        ,allocatable:: xpos(:)
    real(kind=RKIND_POL),allocatable:: coo_ref(:)
    real(kind=RKIND_POL)     :: vloc
    real(kind=8)             :: mval
    integer                  :: ind_gb, i_dof
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! -----------------------------------------------------------------
    !! We assemble the global array to be saved:
    !! --> mesh      with info per cell
    !! --> mesh      with info per node
    !! -----------------------------------------------------------------
    !! init values depending on the type of grid
    nx_gb=0; ny_gb=0; nz_gb=0; nx_loc=0; ny_loc=0; nz_loc=0; ixmin=0 
    iymin=0; izmin=0; ixmax=0; iymax=0; izmax=0; ixmin_off=0; iymin_off=0
    izmin_off=0; ixmax_off=0; iymax_off=0; izmax_off=0; dx=0; dy=0; dz=0
      
    !! Assemble from mesh infos using ctx_domain%cartesian_map >>
    nx_gb =ctx_disc%cartesian_map%nx_gb
    ny_gb =ctx_disc%cartesian_map%ny_gb
    nz_gb =ctx_disc%cartesian_map%nz_gb
    nx_loc=ctx_disc%cartesian_map%nx_loc
    ny_loc=ctx_disc%cartesian_map%ny_loc
    nz_loc=ctx_disc%cartesian_map%nz_loc
    ixmin =ctx_disc%cartesian_map%ixmin_loc
    iymin =ctx_disc%cartesian_map%iymin_loc
    izmin =ctx_disc%cartesian_map%izmin_loc
    ixmax =ctx_disc%cartesian_map%ixmax_loc
    iymax =ctx_disc%cartesian_map%iymax_loc
    izmax =ctx_disc%cartesian_map%izmax_loc
    dx    =ctx_disc%cartesian_map%dx
    dy    =ctx_disc%cartesian_map%dy
    dz    =ctx_disc%cartesian_map%dz
    ox    =ctx_disc%cartesian_map%ox
    oy    =ctx_disc%cartesian_map%oy
    oz    =ctx_disc%cartesian_map%oz
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! allocate global model, only master will save here
    allocate(model_loc(nx_gb,ny_gb,nz_gb))
    if(ctx_paral%master) then
      allocate(array_out(nx_gb,ny_gb,nz_gb))
    else
      allocate(array_out(1,1,1))
    endif
    !! -----------------------------------------------------------------
    !! create approriate output for the input model, depending on the type
    !! process cartesian for mesh
    !! -----------------------------------------------------------------
    model_loc=0.0
    array_out=0.0
    select case(trim(adjustl(str_representation)))

      !! for model given per cells
      case('cell','node')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            model_loc(ix,iy,iz) = array_in(i_cell,iarray)
          end if
        end do ; end do ; end do

      !! for model given per dof
      case('dof')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! ndof for this cell
            order     = ctx_domain%order (i_cell)
            i_order   = ctx_disc%order_to_index (order)
            ndof_vol  = ctx_disc%n_dof_per_order(i_order)
            coeff     = 0.0
            offset_dof= int(ctx_disc%offsetdof_sol_loc(i_cell))
            do idof=1,ndof_vol
              coeff = coeff + dble(array_in(offset_dof+idof,iarray)    *   &
              ctx_disc%cartesian_map%weight(ixloc,iyloc,izloc)%array(idof))
            end do
            model_loc(ix,iy,iz) = real(coeff,kind=4)
          end if
        end do ; end do ; end do
      
      !! for model given with basis function at constant order.
      case('dof-orderfix')

        !! order must be > 0
        if(order_subcell < 0) then
          ctx_err%msg   = "** ERROR: the polynomial order for the " //  &
                          "fixed-order dof representation is < 0 "  //  &
                          "[extract_structured_grid] ** "
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
        
        !! create basis function at this order.
        call basis_construct(ctx_domain%dim_domain,order_subcell,       &
                             basisfunc,ctx_err)
        allocate(xpos   (ctx_domain%dim_domain))
        allocate(coo_ref(ctx_domain%dim_domain))
        
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! we need to evaluate the position on the reference element.
            xpos = 0.d0
            select case(ctx_domain%dim_domain)
              !! WE NEED TO OFFSET WITH THE IXMIN POSITION TOO.
              case(1)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                !! convert global position to ref elem position
                !! we have jac1D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(1) = xpos(1)                                    &
                  - ctx_domain%x_node(1,ctx_domain%cell_node(1,i_cell))
                coo_ref(1) = ctx_disc%inv_jac(1,1,i_cell)*dble(coo_ref(1))

              case(2)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(1:2,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))

              case(3)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oy + (iyloc-1)*dy + (iymin-1)*dy
                xpos(3) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac3D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(:,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))
              case default
                ctx_err%msg   = "** ERROR: Incorrect dimension "    //  &
                                "[extract_structured_grid] ** "
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
            
            !! evaluate basis function here
            mval = 0.d0
            ind_gb = (i_cell-1)*basisfunc%nphi
            do i_dof=1,basisfunc%nphi
              vloc = 0.d0
              call polynomial_eval(basisfunc%pol(i_dof),coo_ref,vloc,ctx_err)
              mval = mval + dble(vloc * array_in(ind_gb+i_dof,iarray))
            end do
            model_loc(ix,iy,iz) = real(mval,kind=4)
          end if
        end do ; end do ; end do
        deallocate(xpos)
        deallocate(coo_ref)
        call basis_clean(basisfunc)

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh array "   // & 
                        "representation in [extract_structured_grid] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)             
    end select
    ! ----------------------------------------------------------------
      
    !! reduce so that only master keeps the result
    call reduce_sum(model_loc,array_out,nx_gb*ny_gb*nz_gb,0,           &
                    ctx_paral%communicator,ctx_err)
    ! ----------------------------------------------------------------
    !! only master has the solution
    ! ----------------------------------------------------------------
    deallocate(model_loc)
    return
  end subroutine extract_structured_grid_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Extract the structured cartesian solution from input array given
  !> by cell or by dof. 
  !> We also gives the basic infos of the grid (nx,ny,nz,dx,dy,dz).
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_disc          : type discretization
  !> @param[in]    array_in          : array to be saved of size (:,narray)
  !> @param[in]    iarray            : current array to process
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[inout] array_out         : output cartesian grid
  !> @param[out]   n{XYZ}_gb         : size of the grid
  !> @param[out]   o{XYZ}            : origin of the grid
  !> @param[out]   d{XYZ}            : step of the grid
  !> @param[in]    order_subcell     : in case of multiple points per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine extract_structured_grid_real8(ctx_paral,ctx_domain,        &
                                           ctx_disc,array_in,iarray,    &
                                           str_representation,array_out,&
                                           nx_gb,ny_gb,nz_gb,ox,oy,oz,  &
                                           dx,dy,dz,order_subcell,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real(kind=8),allocatable    ,intent(in)   :: array_in (:,:)
    real(kind=8),allocatable    ,intent(inout):: array_out(:,:,:)
    integer                     ,intent(in)   :: iarray
    integer                     ,intent(in)   :: order_subcell
    integer                     ,intent(out)  :: nx_gb,ny_gb,nz_gb
    real(kind=8)                ,intent(out)  :: ox,oy,oz,dx,dy,dz
    character(len=*)            ,intent(in)   :: str_representation
    type(t_error)               ,intent(inout):: ctx_err

    !! local
    !! -----------------------------------------------------------------
    integer                  :: ix,iy,iz,ixloc,iyloc,izloc
    integer(kind=IKIND_MESH) :: i_cell
    integer                  :: nx_loc,ny_loc,nz_loc
    integer                  :: ixmin,ixmax,iymin,iymax,izmin,izmax
    integer                  :: ixmin_off,iymin_off,izmin_off
    integer                  :: ixmax_off,iymax_off,izmax_off
    integer                  :: i_order,idof,ndof_vol,order,offset_dof
    real(kind=8),allocatable :: model_loc(:,:,:)
    real(kind=8)             :: coeff
    !! for model per dof
    type(t_basis)            :: basisfunc
    real(kind=8)        ,allocatable:: xpos(:)
    real(kind=RKIND_POL),allocatable:: coo_ref(:)
    real(kind=RKIND_POL)     :: vloc
    real(kind=8)             :: mval
    integer                  :: ind_gb, i_dof
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! -----------------------------------------------------------------
    !! We assemble the global array to be saved:
    !! --> mesh      with info per cell
    !! --> mesh      with info per node
    !! -----------------------------------------------------------------
    !! init values depending on the type of grid
    nx_gb=0; ny_gb=0; nz_gb=0; nx_loc=0; ny_loc=0; nz_loc=0; ixmin=0 
    iymin=0; izmin=0; ixmax=0; iymax=0; izmax=0; ixmin_off=0; iymin_off=0
    izmin_off=0; ixmax_off=0; iymax_off=0; izmax_off=0; dx=0; dy=0; dz=0
      
    !! Assemble from mesh infos using ctx_domain%cartesian_map >>
    nx_gb =ctx_disc%cartesian_map%nx_gb
    ny_gb =ctx_disc%cartesian_map%ny_gb
    nz_gb =ctx_disc%cartesian_map%nz_gb
    nx_loc=ctx_disc%cartesian_map%nx_loc
    ny_loc=ctx_disc%cartesian_map%ny_loc
    nz_loc=ctx_disc%cartesian_map%nz_loc
    ixmin =ctx_disc%cartesian_map%ixmin_loc
    iymin =ctx_disc%cartesian_map%iymin_loc
    izmin =ctx_disc%cartesian_map%izmin_loc
    ixmax =ctx_disc%cartesian_map%ixmax_loc
    iymax =ctx_disc%cartesian_map%iymax_loc
    izmax =ctx_disc%cartesian_map%izmax_loc
    dx    =ctx_disc%cartesian_map%dx
    dy    =ctx_disc%cartesian_map%dy
    dz    =ctx_disc%cartesian_map%dz
    ox    =ctx_disc%cartesian_map%ox
    oy    =ctx_disc%cartesian_map%oy
    oz    =ctx_disc%cartesian_map%oz
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! allocate global model, only master will save here
    allocate(model_loc(nx_gb,ny_gb,nz_gb))
    if(ctx_paral%master) then
      allocate(array_out(nx_gb,ny_gb,nz_gb))
    else
      allocate(array_out(1,1,1))
    endif
    !! -----------------------------------------------------------------
    !! create approriate output for the input model, depending on the type
    !! process cartesian for mesh
    !! -----------------------------------------------------------------
    model_loc=0.0
    array_out=0.0
    select case(trim(adjustl(str_representation)))

      !! for model given per cells
      case('cell','node')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            model_loc(ix,iy,iz) = array_in(i_cell,iarray)
          end if
        end do ; end do ; end do
      
      !! for model given per dof
      case('dof')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! ndof for this cell
            order     = ctx_domain%order (i_cell)
            i_order   = ctx_disc%order_to_index (order)
            ndof_vol  = ctx_disc%n_dof_per_order(i_order)
            coeff     = 0.0
            offset_dof= int(ctx_disc%offsetdof_sol_loc(i_cell))
            do idof=1,ndof_vol
              coeff = coeff + real(array_in(offset_dof+idof,iarray)    *   &
              ctx_disc%cartesian_map%weight(ixloc,iyloc,izloc)%array(idof),&
                                   kind=8)
            end do
            model_loc(ix,iy,iz) = coeff
          end if
        end do ; end do ; end do
      
      !! for model given with basis function at constant order.
      case('dof-orderfix')

        !! order must be > 0
        if(order_subcell < 0) then
          ctx_err%msg   = "** ERROR: the polynomial order for the " //  &
                          "fixed-order dof representation is < 0 "  //  &
                          "[extract_structured_grid] ** "
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

        !! create basis function at this order.
        call basis_construct(ctx_domain%dim_domain,order_subcell,       &
                             basisfunc,ctx_err)
        allocate(xpos   (ctx_domain%dim_domain))
        allocate(coo_ref(ctx_domain%dim_domain))
        
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! we need to evaluate the position on the reference element.
            xpos = 0.d0
            select case(ctx_domain%dim_domain)
              !! WE NEED TO OFFSET WITH THE IXMIN POSITION TOO.
              case(1)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                !! convert global position to ref elem position
                !! we have jac1D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(1) = xpos(1)                                    &
                  - ctx_domain%x_node(1,ctx_domain%cell_node(1,i_cell))
                coo_ref(1) = ctx_disc%inv_jac(1,1,i_cell)*dble(coo_ref(1))

              case(2)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(1:2,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))

              case(3)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oy + (iyloc-1)*dy + (iymin-1)*dy
                xpos(3) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac3D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(:,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))
              case default
                ctx_err%msg   = "** ERROR: Incorrect dimension "    //  &
                                "[extract_structured_grid] ** "
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

            !! evaluate basis function here
            mval = 0.d0
            ind_gb = (i_cell-1)*basisfunc%nphi
            do i_dof=1,basisfunc%nphi
              vloc = 0.d0
              call polynomial_eval(basisfunc%pol(i_dof),coo_ref,vloc,ctx_err)
              mval = mval + dble(vloc * array_in(ind_gb+i_dof,iarray))
            end do
            model_loc(ix,iy,iz) = dble(mval)
          
          end if
        end do ; end do ; end do
        deallocate(xpos)
        deallocate(coo_ref)
        call basis_clean(basisfunc)

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh array "   // & 
                        "representation in [extract_structured_grid] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)             
    end select
    ! ----------------------------------------------------------------
      
    !! reduce so that only master keeps the result
    call reduce_sum(model_loc,array_out,nx_gb*ny_gb*nz_gb,0,           &
                    ctx_paral%communicator,ctx_err)
    ! ----------------------------------------------------------------
    !! only master has the solution
    ! ----------------------------------------------------------------
    deallocate(model_loc)
    return
  end subroutine extract_structured_grid_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Extract the structured cartesian solution from input array given
  !> by cell or by dof. 
  !> We also gives the basic infos of the grid (nx,ny,nz,dx,dy,dz).
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_disc          : type discretization
  !> @param[in]    array_in          : array to be saved of size (:,narray)
  !> @param[in]    iarray            : current array to process
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[inout] array_out         : output cartesian grid
  !> @param[out]   n{XYZ}_gb         : size of the grid
  !> @param[out]   o{XYZ}            : origin of the grid
  !> @param[out]   d{XYZ}            : step of the grid
  !> @param[in]    order_subcell     : in case of multiple points per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine extract_structured_grid_complex4(ctx_paral,ctx_domain,     &
                                           ctx_disc,array_in,iarray,    &
                                           str_representation,array_out,&
                                           nx_gb,ny_gb,nz_gb,ox,oy,oz,  &
                                           dx,dy,dz,order_subcell,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=4),allocatable ,intent(in)   :: array_in (:,:)
    complex(kind=4),allocatable ,intent(inout):: array_out(:,:,:)
    integer                     ,intent(in)   :: iarray
    integer                     ,intent(in)   :: order_subcell
    integer                     ,intent(out)  :: nx_gb,ny_gb,nz_gb
    real(kind=8)                ,intent(out)  :: ox,oy,oz,dx,dy,dz
    character(len=*)            ,intent(in)   :: str_representation
    type(t_error)               ,intent(inout):: ctx_err

    !! local
    !! -----------------------------------------------------------------
    integer                  :: ix,iy,iz,ixloc,iyloc,izloc
    integer(kind=IKIND_MESH) :: i_cell
    integer                  :: nx_loc,ny_loc,nz_loc
    integer                  :: ixmin,ixmax,iymin,iymax,izmin,izmax
    integer                  :: ixmin_off,iymin_off,izmin_off
    integer                  :: ixmax_off,iymax_off,izmax_off
    integer                  :: i_order,idof,ndof_vol,order,offset_dof
    complex(kind=4),allocatable :: model_loc(:,:,:)
    complex(kind=8)          :: coeff
    !! for model per dof
    type(t_basis)            :: basisfunc
    real(kind=8)        ,allocatable:: xpos(:)
    real(kind=RKIND_POL),allocatable:: coo_ref(:)
    real(kind=RKIND_POL)     :: vloc
    complex(kind=8)          :: mval
    integer                  :: ind_gb, i_dof
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! -----------------------------------------------------------------
    !! We assemble the global array to be saved:
    !! --> mesh      with info per cell
    !! --> mesh      with info per node
    !! -----------------------------------------------------------------
    !! init values depending on the type of grid
    nx_gb=0; ny_gb=0; nz_gb=0; nx_loc=0; ny_loc=0; nz_loc=0; ixmin=0 
    iymin=0; izmin=0; ixmax=0; iymax=0; izmax=0; ixmin_off=0; iymin_off=0
    izmin_off=0; ixmax_off=0; iymax_off=0; izmax_off=0; dx=0; dy=0; dz=0
      
    !! Assemble from mesh infos using ctx_domain%cartesian_map >>
    nx_gb =ctx_disc%cartesian_map%nx_gb
    ny_gb =ctx_disc%cartesian_map%ny_gb
    nz_gb =ctx_disc%cartesian_map%nz_gb
    nx_loc=ctx_disc%cartesian_map%nx_loc
    ny_loc=ctx_disc%cartesian_map%ny_loc
    nz_loc=ctx_disc%cartesian_map%nz_loc
    ixmin =ctx_disc%cartesian_map%ixmin_loc
    iymin =ctx_disc%cartesian_map%iymin_loc
    izmin =ctx_disc%cartesian_map%izmin_loc
    ixmax =ctx_disc%cartesian_map%ixmax_loc
    iymax =ctx_disc%cartesian_map%iymax_loc
    izmax =ctx_disc%cartesian_map%izmax_loc
    dx    =ctx_disc%cartesian_map%dx
    dy    =ctx_disc%cartesian_map%dy
    dz    =ctx_disc%cartesian_map%dz
    ox    =ctx_disc%cartesian_map%ox
    oy    =ctx_disc%cartesian_map%oy
    oz    =ctx_disc%cartesian_map%oz
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! allocate global model, only master will save here
    allocate(model_loc(nx_gb,ny_gb,nz_gb))
    if(ctx_paral%master) then
      allocate(array_out(nx_gb,ny_gb,nz_gb))
    else
      allocate(array_out(1,1,1))
    endif
    !! -----------------------------------------------------------------
    !! create approriate output for the input model, depending on the type
    !! process cartesian for mesh
    !! -----------------------------------------------------------------
    model_loc=0.0
    array_out=0.0
    select case(trim(adjustl(str_representation)))

      !! for model given per cells
      case('cell','node')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            model_loc(ix,iy,iz) = array_in(i_cell,iarray)
          end if
        end do ; end do ; end do
      
      !! for model given per dof
      case('dof')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! ndof for this cell
            order     = ctx_domain%order (i_cell)
            i_order   = ctx_disc%order_to_index (order)
            ndof_vol  = ctx_disc%n_dof_per_order(i_order)
            coeff     = 0.0
            offset_dof= int(ctx_disc%offsetdof_sol_loc(i_cell))
            do idof=1,ndof_vol
              coeff = coeff + dcmplx(array_in(offset_dof+idof,iarray) * &
              ctx_disc%cartesian_map%weight(ixloc,iyloc,izloc)%array(idof))
            end do
            model_loc(ix,iy,iz) = cmplx(coeff,kind=4)
          end if
        end do ; end do ; end do

      
      !! for model given per several constant per dof
      !! for model given with basis function at constant order.
      case('dof-orderfix')

        !! order must be > 0
        if(order_subcell < 0) then
          ctx_err%msg   = "** ERROR: the polynomial order for the " //  &
                          "fixed-order dof representation is < 0 "  //  &
                          "[extract_structured_grid] ** "
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
        
        !! create basis function at this order.
        call basis_construct(ctx_domain%dim_domain,order_subcell,       &
                             basisfunc,ctx_err)
        allocate(xpos   (ctx_domain%dim_domain))
        allocate(coo_ref(ctx_domain%dim_domain))
        
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! we need to evaluate the position on the reference element.
            xpos = 0.d0
            select case(ctx_domain%dim_domain)
              !! WE NEED TO OFFSET WITH THE IXMIN POSITION TOO.
              case(1)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                !! convert global position to ref elem position
                !! we have jac1D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(1) = xpos(1)                                    &
                  - ctx_domain%x_node(1,ctx_domain%cell_node(1,i_cell))
                coo_ref(1) = ctx_disc%inv_jac(1,1,i_cell)*dble(coo_ref(1))

              case(2)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(1:2,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))

              case(3)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oy + (iyloc-1)*dy + (iymin-1)*dy
                xpos(3) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac3D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(:,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))
              case default
                ctx_err%msg   = "** ERROR: Incorrect dimension "    //  &
                                "[extract_structured_grid] ** "
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
     
            !! evaluate basis function here
            mval = 0.d0
            ind_gb = (i_cell-1)*basisfunc%nphi
            do i_dof=1,basisfunc%nphi
              vloc = 0.d0
              call polynomial_eval(basisfunc%pol(i_dof),coo_ref,vloc,ctx_err)
              mval = mval + dcmplx(vloc * array_in(ind_gb+i_dof,iarray))
            end do
            model_loc(ix,iy,iz) = cmplx(mval,kind=4)
            
          end if
        end do ; end do ; end do
        deallocate(xpos)
        deallocate(coo_ref)
        call basis_clean(basisfunc)

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh array "   // & 
                        "representation in [extract_structured_grid] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)             
    end select
    ! ----------------------------------------------------------------
      
    !! reduce so that only master keeps the result
    call reduce_sum(model_loc,array_out,nx_gb*ny_gb*nz_gb,0,           &
                    ctx_paral%communicator,ctx_err)
    ! ----------------------------------------------------------------
    !! only master has the solution
    ! ----------------------------------------------------------------
    deallocate(model_loc)
    return
  end subroutine extract_structured_grid_complex4
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Extract the structured cartesian solution from input array given
  !> by cell or by dof. 
  !> We also gives the basic infos of the grid (nx,ny,nz,dx,dy,dz).
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_disc          : type discretization
  !> @param[in]    array_in          : array to be saved of size (:,narray)
  !> @param[in]    iarray            : current array to process
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[inout] array_out         : output cartesian grid
  !> @param[out]   n{XYZ}_gb         : size of the grid
  !> @param[out]   o{XYZ}            : origin of the grid
  !> @param[out]   d{XYZ}            : step of the grid
  !> @param[in]    order_subcell     : in case of multiple points per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine extract_structured_grid_complex8(ctx_paral,ctx_domain,     &
                                           ctx_disc,array_in,iarray,    &
                                           str_representation,array_out,&
                                           nx_gb,ny_gb,nz_gb,ox,oy,oz,  &
                                           dx,dy,dz,order_subcell,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=8),allocatable ,intent(in)   :: array_in (:,:)
    complex(kind=8),allocatable ,intent(inout):: array_out(:,:,:)
    integer                     ,intent(in)   :: iarray
    integer                     ,intent(in)   :: order_subcell
    integer                     ,intent(out)  :: nx_gb,ny_gb,nz_gb
    real(kind=8)                ,intent(out)  :: ox,oy,oz,dx,dy,dz
    character(len=*)            ,intent(in)   :: str_representation
    type(t_error)               ,intent(inout):: ctx_err

    !! local
    !! -----------------------------------------------------------------
    integer                  :: ix,iy,iz,ixloc,iyloc,izloc
    integer(kind=IKIND_MESH) :: i_cell
    integer                  :: nx_loc,ny_loc,nz_loc
    integer                  :: ixmin,ixmax,iymin,iymax,izmin,izmax
    integer                  :: ixmin_off,iymin_off,izmin_off
    integer                  :: ixmax_off,iymax_off,izmax_off
    integer                  :: i_order,idof,ndof_vol,order,offset_dof
    complex(kind=8),allocatable :: model_loc(:,:,:)
    complex(kind=8)          :: coeff
    !! for model per dof
    type(t_basis)            :: basisfunc
    real(kind=8)         ,allocatable:: xpos(:)
    real(kind=RKIND_POL) ,allocatable:: coo_ref(:)
    real(kind=RKIND_POL)     :: vloc
    complex(kind=8)          :: mval
    integer                  :: ind_gb, i_dof
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! -----------------------------------------------------------------
    !! We assemble the global array to be saved:
    !! --> mesh      with info per cell
    !! --> mesh      with info per node
    !! -----------------------------------------------------------------
    !! init values depending on the type of grid
    nx_gb=0; ny_gb=0; nz_gb=0; nx_loc=0; ny_loc=0; nz_loc=0; ixmin=0 
    iymin=0; izmin=0; ixmax=0; iymax=0; izmax=0; ixmin_off=0; iymin_off=0
    izmin_off=0; ixmax_off=0; iymax_off=0; izmax_off=0; dx=0; dy=0; dz=0
      
    !! Assemble from mesh infos using ctx_domain%cartesian_map >>
    nx_gb =ctx_disc%cartesian_map%nx_gb
    ny_gb =ctx_disc%cartesian_map%ny_gb
    nz_gb =ctx_disc%cartesian_map%nz_gb
    nx_loc=ctx_disc%cartesian_map%nx_loc
    ny_loc=ctx_disc%cartesian_map%ny_loc
    nz_loc=ctx_disc%cartesian_map%nz_loc
    ixmin =ctx_disc%cartesian_map%ixmin_loc
    iymin =ctx_disc%cartesian_map%iymin_loc
    izmin =ctx_disc%cartesian_map%izmin_loc
    ixmax =ctx_disc%cartesian_map%ixmax_loc
    iymax =ctx_disc%cartesian_map%iymax_loc
    izmax =ctx_disc%cartesian_map%izmax_loc
    dx    =ctx_disc%cartesian_map%dx
    dy    =ctx_disc%cartesian_map%dy
    dz    =ctx_disc%cartesian_map%dz
    ox    =ctx_disc%cartesian_map%ox
    oy    =ctx_disc%cartesian_map%oy
    oz    =ctx_disc%cartesian_map%oz
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! allocate global model, only master will save here
    allocate(model_loc(nx_gb,ny_gb,nz_gb))
    if(ctx_paral%master) then
      allocate(array_out(nx_gb,ny_gb,nz_gb))
    else
      allocate(array_out(1,1,1))
    endif
    !! -----------------------------------------------------------------
    !! create approriate output for the input model, depending on the type
    !! process cartesian for mesh
    !! -----------------------------------------------------------------
    model_loc=0.0
    array_out=0.0
    select case(trim(adjustl(str_representation)))

      !! for model given per cells
      case('cell','node')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            model_loc(ix,iy,iz) = array_in(i_cell,iarray)
          end if
        end do ; end do ; end do
      
      !! for model given per dof
      case('dof')
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! ndof for this cell
            order     = ctx_domain%order (i_cell)
            i_order   = ctx_disc%order_to_index (order)
            ndof_vol  = ctx_disc%n_dof_per_order(i_order)
            coeff     = 0.0
            offset_dof= int(ctx_disc%offsetdof_sol_loc(i_cell))
            do idof=1,ndof_vol
              coeff = coeff + dcmplx(array_in(offset_dof+idof,iarray)*  &
              ctx_disc%cartesian_map%weight(ixloc,iyloc,izloc)%array(idof))
            end do
            model_loc(ix,iy,iz) = coeff
          end if
        end do ; end do ; end do

      !! for model given with basis function at constant order.
      case('dof-orderfix')
        
        !! order must be > 0
        if(order_subcell < 0) then
          ctx_err%msg   = "** ERROR: the polynomial order for the " //  &
                          "fixed-order dof representation is < 0 "  //  &
                          "[extract_structured_grid] ** "
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
        
        !! create basis function at this order.
        call basis_construct(ctx_domain%dim_domain,order_subcell,       &
                             basisfunc,ctx_err)
        allocate(xpos   (ctx_domain%dim_domain))
        allocate(coo_ref(ctx_domain%dim_domain))
        
        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          ixloc=ix-ixmin+1 ; iyloc=iy-iymin+1 ; izloc=iz-izmin+1
          i_cell = ctx_disc%cartesian_map%i_cell(ixloc,iyloc,izloc)
          if(i_cell > 0) then
            !! we need to evaluate the position on the reference element.
            xpos = 0.d0
            select case(ctx_domain%dim_domain)
              !! WE NEED TO OFFSET WITH THE IXMIN POSITION TOO.
              case(1)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                !! convert global position to ref elem position
                !! we have jac1D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(1) = xpos(1)                                    &
                  - ctx_domain%x_node(1,ctx_domain%cell_node(1,i_cell))
                coo_ref(1) = ctx_disc%inv_jac(1,1,i_cell)*dble(coo_ref(1))

              case(2)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(1:2,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))

              case(3)
                xpos(1) = ox + (ixloc-1)*dx + (ixmin-1)*dx
                xpos(2) = oy + (iyloc-1)*dy + (iymin-1)*dy
                xpos(3) = oz + (izloc-1)*dz + (izmin-1)*dz
                !! convert global position to ref elem position
                !! we have jac3D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al  
                coo_ref(:) = xpos(:)                                    &
                - ctx_domain%x_node(:,ctx_domain%cell_node(1,i_cell))
                coo_ref(:) = matmul(ctx_disc%inv_jac(:,:,i_cell),coo_ref(:))
              case default
                ctx_err%msg   = "** ERROR: Incorrect dimension "    //  &
                                "[extract_structured_grid] ** "
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
         
            !! evaluate basis function here
            mval = 0.d0
            ind_gb = (i_cell-1)*basisfunc%nphi
            do i_dof=1,basisfunc%nphi
              vloc = 0.d0
              call polynomial_eval(basisfunc%pol(i_dof),coo_ref,vloc,ctx_err)
              mval = mval + dcmplx(vloc * array_in(ind_gb+i_dof,iarray))
            end do
            model_loc(ix,iy,iz) = dcmplx(mval)
          end if
        end do ; end do ; end do
        deallocate(xpos)
        deallocate(coo_ref)
        call basis_clean(basisfunc)

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh array "   // & 
                        "representation in [extract_structured_grid] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)             
    end select
    ! ----------------------------------------------------------------
      
    !! reduce so that only master keeps the result
    call reduce_sum(model_loc,array_out,nx_gb*ny_gb*nz_gb,0,           &
                    ctx_paral%communicator,ctx_err)
    ! ----------------------------------------------------------------
    !! only master has the solution
    ! ----------------------------------------------------------------
    deallocate(model_loc)
    return
  end subroutine extract_structured_grid_complex8
  
end module m_extract_structured_grid
