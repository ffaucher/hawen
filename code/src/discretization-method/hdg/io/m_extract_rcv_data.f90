!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_extract_rcv_data.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to extract the receivers data point using
!> the wavefield infos. It uses the weights that have been 
!> computed in the discretization context.
!
!------------------------------------------------------------------------------
module m_extract_rcv_data

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH
  use m_ctx_discretization,     only: t_discretization
  !! -------------------------------------------------------------------
  implicit none

  interface extract_rcv_data
     module procedure extract_rcv_data_complex4
     module procedure extract_rcv_data_complex8
  end interface
  
  private
  public  :: extract_rcv_data

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> extract the data at the receivers location
  !
  !> @param[in]    ctx_dg            : type DG
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    nrcv              : number of receivers
  !> @param[in]    isrc              : current source
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    basename          : path to the file to be saved
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine extract_rcv_data_complex4(ctx_dg,array,narray,nrcv,isource,&
                                       rcv_data,ctx_err)
    implicit none
    type(t_discretization)      ,intent(in)   :: ctx_dg
    complex                     ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    integer                     ,intent(in)   :: nrcv,isource
    complex(kind=4)             ,intent(inout):: rcv_data(:,:)
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer                  :: imodel,idof,offset_dof,k
    integer(kind=IKIND_MESH) :: i_cell
    complex(kind=8)          :: coeff
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! -----------------------------------------------------------------
    !! Save the data at the receivers location
    !! -----------------------------------------------------------------
    rcv_data = 0.0
    !! use the dof infos contained in ctx_dg%src(isource)%rcv(:)
    do k=1,nrcv
      i_cell = ctx_dg%src(isource)%rcv(k)%icell_loc
      if(i_cell > 0) then
        !! loop over all components
        do imodel=1,narray
          coeff     = 0.0
          offset_dof= int(ctx_dg%offsetdof_sol_loc(i_cell))
          do idof=1,ctx_dg%src(isource)%rcv(k)%ndof_vol
            coeff = coeff +                                         &
                    dcmplx(ctx_dg%src(isource)%rcv(k)%weight(idof) *&
                           array(offset_dof+idof,imodel))
          end do
          rcv_data(k,imodel) = cmplx(coeff)
        end do
      end if
    end do
    !! -----------------------------------------------------------------
    return
  end subroutine extract_rcv_data_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> extract the data at the receivers location
  !
  !> @param[in]    ctx_dg            : type DG
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    nrcv              : number of receivers
  !> @param[in]    isrc              : current source
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    basename          : path to the file to be saved
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine extract_rcv_data_complex8(ctx_dg,array,narray,nrcv,isource,&
                                       rcv_data,ctx_err)
    implicit none
    type(t_discretization)      ,intent(in)   :: ctx_dg
    complex(kind=8)             ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    integer                     ,intent(in)   :: nrcv,isource
    complex(kind=8)             ,intent(inout):: rcv_data(:,:)
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer                  :: imodel,idof,offset_dof,k
    integer(kind=IKIND_MESH) :: i_cell
    complex(kind=8)          :: coeff
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! -----------------------------------------------------------------
    !! Save the data at the receivers location
    !! -----------------------------------------------------------------
    rcv_data = 0.0
    !! use the dof infos contained in ctx_dg%src(isource)%rcv(:)
    do k=1,nrcv
      i_cell = ctx_dg%src(isource)%rcv(k)%icell_loc
      if(i_cell > 0) then
        !! loop over all components
        do imodel=1,narray
          coeff     = 0.0
          offset_dof= int(ctx_dg%offsetdof_sol_loc(i_cell))
          do idof=1,ctx_dg%src(isource)%rcv(k)%ndof_vol
            coeff = coeff +                                         &
                    dcmplx(ctx_dg%src(isource)%rcv(k)%weight(idof) *&
                           array(offset_dof+idof,imodel))
          end do
          rcv_data(k,imodel) = dcmplx(coeff)
        end do
      end if
    end do
    !! -----------------------------------------------------------------
    return
  end subroutine extract_rcv_data_complex8
  
end module m_extract_rcv_data
