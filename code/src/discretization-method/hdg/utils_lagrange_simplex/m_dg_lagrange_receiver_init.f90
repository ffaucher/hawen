!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_source_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize acquisition infos for DG discretization
!> for the receivers by finding the weights of each dof. 
!> Receivers are considered as point devices.
!
!------------------------------------------------------------------------------
module m_dg_lagrange_receiver_init

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,              only: raise_error, t_error
  use m_define_precision,         only: IKIND_MESH,RKIND_POL
  use m_ctx_parallelism,          only: t_parallelism
  !> matrix and mesh domain types
  use m_ctx_acquisition,          only: t_acquisition,SRC_DIRAC
  use m_ctx_domain,               only: t_domain
  use m_ctx_discretization,       only: t_discretization
  use m_dg_lagrange_source_dirac, only: dg_init_point_source_1d,        &
                                        dg_init_point_source_2d,        &
                                        dg_init_point_source_3d
  use m_tag_scale_rhs,            only: tag_RHS_SCALE_ID
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: dg_receiver_init

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of receivers
  !> - the cell(s) which are involved
  !> - the weight for each dof
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : mesh grid type
  !> @param[inout] ctx_dg         : type discretization DG
  !> @param[in]    i_src_first    : first source index in acquisition
  !> @param[in]    i_src_last     : last source index in acquisition
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_receiver_init(ctx_paral,ctx_aq,ctx_grid,ctx_dg,         &
                              i_src_first,i_src_last,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first
    integer                          ,intent(in)   :: i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! We initialize the rcv infos, each one is a point
    !! -----------------------------------------------------------------
    !! depends on the dimension
    select case(ctx_grid%dim_domain)
      case(1) !! 1 dimension
        call dg_rcv_init_1d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,           &
                            i_src_first,i_src_last,ctx_err)
      case(2) !! 2 dimension
        call dg_rcv_init_2d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,           &
                            i_src_first,i_src_last,ctx_err)
      case(3) !! 3 dimension
        call dg_rcv_init_3d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,           &
                            i_src_first,i_src_last,ctx_err)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension " //   &
                        "[dg_receiver_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! -----------------------------------------------------------------

    return
  end subroutine dg_receiver_init


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of receivers in 1 dimension
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_rcv_init_1d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,i_src_first, &
                            i_src_last,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer                             :: i_src,ndof_vol,ndof_face_gb
    integer                             :: ndof_face(2) !! 2 faces segment
    integer                             :: j,nrcv,i_rcv
    integer(kind=IKIND_MESH)            :: local_cell
    real   (kind=RKIND_POL),allocatable :: weight(:)
    real   (kind=RKIND_POL),allocatable :: weight_dbasis(:,:)
    real(kind=8)                        :: coo1d(1)
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! loop over all sources >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do i_src=i_src_first, i_src_last
      j = i_src-i_src_first+1
      !! number of receivers given by acquisition
      nrcv = ctx_aq%sources(i_src)%rcv%nrcv
      ctx_dg%src(j)%nrcv = nrcv

      !! note that receivers are point source
      if(nrcv > 0) then
        allocate(ctx_dg%src(j)%rcv(nrcv))
        !! loop over all receivers >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !! if all receivers are in the same place, we do not repeat
        !! the search for the cells to save time
        if((j == 1) .or. (.not. ctx_aq%flag_same_rcv)) then
          do i_rcv=1,nrcv
            ctx_dg%src(j)%rcv(i_rcv)%icell_loc = 0
            ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = 0
            ctx_dg%src(j)%rcv(i_rcv)%ndof_face = 0

            !! locate the corresponding cell if it is on this subgrid,
            !! no scaling whatsoever
            local_cell = 0
            coo1d(1) = dble(ctx_aq%sources(i_src)%rcv%x(i_rcv))
            call dg_init_point_source_1d(ctx_paral,ctx_grid,ctx_dg,coo1d, &
                                         SRC_DIRAC,tag_RHS_SCALE_ID,      &
                                         local_cell,ndof_vol,ndof_face,   &
                                         weight,weight_dbasis,ctx_err)
            if(local_cell .ne. 0) then
              !! only this source or all of them
              ctx_dg%src(j)%rcv(i_rcv)%icell_loc = local_cell
              allocate(ctx_dg%src(j)%rcv(i_rcv)%weight(ndof_vol))
              ctx_dg%src(j)%rcv(i_rcv)%weight(:) = weight(:)
              ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
              ctx_dg%src(j)%rcv(i_rcv)%ndof_face = sum(ndof_face)
              deallocate(weight)
            end if
          end do
        !! all sources have the same receivers, we copy the first source
        !! information ! ------------------------------------------
        else
          do i_rcv=1,nrcv
            local_cell= ctx_dg%src(1)%rcv(i_rcv)%icell_loc
            ndof_vol  = ctx_dg%src(1)%rcv(i_rcv)%ndof_vol
            ndof_face_gb = ctx_dg%src(1)%rcv(i_rcv)%ndof_face
            ctx_dg%src(j)%rcv(i_rcv)%icell_loc = local_cell
            ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
            ctx_dg%src(j)%rcv(i_rcv)%ndof_face = ndof_face_gb
            if(local_cell .ne. 0) then
              allocate(ctx_dg%src(j)%rcv(i_rcv)%weight(ndof_vol))
              ctx_dg%src(j)%rcv(i_rcv)%weight(:) =                      &
                                      ctx_dg%src(1)%rcv(i_rcv)%weight(:)
              ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
              ctx_dg%src(j)%rcv(i_rcv)%ndof_face = ndof_face_gb
            end if
          end do
        end if !! else if first source or not same receivers
      end if
    end do
    !! -----------------------------------------------------------------

    return
  end subroutine dg_rcv_init_1d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of receivers in 2 dimension
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_rcv_init_2d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,i_src_first, &
                            i_src_last,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer                             :: i_src,ndof_vol,ndof_face_gb
    integer                             :: ndof_face(3) !! 3 faces triangle
    integer                             :: j,nrcv,i_rcv
    integer(kind=IKIND_MESH)            :: local_cell
    real   (kind=RKIND_POL),allocatable :: weight(:)
    real   (kind=RKIND_POL),allocatable :: weight_dbasis(:,:)
    real(kind=8)                        :: coo2d(2)
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! loop over all sources >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do i_src=i_src_first, i_src_last
      j = i_src-i_src_first+1
      !! number of receivers given by acquisition
      nrcv = ctx_aq%sources(i_src)%rcv%nrcv
      ctx_dg%src(j)%nrcv = nrcv

      !! note that receivers are point source
      if(nrcv > 0) then
        allocate(ctx_dg%src(j)%rcv(nrcv))
        !! loop over all receivers >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !! if all receivers are in the same place, we do not repeat
        !! the search for the cells to save time
        if((j == 1) .or. (.not. ctx_aq%flag_same_rcv)) then
          do i_rcv=1,nrcv
            ctx_dg%src(j)%rcv(i_rcv)%icell_loc = 0
            ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = 0
            ctx_dg%src(j)%rcv(i_rcv)%ndof_face = 0

            !! locate the corresponding cell if it is on this subgrid,
            !! no scaling whatsoever
            local_cell = 0
            coo2d(1) = dble(ctx_aq%sources(i_src)%rcv%x(i_rcv))
            coo2d(2) = dble(ctx_aq%sources(i_src)%rcv%z(i_rcv))
            call dg_init_point_source_2d(ctx_paral,ctx_grid,ctx_dg,coo2d, &
                                         SRC_DIRAC,tag_RHS_SCALE_ID,      &
                                         local_cell,ndof_vol,ndof_face,   &
                                         weight,weight_dbasis,ctx_err)
            if(local_cell .ne. 0) then
              !! only this source or all of them
              ctx_dg%src(j)%rcv(i_rcv)%icell_loc = local_cell
              allocate(ctx_dg%src(j)%rcv(i_rcv)%weight(ndof_vol))
              ctx_dg%src(j)%rcv(i_rcv)%weight(:) = weight(:)
              ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
              ctx_dg%src(j)%rcv(i_rcv)%ndof_face = sum(ndof_face)
              deallocate(weight)
            end if
          end do
        !! all sources have the same receivers, we copy the first source
        !! information ! ------------------------------------------
        else
          do i_rcv=1,nrcv
            local_cell= ctx_dg%src(1)%rcv(i_rcv)%icell_loc
            ndof_vol  = ctx_dg%src(1)%rcv(i_rcv)%ndof_vol
            ndof_face_gb = ctx_dg%src(1)%rcv(i_rcv)%ndof_face
            ctx_dg%src(j)%rcv(i_rcv)%icell_loc = local_cell
            ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
            ctx_dg%src(j)%rcv(i_rcv)%ndof_face = ndof_face_gb
            if(local_cell .ne. 0) then
              allocate(ctx_dg%src(j)%rcv(i_rcv)%weight(ndof_vol))
              ctx_dg%src(j)%rcv(i_rcv)%weight(:) =                      &
                                      ctx_dg%src(1)%rcv(i_rcv)%weight(:)
              ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
              ctx_dg%src(j)%rcv(i_rcv)%ndof_face = ndof_face_gb
            end if
          end do
        end if !! else if first source or not same receivers
      end if
    end do
    !! -----------------------------------------------------------------

    return
  end subroutine dg_rcv_init_2d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of receivers in 3 dimensions
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_rcv_init_3d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,           &
                            i_src_first,i_src_last,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer                             :: i_src,ndof_vol,ndof_face_gb
    integer                             :: ndof_face(4) !! 4 faces tetra
    integer                             :: j,nrcv,i_rcv
    integer(kind=IKIND_MESH)            :: local_cell
    real   (kind=RKIND_POL),allocatable :: weight(:)
    real   (kind=RKIND_POL),allocatable :: weight_dbasis(:,:)
    real(kind=8)                        :: coo3d(3)
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! loop over all sources >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do i_src=i_src_first, i_src_last
      j = i_src-i_src_first+1
      !! number of receivers given by acquisition
      nrcv = ctx_aq%sources(i_src)%rcv%nrcv
      ctx_dg%src(j)%nrcv = nrcv

      !! note that receivers are point source
      if(nrcv > 0) then
        allocate(ctx_dg%src(j)%rcv(nrcv))
        !! loop over all receivers >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !! if all receivers are in the same place, we do not repeat
        !! the search for the cells to save time
        if((j == 1) .or. (.not. ctx_aq%flag_same_rcv)) then
          do i_rcv=1,nrcv
            ctx_dg%src(j)%rcv(i_rcv)%icell_loc = 0
            ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = 0
            ctx_dg%src(j)%rcv(i_rcv)%ndof_face = 0

            !! locate the corresponding cell if it is on this subgrid,
            !! no scaling whatsoever
            local_cell = 0
            coo3d(1) = dble(ctx_aq%sources(i_src)%rcv%x(i_rcv))
            coo3d(2) = dble(ctx_aq%sources(i_src)%rcv%y(i_rcv))
            coo3d(3) = dble(ctx_aq%sources(i_src)%rcv%z(i_rcv))
            call dg_init_point_source_3d(ctx_paral,ctx_grid,ctx_dg,coo3d, &
                                         SRC_DIRAC,tag_RHS_SCALE_ID,      &
                                         local_cell,ndof_vol,ndof_face,   &
                                         weight,weight_dbasis,ctx_err)
            if(local_cell .ne. 0) then
              ctx_dg%src(j)%rcv(i_rcv)%icell_loc = local_cell
              allocate(ctx_dg%src(j)%rcv(i_rcv)%weight(ndof_vol))
              ctx_dg%src(j)%rcv(i_rcv)%weight(:) = weight(:)
              ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
              ctx_dg%src(j)%rcv(i_rcv)%ndof_face = sum(ndof_face)
              deallocate(weight)
            end if
          end do
        !! all sources have the same receivers, we copy the first source
        !! information ! ------------------------------------------
        else
          do i_rcv=1,nrcv
            local_cell= ctx_dg%src(1)%rcv(i_rcv)%icell_loc
            ndof_vol  = ctx_dg%src(1)%rcv(i_rcv)%ndof_vol
            ndof_face_gb = ctx_dg%src(1)%rcv(i_rcv)%ndof_face
            ctx_dg%src(j)%rcv(i_rcv)%icell_loc = local_cell
            ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
            ctx_dg%src(j)%rcv(i_rcv)%ndof_face = ndof_face_gb
            if(local_cell .ne. 0) then
              allocate(ctx_dg%src(j)%rcv(i_rcv)%weight(ndof_vol))
              ctx_dg%src(j)%rcv(i_rcv)%weight(:) =                      &
                                      ctx_dg%src(1)%rcv(i_rcv)%weight(:)
              ctx_dg%src(j)%rcv(i_rcv)%ndof_vol  = ndof_vol
              ctx_dg%src(j)%rcv(i_rcv)%ndof_face = ndof_face_gb
            end if
          end do
        end if !! else if first source or not same receivers
      end if
    end do
    !! -----------------------------------------------------------------

    return
  end subroutine dg_rcv_init_3d

end module m_dg_lagrange_receiver_init
