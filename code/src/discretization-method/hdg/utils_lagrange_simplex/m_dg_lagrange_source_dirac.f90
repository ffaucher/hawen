!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_source_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize a delta-Dirac source using HDG
!> discretization in the context of Lagrange Basis functions. 
!> The source is located on one cell, and the rhs for the cell
!> K and dof j is 
!>   
!>    \integral_K \delta(x_0) \phi_j(x) dK
!>  = \phi_j(x_0).
!
!------------------------------------------------------------------------------
module m_dg_lagrange_source_dirac

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: IKIND_MESH,RKIND_POL
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_max,          only: allreduce_max
  use m_allreduce_sum,          only: allreduce_sum
  use m_find_cell,              only: find_cell
  use m_array_types,            only: array_allocate
  use m_basis_functions,        only: t_basis, basis_construct,         &
                                      basis_derivative_construct, basis_clean
  use m_polynomial,             only: polynomial_eval
  !! matrix and mesh domain types
  use m_ctx_acquisition,        only: t_acquisition,SRC_DIRAC,          &
                                      SRC_DIRAC_DERIV,SRC_DIRAC_DIV,    &
                                      SRC_DIRAC_DERIVX,SRC_DIRAC_DERIVY,&
                                      SRC_DIRAC_DERIVZ,SRC_ANALYTIC
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_mesh_simplex_interface, only: interface_2D_triangle,            &
                                      interface_3D_tetrahedron
  use m_lapack_solve,           only: lapack_solve_system
  !! to adjust the rhs
  use m_tag_scale_rhs,          only: tag_RHS_SCALE_ID,tag_RHS_SCALE_R, &
                                      tag_RHS_SCALE_R2,tag_RHS_SCALE_X, &
                                      tag_RHS_SCALE_X2,                 &
                                      tag_RHS_SCALE_INV2PI,tag_RHS_SCALE_XINV2PI
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: dg_source_init_dirac, dg_init_point_source_2d
  public :: dg_init_point_source_3d, dg_init_point_source_1d
  private:: dg_source_init_2d 
  private:: dg_source_init_3d 
  private:: dg_source_init_1d 
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for a delta-Dirac sources:
  !> - the cell(s) which are involved
  !> - the weight for the integral
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : mesh grid type
  !> @param[inout] ctx_dg         : type discretization DG
  !> @param[in]    i_src_first    : first source index in acquisition
  !> @param[in]    i_src_last     : last source index in acquisition
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_dirac(ctx_paral,ctx_aq,ctx_grid,ctx_dg,     &
                                  i_src_first,i_src_last,tag_scale_rhs, &
                                  ctx_err)
    implicit none

    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first
    integer                          ,intent(in)   :: i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! allocate the context and fill depending on the type of sources
    ctx_dg%src(:)%ncell_loc=0

    !! then it depends on the dimension.
    select case(ctx_grid%dim_domain)
      case(1) !! 1 dimension
        call dg_source_init_1d(ctx_paral,ctx_aq,ctx_grid,ctx_dg, &
                               i_src_first,i_src_last,tag_scale_rhs,ctx_err)

      case(2) !! 2 dimensions
        call dg_source_init_2d(ctx_paral,ctx_aq,ctx_grid,ctx_dg, &
                               i_src_first,i_src_last,tag_scale_rhs,ctx_err)

      case(3) !! 3 dimensions
        call dg_source_init_3d(ctx_paral,ctx_aq,ctx_grid,ctx_dg, &
                               i_src_first,i_src_last,tag_scale_rhs,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "//&
                        "[dg_source_init_dirac] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)
    
    return
  end subroutine dg_source_init_dirac


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of sources in 1 dimension
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_1d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,        &
                               i_src_first,i_src_last,tag_scale_rhs,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer                             :: k,i_src,ndof_vol,ndof_face_gb
    integer                             :: ndof_face(2) !! 2 faces segment
    integer                             :: l,j,i_sol
    integer(kind=IKIND_MESH)            :: local_cell,counter_cell
    real   (kind=RKIND_POL),allocatable :: weight(:)
    real   (kind=RKIND_POL),allocatable :: weight_dbasis(:,:)
    real(kind=8)                        :: coo1d(1)
    !! for face sources
    real(kind=8)                        :: pos_bdry,d_bdry
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    do i_src=i_src_first, i_src_last
      if(ctx_err%ierr .ne. 0) cycle
 
      j = i_src-i_src_first+1

      ! -------------------------------------------------------
      !! for Dirac source: one source is concerned per point, we 
      !! only create the local arrays here.
      !! At most, we have all point-sources on the same processor, 
      !! that is, nsim. 
      !! actually, only the first ncell_loc are filled/allocated here.
      !!
      allocate(ctx_dg%src(j)%flag_on_bdry(ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%icell_loc   (ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%ndof_vol    (ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%ndof_face   (ctx_aq%sources(i_src)%nsim))
      !! in case of boundary source
      allocate(ctx_dg%src(j)%iface_index (ctx_aq%sources(i_src)%nsim))
      !! only the first n_cell_loc are actually allocated, possibly
      !! different for each of the component of the problem.
      allocate(ctx_dg%src(j)%weight       (ctx_dg%dim_sol,ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%weight_dbasis(ctx_dg%dim_sol,ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%hdg_S        (ctx_aq%sources(i_src)%nsim))
      ctx_dg%src(j)%icell_loc     = 0
      ctx_dg%src(j)%ncell_loc     = 0
      ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      ctx_dg%src(j)%flag_on_bdry  = .false.
      counter_cell = 0

      !! locate the corresponding cell
      do k=1, ctx_aq%sources(i_src)%nsim
        if(ctx_err%ierr .ne. 0) cycle
        local_cell = 0
        coo1d(1) = dble(ctx_aq%sources(i_src)%x(k))
        call dg_init_point_source_1d(ctx_paral,ctx_grid,ctx_dg,coo1d,   &
                                     ctx_aq%src_type,tag_scale_rhs,     &
                                     local_cell,ndof_vol,ndof_face,     &
                                     weight,weight_dbasis,ctx_err)

        if(local_cell .ne. 0) then
          !! it is on this processor, increment the counter
          counter_cell                          = counter_cell + 1
          ctx_dg%src(j)%icell_loc(counter_cell) = local_cell
          ndof_face_gb = sum(ndof_face)


          !! ---------------------------------------------------------
          !! check if the source is on the boundary or not, i.e., the
          !! the neighbor is outside. This is because the HDG source
          !! array for reconstruction only concerns the VOLUME sources
          !! ---------------------------------------------------------
          do l=1,ctx_grid%n_neigh_per_cell
            if(ctx_grid%cell_neigh(l,local_cell) < 0) then
              !! distance to boundary (1 coo in 1D)
              pos_bdry= ctx_grid%x_node(1,ctx_grid%cell_node(l,local_cell))
              d_bdry  = abs(coo1d(1)-pos_bdry)
              !! if closer than SIMPLE zeros
              if(d_bdry < tiny(1.0)) then
                ctx_dg%src(j)%flag_on_bdry(counter_cell) = .true.
                ctx_dg%src(j)%iface_index (counter_cell) = l
                ndof_face_gb = 1 !! only One Dof for 1D face.
              
              end if
            end if
          end do
          !! ---------------------------------------------------------

          !! it is different if the source if a boundary one or not.
          ctx_dg%src(j)%ndof_face(counter_cell) = ndof_face_gb 

          if(.not. ctx_dg%src(j)%flag_on_bdry (counter_cell)) then
            
            !! save the weight array, one per variable.
            do i_sol=1,ctx_dg%dim_sol
              call array_allocate(ctx_dg%src(j)%weight(i_sol,counter_cell), &
                                  ndof_vol,ctx_err)
              ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = weight(:)
            end do
            ctx_dg%src(j)%ndof_vol(counter_cell) = ndof_vol
            !! save the derivatives if needed
            select case(ctx_aq%src_type)
              case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX)
                !! same for all variables.
                do i_sol=1,ctx_dg%dim_sol
                  call array_allocate(                                       &
                             ctx_dg%src(j)%weight_dbasis(i_sol,counter_cell),&
                             ctx_dg%dim_domain,ndof_vol,ctx_err)
                  ctx_dg%src(j)%weight_dbasis(i_sol,counter_cell)%array(1,:)=&
                    weight_dbasis(1,:)
                end do
                deallocate(weight_dbasis)

              case(SRC_DIRAC)
                !! done above, ok
              case default
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //    &
                                "source [init_pt_src] **"
                ctx_err%ierr  = -300
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          !! the source is on the boundary
          !! -----------------------------------------------------------
          else
            !! boundary in 1D: only one dof which is set to 1.0 
            do i_sol=1,ctx_dg%dim_sol
              call array_allocate(ctx_dg%src(j)%weight(i_sol,counter_cell), &
                                  ndof_face_gb,ctx_err)
              ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = 1
            end do

            !! the ndof_vol is set to 0, but the ndof_face is set of 
            !! the ndof of only the concerned face.
            ctx_dg%src(j)%ndof_vol (counter_cell) = 0
            !! save the derivatives if needed
            select case(ctx_aq%src_type)
              case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,      &
                   SRC_DIRAC_DERIVY,SRC_DIRAC_DERIVZ)
                !! raise error
                ctx_err%msg   = "** ERROR: the use of Dirac " //        &
                                "derivatives for boundary condition "// &
                                "is not supported (it needs to be a "// &
                                "volume source) [init_pt_src] **"
                ctx_err%ierr  = -303
                ctx_err%critic=.true.
                call raise_error(ctx_err)
                
              case(SRC_DIRAC)
              case default
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //&
                                "source [init_pt_src] **"
                ctx_err%ierr  = -300
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          end if
          deallocate(weight)
        
        end if
      end do !! end loop over the point soures

      !! total number of cells on this processor     
      ctx_dg%src(j)%ncell_loc  = int(counter_cell,kind=4) !> force int4
    
    end do !! end loop over the rhs
    !! -----------------------------------------------------------------
           
    return

  end subroutine dg_source_init_1d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of sources in 2 dimension
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_2d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,       &
                               i_src_first,i_src_last,tag_scale_rhs,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer                             :: k,i_src,ndof_vol,ndof_face_gb
    integer                             :: ndof_face(3) !! 3 faces triangles
    integer                             :: l,j,i_sol
    integer(kind=IKIND_MESH)            :: local_cell,counter_cell
    real   (kind=RKIND_POL),allocatable :: weight(:)
    real   (kind=RKIND_POL),allocatable :: weight_dbasis(:,:)
    real(kind=8)                        :: coo2d(2)
    real(kind=8)                        :: node_face(2,2) !! 2D, 2nodes
    !! for face sources
    real(kind=8)                        :: xa,ya,xb,yb
    real(kind=8)                        :: alpha,beta,d_bdry
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    do i_src=i_src_first, i_src_last
      if(ctx_err%ierr .ne. 0) cycle
 
      j = i_src-i_src_first+1

      ! -------------------------------------------------------
      !! for Dirac source: one source is concerned per point, we 
      !! only create the local arrays here.
      !! At most, we have all point-sources on the same processor, 
      !! that is, nsim. 
      !! actually, only the first ncell_loc are filled/allocated here.
      !!
      allocate(ctx_dg%src(j)%flag_on_bdry(ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%icell_loc   (ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%ndof_vol    (ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%ndof_face   (ctx_aq%sources(i_src)%nsim))
      !! in case of boundary source
      allocate(ctx_dg%src(j)%iface_index (ctx_aq%sources(i_src)%nsim))
      !! only the first n_cell_loc are actually allocated
      allocate(ctx_dg%src(j)%weight       (ctx_dg%dim_sol,ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%weight_dbasis(ctx_dg%dim_sol,ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%hdg_S        (ctx_aq%sources(i_src)%nsim))
      ctx_dg%src(j)%icell_loc     = 0
      ctx_dg%src(j)%ncell_loc     = 0
      ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      ctx_dg%src(j)%flag_on_bdry  = .false.
      counter_cell = 0

      !! locate the corresponding cell
      do k=1, ctx_aq%sources(i_src)%nsim
        if(ctx_err%ierr .ne. 0) cycle
        local_cell = 0
        coo2d(1) = dble(ctx_aq%sources(i_src)%x(k))
        coo2d(2) = dble(ctx_aq%sources(i_src)%z(k))
        call dg_init_point_source_2d(ctx_paral,ctx_grid,ctx_dg,coo2d,   &
                                     ctx_aq%src_type,tag_scale_rhs,     &
                                     local_cell,ndof_vol,ndof_face,     &
                                     weight,weight_dbasis,ctx_err)

        if(local_cell .ne. 0) then
          !! it is on this processor, increment the counter
          counter_cell                          = counter_cell + 1
          ctx_dg%src(j)%icell_loc(counter_cell) = local_cell
          ndof_face_gb = sum(ndof_face)

          !! -------------------------------------------------------
          !! check if the source is on the boundary or not, i.e., the
          !! the neighbor is outside. This is because the HDG source
          !! array for reconstruction only concerns the VOLUME sources
          !! -------------------------------------------------------
          !!********************************************************
          !!********************************************************
          !!
          !! Detection is ok but, 
          !!  -> except if the point is on a node, it may not be in
          !!     the one at the boundary 
          !!  -> problem also if the point is shared by several faces?
          !!
          !!
          !! also the rhs creation in m_dg_lagrange_rsh is KO
          !!********************************************************
          !!********************************************************
          node_face = 0.d0
          do l=1,ctx_grid%n_neigh_per_cell
            if(ctx_err%ierr .ne. 0) cycle

            if(ctx_grid%cell_neigh(l,local_cell) < 0) then
              !! equation of the line is given by 
              !!     \alpha x_a + \beta = y_a
              !!     \alpha x_b + \beta = y_b
              !! =>  \alpha(x_a - x_b) = y_a - y_b
              !! neighbor l given by the opposite node...
              xa = ctx_grid%x_node(1,ctx_grid%cell_node(           &
                              interface_2D_triangle(1,l),local_cell))
              ya = ctx_grid%x_node(2,ctx_grid%cell_node(           &
                              interface_2D_triangle(1,l),local_cell))
              xb = ctx_grid%x_node(1,ctx_grid%cell_node(           &
                              interface_2D_triangle(2,l),local_cell))
              yb = ctx_grid%x_node(2,ctx_grid%cell_node(           &
                              interface_2D_triangle(2,l),local_cell))
          
              if( abs(xa-xb) > tiny(1.) ) then 
                alpha = (ya-yb)/(xa-xb)
                beta  = ya - alpha*xa
          
                !! check if the point is on the line 
                d_bdry  = abs(coo2d(2)- (alpha*coo2d(1) + beta))
                !! if closer than SIMPLE zeros
                if(d_bdry < tiny(1.)) then
                  ctx_dg%src(j)%flag_on_bdry(counter_cell) = .true.
                  ctx_dg%src(j)%iface_index (counter_cell) = l
                  ndof_face_gb = ndof_face(l) !! only one face for the ndof
                  !! savet the nodes 
                  node_face(1,1) = xa
                  node_face(2,1) = ya
                  node_face(1,2) = xb
                  node_face(2,2) = yb
                end if
              else !! flat surface in z only 
                d_bdry = abs(coo2d(1)-xa)
                if(d_bdry < tiny(1.)) then
                  ctx_dg%src(j)%flag_on_bdry(counter_cell) = .true.
                  ctx_dg%src(j)%iface_index (counter_cell) = l
                  ndof_face_gb = ndof_face(l) !! only one face for the ndof
                  !! save the nodes 
                  node_face(1,1) = xa
                  node_face(2,1) = ya
                  node_face(1,2) = xb
                  node_face(2,2) = yb
                end if
              end if
            end if
          end do
          !! -------------------------------------------------------
        
          !! it is different if the source if a boundary one or not.
          ctx_dg%src(j)%ndof_face(counter_cell) = ndof_face_gb 

          if(.not. ctx_dg%src(j)%flag_on_bdry (counter_cell)) then
            !! save the weight array
            do i_sol=1,ctx_dg%dim_sol
              call array_allocate(ctx_dg%src(j)%weight(i_sol,counter_cell), &
                                  ndof_vol,ctx_err)
              ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = weight(:)
            end do
            ctx_dg%src(j)%ndof_vol(counter_cell) = ndof_vol
            !! save the derivatives if needed
            select case(ctx_aq%src_type)
              case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,      &
                   SRC_DIRAC_DERIVZ)
                do i_sol=1,ctx_dg%dim_sol
                  call array_allocate(                                       &
                             ctx_dg%src(j)%weight_dbasis(i_sol,counter_cell),&
                             ctx_dg%dim_domain,ndof_vol,ctx_err)
                  ctx_dg%src(j)%weight_dbasis(i_sol,counter_cell)%array(:,:)=&
                               weight_dbasis(:,:)
                end do
                deallocate(weight_dbasis)
                
              case(SRC_DIRAC)
              case default
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //&
                                "source [init_pt_src] **"
                ctx_err%ierr  = -300
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          !! the source is on the boundary
          !! -----------------------------------------------------------
          else
            !! boundary source project onto the face only
            deallocate(weight)
            call dg_init_point_source_boundary_2d(ctx_grid,ctx_dg,coo2d,&
                               local_cell,                              &
                               ctx_dg%src(j)%iface_index(counter_cell), &
                               node_face,weight,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            !! save the face weight array
            do i_sol=1, ctx_dg%dim_sol
              call array_allocate(ctx_dg%src(j)%weight(i_sol,counter_cell),&
                                  ndof_face_gb,ctx_err)
              ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = weight(:)
            end do
            !! the ndof_vol is set to 0, but the ndof_face is set of 
            !! the ndof of onlyt the concerned face.
            ctx_dg%src(j)%ndof_vol (counter_cell) = 0
            select case(ctx_aq%src_type)
              case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,      &
                   SRC_DIRAC_DERIVY,SRC_DIRAC_DERIVZ)
                !! raise error
                ctx_err%msg   = "** ERROR: the use of Dirac " //        &
                                "derivatives for boundary condition "// &
                                "is not supported (it needs to be a "// &
                                "volume source) [init_pt_src] **"
                ctx_err%ierr  = -303
                ctx_err%critic=.true.
                call raise_error(ctx_err)
                
              case(SRC_DIRAC)
              case default
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //    &
                                "source [init_pt_src] **"
                ctx_err%ierr  = -300
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select          
          
          end if
          deallocate(weight)
        
        end if
      end do !! end loop over the point soures

      !! total number of cells on this processor
      ctx_dg%src(j)%ncell_loc  = int(counter_cell,kind=4) !> force int4
    
    end do !! end loop over the rhs
    !! -----------------------------------------------------------------
           
    return

  end subroutine dg_source_init_2d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of sources in 3 dimensions
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_3d(ctx_paral,ctx_aq,ctx_grid,ctx_dg,        &
                               i_src_first,i_src_last,tag_scale_rhs,    &
                               ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer                             :: k,i_src,ndof_vol,ndof_face_gb
    integer                             :: ndof_face(4) !! 4 faces tetra
    integer                             :: l,j,i_sol
    integer(kind=IKIND_MESH)            :: local_cell,counter_cell
    real   (kind=RKIND_POL),allocatable :: weight(:)
    real   (kind=RKIND_POL),allocatable :: weight_dbasis(:,:)
    real(kind=8)                        :: coo3d(3)
    real(kind=8)                        :: node_face(3,3) !! 3D, 3 nodes
    !! for face sources
    real(kind=8)                        :: pta(3),ptb(3),ptc(3),d_bdry
    real(kind=8)                        :: v1(3),v2(3),cproduct(3),a,b,c,d
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    do i_src=i_src_first, i_src_last
      if(ctx_err%ierr .ne. 0) cycle
 
      j = i_src-i_src_first+1

      ! -------------------------------------------------------
      !! for Dirac source: one source is concerned per point, we 
      !! only create the local arrays here.
      !! At most, we have all point-sources on the same processor, 
      !! that is, nsim. 
      !! actually, only the first ncell_loc are filled/allocated here.
      !!
      allocate(ctx_dg%src(j)%flag_on_bdry(ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%icell_loc   (ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%ndof_vol    (ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%ndof_face   (ctx_aq%sources(i_src)%nsim))
      !! in case of boundary source
      allocate(ctx_dg%src(j)%iface_index (ctx_aq%sources(i_src)%nsim))
      !! only the first n_cell_loc are actually allocated
      allocate(ctx_dg%src(j)%weight       (ctx_dg%dim_sol,ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%weight_dbasis(ctx_dg%dim_sol,ctx_aq%sources(i_src)%nsim))
      allocate(ctx_dg%src(j)%hdg_S        (ctx_aq%sources(i_src)%nsim))
      ctx_dg%src(j)%icell_loc     = 0
      ctx_dg%src(j)%ncell_loc     = 0
      ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      ctx_dg%src(j)%flag_on_bdry  = .false.
      counter_cell = 0

      !! locate the corresponding cell
      do k=1, ctx_aq%sources(i_src)%nsim
        if(ctx_err%ierr .ne. 0) cycle
        local_cell = 0
        coo3d(1) = dble(ctx_aq%sources(i_src)%x(k))
        coo3d(2) = dble(ctx_aq%sources(i_src)%y(k))
        coo3d(3) = dble(ctx_aq%sources(i_src)%z(k))
        call dg_init_point_source_3d(ctx_paral,ctx_grid,ctx_dg,coo3d,   &
                                     ctx_aq%src_type,tag_scale_rhs,     &
                                     local_cell,ndof_vol,ndof_face,     &
                                     weight,weight_dbasis,ctx_err)

        if(local_cell .ne. 0) then
          !! it is on this processor, increment the counter
          counter_cell                          = counter_cell + 1
          ctx_dg%src(j)%icell_loc(counter_cell) = local_cell
          ndof_face_gb = sum(ndof_face)

          !! -------------------------------------------------------
          !! check if the source is on the boundary or not, i.e., the
          !! the neighbor is outside. This is because the HDG source
          !! array for reconstruction only concerns the VOLUME sources
          !! -------------------------------------------------------
          !!********************************************************
          !!********************************************************
          !!
          !! Detection is ok but, 
          !!  -> except if the point is on a node, it may not be in
          !!     the one at the boundary 
          !!  -> problem also if the point is shared by several faces?
          !!
          !!
          !! also the rhs creation in m_dg_lagrange_rsh is KO
          !!********************************************************
          !!********************************************************
          node_face = 0.d0
          do l=1,ctx_grid%n_neigh_per_cell
            if(ctx_err%ierr .ne. 0) cycle

            if(ctx_grid%cell_neigh(l,local_cell) < 0) then

              !! follow steps for equation of the plane:
              !! get two vectors from the three nodes
              pta = ctx_grid%x_node(:,ctx_grid%cell_node(           &
                           interface_3D_tetrahedron(1,l),local_cell))
              ptb = ctx_grid%x_node(:,ctx_grid%cell_node(           &
                           interface_3D_tetrahedron(2,l),local_cell))
              ptc = ctx_grid%x_node(:,ctx_grid%cell_node(           &
                           interface_3D_tetrahedron(3,l),local_cell))
              
              v1 = pta-ptb
              v2 = ptc-ptb
              !! compute cross product
              cproduct(1) = v1(2)*v2(3) - v1(3)*v2(2)
              cproduct(2) = v1(3)*v2(1) - v1(1)*v2(3)
              cproduct(3) = v1(1)*v2(2) - v1(2)*v2(1)
                          
              !! plane equation given by a x + b y + c z = d with,
              a=cproduct(1)
              b=cproduct(2)
              c=cproduct(3)
              d=a*pta(1)+b*pta(2)+c*pta(3)
              !! check if the point is on ly line 
              d_bdry  = abs(d - (a*coo3d(1)+b*coo3d(2)+c*coo3d(3)))
              !! if closer than SIMPLE zeros
              
              !! if closer than SIMPLE zeros
              if(d_bdry < tiny(1.0)) then
                ctx_dg%src(j)%flag_on_bdry(counter_cell) = .true.
                ctx_dg%src(j)%iface_index (counter_cell) = l
                ndof_face_gb = ndof_face(l) !! only one face for the ndof
                !! save the nodes 
                node_face(:,1) = pta
                node_face(:,2) = ptb
                node_face(:,3) = ptc
              end if
              
            end if
          end do
          !! -------------------------------------------------------
        
          !! it is different if the source if a boundary one or not.
          ctx_dg%src(j)%ndof_face(counter_cell) = ndof_face_gb 

          if(.not. ctx_dg%src(j)%flag_on_bdry (counter_cell)) then
            !! save the weight array
            do i_sol=1, ctx_dg%dim_sol
              call array_allocate(                                      &
                         ctx_dg%src(j)%weight(i_sol,counter_cell),      &
                         ndof_vol,ctx_err)
              ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = weight(:)
            end do
            ctx_dg%src(j)%ndof_vol (counter_cell)          = ndof_vol

            !! save the derivatives if needed
            select case(ctx_aq%src_type)
              case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,      &
                   SRC_DIRAC_DERIVY,SRC_DIRAC_DERIVZ)
                do i_sol=1, ctx_dg%dim_sol
                  call array_allocate(                                  &
                             ctx_dg%src(j)%weight_dbasis(i_sol,counter_cell),&
                             ctx_dg%dim_domain,ndof_vol,ctx_err)
                  ctx_dg%src(j)%weight_dbasis(i_sol,counter_cell)%array(:,:)=&
                    weight_dbasis(:,:)
                end do
                deallocate(weight_dbasis)

              case(SRC_DIRAC,SRC_ANALYTIC)
              case default
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //&
                                "source [init_pt_src] **"
                ctx_err%ierr  = -300
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          !! the source is on the boundary
          !! -----------------------------------------------------------
          else
            !! boundary source project onto the face only
            deallocate(weight)
            call dg_init_point_source_boundary_3d(ctx_grid,ctx_dg,coo3d,&
                               local_cell,                              &
                               ctx_dg%src(j)%iface_index(counter_cell), &
                               node_face,weight,ctx_err)

            if(ctx_err%ierr .ne. 0) cycle
            !! save the face weight array
            do i_sol=1, ctx_dg%dim_sol
              call array_allocate(ctx_dg%src(j)%weight(i_sol,counter_cell), &
                                  ndof_face_gb,ctx_err)
              ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = weight(:)
            end do
            !! the ndof_vol is set to 0, but the ndof_face is set of 
            !! the ndof of onlyt the concerned face.
            ctx_dg%src(j)%ndof_vol (counter_cell) = 0
            select case(ctx_aq%src_type)
              case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,      &
                   SRC_DIRAC_DERIVY,SRC_DIRAC_DERIVZ)
                !! raise error
                ctx_err%msg   = "** ERROR: the use of Dirac " //        &
                                "derivatives for boundary condition "// &
                                "is not supported (it needs to be a "// &
                                "volume source) [init_pt_src] **"
                ctx_err%ierr  = -303
                ctx_err%critic=.true.
                call raise_error(ctx_err)
                
              case(SRC_DIRAC,SRC_ANALYTIC)
              case default
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //    &
                                "source [init_pt_src] **"
                ctx_err%ierr  = -302
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          end if
          deallocate(weight)
        end if
      end do !! end loop over the point soures

      !! total number of cells on this processor     
      ctx_dg%src(j)%ncell_loc  = int(counter_cell,kind=4) !> force int4
    end do !! end loop over the rhs
    !! -----------------------------------------------------------------
           
    return

  end subroutine dg_source_init_3d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for a point source
  !>
  !----------------------------------------------------------------------------
  subroutine dg_init_point_source_1d(ctx_paral,ctx_grid,ctx_dg,coo1d,   &
                                     src_type,tag_scale_rhs,            &
                                     local_cell,ndof_vol,ndof_face,     &
                                     weight,weight_dbasis,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: coo1d(1)
    integer                          ,intent(in)   :: src_type
    integer                          ,intent(in)   :: tag_scale_rhs
    integer(kind=IKIND_MESH)         ,intent(out)  :: local_cell
    integer                          ,intent(out)  :: ndof_vol
    integer                          ,intent(out)  :: ndof_face(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight_dbasis(:,:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)            :: basis
    integer                  :: order,i_order,i_dof,f
    integer(kind=IKIND_MESH) :: face
    real   (kind=RKIND_POL)  :: coo_loc(1)
    real   (kind=RKIND_POL)  :: coeff
    logical                  :: flag_dbasis
    real   (kind=8), parameter :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    local_cell = 0
    ndof_face  = 0
    ndof_vol   = 0
    !! *****************************************************************
    !! ** WARNING ******* THIS ROUTINE USES OMP  ***********************
    !! *****************************************************************
    call find_cell(ctx_paral,coo1d,local_cell,ctx_grid%x_node,          &
                   ctx_grid%cell_node,ctx_grid%index_loctoglob_cell,    &
                   ctx_grid%n_cell_loc,ctx_grid%n_cell_gb,              &
                   ctx_grid%dim_domain,ctx_grid%cell_neigh,             &
                   ctx_dg%format_cell,.true.,ctx_err)

    !! we now compute the coefficient weight for the cell we have
    if(local_cell .ne. 0) then
      !! for the face dof, we sum all of face
      ndof_face=0
      do f=1,ctx_grid%n_neigh_per_cell
        face     = ctx_grid%cell_face   (f,local_cell)
        order    = ctx_grid%order_face  (face)
        i_order  = ctx_dg%order_to_index(order)
        ndof_face(f) = ctx_dg%n_dof_face_per_order(i_order)
      enddo
      order    = ctx_grid%order             (local_cell)
      i_order  = ctx_dg%order_to_index      (order)
      ndof_vol = ctx_dg%n_dof_per_order     (i_order)

      allocate(weight(ndof_vol))
      weight=0.0
      flag_dbasis=.false.
      !! evaluate the basis function:
      !!   1) get the local coordinates on the reference element
      !!   2) evaluate the basis function at this location
      !! remove offset from the very first node
      coo_loc(:) = real(coo1d(:)-                                       &
       ctx_grid%x_node(:,ctx_grid%cell_node(1,local_cell)),kind=RKIND_POL)
      coo_loc(:) = real(ctx_dg%inv_jac(1,1,local_cell)*dble(coo_loc(1)),&
                        kind=RKIND_POL)

      !! ---------------------------------------------------------------
      !! compute basis function, derivative may be needed.
      call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
      !! always the regular one
      do i_dof=1,ndof_vol
        call polynomial_eval(basis%pol(i_dof),coo_loc(1),coeff,ctx_err)
        weight(i_dof) = coeff
      end do
      !! possibly the derivatives
      select case(src_type)
        case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX)
          flag_dbasis = .true.
          allocate(weight_dbasis(1,ndof_vol)) !! dimension 1
          weight_dbasis=0.0
          call basis_derivative_construct(basis,1,ctx_err)
          do i_dof=1,ndof_vol
            call polynomial_eval(basis%pol_derivative(1)%p(i_dof,1),    &
                                 coo_loc(1),coeff,ctx_err)
            weight_dbasis(1,i_dof) = coeff*ctx_dg%inv_jac(1,1,local_cell)
          end do

        case(SRC_DIRAC)
        case default
          !! raise error
          ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //    &
                          "source [init_pt_src] **"
          ctx_err%ierr  = -300
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      call basis_clean(basis)
      !! ---------------------------------------------------------------

      !! possibility of scaling here -----------------------------------
      select case(tag_scale_rhs)
        case(tag_RHS_SCALE_ID)
          !! nothing to do 
        case(tag_RHS_SCALE_INV2PI)
          weight = weight * 1.d0/(2.d0*pi)
          if(flag_dbasis) weight_dbasis = weight_dbasis* 1.d0/(2.d0*pi)

        case(tag_RHS_SCALE_R,tag_RHS_SCALE_R2,tag_RHS_SCALE_X,          &
             tag_RHS_SCALE_X2,tag_RHS_SCALE_XINV2PI)
          !! multiply by the norm of the source position.
          !! it cannot be zero. 
          if(abs(coo1d(1)) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            select case(tag_scale_rhs)
              case(tag_RHS_SCALE_R,tag_RHS_SCALE_X)
                weight = weight * abs(coo1d(1))
                if(flag_dbasis) then
                  !! we have \nabla(x F) = x \nabla(F) + F
                  !! in 1D we have that r=x anyway
                  weight_dbasis(1,:) = weight_dbasis(1,:)*abs(coo1d(1)) &
                                     + weight(:)
                end if
              case(tag_RHS_SCALE_XINV2PI)
                weight = weight * abs(coo1d(1))/(2.d0*pi)
                if(flag_dbasis) then
                  !! we have \nabla(x F) = x \nabla(F) + F
                  !! in 1D we have that r=x anyway
                  weight_dbasis(1,:) = weight_dbasis(1,:)               &
                                      *abs(coo1d(1))/(2.d0*pi)          &
                                     + weight(:)/(2.d0*pi)
                end if
              case(tag_RHS_SCALE_R2,tag_RHS_SCALE_X2)
                weight = weight * abs(coo1d(1))*abs(coo1d(1))
                if(flag_dbasis) then
                  !! we have \nabla(x F) = x \nabla(F) + F
                  !! in 1D we have that r=x anyway
                  weight_dbasis(1,:) = weight_dbasis(1,:)*abs(coo1d(1)) &
                                                         *abs(coo1d(1)) &
                                     + 2.d0*abs(coo1d(1))*weight(:)
                end if
            end select
          end if

        case default
          ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                          "RHS [init_point_source_1D] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          !! non-shared error raised at above level
      end select 
      !! --------------------------------------------------------------- 

    end if

    return
  end subroutine dg_init_point_source_1d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for a point source
  !>
  !----------------------------------------------------------------------------
  subroutine dg_init_point_source_2d(ctx_paral,ctx_grid,ctx_dg,coo2d,   &
                                     src_type,tag_scale_rhs,            &
                                     local_cell,ndof_vol,ndof_face,     &
                                     weight,weight_dbasis,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: coo2d(2)
    integer                          ,intent(in)   :: src_type
    integer                          ,intent(in)   :: tag_scale_rhs
    integer(kind=IKIND_MESH)         ,intent(out)  :: local_cell
    integer                          ,intent(out)  :: ndof_vol
    integer                          ,intent(out)  :: ndof_face(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight_dbasis(:,:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)            :: basis
    integer                  :: order,i_order,i_dof,f
    integer(kind=IKIND_MESH) :: face
    real   (kind=RKIND_POL)  :: coo_loc(2)
    real   (kind=RKIND_POL)  :: coeff,coeff_dx,coeff_dz
    logical                  :: flag_dbasis
    real   (kind=8), parameter :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    local_cell = 0
    ndof_face  = 0
    ndof_vol   = 0
    !! *****************************************************************
    !! ** WARNING ******* THIS ROUTINE USES OMP  ***********************
    !! *****************************************************************
    call find_cell(ctx_paral,coo2d,local_cell,ctx_grid%x_node,          &
                   ctx_grid%cell_node,ctx_grid%index_loctoglob_cell,    &
                   ctx_grid%n_cell_loc,ctx_grid%n_cell_gb,              &
                   ctx_grid%dim_domain,ctx_grid%cell_neigh,             &
                   ctx_dg%format_cell,.true.,ctx_err)

    !! we now compute the coefficient weight for the cell we have
    if(local_cell .ne. 0) then
      !! for the face dof, we sum all of face
      ndof_face=0
      do f=1,ctx_grid%n_neigh_per_cell
        face     = ctx_grid%cell_face   (f,local_cell)
        order    = ctx_grid%order_face  (face)
        i_order  = ctx_dg%order_to_index(order)
        ndof_face(f)= ctx_dg%n_dof_face_per_order(i_order)
      enddo
      order    = ctx_grid%order             (local_cell)
      i_order  = ctx_dg%order_to_index      (order)
      ndof_vol = ctx_dg%n_dof_per_order     (i_order)

      allocate(weight(ndof_vol))
      weight=0.0
      flag_dbasis = .false.

      !! evaluate the basis function:
      !!   1) get the local coordinates on the reference element
      !!   2) evaluate the basis function at this location
      !! remove offset from the very first node
      coo_loc(:) = real(coo2d(:) -                                      &
       ctx_grid%x_node(:,ctx_grid%cell_node(1,local_cell)),kind=RKIND_POL)
      coo_loc(:) = real(matmul(ctx_dg%inv_jac(:,:,local_cell),          &
                   dble(coo_loc(:))),kind=RKIND_POL)

      !! ---------------------------------------------------------------
      !! compute basis function: always the regular one
      call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
      do i_dof=1,ndof_vol
        call polynomial_eval(basis%pol(i_dof),coo_loc(1),coo_loc(2), &
                             coeff,ctx_err)
        weight(i_dof) = coeff
      end do
      !! possibly the derivatives
      select case(src_type)
        case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,SRC_DIRAC_DERIVZ)
          flag_dbasis=.true.
          allocate(weight_dbasis(ctx_dg%dim_domain,ndof_vol)) !! dimension 2
          weight_dbasis=0.0
          call basis_derivative_construct(basis,1,ctx_err)

          do i_dof=1,ndof_vol
              !! compute dx and dz on the reference element:
              call polynomial_eval(basis%pol_derivative(1)%p(i_dof,1),  &
                                   coo_loc(1),coo_loc(2),coeff_dx,ctx_err)
              call polynomial_eval(basis%pol_derivative(1)%p(i_dof,2),  &
                                   coo_loc(1),coo_loc(2),coeff_dz,ctx_err)
              !! transfer to actual element
              weight_dbasis(1,i_dof) =                                  &
                          coeff_dx*ctx_dg%inv_jac(1,1,local_cell) +     &
                          coeff_dz*ctx_dg%inv_jac(2,1,local_cell)
              weight_dbasis(2,i_dof) =                                  &
                          coeff_dx*ctx_dg%inv_jac(1,2,local_cell) +     &
                          coeff_dz*ctx_dg%inv_jac(2,2,local_cell)
          end do

        case(SRC_DIRAC)
        case default
          !! raise error
          ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //    &
                          "source [init_pt_src] **"
          ctx_err%ierr  = -300
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      call basis_clean(basis)
      !! ---------------------------------------------------------------
      
      !! possibility of scaling here -----------------------------------
      select case(tag_scale_rhs)
        case(tag_RHS_SCALE_ID)
          !! nothing to do 
        case(tag_RHS_SCALE_INV2PI)
          weight = weight * 1.d0/(2.d0*pi)
          if(flag_dbasis) weight_dbasis = weight_dbasis* 1.d0/(2.d0*pi)
        case(tag_RHS_SCALE_R,tag_RHS_SCALE_R2)
          !! multiply by the norm of the source position.
          !! it cannot be zero. 
          if(norm2(coo2d) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            select case(tag_scale_rhs)
              case(tag_RHS_SCALE_R)
                weight = weight * norm2(coo2d)
                if(flag_dbasis) then
                  !! we have \nabla(r F) = r \nabla(F) + \nabla(r) . F
                  !! with r=(x^2 + z^2)^(1/2) 
                  !! => dr/dx = x (x^2 + z^2)^(-1/2)
                  !!          = x / r 
                  weight_dbasis(1,:) = weight_dbasis(1,:)*norm2(coo2d)  &
                                     + coo2d(1)/norm2(coo2d)*weight(:)
                  weight_dbasis(2,:) = weight_dbasis(2,:)*norm2(coo2d)  &
                                     + coo2d(2)/norm2(coo2d)*weight(:)
                end if
              case(tag_RHS_SCALE_R2)
                weight = weight * norm2(coo2d) * norm2(coo2d)
                if(flag_dbasis) then
                  !! we have \nabla(r F) = r \nabla(F) + \nabla(r) . F
                  !! with r² = (x^2 + z^2)
                  !! => dr/dx = 2 x 
                  weight_dbasis(1,:) = weight_dbasis(1,:)*(norm2(coo2d)**2) &
                                     + 2.d0*coo2d(1)*weight(:)
                  weight_dbasis(2,:) = weight_dbasis(2,:)*(norm2(coo2d)**2) &
                                     + 2.d0*coo2d(2)*weight(:)

                  weight_dbasis = weight_dbasis * norm2(coo2d) * norm2(coo2d)
                end if
            end select
            
          end if
        case(tag_RHS_SCALE_X)
          !! multiply by the coorindate in x, which cannot be zero. 
          if(abs(coo2d(1)) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            weight = weight * coo2d(1)
            if(flag_dbasis) then
              !! we have \nabla(x F) = x \nabla(F) + F
              weight_dbasis(1,:) = weight_dbasis(1,:)*coo2d(1) + weight(:)
              weight_dbasis(2,:) = weight_dbasis(2,:)*coo2d(1)
            end if
          end if

        case(tag_RHS_SCALE_XINV2PI)
          !! multiply by the coorindate in x, which cannot be zero. 
          if(abs(coo2d(1)) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            weight = weight * coo2d(1)/(2.d0*pi)
            if(flag_dbasis) then
              !! we have \nabla(x F) = x \nabla(F) + F
              weight_dbasis(1,:) = weight_dbasis(1,:)*coo2d(1)/(2.d0*pi)&
                                 + weight(:)/(2.d0*pi)
              weight_dbasis(2,:) = weight_dbasis(2,:)*coo2d(1)/(2.d0*pi)
            end if
          end if

        case(tag_RHS_SCALE_X2)
          !! multiply by the coorindate in x, which cannot be zero. 
          if(abs(coo2d(1)**2) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            weight = weight * coo2d(1) * coo2d(1)
            if(flag_dbasis) then
              !! we have \nabla(x² F) = x² \nabla(F) + 2 x F
              weight_dbasis(1,:) = weight_dbasis(1,:)*(coo2d(1)**2)     &
                                 + 2.d0*coo2d(1)*weight(:)
              weight_dbasis(2,:) = weight_dbasis(2,:)*(coo2d(1)**2)
            end if
          end if

        case default
          ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                          "RHS [init_point_source_2D] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          !! non-shared error raised at above level
      end select 
      !! ---------------------------------------------------------------    
    end if

    return
  end subroutine dg_init_point_source_2d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for a point source that is on the boundary
  !>
  !----------------------------------------------------------------------------
  subroutine dg_init_point_source_boundary_2d(ctx_grid,ctx_dg,coo2d,    &
                                              local_cell,i_face,        &
                                              cooface,weight,ctx_err)
    implicit none
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: coo2d(2)
    integer(kind=IKIND_MESH)         ,intent(in)   :: local_cell
    integer(kind=4)                  ,intent(in)   :: i_face
    real   (kind=8)                  ,intent(in)   :: cooface(2,2)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight(:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)            :: basis
    integer                  :: order,i_order,i_dof,ndof_face
    integer(kind=IKIND_MESH) :: face
    real   (kind=RKIND_POL)  :: coo_project(1)
    real   (kind=RKIND_POL)  :: coeff
    real   (kind=8)          :: normcoo1,normcoo2,pmin,pmax
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    face      = ctx_grid%cell_face(i_face,local_cell)
    order     = ctx_grid%order_face  (face)
    i_order   = ctx_dg%order_to_index(order)
    ndof_face = ctx_dg%n_dof_face_per_order(i_order)

    allocate(weight(ndof_face))
    weight=0.0

    !! Here we could use the volume polynomial, which can be created 
    !! on the face too, using 
    !! call basis_construct     (ctx_grid%dim_domain,order,basis,ctx_err)
    !! call basis_face_construct(basis,0,ctx_err) !! 0th order derivative
    !! HOWEVER, it is not correct because we do not have as many 
    !!          degrees as the number of the volumic dof.
    !! INSTEAD, we juste create a basis function in dimension dim-1=1
    !!          with order the order of the FACE!
    !! then, we simply save the weight for this function.
    
    !! Projection of the 2D position onto the face, given by cooface(2,2)
    !! get the two nodes that correspond to the face
    !! the norm should work, we already know we are on the line.
    normcoo1 = norm2(cooface(:,1))
    normcoo2 = norm2(cooface(:,2))
    pmin     = min(normcoo1,normcoo2)
    pmax     = max(normcoo1,normcoo2)
    coo_project(1) = (norm2(coo2d) - pmin)/(pmax - pmin)  

    if(coo_project(1) < 0.d0 .or. coo_project(1) > 1.d0) then 
      ctx_err%msg   = "** ERROR: the projection to the boundary gives"//&
                      " a point outside the reference element [dg_pt_src] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! using the order of the FACE.
    call basis_construct(ctx_grid%dim_domain-1,order,basis,ctx_err)
    do i_dof=1,ndof_face
      call polynomial_eval(basis%pol(i_dof),coo_project(1),coeff,ctx_err)
      weight(i_dof) = coeff
    end do
    call basis_clean(basis)
      
    !! We do not scale when it is at the surface as it is not an
    !! interior source -------------------------------------------------


    return
  end subroutine dg_init_point_source_boundary_2d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for a point source that is on the boundary
  !>
  !----------------------------------------------------------------------------
  subroutine dg_init_point_source_boundary_3d(ctx_grid,ctx_dg,coo3d,    &
                                              local_cell,i_face,        &
                                              cooface,weight,ctx_err)
    implicit none
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: coo3d(3)
    integer(kind=IKIND_MESH)         ,intent(in)   :: local_cell
    integer(kind=4)                  ,intent(in)   :: i_face
    real   (kind=8)                  ,intent(in)   :: cooface(3,3)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight(:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)            :: basis
    integer                  :: order,i_order,i_dof,ndof_face
    integer(kind=IKIND_MESH) :: face
    real   (kind=8)          :: coo_project(2)
    real   (kind=RKIND_POL)  :: coeff
    !! to convert to the 2D face.
    real(kind=8)             :: a1,b1,c1,a2,b2,c2,a3,b3,c3
    real(kind=8)             :: matA(3,2),matB(3)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    face      = ctx_grid%cell_face(i_face,local_cell)
    order     = ctx_grid%order_face  (face)
    i_order   = ctx_dg%order_to_index(order)
    ndof_face = ctx_dg%n_dof_face_per_order(i_order)

    allocate(weight(ndof_face))
    weight=0.0
    !! Here we could use the volume polynomial, which can be created 
    !! on the face too, using 
    !! call basis_construct     (ctx_grid%dim_domain,order,basis,ctx_err)
    !! call basis_face_construct(basis,0,ctx_err) !! 0th order derivative
    !! HOWEVER, it is not correct because we do not have as many 
    !!          degrees as the number of the volumic dof.
    !! INSTEAD, we juste create a basis function in dimension dim-1=1
    !!          with order the order of the FACE!
    !! then, we simply save the weight for this function.
    
    !! convert face 3D triangles to 2D face
    
    !! ----------------------------------
    !! say xnode1 => (0,0)
    !! say xnode2 => (0,1)
    !! say xnode3 => (1,0)
    !! we have a1 xhat + b1 yhat + c1 = x
    !!         a2 xhat + b2 yhat + c2 = y
    !!         a3 xhat + b3 yhat + c3 = z
    !! ----------------------------------
    !! a1 0 + b1 0 + c1 = x1
    !! a1 0 + b1 1 + c1 = x2
    !! a1 1 + b1 0 + c1 = x3
    !! ----------------------------------
    !!   
    c1  = cooface(1,1) ; c2 = cooface(2,1) ; c3 = cooface(3,1) 
    a1  = cooface(1,3) - c1 
    b1  = cooface(1,2) - c1 
    a2  = cooface(2,3) - c2 
    b2  = cooface(2,2) - c2 
    a3  = cooface(3,3) - c3 
    b3  = cooface(3,2) - c3 
    
    !! convert the input coo3d to the 2d coordinates on the reference 
    !! element, we have 
    !!   A X2D  = X3D     ==>   X2D = A^{-1} X3D.
    !!       ( a1   b1   c1) (X2D_x)    (x3D_x)
    !!       ( a2   b2   c2) (X2D_z)  = (x3D_y)
    !!       ( a3   b3   c3) (1.0  )    (x3D_z)
    !! or 
    !!       ( a1   b1) (X2D_x)    (x3D_x - c1)
    !!       ( a2   b2) (X2D_z)  = (x3D_y - c2)
    !!       ( a3   b3)            (x3D_z - c3)

    !! -----------------------------------------------------------------
    matA(1,1) = a1 ; matA(1,2) = b1
    matA(2,1) = a2 ; matA(2,2) = b2
    matA(3,1) = a3 ; matA(3,2) = b3
    matB(1)   = coo3d(1) - c1
    matB(2)   = coo3d(2) - c2
    matB(3)   = coo3d(3) - c3
    !! false indicates we do not solve the adjoint, we force double
    !! precision here.
    call lapack_solve_system(matA,matB,3,2,.false.,coo_project,ctx_err)
   
    if(norm2(coo_project) > 1.d0) then 
      ctx_err%msg   = "** ERROR: the projection to the boundary gives"//&
                      " a point outside the reference element [dg_pt_src] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    
    !! using the order of the FACE.
    call basis_construct(ctx_grid%dim_domain-1,order,basis,ctx_err)
    do i_dof=1,ndof_face
      !! respect pol kind
      call polynomial_eval(basis%pol(i_dof),                            &
                           real(coo_project(1),kind=RKIND_POL),         &
                           real(coo_project(2),kind=RKIND_POL),         &
                           coeff,ctx_err)
      weight(i_dof) = coeff
    end do
    call basis_clean(basis)

    !! We do not scale when it is at the surface as it is not an
    !! interior source -------------------------------------------------

    return
  end subroutine dg_init_point_source_boundary_3d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for a point source
  !>
  !----------------------------------------------------------------------------
  subroutine dg_init_point_source_3d(ctx_paral,ctx_grid,ctx_dg,coo3d,    &
                                     src_type,tag_scale_rhs,             &
                                     local_cell,ndof_vol,ndof_face,      &
                                     weight,weight_dbasis,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: coo3d(3)
    integer                          ,intent(in)   :: src_type
    integer                          ,intent(in)   :: tag_scale_rhs
    integer(kind=IKIND_MESH)         ,intent(out)  :: local_cell
    integer                          ,intent(out)  :: ndof_vol
    integer                          ,intent(out)  :: ndof_face(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight_dbasis(:,:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)            :: basis
    integer                  :: order,i_order,i_dof,f
    integer(kind=IKIND_MESH) :: face
    real   (kind=RKIND_POL)  :: coo_loc(3)
    real   (kind=RKIND_POL)  :: coeff,coeff_dx,coeff_dy,coeff_dz
    logical                  :: flag_dbasis
    real   (kind=8), parameter :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    local_cell = 0
    ndof_face  = 0
    ndof_vol   = 0
    !! *****************************************************************
    !! ** WARNING ******* THIS ROUTINE USES OMP  ***********************
    !! *****************************************************************
    call find_cell(ctx_paral,coo3d,local_cell,ctx_grid%x_node,          &
                   ctx_grid%cell_node,ctx_grid%index_loctoglob_cell,    &
                   ctx_grid%n_cell_loc,ctx_grid%n_cell_gb,              &
                   ctx_grid%dim_domain,ctx_grid%cell_neigh,             &
                   ctx_dg%format_cell,.true.,ctx_err)

    !! we now compute the coefficient weight for the cell we have
    if(local_cell .ne. 0) then
      !! for the face dof, we sum all of face
      ndof_face=0
      do f=1,ctx_grid%n_neigh_per_cell
        face     = ctx_grid%cell_face   (f,local_cell)
        order    = ctx_grid%order_face  (face)
        i_order  = ctx_dg%order_to_index(order)
        ndof_face(f) = ctx_dg%n_dof_face_per_order(i_order)
      enddo
      order    = ctx_grid%order             (local_cell)
      i_order  = ctx_dg%order_to_index      (order)
      ndof_vol = ctx_dg%n_dof_per_order     (i_order)

      allocate(weight(ndof_vol))
      weight=0.0
      flag_dbasis = .false.
      !! evaluate the basis function:
      !!   1) get the local coordinates on the reference element
      !!   2) evaluate the basis function at this location
      !! remove offset from the very first node
      coo_loc(:) = real(coo3d(:) -                                      &
       ctx_grid%x_node(:,ctx_grid%cell_node(1,local_cell)),kind=RKIND_POL)
      coo_loc(:) = real(matmul(ctx_dg%inv_jac(:,:,local_cell),          &
                   dble(coo_loc(:))),kind=RKIND_POL)

      !! compute basis function: always the regular one
      call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
      do i_dof=1,ndof_vol
        call polynomial_eval(basis%pol(i_dof),coo_loc(1),coo_loc(2),    &
                             coo_loc(3),coeff,ctx_err)
        weight(i_dof) = coeff
      end do
      !! possibly the derivatives
      select case(src_type)
        case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,            &
             SRC_DIRAC_DERIVY,SRC_DIRAC_DERIVZ)
          flag_dbasis=.true.
          allocate(weight_dbasis(ctx_dg%dim_domain,ndof_vol)) !! dimension 3
          weight_dbasis=0.0
          call basis_derivative_construct(basis,1,ctx_err)
          do i_dof=1,ndof_vol
              !! compute dx and dz on the reference element:
              call polynomial_eval(basis%pol_derivative(1)%p(i_dof,1),  &
                                   coo_loc(1),coo_loc(2),coo_loc(3),coeff_dx,ctx_err)
              call polynomial_eval(basis%pol_derivative(1)%p(i_dof,2),  &
                                   coo_loc(1),coo_loc(2),coo_loc(3),coeff_dy,ctx_err)
              call polynomial_eval(basis%pol_derivative(1)%p(i_dof,3),  &
                                   coo_loc(1),coo_loc(2),coo_loc(3),coeff_dz,ctx_err)
              !! transfer to actual element
              weight_dbasis(1,i_dof) =                                  &
                          coeff_dx*ctx_dg%inv_jac(1,1,local_cell) +     &
                          coeff_dy*ctx_dg%inv_jac(2,1,local_cell) +     &
                          coeff_dz*ctx_dg%inv_jac(3,1,local_cell)
              weight_dbasis(2,i_dof) =                                  &
                          coeff_dx*ctx_dg%inv_jac(1,2,local_cell) +     &
                          coeff_dy*ctx_dg%inv_jac(2,2,local_cell) +     &
                          coeff_dz*ctx_dg%inv_jac(3,2,local_cell)
              weight_dbasis(3,i_dof) =                                  &
                          coeff_dx*ctx_dg%inv_jac(1,3,local_cell) +     &
                          coeff_dy*ctx_dg%inv_jac(2,3,local_cell) +     &
                          coeff_dz*ctx_dg%inv_jac(3,3,local_cell)
          end do
        case(SRC_DIRAC,SRC_ANALYTIC)
        case default
          !! raise error
          ctx_err%msg   = "** ERROR: Unrecognized type of Dirac " //    &
                          "source [init_pt_src] **"
          ctx_err%ierr  = -300
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      call basis_clean(basis)
      !! ---------------------------------------------------------------

      !! possibility of scaling here -----------------------------------
      select case(tag_scale_rhs)
        case(tag_RHS_SCALE_ID)
          !! nothing to do
        case(tag_RHS_SCALE_INV2PI)
          weight = weight * 1.d0/(2.d0*pi)
          if(flag_dbasis) weight_dbasis = weight_dbasis* 1.d0/(2.d0*pi)
        case(tag_RHS_SCALE_R,tag_RHS_SCALE_R2)
          !! multiply by the norm of the source position.
          !! it cannot be zero. 
          if(norm2(coo3d) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            select case(tag_scale_rhs)
              case(tag_RHS_SCALE_R)
                weight = weight * norm2(coo3d)
                if(flag_dbasis) then
                  !! we have \nabla(r F) = r \nabla(F) + \nabla(r) . F
                  !! with r=(x² + y² + z²)^(1/2) 
                  !! => dr/dx = x (x² + y² + z²)^(-1/2)
                  !!          = x / r 
                  weight_dbasis(1,:) = weight_dbasis(1,:)*norm2(coo3d)  &
                                     + coo3d(1)/norm2(coo3d)*weight(:)
                  weight_dbasis(2,:) = weight_dbasis(2,:)*norm2(coo3d)  &
                                     + coo3d(2)/norm2(coo3d)*weight(:)
                  weight_dbasis(3,:) = weight_dbasis(3,:)*norm2(coo3d)  &
                                     + coo3d(3)/norm2(coo3d)*weight(:)
                end if
              case(tag_RHS_SCALE_R2)
                weight = weight * norm2(coo3d) * norm2(coo3d)
                if(flag_dbasis) then
                  !! we have \nabla(r F) = r \nabla(F) + \nabla(r) . F
                  !! with r=(x² + y² + z²)
                  !! => dr/dx = 2 x
                  weight_dbasis(1,:) = weight_dbasis(1,:)*(norm2(coo3d)**2)&
                                     + 2.d0*coo3d(1)*weight(:)
                  weight_dbasis(2,:) = weight_dbasis(2,:)*(norm2(coo3d)**2)&
                                     + 2.d0*coo3d(2)*weight(:)
                  weight_dbasis(3,:) = weight_dbasis(3,:)*(norm2(coo3d)**2)&
                                     + 2.d0*coo3d(3)*weight(:)
                end if
            end select
          end if
        case(tag_RHS_SCALE_X)
          !! multiply by the norm of the source position.
          !! it cannot be zero. 
          if(abs(coo3d(1)) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            weight = weight * coo3d(1)
            if(flag_dbasis) then
              !! we have \nabla(x F) = x \nabla(F) + \nabla(x) . F
              weight_dbasis(1,:) = weight_dbasis(1,:)*coo3d(1) + weight(:)
              weight_dbasis(2,:) = weight_dbasis(2,:)*coo3d(1)
              weight_dbasis(3,:) = weight_dbasis(3,:)*coo3d(1)
            end if
          end if

        case(tag_RHS_SCALE_XINV2PI)
          !! multiply by the norm of the source position.
          !! it cannot be zero. 
          if(abs(coo3d(1)) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            weight = weight * coo3d(1)
            if(flag_dbasis) then
              !! we have \nabla(x F) = x \nabla(F) + \nabla(x) . F
              weight_dbasis(1,:) = weight_dbasis(1,:)*coo3d(1)/(2.d0*pi)&
                                 + weight(:)/(2.d0*pi)
              weight_dbasis(2,:) = weight_dbasis(2,:)*coo3d(1)/(2.d0*pi)
              weight_dbasis(3,:) = weight_dbasis(3,:)*coo3d(1)/(2.d0*pi)
            end if
          end if

        case(tag_RHS_SCALE_X2)
          !! multiply by the norm of the source position.
          !! it cannot be zero. 
          if(abs(coo3d(1)**2) < tiny(1.0)) then
            !! raise a warning
            ctx_err%msg   = "** WARNING: the RHS IS NOT multiply by the"//&
                            " position because it is zero [init_pt_src] **"
            ctx_err%ierr  = -301
            ctx_err%critic=.false.
            call raise_error(ctx_err)
            ctx_err%ierr  = 0
            ctx_err%msg   = ""
          else
            weight = weight * coo3d(1) * coo3d(1)
            if(flag_dbasis) then
              !! we have \nabla(x F) = x \nabla(F) + \nabla(x) . F
              weight_dbasis(1,:) = weight_dbasis(1,:)*(coo3d(1)**2)     &
                                 + 2.d0*coo3d(1)*weight(:)
              weight_dbasis(2,:) = weight_dbasis(2,:)*(coo3d(1)**2)
              weight_dbasis(3,:) = weight_dbasis(3,:)*(coo3d(1)**2)
            end if
          end if
        case default
          ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                          "RHS [init_point_source_3D] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          !! non-shared error raised at above level
      end select 
      !! --------------------------------------------------------------- 

    end if

    return
  end subroutine dg_init_point_source_3d

end module m_dg_lagrange_source_dirac
