!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_source_gaussian.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize a Gaussian sources for HDG
!> discretization in the context of Lagrange Basis functions for
!> simplex meshes. 
!> The source concerns multiple cells, depending on the variance
!> of the Gaussian function, it corresponds on cell K dof j to
!>   
!>   \f$ \int_K G(x-x_0) \phi_j(x) d K .\f$
!
!------------------------------------------------------------------------------
module m_dg_lagrange_source_gaussian

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: IKIND_MESH,RKIND_POL
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_max,          only: allreduce_max
  use m_allreduce_sum,          only: allreduce_sum
  use m_array_types,            only: array_allocate
  use m_basis_functions,        only: t_basis, basis_construct,         &
                                      basis_derivative_construct, basis_clean
  use m_polynomial,             only: polynomial_eval
  !! matrix and mesh domain types
  use m_ctx_acquisition,        only: t_acquisition,SRC_GAUSSIAN
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_compute_gaussian,       only: compute_gaussian_1d,              &
                                      compute_gaussian_2d,              &
                                      compute_gaussian_3d
  !! to identify the cells that are concerned
  use m_mesh_simplex_find_cellgroup,  only: find_cellgroup_simplex
  !! to adjust the rhs
  use m_tag_scale_rhs,          only: tag_RHS_SCALE_ID,tag_RHS_SCALE_R, &
                                      tag_RHS_SCALE_R2,tag_RHS_SCALE_X, &
                                      tag_RHS_SCALE_X2,                 &
                                      tag_RHS_SCALE_INV2PI,tag_RHS_SCALE_XINV2PI
  !! -------------------------------------------------------------------
  implicit none

  private
  
  public :: dg_source_init_gaussian


  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for a delta-Dirac sources:
  !> - the cell(s) which are involved
  !> - the weight for the integral
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : mesh grid type
  !> @param[inout] ctx_dg         : type discretization DG
  !> @param[in]    i_src_first    : first source index in acquisition
  !> @param[in]    i_src_last     : last source index in acquisition
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_gaussian(ctx_paral,ctx_aq,ctx_grid,ctx_dg,  &
                                     i_src_first,i_src_last,            &
                                     tag_scale_rhs,ctx_err)
    implicit none

    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first
    integer                          ,intent(in)   :: i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! allocate the context and fill depending on the type of sources
    ctx_dg%src(:)%ncell_loc=0

    !! then it depends on the dimension.
    select case(ctx_grid%dim_domain)
      case(1) !! 1 dimension
        call dg_source_init_gaussian_1d(ctx_aq,ctx_grid,ctx_dg,         &
                                        i_src_first,i_src_last,         &
                                        tag_scale_rhs,ctx_err)
      case(2) !! 2 dimensions
        call dg_source_init_gaussian_2d(ctx_aq,ctx_grid,ctx_dg,         &
                                        i_src_first,i_src_last,         &
                                        tag_scale_rhs,ctx_err)
      case(3) !! 3 dimensions
        call dg_source_init_gaussian_3d(ctx_aq,ctx_grid,ctx_dg,         &
                                        i_src_first,i_src_last,         &
                                        tag_scale_rhs,ctx_err)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "//&
                        "[dg_source_init_dirac] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)
    
    return
  end subroutine dg_source_init_gaussian

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of Gaussian sources in 1 dimension.
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine dg_source_init_gaussian_1d(ctx_aq,ctx_grid,ctx_dg,         &
                                        i_src_first,i_src_last,         &
                                        tag_scale_rhs,ctx_err)

    implicit none

    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err

    !! local
    integer                              :: i_src, j, k, i_sol, ndof_face_gb
    integer                              :: ndof_vol, ndof_face(2) !! 2 edges
    integer(kind=IKIND_MESH)             :: n_cell_loc, n_cell_gb
    integer(kind=IKIND_MESH),allocatable :: cells_loc(:), cells_gb(:)
    integer(kind=IKIND_MESH),allocatable :: ksim_for_cell(:)
    real(kind=8)                         :: radius, x0(1)
    real(kind=8)                         :: sigma
    real(kind=RKIND_POL)    ,allocatable :: weight(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0


    !! -----------------------------------------------------------------
    !! we compute the maximal radius of the Gaussian:
    !! it depends on the variance of the Gaussian
    !! in 1D the Gaussian function is 
    !!         exp(-1.d0/2.d0 * (x-x0)² / sigma²)
    !! we want x such that the energy is less than 0.01% = 0.0001
    !!       alpha    = exp(-1./2. * (x-x0)² / sigma²)
    !!=> log(alpha)   =     -1./2. * (x-x0)² / sigma²
    !!=> -2log(alpha) = (x-x0)² / sigma²
    !!=> -2 sigma² log(alpha) = (x-x0)²
    !!=> sqrt(-2 sigma² log(alpha)) = (x-x0)
    !!=> sqrt(-2 sigma² log(alpha)) = radius
    !! -----------------------------------------------------------------
    !! with log(0.01%) = log(0.0001) =-9.210340371976182
    !! we approximate to 10.0
    sigma   = ctx_aq%src_info%gaussian_variance(1)
    radius  = sqrt(20.d0)*sigma ! ~ 4.47*sigma
    !! -----------------------------------------------------------------
    
    do i_src=i_src_first, i_src_last
      if(ctx_err%ierr .ne. 0) cycle
 
      j = i_src-i_src_first+1

      ! ----------------------------------------------------------------
      ! for Gaussian sources: the source is spread among multiple
      ! cells, and each dof will have a value which is the integral
      ! of the Gaussian with test function.
      ! ----------------------------------------------------------------
      ! for each of the source position, we 
      ! detect which cells are concerned
      ! ----------------------------------------------------------------
      allocate(cells_gb     (ctx_aq%sources(i_src)%nsim*ctx_grid%n_cell_loc)) ! maximal size
      allocate(ksim_for_cell(ctx_aq%sources(i_src)%nsim*ctx_grid%n_cell_loc)) ! maximal size
      n_cell_gb      = 0
      cells_gb       = 0
      ksim_for_cell  = 0
      do k=1, ctx_aq%sources(i_src)%nsim
        if(ctx_err%ierr .ne. 0) cycle
        
        x0(1) = dble(ctx_aq%sources(i_src)%x(k))
        n_cell_loc = 0
        call find_cellgroup_simplex(x0,radius,n_cell_loc,cells_loc,     &
                                    ctx_grid%x_node,ctx_grid%cell_node, &
                                    ctx_grid%n_cell_loc,                &
                                    ctx_grid%dim_domain,ctx_err)
        if(ctx_err%ierr .ne. 0) cycle

        if(n_cell_loc > 0) then !!  .and. allocated(cells_loc)) then
          cells_gb     (n_cell_gb+1:n_cell_gb+n_cell_loc) = cells_loc(:)
          ksim_for_cell(n_cell_gb+1:n_cell_gb+n_cell_loc) = k
          n_cell_gb         = n_cell_gb + n_cell_loc
          deallocate(cells_loc)
        end if

      end do 
      if(ctx_err%ierr .ne. 0) cycle
      
      !! ---------------------------------------------------------------
      !! we have all cells concerned: there are n_cell_gb and they are
      !! listed in array cells_gb. Now we can allocate the array.
      allocate(ctx_dg%src(j)%flag_on_bdry (n_cell_gb))
      allocate(ctx_dg%src(j)%icell_loc    (n_cell_gb))
      allocate(ctx_dg%src(j)%ndof_vol     (n_cell_gb))
      allocate(ctx_dg%src(j)%ndof_face    (n_cell_gb))
      allocate(ctx_dg%src(j)%weight       (ctx_dg%dim_sol,n_cell_gb))
      allocate(ctx_dg%src(j)%weight_dbasis(ctx_dg%dim_sol,n_cell_gb))
      allocate(ctx_dg%src(j)%hdg_S        (n_cell_gb))
      !! only used in case of boundary source
      !! allocate(ctx_dg%src(j)%iface_index (n_cell_gb))
      
      ctx_dg%src(j)%flag_on_bdry  = .false. !! none are on the boundary
      ctx_dg%src(j)%icell_loc(:)  = cells_gb(1:n_cell_gb)
      ctx_dg%src(j)%ncell_loc     = n_cell_gb
      !! ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      !! ---------------------------------------------------------------
      deallocate(cells_gb)

      !! initialize the weighting arrays for all cells
      do k=1,n_cell_gb
        if(ctx_err%ierr .ne. 0) cycle

        !! We need the x0 corresponding to this source
        x0(1) = dble(ctx_aq%sources(i_src)%x(ksim_for_cell(k)))

        call src_gaussian_weight_dof_1d(ctx_grid,ctx_dg,x0,sigma,       &
                                        tag_scale_rhs,                  &
                                        ctx_dg%src(j)%icell_loc(k),     &
                                        ndof_vol,ndof_face,             &
                                        weight,ctx_err)
        if(ctx_err%ierr .ne. 0) cycle
        
        !! Save the arrays ---------------------------------------------
        do i_sol=1,ctx_dg%dim_sol
          call array_allocate(ctx_dg%src(j)%weight(i_sol,k),ndof_vol,ctx_err)
          ctx_dg%src(j)%weight(i_sol,k)%array(:) = weight(:)
        end do
        ctx_dg%src(j)%ndof_vol(k) = ndof_vol
        ndof_face_gb = sum(ndof_face)
        !! it could be different if the source was on a boundary.
        ctx_dg%src(j)%ndof_face(k) = ndof_face_gb
        deallocate(weight) 
        
      end do
      if(ctx_err%ierr .ne. 0) cycle

      !! clean for the next rhs
      deallocate(ksim_for_cell)

    end do !! end loop over the rhs
    !! -----------------------------------------------------------------
           
    return

  end subroutine dg_source_init_gaussian_1d



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We computet the volume integral of the Gaussian function
  !> with each of the basis function of the given cell. That 
  !> is, we compute, for all j, 
  !>     
  !>     \f$ \int_K g(x).\phi_j(x) \sim \sum_k g(x_k).\phi_j(x_k). \f$
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine src_gaussian_weight_dof_1d(ctx_grid,ctx_dg,x0,sigma,       &
                                        tag_scale_rhs,local_cell,       &
                                        ndof_vol,ndof_face,weight,ctx_err)

    implicit none

    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: x0(1)
    real(kind=8)                     ,intent(in)   :: sigma
    integer                          ,intent(in)   :: tag_scale_rhs
    integer(kind=IKIND_MESH)         ,intent(in)   :: local_cell
    integer                          ,intent(out)  :: ndof_vol
    integer                          ,intent(out)  :: ndof_face(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight   (:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)                :: basis
    integer                      :: order,i_order,i_dof,f,k
    integer(kind=IKIND_MESH)     :: face
    real   (kind=8), allocatable :: scaling(:)
    real   (kind=RKIND_POL)      :: fbasis
    real   (kind=8)              :: fgauss,int_vol
    real   (kind=8)              :: xpt_loc(1),xpt_gb(1)
    real   (kind=8)              :: node_coo(1,2)
    real   (kind=8), parameter   :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    ndof_face    = 0
    ndof_vol     = 0
    node_coo(:,:)= dble(ctx_grid%x_node(:,ctx_grid%cell_node(:,local_cell)))
    
    !! -----------------------------------------------------------------
    !! for the face dof, we sum all of face
    ndof_face=0
    do f=1,ctx_grid%n_neigh_per_cell
      face     = ctx_grid%cell_face   (f,local_cell)
      order    = ctx_grid%order_face  (face)
      i_order  = ctx_dg%order_to_index(order)
      ndof_face(f)= ctx_dg%n_dof_face_per_order(i_order)
    enddo
    order    = ctx_grid%order        (local_cell)
    i_order  = ctx_dg%order_to_index (order)
    ndof_vol = ctx_dg%n_dof_per_order(i_order)

    !! the weight array ------------------------------------------------
    allocate(weight(ndof_vol))
    weight=0.0
    
    !! -----------------------------------------------------------------
    !! possibility of a scaling in the RHS.
    allocate(scaling(ctx_dg%quadGL_npts))
    scaling = 1.d0
    select case(tag_scale_rhs)
      
      case(tag_RHS_SCALE_ID)        !! nothing to do 
      case(tag_RHS_SCALE_INV2PI)    !! global scaling
        scaling = 1.d0/(2.d0*pi)
      case(tag_RHS_SCALE_R)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)
        end do
      case(tag_RHS_SCALE_R2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)**2
        end do
      case(tag_RHS_SCALE_X2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)*xpt_gb(1)
        end do
      case(tag_RHS_SCALE_XINV2PI)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)/(2.d0*pi)
        end do
      case(tag_RHS_SCALE_X)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)
        end do
      case default
        ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                        "RHS [gaussian_weight_src_2d] **"
        ctx_err%ierr  = -305
        ctx_err%critic=.true.
        return !! non-shared error here
    end select
    !! make sure they are not all zero
    if(minval(abs(scaling)) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: scaling of the RHS is zero for all "//&
                      "positions [gaussian_weight_src_2d] **"
      ctx_err%ierr  = -305
      ctx_err%critic=.true.
      return !! non-shared error here    
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! compute basis function
    call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
    
    do i_dof=1,ndof_vol
      
      int_vol = 0.d0
      !! loop over all quadrature point to update the volume integral.
      do k=1, ctx_dg%quadGL_npts
        xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
        xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                   + dble(node_coo(:,1))
        
        !! evaluate the basis function at this position.
        call polynomial_eval(basis%pol(i_dof),ctx_dg%quadGL_pts(1,k),   &
                             fbasis,ctx_err)
        
        !! compute the value of the gaussian at this position.
        call compute_gaussian_1d(xpt_gb(1),x0(1),sigma,fgauss)
        
        !! update sum for the quadrature approximation of the integral
        int_vol = int_vol + dble(ctx_dg%quadGL_weight(k)*scaling(k)*fbasis*fgauss)

      end do

      !! update weight array 
      weight(i_dof) = int_vol*ctx_dg%det_jac(local_cell)

    end do
    call basis_clean(basis)
    deallocate(scaling)
    !! -----------------------------------------------------------------

    return

  end subroutine src_gaussian_weight_dof_1d

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of Gaussian sources in 2 dimensions.
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine dg_source_init_gaussian_2d(ctx_aq,ctx_grid,ctx_dg,         &
                                        i_src_first,i_src_last,         &
                                        tag_scale_rhs,ctx_err)

    implicit none

    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err

    !! local
    integer                              :: i_src, j, k, i_sol, ndof_face_gb
    integer                              :: ndof_vol, ndof_face(3) !! 3 faces triangles
    integer(kind=IKIND_MESH)             :: n_cell_loc, n_cell_gb
    integer(kind=IKIND_MESH),allocatable :: cells_loc(:), cells_gb(:)
    integer(kind=IKIND_MESH),allocatable :: ksim_for_cell(:)
    real(kind=8)                         :: sigma_max, radius, x0(2)
    real(kind=8)                         :: sigma(2)
    real(kind=RKIND_POL)    ,allocatable :: weight(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! we compute the maximal radius of the Gaussian:
    !! it depends on the variance of the Gaussian
    !! in 1D the Gaussian function is 
    !!         exp(-1.d0/2.d0 * (x-x0)² / sigma²)
    !! we want x such that the energy is less than 0.01% = 0.0001
    !!       alpha    = exp(-1./2. * (x-x0)² / sigma²)
    !!=> log(alpha)   =     -1./2. * (x-x0)² / sigma²
    !!=> -2log(alpha) = (x-x0)² / sigma²
    !!=> -2 sigma² log(alpha) = (x-x0)²
    !!=> sqrt(-2 sigma² log(alpha)) = (x-x0)
    !!=> sqrt(-2 sigma² log(alpha)) = radius
    !! -----------------------------------------------------------------
    !! with log(0.01%) = log(0.0001) =-9.210340371976182
    !! we approximate to 10.0
    sigma(1)  = ctx_aq%src_info%gaussian_variance(1)
    sigma(2)  = ctx_aq%src_info%gaussian_variance(2)
    sigma_max = maxval(sigma)
    radius    = sqrt(20.d0)*sigma_max ! ~ 4.47*sigma
    !! -----------------------------------------------------------------
    
    do i_src=i_src_first, i_src_last
      if(ctx_err%ierr .ne. 0) cycle
 
      j = i_src-i_src_first+1

      ! ----------------------------------------------------------------
      ! for Gaussian sources: the source is spread among multiple
      ! cells, and each dof will have a value which is the integral
      ! of the Gaussian with test function.
      ! ----------------------------------------------------------------
      ! for each of the source position, we 
      ! detect which cells are concerned
      ! ----------------------------------------------------------------
      allocate(cells_gb     (ctx_aq%sources(i_src)%nsim*ctx_grid%n_cell_loc)) ! maximal size
      allocate(ksim_for_cell(ctx_aq%sources(i_src)%nsim*ctx_grid%n_cell_loc)) ! maximal size
      n_cell_gb      = 0
      cells_gb       = 0
      ksim_for_cell  = 0
      do k=1, ctx_aq%sources(i_src)%nsim
        if(ctx_err%ierr .ne. 0) cycle
        
        x0(1) = dble(ctx_aq%sources(i_src)%x(k))
        x0(2) = dble(ctx_aq%sources(i_src)%z(k))
        n_cell_loc = 0
        call find_cellgroup_simplex(x0,radius,n_cell_loc,cells_loc,     &
                                    ctx_grid%x_node,ctx_grid%cell_node, &
                                    ctx_grid%n_cell_loc,                &
                                    ctx_grid%dim_domain,ctx_err)
        if(ctx_err%ierr .ne. 0) cycle

        if(n_cell_loc > 0) then !!  .and. allocated(cells_loc)) then
          cells_gb     (n_cell_gb+1:n_cell_gb+n_cell_loc) = cells_loc(:)
          ksim_for_cell(n_cell_gb+1:n_cell_gb+n_cell_loc) = k
          n_cell_gb         = n_cell_gb + n_cell_loc
          deallocate(cells_loc)
        end if

      end do 
      if(ctx_err%ierr .ne. 0) cycle
      
      !! ---------------------------------------------------------------
      !! we have all cells concerned: there are n_cell_gb and they are
      !! listed in array cells_gb. Now we can allocate the array.
      allocate(ctx_dg%src(j)%flag_on_bdry (n_cell_gb))
      allocate(ctx_dg%src(j)%icell_loc    (n_cell_gb))
      allocate(ctx_dg%src(j)%ndof_vol     (n_cell_gb))
      allocate(ctx_dg%src(j)%ndof_face    (n_cell_gb))
      allocate(ctx_dg%src(j)%weight       (ctx_dg%dim_sol,n_cell_gb))
      allocate(ctx_dg%src(j)%weight_dbasis(ctx_dg%dim_sol,n_cell_gb))
      allocate(ctx_dg%src(j)%hdg_S        (n_cell_gb))
      !! only used in case of boundary source
      !! allocate(ctx_dg%src(j)%iface_index (n_cell_gb))
      
      ctx_dg%src(j)%flag_on_bdry  = .false. !! none are on the boundary
      ctx_dg%src(j)%icell_loc(:)  = cells_gb(1:n_cell_gb)
      ctx_dg%src(j)%ncell_loc     = n_cell_gb
      !! ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      !! ---------------------------------------------------------------
      deallocate(cells_gb)

      !! initialize the weighting arrays for all cells
      do k=1,n_cell_gb
        if(ctx_err%ierr .ne. 0) cycle

        !! We need the x0 corresponding to this source
        x0(1) = dble(ctx_aq%sources(i_src)%x(ksim_for_cell(k)))
        x0(2) = dble(ctx_aq%sources(i_src)%z(ksim_for_cell(k)))

        call src_gaussian_weight_dof_2d(ctx_grid,ctx_dg,x0,sigma,       &
                                        tag_scale_rhs,                  &
                                        ctx_dg%src(j)%icell_loc(k),     &
                                        ndof_vol,ndof_face,             &
                                        weight,ctx_err)
        if(ctx_err%ierr .ne. 0) cycle
        
        !! Save the arrays ---------------------------------------------
        do i_sol=1,ctx_dg%dim_sol
          call array_allocate(ctx_dg%src(j)%weight(i_sol,k),ndof_vol,ctx_err)
          ctx_dg%src(j)%weight(i_sol,k)%array(:) = weight(:)
        end do
        ctx_dg%src(j)%ndof_vol(k) = ndof_vol
        ndof_face_gb = sum(ndof_face)
        !! it could be different if the source was on a boundary.
        ctx_dg%src(j)%ndof_face(k) = ndof_face_gb
        deallocate(weight) 
        
      end do
      if(ctx_err%ierr .ne. 0) cycle

      !! clean for the next rhs
      deallocate(ksim_for_cell)

    end do !! end loop over the rhs
    !! -----------------------------------------------------------------
           
    return

  end subroutine dg_source_init_gaussian_2d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We computet the volume integral of the Gaussian function
  !> with each of the basis function of the given cell. That 
  !> is, we compute, for all j, 
  !>     
  !>     \f$ \int_K g(x).\phi_j(x) \sim \sum_k g(x_k).\phi_j(x_k). \f$
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine src_gaussian_weight_dof_2d(ctx_grid,ctx_dg,x0,sigma,       &
                                        tag_scale_rhs,local_cell,       &
                                        ndof_vol,ndof_face,weight,ctx_err)

    implicit none

    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: x0   (2)
    real(kind=8)                     ,intent(in)   :: sigma(2)
    integer                          ,intent(in)   :: tag_scale_rhs
    integer(kind=IKIND_MESH)         ,intent(in)   :: local_cell
    integer                          ,intent(out)  :: ndof_vol
    integer                          ,intent(out)  :: ndof_face(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight   (:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)                :: basis
    integer                      :: order,i_order,i_dof,f,k
    integer(kind=IKIND_MESH)     :: face
    real   (kind=8), allocatable :: scaling(:)
    real   (kind=RKIND_POL)      :: fbasis
    real   (kind=8)              :: fgauss,int_vol
    real   (kind=8)              :: xpt_loc(2),xpt_gb(2)
    real   (kind=8)              :: node_coo(2,3)
    real   (kind=8), parameter   :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    ndof_face  = 0
    ndof_vol   = 0
    node_coo(:,:)= dble(ctx_grid%x_node(:,ctx_grid%cell_node(:,local_cell)))
    
    !! -----------------------------------------------------------------
    !! for the face dof, we sum all of face
    ndof_face=0
    do f=1,ctx_grid%n_neigh_per_cell
      face     = ctx_grid%cell_face   (f,local_cell)
      order    = ctx_grid%order_face  (face)
      i_order  = ctx_dg%order_to_index(order)
      ndof_face(f)= ctx_dg%n_dof_face_per_order(i_order)
    enddo
    order    = ctx_grid%order        (local_cell)
    i_order  = ctx_dg%order_to_index (order)
    ndof_vol = ctx_dg%n_dof_per_order(i_order)

    !! the weight array ------------------------------------------------
    allocate(weight(ndof_vol))
    weight=0.0
    
    !! -----------------------------------------------------------------
    !! possibility of a scaling in the RHS.
    allocate(scaling(ctx_dg%quadGL_npts))
    scaling = 1.d0
    select case(tag_scale_rhs)
      
      case(tag_RHS_SCALE_ID)        !! nothing to do 
      case(tag_RHS_SCALE_INV2PI)    !! global scaling
        scaling = 1.d0/(2.d0*pi)
      case(tag_RHS_SCALE_R)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)
        end do
      case(tag_RHS_SCALE_R2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)**2
        end do
      case(tag_RHS_SCALE_X2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)*xpt_gb(1)
        end do
      case(tag_RHS_SCALE_XINV2PI)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)/(2.d0*pi)
        end do
      case(tag_RHS_SCALE_X)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)
        end do
      case default
        ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                        "RHS [gaussian_weight_src_2d] **"
        ctx_err%ierr  = -305
        ctx_err%critic=.true.
        return !! non-shared error here
    end select
    !! make sure they are not all zero
    if(minval(abs(scaling)) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: scaling of the RHS is zero for all "//&
                      "positions [gaussian_weight_src_2d] **"
      ctx_err%ierr  = -305
      ctx_err%critic=.true.
      return !! non-shared error here    
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! compute basis function
    call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
    
    do i_dof=1,ndof_vol
      
      int_vol = 0.d0
      !! loop over all quadrature point to update the volume integral.
      do k=1, ctx_dg%quadGL_npts
        xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
        xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                   + dble(node_coo(:,1))
        
        !! evaluate the basis function at this position.
        call polynomial_eval(basis%pol(i_dof),ctx_dg%quadGL_pts(1,k),   &
                             ctx_dg%quadGL_pts(2,k),fbasis,ctx_err)
        
        !! compute the value of the gaussian at this position.
        call compute_gaussian_2d(xpt_gb,x0,sigma,fgauss)
        
        !! update sum for the quadrature approximation of the integral
        int_vol = int_vol + dble(ctx_dg%quadGL_weight(k)*scaling(k)*fbasis*fgauss)

      end do

      !! update weight array 
      weight(i_dof) = int_vol*ctx_dg%det_jac(local_cell)

    end do
    call basis_clean(basis)
    deallocate(scaling)
    !! -----------------------------------------------------------------

    return

  end subroutine src_gaussian_weight_dof_2d

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of Gaussian sources in 3 dimensions.
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine dg_source_init_gaussian_3d(ctx_aq,ctx_grid,ctx_dg,         &
                                        i_src_first,i_src_last,         &
                                        tag_scale_rhs,ctx_err)

    implicit none

    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    integer                          ,intent(in)   :: i_src_first,i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    type(t_error)                    ,intent(inout):: ctx_err

    !! local
    integer                              :: i_src, j, k, i_sol, ndof_face_gb
    integer                              :: ndof_vol, ndof_face(4) !! 4 faces tetrahedron
    integer(kind=IKIND_MESH)             :: n_cell_loc, n_cell_gb
    integer(kind=IKIND_MESH),allocatable :: cells_loc(:), cells_gb(:)
    integer(kind=IKIND_MESH),allocatable :: ksim_for_cell(:)
    real(kind=8)                         :: sigma_max, radius, x0(3)
    real(kind=8)                         :: sigma(3)
    real(kind=RKIND_POL)    ,allocatable :: weight(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! we compute the maximal radius of the Gaussian:
    !! it depends on the variance of the Gaussian
    !! in 1D the Gaussian function is 
    !!         exp(-1.d0/2.d0 * (x-x0)² / sigma²)
    !! we want x such that the energy is less than 0.01% = 0.0001
    !!       alpha    = exp(-1./2. * (x-x0)² / sigma²)
    !!=> log(alpha)   =     -1./2. * (x-x0)² / sigma²
    !!=> -2log(alpha) = (x-x0)² / sigma²
    !!=> -2 sigma² log(alpha) = (x-x0)²
    !!=> sqrt(-2 sigma² log(alpha)) = (x-x0)
    !!=> sqrt(-2 sigma² log(alpha)) = radius
    !! -----------------------------------------------------------------
    !! with log(0.01%) = log(0.0001) =-9.210340371976182
    !! we approximate to 10.0
    sigma(:)  = ctx_aq%src_info%gaussian_variance(:)
    sigma_max = maxval(sigma)
    radius    = sqrt(20.d0)*sigma_max ! ~ 4.47*sigma
    !! -----------------------------------------------------------------
    
    do i_src=i_src_first, i_src_last
      if(ctx_err%ierr .ne. 0) cycle
 
      j = i_src-i_src_first+1

      ! ----------------------------------------------------------------
      ! for Gaussian sources: the source is spread among multiple
      ! cells, and each dof will have a value which is the integral
      ! of the Gaussian with test function.
      ! ----------------------------------------------------------------
      ! for each of the source position, we 
      ! detect which cells are concerned
      ! ----------------------------------------------------------------
      allocate(cells_gb     (ctx_aq%sources(i_src)%nsim*ctx_grid%n_cell_loc)) ! maximal size
      allocate(ksim_for_cell(ctx_aq%sources(i_src)%nsim*ctx_grid%n_cell_loc)) ! maximal size
      n_cell_gb      = 0
      cells_gb       = 0
      ksim_for_cell  = 0
      do k=1, ctx_aq%sources(i_src)%nsim
        if(ctx_err%ierr .ne. 0) cycle
        
        x0(1) = dble(ctx_aq%sources(i_src)%x(k))
        x0(2) = dble(ctx_aq%sources(i_src)%y(k))
        x0(3) = dble(ctx_aq%sources(i_src)%z(k))
        n_cell_loc = 0
        call find_cellgroup_simplex(x0,radius,n_cell_loc,cells_loc,     &
                                    ctx_grid%x_node,ctx_grid%cell_node, &
                                    ctx_grid%n_cell_loc,                &
                                    ctx_grid%dim_domain,ctx_err)
        if(ctx_err%ierr .ne. 0) cycle

        if(n_cell_loc > 0) then !!  .and. allocated(cells_loc)) then
          cells_gb     (n_cell_gb+1:n_cell_gb+n_cell_loc) = cells_loc(:)
          ksim_for_cell(n_cell_gb+1:n_cell_gb+n_cell_loc) = k
          n_cell_gb         = n_cell_gb + n_cell_loc
          deallocate(cells_loc)
        end if

      end do 
      if(ctx_err%ierr .ne. 0) cycle
      
      !! ---------------------------------------------------------------
      !! we have all cells concerned: there are n_cell_gb and they are
      !! listed in array cells_gb. Now we can allocate the array.
      allocate(ctx_dg%src(j)%flag_on_bdry (n_cell_gb))
      allocate(ctx_dg%src(j)%icell_loc    (n_cell_gb))
      allocate(ctx_dg%src(j)%ndof_vol     (n_cell_gb))
      allocate(ctx_dg%src(j)%ndof_face    (n_cell_gb))
      allocate(ctx_dg%src(j)%weight       (ctx_dg%dim_sol,n_cell_gb))
      allocate(ctx_dg%src(j)%weight_dbasis(ctx_dg%dim_sol,n_cell_gb))
      allocate(ctx_dg%src(j)%hdg_S        (n_cell_gb))
      !! only used in case of boundary source
      !! allocate(ctx_dg%src(j)%iface_index (n_cell_gb))
      
      ctx_dg%src(j)%flag_on_bdry  = .false. !! none are on the boundary
      ctx_dg%src(j)%icell_loc(:)  = cells_gb(1:n_cell_gb)
      ctx_dg%src(j)%ncell_loc     = n_cell_gb
      !! ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      !! ---------------------------------------------------------------
      deallocate(cells_gb)

      !! initialize the weighting arrays for all cells
      do k=1,n_cell_gb
        if(ctx_err%ierr .ne. 0) cycle

        !! We need the x0 corresponding to this source
        x0(1) = dble(ctx_aq%sources(i_src)%x(ksim_for_cell(k)))
        x0(2) = dble(ctx_aq%sources(i_src)%y(ksim_for_cell(k)))
        x0(3) = dble(ctx_aq%sources(i_src)%z(ksim_for_cell(k)))

        call src_gaussian_weight_dof_3d(ctx_grid,ctx_dg,x0,sigma,       &
                                        tag_scale_rhs,                  &
                                        ctx_dg%src(j)%icell_loc(k),     &
                                        ndof_vol,ndof_face,             &
                                        weight,ctx_err)
        if(ctx_err%ierr .ne. 0) cycle
        
        !! Save the arrays ---------------------------------------------
        do i_sol=1,ctx_dg%dim_sol
          call array_allocate(ctx_dg%src(j)%weight(i_sol,k),ndof_vol,ctx_err)
          ctx_dg%src(j)%weight(i_sol,k)%array(:) = weight(:)
        end do
        ctx_dg%src(j)%ndof_vol(k) = ndof_vol
        ndof_face_gb = sum(ndof_face)
        !! it could be different if the source was on a boundary.
        ctx_dg%src(j)%ndof_face(k) = ndof_face_gb
        deallocate(weight) 
        
      end do
      if(ctx_err%ierr .ne. 0) cycle

      !! clean for the next rhs
      deallocate(ksim_for_cell)

    end do !! end loop over the rhs
    !! -----------------------------------------------------------------
           
    return

  end subroutine dg_source_init_gaussian_3d


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We computet the volume integral of the Gaussian function
  !> with each of the basis function of the given cell. That 
  !> is, we compute, for all j, 
  !>     
  !>     \f$ \int_K g(x).\phi_j(x) \sim \sum_k g(x_k).\phi_j(x_k). \f$
  !>
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine src_gaussian_weight_dof_3d(ctx_grid,ctx_dg,x0,sigma,       &
                                        tag_scale_rhs,local_cell,       &
                                        ndof_vol,ndof_face,weight,ctx_err)

    implicit none

    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real(kind=8)                     ,intent(in)   :: x0   (3)
    real(kind=8)                     ,intent(in)   :: sigma(3)
    integer                          ,intent(in)   :: tag_scale_rhs
    integer(kind=IKIND_MESH)         ,intent(in)   :: local_cell
    integer                          ,intent(out)  :: ndof_vol
    integer                          ,intent(out)  :: ndof_face(:)
    real(kind=RKIND_POL),allocatable ,intent(inout):: weight   (:)
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_basis)                :: basis
    integer                      :: order,i_order,i_dof,f,k
    integer(kind=IKIND_MESH)     :: face
    real   (kind=8), allocatable :: scaling(:)
    real   (kind=RKIND_POL)      :: fbasis
    real   (kind=8)              :: fgauss,int_vol
    real   (kind=8)              :: xpt_loc(3),xpt_gb(3)
    real   (kind=8)              :: node_coo(3,4)
    real   (kind=8), parameter   :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    ndof_face    = 0
    ndof_vol     = 0
    node_coo(:,:)= dble(ctx_grid%x_node(:,ctx_grid%cell_node(:,local_cell)))
    
    !! -----------------------------------------------------------------
    !! for the face dof, we sum all of face
    ndof_face=0
    do f=1,ctx_grid%n_neigh_per_cell
      face     = ctx_grid%cell_face   (f,local_cell)
      order    = ctx_grid%order_face  (face)
      i_order  = ctx_dg%order_to_index(order)
      ndof_face(f)= ctx_dg%n_dof_face_per_order(i_order)
    end do
    order    = ctx_grid%order        (local_cell)
    i_order  = ctx_dg%order_to_index (order)
    ndof_vol = ctx_dg%n_dof_per_order(i_order)

    !! the weight array ------------------------------------------------
    allocate(weight(ndof_vol))
    weight=0.0
    
    !! -----------------------------------------------------------------
    !! possibility of a scaling in the RHS.
    allocate(scaling(ctx_dg%quadGL_npts))
    scaling = 1.d0
    select case(tag_scale_rhs)
      
      case(tag_RHS_SCALE_ID)        !! nothing to do 
      case(tag_RHS_SCALE_INV2PI)    !! global scaling
        scaling = 1.d0/(2.d0*pi)
      case(tag_RHS_SCALE_R)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)
        end do
      case(tag_RHS_SCALE_R2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)**2
        end do
      case(tag_RHS_SCALE_X2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)*xpt_gb(1)
        end do
      case(tag_RHS_SCALE_XINV2PI)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)/(2.d0*pi)
        end do
      case(tag_RHS_SCALE_X)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)
        end do
      case default
        ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                        "RHS [gaussian_weight_src_2d] **"
        ctx_err%ierr  = -305
        ctx_err%critic=.true.
        return !! non-shared error here
    end select
    !! make sure they are not all zero
    if(minval(abs(scaling)) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: scaling of the RHS is zero for all "//&
                      "positions [gaussian_weight_src_2d] **"
      ctx_err%ierr  = -305
      ctx_err%critic=.true.
      return !! non-shared error here    
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! compute basis function
    call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
    
    do i_dof=1,ndof_vol
      
      int_vol = 0.d0
      !! loop over all quadrature point to update the volume integral.
      do k=1, ctx_dg%quadGL_npts
        xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
        xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                   + dble(node_coo(:,1))
        
        !! evaluate the basis function at this position.
        call polynomial_eval(basis%pol(i_dof),ctx_dg%quadGL_pts(1,k),   &
                             ctx_dg%quadGL_pts(2,k),                    &
                             ctx_dg%quadGL_pts(3,k),fbasis,ctx_err)
        
        !! compute the value of the gaussian at this position.
        call compute_gaussian_3d(xpt_gb,x0,sigma,fgauss)
        
        !! update sum for the quadrature approximation of the integral
        int_vol = int_vol + dble(ctx_dg%quadGL_weight(k)*scaling(k)*fbasis*fgauss)

      end do

      !! update weight array 
      weight(i_dof) = int_vol*ctx_dg%det_jac(local_cell)

    end do
    call basis_clean(basis)
    deallocate(scaling)
    !! -----------------------------------------------------------------

    return

  end subroutine src_gaussian_weight_dof_3d

end module m_dg_lagrange_source_gaussian
