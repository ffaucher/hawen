!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_source_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize a planewave source using HDG
!> discretization in the context of Lagrange Basis functions. 
!> The source is located on the faces that have the boundary tags,
!> as a Dirichlet condition.
!
!
!------------------------------------------------------------------------------
module m_dg_lagrange_source_planewave

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_define_precision,       only: IKIND_MESH,RKIND_POL
  use m_array_types,            only: array_allocate
  !> matrix and mesh domain types
  use m_ctx_acquisition,        only: t_acquisition
  use m_tag_bc,                 only: tag_planewave,                    &
                                      tag_planewave_isoP,               &
                                      tag_planewave_isoS,               &
                                      tag_planewave_vtiYSH,             &
                                      tag_planewave_vtiYSV,             &
                                      tag_planewave_vtiYqP,             &
                                      tag_analytic_orthorhombic,        &
                                      tag_analytic_elasticiso,          &
                                      tag_planewave_scatt
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization,                 &
                                      dg_source_copy_context,           &
                                      t_dg_source,dg_source_clean_context
  use m_dg_lagrange_dof_coordinates,  only: discretization_dof_coo_ref_elem
  use m_lapack_solve,           only: lapack_solve_system
  use m_mesh_convert_face_position,                                     &
                                only: mesh_convert_face_position_2D,    &
                                      mesh_convert_face_position_3D
  use m_compute_elasticity,     only: analytic_function_elastic_orthorhombic, &
                                      analytic_function_elastic_isotropic
  use m_tag_scale_rhs,          only: tag_RHS_SCALE_ID
  use m_dg_lagrange_source_dirac, & 
                                only: dg_source_init_dirac
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: dg_source_init_planewave
  private:: dg_source_init_planewave_2d
  private:: dg_source_init_planewave_3d
  public :: dg_source_init_analytic
  private:: dg_source_init_analytic_elastic
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of sources:
  !> Dirichlet conditions with plane-waves vales
  !
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_mesh       : mesh grid type
  !> @param[inout] ctx_dg         : type discretization DG
  !> @param[in]    freq           : plane wave frequency
  !> @param[in]    i_src_first    : first source index in acquisition
  !> @param[in]    i_src_last     : last source index in acquisition
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_planewave(ctx_aq,ctx_mesh,ctx_dg,freq,      &
                                      i_src_first,i_src_last,ctx_err)
    implicit none

    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_mesh
    type(t_discretization)           ,intent(inout):: ctx_dg
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: i_src_first
    integer                          ,intent(in)   :: i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! allocate the context and fill depending on the type of sources
    ctx_dg%src(:)%ncell_loc=0

    !! then it depends on the dimension.
    select case(ctx_mesh%dim_domain)
      case(2) !! 2 dimensions
        call dg_source_init_planewave_2d(ctx_aq,ctx_mesh,ctx_dg,freq,   &
                                         i_src_first,i_src_last,ctx_err)

      case(3) !! 3 dimensions
        call dg_source_init_planewave_3d(ctx_aq,ctx_mesh,ctx_dg,freq,   &
                                         i_src_first,i_src_last,ctx_err)
  
      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "//&
                        "[dg_source_init_planewave] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine dg_source_init_planewave

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of sources:
  !> Dirichlet conditions with plane-waves vales
  !
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_mesh       : mesh grid type
  !> @param[inout] ctx_dg         : type discretization DG
  !> @param[in]    freq           : plane wave frequency
  !> @param[in]    i_src_first    : first source index in acquisition
  !> @param[in]    i_src_last     : last source index in acquisition
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_analytic(ctx_paral,ctx_aq,ctx_mesh,         &
                                     ctx_dg,freq,i_src_first,           &
                                     i_src_last,ctx_err)
    implicit none

    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_mesh
    type(t_discretization)           ,intent(inout):: ctx_dg
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: i_src_first
    integer                          ,intent(in)   :: i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! allocate the context and fill depending on the type of sources
    ctx_dg%src(:)%ncell_loc=0

    !! then it depends on the dimension.
    select case(ctx_mesh%dim_domain)
      case(3) !! 3 dimensions
        call dg_source_init_analytic_elastic(ctx_paral,ctx_aq,ctx_mesh, &
                                             ctx_dg,freq,i_src_first,   &
                                             i_src_last,ctx_err)
  
      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "//&
                        "[dg_source_init_planewave] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine dg_source_init_analytic

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the sources for planewave in 2 dimensions
  !>  - the planewave is defined by its direction
  !>  - it is applied on the specified boundary
  !>  - it corresponds to a dirichlet condition imposed: that is, 
  !>    we only have BOUNDARY source in this case. 
  !> In addition, we allow for several directions at the same time, 
  !> using a sum of planewaves.
  !>
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    freq           : plane wave frequency
  !> @param[in]    i_src_first    : fisrt source index
  !> @param[in]    i_src_last     : last source index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_planewave_2d(ctx_aq,ctx_mesh,ctx_dg,freq,   &
                                         i_src_first,i_src_last,ctx_err)
    implicit none
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_mesh
    type(t_discretization)           ,intent(inout):: ctx_dg
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: i_src_first,i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)            :: i_cell,nface_pw,neigh,face
    integer(kind=IKIND_MESH)            :: counter_cell
    integer                             :: f,j,i_src,i_dof,i_var,i_sol
    integer                             :: ndof_face,k,i_order,order
    real(kind=8)                        :: x_dir,xsrc(2)
    real(kind=8)                        :: polarization(2)
    complex(kind=8)                     :: scaling
    complex(kind=8)                     :: pw_coeff,wavespeed
    integer                             :: iquad,index_face_dofi
    !! for verification
    logical                             :: check_alloc
    integer                             :: check_nspeed
    !! linear system for planewave
    integer                             :: ndof_max_face
    real   (kind=8)        ,allocatable :: quad_2dposition_gb (:,:)
    real   (kind=8)        ,allocatable :: quad_2dposition_loc(:,:)
    complex(kind=8),allocatable         :: system_rhs(:,:)
    complex(kind=8),allocatable         :: system_sol(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    !! get the maximum order for allocations
    i_order       = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order)
    allocate(system_rhs(ndof_max_face,ctx_dg%dim_var))
    allocate(system_sol(ndof_max_face))
    allocate(quad_2dposition_gb (2,ctx_dg%quadGL_face_npts))
    allocate(quad_2dposition_loc(2,ctx_dg%quadGL_face_npts))
    
    ! -------------------------------------------------------
    !! for Planewave source: all dof on the specified boundary
    !! are to be taken.
    !! 1. First, we loop to identify the cells that are on the 
    !!    planewave boundary. 
    nface_pw = 0
    do i_cell=1,ctx_mesh%n_cell_loc
      do f=1,ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(f,i_cell)
        !! get the planewave tag
        if(neigh == tag_planewave       .or. &
           neigh == tag_planewave_isoP  .or. &
           neigh == tag_planewave_isoS  .or. &
           neigh == tag_planewave_scatt) then
          nface_pw = nface_pw + 1
        elseif(neigh == tag_planewave_vtiYSH .or. &
               neigh == tag_planewave_vtiYSV .or. &
               neigh == tag_planewave_vtiYqP) then
          ctx_err%msg   = "** ERROR: planewave VTI is not working "  // &
                          "for 2D domain [dg_source_init_planewave_3D] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !! non-shared error
        end if
      end do
    end do
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! 2. Allocation of the sources array, only boundary sources.
    do i_src=i_src_first, i_src_last
      j = i_src-i_src_first+1

      allocate(ctx_dg%src(j)%flag_on_bdry(nface_pw))
      allocate(ctx_dg%src(j)%icell_loc   (nface_pw))
      allocate(ctx_dg%src(j)%ndof_vol    (nface_pw))
      allocate(ctx_dg%src(j)%ndof_face   (nface_pw))
      allocate(ctx_dg%src(j)%iface_index (nface_pw))
      !! only the first n_cell_loc are actually allocated
      allocate(ctx_dg%src(j)%weight(ctx_dg%dim_sol,nface_pw))
      !! remains empty because we only have boundary sources.
      allocate(ctx_dg%src(j)%hdg_S(nface_pw))
      ctx_dg%src(j)%icell_loc     = 0
      ctx_dg%src(j)%ncell_loc     = int(nface_pw,kind=4) !! force int 4
      ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      ctx_dg%src(j)%flag_on_bdry  = .true. !> all are on the boundary
    end do
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! Here we impose the function using the weak form, that is 
    !! \int \Lambda \xi = \int g  \xi, with g the source function.
    !! 
    !! because we want an identity block to preserve the symmetry
    !! of the matrix, we have to solve the local system and find
    !! the values of \Lambda here.
    !! This is only if the matrix is symmetric, otherwise, not so important.
    !! -----------------------------------------------------------------
    !! 3.b) We create the face weight.
    !! -----------------------------------------------------------------
    counter_cell = 0
    do i_cell=1,ctx_mesh%n_cell_loc
      do f=1,ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(f,i_cell)
        !! get the planewave tag
        if(neigh == tag_planewave      .or. &
           neigh == tag_planewave_isoP .or. &
           neigh == tag_planewave_isoS .or. &
           neigh == tag_planewave_scatt) then
          counter_cell = counter_cell + 1
          
          !! get order
          face     = ctx_mesh%cell_face   (f,i_cell)
          order    = ctx_mesh%order_face  (face)
          i_order  = ctx_dg%order_to_index(order)
          ndof_face= ctx_dg%n_dof_face_per_order(i_order)
          
          !! check everything is ok 
          check_alloc  = allocated(ctx_dg%face_velocity(face)%array)
          check_nspeed = 0
          if(check_alloc) check_nspeed = size(ctx_dg%face_velocity(face)%array)
          if(.not. check_alloc .or. check_nspeed < ctx_dg%dim_var) then
            ctx_err%msg   = "** ERROR: Inconsistencies in the " //      &
                            "plane-wave wave speeds "           //     &
                            "[dg_source_init_planewave_2D] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared errpr
          end if
          
          
          !! fill the source array
          do i_src=i_src_first, i_src_last
            j = i_src-i_src_first+1  
            ctx_dg%src(j)%icell_loc  (counter_cell)   = i_cell
            ctx_dg%src(j)%iface_index(counter_cell)   = f
            ctx_dg%src(j)%ndof_face  (counter_cell)   = ndof_face
            do i_sol=1,ctx_dg%dim_sol
              call array_allocate(                                      &
                     ctx_dg%src(j)%weight(i_sol,counter_cell),          &
                     ndof_face,ctx_err)
              ctx_dg%src(j)%weight(i_sol,counter_cell)%array = 0.d0
            end do
          end do

          !! -----------------------------------------------------------
          !! convert the face quadrature positions to the 2D global mesh
          call mesh_convert_face_position_2D(                       &
               !! coordinates of the nodes of the triangle
               dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
               !! face index and inverse jacobian
               f,dble(ctx_dg%inv_jac(:,:,i_cell)),                  &
               !! all the positions to treat
               dble(ctx_dg%quadGL_face_pts(1,:)),                   &
               ctx_dg%quadGL_face_npts,                             &
               !! global and local positions.
               quad_2dposition_gb,quad_2dposition_loc,ctx_err)
          if(ctx_err%ierr .ne. 0) return !! non-shared error.      
          !! -----------------------------------------------------------

          !! evaluate the plane-wave at this position, depending on 
          !! its direction
          !! = exp( i (\omega/c) * (dir . x))
          do i_src=i_src_first, i_src_last
            j = i_src-i_src_first+1

            !! sum over all given directions
            system_rhs(:,:) = 0.d0
            do k=1, ctx_aq%sources(i_src)%nsim                
              !! 2D here
              xsrc = [ctx_aq%sources(i_src)%x(k), ctx_aq%sources(i_src)%z(k)]
              !! make sure it is normalized
              xsrc = xsrc / norm2(xsrc)
              !! polarization depends on the type of planewave
              scaling = 1.d0
              select case (neigh)
                case(tag_planewave_isoP)
                  polarization = xsrc
                  wavespeed    = dcmplx(ctx_dg%face_velocity(face)%array(1)) ! P-wave speed
                case(tag_planewave_isoS)
                  ! normal direction, because the vector is normalized
                  ! already, it simplifies the formulas.
                  polarization(1)= xsrc(2)
                  polarization(2)=-xsrc(1)
                  wavespeed    = dcmplx(ctx_dg%face_velocity(face)%array(2)) ! S-wave speed
                case(tag_planewave_scatt)
                  ! planewave_scattered
                  ! we have ugb = uinc + uscat with uinc the planewave
                  ! and uscat verifies Sommerfeld on the boundary, that is
                  ! \partial_n uscat - i k uscat = 0
                  ! condition for ugb on the boundary becomes: 
                  ! \partial_n ugb - i k ugb = +(\partial_n - ik) uinc
                  !                          =  (d.n i k - i k) uinc
                  wavespeed    = dcmplx(ctx_dg%face_velocity(face)%array(1)) ! P-wave speed
                  polarization = 1.d0
                  scaling      = (dot_product(xsrc,ctx_dg%normal(:,f,i_cell)) - 1.d0) &
                               * (freq/wavespeed)
                  !! HDG scaling: f/(i omega rho)
                  scaling      = scaling / (freq * ctx_dg%face_velocity(face)%array(2)) 

                case default !! for planewave
                  polarization = 1.d0
                  wavespeed    = dcmplx(ctx_dg%face_velocity(face)%array(1))
              end select
              
              
              !! create the weights
              do i_dof = 1, ndof_face !! loop over basis function.
                
                !! need to convert the index of the volume basis to     
                !! the appropriate face dof
                index_face_dofi = ctx_dg%face_map(i_order)%array(i_dof,f)

                !! -----------------------------------------------------
                !! compute the integral \int g \bxi using quadrature
                !! one per variable.
                do i_var=1,ctx_dg%dim_var
                  pw_coeff = 0.d0 
                  do iquad=1, ctx_dg%quadGL_face_npts
                      x_dir = dot_product(quad_2dposition_gb(:,iquad),xsrc)
                      !! planewave is 
                      !!
                      !!  polarization * exp(-ik (x.d))
                      !!
                      !! The array ctx_dg%quadGL_face_phi_w already
                      !! includes the weights for the quadrature.
                      pw_coeff = pw_coeff +                               &
                          ctx_dg%quadGL_face_phi_w(i_order)               &
                                          %array(index_face_dofi,iquad,f) &
                        * scaling*polarization(i_var)*exp(freq/wavespeed * x_dir)
                  end do
                  pw_coeff   = pw_coeff*ctx_dg%det_jac_face(f,i_cell)
                  system_rhs(i_dof,i_var) = system_rhs(i_dof,i_var)+pw_coeff
                end do
                
              end do
            end do !! end loop over nsim

            !! ----------------------------------------------------------- %
            !! we need to convert the full block L Lambda_face = planewave
            !! To a identity based bloc Id Lambda = hat{planewave}.
            !! ----------------------------------------------------------- %
            if(ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize) then
              !! solve the dense linear system (false is for adjoint)
              do i_var=1,ctx_dg%dim_var
                system_sol = 0.d0
                i_sol = ctx_dg%ind_var_to_sol(i_var) !! convert trace to volume position
                call lapack_solve_system(                                   &
                dcmplx(ctx_dg%ctx_symmetrize_mat%f_matbc(face,i_var)%array),&
                       system_rhs(1:ndof_face,i_var),                       &
                       ndof_face,ndof_face,.false.,                         &
                       system_sol(1:ndof_face),ctx_err)
                !! replace the RHS accordingly
                ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) =     &
                                                 system_sol(1:ndof_face)
              end do
            else
              !! no need as the matrix is not symmetric anyway.
              do i_var=1,ctx_dg%dim_var
                !! convert var position to global sol position
                i_sol = ctx_dg%ind_var_to_sol(i_var)
                ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) =     &
                                             system_rhs(1:ndof_face,i_var)
              end do
            end if

          end do !! end loop over sources

       else if(neigh == tag_planewave_vtiYSH .or.                       &
               neigh == tag_planewave_vtiYSV .or.                       &
               neigh == tag_planewave_vtiYqP) then
          ctx_err%msg   = "** ERROR: planewave VTI is not working "  // &
                          "for 2D domain [dg_source_init_planewave_3D] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !! non-shared error

       end if !! end if planewave boundary

      end do !! end loop over faces

    end do !! end loop over cell

    deallocate(system_rhs         )
    deallocate(system_sol         )
    deallocate(quad_2dposition_gb )
    deallocate(quad_2dposition_loc)
    return

  end subroutine dg_source_init_planewave_2d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the sources for planewave in 3 dimensions
  !>  - the planewave is defined by its direction
  !>  - it is applied on the specified boundary
  !>  - it corresponds to a dirichlet condition imposed: that is, 
  !>    we only have BOUNDARY source in this case. 
  !> In addition, we allow for several directions at the same time, 
  !> using a sum of planewaves.
  !>
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    freq           : plane wave frequency
  !> @param[in]    i_src_first    : fisrt source index
  !> @param[in]    i_src_last     : last source index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_planewave_3d(ctx_aq,ctx_mesh,ctx_dg,freq,   &
                                         i_src_first,i_src_last,ctx_err)
    implicit none

    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_mesh
    type(t_discretization)           ,intent(inout):: ctx_dg
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: i_src_first,i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)            :: i_cell,nface_pw,neigh,face
    integer(kind=IKIND_MESH)            :: counter_cell
    integer                             :: f,j,i_src,i_dof,i_var,i_sol
    integer                             :: ndof_face,k,i_order,order
    real   (kind=8)                     :: x_dir,xsrc(3)
    complex(kind=8)                     :: polarization(3)
    complex(kind=8)                     :: pw_coeff,wavespeed,scaling
    !! convert 3D face triangles to 2D for the 4 faces
    integer                             :: iquad,index_face_dofi
    !! for verification
    logical                             :: check_alloc
    integer                             :: check_nspeed
    !! verification of vti
    real   (kind=8)                     :: rho
    complex(kind=8)                     :: c11, c12, c13, c22, c23
    complex(kind=8)                     :: c33, c44, c55, c66, Ccoeff
    real   (kind=8), parameter          :: tol_vti = 1.d-9
    !! linear system for planewave or analytic functions
    integer                             :: ndof_max_face
    real   (kind=8)        ,allocatable :: quad_3dposition_gb (:,:)
    real   (kind=8)        ,allocatable :: quad_3dposition_loc(:,:)
    complex(kind=8),allocatable         :: system_rhs(:,:)
    complex(kind=8),allocatable         :: system_sol(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! get the maximum order for allocations to solve linear system.
    i_order       = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order)
    allocate(system_rhs(ndof_max_face,ctx_dg%dim_var))
    allocate(system_sol(ndof_max_face))
    allocate(quad_3dposition_gb (3,ctx_dg%quadGL_face_npts))
    allocate(quad_3dposition_loc(3,ctx_dg%quadGL_face_npts))
    wavespeed = 0.d0

    ! -------------------------------------------------------
    !! for Planewave source: all dof on the specified boundary
    !! are to be taken.
    !! 1. First, we loop to identify the cells that are on the 
    !!    planewave boundary. 
    nface_pw = 0
    do i_cell=1,ctx_mesh%n_cell_loc
      do f=1,ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(f,i_cell)
        !! get the planewave tag
        if(neigh == tag_planewave_isoP   .or. &
           neigh == tag_planewave_isoS   .or. &
           neigh == tag_planewave        .or. &
           neigh == tag_planewave_vtiYSH .or. &
           neigh == tag_planewave_vtiYSV .or. &
           neigh == tag_planewave_vtiYqP .or. &
           neigh == tag_planewave_scatt) then
          nface_pw = nface_pw + 1
       end if
      end do
    end do
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! 2. Allocation of the sources array, only boundary sources.
    do i_src=i_src_first, i_src_last
      j = i_src-i_src_first+1

      allocate(ctx_dg%src(j)%flag_on_bdry(nface_pw))
      allocate(ctx_dg%src(j)%icell_loc   (nface_pw))
      allocate(ctx_dg%src(j)%ndof_vol    (nface_pw))
      allocate(ctx_dg%src(j)%ndof_face   (nface_pw))
      allocate(ctx_dg%src(j)%iface_index (nface_pw))
      !! only the first n_cell_loc are actually allocated
      allocate(ctx_dg%src(j)%weight(ctx_dg%dim_sol,nface_pw))
      !! remains empty because we only have boundary sources.
      allocate(ctx_dg%src(j)%hdg_S (nface_pw))
      ctx_dg%src(j)%icell_loc     = 0
      ctx_dg%src(j)%ncell_loc     = int(nface_pw,kind=4) !! force int 4
      ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      ctx_dg%src(j)%flag_on_bdry  = .true. !> all are on the boundary
    end do
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! Because we impose the dirichlet boundary condition using 
    !! the identity matrix, we simply need to force the value to
    !! the planewewave.
    !! That is, we do not need to approximate integrals.
    !! -----------------------------------------------------------------
    !! 3.b) We create the face weight.
    !! -----------------------------------------------------------------
    counter_cell = 0
    do i_cell=1,ctx_mesh%n_cell_loc
      do f=1,ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(f,i_cell)
        !! get the planewave tag
        if(neigh == tag_planewave        .or. & 
           neigh == tag_planewave_isoP   .or. &
           neigh == tag_planewave_isoS   .or. &
           neigh == tag_planewave_vtiYSH .or. &
           neigh == tag_planewave_vtiYSV .or. &
           neigh == tag_planewave_vtiYqP .or. &
           neigh == tag_planewave_scatt) then
          counter_cell = counter_cell + 1
          
          !! get order
          face     = ctx_mesh%cell_face   (f,i_cell)
          order    = ctx_mesh%order_face  (face)
          i_order  = ctx_dg%order_to_index(order)
          ndof_face= ctx_dg%n_dof_face_per_order(i_order)
          
          !! fill the source array
          do i_src=i_src_first, i_src_last
            j = i_src-i_src_first+1  
            ctx_dg%src(j)%icell_loc  (counter_cell)   = i_cell
            ctx_dg%src(j)%iface_index(counter_cell)   = f
            ctx_dg%src(j)%ndof_face  (counter_cell)   = ndof_face
            do i_sol=1,ctx_dg%dim_sol
              call array_allocate(                                      &
                              ctx_dg%src(j)%weight(i_sol,counter_cell), &
                              ndof_face,ctx_err)
            end do
          end do

          !! -----------------------------------------------------------
          !! convert the face quadrature positions to the 2D global mesh
          call mesh_convert_face_position_3D(                     &
               !! coordinates of the nodes of the triangle
               dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
               !! face index and inverse jacobian
               f,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
               !! all the positions to treat
               dble(ctx_dg%quadGL_face_pts(:,:)),                 &
               ctx_dg%quadGL_face_npts,                           &
               !! global and local positions.
               quad_3dposition_gb,quad_3dposition_loc,ctx_err)
          if(ctx_err%ierr .ne. 0) return !! non-shared error.
          !! -----------------------------------------------------------
          
          !! check everything is ok 
          check_alloc  = allocated(ctx_dg%face_velocity(face)%array)
          check_nspeed = 0
          if(check_alloc) check_nspeed =                            &
                          size(ctx_dg%face_velocity(face)%array)
          select case (neigh)
            ! elastic isotropic planewave ------------------------------
            case(tag_planewave_isoP,tag_planewave_isoS)
              !! vp, vs and rho
              if(.not. check_alloc .or. check_nspeed .ne. 3) then
                ctx_err%msg   = "** ERROR: Inconsistencies in the " //  &
                                "plane-wave wave speeds "           //  &
                                "[dg_source_init_planewave_3D] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                return !! non-shared error
              end if
            ! vti planewave --------------------------------------------
            case(tag_planewave_vtiYSH,tag_planewave_vtiYSV,tag_planewave_vtiYqP)
              !! we need VTI:
              !! ( c11      (c11-2c66)   c13     0   0   0 )
              !! ((c11-2c66)   c11       c13     0   0   0 )
              !! ( c13         c13       c33     0   0   0 )
              !! (   0         0          0     c55  0   0 )
              !! (   0         0          0      0   c55 0 )
              !! (   0         0          0      0   0   c66)
              !! here we save the symmetric stiffness C made up of
              !! 9 parameters and density
              !! later on, we will check it has the appropriate
              !! structures
              if(.not. check_alloc .or. check_nspeed .ne. 10) then
                ctx_err%msg   = "** ERROR: Inconsistencies in the " //  &
                                "plane-wave stiffness tensor C "    //  &
                                "[dg_source_init_planewave_3D] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                return !! non-shared error
              end if
            ! acoustic planewave ---------------------------------------
            case(tag_planewave)
              if(.not. check_alloc .or. check_nspeed .ne. 1) then
                ctx_err%msg   = "** ERROR: Inconsistencies in the " //  &
                                "plane-wave wave speeds "           //  &
                                "[dg_source_init_planewave_3D] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                return !! non-shared error
              end if
            case(tag_planewave_scatt)
              if(.not. check_alloc .or. check_nspeed .ne. 2) then
                ctx_err%msg   = "** ERROR: Inconsistencies in the " //  &
                                "plane-wave wave speeds "           //  &
                                "[dg_source_init_planewave_3D] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                return !! non-shared error
              end if
            case default
              ctx_err%msg   = "** ERROR: Unrecognized boundary "    //  &
                              "[dg_source_init_planewave_3D] **"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              return !! non-shared error
          end select
          !! -------------------------------------------------------
          
          do i_src=i_src_first, i_src_last
            j = i_src-i_src_first+1
            
            !! sum over all given directions
            system_rhs = 0.d0
            do k=1, ctx_aq%sources(i_src)%nsim                
              !! 3D here
              xsrc = [ctx_aq%sources(i_src)%x(k),                   &
                      ctx_aq%sources(i_src)%y(k),                   &
                      ctx_aq%sources(i_src)%z(k)]

              !! polarization and scaling depend on the type of planewave
              scaling = 1.d0 !! only used for scattered field for now.
              select case (neigh)
                
                !! isotropic elastic P-wave ---------------------------------------------
                case(tag_planewave_isoP)
                  polarization = xsrc
                  wavespeed    = ctx_dg%face_velocity(face)%array(1) ! P-wave speed
                
                !! isotropic elastic S-wave ---------------------------------------------
                case(tag_planewave_isoS)
                  ! normal direction to the 3D vector, as the input is 
                  ! already normalized, it simplifies the formulas.
                  ! xsrc . polarization = 0
                  ! => a xsrc(1) + b xsrc(2) + c xsrc(3) = 0
                  ! hence there are many solutions .....................
                  if(abs(xsrc(1)) < tiny(1.0)) then
                    ! say the normal to (y-z).
                    polarization(1)=  0.d0 
                    polarization(2)= xsrc(3)
                    polarization(3)=-xsrc(2)
                  else
                    if(abs(xsrc(2)) < tiny(1.0)) then
                      ! say the normal to (x-z).
                      polarization(1)= xsrc(3)
                      polarization(2)=  0.d0
                      polarization(3)=-xsrc(1)
                    else
                      if(abs(xsrc(3)) < tiny(1.0)) then
                        ! say the normal to (x-y).
                        polarization(1)= xsrc(2)
                        polarization(2)=-xsrc(1)
                        polarization(3)=  0.d0
                      else
                        ! arbitrary
                        ! => a xsrc(1) + b xsrc(2) + c xsrc(3) = 0
                        polarization(1) = xsrc(2)
                        polarization(2) =-xsrc(1)
                        polarization(3) = 0.d0
                        polarization = polarization / norm2(real(polarization))
                      end if
                    end if
                  end if
                  wavespeed    = ctx_dg%face_velocity(face)%array(2) ! S-wave speed
                
                !! acoustic ------------------------------------------------------
                case(tag_planewave)
                  polarization = 1.d0
                  wavespeed    = ctx_dg%face_velocity(face)%array(1)
                !! for scattered field
                case(tag_planewave_scatt)
                  wavespeed    = dcmplx(ctx_dg%face_velocity(face)%array(1))
                  polarization = 1.d0
                  scaling      = (dot_product(xsrc,ctx_dg%normal(:,f,i_cell)) - 1.d0) &
                               * (freq/wavespeed)
                  !! HDG scaling: f/(i omega rho)
                  scaling      = scaling / (freq * ctx_dg%face_velocity(face)%array(2)) 


                !! elastic VTI y direction ---------------------------------------
                case(tag_planewave_vtiYSH,                              &
                     tag_planewave_vtiYSV,                              &
                     tag_planewave_vtiYqP)
                  c11 = ctx_dg%face_velocity(face)%array(1)
                  c22 = ctx_dg%face_velocity(face)%array(2)
                  c33 = ctx_dg%face_velocity(face)%array(3)
                  c44 = ctx_dg%face_velocity(face)%array(4)
                  c55 = ctx_dg%face_velocity(face)%array(5)
                  c66 = ctx_dg%face_velocity(face)%array(6)
                  c12 = ctx_dg%face_velocity(face)%array(7)
                  c13 = ctx_dg%face_velocity(face)%array(8)
                  c23 = ctx_dg%face_velocity(face)%array(9)
                  rho = real(ctx_dg%face_velocity(face)%array(10))

                  !! validations ==================
                  if( abs(c22-c11)            > tol_vti .or.          &
                      abs(c44-c55)            > tol_vti .or.          &
                      abs(c13-c23)            > tol_vti .or.          &
                      abs(c12-(c11-2.d0*c66)) > tol_vti) then
                    ctx_err%msg   = "** ERROR: The medium does not " // &
                                    "seem to be properly VTI for the "//&
                                    "planewave [dg_source_init_planewave_3D] **"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    return !! non-shared error                  
                  
                  end if
                  if(abs(xsrc(2)) > tiny(1.0)) then
                    ctx_err%msg   = "** ERROR: VTI planewave only "  // &
                                    "supported if y-direction set to 0 "//&
                                    "[dg_source_init_planewave_3D] **"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    return !! non-shared error
                  end if
                  
                  select case (neigh)
                    case(tag_planewave_vtiYSH)
                      !! computed from vsh² = (c66 kx² + c55kz²)/rho
                      wavespeed  = sqrt((c66*xsrc(1)**2 + c55*xsrc(3)**2)/rho)
                      polarization(1)=  0.d0
                      polarization(2)=  1.d0
                      polarization(3)=  0.d0
                    case(tag_planewave_vtiYSV)
                      Ccoeff = ((c11-c55)*xsrc(1)**2 + (c55-c33)*xsrc(3)**2)**2&
                             + 4.d0*((c13+c55)**2)*(xsrc(1)**2)*(xsrc(3)**2)
                      Ccoeff = sqrt(Ccoeff)
                      wavespeed  = sqrt(                                 &
                         (c11*xsrc(1)**2 + c33*xsrc(3)**2 + c55 - Ccoeff)&
                          / (2.d0*rho) )
                      ! reset for polarization
                      Ccoeff = 1.d0/sqrt((c11+c55)*xsrc(1)**2           &
                                        +(c55+c33)*xsrc(3)**2           &
                                        -2.d0*rho*wavespeed)
                      polarization(1)=  Ccoeff*sqrt(c55*xsrc(1)**2      &
                                        + c33*xsrc(3)**2 - rho*wavespeed**2)
                      polarization(2)=  0.d0
                      polarization(3)=  Ccoeff*sqrt(c11*xsrc(1)**2      &
                                        + c55*xsrc(3)**2 - rho*wavespeed**2)
                    case(tag_planewave_vtiYqP)
                      Ccoeff = ((c11-c55)*xsrc(1)**2 + (c55-c33)*xsrc(3)**2)**2&
                             + 4.d0*((c13+c55)**2)*(xsrc(1)**2)*(xsrc(3)**2)
                      Ccoeff = sqrt(Ccoeff)
                      wavespeed  = sqrt(                                 &
                         (c11*xsrc(1)**2 + c33*xsrc(3)**2 + c55 + Ccoeff)&
                          / (2.d0*rho) )
                      
                      ! reset for polarization
                      Ccoeff = 1.d0/sqrt((c11+c55)*xsrc(1)**2           &
                                        +(c55+c33)*xsrc(3)**2           &
                                        -2.d0*rho*wavespeed)
                      polarization(1)=  Ccoeff*sqrt(c55*xsrc(1)**2      &
                                        + c33*xsrc(3)**2 - rho*wavespeed**2)
                      polarization(2)=  0.d0
                      polarization(3)=  Ccoeff*sqrt(c11*xsrc(1)**2      &
                                        + c55*xsrc(3)**2 - rho*wavespeed**2)
                  end select

                !! error -----------------------------------------------
                case default
                  ctx_err%msg   = "** ERROR: Unrecognized boundary "    //  &
                                  "[dg_source_init_planewave_3D] **"
                  ctx_err%ierr  = -1
                  ctx_err%critic=.true.
                  return !! non-shared error
              end select


              !! update weight
              do i_dof = 1, ndof_face

                !! need to convert the index of the volume basis to     
                !! the appropriate face dof
                index_face_dofi = ctx_dg%face_map(i_order)%array(i_dof,f)

                !! -----------------------------------------------------
                !! compute the integral \int g \bxi using quadrature
                !! one per var
                do i_var=1,ctx_dg%dim_var
                  pw_coeff = 0.d0 
                  do iquad=1, ctx_dg%quadGL_face_npts
                      x_dir    = dot_product(quad_3dposition_gb(:,iquad),xsrc)
                      pw_coeff = pw_coeff +                               &
                          ctx_dg%quadGL_face_phi_w(i_order)               &
                                          %array(index_face_dofi,iquad,f) &
                        *scaling*polarization(i_var)*exp(freq/wavespeed * x_dir)
                  end do
                  pw_coeff = pw_coeff*ctx_dg%det_jac_face(f,i_cell)
                  system_rhs(i_dof,i_var) = system_rhs(i_dof,i_var) + pw_coeff
                end do
              end do
            end do !! end loop over simultaneous sources 
 
            !! ----------------------------------------------------------- %
            !! we need to convert the full block L Lambda_face = planewave
            !! To a identity based bloc Id Lambda = hat{planewave}.
            !! ----------------------------------------------------------- %
            if(ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize) then
              !! solve the dense linear system (false is for adjoint)
              do i_var=1,ctx_dg%dim_var
                i_sol = ctx_dg%ind_var_to_sol(i_var) !! convert trace to volume position
                system_sol = 0.d0
                call lapack_solve_system(                                   &
                dcmplx(ctx_dg%ctx_symmetrize_mat%f_matbc(face,i_var)%array),&
                      system_rhs(1:ndof_face,i_var),ndof_face,ndof_face,    &
                      .false.,system_sol(1:ndof_face),ctx_err)
                !! replace the RHS accordingly
                ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = &
                                                 system_sol(1:ndof_face)
              end do
            else
              !! no need as the matrix is not symmetric anyway.
              do i_var=1,ctx_dg%dim_var
                i_sol = ctx_dg%ind_var_to_sol(i_var) !! convert trace to volume position
                ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = &
                                            system_rhs(1:ndof_face,i_var)
              end do
            end if
          
          end do !! end loop over sources
            
       end if !! end if planewave boundary

      end do !! end loop over faces

    end do !! end loop over cell

    deallocate(system_rhs         )
    deallocate(system_sol         )
    deallocate(quad_3dposition_gb )
    deallocate(quad_3dposition_loc)

    return

  end subroutine dg_source_init_planewave_3d



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the sources for planewave in 3 dimensions
  !>  - the planewave is defined by its direction
  !>  - it is applied on the specified boundary
  !>  - it corresponds to a dirichlet condition imposed: that is, 
  !>    we only have BOUNDARY source in this case. 
  !> In addition, we allow for several directions at the same time, 
  !> using a sum of planewaves.
  !>
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    freq           : plane wave frequency
  !> @param[in]    i_src_first    : fisrt source index
  !> @param[in]    i_src_last     : last source index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_source_init_analytic_elastic(ctx_paral,ctx_aq,ctx_mesh, &
                                              ctx_dg,freq,              &
                                              i_src_first,i_src_last,   &
                                              ctx_err)
    implicit none

    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_mesh
    type(t_discretization)           ,intent(inout):: ctx_dg
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: i_src_first,i_src_last
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)            :: i_cell,nface_pw,neigh,face
    integer(kind=IKIND_MESH)            :: counter_cell
    integer                             :: f,j,i_src,i_dof,i_var
    integer                             :: ndof_face,k,i_order,order
    real(kind=8)                        :: xsrc(3)
    !! convert 3D face triangles to 2D for the 4 faces
    integer                             :: iquad,index_face_dofi
    !! for verification
    logical                             :: check_alloc
    integer                             :: check_nspeed
    !! linear system for planewave or analytic functions
    integer                             :: ndof_max_face
    real   (kind=8)        ,allocatable :: quad_3dposition_gb (:,:)
    real   (kind=8)        ,allocatable :: quad_3dposition_loc(:,:)
    complex(kind=8),allocatable         :: system_rhs(:,:)
    complex(kind=8),allocatable         :: system_sol(:)
    !! for orthorhombic and isotropy
    integer                             :: n,n1,i_sol
    complex(kind=8)        ,allocatable :: uval(:,:)
    complex(kind=8)                     :: Cstiff(6,6),vp,vs
    complex(kind=RKIND_POL)             :: int_phi_f
    real   (kind=8)                     :: rho
    type(t_dg_source)     ,allocatable  :: ctx_src_dirac(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! get the maximum order for allocations to solve linear system.
    i_order       = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order)
    allocate(system_rhs(ndof_max_face,ctx_dg%dim_var))
    allocate(system_sol(ndof_max_face))
    allocate(quad_3dposition_gb (3,ctx_dg%quadGL_face_npts))
    allocate(quad_3dposition_loc(3,ctx_dg%quadGL_face_npts))

    !! -----------------------------------------------------------------
    !! first create the dirac source at the positions
    !! 
    !! -----------------------------------------------------------------
    call dg_source_init_dirac(ctx_paral,ctx_aq,ctx_mesh,ctx_dg,         &
                              i_src_first,i_src_last,tag_RHS_SCALE_ID,  &
                              ctx_err)
    if(ctx_err%ierr .ne. 0) return
    n = size(ctx_dg%src)
    allocate(ctx_src_dirac(n))
    do k=1,n
      call dg_source_copy_context (ctx_dg%src(k),ctx_src_dirac(k),ctx_err)
      call dg_source_clean_context(ctx_dg%src(k),.true.)
      if(ctx_err%ierr .ne. 0) return
    end do
    !! -----------------------------------------------------------------

    ! -------------------------------------------------------
    !! for function imposed on the boundary: all dof on the 
    !! specified boundary are to be taken.
    !! 1. First, we loop to identify the cells that are on the boundary
    nface_pw = 0
    do i_cell=1,ctx_mesh%n_cell_loc
      do f=1,ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(f,i_cell)
        !! get the planewave tag
        if(neigh == tag_analytic_orthorhombic .or. &
           neigh == tag_analytic_elasticiso) then
          nface_pw = nface_pw + 1
       end if
      end do
    end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 2. Allocation of the sources array, with both boundary sources
    !!    and the dirac.
    do i_src=i_src_first, i_src_last
      j = i_src-i_src_first+1
      !! include the dirac
      n = int(ctx_src_dirac(j)%ncell_loc, kind=4)

      allocate(ctx_dg%src(j)%flag_on_bdry(nface_pw + n))
      allocate(ctx_dg%src(j)%icell_loc   (nface_pw + n))
      allocate(ctx_dg%src(j)%ndof_vol    (nface_pw + n))
      allocate(ctx_dg%src(j)%ndof_face   (nface_pw + n))
      allocate(ctx_dg%src(j)%iface_index (nface_pw + n))
      !! only the first n_cell_loc are actually allocated
      allocate(ctx_dg%src(j)%weight(ctx_dg%dim_sol,nface_pw + n))
      !! remains empty for boundary sources but not for dirac.
      allocate(ctx_dg%src(j)%hdg_S(nface_pw + n))
      ctx_dg%src(j)%icell_loc     = 0
      ctx_dg%src(j)%ncell_loc     = int(nface_pw + n,kind=4) !! force int 4
      ctx_dg%src(j)%iface_index   = 0
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      ctx_dg%src(j)%flag_on_bdry  = .true. !> all are on the boundary
    end do
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! Because we impose the dirichlet boundary condition using 
    !! the identity matrix, we simply need to force the value to
    !! the planewewave.
    !! That is, we do not need to approximate integrals.
    !! -----------------------------------------------------------------
    !! 3.b) We create the face weight.
    !! -----------------------------------------------------------------
    counter_cell = 0
    do i_cell=1,ctx_mesh%n_cell_loc
      do f=1,ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(f,i_cell)
        !! get the planewave tag
        if(neigh == tag_analytic_elasticiso .or.   & 
           neigh == tag_analytic_orthorhombic) then
          counter_cell = counter_cell + 1
          
          !! get order
          face     = ctx_mesh%cell_face   (f,i_cell)
          order    = ctx_mesh%order_face  (face)
          i_order  = ctx_dg%order_to_index(order)
          ndof_face= ctx_dg%n_dof_face_per_order(i_order)
          
          !! fill the source array
          do i_src=i_src_first, i_src_last
            j = i_src-i_src_first+1  
            ctx_dg%src(j)%icell_loc  (counter_cell)   = i_cell
            ctx_dg%src(j)%iface_index(counter_cell)   = f
            ctx_dg%src(j)%ndof_face  (counter_cell)   = ndof_face
            do i_var=1,ctx_dg%dim_var
              call array_allocate(                                      &
                              ctx_dg%src(j)%weight(i_var,counter_cell), &
                              ndof_face,ctx_err)
            end do
          end do

          !! -----------------------------------------------------------
          !! convert the face quadrature positions to the 2D global mesh
          call mesh_convert_face_position_3D(                     &
               !! coordinates of the nodes of the triangle
               dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
               !! face index and inverse jacobian
               f,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
               !! all the positions to treat
               dble(ctx_dg%quadGL_face_pts(:,:)),                 &
               ctx_dg%quadGL_face_npts,                           &
               !! global and local positions.
               quad_3dposition_gb,quad_3dposition_loc,ctx_err)
          if(ctx_err%ierr .ne. 0) return !! non-shared error.      
          !! -----------------------------------------------------------


          !! -----------------------------------------------------------
          !! we impose the function on the boundary 
          !! we allow for planewave or analytic functions.
          !! -----------------------------------------------------------
          allocate(uval(3,ctx_dg%quadGL_face_npts))
          
          do i_src=i_src_first, i_src_last
            j = i_src-i_src_first+1
          
            system_rhs = 0.d0
            do k=1, ctx_aq%sources(i_src)%nsim                
              !! 3D here
              xsrc = [ctx_aq%sources(i_src)%x(k),                   &
                      ctx_aq%sources(i_src)%y(k),                   &
                      ctx_aq%sources(i_src)%z(k)]


              select case(neigh)
              
                !! *********************************************************            
                !!             
                !! Analytic 3D Orthorhombic function 
                !!
                !! *********************************************************
                case(tag_analytic_orthorhombic)
              
                  !! check everything is ok 
                  check_alloc  = allocated(ctx_dg%face_velocity(face)%array)
                  check_nspeed = 0
                  if(check_alloc) check_nspeed =                            &
                                  size(ctx_dg%face_velocity(face)%array)
                  !! symmetric C = 6+5+4+3+2+1 = 21 + rho = 22.
                  if(.not. check_alloc .or. check_nspeed .ne. 22) then
                    ctx_err%msg   = "** ERROR: Inconsistencies in the " //  &
                                    "analytic orthorhombic parameters " //  &
                                    "[dg_source_init_planewave_3D] **"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    return !! non-shared error
                  end if
                  !! -------------------------------------------------------
                  
                  !! .....................................................
                  !! there is no sum over directions now.
                  !! .....................................................
                  Cstiff=0.d0
                  rho   =0.d0
                  Cstiff(1,1) = ctx_dg%face_velocity(face)%array( 1)
                  Cstiff(2,2) = ctx_dg%face_velocity(face)%array( 2)
                  Cstiff(3,3) = ctx_dg%face_velocity(face)%array( 3)
                  Cstiff(4,4) = ctx_dg%face_velocity(face)%array( 4)
                  Cstiff(5,5) = ctx_dg%face_velocity(face)%array( 5)
                  Cstiff(6,6) = ctx_dg%face_velocity(face)%array( 6)
                  Cstiff(1,2) = ctx_dg%face_velocity(face)%array( 7)
                  Cstiff(1,3) = ctx_dg%face_velocity(face)%array( 8)
                  Cstiff(1,4) = ctx_dg%face_velocity(face)%array( 9)
                  Cstiff(1,5) = ctx_dg%face_velocity(face)%array(10)
                  Cstiff(1,6) = ctx_dg%face_velocity(face)%array(11)
                  Cstiff(2,3) = ctx_dg%face_velocity(face)%array(12)
                  Cstiff(2,4) = ctx_dg%face_velocity(face)%array(13)
                  Cstiff(2,5) = ctx_dg%face_velocity(face)%array(14)
                  Cstiff(2,6) = ctx_dg%face_velocity(face)%array(15)
                  Cstiff(3,4) = ctx_dg%face_velocity(face)%array(16)
                  Cstiff(3,5) = ctx_dg%face_velocity(face)%array(17)
                  Cstiff(3,6) = ctx_dg%face_velocity(face)%array(18)
                  Cstiff(4,5) = ctx_dg%face_velocity(face)%array(19)
                  Cstiff(4,6) = ctx_dg%face_velocity(face)%array(20)
                  Cstiff(5,6) = ctx_dg%face_velocity(face)%array(21)
                  rho         = dble(ctx_dg%face_velocity(face)%array(22))

                  call analytic_function_elastic_orthorhombic(          &
                                Cstiff,rho,freq,quad_3dposition_gb,xsrc,&
                                ctx_aq%src_component,                   &
                                ctx_aq%ncomponent_src,                  &
                                ctx_dg%quadGL_face_npts,                &
                                uval,ctx_err)
                  if(ctx_err%ierr .ne. 0) return !! non-shared error. 

                !! *********************************************************            
                !!             
                !! Analytic 3D Elastic Isotropy
                !!
                !! *********************************************************
                case(tag_analytic_elasticiso)
              
                  !! check everything is ok 
                  check_alloc  = allocated(ctx_dg%face_velocity(face)%array)
                  check_nspeed = 0
                  if(check_alloc) check_nspeed =                            &
                                  size(ctx_dg%face_velocity(face)%array)
                  !! vp, vs and rho
                  if(.not. check_alloc .or. check_nspeed .ne. 3) then
                    ctx_err%msg   = "** ERROR: Inconsistencies in the " //  &
                                    "analytic parameters "              //  &
                                    "[dg_source_init_planewave_3D] **"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    return !! non-shared error
                  end if
                  !! -------------------------------------------------------                  
                  vp  =dcmplx(ctx_dg%face_velocity(face)%array(1))
                  vs  =dcmplx(ctx_dg%face_velocity(face)%array(2))
                  rho =  dble(ctx_dg%face_velocity(face)%array(3))
                  call analytic_function_elastic_isotropic(             &
                                         vp,vs,rho,freq,                &
                                         quad_3dposition_gb,xsrc,       &
                                         ctx_aq%src_component,          &
                                         ctx_aq%ncomponent_src,         &
                                         ctx_dg%quadGL_face_npts,       &
                                         uval,ctx_err)
                  if(ctx_err%ierr .ne. 0) return !! non-shared error. 
                case default
                  ctx_err%msg   = "** ERROR: unrecognized tag " // &
                                  "[dg_source_init_planewave_3d] **"
                  ctx_err%ierr  = -1
                  ctx_err%critic=.true.
                  return ! non-share error
              end select


              !! ---------------------------------------------------
              !! update weight: for each of the basis function, we 
              !! compute \int \phi_i(x) f(x),
              !! with \phi the basis function and f the analytic 
              !! function computed.
              !! ---------------------------------------------------
              do i_dof = 1, ndof_face
          
                !! need to convert the index of the volume basis to     
                !! the appropriate face dof
                index_face_dofi = ctx_dg%face_map(i_order)%array(i_dof,f)
          
                !! -----------------------------------------------------
                !! compute the integral \int g \bxi using quadrature
                !! one per var
                do i_var=1,ctx_dg%dim_var
                  int_phi_f = 0.d0 
                  do iquad=1, ctx_dg%quadGL_face_npts
                      int_phi_f = int_phi_f +                       &
                          ctx_dg%quadGL_face_phi_w(i_order)         &
                                    %array(index_face_dofi,iquad,f) &
                        * uval(i_var,iquad)
                  
                  end do
                  int_phi_f = int_phi_f*ctx_dg%det_jac_face(f,i_cell)
                  system_rhs(i_dof,i_var) = system_rhs(i_dof,i_var) &
                                          + int_phi_f

                end do ! end loop over variables (i_var)
              end do ! end loop over basis function (i_dof)
            end do ! end loop over simultaneous sources 

            !! ----------------------------------------------------------- %
            !! we need to convert the full block L Lambda_face = planewave
            !! To a identity based bloc Id Lambda = hat{planewave}.
            !! ----------------------------------------------------------- %
            if(ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize) then
              !! solve the dense linear system (false is for adjoint)
              do i_var=1,ctx_dg%dim_var
                i_sol = ctx_dg%ind_var_to_sol(i_var) !! convert trace to volume position
                system_sol = 0.d0
                call lapack_solve_system(                                   &
                dcmplx(ctx_dg%ctx_symmetrize_mat%f_matbc(face,i_var)%array),&
                      system_rhs(1:ndof_face,i_var),ndof_face,ndof_face,    &
                      .false.,system_sol(1:ndof_face),ctx_err)
                !! replace the RHS accordingly
                ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) =     &
                                                 system_sol(1:ndof_face)
              end do
            else
              !! no need as the matrix is not symmetric anyway.
              do i_var=1,ctx_dg%dim_var
                i_sol = ctx_dg%ind_var_to_sol(i_var) !! convert trace to volume position
                ctx_dg%src(j)%weight(i_sol,counter_cell)%array(:) = &
                                            system_rhs(1:ndof_face,i_var)
              end do
            end if

          end do !! end loop over sources

          deallocate(uval)
           
       end if !! end if planewave boundary

      end do !! end loop over faces

    end do !! end loop over cell

      
    !! -----------------------------------------------------------------
    !!
    !! Include the informations saved for the Dirac source at the 
    !! end of the arrays
    !!
    !! -----------------------------------------------------------------
    do i_src=i_src_first, i_src_last
      j = i_src-i_src_first+1
      !! include the dirac
      n = int(ctx_src_dirac(j)%ncell_loc, kind=4)
      
      if(n .ne. 0) then
        !! main arrays
        ctx_dg%src(j)%flag_on_bdry(nface_pw+1:nface_pw+n) = ctx_src_dirac(j)%flag_on_bdry(1:n)
        ctx_dg%src(j)%icell_loc   (nface_pw+1:nface_pw+n) = ctx_src_dirac(j)%icell_loc   (1:n)
        ctx_dg%src(j)%ndof_vol    (nface_pw+1:nface_pw+n) = ctx_src_dirac(j)%ndof_vol    (1:n)
        ctx_dg%src(j)%ndof_face   (nface_pw+1:nface_pw+n) = ctx_src_dirac(j)%ndof_face   (1:n)
        ctx_dg%src(j)%iface_index (nface_pw+1:nface_pw+n) = ctx_src_dirac(j)%iface_index (1:n)

        !! loop over all concerned cells to copy the weight and hdgSarray.
        do k=1,n
          if(allocated(ctx_src_dirac(j)%weight(1,k)%array)) then
            n1=size(ctx_src_dirac(j)%weight(1,k)%array)
            do i_sol = 1,ctx_dg%dim_sol
              call array_allocate(ctx_dg%src(j)%weight(i_sol,nface_pw+k),&
                                  n1,ctx_err)
              ctx_dg%src(j)%weight(i_sol,nface_pw+k)%array =             &
                                    ctx_src_dirac(j)%weight(i_sol,k)%array
            end do
          end if
          
          if(allocated(ctx_dg%src(j)%hdg_S(k)%array)) then
            n1=size(ctx_dg%src(j)%hdg_S(k)%array)
            do i_sol = 1,ctx_dg%dim_sol
              call array_allocate(ctx_dg%src(j)%hdg_S(nface_pw+k),n1,   &
                                  ctx_err)
              ctx_dg%src(j)%hdg_S(nface_pw+k)%array =                   &
                                           ctx_src_dirac(j)%hdg_S(k)%array
            end do
          end if
          
        end do
      end if
    end do
    ! -----------------------------------------------------------------
    deallocate(system_rhs         )
    deallocate(system_sol         )
    deallocate(quad_3dposition_gb )
    deallocate(quad_3dposition_loc)

    return

  end subroutine dg_source_init_analytic_elastic

end module m_dg_lagrange_source_planewave
