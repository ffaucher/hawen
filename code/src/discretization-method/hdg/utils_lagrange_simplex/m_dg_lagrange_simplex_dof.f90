!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_simplex_dof.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for simplex mesh to deal with the d.o.f.
!> - interface dof numbering (for hdg)
!> - volume dof numbering (for visualization)
!> - volume dof positions (for visualization)
!
!------------------------------------------------------------------------------
module m_dg_lagrange_simplex_dof

  use m_define_precision,     only : IKIND_MESH, RKIND_MESH
  use m_raise_error,          only : raise_error, t_error
  implicit none


  private 
  public  :: lagrange_simplex_face_dof_index, lagrange_simplex_dof_coo, &
             lagrange_simplex_paraview_connectivity
  
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the numbering information for interface between elements
  !> depending on the order and dimension
  !
  !> @param[in]    dim_domain      : domain dimension
  !> @param[in]    orientation     : orientation
  !> @param[in]    order           : order
  !> @param[in]    n_dof_face      : ndof on the interfance
  !> @param[inout] idof_face_ind   : index array
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine lagrange_simplex_face_dof_index(dim_domain,orientation,    &
                                             order,n_dof_face,          &
                                             idof_face_ind,ctx_err)
    integer                ,intent(in)   :: dim_domain, orientation
    integer                ,intent(in)   :: n_dof_face, order
    integer                ,intent(inout):: idof_face_ind(:)
    type(t_error)          ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    idof_face_ind = 0
    !! ORDER 0 Is Prohibited because there would not be dof for the face
    if(order<=0) then
        ctx_err%msg   = "** ERROR: Order cannot be <= 0 for HDG face "//&
                        "[lagrange_simplex_face_dof_index] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if
        
    !! the interface has dimension (dim_domain-1)
    select case (dim_domain)
      case(1)
        idof_face_ind = 1 !! nothing special in 1D, only one node
      case(2)
        call face_dof_1d_numbering(orientation,n_dof_face,idof_face_ind,ctx_err)

      case(3)
        call face_dof_2d_numbering(orientation,order,n_dof_face,        &
                                   idof_face_ind,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "// &
                        "[lagrange_simplex_face_dof_index] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select   
    
    return
  end subroutine lagrange_simplex_face_dof_index

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Compute the coordinates of the dof
  !> depending on the order and dimension
  !
  !> @param[in]    dim_domain      : domain dimension
  !> @param[in]    xnode           : cell node coordinates 
  !> @param[in]    order           : order
  !> @param[in]    n_dof           : ndof on the volume
  !> @param[inout] dof_coo         : coordinates of the dof
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine lagrange_simplex_dof_coo(dim_domain,xnode,order,n_dof,     &
                                      dof_coo,ctx_err)
    
    integer                 ,intent(in)   :: dim_domain
    real(kind=8),allocatable,intent(in)   :: xnode(:,:)
    integer                 ,intent(in)   :: n_dof, order
    real(kind=8),allocatable,intent(inout):: dof_coo(:,:)
    type(t_error)           ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    dof_coo = 0
    !! ORDER 0 Is Prohibited because there would not be dof for the face
    if(order<=0) then
        ctx_err%msg   = "** ERROR: Order cannot be <= 0 for HDG face "//&
                        "[lagrange_simplex_dof_coo] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if
        
    !! the coordinates of the dof on the simplex
    select case (dim_domain)
      case(1)
        call coordinates_dof_1d(xnode,order,n_dof,dof_coo,ctx_err)
      case(2)
        call coordinates_dof_2d(xnode,order,n_dof,dof_coo,ctx_err)
      case(3)
        call coordinates_dof_3d(xnode,order,n_dof,dof_coo,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "// &
                        "[lagrange_simplex_dof_coo] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select   
    
    return
  end subroutine lagrange_simplex_dof_coo

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Compute the paraview connectivity for high order element
  !> depending on the order and dimension
  !
  !> @param[in]    dim_domain      : domain dimension
  !> @param[in]    order           : order
  !> @param[inout] connectivity    : paraview connectivity
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine lagrange_simplex_paraview_connectivity(dim_domain,order,   &
                                                    connectivity,ctx_err)
    
    integer                               ,intent(in)   :: dim_domain
    integer                               ,intent(in)   :: order
    integer(kind=IKIND_MESH), allocatable ,intent(inout):: connectivity(:)
    type(t_error)                         ,intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH), allocatable :: map_main2d(:,:)
    integer(kind=IKIND_MESH), allocatable :: map_paraview2d(:,:)
    integer(kind=IKIND_MESH), allocatable :: map_main3d(:,:,:)
    integer(kind=IKIND_MESH), allocatable :: map_paraview3d(:,:,:)
    integer                  :: i,j,k,ndof_line
    
    ctx_err%ierr  = 0

    connectivity = 0
       
    !! the interface has dimension (dim_domain-1)
    select case (dim_domain)
      case(2)
        ndof_line = order-1+2
        allocate(map_main2d    (ndof_line,ndof_line))
        allocate(map_paraview2d(ndof_line,ndof_line))
        map_main2d    =0
        map_paraview2d=0
        call map_2D_connectivity_dof_main    (order,map_main2d,ctx_err)
        call map_2D_connectivity_dof_paraview(order,map_paraview2d,ctx_err)
        do i=1,ndof_line ; do j=1,ndof_line-i+1
          connectivity(map_paraview2d(i,j)) = map_main2d(i,j)
        enddo ; end do
        deallocate(map_main2d    )
        deallocate(map_paraview2d)
      
      case(3)
        ndof_line = order-1+2
        allocate(map_main3d    (ndof_line,ndof_line,ndof_line))
        allocate(map_paraview3d(ndof_line,ndof_line,ndof_line))
        map_main3d    =0
        map_paraview3d=0
        call map_3D_connectivity_dof('main',    order,map_main3d,    ctx_err)
        call map_3D_connectivity_dof('paraview',order,map_paraview3d,ctx_err)
        do i=1,ndof_line ; do j=1,ndof_line-i+1 ; do k=1,ndof_line-i-j+2
          connectivity(map_paraview3d(i,j,k)) = map_main3d(i,j,k)
        enddo ; end do ; end do
        deallocate(map_main3d    )
        deallocate(map_paraview3d)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "// &
                        "[lagrange_simplex_paraview_connectivity] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select   
    
    return
  end subroutine lagrange_simplex_paraview_connectivity
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the numbering information for a 1D line face
  !> i.e. between 2D elements.
  !> depending on the order, we create a indexing array to have the local 
  !> dof. the number of possibilities depends on the dimension of the 
  !> face and the geometry of elements.
  !>
  !>   - example 1: for 2D triangle mesh; the interface is a segment,
  !>                there are two possible combinations to ensure that
  !>                the normal of each triangle that share a segment
  !>                is outward: at order 2, we have 3 dof per face (line)
  !>                1          3          2   
  !>                x----------x----------x  INTERFACE
  !>                2          3          1   
  !>
  !
  !> @param[in]    orientation     : orientation
  !> @param[in]    n_dof           : ndof on the interfance
  !> @param[inout] idof_face_ind   : index array
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine face_dof_1d_numbering(orientation,n_dof,idof_face_ind,ctx_err)
    integer                ,intent(in)   :: orientation
    integer                ,intent(in)   :: n_dof
    integer                ,intent(inout):: idof_face_ind(:)
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    integer                              :: l

    ctx_err%ierr  = 0

    select case (orientation)     
      !! Consecutive  numbering
      case(1) 
        idof_face_ind(1)    =1  
        idof_face_ind(n_dof)=2
        do l=2,n_dof-1
          idof_face_ind(l)         = l+1
        end do
      
      !! Backward  numbering
      case(2) 
        idof_face_ind(1)    =2
        idof_face_ind(n_dof)=1
        do l=2,n_dof-1
          idof_face_ind(n_dof-l+1) = l+1
        end do
      
      case default
        ctx_err%msg   = "** ERROR: Unrecognized face orientation "// &
                        "[face_dof_1d_numbering] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    return
  end subroutine face_dof_1d_numbering
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the numbering information for a 2D triangle interface
  !> i.e. between 3D elements.
  !> depending on the order, we create a indexing array to have the local 
  !> dof. the number of possibilities depends on the dimension of the 
  !> face and the geometry of elements.
  !>   - example 2: for 3D tetrahedral mesh; the interface is a triangle,
  !>                there are four possible combinations to ensure that
  !>                the outward normal on both side: 
  !>                - we fixe one of the two neighbors  (1 case)
  !>                - for the other to have outward normal, one of the
  !>                  three line of the interface (i.e. triangle) must
  !>                  be reverted; i.e. 3 more cases.
  !>                There is a total of 4 cases.
  !! > ORDER1 >>
  !>  3               2            3            1
  !>  | \             | \          | \          | \
  !>  |  \            |  \         |  \         |  \
  !>  1---2           1---3        2---1        3---2
  !>  main         invert[2-3]  invert[1-2]  invert[1-3]
  !>
  !! > ORDER3 >>
  !>  3                 2               3               1
  !>  | \               | \             | \             | \
  !>  8   7             5   6           7   8           9   4
  !>  |     \           |     \         |     \         |     \
  !>  9  10   6         4  10   7       6  10   9       8  10   5
  !>  |         \       |         \     |         \     |         \
  !>  1---4---5---2     1---9---8---3   2---5---4---1   3---7---6---2 
  !>     main          invert[2-3]     invert[1-2]     invert[1-3]
  !>
  !> For higher order, careful with interior node ordering as well...
  !>
  !
  !> @param[in]    orientation     : orientation
  !> @param[in]    order           : order
  !> @param[in]    n_dof           : ndof on the interfance
  !> @param[inout] idof_face_ind   : index array
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine face_dof_2d_numbering(orientation,order,n_dof,idof_face_ind,ctx_err)
    integer                ,intent(in)   :: orientation
    integer                ,intent(in)   :: n_dof,order
    integer                ,intent(inout):: idof_face_ind(:)
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    integer     :: edge_list(3,2),ndof_in_edge,i_edge,l,idof_offset
    integer     :: dofcount_edge(3),dofind_edge(3),edge_orient(3)
    integer     :: n_edge = 3 !! TRIANGLE

    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! first define the orientation correspondance
    select case (orientation)
      case(1) 
        edge_list(1,:)= (/ 1, 2/)
        edge_list(2,:)= (/ 2, 3/)
        edge_list(3,:)= (/ 3, 1/)
        edge_orient(1:3) = (/ 1, 1, 1 /)
      case(2) !! Invert nodes [2-3]
        edge_list(1,:)= (/ 1, 3/)
        edge_list(2,:)= (/ 3, 2/)
        edge_list(3,:)= (/ 2, 1/)
        edge_orient(1:3) = (/-1,-1,-1 /)
      case(3) !! Invert nodes [1-2]
        edge_list(1,:)= (/ 2, 1/)
        edge_list(2,:)= (/ 1, 3/)
        edge_list(3,:)= (/ 3, 2/)
        edge_orient(1:3) = (/-1,-1,-1 /)
      case(4) !! Invert nodes [1-3]
        edge_list(1,:)= (/ 3, 2/)
        edge_list(2,:)= (/ 2, 1/)
        edge_list(3,:)= (/ 1, 3/)
        edge_orient(1:3) = (/-1,-1,-1 /)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized face orientation "// &
                        "[face_dof_2d_numbering] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! -----------------------------------------------------------------

    !! first the triangle nodes, according to orientation
    do i_edge=1,n_edge
      idof_face_ind(i_edge) = edge_list(i_edge,1)
    end do
    
    !! count dof in edge
    ndof_in_edge = order-1 !! exclude corners dof

    !! for the inner edge dof, we define (same for inside dof later)
    !!   (o) dofcount_edge(k) is the first dof number inside edge k
    !!   (o) dofind_edge  (k) is the dof index inside edge k
    do i_edge=1,n_edge
      dofind_edge(i_edge) = n_edge + ndof_in_edge*(i_edge-1) + 1
      if(edge_orient(i_edge) == 1)  then
        dofcount_edge(i_edge) = n_edge+ndof_in_edge*(edge_list(i_edge,1)-1)+1
      else 
        dofcount_edge(i_edge) = n_edge+ndof_in_edge*(edge_list(i_edge,2)) 
      end if
    end do
    
    
    !! -----------------------------------------------------------------
    !! loop over inner sub-triangles: we do the inner edge dof and next
    !! corners
    idof_offset=0
    do while (ndof_in_edge > 0)
      
      do i_edge=1,n_edge
        do l=0, ndof_in_edge-1
          idof_face_ind(dofind_edge(i_edge) + l) = dofcount_edge(i_edge)&
                                                 + l*edge_orient(i_edge)
        end do
      end do
      
      !! check for interior nodes: this is pretty much the same story
      !! Example: ORDER 5 has 6 interior dof such that:
      !!  20               18              20              16
      !!  | \              | \             | \             | \          
      !!  |   \            |   \           |   \           |   \        
      !! 21     19        17     19       19     21       21     17     
      !!  |       \        |       \       |       \       |       \    
      !!  |         \      |         \     |         \     |         \  
      !! 16----17----18   16----21----20  18----17----16  20----19----18
      !!     main          invert[2-3]     invert[1-2]     invert[1-3]
      !!
      !! ORDER 6
      !!  25                22 
      !!  | \               | \          
      !! 26   24           21   23
      !!  |     \           |     \      
      !! 27  28  23        20  28  24    
      !!  |         \       |         \  
      !! 19--20--21--22    19--27--26--25
      !!     main           invert[2-3]
      !!
      !! ORDER 7 remains 15, ie triangle order 4
      !!    x  30
      !!    | \
      !! 31 x  x  29
      !!    |   \
      !! 32 x  *  x  28
      !!    |      \
      !! 33 x  * *   x 27
      !!    |          \
      !!    x--x--x--x--x 
      !!    22 23 24 25 26
      !! ---------------------------------------------------------------
      !! what has already been done
      idof_offset = idof_offset + n_edge + (ndof_in_edge)*n_edge
      
      ndof_in_edge = ndof_in_edge - 1 !! WE INCLUDE CORNERS

      select case(ndof_in_edge)
        case(0)
          !! We are good  

        case(1)
          !! if one remaining, then it is the last one
          idof_face_ind(n_dof) = n_dof
          ndof_in_edge = 0    
        
        case default
          !! otherwize we get the next corners infos and adapt dofind/dofcount
          ndof_in_edge = ndof_in_edge - 2 !! WE EXCLUDE CORNERS
          do i_edge=1,n_edge
            !! Corner values and index first
            dofcount_edge(i_edge) = idof_offset +                         &
                                    !! local index
                                    ndof_in_edge*(edge_list(i_edge,1)-1) +&
                                    !! already done corners
                                    edge_list(i_edge,1)
            dofind_edge(i_edge)   = idof_offset +                         &
                                    !! local index
                                    ndof_in_edge*(i_edge-1)              +&
                                    !! already done corners
                                    i_edge
            idof_face_ind(dofind_edge(i_edge)) = dofcount_edge(i_edge)
            !! ---------------------------------------------------------
            
            !! readjust for inside edge numbering
            dofind_edge(i_edge) = dofind_edge(i_edge) + 1
            
            if(edge_orient(i_edge) == 1)  then
              dofcount_edge(i_edge) = dofcount_edge(i_edge) + 1
            else 
              dofcount_edge(i_edge) = idof_offset +                       &
                                      ndof_in_edge*(edge_list(i_edge,2)) +&
                                      edge_list(i_edge,2)
            end if
          end do

      end select
    end do

    return
  end subroutine face_dof_2d_numbering


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the local position of the dof (with 
  !>  respect to the current cell) for a given order in 1 dimension: 
  !>  for a segment.
  !>
  !>
  !> @param[in]    xnode           : triangle node position
  !> @param[in]    order           : polynomials order
  !> @param[in]    ndof            : number of dof in the cell
  !> @param[out]   xdof            : all dof positions
  !> @param[out]   ctx_err         : error context
  !
  !----------------------------------------------------------------------------
  subroutine coordinates_dof_1d(xnode,order,ndof,xdof,ctx_err)
  
    implicit none
    real(kind=8)              , intent(in)    :: xnode(1,2)  !! 1coo, 2nodes
    integer                   , intent(in)    :: order,ndof
    real(kind=8),allocatable  , intent(inout) :: xdof (:,:)
    type(t_error)             , intent(out)   :: ctx_err
    !! local
    real(kind=8)                          :: xstep
    integer                               :: i

    ctx_err%ierr=0

    !! check consistency of the given number of dof and order
    !! 1D triangle ndof given by [(o+1)]
    if( order <= 0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: order is <= 0 [mesh_dof_position_1d] **"
    end if
    if( (order+1) .ne. ndof) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: inconsistent number of dof given" // &
                   " [mesh_dof_position_1d] **"
    end if
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------
    xdof  = 0.0d0
    xstep = abs(xnode(1,2) - xnode(1,1)) / (ndof-1)
    
    xdof(1,1   ) = xnode(1,1)
    xdof(1,ndof) = xnode(1,2)
    if(xnode(1,2) > xnode(1,1)) then
      do i=2,ndof-1
        xdof(:,i) = xnode(1,1) + (i-1)*xstep
      end do
    else
      do i=ndof-1,2,-1
        xdof(:,i) = xnode(1,2) + (i-1)*xstep
      end do
    end if

    return
  end subroutine coordinates_dof_1d

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the local position of the dof (with 
  !>  respect to the current cell) for a given order in 2 dimensions: 
  !>  for a triangle.
  !>
  !>
  !> @param[in]    xnode           : triangle node position
  !> @param[in]    order           : polynomials order
  !> @param[in]    ndof            : number of dof in the cell
  !> @param[out]   xdof            : all dof positions
  !> @param[out]   ctx_err         : error context
  !
  !----------------------------------------------------------------------------
  subroutine coordinates_dof_2d(xnode,order,ndof,xdof,ctx_err)
  
    implicit none
    real(kind=8)              , intent(in)    :: xnode(2,3)  !! 2coo, 3nodes
    integer                   , intent(in)    :: order,ndof
    real(kind=8),allocatable  , intent(inout) :: xdof (:,:)
    type(t_error)             , intent(out)   :: ctx_err
    !! local
    real(kind=8)                          :: hstep(2,3),xref,zref
    real(kind=8)             ,allocatable :: map_coo(:,:,:)
    real(kind=8)                          :: xmin,zmin,xmax,zmax
    integer(kind=IKIND_MESH) ,allocatable :: map_connexion(:,:)
    integer                               :: ndof_line,i,j

    ctx_err%ierr=0

    !! check consistency of the given number of dof and order
    !! 2D triangle ndof given by [(o+2)*(o+1)/2]
    if( order <= 0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: order is <= 0 [mesh_dof_position_2d] **"
    end if
    if( (order+2)*(order+1)/2 .ne. ndof) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: inconsistent number of dof given" // &
                   " [mesh_dof_position_2d] **"
    end if
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------
    xdof = 0.0d0

    !! We use a map for the computation ofthe position, such that
    !! we compute the coordinates of all "node", careful that the
    !! map corresponding to the element is not straight...
    !!
    !! X   O   O   O    i=4
    !! | \ |   |   |
    !! X---X   O   O    i=3
    !! | \ | \ |   |
    !! X---X---X   O    i=2
    !! | \ | \ | \ |
    !! X---X---X---X    i=1
    !!
    ! j=1 j=2 j=3 j=4
    !!
    !! Node 1 is in (i=1,j=1), 2 in (i=1,j=end), 3 in (i=end,j=1)
    
    !! compuation of the step size for all edges
    xmin = minval(xnode(1,:))
    zmin = minval(xnode(2,:))
    xmax = maxval(xnode(1,:))
    zmax = maxval(xnode(2,:))
    !! in x/z between (1--2)
    hstep(1,1) = xnode(1,2) - xnode(1,1)
    hstep(2,1) = xnode(2,2) - xnode(2,1)
    !! in x/z between (2--3)
    hstep(1,2) = xnode(1,3) - xnode(1,2)
    hstep(2,2) = xnode(2,3) - xnode(2,2)
    !! in x/z between (3--1)
    hstep(1,3) = xnode(1,1) - xnode(1,3)
    hstep(2,3) = xnode(2,1) - xnode(2,3)

    !! we have order-1 dof inside the main edge (i.e. excluding the nodes)
    !! so + 2 with the corners
    ndof_line = order-1+2
    !! the step is the interval between two nodes cf. Figure above
    hstep   = hstep / dble(ndof_line-1)

    allocate(map_coo(ndof_line,ndof_line,2))
    map_coo = 0.0d0
    !! lower left is node 1
    xref = xnode(1,1)
    zref = xnode(2,1)
    do i=1,ndof_line
      do j=1,ndof_line-i+1
        map_coo(i,j,1) = xref + hstep(1,1)*(j-1)
        map_coo(i,j,2) = zref + hstep(2,1)*(j-1)
        ! avoid round-off error
        if(map_coo(i,j,1) < xmin) map_coo(i,j,1) = xmin
        if(map_coo(i,j,2) < zmin) map_coo(i,j,2) = zmin
        if(map_coo(i,j,1) > xmax) map_coo(i,j,1) = xmax
        if(map_coo(i,j,2) > zmax) map_coo(i,j,2) = zmax
      end do 
      xref = xref - hstep(1,3)
      zref = zref - hstep(2,3)
    end do
    !! make sure to use exactly the node for the limit to avoid roundoff
    map_coo(1,ndof_line,1) = xnode(1,2)
    map_coo(1,ndof_line,2) = xnode(2,2)
    map_coo(ndof_line,1,1) = xnode(1,3)
    map_coo(ndof_line,1,2) = xnode(2,3)

    !! need to convert map(i,j) with the volume indexing
    !! In 2D we refer to the drawingmade for the subroutine
    !! FACE_DOF_2D_NUMBERING
    allocate(map_connexion(ndof_line,ndof_line))
    call map_2D_connectivity_dof_main(order,map_connexion,ctx_err)

    !! give appropriate readings
    do i=1,ndof_line ; do j=1,ndof_line-i+1
      xdof(:,map_connexion(i,j)) = map_coo(i,j,:)
    enddo ; enddo

    deallocate(map_coo)
    deallocate(map_connexion)

    return
  end subroutine coordinates_dof_2d

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the local position of the dof (with respect
  !>  to the current cell) for a given order in 3 dimensions: for a 
  !>  tetrahedron.
  !>
  !>
  !> @param[in]    xnode           : tetra node position
  !> @param[in]    order           : polynomials order
  !> @param[in]    ndof            : number of dof in the cell
  !> @param[out]   xdof            : all dof positions
  !> @param[out]   ctx_err         : error context
  !
  !----------------------------------------------------------------------------
  subroutine coordinates_dof_3d(xnode,order,ndof,xdof,ctx_err)
  
    implicit none
    real(kind=8)              , intent(in)    :: xnode(3,4)  !! 3coo, 4nodes
    integer                   , intent(in)    :: order,ndof
    real(kind=8),allocatable  , intent(inout) :: xdof (:,:)
    type(t_error)             , intent(out)   :: ctx_err
    !! local
    real(kind=8)                          :: hstep_i(3),hstep_j(3),hstep_k(3)
    real(kind=8)                          :: xref,yref,zref
    real(kind=8)                          :: xmin,ymin,zmin,xmax,ymax,zmax
    real(kind=8)             ,allocatable :: map_coo      (:,:,:,:)
    integer(kind=IKIND_MESH) ,allocatable :: map_connexion(:,:,:)
    integer                               :: ndof_line,i,j,k

    ctx_err%ierr=0

    !! check consistency of the given number of dof and order
    !! 3D triangle ndof given by [(o+3)*(o+2)*(o+1)/6]
    if( order <= 0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: order is <= 0 [mesh_dof_position_3d] **"
    end if
    if( (order+3)*(order+2)*(order+1)/6 .ne. ndof) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: inconsistent number of dof given" // &
                   " [mesh_dof_position_3d] **"
    end if
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------
    xdof = 0.0d0

    !! We use a map for the computation of the position, such that
    !! we compute the coordinates of all "node", careful that the
    !! map corresponding to the element is not straight...
    !!
    !! X   O   O   O    i=4
    !! | \ |   |   |
    !! X---X   O   O    i=3
    !! | \ | \ |   |               In Depth We Have The Z Dimension Too.
    !! X---X---X   O    i=2
    !! | \ | \ | \ |               y î / z
    !! X---X---X---X    i=1          |/
    !!                               ----> x
    ! j=1 j=2 j=3 j=4
    !!
    !! THERE IS ALSO THE Z DIMENSIONS: (i,j,k)
    !! -----------------------------------------------------------------
    xmin = minval(xnode(1,:))
    ymin = minval(xnode(2,:))
    zmin = minval(xnode(3,:))
    xmax = maxval(xnode(1,:))
    ymax = maxval(xnode(2,:))
    zmax = maxval(xnode(3,:))
    
    !! the step along j is given by edge [1-2]
    hstep_j(1) = xnode(1,2) - xnode(1,1)
    hstep_j(2) = xnode(2,2) - xnode(2,1)
    hstep_j(3) = xnode(3,2) - xnode(3,1)
    !! the step along i is given by edge [1-3]
    hstep_i(1) = xnode(1,3) - xnode(1,1)
    hstep_i(2) = xnode(2,3) - xnode(2,1)
    hstep_i(3) = xnode(3,3) - xnode(3,1)
    !! the step along k is given by edge [1-4]
    hstep_k(1) = xnode(1,4) - xnode(1,1)
    hstep_k(2) = xnode(2,4) - xnode(2,1)
    hstep_k(3) = xnode(3,4) - xnode(3,1)
    !! -----------------------------------------------------------------
   
    !! we have order-1 dof inside the edge 
    !! (i.e. excluding the nodes) so + 2 with the corners
    !! order-1+2 = order + 1
    ndof_line = order + 1
    !! the step is the interval between two nodes cf. Figure above
    hstep_i   = hstep_i / (dble(ndof_line)-1.d0)
    hstep_j   = hstep_j / (dble(ndof_line)-1.d0)
    hstep_k   = hstep_k / (dble(ndof_line)-1.d0)
    
    !! 3D map, the last indicate the coordinates (x,y,z)
    allocate(map_coo(ndof_line,ndof_line,ndof_line,3))
    map_coo = 0.0d0
    !! lower left is node 1
    xref = xnode(1,1)
    yref = xnode(2,1)
    zref = xnode(3,1)

    do i=1,ndof_line
      do j=1,ndof_line-i+1
        do k=1,ndof_line-j-i+2
          !! change depth
          map_coo(i,j,k,1) = xref + hstep_k(1)*(k-1)
          map_coo(i,j,k,2) = yref + hstep_k(2)*(k-1)
          map_coo(i,j,k,3) = zref + hstep_k(3)*(k-1)
          !! avoid round-off error
          if(map_coo(i,j,k,1) < xmin) map_coo(i,j,k,1) = xmin
          if(map_coo(i,j,k,2) < ymin) map_coo(i,j,k,2) = ymin
          if(map_coo(i,j,k,3) < zmin) map_coo(i,j,k,3) = zmin
          if(map_coo(i,j,k,1) > xmax) map_coo(i,j,k,1) = xmax
          if(map_coo(i,j,k,2) > ymax) map_coo(i,j,k,2) = ymax
          if(map_coo(i,j,k,3) > zmax) map_coo(i,j,k,3) = zmax

        end do
        !! change col
        xref = xref + hstep_j(1)
        yref = yref + hstep_j(2)
        zref = zref + hstep_j(3)
      end do 
      !! change line
      xref = xnode(1,1) + hstep_i(1)*dble(i)
      yref = xnode(2,1) + hstep_i(2)*dble(i)
      zref = xnode(3,1) + hstep_i(3)*dble(i)
    end do
    !! make sure to use exactly the node for the limit to avoid roundoff
    map_coo(1,1,1,1)         = xnode(1,1)
    map_coo(1,1,1,2)         = xnode(2,1)
    map_coo(1,1,1,3)         = xnode(3,1)
    !!
    map_coo(1,ndof_line,1,1) = xnode(1,2)
    map_coo(1,ndof_line,1,2) = xnode(2,2)
    map_coo(1,ndof_line,1,3) = xnode(3,2)
    !! 
    map_coo(ndof_line,1,1,1) = xnode(1,3)
    map_coo(ndof_line,1,1,2) = xnode(2,3)
    map_coo(ndof_line,1,1,3) = xnode(3,3)
    !! 
    map_coo(1,1,ndof_line,1) = xnode(1,4)
    map_coo(1,1,ndof_line,2) = xnode(2,4)
    map_coo(1,1,ndof_line,3) = xnode(3,4)

    !! need to convert map(i,j,k) with the volume indexing
    allocate(map_connexion(ndof_line,ndof_line,ndof_line))
    
    call map_3D_connectivity_dof('main',order,map_connexion,ctx_err)
    !! give appropriate readings
    do i=1,ndof_line ; do j=1,ndof_line-i+1 ; do k=1,ndof_line-j-i+2
      xdof(:,map_connexion(i,j,k)) = map_coo(i,j,k,:)
    enddo ; enddo ; enddo

    deallocate(map_coo)
    deallocate(map_connexion)

    return
  end subroutine coordinates_dof_3d


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the conecting map between cartesian representation 
  !> and local ordering of dof
  !! > ORDER3 >>
  !>  3                 X   0   0   0   i=4
  !>  | \               | \           
  !>  8   7             X   X   0   0   i=3
  !>  |     \           |     \       
  !>  9  10   6         X   X   X   0   i=2
  !>  |         \       |         \   
  !>  1---4---5---2     X---X---X---X   i=1
  !>     main          j=1 j=2 j=3 j=4
  !>
  !>  map(i,j) = index
  !>
  !>
  !
  !> @param[in]    order           : order
  !> @param[inout] map_connexion   : map connexion(i,j) = index
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine map_2D_connectivity_dof_main(order,map_connexion,ctx_err)
    integer                              ,intent(in)   :: order
    integer(kind=IKIND_MESH),allocatable ,intent(inout):: map_connexion(:,:)
    type(t_error)                        ,intent(out)  :: ctx_err
    !! local
    integer                  :: edge_list(3,2),ndof_in_edge,i_edge,l
    integer                  :: ndof_line,idof_offset,iloop
    integer(kind=IKIND_MESH) :: dofcount_edge(3)
    integer, parameter :: n_edge = 3 !! TRIANGLE

    ctx_err%ierr  = 0
    map_connexion = 0

    !! -----------------------------------------------------------------
    !!
    !! IT FOLLOWS FACE_DOF_2D_NUMBERING
    !!
    !! -----------------------------------------------------------------
    !! first define the orientation correspondance
    edge_list(1,:)   = (/ 1, 2/)
    edge_list(2,:)   = (/ 2, 3/)
    edge_list(3,:)   = (/ 3, 1/)
    ! edge_orient(1:3) = (/ 1, 1, 1 /)

    !! first the triangle nodes
    ndof_line = order - 1 + 2 !! cartesian size
    map_connexion(1,1)         = 1
    map_connexion(1,ndof_line) = 2
    map_connexion(ndof_line,1) = 3
    
    !! count dof in edge
    ndof_in_edge = order-1 !! exclude corners dof

    !! for the inner edge dof, we define (same for inside dof later)
    !!   (o) dofcount_edge(k) is the first dof number inside edge k
    do i_edge=1,n_edge
      dofcount_edge(i_edge) = n_edge+ndof_in_edge*(edge_list(i_edge,1)-1)+1
    end do
    
    !! -----------------------------------------------------------------
    iloop      =1
    idof_offset=0
    do while (ndof_in_edge > 0)
      
      !! fill in the inner edge dof
      do l=0, ndof_in_edge-1
        !! [1-2] line
        map_connexion(iloop,iloop+1+l) = dofcount_edge(1) + l
      enddo
      do l=0, ndof_in_edge-1
        !! [2-3] line
        map_connexion(iloop+1+l,iloop+ndof_in_edge-l) = dofcount_edge(2) + l       
      enddo
      do l=0, ndof_in_edge-1
        !! [3-1] line
        map_connexion(iloop+ndof_in_edge-l,iloop)    = dofcount_edge(3) + l
      end do
      
      !! what has already been done
      idof_offset = idof_offset + n_edge + (ndof_in_edge)*n_edge
      
      ndof_in_edge = ndof_in_edge - 1 !! WE INCLUDE CORNERS

      select case(ndof_in_edge)
        case(0)
          !! We are good  

        case(1)
          !! if one remaining, then it is the last one
          map_connexion(iloop+1,iloop+1) = idof_offset + 1
          ndof_in_edge = 0    
        
        case default
          !! next line/col
          iloop = iloop + 1
          !! otherwize we get the next corners infos and adapt dofind/dofcount
          ndof_in_edge = ndof_in_edge - 2 !! WE EXCLUDE CORNERS
          
          !! 1
          map_connexion(iloop,iloop)           = idof_offset + 1
          !! 2 (main has continuing numbering, contrary to paraview)
          map_connexion(iloop,ndof_line-2*(iloop-1)) = idof_offset + 1 +&
                                     !! 1 inner line of dof + 1 corner
                                     ndof_in_edge  + 1
          !! 3 (main has continuing numbering, contrary to paraview)
          map_connexion(ndof_line-2*(iloop-1),iloop) = idof_offset + 1 +&
                                     !! 2 inner line of dof + 2 corner
                                     (2*ndof_in_edge)  + 2

          !! update dofcount
          dofcount_edge(1) = map_connexion(iloop,iloop)+1
          dofcount_edge(2) = map_connexion(iloop,ndof_line-2*(iloop-1))+1
          dofcount_edge(3) = map_connexion(ndof_line-2*(iloop-1),iloop)+1
          
      end select
    end do

    return
  end subroutine map_2D_connectivity_dof_main

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the conecting map between cartesian representation 
  !> and local ordering of dof
  !! > ORDER3 >>
  !>  3                 X   0   0   0   i=4
  !>  | \               | \           
  !>  8   7             X   X   0   0   i=3
  !>  |     \           |     \       
  !>  9  10   6         X   X   X   0   i=2
  !>  |         \       |         \   
  !>  1---4---5---2     X---X---X---X   i=1
  !>     main          j=1 j=2 j=3 j=4
  !>
  !>  map(i,j) = index
  !>
  !> Example: ORDER 5 has 6 interior dof such that:
  !>  20               18            
  !>  | \              | \           
  !>  |   \            |   \         
  !> 21     19        21     20      
  !>  |       \        |       \     
  !>  |         \      |         \   
  !> 16----17----18   16----19----17 
  !>     main          in paraview 
  !>
  !
  !> @param[in]    order           : order
  !> @param[inout] map_connexion   : map connexion(i,j) = index
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine map_2D_connectivity_dof_paraview(order,map_connexion,ctx_err)
    integer                              ,intent(in)   :: order
    integer(kind=IKIND_MESH),allocatable ,intent(inout):: map_connexion(:,:)
    type(t_error)                        ,intent(out)  :: ctx_err
    !! local
    integer            :: edge_list(3,2),ndof_in_edge,i_edge,l
    integer            :: dofcount_edge(3),ndof_line,idof_offset,iloop
    integer, parameter :: n_edge = 3 !! TRIANGLE

    ctx_err%ierr  = 0
    map_connexion = 0

    !! -----------------------------------------------------------------
    !!
    !! IT FOLLOWS FACE_DOF_2D_NUMBERING
    !!
    !! -----------------------------------------------------------------
    !! first define the orientation correspondance
    edge_list(1,:)   = (/ 1, 2/)
    edge_list(2,:)   = (/ 2, 3/)
    edge_list(3,:)   = (/ 3, 1/)
    ! edge_orient(1:3) = (/ 1, 1, 1 /)

    !! first the triangle nodes
    ndof_line = order - 1 + 2 !! cartesian size
    map_connexion(1,1)         = 1
    map_connexion(1,ndof_line) = 2
    map_connexion(ndof_line,1) = 3
    
    !! count dof in edge
    ndof_in_edge = order-1 !! exclude corners dof

    !! for the inner edge dof, we define (same for inside dof later)
    !!   (o) dofcount_edge(k) is the first dof number inside edge k
    do i_edge=1,n_edge
      dofcount_edge(i_edge) = n_edge+ndof_in_edge*(edge_list(i_edge,1)-1)+1
    end do
    
    !! -----------------------------------------------------------------
    iloop      =1
    idof_offset=0
    do while (ndof_in_edge > 0)
      
      !! fill in the inner edge dof
      do l=0, ndof_in_edge-1
        !! [1-2] line
        map_connexion(iloop,iloop+1+l) = dofcount_edge(1) + l
      enddo
      do l=0, ndof_in_edge-1
        !! [2-3] line
        map_connexion(iloop+1+l,iloop+ndof_in_edge-l) = dofcount_edge(2) + l       
      enddo
      do l=0, ndof_in_edge-1
        !! [3-1] line
        map_connexion(iloop+ndof_in_edge-l,iloop)    = dofcount_edge(3) + l
      end do
      
      !! what has already been done
      idof_offset = idof_offset + n_edge + (ndof_in_edge)*n_edge
      
      ndof_in_edge = ndof_in_edge - 1 !! WE INCLUDE CORNERS

      select case(ndof_in_edge)
        case(0)
          !! We are good  

        case(1)
          !! if one remaining, then it is the last one
          map_connexion(iloop+1,iloop+1) = idof_offset + 1
          ndof_in_edge = 0    
        
        case default
          !! next line/col
          iloop = iloop + 1
          !! otherwize we get the next corners infos and adapt dofind/dofcount
          ndof_in_edge = ndof_in_edge - 2 !! WE EXCLUDE CORNERS
          
          !! 1
          map_connexion(iloop,iloop)                 = idof_offset + 1
          !! 2 NOT CONINUOUS 
          map_connexion(iloop,ndof_line-2*(iloop-1)) = idof_offset + 2
          !! 3 NOT CONINUOUS 
          map_connexion(ndof_line-2*(iloop-1),iloop) = idof_offset + 3

          !! update dofcount
          dofcount_edge(1) = idof_offset + 3 +                  1
          dofcount_edge(2) = idof_offset + 3 +   ndof_in_edge + 1
          dofcount_edge(3) = idof_offset + 3 + 2*ndof_in_edge + 1
          
      end select
    end do

    return
  end subroutine map_2D_connectivity_dof_paraview


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the conecting map between cartesian representation 
  !> and local ordering of dof
  !>      We use a map 
  !>
  !> X   O   O   O    i=4
  !> | \ |   |   |
  !> X---X   O   O    i=3
  !> | \ | \ |   |               In Depth We Have The Z Dimension Too.
  !> X---X---X   O    i=2
  !> | \ | \ | \ |               y î / z
  !> X---X---X---X    i=1          |/
  !>                               ----> x
  ! j=1 j=2 j=3 j=4
  !> THERE IS ALSO THE Z DIMENSIONS: (i,j,k)
  !! -------------------------------------------------------------------
  !> in 3D, the workflows is as follows
  !>  (1)  main nodes
  !>  (2a) main face edges: [1-2]
  !>                        [2-3]
  !>                        [3-1]
  !>  (2b) edges to 4:      [1-4]
  !>                        [2-4]
  !>                        [3-4]
  !>  (3)  inner face dof in snail ordering, the face are ordered such 
  !>       that it is in front of the node
  !>       FACE 1 => [2 3 4]
  !>       FACE 2 => [1 3 4]
  !>       FACE 3 => [1 2 4]
  !>       FACE 4 => [1 2 3]
  !>  PARAVIEW NUMBERS AS           FACE 3 -- FACE 1 -- FACE 2 -- FACE 4       
  !>
  !>  (4)  inner tetrahedron uses cartesian coo (i.e. no differenciation
  !>       between the node of the tetra), going with loops in (i,j,k)
  !>       i.e., the numbering goes along the depth first
  !>
  !! -------------------------------------------------------------------
  !  REMARK: the best way to check the numbering for the polynomials 
  !>         is to go to the file m_basis_functions.f90
  !>         in the routine         basis_construct_3d
  !>         starting line 600 and something...
  !! -------------------------------------------------------------------
  !>
  !>
  !>  map(i,j,k) = index
  !>
  !>
  !
  !> @param[in]    str_numbering   : numbering version ('paraview' or 'main')
  !> @param[in]    order           : order
  !> @param[inout] map_connexion   : map connexion(i,j,k) = index
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine map_3D_connectivity_dof(str_numbering,order,map_connexion,ctx_err)
    character(len=*)                     ,intent(in)   :: str_numbering
    integer                              ,intent(in)   :: order
    integer(kind=IKIND_MESH),allocatable ,intent(inout):: map_connexion(:,:,:)
    type(t_error)                        ,intent(out)  :: ctx_err
    !! local
    integer, parameter  :: n_node = 4 !! TETRAHEDRON
    integer, parameter  :: n_edge = 6 !! TETRAHEDRON
    integer, parameter  :: n_face = 4 !! TETRAHEDRON
    integer             :: inode(4),jnode(4),knode(4),iface
    integer             :: ifacenode(4),jfacenode(4),kfacenode(4)
    integer             :: iloc,jloc,kloc,iend,jend,kend,i,j,k,counter
    integer             :: ndof_line,ndof_in_face,node1,node2
    integer             :: ndof_in_edge,offset_in_edge,offset_node
    integer             :: edgeface(4,3,2),edge_list(6,2)
    integer             :: istepin(4),jstepin(4),kstepin(4)
    logical             :: flag_node(4),flag_node1,flag_node2
    logical             :: flag_node_face(4,4)
    character(len=32)   :: str_version

    ctx_err%ierr  = 0
    map_connexion = 0

    !! We define 6 edges for the tetrahedron:
    !! [1-2]  [2-3]  [3-1]  the main face here 
    !! [1-4]  [2-4]  [3-4]  all connexions to node (4)
    edge_list(1,:)   = (/ 1, 2/)
    edge_list(2,:)   = (/ 2, 3/)
    edge_list(3,:)   = (/ 3, 1/)
    edge_list(4,:)   = (/ 1, 4/)
    edge_list(5,:)   = (/ 2, 4/)
    edge_list(6,:)   = (/ 3, 4/)

    ndof_line    = order - 1 + 2 !! cartesian size
    ndof_in_edge = order - 1     !! excluding corners dof
    ndof_in_face = 0
    do k=1,ndof_in_edge-1
      ndof_in_face = ndof_in_face + k
    end do
    !! save (i,j,k) of the main nodes
    inode(1)=1         ; jnode(1)=1         ; knode(1)=1
    inode(2)=1         ; jnode(2)=ndof_line ; knode(2)=1
    inode(3)=ndof_line ; jnode(3)=1         ; knode(3)=1
    inode(4)=1         ; jnode(4)=1         ; knode(4)=ndof_line
       
    !! 1./ Number the main edges >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    str_version='node_first'     !! the nodes are numbered first
    offset_node    =0            !! we start
    offset_in_edge =n_node       !! nodes are first
    flag_node=.true.
    do k=1,n_edge
      node1= edge_list(k,1)
      node2= edge_list(k,2)
      iloc = inode(node1)
      jloc = jnode(node1)
      kloc = knode(node1)
      flag_node1 = flag_node(node1)
      flag_node2 = flag_node(node2)
      call numbering_edge_3D(map_connexion,ndof_line,node1,node2,iloc,  &
                             jloc,kloc,offset_node,offset_in_edge,      &
                             flag_node1,flag_node2,str_version,iend,    &
                             jend,kend,ctx_err)
      flag_node(node1) = .false. 
      flag_node(node2) = .false. 
      offset_in_edge = offset_in_edge + ndof_in_edge
    end do

    !! 2./ Tetrahedron faces >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! for the main ordering, we have: 
    !!     Face 1 is in front of node 1 ==> nodes [2-3-4]
    !!     Face 2 is in front of node 2 ==> nodes [1-3-4]
    !!     Face 3 is in front of node 3 ==> nodes [1-2-4]
    !!     Face 4 is in front of node 4 ==> nodes [1-2-3]
    !! 
    !! for paraview it is different
    !!     Face 1 is nodes [1-2-4]  (Face 3)
    !!     Face 2 is nodes [3-4-2]  (Face 1 reversed)
    !!     Face 3 is nodes [1-4-3]  (Face 2 reversed)
    !!     Face 4 is nodes [1-3-2]  (Face 4 reversed)
    !! -----------------------------------------------------------------
    select case (trim(adjustl(str_numbering)))
      case('main')
        edgeface(1,1,:) = (/2,4/)
        edgeface(1,2,:) = (/4,3/)
        edgeface(1,3,:) = (/3,2/)
        edgeface(2,1,:) = (/1,3/)
        edgeface(2,2,:) = (/3,4/)
        edgeface(2,3,:) = (/4,1/)
        edgeface(3,1,:) = (/1,4/)
        edgeface(3,2,:) = (/4,2/)
        edgeface(3,3,:) = (/2,1/)
        edgeface(4,1,:) = (/1,2/)
        edgeface(4,2,:) = (/2,3/)
        edgeface(4,3,:) = (/3,1/)
        
        !! index of first node in the face
        ifacenode(1) = inode(2)+1
        jfacenode(1) = jnode(2)-2
        kfacenode(1) = knode(2)+1
        ifacenode(2) = inode(1)+1
        jfacenode(2) = jnode(1)
        kfacenode(2) = knode(1)+1
        ifacenode(3) = inode(1)
        jfacenode(3) = jnode(1)+1
        kfacenode(3) = knode(1)+1
        ifacenode(4) = inode(1)+1
        jfacenode(4) = jnode(1)+1
        kfacenode(4) = knode(1)
      
      case('paraview')
        edgeface(1,1,:) = (/1,2/) 
        edgeface(1,2,:) = (/2,4/) 
        edgeface(1,3,:) = (/4,1/) 
        edgeface(2,1,:) = (/3,4/)
        edgeface(2,2,:) = (/4,2/) 
        edgeface(2,3,:) = (/2,3/) 
        edgeface(3,1,:) = (/1,4/) 
        edgeface(3,2,:) = (/4,3/) 
        edgeface(3,3,:) = (/3,1/) 
        edgeface(4,1,:) = (/1,3/) 
        edgeface(4,2,:) = (/3,2/) 
        edgeface(4,3,:) = (/2,1/) 

        ifacenode(2) = inode(3)-2
        jfacenode(2) = jnode(3)+1
        kfacenode(2) = knode(3)+1

        ifacenode(3) = inode(1)+1
        jfacenode(3) = jnode(1)
        kfacenode(3) = knode(1)+1
        ifacenode(1) = inode(1)
        jfacenode(1) = jnode(1)+1
        kfacenode(1) = knode(1)+1
        ifacenode(4) = inode(1)+1
        jfacenode(4) = jnode(1)+1
        kfacenode(4) = knode(1)
      case default
        ctx_err%msg   = "** ERROR: Unknown version in "//&
                        "[map_3D_connectivity_dof] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
!! ---------------------------------------------------------------------
!! ---------------------------------------------------------------------
!! NOT WORKING FOR ORDER > 5, PROBABLY THAT PARAVIEW REQUIRES 
!! node_first NUMBERING AND NON-CARTESIAN FOR THE INTERIOR ?
!! ---------------------------------------------------------------------
!! ---------------------------------------------------------------------
    
    !! this defines how to go further into the face index, depending
    !! on the face
    istepin(1) = +1
    jstepin(1) = -2
    kstepin(1) = +1
    istepin(2) = +1
    jstepin(2) =  0
    kstepin(2) = +1
    istepin(3) = 0
    jstepin(3) = +1
    kstepin(3) = +1
    istepin(4) = +1
    jstepin(4) = +1
    kstepin(4) = 0
   
    ndof_line      = ndof_line  - 3     !! because the external are ok
    ndof_in_edge   = ndof_line  - 2

    select case (trim(adjustl(str_numbering)))
      case('main')
        str_version='consecutive'  !> the nodes are not numbered first
        offset_node    = offset_in_edge
        offset_in_edge = offset_in_edge + 1 !! for the node
        flag_node_face = .true.
      case('paraview') !! this is a guess...
        !! ok for order <= 4
        if(order <= 4) then
          str_version='consecutive'  !> the nodes are not numbered first
          offset_node    = offset_in_edge
          offset_in_edge = offset_in_edge + 1 !! for the node
          flag_node_face = .true.
        else !! this is a guess...
          str_version='node_first'
          offset_node    = offset_in_edge
          offset_in_edge = offset_in_edge + 3 !! for the nodes of the 
                                              !! triangle face only
          flag_node_face = .false. !> we do it ourselves
          if(order > 5) then
            ctx_err%msg   = "** ERROR: Numbering not working for "   // &
                            "paraview [map_3D_connectivity_dof] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
        end if

      case default
        ctx_err%msg   = "** ERROR: Unknown version in "//&
                        "[map_3D_connectivity_dof] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    !! loop over the faces >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do iface=1, n_face
      iloc = ifacenode(iface)
      jloc = jfacenode(iface)
      kloc = kfacenode(iface)

      do while (ndof_line > 0)
        do k=1,3 !! loop over the face edges
          node1      = edgeface(iface,k,1)
          node2      = edgeface(iface,k,2)    
          flag_node1 = flag_node_face(iface,node1)
          flag_node2 = flag_node_face(iface,node2)
          call numbering_edge_3D(map_connexion,ndof_line,node1,node2,iloc,  &
                                 jloc,kloc,offset_node,offset_in_edge,      &
                                 flag_node1,flag_node2,str_version,iend,    &
                                 jend,kend,ctx_err)
          flag_node_face(iface,node1) = .false.
          flag_node_face(iface,node2) = .false.

          !! update the offset, depending on the case
          select case(trim(adjustl(str_version)))
            case('node_first')
              offset_in_edge = offset_in_edge + (ndof_line-2)
              !! take care of nodes
              map_connexion(iloc,jloc,kloc) = offset_node + k
            case('consecutive')
              offset_node    = offset_node + (ndof_line-1)
              offset_in_edge = offset_node + 1
            case default
              ctx_err%msg   = "** ERROR: Unknown version in "//&
                  "[map_3D_connectivity_dof] **"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              call raise_error(ctx_err)
          end select

          !! keep on from ending point
          iloc=iend ; jloc=jend ; kloc=kend
        end do
        !! inside nodes
        ndof_line      = ndof_line  - 3     !! because the external are ok
        !! update local with new inner point, step from ending point
        !! DEPENDS ON THE CURRENT FACE 
        iloc = ifacenode(iface) + istepin(iface)
        jloc = jfacenode(iface) + jstepin(iface)
        kloc = kfacenode(iface) + kstepin(iface)
      end do
      !! reset ndof_line values
      ndof_line = ndof_in_edge + 2
      if(ndof_line == 1) then !! update the offset with the node
        offset_node   =offset_node   +1 !! add the very last node
        offset_in_edge=offset_in_edge+1 !! add the very last node
      else
        !! update offset_node for node_first numbering
        if(trim(adjustl(str_version)) .eq. 'node_first') then
          offset_node    = offset_in_edge
          offset_in_edge = offset_node + 3  !! only face triangle 
        end if
      end if
    end do
    
   !! is there inside node now tetrahedron of order (order-3)
   if(order > 3) then
     select case(order)
       case(4) !! only 1 node remains: easy
         map_connexion(inode(1)+1,jnode(1)+1,knode(1)+1) = offset_node + 1
       ! case(5) !! only 4 node remains: easy
       ! !> tetra numbering would be 
       ! map_connexion(inode(1)+1,jnode(1)+1,knode(1)+1) = offset_node + 1
       ! map_connexion(inode(1)+1,jnode(1)+2,knode(1)+1) = offset_node + 2
       ! map_connexion(inode(1)+2,jnode(1)+1,knode(1)+1) = offset_node + 3
       ! map_connexion(inode(1)+1,jnode(1)+1,knode(1)+2) = offset_node + 4
       
       case default 

         select case (trim(adjustl(str_numbering)))

           case('main')
             !! Cartesian numbering  with depth first 
             counter = 0
             do i=1,order-3 ; do j=1,(order-3-i+1) ; do k=1,(order-3-i-j+2)
               counter = counter + 1 
               map_connexion(inode(1)+i,jnode(1)+j,knode(1)+k) =      &
                                                 offset_node + counter
             end do ; end do ; end do
           
           case('paraview') !! ok for order 5 only
             !! Cartesian numbering  with depth first 
             counter = 0
             do k=1,order-3 ; do i=1,(order-3-k+1) ; do j=1,(order-3-i-k+2)
               counter = counter + 1 
               map_connexion(inode(1)+i,jnode(1)+j,knode(1)+k) =      &
                                                 offset_node + counter
             end do ; end do ; end do
         
           case default
             ctx_err%msg   = "** ERROR: Unknown version in "//&
                             "[map_3D_connectivity_dof] **"
             ctx_err%ierr  = -1
             ctx_err%critic=.true.
             call raise_error(ctx_err)
         end select
     end select
   end if

    return
  end subroutine map_3D_connectivity_dof


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the edge numbering in 3D:
  !> it depends on the two nodes involved
  !
  !! X   O   O   O    i=4
  !! | \ |   |   |
  !! X---X   O   O    i=3
  !! | \ | \ |   |               In Depth We Have The Z Dimension Too.
  !! X---X---X   O    i=2
  !! | \ | \ | \ |               y î / z
  !! X---X---X---X    i=1          |/
  !! j=1 j=2 j=3 j=4                ----> x
  ! 
  !! THERE IS ALSO THE Z DIMENSIONS: (i,j,k)
  !
  !> @param[inout] map_global      : 3D index map (i,j,k) = index
  !> @param[in]    ndof_on_edge    : INCLUDING corners
  !> @param[in]    node1           : edge node 1
  !> @param[in]    node2           : edge node 2
  !> @param[in]    {i,j,k}start    : starting position in 3D map
  !> @param[out]   {i,j,k}end      : ending   position in 3D map
  !> @param[in]    offset_node     : offset for the node numbering
  !> @param[in]    offset_in       : offset for the inner-edge numbering
  !> @param[in]    flag_node{2}    : indicates if we number the nodes
  !> @param[in]    str_version     : numbering version 
  !>                                 ('node_first' or 'consecutive')
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine numbering_edge_3D(map_global,ndof_on_edge,node1,node2,     &
                               istart,jstart,kstart,offset_node,        &
                               offset_in,flag_node1,flag_node2,         &
                               str_version,iend,jend,kend,ctx_err)
    integer(kind=IKIND_MESH),allocatable ,intent(inout):: map_global(:,:,:)
    integer             ,intent(in)   :: ndof_on_edge,node1,node2
    integer             ,intent(in)   :: istart,jstart,kstart
    integer             ,intent(out)  :: iend,jend,kend
    integer             ,intent(in)   :: offset_in,offset_node
    logical             ,intent(in)   :: flag_node1,flag_node2
    character(len=*)    ,intent(in)   :: str_version    
    type(t_error)       ,intent(out)  :: ctx_err
    !!
    integer  :: l

    ctx_err%ierr=0
    
    !! return if nothing
    if(ndof_on_edge <= 0) return
    
    !! do we number the first node -------------------------------------
    if(flag_node1) then
      select case(trim(adjustl(str_version)))
        case('node_first')
          map_global(istart,jstart,kstart) = offset_node + node1
        case('consecutive')
          map_global(istart,jstart,kstart) = offset_node + 1
        case default
          ctx_err%msg   = "** ERROR: Unknown keyword in "//&
                          "[numbering_edge_3D] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if
    iend=istart ; jend=jstart ; kend=kstart
    !! -----------------------------------------------------------------
    
    !! return if only one on the edge
    if(ndof_on_edge <= 1) return
    
    !! -----------------------------------------------------------------
    !! numbering inner edge depends on the edge
    !!      We use a map for the increment:
    !!
    !! X   O   O   O    i=4
    !! | \ |   |   |
    !! X---X   O   O    i=3
    !! | \ | \ |   |               In Depth We Have The Z Dimension Too.
    !! X---X---X   O    i=2
    !! | \ | \ | \ |               y î / z
    !! X---X---X---X    i=1          |/
    !!j=1 j=2 j=3 j=4                ----> x
    ! 
    !! THERE IS ALSO THE Z DIMENSIONS: (i,j,k)
    !! -----------------------------------------------------------------
    if    (node1 == 1 .and. node2 == 2) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart,jstart+l,kstart) = offset_in + l
      end do
      !! final position
      iend=istart
      jend=jstart + ndof_on_edge-2 + 1
      kend=kstart
    
    elseif(node1 == 2 .and. node2 == 1) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart,jstart-l,kstart) = offset_in + l
      end do
      !! final position
      iend=istart
      jend=jstart - (ndof_on_edge-2) - 1
      kend=kstart
    
    elseif(node1 == 1 .and. node2 == 3) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart+l,jstart,kstart) = offset_in + l
      end do
      !! final position
      iend=istart + ndof_on_edge-2 + 1
      jend=jstart
      kend=kstart

    elseif(node1 == 3 .and. node2 == 1) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart-l,jstart,kstart) = offset_in + l
      end do
      iend=istart - (ndof_on_edge-2) - 1
      jend=jstart
      kend=kstart

    elseif(node1 == 1 .and. node2 == 4) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart,jstart,kstart+l) = offset_in + l
      end do
      iend=istart
      jend=jstart
      kend=kstart + ndof_on_edge-2 + 1

    elseif(node1 == 4 .and. node2 == 1) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart,jstart,kstart-l) = offset_in + l
      end do
      iend=istart
      jend=jstart
      kend=kstart - (ndof_on_edge-2) - 1
      
    elseif(node1 == 2 .and. node2 == 3) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart+l,jstart-l,kstart) = offset_in + l
      end do
      iend=istart + (ndof_on_edge-2) + 1
      jend=jstart - (ndof_on_edge-2) - 1
      kend=kstart
            
    elseif(node1 == 3 .and. node2 == 2) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart-l,jstart+l,kstart) = offset_in + l
      end do
      iend=istart - (ndof_on_edge-2) - 1
      jend=jstart + (ndof_on_edge-2) + 1
      kend=kstart

    elseif(node1 == 2 .and. node2 == 4) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart,jstart-l,kstart+l) = offset_in + l
      end do
      iend=istart
      jend=jstart - (ndof_on_edge-2) - 1
      kend=kstart + (ndof_on_edge-2) + 1

    elseif(node1 == 4 .and. node2 == 2) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart,jstart+l,kstart-l) = offset_in + l
      end do
      iend=istart
      jend=jstart + (ndof_on_edge-2) + 1
      kend=kstart - (ndof_on_edge-2) - 1

    elseif(node1 == 3 .and. node2 == 4) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart-l,jstart,kstart+l) = offset_in + l
      end do
      iend=istart - (ndof_on_edge-2) - 1
      jend=jstart
      kend=kstart + (ndof_on_edge-2) + 1

    elseif(node1 == 4 .and. node2 == 3) then
      do l=1,ndof_on_edge-2 !! only the inner ones
        map_global(istart+l,jstart,kstart-l) = offset_in + l
      end do
      iend=istart + (ndof_on_edge-2) + 1
      jend=jstart
      kend=kstart - (ndof_on_edge-2) - 1

    else
      ctx_err%msg   = "** ERROR: Unrecognized node edges in "//&
                      "[numbering_edge_3D] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! do we number the last node -------------------------------------
    if(flag_node2) then
      select case(trim(adjustl(str_version)))
        case('node_first')
          map_global(iend,jend,kend) = offset_node + node2
        case('consecutive')
          map_global(iend,jend,kend) = offset_node + (ndof_on_edge-2) + 2
        case default
          ctx_err%msg   = "** ERROR: Unknown keyword in "//&
                          "[numbering_edge_3D] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if
    !! -----------------------------------------------------------------
    
    
    return
    
  end subroutine numbering_edge_3D
end module m_dg_lagrange_simplex_dof
