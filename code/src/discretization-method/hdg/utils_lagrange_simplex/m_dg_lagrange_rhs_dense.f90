!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_rhs_dense.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the right-hand side for
!> DG discretization using Lagrangian basis function in a dense format.
!> here, we assume that all cells are concerned by the volumic dof, and
!> simply use the HDG matrix Se as input.
!
!------------------------------------------------------------------------------
module m_dg_lagrange_rhs_dense

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: IKIND_MAT,RKIND_MAT,IKIND_MESH
  use m_ctx_parallelism,        only: t_parallelism
  use m_reduce_sum,             only: reduce_sum
  use m_barrier,                only: barrier
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_acquisition,        only: t_acquisition
  use m_ctx_domain,             only: t_domain
  use m_array_types,            only: t_array1d_complex,t_array1d_int
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: dg_rhs_create_lagrange_dense
  public :: dg_rhs_backward_create_lagrange_dense

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> The RHS is updated to symmetrize the matrix. With HDG, having a 
  !> Dirichlet boundary conditions lead to identity sub-block that 
  !> break the original symmetry. We enforce the symmetry by updating
  !> the RHS, i.e., some coefficient of the matrix have been set to 
  !> zero, while some part of rhs are now non-zero.
  !>
  !
  !> @param[in]    n_cell_loc       : number of cells to treat
  !> @param[inout] n_dof_rhs        : number of nnz entries for rhs (updated)
  !> @param[inout] nnz_per_cell     : number of nnz for each cell
  !> @param[inout] nnz_line         : line of the nnz
  !> @param[inout] nnz_col          : col  of the nnz
  !> @param[inout] nnz_val          : entries of the matrix in (line,col)
  !> @param[inout] col_loc          : index  of rhs component
  !> @param[inout] rhs_loc          : value  of rhs component
  !----------------------------------------------------------------------------
  subroutine modify_rhs_for_symmetry(n_cell_loc,global_rhs,n_per_rhs,   &
                                     n_rhs,nnz_per_cell,nnz_line,       &
                                     nnz_col,nnz_val)

    implicit none
    
    integer(kind=IKIND_MESH)            , intent(in)    :: n_cell_loc
    complex(kind=8)                     , intent(inout) :: global_rhs(:)
    integer(kind=IKIND_MAT)             , intent(in)    :: n_per_rhs
    integer                             , intent(in)    :: n_rhs
    integer                , allocatable, intent(in)    :: nnz_per_cell(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_line(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_col(:)
    type(t_array1d_complex), allocatable, intent(in)    :: nnz_val(:)

    !! local
    logical                       :: flag_worker
    integer(kind=IKIND_MESH)      :: c
    integer(kind=IKIND_MAT)       :: ind_c,ind_l
    integer                       :: i_rhs,i_sym_rhs
    complex(kind=8)               :: ref
    complex(kind=8),allocatable   :: global_rhs_update(:)

    !! -----------------------------------------------------------------
    !! need a temporary global array now, only allocated if necessary
    flag_worker = .false.
    do c = 1, n_cell_loc
      if(nnz_per_cell(c) > 0) then
        allocate(global_rhs_update(n_per_rhs*n_rhs))
        global_rhs_update = 0.d0
        flag_worker = .true. 
        exit
      end if
    end do
    !! -----------------------------------------------------------------

    do c = 1, n_cell_loc
      if(nnz_per_cell(c) > 0) then

        !! we update the rhs with additional values, 
        !! here we work with a global rhs.
        do i_sym_rhs = 1, nnz_per_cell(c)
          if(nnz_col(c)%array(i_sym_rhs) > 0) then
           
             !! loop over all rhs:
             do i_rhs = 1, n_rhs              
               !! get the ref value from the already existing rhs. 
               ind_c = (i_rhs-1)*n_per_rhs + nnz_col (c)%array(i_sym_rhs)
               ind_l = (i_rhs-1)*n_per_rhs + nnz_line(c)%array(i_sym_rhs)

               ref = global_rhs(ind_c)
               !! add new values if ref is non-zero -----------------------
               !! minus here.
               global_rhs_update(ind_l) = global_rhs_update(ind_l)      &
                                         - dcmplx(ref*nnz_val(c)%array(i_sym_rhs))
  
             end do !! end loop over rhs

          end if !! end if there is a nnz

        end do !! end loop over nnz

      end if !! end if we have nnz

    end do !! end loop over cells
    
    if(flag_worker) then
      !! update rhs
      global_rhs(:) = global_rhs(:) + global_rhs_update(:)
      deallocate(global_rhs_update)
    end if

    return

  end subroutine modify_rhs_for_symmetry

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> The RHS is updated to symmetrize the matrix. With HDG, having a 
  !> Dirichlet boundary conditions lead to identity sub-block that 
  !> break the original symmetry. We enforce the symmetry by updating
  !> the RHS, i.e., some coefficient of the matrix have been set to 
  !> zero, while some part of rhs are now non-zero.
  !>
  !
  !> @param[in]    n_cell_loc       : number of cells to treat
  !> @param[inout] n_dof_rhs        : number of nnz entries for rhs (updated)
  !> @param[inout] nnz_per_cell     : number of nnz for each cell
  !> @param[inout] nnz_line         : line of the nnz
  !> @param[inout] nnz_col          : col  of the nnz
  !> @param[inout] nnz_val          : entries of the matrix in (line,col)
  !> @param[inout] col_loc          : index  of rhs component
  !> @param[inout] rhs_loc          : value  of rhs component
  !----------------------------------------------------------------------------
  subroutine modify_rhs_for_symmetry_adjoint(n_cell_loc,global_rhs,     &
                                             n_per_rhs,n_rhs,           &
                                             nnz_per_cell,nnz_line,     &
                                             nnz_col,nnz_val)

    implicit none
    
    integer(kind=IKIND_MESH)            , intent(in)    :: n_cell_loc
    complex(kind=8)                     , intent(inout) :: global_rhs(:)
    integer(kind=IKIND_MAT)             , intent(in)    :: n_per_rhs
    integer                             , intent(in)    :: n_rhs
    integer                , allocatable, intent(in)    :: nnz_per_cell(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_line(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_col(:)
    type(t_array1d_complex), allocatable, intent(in)    :: nnz_val(:)

    !! local
    logical                       :: flag_worker
    integer(kind=IKIND_MESH)      :: c
    integer(kind=IKIND_MAT)       :: ind_c,ind_l
    integer                       :: i_rhs,i_sym_rhs
    complex(kind=8)               :: ref
    complex(kind=8),allocatable   :: global_rhs_update(:)

    !! -----------------------------------------------------------------
    !! need a temporary global array now, only allocated if necessary
    flag_worker = .false.
    do c = 1, n_cell_loc
      if(nnz_per_cell(c) > 0) then
        allocate(global_rhs_update(n_per_rhs*n_rhs))
        global_rhs_update = 0.d0
        flag_worker = .true. 
        exit
      end if
    end do
    !! -----------------------------------------------------------------

    do c = 1, n_cell_loc
      if(nnz_per_cell(c) > 0) then

        !! we update the rhs with additional values, 
        !! here we work with a global rhs.
        do i_sym_rhs = 1, nnz_per_cell(c)
          if(nnz_col(c)%array(i_sym_rhs) > 0) then
           
             !! loop over all rhs:
             do i_rhs = 1, n_rhs              
               !! get the ref value from the already existing rhs. 
               ind_c = (i_rhs-1)*n_per_rhs + nnz_col (c)%array(i_sym_rhs)
               ind_l = (i_rhs-1)*n_per_rhs + nnz_line(c)%array(i_sym_rhs)

               ref = global_rhs(ind_c)
               !! add new values if ref is non-zero -----------------------
               !!  **** minus because it goes from lhs to rhs
               !!  **** conjugation because we solve adjoint problem
               global_rhs_update(ind_l) = global_rhs_update(ind_l)      &
                                        - dcmplx(ref*                   &
                                          conjg(nnz_val(c)%array(i_sym_rhs)))
  
             end do !! end loop over rhs

          end if !! end if there is a nnz

        end do !! end loop over nnz

      end if !! end if we have nnz

    end do !! end loop over cells
    
    if(flag_worker) then
      !! update rhs
      global_rhs(:) = global_rhs(:) + global_rhs_update(:)
      deallocate(global_rhs_update)
    end if

    return

  end subroutine modify_rhs_for_symmetry_adjoint

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create a dense RHS for HDG discretization, we use the HDG matrix
  !> Se as an input working array, and create the CSR representation. 
  !> Because of Mumps solver, the RHS is kept on the master processor.
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    source_value   : source value
  !> @param[in]    ctx_dg         : DG context
  !> @param[in]    ctx_mesh       : mesh context
  !> @param[in]    nrhs           : number of rhs
  !> @param[in]    flag_bdry      : indicates if boundaries are considered.
  !> @param[inout] Rval           : rhs values
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_rhs_create_lagrange_dense(ctx_paral,ctx_dg,ctx_mesh,    &
                                          nrhs,flag_bdry,Rval,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_discretization)             ,intent(in)    :: ctx_dg
    logical                            ,intent(in)    :: flag_bdry
    integer                            ,intent(in)    :: nrhs
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: Rval(:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! local
    integer                                 :: i_rhs
    integer                                 :: counter_dof_face
    integer                                 :: ndof_vol
    integer                                 :: ndof_face,order,i_order
    integer                                 :: f,i,j,i_orient
    integer(kind=IKIND_MAT)                 :: ndof_per_var,ind,var
    integer(kind=IKIND_MAT)                 :: ind_array,upper_bound
    integer(kind=IKIND_MAT)                 :: ndof_per_rhs,c
    integer(kind=IKIND_MESH)                :: face,iloc,ndof_face_cell
    complex(kind=8),allocatable             :: temp_aval(:)
    complex(kind=8)                         :: coeff
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! we create the global arrays because they will be 
    !! reduced onto the master at the end. 
    ndof_per_var = ctx_dg%n_dofgb_pervar
    ndof_per_rhs = ndof_per_var * ctx_dg%dim_var
    upper_bound  = nrhs * ndof_per_var * ctx_dg%dim_var
    allocate(temp_aval(upper_bound))
    temp_aval = 0.d0 
    
    !! We use the array Se that is contained in 
    !!           ctx_dg%src(:)%hdg_S
    !! all cells are concerned in this dense version.
    do c = 1, ctx_mesh%n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      !! same for all sources because all cells are concerned.
      ndof_vol         = ctx_dg%src(1)%ndof_vol (c)
      ndof_face_cell   = ctx_dg%src(1)%ndof_face(c)
      counter_dof_face = 0
      
      do f=1,ctx_mesh%n_neigh_per_cell     !! loop over all face

        !! if the face is on the boundary, we cycle 
        if( (.not.flag_bdry) .and. ctx_mesh%cell_neigh(f,c) < 0) cycle
        
        !!
        face       = ctx_mesh%cell_face   (f,c)
        order      = ctx_mesh%order_face  (face)
        i_order    = ctx_dg%order_to_index(order)
        ndof_face  = ctx_dg%n_dof_face_per_order(i_order)

        do var=1,ctx_dg%dim_var            !! loop over all matrix var
          do i=1,ndof_face                 !! loop over all face dof
            
            !! get global index and the local one 
            ind = (var-1)*ndof_per_var + ctx_dg%offsetdof_mat_gb(face)
            i_orient = ctx_mesh%cell_face_orientation(f,c)
            ind = ind + ctx_dg%hdg%idof_face(i_orient,i_order)%array(i)
            iloc  = int((var-1)*ndof_face_cell + counter_dof_face + i,  &
                        kind=IKIND_MESH)
            
            !! compute for all rhs
            do i_rhs=1, nrhs
              coeff = 0.0
              do j=1,ndof_vol*ctx_dg%dim_sol !! loop over all vol dof for value
                 !! note that X emcompasses a minus sign already
                 coeff = coeff + dcmplx(ctx_dg%hdg%X(c)%array(iloc,j)*  &
                                        ctx_dg%src(i_rhs)%hdg_S(c)%array(j))
              end do
              
              ind_array = ind + (i_rhs-1) * ndof_per_rhs
              !! for multiple entries we have to update it
              temp_aval(ind_array) = temp_aval(ind_array) + coeff              
            end do

          end do
        end do
        counter_dof_face = counter_dof_face + ndof_face

      !! -------------------------------------------------------------
      end do !! end loop over all faces ------------------------------
   
    end do !! end loop over all cells ----------------------------------
    !! -----------------------------------------------------------------

    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)

    !! -------------------------------------------------------------
    !!
    !! now that we have the "main" rhs, we adjust for symmetry
    !! in case we have dirichlet or planewave source with hdg.
    !!
    !! -------------------------------------------------------------
    if(ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize) then
    upper_bound  = nrhs * ndof_per_var * ctx_dg%dim_var
      call modify_rhs_for_symmetry(ctx_mesh%n_cell_loc,temp_aval,       &
                                   ndof_per_var,nrhs,                   &
                                   ctx_dg%ctx_symmetrize_mat%cell_nval, &
                                   ctx_dg%ctx_symmetrize_mat%c_line,    &
                                   ctx_dg%ctx_symmetrize_mat%c_col,     &
                                   ctx_dg%ctx_symmetrize_mat%c_val)

    end if
    !! -------------------------------------------------------------
    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)

    !! -----------------------------------------------------------------
    !! we reduce the global rhs onto the master only.
    !! -----------------------------------------------------------------
    if(ctx_paral%master) then
      allocate(Rval(upper_bound))
      Rval = 0.d0
    end if
    call reduce_sum(cmplx(temp_aval,kind=RKIND_MAT),Rval,               &
                    int(upper_bound,kind=4),0,ctx_paral%communicator,   &
                    ctx_err)
    deallocate(temp_aval)
    !! -----------------------------------------------------------------
    return
  end subroutine dg_rhs_create_lagrange_dense


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create a dense RHS for HDG discretization, we use the HDG matrix
  !> Se as an input working array, and create the CSR representation. 
  !> Because of Mumps solver, the RHS is kept on the master processor.
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    source_value   : source value
  !> @param[in]    ctx_dg         : DG context
  !> @param[in]    ctx_mesh       : mesh context
  !> @param[in]    nrhs           : number of rhs
  !> @param[in]    flag_bdry      : indicates if boundaries are considered.
  !> @param[inout] Rval           : rhs values
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_rhs_backward_create_lagrange_dense(ctx_paral,ctx_dg,    &
                                                   ctx_mesh,nrhs,       &
                                                   flag_bdry,Rval,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_discretization)             ,intent(in)    :: ctx_dg
    logical                            ,intent(in)    :: flag_bdry
    integer                            ,intent(in)    :: nrhs
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: Rval(:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! local
    integer                                 :: i_rhs
    integer                                 :: counter_dof_face
    integer                                 :: ndof_vol
    integer                                 :: ndof_face,order,i_order
    integer                                 :: f,i,j,i_orient
    integer(kind=IKIND_MAT)                 :: ndof_per_var,ind,var
    integer(kind=IKIND_MAT)                 :: ind_array,upper_bound
    integer(kind=IKIND_MAT)                 :: ndof_per_rhs,c
    integer(kind=IKIND_MESH)                :: face,iloc,ndof_face_cell
    complex(kind=8),allocatable             :: temp_aval(:)
    complex(kind=8)                         :: coeff
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! we create the global arrays because they will be 
    !! reduced onto the master at the end. 
    ndof_per_var = ctx_dg%n_dofgb_pervar
    ndof_per_rhs = ndof_per_var * ctx_dg%dim_var
    upper_bound  = nrhs * ndof_per_var * ctx_dg%dim_var
    allocate(temp_aval(upper_bound))
    temp_aval = 0.d0 
    
    !! We use the array Se that is contained in 
    !!           ctx_dg%src(:)%hdg_S
    !! all cells are concerned in this dense version.
    do c = 1, ctx_mesh%n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      !! same for all sources because all cells are concerned.
      !! so we can use source 1 for instance.
      ndof_vol         = ctx_dg%src(1)%rcv(c)%ndof_vol 
      ndof_face_cell   = ctx_dg%src(1)%rcv(c)%ndof_face
      counter_dof_face = 0
      
      do f=1,ctx_mesh%n_neigh_per_cell     !! loop over all face

        !! if the face is on the boundary, we cycle 
        if( (.not.flag_bdry) .and. ctx_mesh%cell_neigh(f,c) < 0) cycle
        
        !!
        face       = ctx_mesh%cell_face   (f,c)
        order      = ctx_mesh%order_face  (face)
        i_order    = ctx_dg%order_to_index(order)
        ndof_face  = ctx_dg%n_dof_face_per_order(i_order)

        do var=1,ctx_dg%dim_var            !! loop over all matrix var
          do i=1,ndof_face                 !! loop over all face dof
            
            !! get global index and the local one 
            ind = (var-1)*ndof_per_var + ctx_dg%offsetdof_mat_gb(face)
            i_orient = ctx_mesh%cell_face_orientation(f,c)
            ind = ind + ctx_dg%hdg%idof_face(i_orient,i_order)%array(i)
            iloc  = int((var-1)*ndof_face_cell + counter_dof_face + i,  &
                        kind=IKIND_MESH)
            
            !! compute for all rhs
            do i_rhs=1, nrhs
              coeff = 0.0
              do j=1,ndof_vol*ctx_dg%dim_sol !! loop over all vol dof for value
                 !! backward problem, we have for RHS 
                 !! - Rt C* A^{-*} (rhs)
                 !!=- Rt   W       (rhs)                 
                 coeff = coeff - dcmplx(ctx_dg%hdg%W(c)%array(iloc,j)*  &
                                        ctx_dg%src(i_rhs)%rcv(c)%hdg_R(j))
              end do
              
              ind_array = ind + (i_rhs-1) * ndof_per_rhs
              !! for multiple entries we have to update it
              temp_aval(ind_array) = temp_aval(ind_array) + coeff              
            end do

          end do
        end do
        counter_dof_face = counter_dof_face + ndof_face

      !! -------------------------------------------------------------
      end do !! end loop over all faces ------------------------------

    
    end do !! end loop over all cells ----------------------------------
    !! -----------------------------------------------------------------

    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)

    !! -------------------------------------------------------------
    !!
    !! now that we have the "main" rhs, we adjust for symmetry
    !! in case we have dirichlet or planewave source with hdg.
    !!
    !! -------------------------------------------------------------
    if(ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize) then
    upper_bound  = nrhs * ndof_per_var * ctx_dg%dim_var
      call modify_rhs_for_symmetry_adjoint(ctx_mesh%n_cell_loc,temp_aval,&
                                   ndof_per_var,nrhs,                    &
                                   ctx_dg%ctx_symmetrize_mat%cell_nval,  &
                                   ctx_dg%ctx_symmetrize_mat%c_line,     &
                                   ctx_dg%ctx_symmetrize_mat%c_col,      &
                                   ctx_dg%ctx_symmetrize_mat%c_val)

    end if
    !! -------------------------------------------------------------
    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)

    !! -----------------------------------------------------------------
    !! we reduce the global rhs onto the master only.
    !! -----------------------------------------------------------------
    if(ctx_paral%master) then
      allocate(Rval(upper_bound))
      Rval = 0.d0
    end if
    call reduce_sum(cmplx(temp_aval,kind=RKIND_MAT),Rval,               &
                    int(upper_bound,kind=4),0,ctx_paral%communicator,   &
                    ctx_err)
    deallocate(temp_aval)
    !! -----------------------------------------------------------------
    return
  end subroutine dg_rhs_backward_create_lagrange_dense

end module m_dg_lagrange_rhs_dense
