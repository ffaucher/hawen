!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_source_field.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize a secondary problem source for the 
!> passive cross-correlation problem.
!> discretization in the context of Lagrange Basis functions for
!> simplex meshes. 
!> The source concerns all the cells.
!>   
!
!------------------------------------------------------------------------------
module m_dg_lagrange_source_field

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: IKIND_MESH,RKIND_POL, RKIND_MAT
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_max,          only: allreduce_max
  use m_allreduce_sum,          only: allreduce_sum
  use m_array_types,            only: array_allocate
  use m_basis_functions,        only: t_basis, basis_construct,         &
                                      basis_derivative_construct, basis_clean
  use m_polynomial,             only: polynomial_eval
  !! matrix and mesh domain types
  use m_ctx_acquisition,        only: t_acquisition,SRC_GAUSSIAN
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  !! to represent the model
  use m_ctx_model_representation,   &
                                only: t_model_param
  use m_model_parameter_evaluation, &
                                only: model_parameter_evaluation
  !! to adjust the rhs
  use m_tag_scale_rhs,          only: tag_RHS_SCALE_ID,tag_RHS_SCALE_R, &
                                      tag_RHS_SCALE_R2,tag_RHS_SCALE_X, &
                                      tag_RHS_SCALE_X2,                 &
                                      tag_RHS_SCALE_INV2PI,tag_RHS_SCALE_XINV2PI
  !! -------------------------------------------------------------------
  implicit none

  private
  
  public :: dg_source_init_field
  public :: dg_source_backward_init_field

  contains


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of field/covariance sources.
  !>
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    ctx_model_param: source model
  !> @param[in]    field          : wavefield to use
  !> @param[in]    n_rhs_component: number of component concerned
  !> @param[in]    rhs_i_component: list of rhs component 
  !> @param[in]    format_model   : format of the input model parameter
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine dg_source_init_field(ctx_grid,ctx_dg,ctx_model_param_real, &
                                  ctx_model_param_imag,field,           &
                                  n_rhs_component,rhs_i_component,      &
                                  format_model,i_src_first,i_src_last,  &
                                  tag_scale_rhs,ctx_err)
    implicit none
    type(t_domain)                  ,intent(in)   :: ctx_grid
    type(t_discretization)          ,intent(inout):: ctx_dg
    type(t_model_param)             ,intent(in)   :: ctx_model_param_real
    type(t_model_param)             ,intent(in)   :: ctx_model_param_imag
    complex(kind=RKIND_MAT)         ,intent(in)   :: field(:,:)
    integer                         ,intent(in)   :: n_rhs_component
    integer                         ,intent(in)   :: rhs_i_component(:)
    character(len=*)                ,intent(in)   :: format_model
    integer                         ,intent(in)   :: i_src_first, i_src_last
    integer                         ,intent(in)   :: tag_scale_rhs
    type(t_error)                   ,intent(inout):: ctx_err

    !! local
    integer                              ::  k,i_comp,nsrc
    integer                              :: ind_offset_rhs_comp
    integer                              :: igb_comp_rhs
    integer                              :: j,ndof_vol, ndof_all
    integer                 ,allocatable :: ndof_face(:)
    complex(kind=RKIND_POL) ,allocatable :: weight(:,:)
    logical                              :: flag_backward = .false.
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0    
    allocate(ndof_face(ctx_grid%n_neigh_per_cell))

    !! -----------------------------------------------------------------
    !! allocate the arrays, once and for all. 
    !! -----------------------------------------------------------------
    nsrc = i_src_last-i_src_first + 1
    do j=1,nsrc
      if(ctx_err%ierr .ne. 0) cycle
 
      allocate(ctx_dg%src(j)%flag_on_bdry (ctx_grid%n_cell_loc))
      allocate(ctx_dg%src(j)%icell_loc    (ctx_grid%n_cell_loc))
      allocate(ctx_dg%src(j)%ndof_vol     (ctx_grid%n_cell_loc))
      allocate(ctx_dg%src(j)%ndof_face    (ctx_grid%n_cell_loc))
      !allocate(ctx_dg%src(j)%weight       (ctx_dg%dim_sol,ctx_grid%n_cell_gb))
      !allocate(ctx_dg%src(j)%weight_dbasis(ctx_dg%dim_sol,ctx_grid%n_cell_gb))
      allocate(ctx_dg%src(j)%hdg_S        (ctx_grid%n_cell_loc))
      
      !! initialization ------------------------------------------------
      ctx_dg%src(j)%flag_on_bdry  = .false.
      ctx_dg%src(j)%ndof_vol      = 0
      ctx_dg%src(j)%ndof_face     = 0
      ctx_dg%src(j)%ncell_loc     = ctx_grid%n_cell_loc !! full RHS
    end do

    !! -----------------------------------------------------------------
    ! loop for every cell ----------------------------------------------
    do k=1,ctx_grid%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle

      ndof_vol = ctx_dg%n_dof_per_order(ctx_dg%order_to_index(ctx_grid%order(k)))
      ndof_all = ndof_vol * ctx_dg%dim_sol

      do i_comp=1 , n_rhs_component
        if(ctx_err%ierr .ne. 0) cycle
        
        igb_comp_rhs          = rhs_i_component(i_comp)
        ind_offset_rhs_comp   = (igb_comp_rhs-1)*ndof_vol
        call src_field_weight_dof_allsources(ctx_grid,ctx_dg,           &
                                  ctx_model_param_real,                 &
                                  ctx_model_param_imag,                 &
                                  field, format_model,flag_backward,    &
                                  tag_scale_rhs,k,igb_comp_rhs,nsrc,    &
                                  ndof_vol,ndof_face,weight,ctx_err)
        if(ctx_err%ierr .ne. 0) cycle

        !! fill-in arrays for all sources
        do j=1,nsrc
          ctx_dg%src(j)%icell_loc(k)=k
          call array_allocate(ctx_dg%src(j)%hdg_S(k),ndof_all,ctx_err)
          ctx_dg%src(j)%hdg_S(k)%array = dcmplx(0.0,0.0)
          ctx_dg%src(j)%hdg_S(k)%array(ind_offset_rhs_comp+1:             &
                                       ind_offset_rhs_comp+ndof_vol)=weight(:,j)
          ctx_dg%src(j)%ndof_vol(k)  = ndof_vol
          ctx_dg%src(j)%ndof_face(k) = sum(ndof_face)
        end do
        deallocate(weight) 

      end do !! end loop over all components
      !! ---------------------------------------------------------------

    end do !! end loop over all cells
    !! -----------------------------------------------------------------

    deallocate(ndof_face)

    return

  end subroutine dg_source_init_field

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of field/covariance sources.
  !>
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    ctx_model_param: source model
  !> @param[in]    field          : wavefield to use
  !> @param[in]    n_rhs_component: number of component concerned
  !> @param[in]    rhs_i_component: list of rhs component 
  !> @param[in]    format_model   : format of the input model parameter
  !> @param[in]    first/last     : fisrt and last source index
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine dg_source_backward_init_field(ctx_grid,ctx_dg,ctx_model_param_real, &
                                  ctx_model_param_imag,field,           &
                                  n_rhs_component,rhs_i_component,      &
                                  format_model,i_src_first,i_src_last,  &
                                  tag_scale_rhs,ctx_err)

    implicit none
    type(t_domain)                  ,intent(in)   :: ctx_grid
    type(t_discretization)          ,intent(inout):: ctx_dg
    type(t_model_param)             ,intent(in)   :: ctx_model_param_real
    type(t_model_param)             ,intent(in)   :: ctx_model_param_imag
    complex(kind=RKIND_MAT)         ,intent(in)   :: field(:,:)
    integer                         ,intent(in)   :: n_rhs_component
    integer                         ,intent(in)   :: rhs_i_component(:)
    character(len=*)                ,intent(in)   :: format_model
    integer                         ,intent(in)   :: i_src_first, i_src_last
    integer                         ,intent(in)   :: tag_scale_rhs
    type(t_error)                   ,intent(inout):: ctx_err

    !! local
    integer                              :: k, i_comp
    integer                              :: ind_offset_rhs_comp
    integer                              :: igb_comp_rhs
    integer                              :: i_src, j,ndof_vol, ndof_all
    integer                 ,allocatable :: ndof_face(:)
    complex(kind=RKIND_POL) ,allocatable :: weight(:)
    logical                              :: flag_backward = .true.
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0    
    allocate(ndof_face(ctx_grid%n_neigh_per_cell))

    do i_src=i_src_first, i_src_last
      if(ctx_err%ierr .ne. 0) cycle
 
      j = i_src-i_src_first+1
      
      !! ---------------------------------------------------------------
      !! we now need to modify the rcv information because we are 
      !! in a backward process for the computation of the field 
      !! at the end 
      !! Only hdg_R shall be used when we recompute the volumic field.
      !! ---------------------------------------------------------------
      ctx_dg%src(j)%nrcv = ctx_grid%n_cell_loc
      allocate(ctx_dg%src(j)%rcv(ctx_grid%n_cell_loc))
      !!
      !! each receiver is one cell. 
      !! initialization ------------------------------------------------
      ctx_dg%src(j)%rcv(:)%icell_loc = 0
      ctx_dg%src(j)%rcv(:)%ndof_vol  = 0
      ctx_dg%src(j)%rcv(:)%ndof_face = 0 
      !! ctx_dg%src(j)%rcv(:)%weight(:) 
      !! ctx_dg%src(j)%rcv(:)%hdg_R(:) 
  
      ! loop for every cell
      do k=1,ctx_grid%n_cell_loc
        if(ctx_err%ierr .ne. 0) cycle

        ctx_dg%src(j)%rcv(k)%icell_loc = k !! all cells are concerned for dense rhs

        ndof_vol = ctx_dg%n_dof_per_order(ctx_dg%order_to_index &
                                             (ctx_grid%order(k)))
        ndof_all = ndof_vol * ctx_dg%dim_sol
        ! allocate hdg_S
        allocate(ctx_dg%src(j)%rcv(k)%hdg_R(ndof_all))
        ctx_dg%src(j)%rcv(k)%hdg_R = dcmplx(0.0,0.0)


        do i_comp=1 , n_rhs_component
          !! we respect the original rhs here ..........................
          igb_comp_rhs = rhs_i_component(i_comp)
          ind_offset_rhs_comp   = (igb_comp_rhs-1)*ndof_vol
          if(ctx_err%ierr .ne. 0) cycle

          call src_field_weight_dof(ctx_grid,ctx_dg, ctx_model_param_real,&
                                    ctx_model_param_imag,                 &
                                    field, format_model,flag_backward,    &
                                    tag_scale_rhs,k,igb_comp_rhs,j,       &
                                    ndof_vol,ndof_face,weight,ctx_err)



          if(ctx_err%ierr .ne. 0) cycle

          !! fill-in arrays
          ctx_dg%src(j)%rcv(k)%hdg_R(ind_offset_rhs_comp+1:             &
                                     ind_offset_rhs_comp+ndof_vol)=weight
          ctx_dg%src(j)%rcv(k)%ndof_vol     = ndof_vol
          ctx_dg%src(j)%rcv(k)%ndof_face    = sum(ndof_face)
          
          deallocate(weight) 

        end do
        if(ctx_err%ierr .ne. 0) cycle

      end do 
    
    end do

    deallocate(ndof_face)

    return

  end subroutine dg_source_backward_init_field



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We computet the volume integral of the  conj(G(x , .))*S(.) where G 
  !> is represented by a field, S by a model.
  !> with each of the basis function of the given cell. That is, we 
  !> compute, for all j, 
  !>     
  !>     \f$ \int_K \phi_j.S.\alpha*_i.\phi_i(x) dx\f$
  !>      where i , j are the index of the basis functions in the cell
  !>
  !> @param[in]    ctx_field      : field context (field representing the green function, solution of the first problem)
  !> @param[in]    ctx_crosscorr  : CC context (containing the source covariance info)
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[in]    local_cell     : index of the cell we are computing in
  !> @param[in]    i_field        : index of the field we are computing for
  !> @param[in]    ndof_vol       : number of volume dof in the cell
  !> @param[out]   weight         : value of the integral over the cell for each basis function
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine src_field_weight_dof(ctx_grid,ctx_dg,ctx_model_param_real, &
                                  ctx_model_param_imag, field,          &
                                  format_model,flag_backward,           &
                                  tag_scale_rhs,                        &
                                  local_cell,igb_comp_rhs,              &
                                  i_src,ndof_vol,ndof_face,             &
                                  weight,ctx_err)
    implicit none
    
    type(t_domain)                     ,intent(in)    :: ctx_grid
    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_model_param)                ,intent(in)    :: ctx_model_param_real
    type(t_model_param)                ,intent(in)    :: ctx_model_param_imag
    complex(kind=RKIND_MAT)            ,intent(in)    :: field(:,:)
    character(len=*)                   ,intent(in)    :: format_model
    logical                            ,intent(in)    :: flag_backward
    integer                            ,intent(in)    :: tag_scale_rhs
    integer(kind=IKIND_MESH)           ,intent(in)    :: local_cell
    integer                            ,intent(in)    :: igb_comp_rhs
    integer                            ,intent(in)    :: i_src   
    integer                            ,intent(out)   :: ndof_vol
    integer                            ,intent(out)   :: ndof_face(:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: weight(:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    integer                      :: order, i_order, i_dof, j_dof,k , f
    integer                      :: face 
    integer (kind=8)             :: src_offset
    real   (kind=8), allocatable :: scaling(:)
    real   (kind=8)              :: Qk,fcrosscov_real,fcrosscov_imag
    complex(kind=8)              :: fcrosscov
    complex(kind=8)              :: alpha_i
    complex(kind=8), allocatable :: int_vol(:)
    real   (kind=8), allocatable :: xpt_loc(:),xpt_gb(:)
    real   (kind=8)              :: node_coo(ctx_grid%dim_domain,ctx_grid%dim_domain +1)
    real   (kind=8), parameter   :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    

    ndof_face=0
    do f=1,ctx_grid%n_neigh_per_cell
      face     = ctx_grid%cell_face   (f,local_cell)
      order    = ctx_grid%order_face  (face)
      i_order  = ctx_dg%order_to_index(order)
      ndof_face(f)= ctx_dg%n_dof_face_per_order(i_order)
    enddo
    order    = ctx_grid%order        (local_cell)
    i_order  = ctx_dg%order_to_index (order)
    ndof_vol = ctx_dg%n_dof_per_order(i_order)


    allocate(xpt_loc(ctx_grid%dim_domain))
    allocate(xpt_gb (ctx_grid%dim_domain))

    node_coo(:,:)= dble(ctx_grid%x_node(:,ctx_grid%cell_node(:,local_cell)))
    !! the weight array ------------------------------------------------
    allocate(weight(ndof_vol))
    weight=0.0
    
    !! -----------------------------------------------------------------
    !! possibility of a scaling in the RHS. Untouched
    allocate(scaling(ctx_dg%quadGL_npts))
    
    scaling = 1.d0
    select case(tag_scale_rhs)
      
      case(tag_RHS_SCALE_ID)        !! nothing to do 
      case(tag_RHS_SCALE_INV2PI)    !! global scaling
        scaling = 1.d0/(2.d0*pi)
      case(tag_RHS_SCALE_R)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)
        end do
      case(tag_RHS_SCALE_R2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)**2
        end do
      case(tag_RHS_SCALE_X2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)*xpt_gb(1)
        end do
      case(tag_RHS_SCALE_XINV2PI)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)/(2.d0*pi)
        end do
      case(tag_RHS_SCALE_X)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)
        end do
      case default
        ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                        "RHS [field_weight_src] **"
        ctx_err%ierr  = -305
        ctx_err%critic=.true.
      return !! non-shared error here
    end select
    !!---------------------------------------------------------------
    !! make sure they are not all zero
    if(minval(abs(scaling)) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: scaling of the RHS is zero for all "//&
                        "positions [field_weight_src] **"
      ctx_err%ierr  = -305
      ctx_err%critic=.true.
      return !! non-shared error here    
    end if
    !! -----------------------------------------------------------------

    !! -------------------------------------------------------
    ! Computing the source offset so we look at the righ place in the fild array
    src_offset = (i_src-1)*ctx_dg%n_dofloc_persol

    allocate(int_vol(ndof_vol))
    int_vol=0.d0
    
    !! first loop over the quadrature positions seems more efficient
    do k=1, ctx_dg%quadGL_npts
      !! cross-covariance of the source at the position
      xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
      xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                   + dble(node_coo(:,1))
      call model_parameter_evaluation(ctx_model_param_real,format_model, &
                                      ctx_grid%dim_domain,          &
                                      local_cell,xpt_loc,xpt_gb,    &
                                      fcrosscov_real,ctx_err)
      call model_parameter_evaluation(ctx_model_param_imag,format_model, &
                                      ctx_grid%dim_domain,          &
                                      local_cell,xpt_loc,xpt_gb,    &
                                      fcrosscov_imag,ctx_err)
      fcrosscov = dcmplx(fcrosscov_real, fcrosscov_imag)
        
      ! ------------------------------------------------------------------
      !  We compute for every basis function
      do j_dof =1, ndof_vol
        do i_dof = 1, ndof_vol
          !! Compute the Cross-correlation of the source, for the moment, we suppose that 
          ! it is a source function for the pressure
          !! Compute w_k* \phi_j(x_k)*\phi_i(x_k)
          ! w_k -> quadrature weight
          ! x_k -> quadrature point
          Qk = ctx_dg%quadGL_phi_phi_w(i_order)%array(i_dof,j_dof,k)
          !! add the result of every quadrature points in the sum
          !! setting offset of the source so we look into the good field
          alpha_i=conjg(field(src_offset+ctx_dg%offsetdof_sol_loc(local_cell)+&
                              i_dof,igb_comp_rhs))
          int_vol(j_dof) = int_vol(j_dof) + alpha_i*dcmplx(Qk*scaling(k))*fcrosscov

        end do !! end loop over i
      
      end do !! end loop over j 
    
    end do !! end loop over quadrature positions

    !! save
    if(flag_backward) then 
      !! for backward problem the RHS is now 
      !!    U conjg(S) = conjg( conjg(U).S )
    ! weight(:) = ctx_dg%det_jac(local_cell)*conjg(int_vol(:))
      weight(:) = ctx_dg%det_jac(local_cell)*int_vol(:)
    else
      weight(:) = ctx_dg%det_jac(local_cell)*int_vol(:)
    endif

    deallocate(scaling)
    deallocate(int_vol)
    !! ----------------------------------------------------------------
    return
  end subroutine src_field_weight_dof


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We computet the volume integral of the  conj(G(x , .))*S(.) where G 
  !> is represented by a field, S by a model.
  !> with each of the basis function of the given cell. That is, we 
  !> compute, for all j, 
  !>     
  !>     \f$ \int_K \phi_j.S.\alpha*_i.\phi_i(x) dx\f$
  !>      where i , j are the index of the basis functions in the cell
  !>
  !> @param[in]    ctx_field      : field context (field representing the green function, solution of the first problem)
  !> @param[in]    ctx_crosscorr  : CC context (containing the source covariance info)
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    tag_scale_rhs  : indicate a possible scaling of rhs
  !> @param[in]    local_cell     : index of the cell we are computing in
  !> @param[in]    i_field        : index of the field we are computing for
  !> @param[in]    ndof_vol       : number of volume dof in the cell
  !> @param[out]   weight         : value of the integral over the cell for each basis function
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine src_field_weight_dof_allsources(ctx_grid,ctx_dg,ctx_model_param_real, &
                                  ctx_model_param_imag, field,          &
                                  format_model,flag_backward,           &
                                  tag_scale_rhs,                        &
                                  local_cell,igb_comp_rhs,              &
                                  nsrc,ndof_vol,ndof_face,              &
                                  weight,ctx_err)
    implicit none
    
    type(t_domain)                     ,intent(in)    :: ctx_grid
    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_model_param)                ,intent(in)    :: ctx_model_param_real
    type(t_model_param)                ,intent(in)    :: ctx_model_param_imag
    complex(kind=RKIND_MAT)            ,intent(in)    :: field(:,:)
    character(len=*)                   ,intent(in)    :: format_model
    logical                            ,intent(in)    :: flag_backward
    integer                            ,intent(in)    :: tag_scale_rhs
    integer(kind=IKIND_MESH)           ,intent(in)    :: local_cell
    integer                            ,intent(in)    :: igb_comp_rhs
    integer                            ,intent(in)    :: nsrc
    integer                            ,intent(out)   :: ndof_vol
    integer                            ,intent(out)   :: ndof_face(:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: weight(:,:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    integer                      :: order,i_order,i_dof,j_dof,k,f
    integer                      :: face,i_src
    integer (kind=8)             :: src_offset
    real   (kind=8), allocatable :: scaling(:)
    real   (kind=8)              :: Qk,fcrosscov_real,fcrosscov_imag
    complex(kind=8)              :: fcrosscov
    complex(kind=8)              :: alpha_i
    complex(kind=8), allocatable :: int_vol(:,:)
    real   (kind=8), allocatable :: xpt_loc(:),xpt_gb(:)
    real   (kind=8)              :: node_coo(ctx_grid%dim_domain,ctx_grid%dim_domain +1)
    real   (kind=8), parameter   :: pi = 4.d0*datan(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    

    ndof_face=0
    do f=1,ctx_grid%n_neigh_per_cell
      face     = ctx_grid%cell_face   (f,local_cell)
      order    = ctx_grid%order_face  (face)
      i_order  = ctx_dg%order_to_index(order)
      ndof_face(f)= ctx_dg%n_dof_face_per_order(i_order)
    enddo
    order    = ctx_grid%order        (local_cell)
    i_order  = ctx_dg%order_to_index (order)
    ndof_vol = ctx_dg%n_dof_per_order(i_order)


    allocate(xpt_loc(ctx_grid%dim_domain))
    allocate(xpt_gb (ctx_grid%dim_domain))

    node_coo(:,:)= dble(ctx_grid%x_node(:,ctx_grid%cell_node(:,local_cell)))
    !! the weight array ------------------------------------------------
    allocate(weight(ndof_vol,nsrc))
    weight=0.0
    
    !! -----------------------------------------------------------------
    !! possibility of a scaling in the RHS. Untouched
    allocate(scaling(ctx_dg%quadGL_npts))
    
    scaling = 1.d0
    select case(tag_scale_rhs)
      
      case(tag_RHS_SCALE_ID)        !! nothing to do 
      case(tag_RHS_SCALE_INV2PI)    !! global scaling
        scaling = 1.d0/(2.d0*pi)
      case(tag_RHS_SCALE_R)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)
        end do
      case(tag_RHS_SCALE_R2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = norm2(xpt_gb)**2
        end do
      case(tag_RHS_SCALE_X2)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)*xpt_gb(1)
        end do
      case(tag_RHS_SCALE_XINV2PI)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)/(2.d0*pi)
        end do
      case(tag_RHS_SCALE_X)
        do k=1, ctx_dg%quadGL_npts
          xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
          xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                     + dble(node_coo(:,1))
          scaling(k) = xpt_gb(1)
        end do
      case default
        ctx_err%msg   = "** ERROR: unknown scaling to apply to the "//&
                        "RHS [field_weight_src] **"
        ctx_err%ierr  = -305
        ctx_err%critic=.true.
      return !! non-shared error here
    end select
    !!---------------------------------------------------------------
    !! make sure they are not all zero
    if(minval(abs(scaling)) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: scaling of the RHS is zero for all "//&
                        "positions [field_weight_src] **"
      ctx_err%ierr  = -305
      ctx_err%critic=.true.
      return !! non-shared error here    
    end if
    !! -----------------------------------------------------------------

    !! -------------------------------------------------------
    allocate(int_vol(ndof_vol,nsrc))
    int_vol=0.d0
    
    !! first loop over the quadrature positions seems more efficient
    do k=1, ctx_dg%quadGL_npts
      !! cross-covariance of the source at the position
      xpt_loc(:) = dble(ctx_dg%quadGL_pts(:,k))
      xpt_gb (:) = matmul(dble(ctx_dg%jac(:,:,local_cell)),xpt_loc(:))&
                   + dble(node_coo(:,1))
      call model_parameter_evaluation(ctx_model_param_real,format_model, &
                                      ctx_grid%dim_domain,          &
                                      local_cell,xpt_loc,xpt_gb,    &
                                      fcrosscov_real,ctx_err)
      call model_parameter_evaluation(ctx_model_param_imag,format_model, &
                                      ctx_grid%dim_domain,          &
                                      local_cell,xpt_loc,xpt_gb,    &
                                      fcrosscov_imag,ctx_err)
      fcrosscov = dcmplx(fcrosscov_real, fcrosscov_imag)
        
      ! ------------------------------------------------------------------
      !  We compute for every basis function
      do j_dof =1, ndof_vol
        do i_dof = 1, ndof_vol
          !! Compute the Cross-correlation of the source, for the moment, we suppose that 
          ! it is a source function for the pressure
          !! Compute w_k* \phi_j(x_k)*\phi_i(x_k)
          ! w_k -> quadrature weight
          ! x_k -> quadrature point
          Qk = ctx_dg%quadGL_phi_phi_w(i_order)%array(i_dof,j_dof,k)
          !! add the result of every quadrature points in the sum
          !! setting offset of the source so we look into the good field

          !! loop over all sources 
          do i_src=1,nsrc
            ! Computing the source offset so we look at the righ place in the field array
            src_offset = (i_src-1)*ctx_dg%n_dofloc_persol
            alpha_i=conjg(field(src_offset+ctx_dg%offsetdof_sol_loc(local_cell)+ &
                                i_dof,igb_comp_rhs))
            int_vol(j_dof,i_src) = int_vol(j_dof,i_src)                 &
                                 + alpha_i*dcmplx(Qk*scaling(k))*fcrosscov
          end do !! end loop over all sources

        end do !! end loop over i
      
      end do !! end loop over j 
    
    end do !! end loop over quadrature positions

    !! save
    if(flag_backward) then 
      !! for backward problem the RHS is now 
      !!    U conjg(S) = conjg( conjg(U).S )
    ! weight(:) = ctx_dg%det_jac(local_cell)*conjg(int_vol(:))
      weight(:,:) = ctx_dg%det_jac(local_cell)*int_vol(:,:)
    else
      weight(:,:) = ctx_dg%det_jac(local_cell)*int_vol(:,:)
    endif

    deallocate(scaling)
    deallocate(int_vol)
    !! ----------------------------------------------------------------
    return
  end subroutine src_field_weight_dof_allsources



end module m_dg_lagrange_source_field
