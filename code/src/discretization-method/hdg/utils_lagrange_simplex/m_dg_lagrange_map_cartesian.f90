!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_map_cartesian.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize the weighting coefficient when 
!> using a cartesian grid to save with DG method
!
!------------------------------------------------------------------------------
module m_dg_lagrange_map_cartesian

  !! module used -------------------------------------------------------
  use m_raise_error,          only: raise_error, t_error
  use m_array_types,          only: array_allocate
  use m_define_precision,     only: RKIND_POL,IKIND_MESH
  use m_array_types,          only: array_allocate,t_array1d_real
  use m_basis_functions,      only: t_basis, basis_construct, basis_clean
  use m_polynomial,           only: polynomial_eval
  use m_ctx_parallelism,      only: t_parallelism
  !> matrix and mesh domain types
  use m_ctx_domain,           only: t_domain
  use m_ctx_discretization,   only: t_discretization
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: dg_cartesian_map_weight_lagrange

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of nodes that are concerned
  !> by a cartesian grid representation
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_grid       : cartesian grid type
  !> @param[in]    ctx_dg         : type DG
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_cartesian_map_weight_lagrange(ctx_grid,ctx_dg,ixmin,    &
                                              ixmax,iymin,iymax,izmin,  &
                                              izmax,dx,dy,dz,ox,oy,oz,  &
                                              i_cell,weight,mem,ctx_err)
    implicit none
    type(t_domain)                      ,intent(in)   :: ctx_grid
    type(t_discretization)              ,intent(in)   :: ctx_dg
    integer                             ,intent(in)   :: ixmin,ixmax,iymin
    integer                             ,intent(in)   :: izmin,izmax,iymax
    real(kind=8)                        ,intent(in)   :: dx,dy,dz,ox,oy,oz
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: i_cell(:,:,:)
    type(t_array1d_real)    ,allocatable,intent(inout):: weight(:,:,:)
    integer(kind=8)                     ,intent(inout):: mem
    type(t_error)                       ,intent(inout):: ctx_err
    !! local
    type(t_basis)            :: basis
    integer                  :: ix,iy,iz,order,i_order
    integer                  :: i_dof,ndof_vol
    integer(kind=IKIND_MESH) :: local_cell
    real(kind=8)             :: coo2d(2),coo3d(3),coo1d(1)
    real(kind=RKIND_POL)     :: coo_loc2d(2),coo_loc3d(3),coo_loc1d(1)
    real(kind=RKIND_POL)     :: coeff
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! depends on the dimension 
    select case(ctx_grid%dim_domain)
      ! ----------------------------------------------------------------
      !! 1-dimensional domain
      ! ----------------------------------------------------------------
      case(1)

        do ix=ixmin,ixmax 
          local_cell = i_cell(ix-ixmin+1,1,1)
          if(local_cell > 0) then
            !! coordinates
            coo1d(1) = dble(ox) + dble((ix-1)*dx)
            order    = ctx_grid%order         (local_cell)
            i_order  = ctx_dg%order_to_index  (order)
            ndof_vol = ctx_dg%n_dof_per_order (i_order)
            !! allocate weight
            call array_allocate(weight(ix-ixmin+1,1,1),ndof_vol,ctx_err)
            !! memory update
            mem = mem + (ndof_vol * RKIND_POL)
            weight(ix-ixmin+1,1,1)%array = 0.0
            !! evaluate the basis function: 
            !!   1) get the local coordinates on the reference element
            !!   2) evaluate the basis function at this location 
            coo_loc1d(:) = coo1d(:) - &
                         ctx_grid%x_node(:,ctx_grid%cell_node(1,local_cell))
            coo_loc1d(:) = ctx_dg%inv_jac(1,1,local_cell) * &
                           real(coo_loc1d(1),kind=8)
            !! compute basis function
            call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
            do i_dof=1,ndof_vol
              call polynomial_eval(basis%pol(i_dof),coo_loc1d(1),coeff,ctx_err)
              weight(ix-ixmin+1,1,1)%array(i_dof) = coeff
            end do
            call basis_clean(basis)
          end if
        end do

      ! ----------------------------------------------------------------
      !! 2-dimensional domain
      ! ----------------------------------------------------------------
      case(2)

        do ix=ixmin,ixmax ; do iz=izmin,izmax
          local_cell = i_cell(ix-ixmin+1,1,iz-izmin+1)
          if(local_cell > 0) then
            !! coordinates
            coo2d(1) = dble(ox) + dble((ix-1)*dx)
            coo2d(2) = dble(oz) + dble((iz-1)*dz)
            order    = ctx_grid%order         (local_cell)
            i_order  = ctx_dg%order_to_index  (order)
            ndof_vol = ctx_dg%n_dof_per_order (i_order)
            !! allocate weight
            call array_allocate(weight(ix-ixmin+1,1,iz-izmin+1),ndof_vol,ctx_err)
            !! memory update
            mem = mem + (ndof_vol * RKIND_POL)
            weight(ix-ixmin+1,1,iz-izmin+1)%array = 0.0
            !! evaluate the basis function: 
            !!   1) get the local coordinates on the reference element
            !!   2) evaluate the basis function at this location 
            coo_loc2d(:) = coo2d(:) - &
                         ctx_grid%x_node(:,ctx_grid%cell_node(1,local_cell))
            coo_loc2d(:) = matmul(ctx_dg%inv_jac(:,:,local_cell),  &
                           real(coo_loc2d(:),kind=8))
            !! compute basis function
            call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
            do i_dof=1,ndof_vol
              call polynomial_eval(basis%pol(i_dof),coo_loc2d(1),       &
                                   coo_loc2d(2),coeff,ctx_err)
              weight(ix-ixmin+1,1,iz-izmin+1)%array(i_dof) = coeff
            end do
            call basis_clean(basis)
          end if
        end do ; end do
      
      ! ----------------------------------------------------------------
      !! 3-dimensional domain
      ! ----------------------------------------------------------------
      case(3)

        do ix=ixmin,ixmax ; do iy=iymin,iymax ; do iz=izmin,izmax
          local_cell = i_cell(ix-ixmin+1,iy-iymin+1,iz-izmin+1)
          if(local_cell > 0) then
            !! coordinates
            coo3d(1) = dble(ox) + dble((ix-1)*dx)
            coo3d(2) = dble(oy) + dble((iy-1)*dy)
            coo3d(3) = dble(oz) + dble((iz-1)*dz)
            order    = ctx_grid%order         (local_cell)
            i_order  = ctx_dg%order_to_index  (order)
            ndof_vol = ctx_dg%n_dof_per_order (i_order)
            !! allocate weight
            call array_allocate(weight(ix-ixmin+1,iy-iymin+1,iz-izmin+1), &
                                ndof_vol,ctx_err)
            weight(ix-ixmin+1,iy-iymin+1,iz-izmin+1)%array = 0.0
            !! memory update
            mem = mem + (ndof_vol * RKIND_POL)

            !! evaluate the basis function: 
            !!   1) get the local coordinates on the reference element
            !!   2) evaluate the basis function at this location 
            !! remove offset from first node which is the ref (0,0,0)
            coo_loc3d(:) = coo3d(:) - &
                           ctx_grid%x_node(:,ctx_grid%cell_node(1,local_cell))
            coo_loc3d(:) = matmul(ctx_dg%inv_jac(:,:,local_cell),  &
                           real(coo_loc3d(:),kind=8))
            !! compute basis function
            call basis_construct(ctx_grid%dim_domain,order,basis,ctx_err)
            do i_dof=1,ndof_vol
              call polynomial_eval(basis%pol(i_dof),coo_loc3d(1),       &
                                   coo_loc3d(2),coo_loc3d(3),coeff,ctx_err)
              weight(ix-ixmin+1,iy-iymin+1,iz-izmin+1)%array(i_dof) = coeff
            end do
            call basis_clean(basis)
          end if
        end do ; end do ; end do

      ! ----------------------------------------------------------------
      !! Unrecognized dimension 
      ! ----------------------------------------------------------------
      case default
        ctx_err%msg   = "** ERROR: Unrecognized domain dimension "// &
                        "[dg_cartesian_map_weight_lagrange] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
   
    return
  end subroutine dg_cartesian_map_weight_lagrange

end module m_dg_lagrange_map_cartesian
