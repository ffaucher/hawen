!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_index_closest_dof.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module computes the index of the closest dof to a given position.
!
!------------------------------------------------------------------------------
module m_dg_lagrange_index_closest_dof

  use m_raise_error,                  only : t_error, raise_error
  use m_define_precision,             only : IKIND_MESH
  use m_ctx_domain,                   only : t_domain
  !! to find the dof position
  use m_dg_lagrange_dof_coordinates, only:                              &
                                       discretization_dof_coo_loc_elem

  implicit none
  

  interface lagrange_simplex_closest_dof_ind
     module procedure simplex_closest_ind_solo
  end interface lagrange_simplex_closest_dof_ind
  
  private
  public :: lagrange_simplex_closest_dof_ind
  
  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes, for a given position, the index of the 
  !>  closest dof and also provides the number of volumic dof.
  !> @details
  !
  !> @param[in]    mesh            : mesh context
  !> @param[in]    i_cell          : index of the cell that contains the pt
  !> @param[in]    xpt             : order for the representation
  !> @param[in]    xpt             : point to look for
  !> @param[out]   ind_out         : index of the closest dof
  !> @param[out]   ndof            : ndof on the cell
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine simplex_closest_ind_solo(ctx_mesh,i_cell,order,xpt,ind_out,&
                                      ndof,ctx_err)

    implicit none

    type(t_domain)          ,intent(in)   :: ctx_mesh
    integer(kind=4)         ,intent(in)   :: order
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real(kind=8)            ,intent(in)   :: xpt(:)
    integer(kind=4)         ,intent(out)  :: ind_out
    integer(kind=4)         ,intent(out)  :: ndof
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: dim_domain, i_dof
    real(kind=8)              :: dist_ref, dist_loc
    real(kind=8), allocatable :: dof_coo(:,:)
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0

    ind_out = 0
    ndof    = 0
    if(order == 0) then
      ind_out = 1
      ndof    = 1
      return
    endif
    
    dim_domain   = ctx_mesh%dim_domain
    
    !! -----------------------------------------------------------------
    select case(dim_domain)
      case(1)
        ndof = order+1
      case(2)
        ndof = (order+2)*(order+1)/2
      case(3)
        ndof = (order+3)*(order+2)*(order+1)/6
      case default
        ctx_err%msg   = "** ERROR: Unrecognized Dimension [model_dof_ind] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! -----------------------------------------------------------------

    allocate(dof_coo(dim_domain,ndof))
    !! we use the local position on the element
    dof_coo=0.d0
    call discretization_dof_coo_loc_elem(ctx_mesh,i_cell,order,ndof,    &
                                         dof_coo,ctx_err)   
    !! compute the distance of the input point w.r.t. all dof coo
    dist_ref=huge(1.d0)
    dist_loc=huge(1.d0)
    do i_dof = 1, ndof
      dist_loc = norm2(xpt(:) - dof_coo(:,i_dof))
      if(dist_loc < dist_ref) then
        dist_ref = dist_loc
        ind_out = i_dof
      end if
    end do
    deallocate(dof_coo)

    if(ind_out<=0 .or. ind_out>ndof) then
      ctx_err%msg   = "** ERROR: Index position surprisingly not " //   &
                      "found [closest_dof_ind] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    return
  end subroutine simplex_closest_ind_solo

end module m_dg_lagrange_index_closest_dof
