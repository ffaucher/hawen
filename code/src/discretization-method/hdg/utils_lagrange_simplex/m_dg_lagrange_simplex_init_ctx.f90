!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_simplex_init_ctx.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module initializes the t_dg context with reference matrix 
!> in the case of Lagrange Basis Functions over Simplex cells.
!> The reference matrices are saved here to avoid their recomputation 
!> during the processes (particularly important during inversion as 
!> it would be repeated many times).
!
!------------------------------------------------------------------------------
module m_dg_lagrange_simplex_init_ctx

  use m_raise_error,         only : t_error, raise_error
  use m_distribute_error,    only : distribute_error
  use m_tag_bc,              only : tag_ghost_cell
  use m_ctx_parallelism,     only : t_parallelism
  use m_allreduce_sum,       only : allreduce_sum
  use m_allreduce_max,       only : allreduce_max
  use m_allreduce_min,       only : allreduce_min
  use m_barrier,             only : barrier
  use m_send,                only : send
  use m_receive,             only : receive
  use m_array_types,         only : array_allocate
  use m_ctx_domain,          only : t_domain
  use m_define_precision,    only : IKIND_MAT,IKIND_MESH,RKIND_POL,RKIND_MAT
  use m_ctx_discretization,  only : t_discretization
  !> lagrange operations -------------------------------------------------------
  use m_polynomial,       only : polynomial_precomput_construct,               &
                                 polynomial_precomput_clean,                   &
                                 t_polynomial_precomput, t_polynomial,         &
                                 polynomial_multiplication, polynomial_clean,  &
                                 polynomial_eval
  use m_basis_functions,  only : t_basis,basis_construct,basis_face_construct, &
                                 basis_derivative_construct,                   &
                                 basis_clean
  use m_lagrange_matrix,  only : lagrange_vol_construct, t_lagrange_vol,       &
                                 lagrange_vol_clean, t_lagrange_surf,          &
                                 lagrange_surf_construct,                      &
                                 lagrange_surf_clean
  use m_dg_lagrange_simplex_dof, &
                          only : lagrange_simplex_face_dof_index

  implicit none

  private
  public :: dg_init_lagrange_simplex, dg_lagrange_init_ref_matrix
  public :: dg_lagrange_init_quadrature_vector
  public :: dg_lagrange_init_quadrature_vector_triple
  private:: initialize_cells_dof, face_map_create
  
  contains
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init General DG context, in particular from the mesh infos
  !
  !> @param[in]    ctx_paral       : parallelism context
  !> @param[inout] ctx_dg          : DG context
  !> @param[inout] ctx_mesh        : mesh context
  !> @param[in]    flag_inv        : flag to indicate if inversion
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine dg_init_lagrange_simplex(ctx_paral,ctx_dg,ctx_mesh,flag_inv,ctx_err)
    
    implicit none

    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(inout):: ctx_dg
    type(t_domain)         ,intent(in)   :: ctx_mesh
    logical                ,intent(in)   :: flag_inv
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    integer           :: k, i_order, l, n_dof
    integer           :: max_ndof_face, max_ndof_cell
    integer(kind=8)   :: mem
    integer(kind=8)   :: int_size,int_size_mesh,real_size
    integer(kind=8)   :: real_size_pol,cx_size_mat,int_size_mat
    !! for tight allocation
    integer                    :: n_dof_face, n_dof_vol, iface
    integer(kind=IKIND_MESH)   :: i_cell
    ctx_err%ierr  = 0

    !! initialize memory information for the dg context 
    int_size      = 4 ; real_size = 4
    int_size_mesh = IKIND_MESH
    int_size_mat  = IKIND_MAT 
    real_size_pol = RKIND_POL 
    cx_size_mat   = RKIND_MAT
    mem = (36*1)               + & !! 36 characters
          ( 5*int_size)        + & !!  5 integers
          ( 4*int_size_mesh)   + & !!  4 integers mesh
          !! offsetdof_solution x 2
          (2*ctx_mesh%n_cell_loc)*int_size_mesh
    !! offsetdof_mat depends on dg or hdg
    mem = mem + (2*ctx_mesh%n_face_loc)*int_size_mesh

    !! -----------------------------------------------------------------
    !! first we compute the list of dof for every processors.
    !! Depending on the order, we get 
    !!  -> ndof for each element (VOLUMIC and FACE)
    !!  -> ndof for the matrix variable and solution variable
    !!  -> mpi infos so that the master can split the global
    !!     solution towards the slave processors
    !! -----------------------------------------------------------------
    call initialize_cells_dof(ctx_paral,ctx_dg,ctx_mesh,ctx_err)
    !! -----------------------------------------------------------------
    !! update memory
    !! order_to_index; index_to_order; n_dof_per_order; n_dof_face_per_order 
    mem = mem + 4*int8(ctx_dg%n_different_order)*int_size
    do k = 1, ctx_dg%n_different_order
      !! memory for face_map
      mem = mem + int8(ctx_dg%n_dof_face_per_order(k))*                 &
                  int8(ctx_mesh%n_neigh_per_cell)*int8(real_size_pol)
    end do
    !! update memory mpi
    mem = mem + int8(int_size_mat)*int8(ctx_paral%nproc) + & ! mpi_ndofvar_count
                int8(int_size_mat)*int8(ctx_paral%nproc) + & ! mpi_nface_count
                ! mpi_dofvar_list (master only)
                int8(int_size_mat)*int8(2*sum(ctx_dg%mpi_nface_count(:)))


    !! -----------------------------------------------------------------
    !! The reference matrix cannot be created here, it depends on the 
    !! equation and on the model representation (e.g. if not piecewise)
    !! constant, it has to be done on the fly.
    !! -----------------------------------------------------------------
    ctx_dg%flag_refmatrix   = .false.

    !! -----------------------------------------------------------------
    !! HDG specific allocations
    !! -----------------------------------------------------------------
    max_ndof_cell= ctx_dg%dim_sol*maxval(ctx_dg%n_dof_per_order     (:))
    max_ndof_face= ctx_dg%dim_var*maxval(ctx_dg%n_dof_face_per_order(:)) * &
                   ctx_mesh%n_neigh_per_cell
    
    !! loop over all cells to have the correct size on each of them.
    allocate(ctx_dg%hdg%Ainv(ctx_mesh%n_cell_loc))
    allocate(ctx_dg%hdg%Q   (ctx_mesh%n_cell_loc))
    allocate(ctx_dg%hdg%X   (ctx_mesh%n_cell_loc))
    if(flag_inv) then
      allocate(ctx_dg%hdg%W(ctx_mesh%n_cell_loc))
      allocate(ctx_dg%hdg%D(ctx_mesh%n_cell_loc))
    end if

    do i_cell=1,ctx_mesh%n_cell_loc
      !! current ndof volumic and surface depending on order
      k          = ctx_dg%order_to_index(ctx_mesh%order(i_cell))
      n_dof_vol  = ctx_dg%n_dof_per_order     (k)*ctx_dg%dim_sol
      n_dof_face = 0
      do iface=1,ctx_mesh%n_neigh_per_cell
        k = ctx_dg%order_to_index(ctx_mesh%order_face(                  &
                                  ctx_mesh%cell_face(iface,i_cell)))
        n_dof_face = n_dof_face                                         &
                   + ctx_dg%n_dof_face_per_order(k)*ctx_dg%dim_var
      end do
      
      mem = mem &
         !! Ainv
         + int8(n_dof_vol)*int8(n_dof_vol)*int8(cx_size_mat)            &
         !! Q
         + int8(n_dof_vol)*int8(n_dof_face)*int8(cx_size_mat)           &
         !! X
         + int8(n_dof_vol)*int8(n_dof_face)*int8(cx_size_mat)

      call array_allocate(ctx_dg%hdg%Ainv(i_cell),n_dof_vol,n_dof_vol ,ctx_err)
      call array_allocate(ctx_dg%hdg%Q   (i_cell),n_dof_vol,n_dof_face,ctx_err)
      call array_allocate(ctx_dg%hdg%X   (i_cell),n_dof_face,n_dof_vol,ctx_err)
      
      !! for inversion
      if(flag_inv) then
        mem = mem &
          !! W
          + int8(n_dof_vol)*int8(n_dof_face)*int8(cx_size_mat)          &
          !! D
          + int8(n_dof_vol)*int8(n_dof_face)*int8(cx_size_mat)
        
        call array_allocate(ctx_dg%hdg%W(i_cell),n_dof_face,n_dof_vol,ctx_err)
        call array_allocate(ctx_dg%hdg%D(i_cell),n_dof_vol,n_dof_face,ctx_err)
      endif      
    end do
    
    !! mem = mem &
    !!     !! Ainv
    !!     + int8(ctx_mesh%n_cell_loc)*int8(max_ndof_cell*max_ndof_cell)*  &
    !!       int8(cx_size_mat)                                             &
    !!     !! Q
    !!     + int8(ctx_mesh%n_cell_loc)*int8(max_ndof_cell)*                &
    !!       int8(max_ndof_face)*int8(cx_size_mat)                         &
    !!     !! X
    !!     + int8(ctx_mesh%n_cell_loc)*int8(max_ndof_cell)*                &
    !!       int8(max_ndof_face)*int8(cx_size_mat)
    !! if(flag_inv) then
    !!   mem = mem &
    !!     !! W
    !!     + int8(ctx_mesh%n_cell_loc)*int8(max_ndof_face)*                &
    !!       int8(max_ndof_cell)* int8(cx_size_mat)                        &
    !!     !! D
    !!     + int8(ctx_mesh%n_cell_loc)*int8(max_ndof_face)*                &
    !!       int8(max_ndof_cell)*int8(cx_size_mat)
    !! endif

    !! allocate(ctx_dg%hdg%Ainv(max_ndof_cell,max_ndof_cell,ctx_mesh%n_cell_loc))
    !! allocate(ctx_dg%hdg%Q   (max_ndof_cell,max_ndof_face,ctx_mesh%n_cell_loc))
    !! allocate(ctx_dg%hdg%X   (max_ndof_face,max_ndof_cell,ctx_mesh%n_cell_loc))
    !! if(flag_inv) then
    !!   allocate(ctx_dg%hdg%W(max_ndof_face,max_ndof_cell,ctx_mesh%n_cell_loc))
    !!   allocate(ctx_dg%hdg%D(max_ndof_cell,max_ndof_face,ctx_mesh%n_cell_loc))
    !! endif
    !! -----------------------------------------------------------------

    !! pml coeff: each cell has the coeff per cell (double complex)
    !!            and a coeff per face.
    mem = mem + int8(ctx_mesh%n_cell_loc+ ctx_mesh%n_ghost_loc) *       &
                int8(ctx_mesh%dim_domain * 2*8)
    allocate(ctx_dg%pml_cell_coeff(ctx_mesh%dim_domain,                 &
                                   ctx_mesh%n_cell_loc+ctx_mesh%n_ghost_loc))
    ctx_dg%pml_cell_coeff = dcmplx(1.d0,0.d0) !! all set to one at first

    mem = mem + int8(ctx_mesh%n_face_loc) * int8(ctx_mesh%dim_domain)*int8(2*8)
    allocate(ctx_dg%pml_face_coeff(ctx_mesh%dim_domain,ctx_mesh%n_face_loc))
    ctx_dg%pml_face_coeff = dcmplx(1.d0,0.d0) !! all set to one at first

    !! -----------------------------------------------------------------
    !! for all interfaces, we need:
    !!  -> for all orientation, we need the corresponding 
    !!     dof numbering
    !! -----------------------------------------------------------------
    allocate(ctx_dg%hdg%idof_face(ctx_dg%hdg%n_interface_combi,         &
                                  ctx_dg%n_different_order))
    do k=1, ctx_dg%n_different_order         
      n_dof   = ctx_dg%n_dof_face_per_order(k)
      i_order = ctx_dg%index_to_order(k)
      do l=1,ctx_dg%hdg%n_interface_combi
        call array_allocate(ctx_dg%hdg%idof_face(l,k),n_dof,ctx_err)
        mem = mem + int8(n_dof*int_size)
        !! this is the routine for the interface dof numbering
        call lagrange_simplex_face_dof_index(ctx_dg%dim_domain,l,       &
                                       i_order,n_dof,                   &
                                       ctx_dg%hdg%idof_face(l,k)%array, &
                                       ctx_err)
      end do
    end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! we allocate the array to ensure that the matrix will be symmetric
    !! only filled with 0 for now, the col, line and  val are empty here
    !! because they are of array_types
    !! -----------------------------------------------------------------
    allocate(ctx_dg%ctx_symmetrize_mat%cell_nval(ctx_mesh%n_cell_loc))
    allocate(ctx_dg%ctx_symmetrize_mat%c_line   (ctx_mesh%n_cell_loc))
    allocate(ctx_dg%ctx_symmetrize_mat%c_col    (ctx_mesh%n_cell_loc))
    allocate(ctx_dg%ctx_symmetrize_mat%c_val    (ctx_mesh%n_cell_loc))
    !! will be set to true if the PDE leads to symmetric matrix.
    ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize = .false. 
    allocate(ctx_dg%ctx_symmetrize_mat%f_matbc  (ctx_mesh%n_face_loc,   &
                                                 ctx_dg%dim_var))
    !! the source array for velocity in case of planewaves is allocated
    allocate(ctx_dg%face_velocity(ctx_mesh%n_face_loc))
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0
    !! it does not include the number of coeff per cell, which should
    !! remain small as only a few cells are on the boundary.
    mem = mem + ctx_mesh%n_cell_loc*int_size*4                          &
              + ctx_mesh%n_face_loc*ctx_dg%dim_var*int_size*4           &
              + ctx_mesh%n_face_loc*real_size*2*4
    !! -----------------------------------------------------------------

    ctx_dg%memory_loc = mem

    return

  end subroutine dg_init_lagrange_simplex

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> we compute the dof infos for the cells
  !> Depending on the order, we get 
  !>  -> ndof for each element (VOLUMIC and FACE)
  !>  -> ndof for the matrix variable and solution variable
  !>  -> mpi infos so that the master can split the global
  !>     solution towards the slave processors
  !
  !> @param[inout] ctx_paral       : parallelism context
  !> @param[inout] ctx_dg          : DG context
  !> @param[inout] ctx_mesh        : mesh context
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine initialize_cells_dof(ctx_paral,ctx_dg,ctx_mesh,ctx_err)
    
    implicit none

    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(inout):: ctx_dg
    type(t_domain)         ,intent(in)   :: ctx_mesh
    type(t_error)          ,intent(out)  :: ctx_err
    !! local 
    integer             ,allocatable :: temp_order(:), temp_order_gb(:)
    integer             ,allocatable :: alldofsol_loc(:), alldofvar_loc(:)
    integer             ,allocatable :: alldofsol_gb (:), alldofvar_gb(:)
    integer,             allocatable :: sumarray_dofvar_gb (:)
    integer,             allocatable :: sumarray_dofvar_loc(:)
    integer,             allocatable :: sumarray_dofsol(:)
    integer             ,allocatable :: dof_var_loc(:)
    integer(kind=IKIND_MESH)         :: i_cell, i_gb, ind_face, i_loc
    integer                          :: temp_o, order_max, i_order, order_min
    integer                          :: counter, n_dof_face, n_face, k
    integer                          :: order,ndof_loc
    integer(kind=IKIND_MAT)          :: ndof_gbloc,i_face
    !! partition
    integer                          :: tag
    integer,allocatable              :: proc_info(:)
    integer,allocatable              :: proc_info_indexdof(:,:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    !! order max and array of different orders 
    temp_o = maxval(ctx_mesh%order)
    call allreduce_max(temp_o,order_max,1,ctx_paral%communicator,ctx_err)
    ctx_dg%order_max_gb = order_max


    !! -----------------------------------------------------------------
    !! This is related to the basis functions currently implemented,
    !! for instance if one use the reference element method, it cannot
    !! work if the order is too high except if quadruple
    !! -----------------------------------------------------------------
    !! error if we have 2D and order > 8, it has not been validated.
    if(order_max > 9 .and. RKIND_POL < 16) then
      ctx_err%msg   = "** ERROR: polynomial order > 9 should not be "// &
                      "used except if you allow for quadruple "      // &
                      "precision for the polynomials in the file "   // &
                      "m_define_precision.f90 [init_cells_dof] **"
      ctx_err%ierr  = -201
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! *****************************************************************
     
    temp_o = minval(ctx_mesh%order)
    call allreduce_min(temp_o,order_min,1,ctx_paral%communicator,ctx_err)
    ctx_dg%order_min_gb = order_min

    !! check how many we have
    allocate(temp_order   (0:order_max)) ; temp_order   =0
    allocate(temp_order_gb(0:order_max)) ; temp_order_gb=0
    do i_cell =1,ctx_mesh%n_cell_loc
      temp_order(ctx_mesh%order(i_cell)) =                             &
                                  temp_order(ctx_mesh%order(i_cell)) + 1
    end do
    call allreduce_sum(temp_order,temp_order_gb,order_max+1,           &
                       ctx_paral%communicator,ctx_err)

    !! count how many different and associate index
    ctx_dg%n_different_order = 0
    allocate(ctx_dg%order_to_index(0:order_max))
    ctx_dg%order_to_index = 0
    !! -----------------------------------------------------------------
    !! in one-dimension, there must be order 1 for the face information
    !! it is actually an order 0 locally but we say it is order 1 face
    !! -----------------------------------------------------------------
    if(ctx_mesh%dim_domain == 1 .and. temp_order_gb(1) == 0) then
      ctx_dg%n_different_order       = ctx_dg%n_different_order + 1
      ctx_dg%order_to_index(1)       = ctx_dg%n_different_order
    end if
    !! -----------------------------------------------------------------
    do i_order = 0, order_max
      if(temp_order_gb(i_order) .ne. 0) then
        ctx_dg%n_different_order       = ctx_dg%n_different_order + 1
        ctx_dg%order_to_index(i_order) = ctx_dg%n_different_order
      end if
    end do
    !! -----------------------------------------------------------------

    !! revert array
    counter=0
    allocate(ctx_dg%index_to_order(ctx_dg%n_different_order))
    do i_order = 0, order_max
      if(ctx_dg%order_to_index(i_order) .ne. 0) then
        counter=counter+1
        ctx_dg%index_to_order(counter) = i_order
      end if
    end do

    !! ------------------------------------------------------------------
    !! compute the appropriate number of dof per element and per face
    allocate(ctx_dg%n_dof_per_order     (ctx_dg%n_different_order))
    allocate(ctx_dg%n_dof_face_per_order(ctx_dg%n_different_order))
    allocate(ctx_dg%face_map            (ctx_dg%n_different_order))
    
    do k = 1, ctx_dg%n_different_order
      i_order = ctx_dg%index_to_order(k)

      select case(ctx_dg%dim_domain)
        case(1)
          ctx_dg%n_dof_per_order     (k) =  i_order+1
          ctx_dg%n_dof_face_per_order(k) =  1
        case(2)
          ctx_dg%n_dof_per_order     (k) = (i_order+2)*(i_order+1)/2
          ctx_dg%n_dof_face_per_order(k) =  i_order+1
        case(3)
          ctx_dg%n_dof_per_order     (k) = (i_order+3)*(i_order+2)*(i_order+1)/6
          ctx_dg%n_dof_face_per_order(k) = (i_order+2)*(i_order+1)/2
        case default
          ctx_err%msg   = "** ERROR: Unrecognized Dimension [dg_init] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      
      !! Compute map informations
      n_dof_face = ctx_dg%n_dof_face_per_order(k)
      n_face     = ctx_mesh%n_neigh_per_cell
     
      call array_allocate(ctx_dg%face_map(k),n_dof_face,n_face,ctx_err)
      
      !! compute correspondance between face DOF and volume DOF
      call face_map_create(ctx_dg%dim_domain,i_order,                  &
                           ctx_dg%face_map(k)%array,ctx_err)
    end do

    !! -----------------------------------------------------------------
    !!
    !! compute the local and global N_DOF and OFFSET for the solution 
    !! without counting the ghosts
    !!
    !! -----------------------------------------------------------------
    ctx_dg%n_dofloc_persol= 0 ; ctx_dg%n_dofgb_persol = 0 
    do k = 1, ctx_dg%n_different_order
      ctx_dg%n_dofloc_persol=ctx_dg%n_dofloc_persol+ ctx_dg%n_dof_per_order(k)*&
                                           temp_order(ctx_dg%index_to_order(k))
      ctx_dg%n_dofgb_persol =ctx_dg%n_dofgb_persol + ctx_dg%n_dof_per_order(k)*&
                                        temp_order_gb(ctx_dg%index_to_order(k))
    end do
    deallocate(temp_order)
    deallocate(temp_order_gb)

    !! DG offset dof list solution -------------------------------------
    allocate(ctx_dg%offsetdof_sol_loc(ctx_mesh%n_cell_loc))
    ctx_dg%offsetdof_sol_loc=0
    allocate(alldofsol_loc(ctx_mesh%n_cell_gb)) ; alldofsol_loc = 0
    do i_cell = 1, ctx_mesh%n_cell_loc
      i_gb                = ctx_mesh%index_loctoglob_cell(i_cell)
      i_order             = ctx_mesh%order               (i_cell)
      k                   = ctx_dg%order_to_index(i_order)     
      alldofsol_loc(i_gb) = ctx_dg%n_dof_per_order(k)
      if(i_cell.ne.ctx_mesh%n_cell_loc) then
        ctx_dg%offsetdof_sol_loc(i_cell+1) = ctx_dg%n_dof_per_order(k) + &
                                             ctx_dg%offsetdof_sol_loc(i_cell)
      endif
    end do

    !! allreduce for incremental offset array for solution 
    allocate(alldofsol_gb (ctx_mesh%n_cell_gb)) ; alldofsol_gb  = 0   
    !! must use int4 for mpi
    call allreduce_sum(alldofsol_loc,alldofsol_gb,int(ctx_mesh%n_cell_gb), &
                       ctx_paral%communicator,ctx_err)
    deallocate(alldofsol_loc)

    !! we construct the array which consecutively sums
    allocate(sumarray_dofsol(ctx_mesh%n_cell_gb))
    sumarray_dofsol = 0
    do i_cell = 2, ctx_mesh%n_cell_gb
      sumarray_dofsol (i_cell) = sumarray_dofsol (i_cell-1) +           &
                                 alldofsol_gb(i_cell-1)
    end do
    deallocate(alldofsol_gb)

    allocate(ctx_dg%offsetdof_sol_gb(ctx_mesh%n_cell_loc))
    ctx_dg%offsetdof_sol_gb =0
    do i_cell = 1, ctx_mesh%n_cell_loc
      i_gb = ctx_mesh%index_loctoglob_cell(i_cell)
      ctx_dg%offsetdof_sol_gb(i_cell) = sumarray_dofsol(i_gb)
    end do
    deallocate(sumarray_dofsol)

    !! -----------------------------------------------------------------
    !!
    !! compute the local and global N_DOF and OFFSET for the matrix var
    !!                  [DEPENDS ON THE TYPE OF DG]
    !! 
    !! -----------------------------------------------------------------
    ctx_dg%n_dofloc_pervar= 0 ; ctx_dg%n_dofgb_pervar = 0 
    !! count matrix variables per faces
    allocate(alldofvar_loc(ctx_mesh%n_face_gb)) ; alldofvar_loc = 0
    allocate(dof_var_loc  (ctx_mesh%n_face_gb)) ; dof_var_loc   = 0
    ndof_gbloc=0
    do i_cell = 1, ctx_mesh%n_cell_loc
      do i_face = 1, ctx_mesh%n_neigh_per_cell

        !! shared faces dof: add only if the orientation is IN 
        !! for multi-processor, if it is a ghost, it must be added 
        !! to the LOCAL count only
        if(ctx_mesh%cell_face_orientation(i_face,i_cell)==1        .or. &
           ctx_mesh%cell_neigh(i_face,i_cell) .eq. tag_ghost_cell) then
          !! get order
          ind_face= ctx_mesh%cell_face           (i_face,i_cell)
          i_gb    = ctx_mesh%index_loctoglob_face(ind_face)
          order   = ctx_mesh%order_face          (ind_face)
          i_order = ctx_dg%order_to_index        (order)
          ndof_loc= ctx_dg%n_dof_face_per_order(i_order)
          !! update matrix dof and current cell ndof only if main elements
          ctx_dg%n_dofloc_pervar = ctx_dg%n_dofloc_pervar + ndof_loc
          if(ctx_mesh%cell_face_orientation(i_face,i_cell)==1) then
            alldofvar_loc(i_gb) = alldofvar_loc(i_gb) + ndof_loc
            ndof_gbloc = ndof_gbloc + ndof_loc
          end if
          !! we sum offsets afterwards
          dof_var_loc(ind_face) = ndof_loc
        end if

      end do
    end do

    !! global offset for matrix creation
    call allreduce_sum(ndof_gbloc,ctx_dg%n_dofgb_pervar,1,    &
                       ctx_paral%communicator,ctx_err)
    allocate(alldofvar_gb (ctx_mesh%n_face_gb)) ; alldofvar_gb=0
    !! must force int4 count for mpi <!
    call allreduce_sum(alldofvar_loc,alldofvar_gb,                  &
                       int(ctx_mesh%n_face_gb),                     &
                       ctx_paral%communicator,ctx_err)
    deallocate(alldofvar_loc)

    !! we construct the resulting array which consecutively sums the
    !! values of the ndof.
    allocate(sumarray_dofvar_gb (ctx_mesh%n_face_gb))
    allocate(sumarray_dofvar_loc(ctx_mesh%n_face_gb))
    sumarray_dofvar_loc = 0
    sumarray_dofvar_gb  = 0
    do i_face = 2, ctx_mesh%n_face_gb
      sumarray_dofvar_gb (i_face) = sumarray_dofvar_gb (i_face-1) +     &
                                    alldofvar_gb(i_face-1)
      sumarray_dofvar_loc(i_face) = sumarray_dofvar_loc(i_face-1) +     &
                                    dof_var_loc (i_face-1)
    end do
    deallocate(alldofvar_gb)
    deallocate(dof_var_loc)

    !! global and local offset
    allocate(ctx_dg%offsetdof_mat_gb(ctx_mesh%n_face_loc))
    ctx_dg%offsetdof_mat_gb = 0
    allocate(ctx_dg%offsetdof_mat_loc(ctx_mesh%n_face_loc))
    ctx_dg%offsetdof_mat_loc=0

    do i_cell = 1, ctx_mesh%n_cell_loc
      do i_face = 1, ctx_mesh%n_neigh_per_cell
        i_loc = ctx_mesh%cell_face           (i_face,i_cell)
        i_gb  = ctx_mesh%index_loctoglob_face(i_loc)
        ctx_dg%offsetdof_mat_gb (i_loc) = sumarray_dofvar_gb (i_gb)
        ctx_dg%offsetdof_mat_loc(i_loc) = sumarray_dofvar_loc(i_loc)
      end do
    end do
    deallocate(sumarray_dofvar_gb )
    deallocate(sumarray_dofvar_loc)

    !! -----------------------------------------------------------------
    !! Save processors dof indexes because of partition
    !! only the master needs infos so that it can send the
    !! solution. This is due to the direct solver MUMPS which
    !! put the solution on master node.
    !! -----------------------------------------------------------------
    !! general infos
    allocate(proc_info               (ctx_paral%nproc))
    allocate(ctx_dg%mpi_ndofvar_count(ctx_paral%nproc))
    allocate(ctx_dg%mpi_nface_count  (ctx_paral%nproc))
    proc_info               = 0
    ctx_dg%mpi_ndofvar_count= 0
    ctx_dg%mpi_nface_count  = 0
    proc_info(ctx_paral%myrank+1) = int(ctx_dg%n_dofloc_pervar,kind=4)
    call allreduce_sum(proc_info,ctx_dg%mpi_ndofvar_count,ctx_paral%nproc, &
                       ctx_paral%communicator,ctx_err)
    proc_info=0
    proc_info(ctx_paral%myrank+1) = int(ctx_mesh%n_face_loc)
    call allreduce_sum(proc_info,ctx_dg%mpi_nface_count,ctx_paral%nproc,   &
                       ctx_paral%communicator,ctx_err)
    deallocate(proc_info)

    !! list of indexes
    allocate(proc_info_indexdof(ctx_mesh%n_face_loc,2))
    proc_info_indexdof = 0
    do ind_face = 1,ctx_mesh%n_face_loc
      order   = ctx_mesh%order_face          (ind_face)
      i_order = ctx_dg%order_to_index        (order)
      ndof_loc= ctx_dg%n_dof_face_per_order(i_order)
      proc_info_indexdof(ind_face,1) = &
                      int(ctx_dg%offsetdof_mat_gb(ind_face)+1,kind=4)
      proc_info_indexdof(ind_face,2) = ndof_loc
    end do
    
    if (ctx_paral%master) then
      !! allocate and receive lists
      allocate(ctx_dg%mpi_dofvar_list(ctx_paral%nproc))
      call array_allocate(ctx_dg%mpi_dofvar_list(1),               &
                          ctx_dg%mpi_nface_count(1),2,ctx_err)
      ctx_dg%mpi_dofvar_list(1)%array(:,:)=proc_info_indexdof
      
      do k=2,ctx_paral%nproc
        call array_allocate(ctx_dg%mpi_dofvar_list(k),             &
                            ctx_dg%mpi_nface_count(k),2,ctx_err)
        !! receive from (k-1)
        tag=k
        call receive(ctx_dg%mpi_dofvar_list(k)%array,              &
                     int(2*ctx_dg%mpi_nface_count(k)),(k-1),tag,   &
                     ctx_paral%communicator,ctx_err)
      end do
    else
      !! send
      tag=ctx_paral%myrank+1
      !! We must force int4 type for the number of stuff sent 
      call send(proc_info_indexdof,int(2*ctx_mesh%n_face_loc),0, &
                tag,ctx_paral%communicator,ctx_err)
    end if

    call barrier(ctx_paral%communicator,ctx_err)
    return

  end subroutine initialize_cells_dof

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Create the Lagrangian Basis functions that will be required 
  !> for the reference matrices, all present orders must be dealt with, 
  !> we will keep,for the reference element:
  !> VOLUMIC INFOS: 
  !>      \phi_i  \phi_j ==> mass matrix
  !>     d\phi_i  \phi_j
  !>      \phi_i d\phi_j
  !>     d\phi_i d\phi_j
  !> INTERFACE INFOS:
  !>      \phi_i  \phi_j ==> face mass matrix
  !
  !> @param[inout] ctx_dg          : DG context
  !> @param[in]    ctx_mesh        : mesh context
  !> @param[inout] mem             : update memory count
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine dg_lagrange_init_ref_matrix(ctx_paral,ctx_dg,ctx_mesh,mem,ctx_err)
    
    implicit none

    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(inout):: ctx_dg
    type(t_domain)         ,intent(in)   :: ctx_mesh
    integer(kind=8)        ,intent(inout):: mem
    type(t_error)          ,intent(out)  :: ctx_err
    !! local 
    type(t_basis)       ,allocatable :: basis_functions(:)
    type(t_lagrange_vol)             :: work_vol
    type(t_lagrange_surf)            :: work_surf
    type(t_polynomial_precomput)     :: precomp(3)
    integer                          :: k, i_order
    integer                          :: n_dof_l, l, i, j
    integer                          :: n_dof, i_dim1
    integer(kind=8)                  :: real_size_pol
    !! for the parallel construction 
    integer                          :: nfunc_loc,nfunc_tot,nfunc_rem
    integer                          :: func_col1,func_col2
    integer                          :: counter, ind_gb
    integer             ,allocatable :: nfunc_per_mpi    (:)
    integer             ,allocatable :: nfunc_per_mpi_sum(:)
    integer             ,allocatable :: func_ind(:,:)
    real(kind=RKIND_POL),allocatable :: buffer_dphi_phi(:,:,:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr=0

    !! initialize memory information for the dg context 
    real_size_pol = int8(RKIND_POL)

    allocate(basis_functions(ctx_dg%n_different_order))
    do k = 1, ctx_dg%n_different_order
      i_order = ctx_dg%index_to_order(k)
      !! construct basis
      call basis_construct(ctx_dg%dim_domain,i_order,basis_functions(k),ctx_err)
      !! construct first order derivative
      call basis_derivative_construct(basis_functions(k),1,ctx_err)
      !! construct 0th order face information
      call basis_face_construct(basis_functions(k),0,ctx_err)
    end do

    !! ------------------------------------------------------------------
    !! Create the Reference Lagrangian matrices for the different order
    !! ------------------------------------------------------------------
    allocate(ctx_dg%vol_matrix_mass     (ctx_dg%n_different_order))
    allocate(ctx_dg%vol_matrix_dphi_phi (ctx_dg%n_different_order))
    !! allocate(ctx_dg%vol_matrix_phi_dphi (ctx_dg%n_different_order))
    !! allocate(ctx_dg%vol_matrix_dphi_dphi(ctx_dg%n_different_order))
    allocate(ctx_dg%face_matrix_mass    (ctx_dg%n_different_order,    &
                                         ctx_dg%n_different_order))   

    do k = 1, ctx_dg%n_different_order
      !! initial informations
      i_order = ctx_dg%index_to_order (k)
      n_dof   = ctx_dg%n_dof_per_order(k)
      call polynomial_precomput_construct(precomp,ctx_dg%dim_domain,  &
                                          i_order,ctx_err)
      !! update memory  !! vol_matrix_mass
      mem = mem + (n_dof*n_dof)               *real_size_pol   + & 
            !! vol_matrix_dphi_phi, vol_matrix_phi_dphi
            (ctx_dg%dim_domain*n_dof*n_dof)*real_size_pol  !!  + &
            !! vol_matrix_dphi_dphi unnecessary in HDG
            ! (2*ctx_dg%dim_domain*ctx_dg%dim_domain*n_dof*n_dof)* &
            !      real_size_pol

      !! allocate the arrays
      call array_allocate(ctx_dg%vol_matrix_mass(k),n_dof,n_dof,ctx_err)
      call array_allocate(ctx_dg%vol_matrix_dphi_phi(k),n_dof,n_dof,    &
                          ctx_dg%dim_domain,ctx_err)
      !! call array_allocate(ctx_dg%vol_matrix_phi_dphi(k),n_dof,n_dof, &
      !!                     ctx_dg%dim_domain,ctx_err)
           
      !! for the volumic terms, we are going to compute
      !!         \integral_K (            \phi_i             \phi_j) dK
      !!         \integral_K (\partial_xk \phi_i             \phi_j) dK
      !!         \integral_K (\partial_xk \phi_i \partial_xl \phi_j) dK

      !! ===============================================================
      !! if we have multiple processors, we separate the computation 
      !! between each of them, otherwise, it just computes everything
      !! ===============================================================
      if(ctx_paral%nproc > 1) then
        
        !! -------------------------------------------------------------
        !! total number of basis functions
        nfunc_tot = n_dof*n_dof
        nfunc_loc = floor(dble(nfunc_tot/ctx_paral%nproc))
        !! how many remains
        nfunc_rem = nfunc_tot - nfunc_loc*ctx_paral%nproc
        !! all processors know how many in the previous ones.
        allocate(nfunc_per_mpi(ctx_paral%nproc))
        nfunc_per_mpi = nfunc_loc
        do i=1,nfunc_rem
          !! update local value and the global array
          if(ctx_paral%myrank == (i-1)) nfunc_loc = nfunc_loc + 1 
          nfunc_per_mpi(i) = nfunc_per_mpi(i) + 1 
        end do
        !! the summed array of size nproc + 1 
        allocate(nfunc_per_mpi_sum(ctx_paral%nproc+1))
        nfunc_per_mpi_sum = 0
        do i=2,ctx_paral%nproc+1
          nfunc_per_mpi_sum(i) = nfunc_per_mpi_sum(i-1) + nfunc_per_mpi(i-1)
        end do

        !! create the local array of indices, i.e., the (i,j) for the 
        !! computation of phi_i phi_j.
        allocate(func_ind(2,nfunc_loc))
        func_ind = 0
        !! loop over all array        
                    !! OK BECASE RANK STARTS AT 0
        func_col1 = floor  (dble(nfunc_per_mpi_sum(ctx_paral%myrank+1)/n_dof))
        func_col2 = ceiling(dble(nfunc_per_mpi_sum(ctx_paral%myrank+2)/n_dof))+1
        func_col1 = max(func_col1,1)
        func_col2 = min(func_col2,n_dof)
        counter   = 0
        do i=1, n_dof ; do j=func_col1, func_col2
          ind_gb = (j-1)*n_dof + i
          if(ind_gb >  nfunc_per_mpi_sum(ctx_paral%myrank+1) .and.      &
             ind_gb <= nfunc_per_mpi_sum(ctx_paral%myrank+2)) then
            counter=counter+1
            func_ind(1,counter) = i 
            func_ind(2,counter) = j
          end if
        end do; end do
        !! -------------------------------------------------------------

        !! for the volumic terms, we are going to compute
        !!         \integral_K (            \phi_i             \phi_j) dK
        !!         \integral_K (\partial_xk \phi_i             \phi_j) dK
        !! a) ----------------------------------------------------------
        call lagrange_vol_construct(basis_functions(k),work_vol,0,0,    &
                                    func_ind,nfunc_loc,ctx_err,o_coe=precomp)

        !! all reduce to everyone.
        call distribute_error(ctx_err,ctx_paral%communicator)

        call allreduce_sum(work_vol%coeff_int(1,1,:,:),                 &
                           ctx_dg%vol_matrix_mass(k)%array(:,:),        &
                           n_dof*n_dof,ctx_paral%communicator,ctx_err)
        call lagrange_vol_clean(work_vol)

        !! b) ------------------------------------------------------------
        call lagrange_vol_construct(basis_functions(k),work_vol,0,1,    &
                                    func_ind,nfunc_loc,ctx_err,o_coe=precomp)
        call distribute_error(ctx_err,ctx_paral%communicator)
        
        !! reorder in the buffer array: revert i and j
        allocate(buffer_dphi_phi(n_dof,n_dof,ctx_dg%dim_domain))
        buffer_dphi_phi = 0.d0
        
        do i_dim1=1,ctx_dg%dim_domain
          do counter=1, nfunc_loc
            i = func_ind(1,counter)
            j = func_ind(2,counter)
            buffer_dphi_phi(j,i,i_dim1) = work_vol%coeff_int(1,i_dim1,i,j)
          end do
        end do
        call lagrange_vol_clean(work_vol)
        call allreduce_sum(buffer_dphi_phi,                             &
                           ctx_dg%vol_matrix_dphi_phi(k)%array,         &
                           n_dof*n_dof*ctx_dg%dim_domain,               &
                           ctx_paral%communicator,ctx_err)
                                  
        !! clean current order infos
        deallocate(nfunc_per_mpi)
        deallocate(nfunc_per_mpi_sum)
        deallocate(func_ind)
        deallocate(buffer_dphi_phi)

      !! ===============================================================
      !! if one processor, we just compute them all.
      else
        !! for the volumic terms, we are going to compute
        !!         \integral_K (            \phi_i             \phi_j) dK
        !!         \integral_K (\partial_xk \phi_i             \phi_j) dK
        !!         \integral_K (\partial_xk \phi_i \partial_xl \phi_j) dK
        !! a) ----------------------------------------------------------
        call lagrange_vol_construct(basis_functions(k),work_vol,0,0,ctx_err, &
                                    o_coe=precomp)
        ctx_dg%vol_matrix_mass(k)%array(:,:) = work_vol%coeff_int(1,1,:,:)
        call lagrange_vol_clean(work_vol)
        !! b) ------------------------------------------------------------
        call lagrange_vol_construct(basis_functions(k),work_vol,0,1,ctx_err, &
                                    o_coe=precomp)
        !! reorder
        do i_dim1=1,ctx_dg%dim_domain
          do i=1, n_dof ; do j=1, n_dof
            ctx_dg%vol_matrix_dphi_phi(k)%array(j,i,i_dim1) =           &
                                           work_vol%coeff_int(1,i_dim1,i,j)
            !! ctx_dg%vol_matrix_phi_dphi(k)%array(i,j,i_dim1) =        &
            !!                               work_vol%coeff_int(1,i_dim1,i,j)
          end do ; end do
        end do
        call lagrange_vol_clean(work_vol)
      end if


      !! c)  ------------------------------------------------------------
      !! this is to compute (dphi, dphi), unused for HDG
      !! call lagrange_vol_construct(basis_functions(k),work_vol,1,1,ctx_err, &
      !!                             o_coe=precomp)
      !! call array_allocate(ctx_dg%vol_matrix_dphi_dphi(k),n_dof,n_dof,      &
      !!                     ctx_dg%dim_domain,ctx_dg%dim_domain,ctx_err)
      !! do i_dim1=1,ctx_dg%dim_domain ; do i_dim2=1,ctx_dg%dim_domain
      !!   ctx_dg%vol_matrix_dphi_dphi(k)%array(:,:,i_dim1,i_dim2) =          &
      !!                                  work_vol%coeff_int(i_dim1,i_dim2,:,:)
      !! end do ; end do
      !! call lagrange_vol_clean(work_vol)


      !! ---------------------------------------------------------------
      !! the integral over the face which is 
      !!         \integral_{Face} \phi_i  \phi_j dFace
      !! ---------------------------------------------------------------

      !! ===============================================================
      !! if we have multiple processors, we separate the computation 
      !! between each of them, otherwise, it just computes everything
      !! ===============================================================
      if(ctx_paral%nproc > 1) then
        do l = 1, ctx_dg%n_different_order
          n_dof_l = ctx_dg%n_dof_per_order (l)
          !! decompose into the multiple processors:
          
          !! -----------------------------------------------------------
          !! total number of basis functions
          nfunc_tot = n_dof*n_dof_l
          nfunc_loc = floor(dble(nfunc_tot/ctx_paral%nproc))
          !! how many remains
          nfunc_rem = nfunc_tot - nfunc_loc*ctx_paral%nproc
          !! all processors know how many in the previous ones.
          allocate(nfunc_per_mpi(ctx_paral%nproc))
          nfunc_per_mpi = nfunc_loc
          do i=1,nfunc_rem
            !! update local value and the global array
            if(ctx_paral%myrank == (i-1)) nfunc_loc = nfunc_loc + 1 
            nfunc_per_mpi(i) = nfunc_per_mpi(i) + 1 
          end do
          !! the summed array of size nproc + 1 
          allocate(nfunc_per_mpi_sum(ctx_paral%nproc+1))
          nfunc_per_mpi_sum = 0
          do i=2,ctx_paral%nproc+1
            nfunc_per_mpi_sum(i) = nfunc_per_mpi_sum(i-1) + nfunc_per_mpi(i-1)
          end do
  
          !! create the local array of indices, i.e., the (i,j) for the 
          !! computation of phi_i phi_j.
          allocate(func_ind(2,nfunc_loc))
          func_ind = 0
          !! loop over all array        
                      !! OK BECASE RANK STARTS AT 0
          func_col1 = floor  (dble(nfunc_per_mpi_sum(ctx_paral%myrank+1)/n_dof))
          func_col2 = ceiling(dble(nfunc_per_mpi_sum(ctx_paral%myrank+2)/n_dof))+1
          func_col1 = max(func_col1,1)
          func_col2 = min(func_col2,n_dof_l)
          counter   = 0
          do i=1, n_dof ; do j=func_col1, func_col2
            ind_gb = (j-1)*n_dof + i
            if(ind_gb >  nfunc_per_mpi_sum(ctx_paral%myrank+1) .and.      &
               ind_gb <= nfunc_per_mpi_sum(ctx_paral%myrank+2)) then
              counter=counter+1
              func_ind(1,counter) = i 
              func_ind(2,counter) = j
            end if
          end do; end do

          !! -----------------------------------------------------------
          call lagrange_surf_construct(basis_functions(k),              &
                                       basis_functions(l),              &
                                       work_surf,0,0,func_ind,nfunc_loc,&
                                       ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator)

          !! allreduce
          mem = mem + (n_dof*n_dof_l*ctx_mesh%n_neigh_per_cell)*real_size_pol
          call array_allocate(ctx_dg%face_matrix_mass(k,l),n_dof,       &
                              n_dof_l,ctx_mesh%n_neigh_per_cell,ctx_err)

          call allreduce_sum(work_surf%coeff_int(1,1,:,:,:),            &
                           ctx_dg%face_matrix_mass(k,l)%array(:,:,:),   &
                           n_dof*n_dof_l*ctx_mesh%n_neigh_per_cell,     &
                           ctx_paral%communicator,ctx_err)
          call lagrange_surf_clean(work_surf)
                                   
          !! clean current mpi infos
          deallocate(nfunc_per_mpi)
          deallocate(nfunc_per_mpi_sum)
          deallocate(func_ind)
        end do

      !! ===============================================================
      !! single proc
      else
        do l = 1, ctx_dg%n_different_order
          n_dof_l = ctx_dg%n_dof_per_order (l)
          call lagrange_surf_construct(basis_functions(k),              &
                                       basis_functions(l),              &
                                       work_surf,0,0,ctx_err)
          !! reorder
          mem = mem + (n_dof*n_dof_l*ctx_mesh%n_neigh_per_cell)*real_size_pol
          call array_allocate(ctx_dg%face_matrix_mass(k,l),n_dof,       &
                              n_dof_l,ctx_mesh%n_neigh_per_cell,ctx_err)
          do i=1,ctx_mesh%n_neigh_per_cell
            ctx_dg%face_matrix_mass(k,l)%array(:,:,i) =                 &
                                                work_surf%coeff_int(1,1,:,:,i)
          end do
          call lagrange_surf_clean(work_surf)
        end do
      end if
      !! ---------------------------------------------------------------
      call polynomial_precomput_clean(precomp)
    end do !! end loop over all order
    !! -----------------------------------------------------------------
    !! clean basis functions
    do k = 1, ctx_dg%n_different_order
      call basis_clean(basis_functions(k))
    end do
    if(allocated(basis_functions)) deallocate(basis_functions)

    return

  end subroutine dg_lagrange_init_ref_matrix


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Create the vector of weighted coefficient for a reference 
  !> element using the quadrature points already computed: 
  !> for every order, we will keep 2 vectors: 
  !>  (a)          \phi_i(x_k) \phi_j(x_k) weight_k
  !>  (b)      d_x \phi_i(x_k) \phi_j(x_k) weight_k
  !> where phi are the Lagrange basis functions
  !> 
  !> @param[inout] ctx_dg          : DG context
  !> @param[inout] mem             : update memory count
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine dg_lagrange_init_quadrature_vector(ctx_paral,ctx_dg,mem,ctx_err)
    
    implicit none

    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_discretization) ,intent(inout):: ctx_dg
    integer(kind=8)        ,intent(inout):: mem
    type(t_error)          ,intent(out)  :: ctx_err
    !! local 
    type(t_basis)       ,allocatable :: basis_functions(:)
    type(t_polynomial)  ,allocatable :: pol_multd(:)
    type(t_polynomial)               :: pol_mult
    type(t_polynomial_precomput)     :: precomp(3)
    integer                          :: k, i_order, i_order_l
    integer                          :: i, j, l, m
    integer                          :: n_dof, i_dim1, n_dof_l
    integer(kind=8)                  :: real_size_pol
    integer                          :: iquad, i_face, i_dim
    real(kind=RKIND_POL)             :: quad_pt(3),quad_temp
    !! for the parallel construction 
    integer                          :: nfunc_loc,nfunc_tot,nfunc_rem
    integer                          :: func_col1,func_col2
    integer                          :: counter, ind_gb
    integer             ,allocatable :: nfunc_per_mpi    (:)
    integer             ,allocatable :: nfunc_per_mpi_sum(:)
    integer             ,allocatable :: func_ind(:,:)
    real(kind=RKIND_POL),allocatable :: buffer_dphi_phi(:,:,:,:)
    real(kind=RKIND_POL),allocatable :: buffer_face(:,:,:,:)
    real(kind=RKIND_POL),allocatable :: buffer_face_solo(:,:,:)
    real(kind=RKIND_POL),allocatable :: buffer_phi_phi(:,:,:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr=0

    !! initialize memory information for the dg context 
    real_size_pol = int8(RKIND_POL)

    allocate(basis_functions(ctx_dg%n_different_order))
    do k = 1, ctx_dg%n_different_order
      i_order = ctx_dg%index_to_order(k)
      !! construct basis
      call basis_construct(ctx_dg%dim_domain,i_order,basis_functions(k),ctx_err)
      !! construct first order derivative
      call basis_derivative_construct(basis_functions(k),1,ctx_err)
      !! construct (0th order derivate) face information
      call basis_face_construct(basis_functions(k),0,ctx_err)
    end do

    !! ------------------------------------------------------------------
    !! Create the Reference Vector for the quadrature
    !! ------------------------------------------------------------------
    allocate(ctx_dg%quadGL_phi_phi_w     (ctx_dg%n_different_order))
    allocate(ctx_dg%quadGL_dphi_phi_w    (ctx_dg%n_different_order))  
    allocate(ctx_dg%quadGL_face_phi_w    (ctx_dg%n_different_order))
    allocate(ctx_dg%quadGL_face_phi_phi_w(ctx_dg%n_different_order,     &
                                          ctx_dg%n_different_order))  
    allocate(pol_multd(ctx_dg%dim_domain))

    do k = 1, ctx_dg%n_different_order
      !! initial informations
      i_order = ctx_dg%index_to_order  (k)
      
      !! for the volume, we only need this dimension, but for faces 
      !! we will need all, see below where we re-compute precomp.
      call polynomial_precomput_construct(precomp,ctx_dg%dim_domain,    &
                                          i_order,ctx_err)
      n_dof = basis_functions(k)%nphi
      !! update memory  
      mem = mem +                                                       &
           !!phi phi 
           (n_dof*n_dof*ctx_dg%quadGL_npts)*real_size_pol             + & 
           !! dphi_phi 
           (ctx_dg%dim_domain*n_dof*n_dof*ctx_dg%quadGL_npts)*real_size_pol + &
           !! face phi quadGL_face_phi_w, ctx_dg%dim_domain+1 = Nface per simplex
           ctx_dg%n_different_order*(n_dof*(ctx_dg%dim_domain+1)*       &
                             ctx_dg%quadGL_face_npts)*real_size_pol   + &
           !! face phi phi, ctx_dg%dim_domain+1 = Nface per simplex
           ctx_dg%n_different_order*ctx_dg%n_different_order            &
                                   *(n_dof*n_dof*(ctx_dg%dim_domain+1)* &
                                     ctx_dg%quadGL_face_npts)*real_size_pol

      call array_allocate(ctx_dg%quadGL_phi_phi_w(k),n_dof,n_dof,       &
                          ctx_dg%quadGL_npts,ctx_err)
      call array_allocate(ctx_dg%quadGL_dphi_phi_w(k),n_dof,n_dof,      &
                          ctx_dg%quadGL_npts,ctx_dg%dim_domain,ctx_err)
      ctx_dg%quadGL_dphi_phi_w(k)%array = 0.d0
      ctx_dg%quadGL_phi_phi_w (k)%array = 0.d0

      !! ===============================================================
      !! if we have multiple processors, we separate the computation 
      !! between each of them, otherwise, it just computes everything
      !! ===============================================================
      if(ctx_paral%nproc > 1) then
        
        !! -------------------------------------------------------------
        !! total number of basis functions
        nfunc_tot = n_dof*n_dof
        nfunc_loc = floor(dble(nfunc_tot/ctx_paral%nproc))
        !! how many remains
        nfunc_rem = nfunc_tot - nfunc_loc*ctx_paral%nproc
        !! all processors know how many in the previous ones.
        allocate(nfunc_per_mpi(ctx_paral%nproc))
        nfunc_per_mpi = nfunc_loc
        do i=1,nfunc_rem
          !! update local value and the global array
          if(ctx_paral%myrank == (i-1)) nfunc_loc = nfunc_loc + 1 
          nfunc_per_mpi(i) = nfunc_per_mpi(i) + 1 
        end do
        !! the summed array of size nproc + 1 
        allocate(nfunc_per_mpi_sum(ctx_paral%nproc+1))
        nfunc_per_mpi_sum = 0
        do i=2,ctx_paral%nproc+1
          nfunc_per_mpi_sum(i) = nfunc_per_mpi_sum(i-1) + nfunc_per_mpi(i-1)
        end do

        !! create the local array of indices, i.e., the (i,j) for the 
        !! computation of phi_i phi_j.
        allocate(func_ind(2,nfunc_loc))
        func_ind = 0
        !! loop over all array        
                    !! OK BECASE RANK STARTS AT 0
        func_col1 = floor  (dble(nfunc_per_mpi_sum(ctx_paral%myrank+1)/n_dof))
        func_col2 = ceiling(dble(nfunc_per_mpi_sum(ctx_paral%myrank+2)/n_dof))+1
        func_col1 = max(func_col1,1)
        func_col2 = min(func_col2,n_dof)
        counter   = 0
        do i=1, n_dof ; do j=func_col1, func_col2
          ind_gb = (j-1)*n_dof + i
          if(ind_gb >  nfunc_per_mpi_sum(ctx_paral%myrank+1) .and.      &
             ind_gb <= nfunc_per_mpi_sum(ctx_paral%myrank+2)) then
            counter=counter+1
            func_ind(1,counter) = i 
            func_ind(2,counter) = j
          end if
        end do; end do
        !! -------------------------------------------------------------
      else !! mono-proc
        nfunc_tot = n_dof*n_dof
        nfunc_loc = nfunc_tot
        allocate(func_ind(2,nfunc_tot))
        counter=0
        do i=1, n_dof ; do j=1, n_dof
          counter=counter+1
          func_ind(1,counter) = i
          func_ind(2,counter) = j
        end do ; end do
      end if

      !! only loop over the concerned ones ...........................
      do l=1,nfunc_loc
        i = func_ind(1,l)
        j = func_ind(2,l)
        !! I. we work with \phi_i \phi_j multiplication ----------------
        call polynomial_multiplication(precomp,                         &
                                       basis_functions(k)%pol(i),       &
                                       basis_functions(k)%pol(j),       &
                                       pol_mult,ctx_err)
        !! II. we work with \phi_i' \phi_j multiplication --------------
        !! the derivative is in
        !! basis_functions(k)%pol_derivative(ORDER)%p(nphi,COO)
        !! so we loop over the directions
        do i_dim1=1,ctx_dg%dim_domain 
          call polynomial_multiplication(precomp,                          &
                          basis_functions(k)%pol_derivative(1)%p(i,i_dim1),&
                          basis_functions(k)%pol(j),pol_multd(i_dim1),ctx_err)
        end do

        !! if we use the Gauss-Lobatto quadrature rule to evaluate the
        !! integrals, we save dirrectly the values here:       
        ! ---------------------------------------------------------
        !! INFOS -------------------------------------------------------
        ! ---------------------------------------------------------
        !! for debugging, we can check that
        !!   (in3D)  \int x^i y^j z^k =
        !!      factorial(i)*factorial(j)*factorial(k) / factorial(i+j+k+3)
        !!   (in2D)  \int x^i y^j     =
        !!      factorial(i)*factorial(j) / factorial(i+j+2)
        !!
        ! ---------------------------------------------------------
        !! >>> VOLUME INTEGRALS
        ! ---------------------------------------------------------
        select case (ctx_dg%dim_domain) 
          case(1)
            !$OMP PARALLEL DEFAULT(shared)                              &
            !$OMP&         PRIVATE(iquad,quad_pt,quad_temp,i_dim1)
            !$OMP DO
            do iquad=1,ctx_dg%quadGL_npts
              !! we compute the evaluation of the function times the weight
              quad_pt(1) = ctx_dg%quadGL_pts(1,iquad)
              call polynomial_eval(pol_mult,quad_pt(1),quad_temp,ctx_err)
              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad) = quad_temp * &
                                              ctx_dg%quadGL_weight(iquad)
              do i_dim1=1,ctx_dg%dim_domain
                call polynomial_eval(pol_multd(i_dim1),quad_pt(1),      &
                                     quad_temp,ctx_err)
                ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,i_dim1) =   &
                                  quad_temp * ctx_dg%quadGL_weight(iquad)
              end do
            
            end do
            !$OMP END DO
            !$OMP END PARALLEL

          case(2)
            !$OMP PARALLEL DEFAULT(shared)                              &
            !$OMP&         PRIVATE(iquad,quad_pt,quad_temp,i_dim1)
            !$OMP DO
            do iquad=1,ctx_dg%quadGL_npts
              !! we compute the evaluation of the function times the weight
              quad_pt(1:2) = ctx_dg%quadGL_pts(1:2,iquad)
              call polynomial_eval(pol_mult,quad_pt(1),quad_pt(2),      &
                                   quad_temp,ctx_err)
              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad) = quad_temp * &
                                              ctx_dg%quadGL_weight(iquad)
              do i_dim1=1,ctx_dg%dim_domain
                call polynomial_eval(pol_multd(i_dim1),quad_pt(1),      &
                                     quad_pt(2),quad_temp,ctx_err)
                ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,i_dim1) =   &
                                  quad_temp * ctx_dg%quadGL_weight(iquad)
              end do
            
            end do
            !$OMP END DO
            !$OMP END PARALLEL

          case(3)
            !$OMP PARALLEL DEFAULT(shared)                              &
            !$OMP&         PRIVATE(iquad,quad_pt,quad_temp,i_dim1)
            !$OMP DO
            do iquad=1,ctx_dg%quadGL_npts
              !! we compute the evaluation of the function times the weight
              quad_pt(:) = ctx_dg%quadGL_pts(:,iquad)
              call polynomial_eval(pol_mult,quad_pt(1),quad_pt(2),      &
                                   quad_pt(3),quad_temp,ctx_err)
              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad) = quad_temp * &
                                              ctx_dg%quadGL_weight(iquad)
              do i_dim1=1,ctx_dg%dim_domain
                call polynomial_eval(pol_multd(i_dim1),quad_pt(1),      &
                                     quad_pt(2),quad_pt(3),quad_temp,ctx_err)
                ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,i_dim1) =   &
                                  quad_temp * ctx_dg%quadGL_weight(iquad)
              end do
            
            end do
            !$OMP END DO
            !$OMP END PARALLEL
          case default
            ctx_err%msg   = "** ERROR: Unrecognized Dimension [dg_init_quad] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
        
        !! clean polynomials
        call polynomial_clean(pol_mult)
        do i_dim1=1,ctx_dg%dim_domain
          call polynomial_clean(pol_multd(i_dim1))
        end do

      ! ----------------------------------------------------------------
      enddo !! end loop over i/j
      ! ----------------------------------------------------------------
      
      !! allreduce
      if(ctx_paral%nproc > 1) then
        allocate(buffer_phi_phi(n_dof,n_dof,ctx_dg%quadGL_npts))
        buffer_phi_phi = ctx_dg%quadGL_phi_phi_w(k)%array
        ctx_dg%quadGL_phi_phi_w(k)%array = 0.d0
        call allreduce_sum(buffer_phi_phi,ctx_dg%quadGL_phi_phi_w(k)%array, &
                           n_dof*n_dof*ctx_dg%quadGL_npts,                  &
                           ctx_paral%communicator,ctx_err)                          
        deallocate(buffer_phi_phi)
        
        allocate(buffer_dphi_phi(n_dof,n_dof,ctx_dg%quadGL_npts,ctx_dg%dim_domain))
        buffer_dphi_phi = ctx_dg%quadGL_dphi_phi_w(k)%array
        ctx_dg%quadGL_dphi_phi_w(k)%array = 0.d0
        call allreduce_sum(buffer_dphi_phi,ctx_dg%quadGL_dphi_phi_w(k)%array,&
                           n_dof*n_dof*ctx_dg%quadGL_npts*ctx_dg%dim_domain, &
                           ctx_paral%communicator,ctx_err)        
        deallocate(buffer_dphi_phi  )
        deallocate(nfunc_per_mpi    )
        deallocate(nfunc_per_mpi_sum)
      end if
      !! clean
      deallocate(func_ind)

      ! ----------------------------------------------------------------
      !! >>> SURFACE INTEGRALS
      ! ----------------------------------------------------------------
      !! precomp must be re-computed: for the lower dimension, and 
      !! for the maximal orders too. 
      call polynomial_precomput_clean(precomp)
      
      !! for the face, we only need one order: no combination, dim+1 is
      !! the number of neighbors for simplexes, i.e., the number of faces.
      call array_allocate(ctx_dg%quadGL_face_phi_w(k),n_dof,        &
                          ctx_dg%quadGL_face_npts,ctx_dg%dim_domain+1,ctx_err)
      ctx_dg%quadGL_face_phi_w(k)%array = 0.d0
      
     
      do l = 1, ctx_dg%n_different_order

        !! compute precomp for the max order and all dimensions
        i_order_l = ctx_dg%index_to_order(l)
        do i_dim=1,ctx_dg%dim_domain
          call polynomial_precomput_construct(precomp,i_dim,            &
                                              max(i_order,i_order_l),ctx_err)
        end do
        !! -------------------------------------------------------------
        
        !! Nface per simplex is dim_domain + 1 here ....................
        n_dof_l = basis_functions(l)%nphi
        call array_allocate(ctx_dg%quadGL_face_phi_phi_w(k,l),n_dof,    &
                            n_dof_l,ctx_dg%quadGL_face_npts,            &
                            ctx_dg%dim_domain+1,ctx_err)
        ctx_dg%quadGL_face_phi_phi_w(k,l)%array = 0.d0

        if(ctx_paral%nproc > 1) then
          !! -----------------------------------------------------------
          !! total number of basis functions
          nfunc_tot = n_dof*n_dof_l
          nfunc_loc = floor(dble(nfunc_tot/ctx_paral%nproc))
          !! how many remains
          nfunc_rem = nfunc_tot - nfunc_loc*ctx_paral%nproc
          !! all processors know how many in the previous ones.
          allocate(nfunc_per_mpi(ctx_paral%nproc))
          nfunc_per_mpi = nfunc_loc
          do i=1,nfunc_rem
            !! update local value and the global array
            if(ctx_paral%myrank == (i-1)) nfunc_loc = nfunc_loc + 1 
            nfunc_per_mpi(i) = nfunc_per_mpi(i) + 1 
          end do
          !! the summed array of size nproc + 1 
          allocate(nfunc_per_mpi_sum(ctx_paral%nproc+1))
          nfunc_per_mpi_sum = 0
          do i=2,ctx_paral%nproc+1
            nfunc_per_mpi_sum(i) = nfunc_per_mpi_sum(i-1) + nfunc_per_mpi(i-1)
          end do
  
          !! create the local array of indices, i.e., the (i,j) for the 
          !! computation of phi_i phi_j.
          allocate(func_ind(2,nfunc_loc))
          func_ind = 0
          !! loop over all array        
                      !! OK BECASE RANK STARTS AT 0
          func_col1 = floor  (dble(nfunc_per_mpi_sum(ctx_paral%myrank+1)/n_dof))
          func_col2 = ceiling(dble(nfunc_per_mpi_sum(ctx_paral%myrank+2)/n_dof))+1
          func_col1 = max(func_col1,1)
          func_col2 = min(func_col2,n_dof_l)
          counter   = 0
          do i=1, n_dof ; do j=func_col1, func_col2
            ind_gb = (j-1)*n_dof + i
            if(ind_gb >  nfunc_per_mpi_sum(ctx_paral%myrank+1) .and.      &
               ind_gb <= nfunc_per_mpi_sum(ctx_paral%myrank+2)) then
              counter=counter+1
              func_ind(1,counter) = i 
              func_ind(2,counter) = j
            end if
          end do; end do
        else
          !! monoproc
          nfunc_tot = n_dof*n_dof_l
          nfunc_loc = nfunc_tot
          allocate(func_ind(2,nfunc_tot))
          func_ind=0
          counter= 0
          do i=1, n_dof ; do j=1,n_dof_l
            counter=counter+1
            func_ind(1,counter)=i
            func_ind(2,counter)=j
          end do; end do        
        end if

        !! -------------------------------------------------------------
        !! loop over each faces
        do i_face = 1, ctx_dg%dim_domain + 1
          
          do m=1,nfunc_loc
            i=func_ind(1,m)
            j=func_ind(2,m)
          
            !! multiply face polynomials -------------------------------
            !! THE FACE POLYNOMIALS ARE ALREADY OF DIMENSION (DIM-1) SO 
            !! EVERYTHING IS FINE FOR THE COMPUTATIONS, IT USES THE 
            !! POLYNOMIAL TRACE SUBROUTINE.
            call polynomial_multiplication(precomp,                     &
                                 basis_functions(k)%pol_face(i,i_face), &
                                 basis_functions(l)%pol_face(j,i_face), &
                                 pol_mult,ctx_err)
            
            !! we loose one dimension 
            select case (ctx_dg%dim_domain-1) 
              case(0)
                do iquad=1,ctx_dg%quadGL_face_npts !! should be = 1
                  !! should be = 0.d0
                  quad_pt(1) = ctx_dg%quadGL_face_pts(1,iquad)
                  !! call polynomial_eval(pol_mult,quad_pt(1),quad_temp,ctx_err)
                  ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,i_face)  &
                    = basis_functions(k)%pol_face(i,i_face)%coeff_pol(1)     &
                    * ctx_dg%quadGL_face_weight(iquad)
                end do
                !! the solo face, only needed once.
                if(j==1) then
                  do iquad=1,ctx_dg%quadGL_face_npts !! should be = 1
                    !! should be = 0.d0
                    quad_pt(1) = ctx_dg%quadGL_face_pts(1,iquad)
                    !! call polynomial_eval(pol_mult,quad_pt(1),quad_temp,ctx_err)
                    ctx_dg%quadGL_face_phi_w(k)%array(i,iquad,i_face)   &
                                 = basis_functions(k)%pol_face(i,i_face)%coeff_pol(1) &
                                  *ctx_dg%quadGL_face_weight(iquad)     
                  end do           
                end if
                
              case(1) !! 2D face = 1D
                !$OMP PARALLEL DEFAULT(shared)                              &
                !$OMP&         PRIVATE(iquad,quad_pt,quad_temp)
                !$OMP DO
                do iquad=1,ctx_dg%quadGL_face_npts
                  !! we compute the evaluation of the function times the weight
                  quad_pt(1) = ctx_dg%quadGL_face_pts(1,iquad)
                  call polynomial_eval(pol_mult,quad_pt(1),quad_temp,ctx_err)
                  ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,i_face)  &
                               = quad_temp * ctx_dg%quadGL_face_weight(iquad)
                end do
                !$OMP END DO
                if(j==1) then ! only if l=1 for the solo faces
                  !$OMP DO
                  do iquad=1,ctx_dg%quadGL_face_npts
                    !! we compute the evaluation of the function times the weight
                    quad_pt(1) = ctx_dg%quadGL_face_pts(1,iquad)
                    call polynomial_eval(basis_functions(k)%pol_face(i,i_face),&
                                         quad_pt(1),quad_temp,ctx_err)
                    ctx_dg%quadGL_face_phi_w(k)%array(i,iquad,i_face)   &
                                 = quad_temp * ctx_dg%quadGL_face_weight(iquad)
                  end do
                  !$OMP END DO
                end if
                
                !$OMP END PARALLEL
              case(2) !! 3D faces = 2D
                !$OMP PARALLEL DEFAULT(shared)                              &
                !$OMP&         PRIVATE(iquad,quad_pt,quad_temp)
                !$OMP DO
                do iquad=1,ctx_dg%quadGL_face_npts
                  !! we compute the evaluation of the function times the weight
                  quad_pt(1:2) = ctx_dg%quadGL_face_pts(1:2,iquad)
                  call polynomial_eval(pol_mult,quad_pt(1),quad_pt(2),  &
                                       quad_temp,ctx_err)
                  ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,i_face) &
                               = quad_temp * ctx_dg%quadGL_face_weight(iquad)
                end do
                !$OMP END DO

                if(j==1) then ! only if l=1 for the solo faces
                  !$OMP DO
                  do iquad=1,ctx_dg%quadGL_face_npts
                    !! we compute the evaluation of the function times the weight
                    quad_pt(1:2) = ctx_dg%quadGL_face_pts(1:2,iquad)
                    call polynomial_eval(basis_functions(k)%pol_face(i,i_face),&
                                         quad_pt(1),quad_pt(2),quad_temp,     &
                                         ctx_err)
                    ctx_dg%quadGL_face_phi_w(k)%array(i,iquad,i_face)   &
                                 = quad_temp * ctx_dg%quadGL_face_weight(iquad)
                  end do
                  !$OMP END DO
                end if

                !$OMP END PARALLEL
              case default
                ctx_err%msg   = "** ERROR: Unrecognized Dimension [dg_init_quad] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
            
            call polynomial_clean(pol_mult)
          !! -----------------------------------------------------------
          enddo  !! end loop over i/j
          !! -----------------------------------------------------------
        end do !! end loop over the faces
        
        !! allreduce
        if(ctx_paral%nproc > 1) then
          !! face phi phi
          allocate(buffer_face(n_dof,n_dof_l,ctx_dg%quadGL_face_npts,   &
                               ctx_dg%dim_domain+1))
          buffer_face = ctx_dg%quadGL_face_phi_phi_w(k,l)%array(:,:,:,:)
          ctx_dg%quadGL_face_phi_phi_w(k,l)%array = 0.d0
          call allreduce_sum(buffer_face,ctx_dg%quadGL_face_phi_phi_w(k,l)%array,&
                             n_dof*n_dof_l*ctx_dg%quadGL_face_npts               &
                             *(ctx_dg%dim_domain+1),   &
                             ctx_paral%communicator,ctx_err)        
          deallocate(buffer_face)

          !! face phi 
          if(l==1) then
            allocate(buffer_face_solo(n_dof,ctx_dg%quadGL_face_npts,    &
                                 ctx_dg%dim_domain+1))
            buffer_face_solo = ctx_dg%quadGL_face_phi_w(k)%array(:,:,:)
            ctx_dg%quadGL_face_phi_w(k)%array = 0.d0
            call allreduce_sum(buffer_face_solo,                        &
                               ctx_dg%quadGL_face_phi_w(k)%array,       &
                               n_dof*ctx_dg%quadGL_face_npts            &
                               *(ctx_dg%dim_domain+1),                  &
                               ctx_paral%communicator,ctx_err)        
            deallocate(buffer_face_solo)
          end if
          deallocate(nfunc_per_mpi    )
          deallocate(nfunc_per_mpi_sum)
        end if
        !! -------------------------------------------------------------        

        !! clean for next order
        deallocate(func_ind)
        call polynomial_precomput_clean(precomp)       
      end do !! end loop over all order (face only)

      call polynomial_precomput_clean(precomp)
    end do !! end loop over all order (both face and surface)
    !! -----------------------------------------------------------------
    !! clean basis functions
    do k = 1, ctx_dg%n_different_order
      call basis_clean(basis_functions(k))
    end do
    if(allocated(basis_functions)) deallocate(basis_functions)

    return

  end subroutine dg_lagrange_init_quadrature_vector


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Create the vector of weighted coefficient for a reference 
  !> element using the quadrature points already computed in the 
  !> case of a triple integral. It corresponds to a fixed model
  !> order l, but then we have every order for i and j:
  !>    \phi_i(x_k) \phi_j(x_k) \phi_l(x_k) \phi_j(x_k) weight_k
  !> where phi are the Lagrange basis functions
  !> 
  !> @param[inout] ctx_dg          : DG context
  !> @param[inout] mem             : update memory count
  !> @param[inout] order_in        : order of the latest functions
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine dg_lagrange_init_quadrature_vector_triple(ctx_dg,mem,      &
                                                       order_in,ctx_err)
    
    implicit none

    type(t_discretization) ,intent(inout):: ctx_dg
    integer(kind=8)        ,intent(inout):: mem
    integer(kind=4)        ,intent(in)   :: order_in
    type(t_error)          ,intent(out)  :: ctx_err

    !! local
    type(t_basis)       ,allocatable :: basis_functions(:)
    type(t_basis)                    :: basis_function_mod
    type(t_polynomial)               :: pol_mult
    type(t_polynomial)               :: pol_mult_mod
    type(t_polynomial_precomput)     :: precomp(3)
    type(t_polynomial_precomput)     :: precomp_mult(3)
    integer                          :: k, i_order
    integer                          :: i, j, l
    integer                          :: n_dof, n_dof_model
    integer(kind=8)                  :: real_size_pol
    integer                          :: iquad
    real(kind=RKIND_POL)             :: quad_pt(3),quad_temp
    !! -----------------------------------------------------------------
    
    ctx_err%ierr=0

    !! initialize memory information for the dg context 
    real_size_pol = int8(RKIND_POL)

    !! -----------------------------------------------------------------
    !! basis functions for \phi_i and \phi_j
    allocate(basis_functions(ctx_dg%n_different_order))
    do k = 1, ctx_dg%n_different_order
      i_order = ctx_dg%index_to_order(k)
      !! construct basis
      call basis_construct(ctx_dg%dim_domain,i_order,basis_functions(k),ctx_err)
    end do
    !! basis functions for \phi_l: only one order
    call basis_construct(ctx_dg%dim_domain,order_in,basis_function_mod,ctx_err)
    n_dof_model = basis_function_mod%nphi
    !! -----------------------------------------------------------------


    !! ------------------------------------------------------------------
    !! Create the Reference Vector for the quadrature
    !! ------------------------------------------------------------------
    allocate(ctx_dg%quadGL_phi_phi_phi_w (ctx_dg%n_different_order))  
    
    do k = 1, ctx_dg%n_different_order
      !! initial informations
      i_order = ctx_dg%index_to_order(k)
      
      !! for the volume, we only need this dimension, but for faces 
      !! we will need all, see below where we re-compute precomp.
      call polynomial_precomput_construct(precomp,ctx_dg%dim_domain,    &
                                          i_order,ctx_err)
      n_dof = basis_functions(k)%nphi
      !! update memory  
      mem = mem +                                                       &
           !!phi phi phi 
           (n_dof*n_dof*n_dof_model*ctx_dg%quadGL_npts)*real_size_pol

      call array_allocate(ctx_dg%quadGL_phi_phi_phi_w(k),n_dof,n_dof,   &
                          n_dof_model,ctx_dg%quadGL_npts,ctx_err)
      
      !! compute the current multiplication of basis >>>>>>>>>>>>>>>>>>>
      do i=1, n_dof ; do j=1, n_dof

        !! I. we work with \phi_i \phi_j multiplication ----------------
        call polynomial_multiplication(precomp,                         &
                                       basis_functions(k)%pol(i),       &
                                       basis_functions(k)%pol(j),       &
                                       pol_mult,ctx_err)
        !! initialize the precomp with the order of the multiplication
        call polynomial_precomput_construct(precomp_mult,               &
                                            ctx_dg%dim_domain,          &
                                        max(pol_mult%degree,order_in),  &
                                            ctx_err)

        do l=1, n_dof_model
          call polynomial_multiplication(precomp_mult,                  &
                                         pol_mult,                      &
                                         basis_function_mod%pol(l),     &
                                         pol_mult_mod,ctx_err)

          !! if we use the Gauss-Lobatto quadrature rule to evaluate the
          !! integrals, we save dirrectly the values here:       
          ! ---------------------------------------------------------
          !! INFOS -------------------------------------------------------
          ! ---------------------------------------------------------
          !! for debugging, we can check that
          !!   (in3D)  \int x^i y^j z^k =
          !!      factorial(i)*factorial(j)*factorial(k) / factorial(i+j+k+3)
          !!   (in2D)  \int x^i y^j     =
          !!      factorial(i)*factorial(j) / factorial(i+j+2)
          !!
          ! ---------------------------------------------------------
          !! >>> VOLUME INTEGRALS
          ! ---------------------------------------------------------
          select case (ctx_dg%dim_domain) 
            case(1)
              !$OMP PARALLEL DEFAULT(shared) PRIVATE(iquad,quad_pt,quad_temp)
              !$OMP DO
              do iquad=1,ctx_dg%quadGL_npts
                !! we compute the evaluation of the function times the weight
                quad_pt(1) = ctx_dg%quadGL_pts(1,iquad)
                call polynomial_eval(pol_mult_mod,quad_pt(1),quad_temp,ctx_err)
                ctx_dg%quadGL_phi_phi_phi_w(k)%array(i,j,l,iquad) =     &
                                  quad_temp * ctx_dg%quadGL_weight(iquad)
              end do 
              !$OMP END DO
              !$OMP END PARALLEL
            case(2)
              !$OMP PARALLEL DEFAULT(shared) PRIVATE(iquad,quad_pt,quad_temp)
              !$OMP DO
              do iquad=1,ctx_dg%quadGL_npts
                !! we compute the evaluation of the function times the weight
                quad_pt(1:2) = ctx_dg%quadGL_pts(1:2,iquad)
                call polynomial_eval(pol_mult_mod,quad_pt(1),quad_pt(2),&
                                     quad_temp,ctx_err)
                ctx_dg%quadGL_phi_phi_phi_w(k)%array(i,j,l,iquad) =     &
                                  quad_temp*ctx_dg%quadGL_weight(iquad)
              end do
              !$OMP END DO
              !$OMP END PARALLEL
            case(3)
              !$OMP PARALLEL DEFAULT(shared) PRIVATE(iquad,quad_pt,quad_temp)
              !$OMP DO
              do iquad=1,ctx_dg%quadGL_npts
                !! we compute the evaluation of the function times the weight
                quad_pt(:) = ctx_dg%quadGL_pts(:,iquad)
                call polynomial_eval(pol_mult_mod,quad_pt(1),quad_pt(2),&
                                     quad_pt(3),quad_temp,ctx_err)
                ctx_dg%quadGL_phi_phi_phi_w(k)%array(i,j,l,iquad) =     &
                                     quad_temp * ctx_dg%quadGL_weight(iquad)
              end do
              !$OMP END DO
              !$OMP END PARALLEL
 
            case default
              ctx_err%msg   = "** ERROR: Unrecognized Dimension "  //   &
                              "[dg_init_quad_triple] **"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              call raise_error(ctx_err)
          end select

          !! clean polynomial multiplication
          call polynomial_clean(pol_mult_mod)
        
        !! end loop over polynomials in the third phi
        end do
        !! -------------------------------------------------------------

        !! clean polynomial and precomp.
        call polynomial_clean(pol_mult)
        call polynomial_precomput_clean(precomp_mult)

      ! ----------------------------------------------------------------
      enddo ; enddo !! end loop over i/j
      ! ----------------------------------------------------------------
      
      call polynomial_precomput_clean(precomp)

    end do !! end loop over all orders
    !! -----------------------------------------------------------------

    !! clean basis functions
    do k = 1, ctx_dg%n_different_order
      call basis_clean(basis_functions(k))
    end do
    if(allocated(basis_functions)) deallocate(basis_functions)
    call basis_clean(basis_function_mod)
    
    return

  end subroutine dg_lagrange_init_quadrature_vector_triple
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Construct the index correspondence between a face dof
  !> and the cell dof. It is necessary for HDG
  !> face_map(dof_face,face) = dof_vol
  !
  !> @param[in]    dm              : dimension
  !> @param[inout] ctx_dg          : polynomial order
  !> @param[in]    face_map        : outuput map
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine face_map_create(dm,order,face_map,ctx_err)

    implicit none

    integer             ,intent(in)      :: dm
    integer             ,intent(in)      :: order
    integer             ,intent(inout)   :: face_map(:,:)
    type(t_error)       ,intent(out)     :: ctx_err
    !! local
    integer :: i,tmp_index,nb_level,tmp_n_dof

    ctx_err%ierr = 0

    select case(dm)
      
      case(1)
        !! Node 1 (left) and 2 (right), it is forced to be order 1
        !! -------------------------------------------------------
        face_map(1,1) = 1       !! face_map(ndof_face,n_face)
        face_map(1,2) = order+1 !! face_map(ndof_face,n_face)

      case(2)
        !! Edge 1
        !! ------
        face_map(1,1) = 2
        do i = 2, order
           face_map(i,1) = order+1+i
        end do
        face_map(order+1,1) = 3

        !! Edge 2
        !! ------
        face_map(1,2) = 3
        do i = 2, order
           face_map(i,2) = 2*order+i
        end do
        face_map(order+1,2) = 1

        !! Edge 3
        !! ------
        face_map(1,3) = 1
        do i = 2, order
           face_map(i,3) = 2+i
        end do
        face_map(order+1,3) = 2

      case(3)

        nb_level = (order-3)/3+1
        !We compute the number of DoF of the last level for one face
        tmp_n_dof = 0
        do i = 1, order+1-3*nb_level
          tmp_n_dof = tmp_n_dof+1
        end do
        do i = 1, order-3*nb_level
          tmp_n_dof = tmp_n_dof+1
        end do
        do i = 1, order-1-3*nb_level
          tmp_n_dof = tmp_n_dof+1
        end do

        !!DoF on vertices, face 4
        face_map(1,4) = 1
        face_map(2,4) = 2
        face_map(3,4) = 3

        !!DoF on vertices, face 3
        face_map(1,3) = 1
        face_map(2,3) = 4
        face_map(3,3) = 2

        !!DoF on vertices, face 2
        face_map(1,2) = 1
        face_map(2,2) = 3
        face_map(3,2) = 4

        !!DoF on vertices, face 1
        face_map(1,1) = 2
        face_map(2,1) = 4
        face_map(3,1) = 3

        if (order.ge.2) then

          !! ------------
          !! Face 4 (123)
          !! ------------
          !! DoF on edges
          ! 1->2 facephi(4:order+2,4)
          ! 2->3 facephi(order+3:2*order+1,4)
          ! 3->1 facephi(2*order+2:3*order,4)
          do i = 4, 3*order
            face_map(i,4) = 1+i
          end do

          !! DoF inside the face
          !Number of DoF counted before,
          !minus the DoF of the last level for the faces 1, 2 and 3
          tmp_index = 4+6*(order-1)+9*((nb_level-1)*(order)- &
                      3*(nb_level-1)*(nb_level)/2)
          !We compute the total number of DoF counted before
          tmp_index = tmp_index+3*tmp_n_dof
          do  i = 3*order+1, (order+2)*(order+1)/2
            tmp_index = tmp_index+1
            face_map(i,4) = tmp_index
          end do

          !! ------------
          !! Face 3 (142)
          !! ------------
          !! DoF on edges
          !Number of DoF counted before edge 14
          tmp_index = 4+3*(order-1)
          !1->4
          do i = 4, order+2
            tmp_index = tmp_index+1
            face_map(i,3) = tmp_index
          end do
          !4->2
          do i = 2*order+1, order+3,-1
            tmp_index = tmp_index+1
            face_map(i,3) = tmp_index
          end do
          !2->1
          face_map(2*order+2:3*order,3) = face_map(order+2:4:-1,4)

          !! DoF inside the face
          !Number of DoF counted before,
          !minus the DoF of the last level for the faces 1 and 2
          tmp_index = 4+6*(order-1)+6*((nb_level-1)*(order)- &
                      3*(nb_level-1)*(nb_level)/2)
          !We compute the total number of DoF counted before
          tmp_index = tmp_index+2*tmp_n_dof
          do i = 3*order+1, (order+2)*(order+1)/2
            tmp_index = tmp_index+1
            face_map(i,3) = tmp_index
          end do

          !! ------------
          !! Face 2 (134)
          !! ------------
          !! DoF on edges
          !1->3
          face_map(4:order+2,2) = face_map(3*order:2*order+2:-1,4)
          !Number of DoF counted before edge 34
          tmp_index = 4+5*(order-1)
          !3->4
          do i = order+3, 2*order+1
            tmp_index = tmp_index+1
            face_map(i,2) = tmp_index
          end do
          !4->1
          face_map(2*order+2:3*order,2) = face_map(order+2:4:-1,3)

          !! DoF inside the face
          !Number of DoF counted before,
          !minus the DoF of the last level for the face 1
          tmp_index = 4+6*(order-1)+3*((nb_level-1)*(order)- &
                      3*(nb_level-1)*(nb_level)/2)
          !We compute the total number of DoF counted before
          tmp_index = tmp_index+tmp_n_dof
          do i = 3*order+1, (order+2)*(order+1)/2
            tmp_index = tmp_index+1
            face_map(i,2) = tmp_index
          end do

          !! ------------
          !! Face 1 (243)
          !! ------------
          !! DoF on edges
          !2->4
          face_map(4:order+2,1) = face_map(2*order+1:order+3:-1,3)
          !4->3
          face_map(order+3:2*order+1,1) = &
             face_map(2*order+1:order+3:-1,2)
          !3->2
          face_map(2*order+2:3*order,1) = &
             face_map(2*order+1:order+3:-1,4)

          !! DoF inside the face
          !Number of DoF counted before on edges
          tmp_index = 4+6*(order-1)
          do i = 3*order+1, (order+2)*(order+1)/2
            tmp_index = tmp_index+1
            face_map(i,1) = tmp_index
          end do

        end if !!-- (order.ge.2)

      case default
        ctx_err%msg   = "** ERROR: Incorrect dimension [face_map_create] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)

    end select

  end subroutine face_map_create
  
end module m_dg_lagrange_simplex_init_ctx
