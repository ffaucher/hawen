!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_rhs_sparse.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the right hand side for
!> DG discretization using Lagrangian basis function
!
!------------------------------------------------------------------------------
module m_dg_lagrange_rhs_sparse

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: IKIND_MAT,RKIND_MAT,IKIND_MESH,RKIND_POl
  use m_ctx_parallelism,        only: t_parallelism
  use m_reduce_sum,             only: reduce_sum
  use m_allreduce_sum,          only: allreduce_sum
  use m_broadcast,              only: broadcast
  use m_barrier,                only: barrier
  use m_receive,                only : receive
  use m_send,                   only : send
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_acquisition,        only: t_acquisition,SRC_DIRAC,SRC_PW,   &
                                      SRC_DIRAC_DERIV,SRC_DIRAC_DIV,    &
                                      SRC_DIRAC_DERIVX,SRC_DIRAC_DERIVY,&
                                      SRC_DIRAC_DERIVZ,SRC_ANALYTIC,    &
                                      SRC_GAUSSIAN
  use m_ctx_domain,             only: t_domain
  use m_tag_namefield
  use m_array_types,            only: array_allocate, t_array1d_complex,&
                                      t_array1d_int
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: dg_rhs_create_lagrange_sparse
  public :: dg_rhs_backward_create_lagrange_sparse

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> The RHS is updated to symmetrize the matrix. With HDG, having a 
  !> Dirichlet boundary conditions lead to identity sub-block that 
  !> break the original symmetry. We enforce the symmetry by updating
  !> the RHS, i.e., some coefficient of the matrix have been set to 
  !> zero, while some part of rhs are now non-zero.
  !>
  !
  !> @param[in]    n_cell_loc       : number of cells to treat
  !> @param[inout] n_dof_rhs        : number of nnz entries for rhs (updated)
  !> @param[inout] nnz_per_cell     : number of nnz for each cell
  !> @param[inout] nnz_line         : line of the nnz
  !> @param[inout] nnz_col          : col  of the nnz
  !> @param[inout] nnz_val          : entries of the matrix in (line,col)
  !> @param[inout] col_loc          : index  of rhs component
  !> @param[inout] rhs_loc          : value  of rhs component
  !----------------------------------------------------------------------------
  subroutine modify_rhs_for_symmetry(n_cell_loc,n_dof_rhs,nnz_per_cell, &
                                     nnz_line,nnz_col,nnz_val,col_loc,  &
                                     rhs_loc)

    implicit none
    
    integer(kind=IKIND_MESH)            , intent(in)    :: n_cell_loc
    integer                             , intent(inout) :: n_dof_rhs
    integer                , allocatable, intent(in)    :: nnz_per_cell(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_line(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_col(:)
    type(t_array1d_complex), allocatable, intent(in)    :: nnz_val(:)
    integer(kind=IKIND_MAT), allocatable, intent(inout) :: col_loc(:)
    complex(kind=8)        , allocatable, intent(inout) :: rhs_loc(:)

    !! local
    integer(kind=IKIND_MESH) :: c
    integer                  :: n_dof_rhs_in,i_sym_rhs,i_temp
    complex(kind=8)          :: ref

    n_dof_rhs_in = n_dof_rhs
    do c = 1, n_cell_loc
      if(nnz_per_cell(c) > 0) then

        !! we update the rhs with additional values, Mumps will 
        !! update the global rhs automatically.
        do i_sym_rhs = 1, nnz_per_cell(c)
          if(nnz_col(c)%array(i_sym_rhs) > 0) then
           
            !! get the ref value from the already existing rhs. 
            ref = dcmplx(0.d0, 0.d0)
            do i_temp = 1, n_dof_rhs_in
              if(nnz_col(c)%array(i_sym_rhs) .eq. col_loc(i_temp)) then
                ref = ref + rhs_loc(i_temp)
              end if
            end do
            
            !! add new values if ref is non-zero -----------------------
            if(abs(ref) > tiny(1.0)) then
              n_dof_rhs = n_dof_rhs + 1
              col_loc(n_dof_rhs) = nnz_line(c)%array(i_sym_rhs)
              
              !! minus here.
              rhs_loc(n_dof_rhs) =-dcmplx(ref*nnz_val(c)%array(i_sym_rhs))
          
            end if

          end if !! end if we have some column to work with

        end do !! end loop over nnz

      end if !! end if we have nnz

    end do !! end loop over cells
    
    return

  end subroutine modify_rhs_for_symmetry

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> The RHS is updated to symmetrize the matrix. With HDG, having a 
  !> Dirichlet boundary conditions lead to identity sub-block that 
  !> break the original symmetry. We enforce the symmetry by updating
  !> the RHS, i.e., some coefficient of the matrix have been set to 
  !> zero, while some part of rhs are now non-zero.
  !>
  !
  !> @param[in]    n_cell_loc       : number of cells to treat
  !> @param[inout] n_dof_rhs        : number of nnz entries for rhs (updated)
  !> @param[inout] nnz_per_cell     : number of nnz for each cell
  !> @param[inout] nnz_line         : line of the nnz
  !> @param[inout] nnz_col          : col  of the nnz
  !> @param[inout] nnz_val          : entries of the matrix in (line,col)
  !> @param[inout] col_loc          : index  of rhs component
  !> @param[inout] rhs_loc          : value  of rhs component
  !----------------------------------------------------------------------------
  subroutine modify_rhs_for_symmetry_adjoint(n_cell_loc,n_dof_rhs,      &
                                             nnz_per_cell,nnz_line,     &
                                             nnz_col,nnz_val,col_loc,   &
                                             rhs_loc)

    implicit none
    
    integer(kind=IKIND_MESH)            , intent(in)    :: n_cell_loc
    integer                             , intent(inout) :: n_dof_rhs
    integer                , allocatable, intent(in)    :: nnz_per_cell(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_line(:)
    type(t_array1d_int)    , allocatable, intent(in)    :: nnz_col(:)
    type(t_array1d_complex), allocatable, intent(in)    :: nnz_val(:)
    integer(kind=IKIND_MAT), allocatable, intent(inout) :: col_loc(:)
    complex(kind=8)        , allocatable, intent(inout) :: rhs_loc(:)

    !! local
    integer(kind=IKIND_MESH) :: c
    integer                  :: n_dof_rhs_in,i_sym_rhs,i_temp
    complex(kind=8)          :: ref

    n_dof_rhs_in = n_dof_rhs
    do c = 1, n_cell_loc
      if(nnz_per_cell(c) > 0) then

        !! we update the rhs with additional values, Mumps will 
        !! update the global rhs automatically.
        do i_sym_rhs = 1, nnz_per_cell(c)
          if(nnz_col(c)%array(i_sym_rhs) > 0) then
           
            !! get the ref value from the already existing rhs. 
            ref = dcmplx(0.d0, 0.d0)
            do i_temp = 1, n_dof_rhs_in
              if(nnz_col(c)%array(i_sym_rhs) .eq. col_loc(i_temp)) then
                ref = ref + rhs_loc(i_temp)
              end if
            end do
            
            !! add new values if ref is non-zero -----------------------
            if(abs(ref) > tiny(1.0)) then
              n_dof_rhs = n_dof_rhs + 1
              col_loc(n_dof_rhs) = nnz_line(c)%array(i_sym_rhs)
              
              !! minus here.
              rhs_loc(n_dof_rhs) =-dcmplx(ref*conjg(nnz_val(c)%array(i_sym_rhs)))
            end if

          end if !! end if we have some column to work with

        end do !! end loop over nnz

      end if !! end if we have nnz

    end do !! end loop over cells
    
    return

  end subroutine modify_rhs_for_symmetry_adjoint


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create RHS for HDG
  !>   -> when we use sparse CSR representation for the RHS, it means that
  !>      only MASTER processor contains all infos, because Mumps does not
  !>      seem to deal with distributed Sparse RHS
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    source_value   : source value
  !> @param[inout] ctx_dg         : DG context
  !> @param[inout] ctx_mesh       : mesh context
  !> @param[inout] nrhs           : number of rhs
  !> @param[inout] n_rhs_comp     : number of rhs component
  !> @param[inout] rhs_i_comp     : index  of rhs component
  !> @param[inout] rhs_i_comp_bc  : index  of rhs component for boundary
  !> @param[inout] csri           : CSR matrix I
  !> @param[inout] icol           : CSR matrix col
  !> @param[inout] Rval           : CSR matrix values
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_rhs_create_lagrange_sparse(ctx_paral,source_value,ctx_dg,  &
                                           ctx_mesh,nrhs,                  &
                                           n_rhs_component,                &
                                           rhs_i_component,                &
                                           rhs_i_component_bc,csri,icol,   &
                                           Rval,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    complex(kind=8)                    ,intent(in)    :: source_value
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_discretization)             ,intent(inout) :: ctx_dg
    integer                            ,intent(in)    :: nrhs
    integer                            ,intent(in)    :: n_rhs_component
    integer                ,allocatable,intent(in)    :: rhs_i_component   (:)
    integer                ,allocatable,intent(in)    :: rhs_i_component_bc(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: csri(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: icol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: Rval(:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! local
    integer                                 :: i_rhs,c,ndof_all
    integer                                 :: upper_bound,counter_dof_face
    integer                                 :: i_comp,ind_offset,ndof_vol
    integer                                 :: ndof_face,order,i_order
    integer                                 :: f,i,j,i_orient,igb_comp
    integer                                 :: counter_dof
    integer(kind=IKIND_MAT)                 :: ndof_per_var,ind,nval
    integer(kind=IKIND_MESH)                :: local_cell,face
    integer                                 :: iloc,ndof_face_cell,var
    integer(kind=IKIND_MAT)  ,allocatable   :: temp_icol(:)
    integer(kind=IKIND_MAT)  ,allocatable   :: col_loc(:)
    complex(kind=8),allocatable             :: temp_aval(:)
    complex(kind=8),allocatable             :: rhs_loc(:)
    complex(kind=8)                         :: coeff
    !! to assemble on master the csr rhs 
    integer                                 :: i_proc,tag,i_dim
    integer                                 :: ind_in_weight
    integer(kind=IKIND_MAT)                 :: ndof_max
    integer(kind=IKIND_MAT)  ,allocatable   :: iloc1,igb1,iloc2,igb2
    integer(kind=IKIND_MAT)  ,allocatable   :: gb_counter      (:)
    integer(kind=IKIND_MAT)  ,allocatable   :: mpi_rhs_ndof_loc(:,:)
    integer(kind=IKIND_MAT)  ,allocatable   :: mpi_rhs_ndof_gb (:,:)
    integer(kind=IKIND_MAT)  ,allocatable   :: received_col (:)
    complex(kind=8)          ,allocatable   :: received_val (:)
    complex(kind=RKIND_POL)  ,allocatable   :: srcval_dirac(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! at first, we keep local rhs info on the processor. Afterward, the
    !! master will get everything.
    upper_bound = 0
    do i_rhs=1, nrhs
      !! array is filled per point-source.
      ndof_face=sum(ctx_dg%src(i_rhs)%ndof_face(:))
      !! multiply by the number of variable
      upper_bound = upper_bound + ndof_face*ctx_dg%dim_var
      
      !! include the extra-coefficient for symmetrization of the matrix
      !! (it is only for sources that touch the boundary actually but 
      !!  we cannot know here)
      if(ctx_dg%src(i_rhs)%ncell_loc > 0) then 
        do c = 1, ctx_dg%src(i_rhs)%ncell_loc
          local_cell  = ctx_dg%src(i_rhs)%icell_loc(c)
          upper_bound = upper_bound +                                   &
                        ctx_dg%ctx_symmetrize_mat%cell_nval(local_cell)
        end do
      end if
      !! ---------------------------------------------------------------
    end do

    allocate(csri     (nrhs+1))
    allocate(temp_icol(upper_bound))
    allocate(temp_aval(upper_bound))
    csri(:)   = 1 !! all at 1
    temp_icol = 0
    temp_aval = 0.0 

    !! all infos for the sources DG objects are contained in dg%src(:)
    do i_rhs=1, nrhs
      if(ctx_err%ierr .ne. 0) cycle

      !! Is there a rhs on this processor 
      if(ctx_dg%src(i_rhs)%ncell_loc > 0) then 
        
        !! We fill in the local CSR array. 
        ndof_face    = sum(ctx_dg%src(i_rhs)%ndof_face(:))
        ndof_all     = ndof_face * ctx_dg%dim_var !! time Nvariables for face
        !! add the one for the symmetrization of the matrix
        do c = 1, ctx_dg%src(i_rhs)%ncell_loc
          local_cell  = ctx_dg%src(i_rhs)%icell_loc(c)
          ndof_all = ndof_all + ctx_dg%ctx_symmetrize_mat%cell_nval(local_cell)
        end do

        ndof_per_var = ctx_dg%n_dofgb_pervar
        !! local rhs and col
        allocate(rhs_loc(ndof_all)) ; rhs_loc = 0.0
        allocate(col_loc(ndof_all)) ; col_loc = 0

        !! loop over all concerned cells >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !! 1. We need to create the array S for HDG discretization
        !!    only if it is a volumic source.
        !! 2. We fill in the local rhs csr array, using the global
        !!    indexes.
        !! 2a) Volumic sources
        !! 2b) Surface sources
        ! ---------------------------------------------------------
        counter_dof=0
        do c = 1, ctx_dg%src(i_rhs)%ncell_loc
          
          if(ctx_err%ierr .ne. 0) cycle
          
          local_cell     = ctx_dg%src(i_rhs)%icell_loc(c)
          ndof_vol       = ctx_dg%src(i_rhs)%ndof_vol(c)
          ndof_all       = ndof_vol * ctx_dg%dim_sol !! time Nsolution for vol
          ndof_face_cell = ctx_dg%src(i_rhs)%ndof_face(c)

          !! -----------------------------------------------------------
          !! the HDG array concerns ONLY volume source, in the case
          !! the source is on the boundary, we MUST HAVE 0
          !! -----------------------------------------------------------
          if(.not. ctx_dg%src(i_rhs)%flag_on_bdry(c)) then
            allocate(srcval_dirac(ndof_vol))

            call array_allocate(ctx_dg%src(i_rhs)%hdg_S(c),ndof_all,ctx_err)
            ctx_dg%src(i_rhs)%hdg_S(c)%array = dcmplx(0.0,0.0)
            
            !! offset for current component (i.e. solution)
            do i_comp = 1, n_rhs_component
              igb_comp     = rhs_i_component(i_comp)
              ind_offset   = (igb_comp-1)*ndof_vol
              
              !! It depends on the type of dirac source ----------------
              srcval_dirac = 0.d0
              select case(ctx_dg%src(i_rhs)%src_type)
                case(SRC_DIRAC_DERIV)
                  !! depends on which component is the source.
                  srcval_dirac = 0.d0
                  select case(trim(adjustl(ctx_dg%src(i_rhs)%src_component(i_comp))))
                    !! for scalar variable, we apply a divergence.
                    case(tag_FIELD_P,tag_FIELD_U,tag_FIELD_V,tag_FIELD_W)
                      do i_dim = 1, ctx_dg%dim_domain
                        !! minus because of Integration by part for derivative.
                        srcval_dirac = srcval_dirac - cmplx(              &
                        ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                         %array(i_dim,:)                  &
                        * source_value,kind=RKIND_POL)
                      end do
                    
                    !! only X
                    case(tag_FIELD_UX,tag_FIELD_VX)
                      !! minus because of Integration by part for derivative.
                      srcval_dirac = srcval_dirac - cmplx(              &
                      ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                       %array(1,:)                      &
                      * source_value,kind=RKIND_POL)

                    !! only Y
                    case(tag_FIELD_UY,tag_FIELD_VY)
                      !! minus because of Integration by part for derivative.
                      srcval_dirac = srcval_dirac - cmplx(              &
                      ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                       %array(2,:)                      &
                      * source_value,kind=RKIND_POL)
                    !! only Z
                    case(tag_FIELD_UZ,tag_FIELD_VZ)
                      !! minus because of Integration by part for derivative.
                      srcval_dirac = srcval_dirac - cmplx(              &
                      ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                       %array(ctx_dg%dim_domain,:)      &
                      * source_value,kind=RKIND_POL)
                    !! these are unclear
                    !! tag_FIELD_SXX,tag_FIELD_SXY,tag_FIELD_SXZ
                    !! tag_FIELD_SYY,tag_FIELD_SYZ,tag_FIELD_SZZ
                    case default
                      !! raise error
                      ctx_err%msg   = "** ERROR: Unrecognized field " //&
                                      "of directional derivative "    //&
                                      "source [dg_rhs_create_sparse] **"
                      ctx_err%ierr  = -304
                      ctx_err%critic=.true.
                      call raise_error(ctx_err)
                  end select
                case(SRC_DIRAC_DERIVX)
                  i_dim = 1
                  !! minus because of Integration by part for derivative.
                  srcval_dirac =-cmplx(                                 &
                      ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                       %array(i_dim,:)                  &
                      * source_value,kind=RKIND_POL)
                case(SRC_DIRAC_DERIVY)
                  if(ctx_dg%dim_domain .ne. 3) then
                    !! raise error
                    ctx_err%msg   = "** ERROR: y-derivative for "     //&
                                    "source only works in 3D "        //&
                                    "[dg_rhs_create_sparse] **"
                    ctx_err%ierr  = -304
                    ctx_err%critic=.true.
                    call raise_error(ctx_err)
                  end if
                  
                  i_dim = 2 !! only 2nd dimension in 3D
                  !! minus because of Integration by part for derivative.
                  srcval_dirac =-cmplx(                                 &
                      ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                       %array(i_dim,:)                  &
                      * source_value,kind=RKIND_POL)

                case(SRC_DIRAC_DERIVZ)
                  if(ctx_dg%dim_domain <= 1) then
                    !! raise error
                    ctx_err%msg   = "** ERROR: y-derivative for "     //&
                                    "source only works in 3D "        //&
                                    "[dg_rhs_create_sparse] **"
                    ctx_err%ierr  = -304
                    ctx_err%critic=.true.
                    call raise_error(ctx_err)
                  end if

                  i_dim = ctx_dg%dim_domain
                  !! minus because of Integration by part for derivative.
                  srcval_dirac =-cmplx(                                 &
                      ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                       %array(i_dim,:)                  &
                      * source_value,kind=RKIND_POL)

                case(SRC_DIRAC_DIV)
                  srcval_dirac = 0.d0
                  do i_dim = 1, ctx_dg%dim_domain
                    !! minus because of Integration by part for derivative.
                    srcval_dirac = srcval_dirac - cmplx(                &
                      ctx_dg%src(i_rhs)%weight_dbasis(igb_comp,c)       &
                                       %array(i_dim,:)                  &
                      * source_value,kind=RKIND_POL)
                  end do

                case(SRC_DIRAC,SRC_ANALYTIC,SRC_GAUSSIAN)
                  srcval_dirac = cmplx(ctx_dg%src(i_rhs)                &
                                             %weight(igb_comp,c)%array  &
                                       * source_value,kind=RKIND_POL)
                case(SRC_PW)
                  !! raise error
                  ctx_err%msg   = "** ERROR: Planewave of function "  //& 
                                  "imposed should not appear as a "   //&
                                  "volume source source [dg_rhs_create_sparse] **"
                  ctx_err%ierr  = -301
                  ctx_err%critic=.true.
                  call raise_error(ctx_err)
                case default
                  !! raise error
                  ctx_err%msg   = "** ERROR: Unrecognized type of " //&
                                  "source [dg_rhs_create_sparse] **"
                  ctx_err%ierr  = -300
                  ctx_err%critic=.true.
                  call raise_error(ctx_err)
              end select
              !! update array
              ctx_dg%src(i_rhs)%hdg_S(c)%array(ind_offset+1:   &
                                               ind_offset+ndof_vol)=srcval_dirac
            end do
            deallocate(srcval_dirac)
            
            !! -----------------------------------------------------------
            !! For the volumic sources, we have to send it back to the 
            !! numerical traces
            !! -----------------------------------------------------------
            counter_dof_face = 0

            do f=1,ctx_mesh%n_neigh_per_cell     !! loop over all face
              face       = ctx_mesh%cell_face   (f,local_cell)
              order      = ctx_mesh%order_face  (face)
              i_order    = ctx_dg%order_to_index(order)
              ndof_face  = ctx_dg%n_dof_face_per_order(i_order)

              do var=1,ctx_dg%dim_var            !! loop over all matrix var
                do i=1,ndof_face                 !! loop over all face dof
                  !! get global index
                  ind = (var-1)*ndof_per_var + ctx_dg%offsetdof_mat_gb(face)
                  i_orient = ctx_mesh%cell_face_orientation(f,local_cell)
                  ind = ind + ctx_dg%hdg%idof_face(i_orient,i_order)%array(i)

                  iloc  = (var-1)*ndof_face_cell + counter_dof_face + i
                  coeff = 0.0
            
                  do j=1,ndof_vol*ctx_dg%dim_sol !! loop over all vol dof for value
                     !! note that X emcompasses a minus sign already
                     coeff = coeff + dcmplx(                            &
                             ctx_dg%hdg%X(local_cell)%array(iloc,j)*    &
                             ctx_dg%src(i_rhs)%hdg_S(c)%array(j))
                  end do
                  counter_dof = counter_dof + 1
                  col_loc(counter_dof) = ind
                  rhs_loc(counter_dof) = coeff
                end do
              end do
              counter_dof_face = counter_dof_face + ndof_face
            end do

          else !!  boundary source >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            !! the source is on the face, so we use the current 
            !! face dof only, with appropriate weight.
            !! Careful, src%idof_face contains the CSR i_index 
            !! we need to current dof vol index corresponding to
            !! the face dof given by (f,i) with increment of var.
            !! ---------------------------------------------------------
            f          = ctx_dg%src(i_rhs)%iface_index(c)
            face       = ctx_mesh%cell_face   (f,local_cell)
            ndof_face  = ctx_dg%src(i_rhs)%ndof_face(c)
            order      = ctx_mesh%order_face  (face)
            i_order    = ctx_dg%order_to_index(order)
            
            select case(ctx_dg%src(i_rhs)%src_type)
              case(SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,      &
                   SRC_DIRAC_DERIVY,SRC_DIRAC_DERIVZ)
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized derivative "//  &
                                "type of source for boundary " //       &
                                "[dg_rhs_create_sparse] **"
                ctx_err%ierr  = -300
                ctx_err%critic=.true.
                call raise_error(ctx_err)
              case(SRC_DIRAC,SRC_PW,SRC_ANALYTIC)
                !! ok we can continue.
              case default
                !! raise error
                ctx_err%msg   = "** ERROR: Unrecognized type of " //&
                                "source [dg_rhs_create_sparse] **"
                ctx_err%ierr  = -300
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

            !! only loop over the variables that are concerned
            do i_comp=1,n_rhs_component
              if(ctx_err%ierr .ne. 0) cycle
              
              !! ------------------------------------------------------------
              var = rhs_i_component_bc(i_comp)
              !! sanity check
              if(var > ctx_dg%dim_var .or. var <= 0) then
                ctx_err%msg   = "** ERROR: Boundary source not on " //  &
                                "a matrix variable [rhs_sparse] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
              end if

              !! we still use the position of the solution that 
              !! has been created in the weight array.
              ind_in_weight = rhs_i_component(i_comp)
              !! ------------------------------------------------------------
              do i=1,ndof_face                 !! loop over all face dof
                !! get global index
                ind = (var-1)*ndof_per_var + ctx_dg%offsetdof_mat_gb(face)
                i_orient = ctx_mesh%cell_face_orientation(f,local_cell)
                ind = ind + ctx_dg%hdg%idof_face(i_orient,i_order)%array(i)

                counter_dof = counter_dof + 1
                col_loc(counter_dof) = ind
                rhs_loc(counter_dof) = dcmplx(                          &
                        ctx_dg%src(i_rhs)%weight(ind_in_weight,c)%array(i)*source_value)
              end do
            end do

          end if
          ! -------------------------------------------------------

        end do !! end loop over current processor concerned cells

        !! -------------------------------------------------------------
        !!
        !! now that we have the "main" rhs, we adjust for symmetry
        !! in case we have dirichlet or planewave source with hdg.
        !!
        !! -------------------------------------------------------------
        if(ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize) then
           call modify_rhs_for_symmetry(ctx_mesh%n_cell_loc,counter_dof,    &
                                       ctx_dg%ctx_symmetrize_mat%cell_nval, &
                                       ctx_dg%ctx_symmetrize_mat%c_line,    &
                                       ctx_dg%ctx_symmetrize_mat%c_col,     &
                                       ctx_dg%ctx_symmetrize_mat%c_val,     &
                                       col_loc,rhs_loc)
        end if
        !! -------------------------------------------------------------
        
        !! update the local csr array infos ----------------------------
        csri(i_rhs+1) = csri(i_rhs) + counter_dof
        temp_icol(csri(i_rhs):csri(i_rhs+1)-1) = col_loc(1:counter_dof)
        temp_aval(csri(i_rhs):csri(i_rhs+1)-1) = rhs_loc(1:counter_dof)
        !! -------------------------------------------------------------   
        
        deallocate(col_loc)
        deallocate(rhs_loc)
      else
        !! there is no source here, we just repeat the last entry of CSR
        csri(i_rhs+1) = csri(i_rhs)

      end if !! end if there is a rhs for current processor
        
    end do !! end loop over all rhs

    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)

    ! ------------------------------------------------------------------
    !! because of MUMPS, we must create the global CSR array for rhs 
    !! on the master processor only.
    ! ------------------------------------------------------------------

    !! count what is the total amount of dof per rhs on each proc.
    allocate(mpi_rhs_ndof_loc(nrhs,ctx_paral%nproc))
    allocate(mpi_rhs_ndof_gb (nrhs,ctx_paral%nproc))
    mpi_rhs_ndof_loc = 0
    do i_rhs=1, nrhs     
      mpi_rhs_ndof_loc(i_rhs,ctx_paral%myrank+1) = csri(i_rhs+1)-csri(i_rhs)
    end do
    !! all reduce 
    mpi_rhs_ndof_gb = 0
    call allreduce_sum(mpi_rhs_ndof_loc,mpi_rhs_ndof_gb,                &
                       nrhs*ctx_paral%nproc,ctx_paral%communicator,ctx_err)
    deallocate(mpi_rhs_ndof_loc)

    !! master allocate with the appropriate dimension then
    if(ctx_paral%master) then
      csri(:) = 0
      csri(1) = 1
      do i_rhs=1, nrhs
        csri(i_rhs+1) = csri(i_rhs) + sum(mpi_rhs_ndof_gb(i_rhs,:))
      end do
      allocate(icol(csri(nrhs+1)-1))
      allocate(Rval(csri(nrhs+1)-1))
      icol = 0
      Rval = 0.d0
    else
      deallocate(csri) !! no more needed for non-master cpu
    end if
    !! ----------------------------------------------------------------

    !! ----------------------------------------------------------------     
    !! now, master receive what the other send.
    !! ----------------------------------------------------------------    
    if(ctx_paral%master) then
      !! dof at most
      ndof_max = 0
      do i_proc=1,ctx_paral%nproc
        ndof_max = max(ndof_max, sum(mpi_rhs_ndof_gb(:,i_proc)))
      end do

      allocate(received_col(ndof_max))
      allocate(received_val(ndof_max))
      allocate(gb_counter(nrhs))
      gb_counter(:) = csri(1:nrhs)

      !! it starts at 0 
      do i_proc=0,ctx_paral%nproc-1

        !! myself -----------------------------------------------------
        if(i_proc == ctx_paral%myrank) then
          !! loop over all rhs to fill the array 
          iloc1 = 1
          do i_rhs=1, nrhs
            nval = mpi_rhs_ndof_gb(i_rhs,i_proc+1)
            if(nval > 0) then
              igb1 = gb_counter(i_rhs) 
              igb2 = gb_counter(i_rhs)+nval-1 
              iloc2= iloc1 + nval - 1
              icol(igb1:igb2) = temp_icol(iloc1:iloc2)
              Rval(igb1:igb2) = cmplx(temp_aval(iloc1:iloc2),kind=RKIND_MAT)
              iloc1 = iloc1 + nval
              gb_counter(i_rhs) = gb_counter(i_rhs) + nval
            end if
          end do
          
        !! ------------------------------------------------------------
        !! otherwize I receive from the others
        else
          !! receive infos from proc labeled i_proc
          nval = sum(mpi_rhs_ndof_gb(:,i_proc+1))
          received_col = 0
          received_val = 0.d0
          if(nval > 0) then
            tag  = i_proc
            !! force int for mpi
            call receive(received_col(1:nval),int(nval),i_proc,tag,    &
                         ctx_paral%communicator,ctx_err)
            tag  = ctx_paral%nproc + i_proc
            call receive(received_val(1:nval),int(nval),i_proc,tag,    &
                         ctx_paral%communicator,ctx_err)

            !! fill the array for all rhs
            iloc1 = 1
            do i_rhs=1, nrhs
              !! local number of values for this mpi and this rhs.
              nval = mpi_rhs_ndof_gb(i_rhs,i_proc+1)
              if(nval > 0) then
                igb1 = gb_counter(i_rhs) 
                igb2 = gb_counter(i_rhs)+nval-1 
                iloc2= iloc1 + nval - 1
                icol(igb1:igb2) = received_col(iloc1:iloc2)
                Rval(igb1:igb2) = cmplx(received_val(iloc1:iloc2),    &
                                        kind=RKIND_MAT)
                iloc1 = iloc1 + nval
                gb_counter(i_rhs) = gb_counter(i_rhs) + nval
              end if
            end do
          end if

        end if
      end do
      deallocate(received_col)
      deallocate(received_val)
    else
      !! I am not the master: I send to him/her/it then.
      nval = sum(mpi_rhs_ndof_gb(:,ctx_paral%myrank+1))
      if(nval > 0) then
        !! force int for mpi
        tag  = ctx_paral%myrank
        call send(temp_icol(1:nval),int(nval),0,tag,                   &
                  ctx_paral%communicator,ctx_err)
        tag  = ctx_paral%nproc + ctx_paral%myrank
        call send(temp_aval(1:nval),int(nval),0,tag,                   &
                  ctx_paral%communicator,ctx_err)
      end if
    end if

    deallocate(temp_icol)
    deallocate(temp_aval)
    deallocate(mpi_rhs_ndof_gb)

    !! -----------------------------------------------------------------
    call barrier(ctx_paral%communicator,ctx_err)

    return
  end subroutine dg_rhs_create_lagrange_sparse

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create Backward RHS for HDG
  !>   -> We fill the HDG array information in the receivers:
  !>       t_discretization%src(i_source)%rcv(i_rcv)%hdg_R for example
  !>   -> when we use sparse CSR representation for the RHS, it means that
  !>      only MASTER processor contains all infos, because Mumps does not
  !>      seem to deal with distributed Sparse RHS
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] ctx_dg         : DG context
  !> @param[in]    ctx_mesh       : mesh context
  !> @param[in]    nrhs           : number of rhs
  !> @param[in]    source_residu  : rhs values, same on all cpus.
  !> @param[inout] csri           : CSR matrix I
  !> @param[inout] icol           : CSR matrix col
  !> @param[inout] Rval           : CSR matrix values
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_rhs_backward_create_lagrange_sparse(ctx_paral,ctx_dg,   &
                    ctx_mesh,nrhs,source_residuals,csri,icol,Rval,ctx_err)

    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_discretization)             ,intent(inout) :: ctx_dg
    integer                            ,intent(in)    :: nrhs
    complex(kind=8)        ,allocatable,intent(in)    :: source_residuals(:,:,:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: csri(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: icol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: Rval(:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! local
    integer                                 :: i_rhs,nrcv
    integer                                 :: i_rcv,n_comp
    integer                                 :: i_comp,ind_offset,ndof_vol
    integer                                 :: order,i_order
    integer(kind=IKIND_MESH)                :: ndof_face,ind_loc,n_temp
    integer(kind=IKIND_MESH)                :: upper_bound,ndof_all
    integer(kind=IKIND_MESH)                :: counter_dof,i,iloc,j
    integer                                 :: f,i_orient
    integer(kind=IKIND_MAT)                 :: ndof_per_var,ind
    integer(kind=IKIND_MESH)                :: local_cell,face
    integer                                 :: ndof_face_cell,var
    real   (kind=8)                         :: tol
    complex(kind=8)                         :: coeff
    integer(kind=IKIND_MAT)  ,allocatable   :: temp_icol(:)
    complex(kind=8),allocatable             :: temp_aval(:)
    !! to assemble on master the csr rhs 
    integer                                 :: i_proc,tag
    integer(kind=IKIND_MAT)                 :: ndof_max,nval
    integer(kind=IKIND_MAT)  ,allocatable   :: iloc1,igb1,iloc2,igb2
    integer(kind=IKIND_MAT)  ,allocatable   :: gb_counter      (:)
    integer(kind=IKIND_MAT)  ,allocatable   :: mpi_rhs_ndof_loc(:,:)
    integer(kind=IKIND_MAT)  ,allocatable   :: mpi_rhs_ndof_gb (:,:)
    integer(kind=IKIND_MAT)  ,allocatable   :: received_col (:)
    complex(kind=8)          ,allocatable   :: received_val (:)
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0
    n_comp        = ctx_dg%dim_sol
    ndof_per_var  = ctx_dg%n_dofgb_pervar

    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif

    !! -----------------------------------------------------------------
    !! we use CSR format here, only master has it at the end.
    !! first a temporary array with the upper bound
    upper_bound = 0
    do i_rhs=1, nrhs
      !! update with the number of receivers for the current source
      nrcv      = ctx_dg%src(i_rhs)%nrcv
      ndof_face = 0
      n_temp    = 0
      do i_rcv=1, nrcv
        !! It assumes only one cell is impacted by the receivers
        !! multiplication by the variable dimension too.
        n_temp = n_temp + ctx_dg%src(i_rhs)%rcv(i_rcv)%ndof_face*ctx_dg%dim_var

        !! -------------------------------------------------------------
        !! include the extra-coefficient for symmetrization of the matrix
        !! (it is only for boundary sources actually but we cannot know here)
        local_cell     = ctx_dg%src(i_rhs)%rcv(i_rcv)%icell_loc
        if(local_cell .ne. 0) then
          n_temp = n_temp + ctx_dg%ctx_symmetrize_mat%cell_nval(local_cell)
        end if
        !! -------------------------------------------------------------

      end do
      !! overall total
      call allreduce_sum(n_temp,ndof_face,1,ctx_paral%communicator,ctx_err)
      !! multiply by the number of variable
      upper_bound = upper_bound + ndof_face
    end do
    
    !! every mpi has its csr rhs, which is then assembled onto the master.
    allocate(csri     (nrhs+1))
    allocate(temp_icol(upper_bound))
    allocate(temp_aval(upper_bound))
    csri(1)   = 1
    temp_icol = 0
    temp_aval = 0.0
    
    !! all infos for the receivers for DG are contained in dg%src(:)%rcv(:)
    ind_loc = 0
    do i_rhs=1, nrhs

      nrcv  = ctx_dg%src(i_rhs)%nrcv

      do i_rcv = 1, nrcv

        local_cell     = ctx_dg%src(i_rhs)%rcv(i_rcv)%icell_loc

        if(local_cell .ne. 0) then

          ! ------------------------------------------------------------
          !  1) for all source, we need to create an HDG array
          !     for every cell that are affected, it is needed
          !     when we want to project to the local solutions:
          !   -> get the number of dof for this cell
          !   -> allocate ctx_dg%src(i_rhs)%rcv(i_rcv)%hdg_R
          !   -> fill it for all the components
          ! ------------------------------------------------------------
          ndof_vol = ctx_dg%src(i_rhs)%rcv(i_rcv)%ndof_vol
          ndof_all = ndof_vol * ctx_dg%dim_sol
          allocate(ctx_dg%src(i_rhs)%rcv(i_rcv)%hdg_R(ndof_all))
          ctx_dg%src(i_rhs)%rcv(i_rcv)%hdg_R = dcmplx(0.d0,0.d0)
          !! offset for current component (i.e. solution)
          do i_comp = 1, n_comp !! n_comp = dim_sol here because backward
            ind_offset = (i_comp-1)*ndof_vol
            ctx_dg%src(i_rhs)%rcv(i_rcv)%hdg_R(ind_offset+1:            &
                                               ind_offset+ndof_vol)=    &
                            cmplx(ctx_dg%src(i_rhs)%rcv(i_rcv)%weight*  &
                                  source_residuals(i_rcv,i_comp,i_rhs))
          end do
          !! -----------------------------------------------------------

          ndof_face_cell = ctx_dg%src(i_rhs)%rcv(i_rcv)%ndof_face
          ndof_vol       = ctx_dg%src(i_rhs)%rcv(i_rcv)%ndof_vol
          counter_dof    = 0
          do f=1,ctx_mesh%n_neigh_per_cell     !! loop over all face
            face       = ctx_mesh%cell_face   (f,local_cell)
            order      = ctx_mesh%order_face  (face)
            i_order    = ctx_dg%order_to_index(order)
            ndof_face  = ctx_dg%n_dof_face_per_order(i_order)

            do var=1,ctx_dg%dim_var            !! loop over all matrix var
              do i=1,ndof_face                 !! loop over all face dof
                !! get global index
                ind = (var-1)*ndof_per_var + ctx_dg%offsetdof_mat_gb(face)
                i_orient = ctx_mesh%cell_face_orientation(f,local_cell)
                ind = ind + ctx_dg%hdg%idof_face(i_orient,i_order)%array(i)

                iloc  = (var-1)*ndof_face_cell + counter_dof + i
                coeff = 0.0
                do j=1,ndof_vol*ctx_dg%dim_sol !! loop over all vol dof for value
                   coeff = coeff - dcmplx(                              &
                           ctx_dg%hdg%W(local_cell)%array(iloc,j)*      &
                           ctx_dg%src(i_rhs)%rcv(i_rcv)%hdg_R(j))
                end do
                ind_loc            = ind_loc + 1 
                temp_icol(ind_loc) = ind
                !! *****************************************************
                !! We use a minus because we are using the residuals:
                !! Au=0 => dpA u + A dpu = 0
                !!      => A dpu = -dpA u
                !! *****************************************************
                temp_aval(ind_loc) =-coeff
                !! *****************************************************
              end do
            end do
            counter_dof = counter_dof + ndof_face
          end do
        
          !! -----------------------------------------------------------
          !! in case of boundary source, we must adjust the RHS to 
          !! maintain the symmetry of the matrix, only if we have 
          !! a Dirichlet Boundary Condition.
          !! Careful that the backward works with the TRANSPOSED of 
          !! the matrix, so we invert col and line.
          !! ===========================================================
          !! REMARK: it is ok here because every cell is independant, 
          !!         so no need to have the full rhs.
          !! -----------------------------------------------------------

          !! ---------------------------------------------------------
        
        end if !! end if the rcv is in the cell

      end do  !! end loop over rcv


      !! ---------------------------------------------------------------
      !!
      !! now that we have the "main" rhs, we adjust for symmetry
      !! in case we have dirichlet or planewave source with hdg.
      !!
      !! ---------------------------------------------------------------
      if(ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize) then
         call modify_rhs_for_symmetry_adjoint(ctx_mesh%n_cell_loc,        &
                                     ind_loc,                         &
                                     ctx_dg%ctx_symmetrize_mat%cell_nval, &
                                     ctx_dg%ctx_symmetrize_mat%c_line,    &
                                     ctx_dg%ctx_symmetrize_mat%c_col,     &
                                     ctx_dg%ctx_symmetrize_mat%c_val,     &
                                     temp_icol,temp_aval)
      end if
      !! -------------------------------------------------------------

      !! update the csr count on the current processor, using the counters
      csri(i_rhs+1) = csri(i_rhs) + (ind_loc-csri(i_rhs)+1)
      ! ----------------------------------------------------------------

    end do !! loop over all rhs

    ! ------------------------------------------------------------------
    !! because of MUMPS, we must create the global CSR array for rhs 
    !! on the master processor only.
    ! ------------------------------------------------------------------

    !! count what is the total amount of dof per rhs on each proc.
    allocate(mpi_rhs_ndof_loc(nrhs,ctx_paral%nproc))
    allocate(mpi_rhs_ndof_gb (nrhs,ctx_paral%nproc))
    mpi_rhs_ndof_loc = 0
    do i_rhs=1, nrhs     
      mpi_rhs_ndof_loc(i_rhs,ctx_paral%myrank+1) = csri(i_rhs+1)-csri(i_rhs)
    end do
    !! all reduce 
    mpi_rhs_ndof_gb = 0
    call allreduce_sum(mpi_rhs_ndof_loc,mpi_rhs_ndof_gb,                &
                       nrhs*ctx_paral%nproc,ctx_paral%communicator,ctx_err)
    deallocate(mpi_rhs_ndof_loc)

    !! master allocate with the appropriate dimension then
    if(ctx_paral%master) then
      csri(:) = 0
      csri(1) = 1
      do i_rhs=1, nrhs
        csri(i_rhs+1) = csri(i_rhs) + sum(mpi_rhs_ndof_gb(i_rhs,:))
      end do
      allocate(icol(csri(nrhs+1)-1))
      allocate(Rval(csri(nrhs+1)-1))
      icol = 0
      Rval = 0.d0
    else
      deallocate(csri) !! no more needed for non-master cpu
    end if
    ! ----------------------------------------------------------------

    !! ----------------------------------------------------------------     
    !! now, master receive what the other send.
    !! ----------------------------------------------------------------    
    if(ctx_paral%master) then
      !! dof at most
      ndof_max = 0
      do i_proc=1,ctx_paral%nproc
        ndof_max = max(ndof_max, sum(mpi_rhs_ndof_gb(:,i_proc)))
      end do

      allocate(received_col(ndof_max))
      allocate(received_val(ndof_max))
      allocate(gb_counter(nrhs))
      gb_counter(:) = csri(1:nrhs)

      !! it starts at 0 
      do i_proc=0,ctx_paral%nproc-1

        !! myself -----------------------------------------------------
        if(i_proc == ctx_paral%myrank) then
          !! loop over all rhs to fill the array 
          iloc1 = 1
          do i_rhs=1, nrhs
            nval = mpi_rhs_ndof_gb(i_rhs,i_proc+1)
            if(nval > 0) then
              igb1 = gb_counter(i_rhs) 
              igb2 = gb_counter(i_rhs)+nval-1 
              iloc2= iloc1 + nval - 1
              icol(igb1:igb2) = temp_icol(iloc1:iloc2)
              !! *****************************************************
              !! We use a minus because we are using the residuals:
              !! Au=0 => dpA u + A dpu = 0
              !!      => A dpu = -dpA u
              !! this is done at the beginnig.
              !! *****************************************************
              Rval(igb1:igb2) =cmplx(temp_aval(iloc1:iloc2),kind=RKIND_MAT)
              !! *****************************************************
              iloc1 = iloc1 + nval
              gb_counter(i_rhs) = gb_counter(i_rhs) + nval
            end if
          end do
          
        !! ------------------------------------------------------------
        !! otherwize I receive from the others
        else
          !! receive infos from proc labeled i_proc
          nval = sum(mpi_rhs_ndof_gb(:,i_proc+1))
          received_col = 0
          received_val = 0.d0
          if(nval > 0) then
            tag  = i_proc
            !! force int for mpi
            call receive(received_col(1:nval),int(nval),i_proc,tag,    &
                         ctx_paral%communicator,ctx_err)
            tag  = ctx_paral%nproc + i_proc
            call receive(received_val(1:nval),int(nval),i_proc,tag,    &
                         ctx_paral%communicator,ctx_err)

            !! fill the array for all rhs
            iloc1 = 1
            do i_rhs=1, nrhs
              !! local number of values for this mpi and this rhs.
              nval = mpi_rhs_ndof_gb(i_rhs,i_proc+1)
              if(nval > 0) then
                igb1 = gb_counter(i_rhs) 
                igb2 = gb_counter(i_rhs)+nval-1 
                iloc2= iloc1 + nval - 1
                icol(igb1:igb2) = received_col(iloc1:iloc2)
                
                !! *****************************************************
                !! We use a minus because we are using the residuals:
                !! Au=0 => dpA u + A dpu = 0
                !!      => A dpu = -dpA u
                !! this is done at the beginnig.
                Rval(igb1:igb2) = cmplx(received_val(iloc1:iloc2),    &
                                        kind=RKIND_MAT)
                !! *****************************************************
                iloc1 = iloc1 + nval
                gb_counter(i_rhs) = gb_counter(i_rhs) + nval
              end if
            end do
          end if

        end if
      end do
      deallocate(received_col)
      deallocate(received_val)
    else
      !! I am not the master: I send to him/her/it then.
      nval = sum(mpi_rhs_ndof_gb(:,ctx_paral%myrank+1))
      if(nval > 0) then
        !! force int for mpi
        tag  = ctx_paral%myrank
        call send(temp_icol(1:nval),int(nval),0,tag,                   &
                  ctx_paral%communicator,ctx_err)
        tag  = ctx_paral%nproc + ctx_paral%myrank
        call send(temp_aval(1:nval),int(nval),0,tag,                   &
                  ctx_paral%communicator,ctx_err)
      end if
    end if

    deallocate(temp_icol)
    deallocate(temp_aval)
    deallocate(mpi_rhs_ndof_gb)

    !! -----------------------------------------------------------------
    call barrier(ctx_paral%communicator,ctx_err)

    return

  end subroutine dg_rhs_backward_create_lagrange_sparse

end module m_dg_lagrange_rhs_sparse
