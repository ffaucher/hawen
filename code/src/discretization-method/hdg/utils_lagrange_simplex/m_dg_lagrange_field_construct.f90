!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_field_construct.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to reconstruct the wavefields from the 
!> HDG multipliers solution of the linear system, using the 
!> Lagrangian basis function
!
!------------------------------------------------------------------------------
module m_dg_lagrange_field_construct

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: RKIND_MAT,IKIND_MAT,IKIND_MESH
  use m_ctx_parallelism,        only: t_parallelism
  use m_barrier,                only: barrier
  use m_receive,                only: receive
  use m_send,                   only: send
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_domain,             only: t_domain
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: dg_field_construct_lagrange
  private :: hdg_distribute_multiplier,hdg_create_fields

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create fields for HDG: only the master has the global solution,
  !> which are the lagrange multipliers. They first need to be separated
  !> among the processors, and then we re shape to obtain the fields.
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] ctx_dg         : DG context (modified only if HDG)
  !> @param[in]    ctx_mesh       : mesh grid type
  !> @param[in]    n_src          : number of source
  !> @param[in]    n_field        : number of field
  !> @param[out]   n_dofsol_loc   : number of dof per sol locally
  !> @param[out]   n_dofvar_loc   : number of dof per var locally
  !> @param[inout] field          : wavefields
  !> @param[inout] multiplier     : Lagrange multiplier
  !> @param[inout] global_solution: the global solution on master node
  !> @param[in]    flag_backward  : indicates if backward field to create
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_field_construct_lagrange(ctx_paral,ctx_dg,ctx_mesh,     &
                                          global_solution,n_src,n_mult, &
                                          n_field,n_dofsol_loc,         &
                                          n_dofvar_loc,multiplier,      &
                                          field,flag_backward,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_discretization)             ,intent(in)   :: ctx_dg
    type(t_domain)                     ,intent(in)   :: ctx_mesh
    integer                            ,intent(in)   :: n_src,n_field,n_mult
    integer                            ,intent(out)  :: n_dofsol_loc
    integer                            ,intent(out)  :: n_dofvar_loc
    complex(kind=RKIND_MAT),allocatable,intent(inout):: multiplier(:,:)
    complex(kind=RKIND_MAT),allocatable,intent(inout):: field(:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)   :: global_solution(:)
    logical                            ,intent(in)   :: flag_backward
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer           :: ivar,isrc
    
    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------
    !! allocation
    n_dofsol_loc= int(ctx_dg%n_dofloc_persol)
    n_dofvar_loc= int(ctx_dg%n_dofloc_pervar)
    allocate(field(n_src*n_dofsol_loc,n_field))
    field   = 0.0
    allocate(multiplier(n_dofvar_loc*n_src,n_mult))
    multiplier = 0.0
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! loop over all fields and sources to send multipliers
    do ivar=1,n_mult
      call hdg_distribute_multiplier(ctx_paral,ctx_dg,global_solution,  &
                                     multiplier,ivar,n_mult,n_src,ctx_err)
    end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! reconstruct field using OMP
    do isrc=1, n_src
      call hdg_create_fields(ctx_paral,ctx_dg,ctx_mesh,multiplier,field,&
                             isrc,flag_backward,ctx_err)
    end do
    !! -----------------------------------------------------------------

    return

  end subroutine dg_field_construct_lagrange
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Distribute HDG Lagrange multipliers from master processor to slaves
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_dg         : DG context
  !> @param[in]    global_solution: the global solution on master node
  !> @param[inout] multiplier     : local multiplier array 
  !> @param[in]    i_var          : current matrix variable number
  !> @param[in]    n_var          : total matrix variable number
  !> @param[in]    n_src          : number of sources
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_distribute_multiplier(ctx_paral,ctx_dg,global_solution,&
                                       multiplier,i_var,n_var,n_src,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_discretization)             ,intent(in)    :: ctx_dg
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: global_solution(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: multiplier(:,:)
    integer                            ,intent(in)    :: i_var,n_var,n_src
    type(t_error)                      ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    complex(kind=RKIND_MAT),allocatable :: local(:)
    integer                             :: istart_loc,iend_loc,istart_gb
    integer                             :: iface,iend_gb,ndof_loc
    integer                             :: iproc,tag,isrc,nloc
    integer(kind=IKIND_MAT)             :: offset_loc,offset_gb
    !! -----------------------------------------------------------------
    ctx_err%ierr       = 0
    multiplier(:,i_var)= 0.0

    !! -----------------------------------------------------------------
    !! save infos for master first    
    if(ctx_paral%master) then
      nloc = ctx_dg%mpi_ndofvar_count(ctx_paral%myrank+1) * n_src
      allocate(local(nloc))
      !! loop over local faces
      iend_loc=0
      do iface=1,ctx_dg%mpi_nface_count(ctx_paral%myrank+1)
        ndof_loc = ctx_dg%mpi_dofvar_list(ctx_paral%myrank+1)%array(iface,2)
        
        istart_loc= iend_loc+1
        iend_loc  = istart_loc + ndof_loc - 1
        istart_gb = ctx_dg%mpi_dofvar_list(ctx_paral%myrank+1)%array(iface,1)
        iend_gb   = istart_gb  + ndof_loc - 1
        do isrc=1,n_src
          offset_loc = (isrc -1)*ctx_dg%n_dofloc_pervar
          offset_gb  = (isrc -1)*n_var*ctx_dg%n_dofgb_pervar          + &
                       (i_var-1)*      ctx_dg%n_dofgb_pervar
          local(offset_loc+istart_loc:offset_loc+iend_loc) =            &
            global_solution(offset_gb+istart_gb:offset_gb+iend_gb)
        end do
      enddo
      !! save local
      multiplier(:,i_var) = local(:)
      deallocate(local)
    end if
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------
    
    !! for other, send from master
    if(ctx_paral%master) then
      do iproc=1,ctx_paral%nproc-1
        nloc = ctx_dg%mpi_ndofvar_count(iproc+1) * n_src
        allocate(local(nloc))
        !! loop over local faces
        iend_loc=0
        do iface=1,ctx_dg%mpi_nface_count(iproc+1)
          ndof_loc = ctx_dg%mpi_dofvar_list(iproc+1)%array(iface,2)

          istart_loc= iend_loc+1
          iend_loc  = istart_loc + ndof_loc - 1
          istart_gb = ctx_dg%mpi_dofvar_list(iproc+1)%array(iface,1)
          iend_gb   = istart_gb  + ndof_loc - 1
          do isrc=1,n_src
            offset_loc = (isrc -1)*ctx_dg%mpi_ndofvar_count(iproc+1)
            offset_gb  = (isrc -1)*n_var*ctx_dg%n_dofgb_pervar        + &
                         (i_var-1)*      ctx_dg%n_dofgb_pervar
            local(offset_loc+istart_loc:offset_loc+iend_loc) =          &
              global_solution(offset_gb+istart_gb:offset_gb+iend_gb)
          end do
        end do
        !! send 
        tag=(iproc-1)*n_var + i_var
        call send(local,nloc,iproc,tag,ctx_paral%communicator,ctx_err)
        deallocate(local)
      end do
    else
      nloc= int(ctx_dg%n_dofloc_pervar * n_src)
      !! force int for mpi ...
      tag = (ctx_paral%myrank-1)*n_var + i_var
      call receive(multiplier(:,i_var),nloc,0,tag,ctx_paral%communicator,ctx_err)
    end if
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------
    return
  end subroutine hdg_distribute_multiplier


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Create wavefields from HDG Lagrange multipliers
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_dg         : DG context
  !> @param[in]    ctx_mesh       : mesh grid type
  !> @param[in]    multiplier     : Lagrange multiplier
  !> @param[inout] field          : wavefields
  !> @param[in]    i_src          : current source
  !> @param[in]    flag_backward  : indicates if backward field to create
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_create_fields(ctx_paral,ctx_dg,ctx_mesh,multiplier,    &
                               field,i_src,flag_backward,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: multiplier(:,:)
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: field(:,:)
    integer                            ,intent(in)    :: i_src
    logical                            ,intent(in)    :: flag_backward
    type(t_error)                      ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    integer                              :: order,i_order,i_orient
    integer                              :: ndof_cell_face,n,icell_src
    integer                              :: iloc1,iloc2,ndof_vol
    integer                              :: isol,ivar
    integer(kind=IKIND_MAT)              :: offset_gb,igb1,offset_loc,igb2
    integer(kind=IKIND_MESH)             :: iface,icell,ind_face,ic
    integer                              :: idof
    integer                , allocatable :: ndof_face_neigh(:),i_order_face(:)
    complex(kind=RKIND_MAT), allocatable :: lambda_loc(:),sol_loc(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr= 0
    offset_gb =(i_src-1)*ctx_dg%n_dofloc_pervar
    !! ----------------------------------------------------------------- 
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,iface,ind_face,order), &
    !$OMP&         PRIVATE(i_order,ndof_cell_face,ndof_face_neigh),     &
    !$OMP&         PRIVATE(lambda_loc,iloc1,iloc2,igb1,igb2,ndof_vol),  &
    !$OMP&         PRIVATE(sol_loc,isol,ivar,offset_loc,ic,n,icell_src),&
    !$OMP&         PRIVATE(idof,i_order_face,i_orient)
    allocate(ndof_face_neigh(ctx_mesh%n_neigh_per_cell))
    allocate(i_order_face(ctx_mesh%n_neigh_per_cell))
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      i_order  = ctx_dg%order_to_index  (ctx_mesh%order(icell))
      ndof_vol = ctx_dg%n_dof_per_order (i_order)
      ! ----------------------------------------------------------------
      !! count face dof for this cell and neihbors infos
      ndof_cell_face = 0
      do iface=1,ctx_mesh%n_neigh_per_cell
        ind_face      = ctx_mesh%cell_face   (iface,icell)
        order         = ctx_mesh%order_face  (ind_face)
        i_order       = ctx_dg%order_to_index(order)
        i_order_face(iface)=i_order
        ndof_cell_face=ndof_cell_face+ctx_dg%n_dof_face_per_order(i_order)
        ndof_face_neigh(iface) = ctx_dg%n_dof_face_per_order(i_order)
      end do
      ! ----------------------------------------------------------------
      !! obtain lambda loc from multipliers
      allocate(lambda_loc(ndof_cell_face*ctx_dg%dim_var))
      lambda_loc=0.0
      iloc1     =0
      do iface=1,ctx_mesh%n_neigh_per_cell
        ind_face= ctx_mesh%cell_face   (iface,icell)
        i_order = i_order_face(iface)
        do idof = 1, ndof_face_neigh(iface)
          iloc1 = iloc1 + 1
          !! global offsetS : source + iface 
          igb1     = offset_gb + ctx_dg%offsetdof_mat_loc(ind_face)
          i_orient = ctx_mesh%cell_face_orientation(iface,icell)
          igb1 = igb1 + ctx_dg%hdg%idof_face(i_orient,i_order)%array(idof)
          !! loop over all variables
          do ivar=1,ctx_dg%dim_var
            offset_loc=(ivar-1)*ndof_cell_face
            lambda_loc(offset_loc+iloc1) = multiplier(igb1,ivar)
          end do
        end do
      end do

      ! ----------------------------------------------------------------
      !! local solution
      n=ndof_vol*ctx_dg%dim_sol
      allocate(sol_loc(n))
      sol_loc = 0.0
      !! ---------------------------------------------------------------
      !! multiplication depends on the backward or not
      !! ---------------------------------------------------------------
      if(flag_backward) then
        sol_loc(:)=-matmul(ctx_dg%hdg%D(icell)%                         &
                           array(1:n,1:ndof_cell_face*ctx_dg%dim_var),  &
                           lambda_loc(:))
      else
        sol_loc(:)= matmul(ctx_dg%hdg%Q(icell)%array                    &
                          (1:n,1:ndof_cell_face*ctx_dg%dim_var),lambda_loc(:))
      end if

      ! ----------------------------------------------------------------
      !! include source information depends if backward or forward 
      !!
      ! ----------------------------------
      !! Moved at the end to be more efficient.
      ! ----------------------------------

      ! ----------------------------------------------------------------
      !! get the final wavefields
      igb1 = (i_src-1)*ctx_dg%n_dofloc_persol + &
             ctx_dg%offsetdof_sol_loc(icell)  + 1
      igb2 = igb1 + ndof_vol - 1
      do isol=1,ctx_dg%dim_sol
        iloc1 = (isol-1)*ndof_vol + 1
        iloc2 = iloc1  + ndof_vol - 1
        field(igb1:igb2,isol) = sol_loc(iloc1:iloc2)
      end do
      ! ----------------------------------------------------------------
      deallocate(lambda_loc)
      deallocate(sol_loc)
    enddo
    !$OMP END DO
    deallocate(ndof_face_neigh)
    deallocate(i_order_face)

    !! -----------------------------------------------------------------
    !! then we proceed with the sources cells
    !! -----------------------------------------------------------------
    if(flag_backward) then !! it is the receivers for backward pb.
      !$OMP DO
      do icell_src=1,ctx_dg%src(i_src)%nrcv
        ic       = ctx_dg%src(i_src)%rcv(icell_src)%icell_loc
        if( ic > 0) then
          i_order  = ctx_dg%order_to_index  (ctx_mesh%order(ic))
          ndof_vol = ctx_dg%n_dof_per_order (i_order)
          n=ndof_vol*ctx_dg%dim_sol
          allocate(sol_loc(n))
          sol_loc(1:n)= - matmul(                                         &
                     transpose(conjg(ctx_dg%hdg%Ainv(ic)%array(1:n,1:n))),&
                     cmplx(ctx_dg%src(i_src)%rcv(icell_src)%hdg_R(1:n),   &
                           kind=RKIND_MAT))
          !! update field
          igb1 = (i_src-1)*ctx_dg%n_dofloc_persol + &
                 ctx_dg%offsetdof_sol_loc(ic)  + 1
          igb2 = igb1 + ndof_vol - 1
          do isol=1,ctx_dg%dim_sol
            iloc1 = (isol-1)*ndof_vol + 1
            iloc2 = iloc1  + ndof_vol - 1
            field(igb1:igb2,isol) = field(igb1:igb2,isol) + sol_loc(iloc1:iloc2)
          end do
          deallocate(sol_loc)
        end if
      end do
      !$OMP END DO
    else !! in the case of traditional source
      !$OMP DO
      do icell_src=1,ctx_dg%src(i_src)%ncell_loc

        ! ---------------------------------------------------------
        !  only for VOLUMIC SOURCES 
        ! ---------------------------------------------------------
        ic = ctx_dg%src(i_src)%icell_loc(icell_src)
        if( ic > 0 .and. .not.(ctx_dg%src(i_src)%flag_on_bdry(icell_src))) then
          i_order  = ctx_dg%order_to_index  (ctx_mesh%order(ic))
          ndof_vol = ctx_dg%n_dof_per_order (i_order)
          n=ndof_vol*ctx_dg%dim_sol
          allocate(sol_loc(n))
          sol_loc     = 0.d0
          !! this is a + sign as we have 
          !!     A U + C R\Lambda = S
          !!  =>   U = A^(-1) S - C R\Lambda,
          !! and the second term with the minus sign has been treated above.
          sol_loc(1:n)= matmul(ctx_dg%hdg%Ainv(ic)%array(1:n,1:n),      &
                        cmplx(ctx_dg%src(i_src)%hdg_S(icell_src)%array(1:n),&
                              kind=RKIND_MAT))
          !! update field
          igb1 = (i_src-1)*ctx_dg%n_dofloc_persol + &
                 ctx_dg%offsetdof_sol_loc(ic)  + 1
          igb2 = igb1 + ndof_vol - 1
          do isol=1,ctx_dg%dim_sol
            iloc1 = (isol-1)*ndof_vol + 1
            iloc2 = iloc1  + ndof_vol - 1
            field(igb1:igb2,isol) = field(igb1:igb2,isol) + sol_loc(iloc1:iloc2)
          end do
          deallocate(sol_loc)
        end if

      end do
      !$OMP END DO
    end if
    !$OMP END PARALLEL
    !! -----------------------------------------------------------------

    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------
    return
  end subroutine hdg_create_fields
  
end module m_dg_lagrange_field_construct
