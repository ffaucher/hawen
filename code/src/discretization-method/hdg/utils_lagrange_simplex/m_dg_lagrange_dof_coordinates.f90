!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_dof_coordinates.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module is the API to compute the coordinates of the dof on the cell.
!
!------------------------------------------------------------------------------
module m_dg_lagrange_dof_coordinates

  use m_raise_error,                  only : t_error, raise_error
  use m_define_precision,             only : IKIND_MESH
  use m_ctx_domain,                   only : t_domain
  use m_dg_lagrange_simplex_dof,      only : lagrange_simplex_dof_coo

  implicit none
  private
  public :: discretization_dof_coo_loc_elem
  public :: discretization_dof_coo_ref_elem
  
  contains
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the d.o.f coordinates depending on the order of the 
  !> polynomials to be discretized.
  !
  !> @param[in]    ctx_mesh        : mesh context
  !> @param[in]    i_cell          : cell index
  !> @param[in]    order           : order of the polynomials
  !> @param[in]    ndof_vol        : number of dof for this order
  !> @param[out]   dof_coo         : output coordinates of all the dof
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine discretization_dof_coo_loc_elem(ctx_mesh,i_cell,order,     &
                                             ndof_vol,dof_coo,ctx_err)

    implicit none
    
    type(t_domain)           ,intent(in)    :: ctx_mesh    
    integer(kind=IKIND_MESH) ,intent(in)    :: i_cell
    integer                  ,intent(in)    :: order
    integer                  ,intent(in)    :: ndof_vol
    real(kind=8), allocatable,intent(inout) :: dof_coo (:,:)
    type(t_error)            ,intent(out)   :: ctx_err
    !!
    real(kind=8), allocatable :: node_coo(:,:)
    
    ctx_err%ierr  = 0

    !! extract the nodes of the current cell
    allocate(node_coo(ctx_mesh%dim_domain,ctx_mesh%n_node_per_cell))
    node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))

    !! for now it only works for simplexes .............................
    dof_coo = 0.d0
    if(ctx_mesh%dim_domain+1 == ctx_mesh%n_node_per_cell) then
      call lagrange_simplex_dof_coo(ctx_mesh%dim_domain,node_coo,order, &
                                    ndof_vol,dof_coo,ctx_err)
    else
      ctx_err%msg   = "** ERROR: the dof coordinates is only " //       &
                      "supported for simplex cells [dof_coordinates] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    deallocate(node_coo)
    return

  end subroutine discretization_dof_coo_loc_elem

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the d.o.f coordinates depending on the order of the 
  !> polynomials to be discretized for a reference element.
  !
  !> @param[in]    ctx_mesh        : mesh context
  !> @param[in]    i_cell          : cell index
  !> @param[in]    order           : order of the polynomials
  !> @param[in]    ndof_vol        : number of dof for this order
  !> @param[in]    dimselect       : indicates the coo. of which dim
  !>                               | for instance if spherical symmetry, 
  !>                               | the mesh may be 3D but we still 
  !>                               | want the 1D coordinates.
  !> @param[out]   dof_coo         : output coordinates of all the dof
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine discretization_dof_coo_ref_elem(ctx_mesh,order,ndof_vol,   &
                                             dimselect,dof_coo,ctx_err)

    implicit none
    
    type(t_domain)           ,intent(in)    :: ctx_mesh
    integer                  ,intent(in)    :: order
    integer                  ,intent(in)    :: ndof_vol
    integer                  ,intent(in)    :: dimselect
    real(kind=8), allocatable,intent(inout) :: dof_coo (:,:)
    type(t_error)            ,intent(out)   :: ctx_err
    !!
    real(kind=8), allocatable :: node_coo(:,:)
    
    ctx_err%ierr  = 0
          
    !! extract the nodes of the current cell, for the required dimension.
    allocate(node_coo(dimselect,ctx_mesh%n_node_per_cell))

    !! for now it only works for simplexes .............................
    dof_coo = 0.d0
    if(ctx_mesh%dim_domain+1 == ctx_mesh%n_node_per_cell) then
      !! ref coo depends on the dimension 

      !! ***************************************************************
      !! these are extracted from 
      !!           m_basis_function when we construct the polynomials.
      !! ***************************************************************
      select case(dimselect)
        case(1)
          node_coo(1,1)= 0
          node_coo(1,2)= 1
        case(2)
          node_coo(:,1)= [0, 0]
          node_coo(:,2)= [1, 0] 
          node_coo(:,3)= [0, 1]
        case(3)
          node_coo(:,1)= [0, 0, 0]
          node_coo(:,2)= [1, 0, 0] 
          node_coo(:,3)= [0, 1, 0]
          node_coo(:,4)= [0, 0, 1]

        case default
          ctx_err%msg   = "** ERROR: unsupported dimension [dof_coordinates] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      
      call lagrange_simplex_dof_coo(dimselect,node_coo,order,ndof_vol,  & 
                                    dof_coo,ctx_err)
    else
      ctx_err%msg   = "** ERROR: the dof coordinates is only " //       &
                      "supported for simplex cells [dof_coordinates] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    deallocate(node_coo)

    return

  end subroutine discretization_dof_coo_ref_elem

end module m_dg_lagrange_dof_coordinates
