!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_dg_lagrange_source_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize acquisition infos for DG discretization
!> for sources in the context of Lagrange Basis functions. We also init
!> the weight for the receivers.
!
!------------------------------------------------------------------------------
module m_dg_lagrange_src_rcv_api

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,                 only: raise_error, t_error
  use m_distribute_error,            only: distribute_error
  use m_define_precision,            only: IKIND_MESH,RKIND_POL
  use m_ctx_parallelism,             only: t_parallelism
  !> matrix and mesh domain types
  use m_ctx_acquisition,             only: t_acquisition, SRC_DIRAC,    &
                                           SRC_PW, SRC_DIRAC_DERIV,     &
                                           SRC_DIRAC_DIV,               &
                                           SRC_DIRAC_DERIVX,            &
                                           SRC_DIRAC_DERIVY,            &
                                           SRC_DIRAC_DERIVZ,            &
                                           SRC_ANALYTIC, SRC_GAUSSIAN
  use m_ctx_domain,                  only: t_domain
  use m_ctx_discretization,          only: t_discretization
  use m_dg_lagrange_source_dirac,    only: dg_source_init_dirac
  use m_dg_lagrange_source_planewave,only: dg_source_init_planewave,    &
                                           dg_source_init_analytic
  use m_dg_lagrange_source_gaussian, only: dg_source_init_gaussian
  use m_dg_lagrange_receiver_init,   only: dg_receiver_init
  
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: dg_src_rcv_init_lagrange

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> We generate DG infos for the list of sources:
  !> - the cell(s) which are involved
  !> - the weight for the integral
  !> for all sources, we extract the information on the receivers as well
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[in]    ctx_aq         : acquisition type
  !> @param[in]    ctx_grid       : mesh grid type
  !> @param[inout] ctx_dg         : type discretization DG
  !> @param[in]    nrhs           : current number of rhs
  !> @param[in]    i_src_first    : first source index in acquisition
  !> @param[in]    i_src_last     : last source index in acquisition
  !> @param[in]    tag_scale_rhs  : indicates if scaling the RHS
  !> @param[in]    rhs_format     : indicates the format dense or sparse
  !> @param[in]    freq           : frequency needed for planewave
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine dg_src_rcv_init_lagrange(ctx_paral,ctx_aq,ctx_grid,ctx_dg, &
                                      nrhs,i_src_first,i_src_last,      &
                                      tag_scale_rhs,rhs_format,freq,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_acquisition)              ,intent(in)   :: ctx_aq
    type(t_domain)                   ,intent(in)   :: ctx_grid
    type(t_discretization)           ,intent(inout):: ctx_dg
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: nrhs,i_src_first
    integer                          ,intent(in)   :: i_src_last
    integer                          ,intent(in)   :: tag_scale_rhs
    character(len=*)                 ,intent(inout):: rhs_format
    type(t_error)                    ,intent(inout):: ctx_err
    !! local
    integer     :: k
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    rhs_format    = ''

    !! allocate the context and fill depending on the type of sources
    allocate(ctx_dg%src(nrhs))
    ctx_dg%src(:)%ncell_loc=0

    !! -----------------------------------------------------------------
    !! save the source type, we assume they all have the same type,
    !! same with the components.
    ctx_dg%src(:)%src_type = ctx_aq%src_type
    ctx_dg%src(:)%ncomponent_src = ctx_aq%ncomponent_src
    do k=1,nrhs
      allocate(ctx_dg%src(k)%src_component(ctx_dg%src(k)%ncomponent_src))
      ctx_dg%src(k)%src_component(:) = ctx_aq%src_component(:)
    end do
    
   
    !! -----------------------------------------------------------------
    !! we first initialize the sources depending on the type of sources
    !! and of the dimension.
    !! -----------------------------------------------------------------
    select case(ctx_aq%src_type)

      !! ---------------------------------------------------------------
      !! point-source delta-Dirac 
      !!      It uses sparse rhs format, only one cell per point
      !! ---------------------------------------------------------------
      case(SRC_DIRAC,SRC_DIRAC_DERIV,SRC_DIRAC_DIV,SRC_DIRAC_DERIVX,    &
           SRC_DIRAC_DERIVY,SRC_DIRAC_DERIVZ)
        rhs_format = 'sparse'
        call dg_source_init_dirac(ctx_paral,ctx_aq,ctx_grid,ctx_dg,     &
                                  i_src_first,i_src_last,tag_scale_rhs, &
                                  ctx_err)
      !! ---------------------------------------------------------------
      !! planewave or analytic functions:
      !!      It uses sparse rhs format, and concerns the selected boundary
      !! ---------------------------------------------------------------
      case(SRC_PW)
        rhs_format = 'sparse'
        call dg_source_init_planewave(ctx_aq,ctx_grid,ctx_dg,freq,      &
                                      i_src_first,i_src_last,ctx_err)
      !! ---------------------------------------------------------------
      !! planewave or analytic functions:
      !!      It uses sparse rhs format, and concerns the selected boundary
      !! ---------------------------------------------------------------
      case(SRC_ANALYTIC)
        rhs_format = 'sparse'
        call dg_source_init_analytic(ctx_paral,ctx_aq,ctx_grid,         &
                                     ctx_dg,freq,i_src_first,           &
                                     i_src_last,ctx_err)
      !! ---------------------------------------------------------------
      !! Gaussian source function
      !!      It uses sparse rhs format, and concerns a group of cells
      !! ---------------------------------------------------------------
      case(SRC_GAUSSIAN)
        rhs_format = 'sparse'
        call dg_source_init_gaussian(ctx_paral,ctx_aq,ctx_grid,ctx_dg,  &
                                     i_src_first,i_src_last,            &
                                     tag_scale_rhs,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: this type of source is not "    //   &
                        "supported for hdg discretization, Dirac " //   &
                        "are supported though [dg_source_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)

    end select
    !! error distribution
    call distribute_error(ctx_err,ctx_paral%communicator)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! Now we initialize the rcv infos, each one is a point
    !! -----------------------------------------------------------------
    call dg_receiver_init(ctx_paral,ctx_aq,ctx_grid,ctx_dg,i_src_first, &
                          i_src_last,ctx_err)
    !! -----------------------------------------------------------------

    return
  end subroutine dg_src_rcv_init_lagrange

end module m_dg_lagrange_src_rcv_api
