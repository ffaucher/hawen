!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_process.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to process the modeling method, i.e., forward problem
!
!------------------------------------------------------------------------------
module m_process

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_ctx_parallelism,          only: t_parallelism
  use m_barrier,                  only: barrier
  use m_time,                     only: time
  !! read parameter files
  use m_read_parameters_init,     only: read_parameters_init
  !! printing
  use m_print_init,               only: print_init
  use m_print_modeling,           only: print_time_freq, print_time_mode
  !! acquisition
  use m_ctx_acquisition,          only: t_acquisition,acquisition_clean
  use m_acquisition_init,         only: acquisition_init
  use m_source_eval,              only: source_eval
  !! frequency
  use m_frequency_init,           only: frequency_init
  use m_print_freq,               only: print_frequency_welcome
  !! modes
  use m_mode_init,                only: mode_init
  use m_print_mode,               only: print_mode_welcome
  !! domain
  use m_domain_process,           only: domain_process
  use m_ctx_domain,               only: t_domain,domain_clean
  !! equation
  use m_ctx_equation,             only: t_equation,equation_clean
  use m_equation_init,            only: equation_init
  !! I/O
  use m_ctx_io,                   only: t_io, io_clean
  use m_io_init,                  only: io_init
  use m_wavefield_save,           only: wavefield_save
  use m_discretization_io_init,   only: discretization_io_init
  !! model parameters
  use m_ctx_model,                only: t_model, model_clean
  use m_model_init,               only: model_init
  use m_model_load,               only: model_load
  use m_model_save,               only: model_save
  !! discretization
  use m_wavelength_utils,         only: compute_wavelength_order
  use m_ctx_discretization,       only: t_discretization,               &
                                        discretization_clean_context,   &
                                        discretization_reset_context_io,&
                                        discretization_clean_context_io,&
                                        discretization_clean_context_src
  use m_discretization_init,      only: discretization_init
  !! linear algebra
  use m_ctx_matrix,               only: t_matrix,matrix_clean
  use m_ctx_rhs,                  only: t_rhs,rhs_clean,rhs_clean_allocation
  use m_rhs_init,                 only: rhs_init
  use m_matrix_init,              only: matrix_init
  use m_matrix_discretization,    only: matrix_discretization
  use m_rhs_create,               only: rhs_create,rhs_create_field
  use m_api_solver,               only: t_solver, solver_init_options,  &
                                        process_factorization,          &
                                        process_solve,solver_clean
  !! solution 
  use m_ctx_field,                only: t_field, field_clean
  use m_field_create,             only: field_create
  !! cross-correlation specifics
  use m_ctx_crosscorrelation,     only: t_crosscorrelation
  use m_crosscorrelation_init,    only: crosscorrelation_init
  use m_crosscorrelation_save,    only: crosscorrelation_save_covariance_src
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: process

  contains
 
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> process modeling depending on the PDE compiled and input parameter file
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    parameter_file : parameter file where infos are
  !> @param[inout] ctx_err        : context error
  !
  !----------------------------------------------------------------------------
  subroutine process(ctx_paral,parameter_file,ctx_err)

    implicit none

    type(t_parallelism)      ,intent(in)   :: ctx_paral
    character(len=*)         ,intent(in)   :: parameter_file
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                :: dm=0
    logical                :: flag_inversion=.false.
    !! local: acquisition
    type(t_acquisition)    :: ctx_aq
    !! local: domain
    type(t_domain)         :: ctx_domain
    !! local: equation
    type(t_equation)       :: ctx_equation
    !! local: cross-correlation
    type(t_crosscorrelation) :: ctx_crosscorr
    !! local: frequency
    real,allocatable       :: frequency(:,:)
    integer                :: nfreq
    !! local: modes
    integer,allocatable    :: mode(:)
    integer                :: nmode
    !! local: io
    type(t_io)             :: ctx_io
    !! local: model parameters
    type(t_model)          :: ctx_model
    !! local: discretization
    type(t_discretization) :: ctx_discretization
    !! local: matrix & solver
    type(t_matrix)         :: ctx_matrix_forward
    type(t_solver)         :: ctx_solver
    type(t_rhs)            :: ctx_rhs_forward
    !! local: solution wavefields
    type(t_field)          :: ctx_field_dirac
    type(t_field)          :: ctx_field_crosscorr
    !! loop variables  
    integer                :: ifreq,irhs_group,imode
    integer                :: index_src_gb
    !! timers 
    real(kind=8)           :: tstartgb_freq,tstartgb_mode,tstart_mode
    real(kind=8)           :: timegb,timeloc,tstart_freq
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------                                 
    ctx_err%ierr  = 0
    call print_init(ctx_paral,ctx_err)

    !! we get the problem dimensions -----------------------------------
    call read_parameters_init(ctx_paral,parameter_file,dm,ctx_err)

    !! init acquisition ------------------------------------------------
    call acquisition_init(ctx_paral,ctx_aq,parameter_file,ctx_err)

    !! equation infos --------------------------------------------------
    call equation_init(ctx_paral,ctx_equation,parameter_file,dm,ctx_err)

    !! read frequency --------------------------------------------------
    call frequency_init(ctx_paral,frequency,nfreq,parameter_file,       &
                        ctx_equation%flag_freq,ctx_err)
    !! read modes ------------------------------------------------------
    call mode_init(ctx_paral,mode,nmode,parameter_file,                 &
                   ctx_equation%flag_mode,ctx_err)
    !! -----------------------------------------------------------------
    !! initialize the values with very first inputs
    !! there is a minus here, because we have 
    !!     \sigma =  \omega + i \laplace
    !! => i\sigma = i\omega -   \laplace
    !! -----------------------------------------------------------------
    ctx_equation%current_angular_frequency = dcmplx(-frequency(1,2),    &
                              dble(2.d0*4.d0*datan(1.d0)*frequency(1,1)))
    ctx_equation%current_mode              = mode(1)

    !! init domain -----------------------------------------------------
    call domain_process(ctx_paral,dm,ctx_equation%n_media_max,          &
                        parameter_file,ctx_domain,ctx_err)

    !! model informations ----------------------------------------------
    !! we read first so we can compute the wavelength and potentially
    !! use an automatic order right after.
    call model_init(ctx_model,parameter_file,dm,ctx_domain%n_cell_loc,  &
                    ctx_domain%n_cell_gb,ctx_equation%flag_viscous,     &
                    ctx_equation%viscous_model,ctx_err)
    call model_load(ctx_paral,ctx_domain,ctx_model,parameter_file,ctx_err)

    !! initialize the cross-correlation information --------------------
    call crosscorrelation_init(ctx_paral,ctx_domain,parameter_file,     &
                               ctx_crosscorr,ctx_err)

    !! in the case where the order is automatic, use wavelength:
    !! it is computed with the first frequency, and will be updated
    if(trim(adjustl(ctx_domain%order_method)) .eq. 'automatic') then
      call compute_wavelength_order(ctx_paral,ctx_model,ctx_domain,     &
                                    ctx_equation,ctx_err)
    end if

    !! initialize discretization information:
    !!   - in case we are DG, we prepare the geometrical infos.
    !!
    call discretization_init(ctx_paral,ctx_discretization,ctx_domain,   &
                             ctx_equation%dim_var,                      &
                             ctx_equation%dim_solution,                 &
                             ctx_equation%name_var,                     &
                             ctx_equation%name_sol,                     &
                             flag_inversion,                            &
                             ctx_err)

    !! intialize io infos ----------------------------------------------
    call io_init(ctx_paral,ctx_equation,ctx_io,parameter_file,ctx_err)
    !We need to check:
    !!  (o) save_unstructured inconsitant for cartesian 
    !!  (o) save_structured with dx,dy,dz needs specific cartesian grid 
    !!      if we have a mesh
    call discretization_io_init(ctx_paral,ctx_discretization,ctx_domain,&
                                ctx_io%dx,ctx_io%dy,ctx_io%dz,          &
                                ctx_io%save_structured,                 &
                                ctx_io%save_unstructured,ctx_err)

    !! save model 
    call model_save(ctx_paral,ctx_domain,ctx_discretization,ctx_model,  &
                    ctx_io,ctx_model%n_model,ctx_model%name_model,0,    &
                    ctx_equation%current_angular_frequency,ctx_err)
    !! save the cross-correlation source covariance term
    call crosscorrelation_save_covariance_src(ctx_paral,ctx_domain,     &
                               ctx_discretization,                      &
                               ctx_crosscorr%src_crosscovariance_real,  &
                               ctx_io,ctx_crosscorr%format_disc,0,      &
                               ctx_equation%current_angular_frequency,  &
                               ctx_err)
    call crosscorrelation_save_covariance_src(ctx_paral,ctx_domain,     &
                               ctx_discretization,                      &
                               ctx_crosscorr%src_crosscovariance_imag,  &
                               ctx_io,ctx_crosscorr%format_disc,0,      &
                               ctx_equation%current_angular_frequency,  &
                               ctx_err)


    !! initialize main solver options ----------------------------------
    call solver_init_options(ctx_paral,ctx_equation%symmetric_matrix,   &
                             ctx_solver,parameter_file,ctx_err)

    !! initialize rhs group infos --------------------------------------
    call rhs_init(ctx_aq,ctx_rhs_forward,parameter_file,                &
                  ctx_equation%dim_solution,ctx_equation%dim_var,       &
                  ctx_equation%name_sol,ctx_equation%name_var,ctx_err)

    !! -----------------------------------------------------------------
    !!  loop over modes
    !! some equations, such as Helmholtz, do not have modes.
    !! -----------------------------------------------------------------
    call time(tstartgb_mode)
    do imode=1,nmode
      call time(tstart_mode)
      !! only print when there actually is a mode.
      call print_mode_welcome(ctx_paral,mode,nmode,imode,               &
                              ctx_equation%flag_mode,ctx_err)
      !! update value in the equation context
      ctx_equation%current_mode = mode(imode)
      !! ---------------------------------------------------------------
      !!  loop over frequency in the acquisition
      !! ---------------------------------------------------------------
      call time(tstartgb_freq)
      do ifreq=1,nfreq
        call time(tstart_freq)
        call print_frequency_welcome(ctx_paral,frequency,nfreq,ifreq,   &
                                     ctx_equation%flag_freq,ctx_err)        
        !! update value in the equation context
        !! there is a minus here, because we have 
        !!     \sigma =  \omega + i \laplace
        !! => i\sigma = i\omega -   \laplace
        !! -----------------------------------------------------------------
        ctx_equation%current_angular_frequency = dcmplx(-frequency(ifreq,2),&
                             dble(2.d0*4.d0*datan(1.d0)*frequency(ifreq,1)))
        !! ---------------------------------------------------------------
        !! In case of automatic order, we clean and re-compute reference
        !! matrices
        !! ---------------------------------------------------------------
        if(ifreq.ne.1 .and. trim(adjustl(ctx_domain%order_method)) .eq. &
           'automatic') then
          !! clean discretization and reset analysis
          ctx_solver%flag_analysis=.false.
          call discretization_clean_context(ctx_discretization)
          call discretization_reset_context_io(ctx_discretization)

          call compute_wavelength_order(ctx_paral,ctx_model,ctx_domain, &
                                        ctx_equation,ctx_err)
          call discretization_init(ctx_paral,ctx_discretization,        &
                                   ctx_domain,ctx_equation%dim_var,     &
                                   ctx_equation%dim_solution,           &
                                   ctx_equation%name_var,               &
                                   ctx_equation%name_sol,               &
                                   flag_inversion,ctx_err)
          !! this is to adjust the dof weight for the cartesian map
          !! from the Discontinuous Galerkin context
          call discretization_io_init(ctx_paral,ctx_discretization,     &
                                      ctx_domain,ctx_io%dx,             &
                                      ctx_io%dy,ctx_io%dz,              &
                                      ctx_io%save_structured,           &
                                      ctx_io%save_unstructured,ctx_err)
        end if
        !! ---------------------------------------------------------------
  
        !! compute source
        call source_eval(ctx_aq,ctx_equation%current_angular_frequency,ctx_err)
  
        !! ---------------------------------------------------------------
        !! we estimate the number of nnz for the matrix and init ccontext
        !! the size may change with freq if pml adjusted to the wavelength
        call matrix_init(ctx_paral,ctx_equation,ctx_domain,ctx_model,   &
                         ctx_discretization,ctx_matrix_forward,         &
                         ctx_solver%flag_block,ctx_err)
        !! matrix discretization and factorization ---------------------
        call matrix_discretization(ctx_paral,ctx_equation,ctx_domain,   &
                                   ctx_model,ctx_discretization,        &
                                   ctx_matrix_forward,flag_inversion,ctx_err)
        call process_factorization(ctx_paral,ctx_solver,                &
                                   ctx_matrix_forward,ctx_err)
        
        !! -------------------------------------------------------------
        !!  loop over group of RHS
        !! -------------------------------------------------------------
        index_src_gb=ctx_aq%isrc_gb_start !! global source index
        do irhs_group=1,ctx_rhs_forward%n_group
          ctx_rhs_forward%i_group = irhs_group
 
          !! the creation may need the pml size and HDG matrices -------
          call rhs_create(ctx_paral,ctx_aq,ctx_domain,ctx_discretization,&
                          ctx_rhs_forward,                               &
                          ctx_equation%current_angular_frequency,        &
                          ctx_equation%tag_scale_rhs,ctx_err)
  
          !! solve resulting problem
          call process_solve(ctx_paral,ctx_solver,ctx_rhs_forward,      &
                             .false.,ctx_err)
  
          !! -----------------------------------------------------------
          !! the solution is contained in the ctx_solver
          !! distribute local informations here
          !! for HDG we also need the post-processing to retrieve the 
          !! wavefields from the Lagrange multipliers
          !! -----------------------------------------------------------
          call field_create(ctx_paral,ctx_domain,ctx_discretization,    &
                            ctx_field_dirac,                            &
                            ctx_rhs_forward%n_rhs_in_group(irhs_group), &
                            ctx_equation%dim_solution,                  &
                            ctx_equation%name_sol,                      &
                            ctx_solver%solution_global,.false.,ctx_err)

          !! -----------------------------------------------------------
          !! create the RHS for the cross-correlation problem
          !! we update the existing one.
          !! -----------------------------------------------------------
          !! clean rhs and source arrays: only allocation
          call rhs_clean_allocation            (ctx_rhs_forward)
          !! flag true to keep receivers informations. in that way the array
          !! ctx_discretization%src is still allocated
          call discretization_clean_context_src(ctx_discretization,.true.)
          !! create the dense RHS composed of the field and the source
          !! cross-covariance, false flag is for forward problem and not adjoint.
          call rhs_create_field(ctx_paral,ctx_domain,ctx_discretization,&
                                ctx_rhs_forward,                        &
                                ctx_crosscorr%src_crosscovariance_real, &
                                ctx_crosscorr%src_crosscovariance_imag, &
                                ctx_field_dirac%field,                  &
                                ctx_crosscorr%format_disc,.false.,      &
                                ctx_equation%current_angular_frequency, &
                                ctx_equation%tag_scale_rhs,ctx_err)
                              

          !! solve resulting problem
          call process_solve(ctx_paral,ctx_solver,ctx_rhs_forward,      &
                             .false.,ctx_err)
          call field_create(ctx_paral,ctx_domain,ctx_discretization,    &
                            ctx_field_crosscorr,                        &
                            ctx_rhs_forward%n_rhs_in_group(irhs_group), &
                            ctx_equation%dim_solution,                  &
                            ctx_equation%name_sol,                      &
                            ctx_solver%solution_global,.false.,ctx_err)

          !! -----------------------------------------------------------
          !! Save wavefield and receivers infos
          !! cartesian and/or pvtu
          !! -----------------------------------------------------------
          call wavefield_save(ctx_paral,ctx_aq,ctx_equation,ctx_domain, &
                              ctx_discretization,ctx_io,                &
                              ctx_field_crosscorr,                      &
                              index_src_gb,ctx_aq%isrc_step,ctx_err)
          index_src_gb = index_src_gb + ctx_aq%isrc_step *              &
                         ctx_rhs_forward%n_rhs_in_group(irhs_group)
          !! clean group of rhs infos and current discretization infos 
          !! regarding the acquisition.
          call discretization_clean_context_src(ctx_discretization)
          call field_clean                     (ctx_field_dirac)
          call field_clean                     (ctx_field_crosscorr)
          call rhs_clean_allocation            (ctx_rhs_forward)
        end do !! end group of rhs

        !! clean matrix infos
        call matrix_clean(ctx_matrix_forward)
        
        !! print the current time after this frequency if no mode
        if(.not. ctx_equation%flag_mode) then
          call time(timeloc)
          timegb  = timeloc - tstartgb_freq
          timeloc = timeloc - tstart_freq
          call print_time_freq(ctx_paral,ctx_equation%flag_freq,nfreq,  &
                               ifreq,timeloc,timegb,ctx_err)
        end if
        !! -------------------------------------------------------------
      !! ---------------------------------------------------------------
      end do !! end frequency
      !! ---------------------------------------------------------------
      !! print the current time after this frequency if no mode
      if(ctx_equation%flag_mode) then
        call time(timeloc)
        timegb  = timeloc - tstartgb_mode
        timeloc = timeloc - tstart_mode
        call print_time_mode(ctx_paral,ctx_equation%flag_freq,          &
                             ctx_equation%flag_mode,nfreq,nmode,imode,  &
                             timeloc,timegb,ctx_err)
      end if
    !! -----------------------------------------------------------------    
    end do   !! end mode
    !! -----------------------------------------------------------------
    
    !! clean remaining infos
    call solver_clean                   (ctx_solver,ctx_err)
    call model_clean                    (ctx_model)
    call rhs_clean                      (ctx_rhs_forward,ctx_err)
    call domain_clean                   (ctx_domain)
    call equation_clean                 (ctx_equation)
    call discretization_clean_context   (ctx_discretization)
    call discretization_clean_context_io(ctx_discretization)
    call acquisition_clean              (ctx_aq)
    call io_clean                       (ctx_io)
    if(allocated(frequency)) deallocate(frequency)
    if(allocated(mode))      deallocate(mode)
    !! mpi_barrier waits for every processors
    call barrier(ctx_paral%communicator,ctx_err)

    return
  end subroutine process

end module m_process
