!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_process.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to process the inversion scheme, i.e., fwi
!
!------------------------------------------------------------------------------
module m_process

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_ctx_parallelism,          only: t_parallelism
  use m_barrier,                  only: barrier
  use m_time,                     only: time
  !! read parameter files
  use m_read_parameters_init,     only: read_parameters_init
  !! printing
  use m_print_init,               only: print_init
  !! acquisition
  use m_ctx_acquisition,          only: t_acquisition,acquisition_clean
  use m_acquisition_init,         only: acquisition_init
  use m_source_eval,              only: source_eval
  !! frequency
  use m_frequency_init,           only: frequency_init_with_group
  use m_print_freq,               only: print_frequency_welcome
  !! modes
  use m_mode_init,                only: mode_init_with_group
  use m_print_mode,               only: print_mode_welcome
  !! domain
  use m_domain_process,           only: domain_process
  use m_ctx_domain,               only: t_domain,domain_clean
  !! equation
  use m_ctx_equation,             only: t_equation,equation_clean
  use m_equation_init,            only: equation_init
  !! I/O
  use m_ctx_io,                   only: t_io, io_clean
  use m_io_init,                  only: io_init
  use m_io_delete,                only: io_delete
  use m_discretization_io_init,   only: discretization_io_init
  !! model parameters
  use m_ctx_model,                only: t_model, model_clean
  use m_model_init,               only: model_init
  use m_model_load,               only: model_load
  use m_model_save,               only: model_save
  !! discretization
  use m_wavelength_utils,         only: compute_wavelength_order
  use m_ctx_discretization,       only: t_discretization,               &
                                        discretization_clean_context,   &
                                        discretization_reset_context_io,&
                                        discretization_clean_context_io,&
                                        discretization_clean_context_src
  use m_discretization_init,      only: discretization_init
  !! linear algebra
  use m_ctx_matrix,               only: t_matrix,matrix_clean
  use m_ctx_rhs,                  only: t_rhs,rhs_clean,rhs_clean_allocation
  use m_rhs_init,                 only: rhs_init,rhs_init_backward
  use m_matrix_init,              only: matrix_init
  use m_matrix_discretization,    only: matrix_discretization
  use m_rhs_create,               only: rhs_create
  use m_api_solver,               only: t_solver, solver_init_options,  &
                                        process_factorization,          &
                                        process_solve,solver_clean
  !! solution
  use m_ctx_field,                only: t_field, field_clean
  use m_field_create,             only: field_create
  !! -------------------------------------------------------------------
  !! inversion context
  use m_ctx_misfit_function,      only: t_misfit,misfit_clean,misfit_reset
  use m_ctx_gradient,             only: t_gradient,gradient_clean,gradient_reset
  use m_ctx_search_dir,           only: t_search_dir,search_dir_clean,  &
                                        search_dir_reset
  use m_ctx_linesearch,           only: t_linesearch,linesearch_clean,  &
                                        linesearch_reset
  use m_ctx_regularization,       only: t_regularization,               &
                                        regularization_clean,           &
                                        regularization_reset
  !! inversion init context
  use m_misfit_function_init,     only: misfit_init
  use m_gradient_init,            only: gradient_init
  use m_search_dir_init,          only: search_dir_init
  use m_linesearch_init,          only: linesearch_init
  use m_regularization_init,      only: regularization_init
  !! printing system
  use m_print_fwi,                only: print_fwi_welcome_iter,         &
                                        print_fwi_outcome_iter,         &
                                        print_fwi_iter_time,            &
                                        print_fwi_iter_mem
  !! inversion compute and save 
  use m_misfit_compute,           only: misfit_compute_and_backrhs
  use m_gradient_compute,         only: gradient_compute
  use m_misfit_save,              only: misfit_save
  use m_gradient_save,            only: gradient_save
  use m_regularization_compute,   only: regularization_compute
  use m_search_dir_compute,       only: search_dir_compute,search_dir_scale
  use m_search_dir_save,          only: search_dir_save
  use m_linesearch_compute,       only: linesearch_compute
  use m_linesearch_save,          only: linesearch_save
  use m_allmodel_update,          only: allmodel_update
  use m_ctx_source_inv,           only: t_source_inversion,             &
                                        source_inversion_reset,         &
                                        source_inversion_set,           &
                                        source_inversion_clean,         &
                                        source_inversion_update
  use m_source_inv_init,          only: source_inv_init
  use m_source_save,              only: source_save
  !! -------------------------------------------------------------------
!use m_representation_pconstant
  implicit none

  private
  public :: process

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> main algorithm to solve the inverse problem
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    parameter_file : parameter file where infos are
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine process(ctx_paral,parameter_file,ctx_err)

    implicit none 

    type(t_parallelism)      ,intent(in)   :: ctx_paral
    character(len=*)         ,intent(in)   :: parameter_file
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                 :: dm=0
    logical                 :: flag_inversion=.true.
    !! local: acquisition
    type(t_acquisition)     :: ctx_aq
    !! local: domain
    type(t_domain)          :: ctx_domain
    !! local: equation
    type(t_equation)        :: ctx_equation
    !! local: frequency
    real,allocatable        :: frequency(:,:)
    integer                 :: nfreq,nfreq_per_group,ifreq_end,nfreq_loc
    integer                 :: i_freq_group,n_freq_group,ifreq_start
    !! local: modes 
    integer,allocatable     :: mode(:)
    integer                 :: nmode,nmode_per_group,imode_end,nmode_loc
    integer                 :: i_mode_group,n_mode_group,imode_start
    !! global count mode + freq
    integer                 :: i_fm
    !! local: for all subgroups modexfreq
    integer                 :: n_subgroup,i_group,n_freqmode
    !! local: io
    type(t_io)              :: ctx_io
    !! local: model parameters
    type(t_model)           :: ctx_model
    !! local: discretization
    type(t_discretization)  :: ctx_discretization
    !! local: matrix & solver
    type(t_matrix)          :: ctx_matrix_forward
    type(t_solver)          :: ctx_solver
    type(t_rhs)             :: ctx_rhs_forward,ctx_rhs_backward
    !! local: solution wavefields
    type(t_field)           :: ctx_field,ctx_field_backward
    !! loop variables
    integer                 :: ifreq,imode,irhs_group,counter_iter
    !! inversion
    integer                 :: i_iter,verb_lvl
    logical                 :: flag_verb,flag_stagnation
    type(t_misfit)          :: ctx_misfit
    type(t_gradient)        :: ctx_grad
    type(t_search_dir)      :: ctx_search
    type(t_linesearch)      :: ctx_ls
    type(t_source_inversion):: ctx_src_inv
    type(t_regularization)  :: ctx_regularization
    real(kind=8),allocatable:: misfit_evolve(:)
    real(kind=8)            :: time0,time1,time_grad,time_field,time_solve
    real(kind=8)            :: timegb0,timegb1,time_rhs,improvement
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0
    call print_init(ctx_paral,ctx_err)

    !! we get the problem dimensions -----------------------------------
    call read_parameters_init(ctx_paral,parameter_file,dm,ctx_err)

    !! init acquisition ------------------------------------------------
    call acquisition_init(ctx_paral,ctx_aq,parameter_file,ctx_err)

    !! equation infos --------------------------------------------------
    call equation_init(ctx_paral,ctx_equation,parameter_file,dm,ctx_err)

    !! read frequency --------------------------------------------------
    call frequency_init_with_group(ctx_paral,frequency,nfreq,           &
                                   nfreq_per_group,parameter_file,      &
                                   ctx_equation%flag_freq,ctx_err)
    !! count the number of frequency group -----------------------------
    n_freqmode = 1
    if(ctx_equation%flag_freq) then
      n_freq_group = ceiling(real(nfreq)/real(nfreq_per_group))
      n_freqmode   = nfreq_per_group
    else
      n_freq_group = 1
    end if

    !! read modes ------------------------------------------------------
    call mode_init_with_group(ctx_paral,mode,nmode,nmode_per_group,     &
                              parameter_file,ctx_equation%flag_mode,ctx_err)
    !! count the number of mode group ----------------------------------
    if(ctx_equation%flag_mode) then
      n_mode_group = ceiling(real(nmode)/real(nmode_per_group))
      n_freqmode   = n_freqmode * nmode_per_group
    else
      n_mode_group = 1
    end if
    !! compute the number of groups
    n_subgroup = n_mode_group*n_freq_group

    !! -----------------------------------------------------------------
    !! initialize the values with very first inputs
    !! there is a minus here, because we have 
    !!     \sigma =  \omega + i \laplace
    !! => i\sigma = i\omega -   \laplace
    !! -----------------------------------------------------------------
    ctx_equation%current_angular_frequency = dcmplx(-frequency(1,2),    &
                              dble(2.d0*4.d0*datan(1.d0)*frequency(1,1)))
    ctx_equation%current_mode = mode(1)


    !! init domain -----------------------------------------------------
    call domain_process(ctx_paral,dm,ctx_equation%n_media_max,          &
                        parameter_file,ctx_domain,ctx_err)

    !! model informations ----------------------------------------------
    !! we read first so we can compute the wavelength and potentially
    !! use an automatic order right after.
    call model_init(ctx_model,parameter_file,dm,ctx_domain%n_cell_loc,  &
                    ctx_domain%n_cell_gb,ctx_equation%flag_viscous,     &
                    ctx_equation%viscous_model,ctx_err)
    call model_load(ctx_paral,ctx_domain,ctx_model,parameter_file,ctx_err)

    !! in the case where the order is automatic, use wavelength
    if(trim(adjustl(ctx_domain%order_method)) .eq. 'automatic') then
      !! use very first frequency and mode, updated in the loop
      call compute_wavelength_order(ctx_paral,ctx_model,ctx_domain,     &
                                    ctx_equation,ctx_err)
    end if

    !! initialize discretization information:
    !!   - in case we are DG, we prepare the reference matrices.
    !!
    call discretization_init(ctx_paral,ctx_discretization,ctx_domain,   &
                             ctx_equation%dim_var,                      &
                             ctx_equation%dim_solution,                 &
                             ctx_equation%name_var,                     &
                             ctx_equation%name_sol,                     &
                             flag_inversion,ctx_err)

    !! intialize io infos ----------------------------------------------
    call io_init(ctx_paral,ctx_equation,ctx_io,parameter_file,ctx_err)
    !We need to check:
    !!  (o) save_unstructured inconsitant for cartesian 
    !!  (o) save_structured with dx,dy,dz needs specific cartesian grid 
    !!      if we have a mesh
    call discretization_io_init(ctx_paral,ctx_discretization,ctx_domain,&
                                ctx_io%dx,ctx_io%dy,ctx_io%dz,          &
                                ctx_io%save_structured,                 &
                                ctx_io%save_unstructured,ctx_err)

    !! save initial reference models
    call model_save(ctx_paral,ctx_equation,ctx_domain,                  &
                    ctx_discretization,ctx_model,ctx_io,                &
                    ctx_model%n_model,ctx_model%name_model,0,           &
                    ctx_equation%current_angular_frequency,ctx_err)
    !! initialize main solver options ----------------------------------
    call solver_init_options(ctx_paral,ctx_equation%symmetric_matrix,   &
                             ctx_solver,parameter_file,ctx_err)

    !! initialize rhs group infos --------------------------------------
    !! forward and backward
    call rhs_init(ctx_aq,ctx_rhs_forward,parameter_file,                &
                  ctx_equation%dim_solution,ctx_equation%dim_var,       &
                  ctx_equation%name_sol,ctx_equation%name_var,ctx_err)
    call rhs_init_backward(ctx_rhs_backward,ctx_rhs_forward%n_group,    &
                           ctx_rhs_forward%n_rhs_in_group,              &
                           ctx_equation%dim_solution,ctx_err)

    !! initialize FWI information --------------------------------------
    !! a) misfit functional
    call misfit_init  (ctx_paral,ctx_misfit,parameter_file,             &
                       ctx_equation%dim_solution,ctx_equation%name_sol, &
                       ctx_err)
    allocate(misfit_evolve(ctx_misfit%niter))
    !! b) gradient arrays
    call gradient_init(ctx_paral,ctx_model,ctx_grad,parameter_file,ctx_err)
    !! c) search direction
    call search_dir_init(ctx_paral,ctx_search,ctx_grad,parameter_file,  &
                         ctx_io%flag_bin_search,ctx_io%flag_bin_grad,   &
                         ctx_io%flag_bin_model,ctx_err)
    !! d) regularization 
    call regularization_init(ctx_paral,ctx_regularization,ctx_grad,     &
                             parameter_file,ctx_err)
    !! e) line search formula
    call linesearch_init(ctx_paral,ctx_ls,parameter_file,               &
                         ctx_grad%n_grad,n_subgroup,                    &
                         ctx_search%param_name,ctx_err)
    !! f) source inversion 
    call source_inv_init(ctx_paral,ctx_aq,ctx_src_inv,parameter_file,   &
                         n_freqmode,ctx_err)

    !! save initial models using the gradient parametrization
    call model_save(ctx_paral,ctx_equation,ctx_domain,                  &
                    ctx_discretization,ctx_model,ctx_io,ctx_grad%n_grad,&
                    ctx_grad%param_name,0,                              &
                    ctx_equation%current_angular_frequency,ctx_err,     &
                    o_flag_param_grad=.true.)
     
    !! -----------------------------------------------------------------
    !! loop over all mode and frequency groups
    !! -----------------------------------------------------------------
    counter_iter=0
    do i_group = 1, n_subgroup

      !! find the current groups for the frequency and modes, careful if
      !! one or the other is not in the equation.
      if(ctx_equation%flag_freq) then
        i_mode_group = floor(dble(i_group-1)/dble(nfreq)) + 1
        i_freq_group = i_group - (i_mode_group-1)*nfreq
      else !! no frequency
        i_mode_group = i_group
        i_freq_group = 0
      end if
      if(.not. ctx_equation%flag_mode) then
        i_mode_group = 0
        i_freq_group = i_group
      end if

      !! extract the modes/freq that are in this group, if any.
      imode_start = 1 ; ifreq_start = 1
      imode_end   = 1 ; ifreq_end   = 1
      nmode_loc   = 1 ; nfreq_loc   = 1
      if(ctx_equation%flag_mode) then
        imode_start = (i_mode_group-1) * nmode_per_group + 1
        imode_end   = min(nmode,i_mode_group*nmode_per_group)
        nmode_loc   = imode_end-imode_start+1
      end if
      if(ctx_equation%flag_freq) then
        ifreq_start = (i_freq_group-1) * nfreq_per_group + 1
        ifreq_end   = min(nfreq,i_freq_group*nfreq_per_group)
        nfreq_loc   = ifreq_end-ifreq_start+1
      end if

      !! adjust the numer of freq x mode in this group
      n_freqmode = nmode_loc*nfreq_loc

      !! set initial value for this group
      call linesearch_reset      (ctx_ls,i_group)
      call source_inversion_reset(ctx_src_inv,n_freqmode)
      misfit_evolve = 0.0

      !! ---------------------------------------------------------------
      !! loop over minimization iterations
      ! ----------------------------------------------------------------
      do i_iter = 1,ctx_misfit%niter

        !! intial information >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        call time(timegb0)
        counter_iter = counter_iter + 1
        call print_fwi_welcome_iter(ctx_paral,i_group,n_subgroup,       &
                                    nfreq_loc,nmode_loc,                &
                                    frequency(ifreq_start:ifreq_end,:), &
                                    mode     (imode_start:imode_end),   &
                                    i_iter,ctx_misfit%niter,            &
                                    ctx_equation%flag_freq,             &
                                    ctx_equation%flag_mode,counter_iter)
        
        !! level of verbose for screen, intialize time 
        ctx_field_backward%time_distribute=0.
        ctx_field%time_distribute         =0.
        time_grad = 0.0
        time_rhs  = 0.0
        time_field= 0.0
        time_solve= 0.0
        if(i_iter == 1) then
          verb_lvl = 1 ; flag_verb=.true.
        else 
          verb_lvl = 0 ; flag_verb=.false.
        endif
        
        !! initialize FWI arrays
        if(ctx_ls%flag_reset_iter) call linesearch_reset(ctx_ls,i_group)
        call misfit_reset        (ctx_misfit)
        call gradient_reset      (ctx_grad)
        call search_dir_reset    (ctx_search)
        call regularization_reset(ctx_regularization)
        ! --------------------------------------------------------------

        !! -------------------------------------------------------------
        !! loop over all frequencies and modes in current group
        !! the outer loop if for the modes, the inner is for the
        !! frequency, this is an arbitrary choice.
        ! --------------------------------------------------------------
        do imode=1,nmode_loc
          !! update value in the equation context
          ctx_equation%current_mode = mode(imode)
  
          do ifreq=1,nfreq_loc

            !! current parameter index
            i_fm = (imode-1) * nfreq_loc + ifreq

            !! update value in the equation context
            !! there is a minus here, because we have 
            !!     \sigma =  \omega + i \laplace
            !! => i\sigma = i\omega -   \laplace
            !! ---------------------------------------------------------
            ctx_equation%current_angular_frequency =                     &
                          dcmplx(-dble(frequency(ifreq_start+ifreq-1,2)),&
            dble(2.d0*4.d0*datan(1.d0)*frequency(ifreq_start+ifreq-1,1)))
          

            !! ---------------------------------------------------------
            !! In case of automatic order, we clean and re-compute 
            !! reference matrices for this frequency & mode.
            !! except for the very first iteration
            !! ---------------------------------------------------------
            if(trim(adjustl(ctx_domain%order_method)) .eq. 'automatic') then
              !!  -> only if i_iter=1 in non multi-frequency
              !!  -> every time if multi-frequency, except very first iter
              if((i_group==1 .and. i_iter==1 .and. ifreq==1 .and. imode==1) &
                 .or. (i_iter>1 .and. nfreq_loc==1 .and. nmode_loc==1)) then
                !! already done earlier
              else
                !! clean discretization and reset analysis
                ctx_solver%flag_analysis=.false.
                call discretization_clean_context   (ctx_discretization)
                call discretization_reset_context_io(ctx_discretization)
                
                call compute_wavelength_order(ctx_paral,ctx_model,        &
                                              ctx_domain,ctx_equation,ctx_err)
 
                call discretization_init(ctx_paral,ctx_discretization,    &
                                         ctx_domain,ctx_equation%dim_var, &
                                         ctx_equation%dim_solution,       &
                                         ctx_equation%name_var,           &
                                         ctx_equation%name_sol,           &
                                         flag_inversion,ctx_err,          &
                                         o_verb_lvl=verb_lvl)
                !! this is to adjust the dof weight for the cartesian map
                !! from the Discontinuous Galerkin context
                call discretization_io_init(ctx_paral,ctx_discretization, &
                                            ctx_domain,ctx_io%dx,         &
                                            ctx_io%dy,ctx_io%dz,          &
                                            ctx_io%save_structured,       &
                                            ctx_io%save_unstructured,     &
                                            ctx_err,o_verb_lvl=verb_lvl)
    
              end if
            end if
            !! ---------------------------------------------------------

            !! compute source
            call source_eval(ctx_aq,                                    &
                             ctx_equation%current_angular_frequency,ctx_err)
            !! in case we are in the first iteration, we need to save the 
            !! source value, otherwize, we replace with the reconstructed
            !! current value
            if(i_iter == 1) then
              call source_inversion_set(ctx_src_inv,i_fm,               &
                                        ctx_aq%src_info%current_val)
            else
              ctx_aq%src_info%current_val = ctx_src_inv%src_val(1,i_fm)
            end if

            !! ---------------------------------------------------------
            !! we estimate the number of nnz for the matrix and init ccontext
            !! the size may change with freq/mode, e.g., if pml adjusted to 
            !! the wavelength
            call matrix_init(ctx_paral,ctx_equation,ctx_domain,ctx_model, &
                             ctx_discretization,ctx_matrix_forward,       &
                             ctx_solver%flag_block,ctx_err,o_verb_lvl=verb_lvl)
            !! matrix discretization and factorization -------------------
            call matrix_discretization(ctx_paral,ctx_equation,ctx_domain, &
                                       ctx_model,ctx_discretization,      &
                                       ctx_matrix_forward,flag_inversion, &
                                       ctx_err,o_verb_lvl=verb_lvl)
            if(.not. flag_verb) call print_fwi_iter_time(ctx_paral,       &
              'matrix_create',ctx_matrix_forward%time_create) 
            call process_factorization(ctx_paral,ctx_solver,              &
                                       ctx_matrix_forward,ctx_err,        &
                                       o_flag_verb=flag_verb)
            if(.not. flag_verb) then
              call print_fwi_iter_time(ctx_paral,'matrix_facto',          &
                                       ctx_solver%time_facto) 
              call print_fwi_iter_mem (ctx_paral,'matrix_facto',          &
                                       ctx_solver%mem_facto) 
            end if

            !! ---------------------------------------------------------
            !! loop over group of RHS
            !! ---------------------------------------------------------
            do irhs_group=1,ctx_rhs_forward%n_group
              ctx_rhs_forward%i_group = irhs_group
              ctx_rhs_backward%i_group= irhs_group
    
              !! the creation may need the pml size and HDG matrices ---
              call time(time0)
              call rhs_create(ctx_paral,ctx_aq,ctx_domain,              &
                              ctx_discretization,ctx_rhs_forward,       &
                              ctx_equation%current_angular_frequency,   &
                              ctx_equation%tag_scale_rhs,ctx_err)
              call time(time1)
              time_rhs = time_rhs + time1-time0
    
              !! solve resulting problem
              call time(time0)
              call process_solve(ctx_paral,ctx_solver,ctx_rhs_forward,  &
                                 .false.,ctx_err,o_flag_verb=flag_verb)
              call time(time1)
              time_solve = time_solve + time1-time0
              !! -------------------------------------------------------
              !! the solution is contained in the ctx_solver
              !! distribute local informations here
              !! for HDG we also need the post-processing to retrieve the
              !! wavefields from the Lagrange multipliers
              !! -------------------------------------------------------
              call field_create(ctx_paral,ctx_domain,ctx_discretization,   &
                                ctx_field,                                 &
                                ctx_rhs_forward%n_rhs_in_group(irhs_group),&
                                ctx_equation%dim_solution,                 &
                                ctx_equation%name_sol,                     &
                                ctx_solver%solution_global,.false.,ctx_err,&
                                o_flag_verb=flag_verb)

              !! -------------------------------------------------------
              !! We compute the backward rhs and update the cost function
              !! -------------------------------------------------------
              call time(time0)
              call misfit_compute_and_backrhs(ctx_paral,ctx_aq,           &
                                ctx_equation,ctx_domain,                  &
                                ctx_discretization,ctx_field,             &
                                ctx_rhs_backward,                         &
                                frequency(ifreq_start+ifreq-1,:),         &
                                mode     (imode_start+imode-1)  ,         &
                                ctx_misfit%tag_misfit,ctx_misfit%n_field, &
                                ctx_misfit%data_path,                     &
                                ctx_misfit%data_field,ctx_misfit%J,       &
                                ctx_misfit%Jloc,                          &
                                ctx_misfit%n_obs_reciprocity,             &
                                !! information on receivers offset
                                ctx_misfit%flag_rcv_offset,               &
                                ctx_misfit%rcv_offset_x,                  &
                                ctx_misfit%rcv_offset_y,                  &
                                ctx_misfit%rcv_offset_z,                  &
                                ctx_misfit%rcv_offset_rad,                &
                                !! source inversion infos
                                ctx_src_inv%flag_source_inv,              &
                                ctx_src_inv%num(1,i_fm),                  &
                                ctx_src_inv%den(1,i_fm),ctx_err)
              call time(time1)
              time_rhs = time_rhs + time1-time0
    
              !! solve resulting adjoint problem
              call time(time0)
              call process_solve(ctx_paral,ctx_solver,ctx_rhs_backward,   &
                                 .true.,ctx_err,o_flag_verb=flag_verb)
              call time(time1)
              time_solve = time_solve + time1-time0
    
              !! -------------------------------------------------------
              !! the solution is contained in ctx_solver%solution_global
              !! distribute backward field, for HDG we also need the
              !! post-processing to retrieve the wavefields from the
              !! Lagrange multipliers
              !! -------------------------------------------------------
              call field_create(ctx_paral,ctx_domain,                     &
                              ctx_discretization,ctx_field_backward,      &
                              ctx_rhs_backward%n_rhs_in_group(irhs_group),&
                              ctx_equation%dim_solution,                  &
                              ctx_equation%name_sol,                      &
                              ctx_solver%solution_global,.true.,ctx_err,  &
                              o_flag_verb=flag_verb)
    
              call time(time0)
              !! compute the gradient of the misfit functional 
              call gradient_compute(ctx_paral,ctx_discretization,       &
                                 ctx_domain,ctx_model,                  &
                                 ctx_field,ctx_field_backward,          &
                                 ctx_equation%current_angular_frequency,&
                                 ctx_grad,ctx_err)
              !! adjust gradient and misfit with the regularization term
              !! if any
              if(ctx_regularization%flag) then
                !! WARNiNG: because we update the gradient and misfit 
                !! afterwards, the regularization information is LOCAL, 
                !! i.e., it only contains the current group of shots and 
                !! current frequency and mode, it is not enriched as the 
                !! gradient or the misfit which are global.
                call regularization_compute(ctx_paral,ctx_domain,       &
                                ctx_model,ctx_regularization,           &
                                ctx_equation%current_angular_frequency, &
                                ctx_err)
                !! Enrich the gradient and misfit              
                !! we update becase the part_grad and misfit are reset to 
                !! zero everytime we arrive in the regularization_compute
                ctx_grad%grad(:,:) = ctx_grad%grad(:,:) +                 &
                                     ctx_regularization%part_grad(:,:)
                ctx_misfit%Jregularization = ctx_misfit%Jregularization + & 
                                             ctx_regularization%part_misfit
                ctx_misfit%J  = ctx_misfit%J + ctx_regularization%part_misfit
              
              end if
              call time(time1)
              time_grad = time_grad + time1-time0
    
              ! --------------------------------------------------------
              !! clean group of rhs infos and current discretization infos
              !! regarding the acquisition, update time informations
              time_field=time_field + ctx_field_backward%time_distribute+ &
                                      ctx_field%time_distribute
              call discretization_clean_context_src(ctx_discretization)
              call field_clean                     (ctx_field)
              call field_clean                     (ctx_field_backward)
              call rhs_clean_allocation            (ctx_rhs_forward)
              call rhs_clean_allocation            (ctx_rhs_backward)
            end do !! end group of rhs
            !! clean matrix infos
            call matrix_clean(ctx_matrix_forward)

          ! ------------------------------------------------------------
          end do !! end loop over frequency in group
          ! ------------------------------------------------------------

        ! --------------------------------------------------------------       
        end do !! end loop over modes in group
        ! --------------------------------------------------------------

        !! time infos for solve and rhs 
        call print_fwi_iter_time(ctx_paral,'rhs'  ,time_rhs)
        call print_fwi_iter_time(ctx_paral,'solve',time_solve)
        call print_fwi_iter_time(ctx_paral,'field',time_field)

        !! save current misfit cost 
        call misfit_save(ctx_paral,ctx_misfit,ctx_io,counter_iter,      &
                         i_iter,frequency(ifreq_start:ifreq_end,:),     &
                         mode(imode_start:imode_end),nfreq_loc,         &
                         nmode_loc,ctx_equation%flag_freq,              &
                         ctx_equation%flag_mode,ctx_err)
        misfit_evolve(i_iter) = ctx_misfit%J
        
        !! save the gradient of the cost function
        call time(time0)
        call gradient_save(ctx_paral,ctx_discretization,ctx_domain,     &
                           ctx_grad,ctx_io,counter_iter,ctx_err)
        call time(time1)
        time_grad = time_grad + (time1-time0)
        call print_fwi_iter_time(ctx_paral,'gradient',time_grad)
        
        !! save the search direction for this iteration
        call time(time0) 
        call search_dir_compute(ctx_paral,ctx_domain,ctx_io,ctx_grad,   &
                                ctx_search,counter_iter,i_iter,ctx_err)
        call search_dir_save(ctx_paral,ctx_discretization,ctx_domain,   &
                             ctx_search,ctx_io,                         &
                             trim(adjustl(ctx_grad%model_rep)),         &
                             counter_iter,ctx_err)
        call search_dir_scale(ctx_paral,ctx_grad,ctx_search,ctx_err)
        call search_dir_save(ctx_paral,ctx_discretization,ctx_domain,   &
                             ctx_search,ctx_io,                         &
                             trim(adjustl(ctx_grad%model_rep)),         &
                             counter_iter,ctx_err,o_flag_scaled=.true.)
        call time(time1)
        call print_fwi_iter_time(ctx_paral,'search_dir',time1-time0)

        ! ---------------------------------------------------------
        !! Compute linesearch to estimate the step
        !! -------------------------------------------------------------        
        call time(time0)
        call linesearch_compute(ctx_paral,ctx_equation,ctx_aq,          &
                        ctx_domain,ctx_discretization,ctx_model,        &
                        ctx_misfit,ctx_grad,ctx_regularization,         &
                        ctx_search,ctx_ls,ctx_solver,ctx_rhs_forward,   &
                        frequency(ifreq_start:ifreq_end,:),             &
                        mode     (imode_start:imode_end),nfreq_loc,     &
                        nmode_loc,ctx_src_inv%src_val(1,:),ctx_err)
        call linesearch_save(ctx_paral,ctx_ls,ctx_io,counter_iter,      &
                             i_iter,                                    &
                             frequency(ifreq_start:ifreq_end,:),        &
                             mode     (imode_start:imode_end)  ,        &
                             nfreq_loc,nmode_loc,ctx_equation%flag_freq,&
                             ctx_equation%flag_mode,ctx_err)
        call time(time1)
        call print_fwi_iter_time(ctx_paral,'linesearch',time1-time0)
        
        ! ---------------------------------------------------------
        !! update and save new source (after the linesearch per 
        !! consistency). It is saved before we update, because it is 
        !! the current iteration 
        !! -------------------------------------------------------------
        call source_save(ctx_paral,ctx_io,counter_iter,i_iter,          &
                         frequency(ifreq_start:ifreq_end,:),            &
                         mode     (imode_start:imode_end),              &
                         nfreq_loc,nmode_loc,ctx_equation%flag_freq,    &
                         ctx_equation%flag_mode,ctx_src_inv%src_val,ctx_err)
        call source_inversion_update(ctx_src_inv,ctx_err)
        call allmodel_update(ctx_paral,ctx_model,ctx_grad,ctx_search,   &
                             ctx_ls,                                    &
                             ctx_equation%current_angular_frequency,ctx_err)
        
        ! ---------------------------------------------------------
        !! stagnation check
        !! -------------------------------------------------------------
        flag_stagnation=.false.
        if(i_iter > 4) then
          improvement = (misfit_evolve(i_iter-4)-misfit_evolve(i_iter))/&
                         misfit_evolve(i_iter-4)
          if(improvement < ctx_misfit%tol_stagnate .and.                &
             i_iter >= ctx_misfit%niter_min) then
            flag_stagnation=.true.
          end if
        end if

        ! ---------------------------------------------------------
        !! if last iteration of stagnation, we save last model with 
        !! frequency and mode infos, otherwize, only iteration
        if(flag_stagnation .or. i_iter == ctx_misfit%niter) then
          !! possibility to clean output
          call time(time0)
          call io_delete(ctx_paral,ctx_io,ctx_err)
          call time(time1)
          if(ctx_io%clean_final > 0)  then
            call print_fwi_iter_time(ctx_paral,'clean_io',time1-time0)
          end if
          !! -----------------------------------------------------------
          !! save current models parametrization and reference
          !! -----------------------------------------------------------
          call model_save(ctx_paral,ctx_equation,ctx_domain,            &
                          ctx_discretization,ctx_model,ctx_io,          &
                          ctx_grad%n_grad,ctx_grad%param_name,          &
                          counter_iter,                                 &
                          ctx_equation%current_angular_frequency,       &
                          ctx_err,o_flag_param_grad=.true.,             &
                          o_frequency=frequency(ifreq_end,:),           &
                          o_mode=mode(imode_end))
          call model_save(ctx_paral,ctx_equation,ctx_domain,            &
                          ctx_discretization,ctx_model,ctx_io,          &
                          ctx_model%n_model,ctx_model%name_model,       &
                          counter_iter,                                 &
                          ctx_equation%current_angular_frequency,       &
                          ctx_err,                                      &
                          o_frequency=frequency(ifreq_end,:),           &
                          o_mode=mode(imode_end))
        else
          !! save current models parametrization 
          call model_save(ctx_paral,ctx_equation,ctx_domain,            &
                          ctx_discretization,ctx_model,ctx_io,          &
                          ctx_grad%n_grad,ctx_grad%param_name,          &
                          counter_iter,                                 &
                          ctx_equation%current_angular_frequency,       &
                          ctx_err,                                      &
                          o_flag_param_grad=.true.)
        end if 
        ! ---------------------------------------------------------
        call time(timegb1)
        call print_fwi_outcome_iter(ctx_paral,i_iter,ctx_misfit%niter,  &
                                   counter_iter,timegb1-timegb0,        &
                                   improvement,flag_stagnation)
        if(flag_stagnation) then
          exit !! Exit iteration loop for next frequency
        end if
        ! ---------------------------------------------------------

      end do !! end loop minimization iterations

    end do !! end loop group of frequency and modes

    !! clean inversion infos
    call io_delete(ctx_paral,ctx_io,ctx_err) !! (conditional delete)
    call misfit_clean          (ctx_misfit)
    call gradient_clean        (ctx_grad)
    call search_dir_clean      (ctx_search)
    call linesearch_clean      (ctx_ls)
    call source_inversion_clean(ctx_src_inv)
    call regularization_clean  (ctx_regularization)
    !! clean remaining infos
    call solver_clean                   (ctx_solver,ctx_err)
    call model_clean                    (ctx_model)
    call rhs_clean                      (ctx_rhs_forward,ctx_err)
    call rhs_clean                      (ctx_rhs_backward,ctx_err)
    call domain_clean                   (ctx_domain)
    call equation_clean                 (ctx_equation)
    call discretization_clean_context   (ctx_discretization)
    call discretization_clean_context_io(ctx_discretization)
    call acquisition_clean              (ctx_aq)
    call io_clean                       (ctx_io)
    if(allocated(frequency)) deallocate(frequency)
    if(allocated(mode))      deallocate(mode)
    !! mpi_barrier waits for every processors
    call barrier(ctx_paral%communicator,ctx_err)

    return
  end subroutine process
  !---------------------------------------------------------------------

!------------------------------------------------------------------------------
end module m_process
!------------------------------------------------------------------------------
