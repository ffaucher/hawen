!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_define_precision
!
!> Definition of the precision for integers and reals
!>
!> REFERENCE: {https://software.intel.com/en-us/node/691891}
!>
!
!> INTEGER(4) :: values range from -2 147 483 648 to 2 147 483 647
!>   [4bytes]    and are stored in 4 contiguous bytes
!> INTEGER(8) :: values range from -9,223,372,036,854,775,808 to 
!>                                  9,223,372,036,854,775,807 
!>               and are stored in 8 contiguous bytes.
!> Remark: INTEGER(1) is from    -128 to    127
!>         INTEGER(2) is from -32 768 to 32 767
!
!> REAL(4)    :: Single-precision real floating-point values in 
!>   [4bytes]    IEEE S_floating format ranging from 
!>               1.17549435E-38 to 3.40282347E38. 
!>               Values between 1.17549429E-38 and 1.40129846E-45 are 
!>               denormalized (subnormal).
!> REAL(8)    :: Double-precision real floating-point values in IEEE 
!>               T_floating format ranging from 
!>               2.2250738585072013D-308 to 1.7976931348623158D308. 
!>               Values between 2.2250738585072008D-308 and 
!>               4.94065645841246544D-324 are denormalized (subnormal).
!> REAL(16)   :: Extended-precision real floating-point values in 
!>               IEEE-style X_floating format ranging from 
!>               6.4751751194380251109244389582276465524996Q-4966 to 
!>               1.189731495357231765085759326628007016196477Q4932.
!
!> COMPLX(4)  :: Single-precision complex floating-point values in a 
!>   [8bytes]    pair of IEEE S_floating format parts, i.e. 2 REAL(4).
!> COMPLEX(8) :: Double-precision complex floating-point values in a 
!>               pair of IEEE T_floating format parts, i.e. 2 REAL(8).
!> COMPLEX(16):: Extended-precision complex floating-point values in a 
!>               pair of IEEE-style X_floating format part, i.e. 2 REAL(16).
!
!------------------------------------------------------------------------------
module m_define_precision
  
  implicit none
  private
  public   :: IKIND_MESH, RKIND_MESH, IKIND_MAT, RKIND_MAT, RKIND_POL
  public   :: IKIND_METIS, RKIND_METIS
  !> for polynomials computation, should be 8 (double) or 16 (quadruple)
  integer, parameter :: RKIND_POL = 8

  !> mesh informations: should be 4 or 8 and
  integer, parameter :: IKIND_MESH = 4
  integer, parameter :: RKIND_MESH = 8

  !> this is to chose depending on the compilation of 
  !> Metis: 4 if simple precision (32 bits)
  !>        8 if double precision (64 bits)
  !> careful that even if compiled with int 64 bits,
  !> the size of reals is usually 32 bits nonetheless.
  integer, parameter :: IKIND_METIS = 8
  integer, parameter :: RKIND_METIS = 4

  !> matrix informations: should be 4 or 8
  integer, parameter :: IKIND_MAT  = 4
  integer, parameter :: RKIND_MAT  = IKIND_MAT

end module m_define_precision
