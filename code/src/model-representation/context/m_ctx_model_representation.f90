!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_model_representation.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines of the context for the model representation, e.g.,
!>  -> piecewise constant
!>  -> piecewise polynomials
!>  -> splines
!
!------------------------------------------------------------------------------
module m_ctx_model_representation

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH
  !! for piecewise linear 
  use m_polynomial,             only: t_polynomial,polynomial_clean,    &
                                      polynomial_copy
  !! for model represented per dof.
  use m_basis_functions,        only: t_basis,basis_construct,basis_clean
  use m_array_types,            only: t_array1d_real, array_deallocate, &
                                      array_allocate
  !! -------------------------------------------------------------------
  implicit none

  !! the list of sep format 
  character(len=9) , parameter :: tag_SEPFORMAT_CART       = 'cartesian'
  character(len=12), parameter :: tag_SEPFORMAT_SPH1D      = 'spherical-1d'
  character(len=13), parameter :: tag_SEPFORMAT_SPH3D      = 'spherical-coo'

  !! list of tag for the model representations -------------------------
  character(len=18), parameter :: tag_MODEL_PCONSTANT= 'piecewise-constant'
  character(len=20), parameter :: tag_MODEL_PPOLY    = 'piecewise-polynomial'
  character(len=3) , parameter :: tag_MODEL_SEP      = 'sep'
  character(len=3) , parameter :: tag_MODEL_DOF      = 'dof'
  !! -------------------------------------------------------------------
  
  private
  public  :: t_model_param, model_representation_clean
  public  :: model_representation_copy, model_representation_eval_sep
  public  :: model_representation_eval_sep_extend0
  public  :: tag_MODEL_PCONSTANT, tag_MODEL_PPOLY, tag_MODEL_SEP
  public  :: tag_MODEL_DOF
  public  :: tag_SEPFORMAT_CART, tag_SEPFORMAT_SPH1D, tag_SEPFORMAT_SPH3D

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_model_param_sep contains info on the models 
  !> representation if the original sep model is kept.
  !
  !> utils:
  !> @param[logical]     str_sep_format  : e.g., spherical representation
  !> @param[double]      d{x,y,z}        : step
  !> @param[integer]     n{x,y,z}        : size
  !> @param[double]      o{x,y,z}        : origins
  !> @param[double]      model           : sep model
  !> @param[double]      spherical       : spherical sep model
  !---------------------------------------------------------------------
  type t_model_param_sep
  
    !! spherical representation.
    character(len=32)               :: str_sep_format
    !! step
    real(kind=8)                    :: dx, dy, dz
    !! size
    integer                         :: nx, ny, nz
    !! origins
    real(kind=8)                    :: ox, oy, oz
    !! models
    real(kind=8),      allocatable  :: sep_model(:,:,:)
    real(kind=8),      allocatable  :: sep_model_spherical(:)

  end type t_model_param_sep

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_model_param_dof contains info on the models 
  !> representation per dof. There is one value per dof, 
  !> every cell has Ndof values, and a specific order.
  !
  !> utils:
  !> @param[integer]     order       : order per cell
  !> @param[integer]     ndof        : ndof per cell
  !> @param[t_basis]     basis       : basis functions
  !> @param[double]      val         : model dof values
  !---------------------------------------------------------------------
  type t_model_param_dof
  
    !! constant order 
    integer                              :: order
    !! ndof per cells
    integer                              :: ndof
    !! basis function at this order
    type(t_basis)                        :: basis
    !! the dof values on each cell, size n_cells
    type(t_array1d_real),   allocatable  :: mval (:)

  end type t_model_param_dof

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_model_param contains info on the models 
  !> representation and the sub-domain decomposition
  !
  !> utils:
  !> @param[logical]     flag       : specific representation or not ?
  !> @param[t_pconstant] pconstant  : may be piecewise constant
  !> @param[t_poly]      ppoly      : may be piecewise polynomials
  !> @param[real]        reduc_tol  : target reduction (between 0 and 1)
  !> @param[integer]     n_subdomain: number of representation subdomains
  !> @param[integer]     decision   : convert local cell to global subdomain
  !---------------------------------------------------------------------
  type t_model_param

    !! model parameters for the representation: 
    !! picewise-constant, piecewise-polynomial or per dof.
    real(kind=8),      allocatable  :: pconstant(:)
    
    !! type t_polynomial is composed of 
    !       - dim_domain   => dimension
    !       - degree       => degree 
    !       - size_coeff   => n_coefficients 
    !       - coeff_pol(:) => polynomial coefficients
    type(t_polynomial),allocatable  :: ppoly(:)
    !! in the case of piecewise-polynomials: the order of the pol.
    integer                         :: ppoly_order
    !! in the case of original cartesian sep.
    type(t_model_param_sep)         :: param_sep
    !! in the case of a piecewise-constant per dof.
    type(t_model_param_dof)         :: param_dof

    !! -----------------------------------------------------------------
    !! in case of compression only -------------------------------------
    !! convert the local element n° to global subdomain number
    !! size(n_cell_loc), common for reduction or not for evaluation.
    integer(kind=IKIND_MESH), allocatable  :: decision(:)
    !! actual number of subdomain = ncells if no compression
    integer(kind=IKIND_MESH)        :: n_subdomain_gb

    !! -----------------------------------------------------------------    
    !! associated with model reduction only ----------------------------
    !! indicates if the model is reduced or not 
    logical                         :: compression_flag
    !! indicates the format method if reduction (e.g. neighbors, structured)
    character(len=64)               :: compression_str
    !! sub-domain informations
    !! target reduction (between 0 and 1), 1 = no reduction
    real                            :: compression_tol
    !! for box compression, indicates the limit
    real                            :: compression_box_x
    real                            :: compression_box_y
    real                            :: compression_box_z
    !! -----------------------------------------------------------------

  end type t_model_param
  
  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model from the saved sep file
  !
  !> @param[in]    t_model_param_sep  : model sep context
  !> @param[in]    xpt                : which x
  !> @param[out]   out_val            : output value
  !> @param[inout] ctx_err            : context error
  !----------------------------------------------------------------------------
  subroutine model_representation_eval_sep(ctx_model_sep,dim_domain,    &
                                           xpt,out_val,ctx_err)

    implicit none

    type(t_model_param_sep) ,intent(in)   :: ctx_model_sep
    integer                 ,intent(in)   :: dim_domain
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                    :: ind_x, ind_y, ind_z
    real(kind=8)               :: xpos, coo(3), r, theta_rad, theta_deg
    real(kind=8)               :: phi_rad, phi_deg
    real(kind=8), parameter    :: pi = 4.d0*datan(1.d0)

    out_val = 0.d0

    !! depends on the format of the sep
    select case (trim(adjustl(ctx_model_sep%str_sep_format)))
      !! classic cartesian ---------------------------------------------
      case(tag_SEPFORMAT_CART) 
      !! ---------------------------------------------------------------
        ind_x = 1
        ind_y = 1
        ind_z = 1
        select case(dim_domain)
          case(1)
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
          case(2)
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_z = floor((xpt(2) - ctx_model_sep%oz)/ctx_model_sep%dz) + 1
          case(3)
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_y = floor((xpt(2) - ctx_model_sep%oy)/ctx_model_sep%dy) + 1
            ind_z = floor((xpt(3) - ctx_model_sep%oz)/ctx_model_sep%dz) + 1
         case default
          ctx_err%msg   = "** ERROR: incorrect dimension [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error
        end select
        
        !! consitency
        if(ind_x > ctx_model_sep%nx .or. ind_x < 1 .or.  &
           ind_y > ctx_model_sep%ny .or. ind_y < 1 .or.  &
           ind_z > ctx_model_sep%nz .or. ind_z < 1) then
          ctx_err%msg   = "** ERROR: index is out [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error
        end if
        out_val = ctx_model_sep%sep_model(ind_x,ind_y,ind_z)

      !! spherical 1d --------------------------------------------------
      case(tag_SEPFORMAT_SPH1D) 
      !! ---------------------------------------------------------------
        xpos    = 0.d0
        coo     = 0.d0
        coo(1)  = xpt(1) - ctx_model_sep%ox
        select case(dim_domain)
          case(1)
          case(2) 
            coo(2)  = xpt(2) - ctx_model_sep%oz
          case(3)
            coo(2)  = xpt(2) - ctx_model_sep%oy
            coo(3)  = xpt(3) - ctx_model_sep%oz
          case default
           ctx_err%msg   = "** ERROR: incorrect dimension [model_eval_sep] "
           ctx_err%ierr  = -1
           return ! non shared error
        end select
 
        xpos  = norm2(coo)
        ind_x = floor(xpos/ctx_model_sep%dx) + 1
 
        !! consitency
        if(ind_x > ctx_model_sep%nx .or. ind_x < 1) then
          ctx_err%msg   = "** ERROR: index is out [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error
        end if
        out_val = ctx_model_sep%sep_model_spherical(ind_x)

      !! spherical 3d --------------------------------------------------
      case(tag_SEPFORMAT_SPH3D) 
      !! ---------------------------------------------------------------
        !! convert in r, theta, phi
        ind_x = 1
        ind_y = 1
        ind_z = 1
        select case(dim_domain)
          case(1)
            !! only r
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
          case(2)
            !! r and theta <=> x and z by convention
            coo(1) = xpt(1)
            coo(2) = 0.d0
            coo(3) = xpt(3)
            r      = norm2(coo)
            if(r > tiny(1.d0)) then
              theta_rad = datan2(coo(3),coo(1))
            else
              theta_rad = 0.d0
            end if
            !! convert to degrees
            theta_deg = theta_rad * 180.d0 / pi
            !! set betwee 0 and 359.9
            if(theta_deg < 0)              theta_deg = 360 + theta_deg
            if(theta_deg > 360-tiny(1.d0)) theta_deg = 0.0
            
            ind_x = floor((r        -ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_z = floor((theta_deg-ctx_model_sep%oz)/ctx_model_sep%dz) + 1
          
          case(3)
            !! r, theta, phi <=> x, z and y by convention
            coo(1) = xpt(1)
            coo(2) = xpt(2)
            coo(3) = xpt(3)
            r      = norm2(coo)
            !! now we need the angle theta and phi
            if(abs(r) > tiny(1.d0)) then
              
              theta_rad = acos(dble(coo(3)/r))
              if(abs(coo(1)) > tiny(1.d0) .and. abs(coo(2)) > tiny(1.d0)) then
                phi_rad = datan2(coo(2), coo(1))
              else
                phi_rad = 0.d0
              end if
            else
              theta_rad = 0.d0
              phi_rad   = 0.d0
            end if
          
            !! convert to degrees
            theta_deg = theta_rad * 180.d0 / pi
            phi_deg   =   phi_rad * 180.d0 / pi
            !! set betwee 0 and 359.9
            if(theta_deg < 0             ) theta_deg = 360 + theta_deg
            if(theta_deg > 360-tiny(1.d0)) theta_deg = 0.0
            if(  phi_deg < 0             )   phi_deg = 360 + phi_deg
            if(  phi_deg > 360-tiny(1.d0))   phi_deg = 0.0


            ind_x = floor((r        - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_y = floor((phi_deg  - ctx_model_sep%oy)/ctx_model_sep%dy) + 1
            ind_z = floor((theta_deg- ctx_model_sep%oz)/ctx_model_sep%dz) + 1
         case default
          ctx_err%msg   = "** ERROR: incorrect dimension [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error
        end select
        
        !! consitency
        if(ind_x > ctx_model_sep%nx .or. ind_x < 1 .or.  &
           ind_y > ctx_model_sep%ny .or. ind_y < 1 .or.  &
           ind_z > ctx_model_sep%nz .or. ind_z < 1) then
          ctx_err%msg   = "** ERROR: index is out [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error
        end if
        out_val = ctx_model_sep%sep_model(ind_x,ind_y,ind_z)

      !! default error -------------------------------------------------
      case default
      !! ---------------------------------------------------------------
        ctx_err%msg   = "** ERROR: unrecognized sep format of " //      &
                        "coordinates: "                         //      &
                        trim(adjustl(ctx_model_sep%str_sep_format))//   &
                        " [model_eval_sep] "
        ctx_err%ierr  = -1
        return ! non shared error      
    end select

    return

  end subroutine model_representation_eval_sep

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model from the saved sep file, we extend by 0 if the 
  !> position is not found in the mesh.
  !
  !> @param[in]    t_model_param_sep  : model sep context
  !> @param[in]    xpt                : which x
  !> @param[out]   out_val            : output value
  !> @param[inout] ctx_err            : context error
  !----------------------------------------------------------------------------
  subroutine model_representation_eval_sep_extend0(ctx_model_sep,       &
                                                   dim_domain,xpt,      &
                                                   out_val,ctx_err)

    implicit none

    type(t_model_param_sep) ,intent(in)   :: ctx_model_sep
    integer                 ,intent(in)   :: dim_domain
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                    :: ind_x, ind_y, ind_z
    real(kind=8)               :: xpos, coo(3), r, theta_rad, theta_deg
    real(kind=8)               :: phi_rad, phi_deg
    real(kind=8), parameter    :: pi = 4.d0*datan(1.d0)

    out_val = 0.d0
    
    !! depends on the format of the sep
    select case (trim(adjustl(ctx_model_sep%str_sep_format)))
      !! classic cartesian ---------------------------------------------
      case(tag_SEPFORMAT_CART) 
      !! ---------------------------------------------------------------
        ind_x = 1
        ind_y = 1
        ind_z = 1
        select case(dim_domain)
          case(1)
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
          case(2)
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_z = floor((xpt(2) - ctx_model_sep%oz)/ctx_model_sep%dz) + 1
          case(3)
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_y = floor((xpt(2) - ctx_model_sep%oy)/ctx_model_sep%dy) + 1
            ind_z = floor((xpt(3) - ctx_model_sep%oz)/ctx_model_sep%dz) + 1
         case default
          ctx_err%msg   = "** ERROR: incorrect dimension [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error
        end select
        
        !! we return zero if point not found
        if(ind_x > ctx_model_sep%nx .or. ind_x < 1 .or.  &
           ind_y > ctx_model_sep%ny .or. ind_y < 1 .or.  &
           ind_z > ctx_model_sep%nz .or. ind_z < 1) then
          out_val = 0.d0
        else
          out_val = ctx_model_sep%sep_model(ind_x,ind_y,ind_z)
        end if
        

      !! spherical 1d --------------------------------------------------
      case(tag_SEPFORMAT_SPH1D) 
      !! ---------------------------------------------------------------
        xpos    = 0.d0
        coo     = 0.d0
        coo(1)  = xpt(1) - ctx_model_sep%ox
        select case(dim_domain)
          case(1)
          case(2) 
            coo(2)  = xpt(2) - ctx_model_sep%oz
          case(3)
            coo(2)  = xpt(2) - ctx_model_sep%oy
            coo(3)  = xpt(3) - ctx_model_sep%oz
          case default
           ctx_err%msg   = "** ERROR: incorrect dimension [model_eval_sep] "
           ctx_err%ierr  = -1
           return ! non shared error
        end select

        xpos  = norm2(coo)
        ind_x = floor(xpos/ctx_model_sep%dx) + 1

        !! consitency
        if(ind_x > ctx_model_sep%nx .or. ind_x < 1) then
          out_val = 0.d0
        else
          out_val = ctx_model_sep%sep_model_spherical(ind_x)
        end if

      !! spherical 3d --------------------------------------------------
      case(tag_SEPFORMAT_SPH3D) 
      !! ---------------------------------------------------------------
        !! convert in r, theta, phi
        ind_x = 1
        ind_y = 1
        ind_z = 1
        select case(dim_domain)
          case(1)
            !! only r
            ind_x = floor((xpt(1) - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
          case(2)
            !! r and theta <=> x and z by convention
            coo(1) = xpt(1)
            coo(2) = 0.d0
            coo(3) = xpt(2)
            r      = norm2(coo)
            if(r > tiny(1.d0)) then
              theta_rad = datan2(coo(3),coo(1))
            else
              theta_rad = 0.d0
            end if
            !! convert to degrees
            theta_deg = theta_rad * 180.d0 / pi
            !! set betwee 0 and 359.9
            if(theta_deg < 0)              theta_deg = 360 + theta_deg
            if(theta_deg > 360-tiny(1.d0)) theta_deg = 0.0
            
            ind_x = floor((r        -ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_z = floor((theta_deg-ctx_model_sep%oz)/ctx_model_sep%dz) + 1
          
          case(3)
            !! r, theta, phi <=> x, z and y by convention
            coo(1) = xpt(1)
            coo(2) = xpt(2)
            coo(3) = xpt(3)
            r      = norm2(coo)
            !! now we need the angle theta and phi
            if(abs(r) > tiny(1.d0)) then
              theta_rad = acos(dble(coo(3)/r))
              if(abs(coo(1)) > tiny(1.d0) .and. abs(coo(2)) > tiny(1.d0)) then
                phi_rad = datan2(coo(2), coo(1))
              else
                phi_rad = 0.d0
              end if
            else
              theta_rad = 0.d0
              phi_rad   = 0.d0
            end if
          
            !! convert to degrees
            theta_deg = theta_rad * 180.d0 / pi
            phi_deg   =   phi_rad * 180.d0 / pi
            !! theta is between 0 and 180, while phi is between -180 and 180.

            !! set betwee 0 and 359.9
            !! if(theta_deg < 0             ) theta_deg = 360 + theta_deg
            !! if(theta_deg > 360-tiny(1.d0)) theta_deg = 0.0
            !! if(  phi_deg < 0             )   phi_deg = 360 + phi_deg
            !! if(  phi_deg > 360-tiny(1.d0))   phi_deg = 0.0


            ind_x = floor((r        - ctx_model_sep%ox)/ctx_model_sep%dx) + 1
            ind_y = floor((phi_deg  - ctx_model_sep%oy)/ctx_model_sep%dy) + 1
            ind_z = floor((theta_deg- ctx_model_sep%oz)/ctx_model_sep%dz) + 1

         case default
          ctx_err%msg   = "** ERROR: incorrect dimension [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error
        end select
        
        !! consitency
        if(ind_x > ctx_model_sep%nx .or. ind_x < 1 .or.  &
           ind_y > ctx_model_sep%ny .or. ind_y < 1 .or.  &
           ind_z > ctx_model_sep%nz .or. ind_z < 1) then
          out_val = 0.d0
        else
          out_val = ctx_model_sep%sep_model(ind_x,ind_y,ind_z)
        end if

      !! default error -------------------------------------------------
      case default
      !! ---------------------------------------------------------------
          ctx_err%msg   = "** ERROR: unrecognized sep form of " //      &
                          "coordinates [model_eval_sep] "
          ctx_err%ierr  = -1
          return ! non shared error      
    end select

    return

  end subroutine model_representation_eval_sep_extend0

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> copy model information
  !
  !> @param[in]  ctx_model_in  : model context in
  !> @param[out] ctx_model_out : model context out
  !> @param[in]  flag_allocate : indicates if we need to allocate arrays too
  !> @param[out] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine model_representation_copy(ctx_model_param_in,ctx_model_param_cp, &
                                       flag_allocate,ctx_err)
    implicit none
    type(t_model_param)   ,intent(in)     :: ctx_model_param_in
    type(t_model_param)   ,intent(inout)  :: ctx_model_param_cp
    logical               ,intent(in)     :: flag_allocate
    type(t_error)         , intent(out)   :: ctx_err
    !! 
    integer :: n,k

    ctx_model_param_cp%ppoly_order      =ctx_model_param_in%ppoly_order
    ctx_model_param_cp%compression_tol  =ctx_model_param_in%compression_tol
    ctx_model_param_cp%n_subdomain_gb   =ctx_model_param_in%n_subdomain_gb
    ctx_model_param_cp%compression_str  =trim(adjustl(ctx_model_param_in%compression_str))
    ctx_model_param_cp%compression_box_x=ctx_model_param_in%compression_box_x   
    ctx_model_param_cp%compression_box_y=ctx_model_param_in%compression_box_y   
    ctx_model_param_cp%compression_box_z=ctx_model_param_in%compression_box_z   
     
    !! allocate
    if(allocated(ctx_model_param_in%decision)) then
      n = size(ctx_model_param_in%decision)
      if(flag_allocate) allocate(ctx_model_param_cp%decision(n))
      ctx_model_param_cp%decision = ctx_model_param_in%decision
    end if
    if(allocated(ctx_model_param_in%pconstant)) then
      n = size(ctx_model_param_in%pconstant)
      if(flag_allocate) allocate(ctx_model_param_cp%pconstant(n))
      ctx_model_param_cp%pconstant = ctx_model_param_in%pconstant
    end if

    !! -----------------------------------------------------------------
    !! copy if dof representation
    ctx_model_param_cp%param_dof%order = ctx_model_param_in%param_dof%order
    ctx_model_param_cp%param_dof%ndof  = ctx_model_param_in%param_dof%ndof
    if(allocated(ctx_model_param_in%param_dof%mval)) then
    
      !! copy basis function
      if(flag_allocate) then
        call basis_construct(ctx_model_param_in%param_dof%basis%dim_domain, &
                             ctx_model_param_in%param_dof%order,            &
                             ctx_model_param_cp%param_dof%basis,ctx_err)
      end if
      
      n = size(ctx_model_param_in%param_dof%mval) !! number of cells !
      if(flag_allocate) then
        allocate(ctx_model_param_cp%param_dof%mval(n))
      end if
      !! copy values
      do k=1,n
        if(flag_allocate) then
          call array_allocate(ctx_model_param_cp%param_dof%mval(k),     &
                              ctx_model_param_in%param_dof%ndof,ctx_err)
        end if
        ctx_model_param_cp%param_dof%mval(k)%array =                    &
                              ctx_model_param_in%param_dof%mval(k)%array
      end do
    end if
    !! -----------------------------------------------------------------

    !! need to copy the polynomials too
    if(allocated(ctx_model_param_in%ppoly)) then
      n = size(ctx_model_param_in%ppoly)
      if(flag_allocate) allocate(ctx_model_param_cp%ppoly(n))
      do k=1,n
        call polynomial_copy(ctx_model_param_in%ppoly(k),               &
                             ctx_model_param_cp%ppoly(k))
      end do
    end if
    !! -----------------------------------------------------------------
    
    !! sep models represenation
    ctx_model_param_cp%param_sep%str_sep_format =                       &
      ctx_model_param_in%param_sep%str_sep_format
    ctx_model_param_cp%param_sep%dx = ctx_model_param_in%param_sep%dx
    ctx_model_param_cp%param_sep%dy = ctx_model_param_in%param_sep%dy
    ctx_model_param_cp%param_sep%dz = ctx_model_param_in%param_sep%dz
    ctx_model_param_cp%param_sep%nx = ctx_model_param_in%param_sep%nx
    ctx_model_param_cp%param_sep%ny = ctx_model_param_in%param_sep%ny
    ctx_model_param_cp%param_sep%nz = ctx_model_param_in%param_sep%nz
    ctx_model_param_cp%param_sep%ox = ctx_model_param_in%param_sep%ox
    ctx_model_param_cp%param_sep%oy = ctx_model_param_in%param_sep%oy
    ctx_model_param_cp%param_sep%oz = ctx_model_param_in%param_sep%oz
    if(allocated(ctx_model_param_in%param_sep%sep_model)) then
      if(flag_allocate) then
        allocate(ctx_model_param_cp%param_sep%sep_model(                &
                 ctx_model_param_cp%param_sep%nx,                       &
                 ctx_model_param_cp%param_sep%ny,                       &
                 ctx_model_param_cp%param_sep%nz))
        ctx_model_param_cp%param_sep%sep_model =                        &
        ctx_model_param_in%param_sep%sep_model
      end if
    end if
    if(allocated(ctx_model_param_in%param_sep%sep_model_spherical)) then
      if(flag_allocate) then
        allocate(ctx_model_param_cp%param_sep%sep_model_spherical(      &
                 ctx_model_param_cp%param_sep%nx))
        ctx_model_param_cp%param_sep%sep_model_spherical =              &
        ctx_model_param_in%param_sep%sep_model_spherical
      end if
    end if

    return
  end subroutine model_representation_copy

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean model information
  !
  !> @param[inout] ctx_model    : model context
  !----------------------------------------------------------------------------
  subroutine model_representation_clean(ctx_model_param)
    implicit none
    type(t_model_param)   ,intent(inout)  :: ctx_model_param
    integer :: k,n

    !! deallocated
    if(allocated(ctx_model_param%decision))        & 
      deallocate(ctx_model_param%decision)
    if(allocated(ctx_model_param%pconstant))       & 
      deallocate(ctx_model_param%pconstant)
    if(allocated(ctx_model_param%param_dof%mval)) then
      n = size(ctx_model_param%param_dof%mval)
      do k=1,n
        call array_deallocate(ctx_model_param%param_dof%mval(k))
      end do
      deallocate(ctx_model_param%param_dof%mval)
      call basis_clean(ctx_model_param%param_dof%basis)
    end if
    if(allocated(ctx_model_param%ppoly)) then
      n = size(ctx_model_param%ppoly)
      do k=1,n
        call polynomial_clean(ctx_model_param%ppoly(k))
      end do
      deallocate(ctx_model_param%ppoly)
    end if

    ctx_model_param%compression_flag =.false.
    ctx_model_param%compression_str  =''
    ctx_model_param%ppoly_order      =0
    ctx_model_param%compression_tol  =0.0
    ctx_model_param%compression_box_x=0.0
    ctx_model_param%compression_box_y=0.0
    ctx_model_param%compression_box_z=0.0
    ctx_model_param%n_subdomain_gb =0

    !! sep models represenation
    ctx_model_param%param_sep%str_sep_format=''
    ctx_model_param%param_sep%dx = 0.d0
    ctx_model_param%param_sep%dy = 0.d0
    ctx_model_param%param_sep%dz = 0.d0
    ctx_model_param%param_sep%nx = 0
    ctx_model_param%param_sep%ny = 0
    ctx_model_param%param_sep%nz = 0
    ctx_model_param%param_sep%ox = 0.d0
    ctx_model_param%param_sep%oy = 0.d0
    ctx_model_param%param_sep%oz = 0.d0
    if(allocated(ctx_model_param%param_sep%sep_model))               &
      deallocate(ctx_model_param%param_sep%sep_model)
    if(allocated(ctx_model_param%param_sep%sep_model_spherical))     &
      deallocate(ctx_model_param%param_sep%sep_model_spherical)

    return
  end subroutine model_representation_clean

end module m_ctx_model_representation
