!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_representation_dof.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the model representation per dof. 
!> That is, given an order, the corresponding dof each has a value 
!> of the material properties
!> 
!
!------------------------------------------------------------------------------
module m_model_representation_dof

  use m_define_precision,         only : IKIND_MESH, RKIND_POL
  use m_raise_error,              only : raise_error, t_error
  use m_ctx_domain,               only : t_domain
  use m_array_types,              only : t_array1d_real, array_allocate
  use m_polynomial,               only : polynomial_eval 
  use m_basis_functions,          only : basis_construct
  !! model representation 
  use m_ctx_model_representation, only : t_model_param,                 &
                                         model_representation_eval_sep
  !! to find the dof position
  use m_dg_lagrange_dof_coordinates, only:                              &
                                       discretization_dof_coo_loc_elem
  implicit none

  interface model_representation_dof_eval_refelem
     module procedure model_representation_dof_eval_refelem_solo
     module procedure model_representation_dof_eval_refelem_multi
  end interface model_representation_dof_eval_refelem


  private  

  public :: model_representation_dof_init
  public :: model_representation_dof_eval_refelem
    
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the number of volume dof from a given order. It uses
  !> simplex domain.
  !
  !> @param[in]    dim_domain         : dimension
  !> @param[in]    order              : order
  !> @param[out]   ndof               : number of volume dof
  !----------------------------------------------------------------------------
  pure function compute_volumic_ndof(dim_domain,order) result(ndof)

    implicit none
    integer(kind=4)      ,intent(in)   :: dim_domain
    integer(kind=4)      ,intent(in)   :: order
    integer(kind=4)                    :: ndof
    
    ndof = 0
    select case(dim_domain)
      case(1)
        ndof = order+1
      case(2)
        ndof = (order+2)*(order+1)/2
      case(3)
        ndof = (order+3)*(order+2)*(order+1)/6
    end select

    return
  end function compute_volumic_ndof


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine compute the model dof representation.
  !> @details
  !
  !> @param[in]    mesh          : mesh context
  !> @param[out]   ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine model_representation_dof_init(ctx_mesh,ctx_model_param,ctx_err)

    implicit none

    type(t_domain)          ,intent(in)   :: ctx_mesh    
    type(t_model_param)     ,intent(inout):: ctx_model_param
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: dim_domain, i_dof, ndof
    integer(kind=IKIND_MESH)  :: i_cell
    integer                   :: order
    real(kind=8)              :: sep_val
    real(kind=8), allocatable :: dof_coo(:,:)
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0
    dim_domain   = ctx_mesh%dim_domain

    !! -----------------------------------------------------------------
    !! compute how many dof for this order   ---------------------------
    ctx_model_param%param_dof%ndof  = 0
    order = ctx_model_param%param_dof%order
    if(dim_domain>=1 .and. dim_domain<=3) then
      ndof = compute_volumic_ndof(dim_domain,order)
    else
      ctx_err%msg   = "** ERROR: Unrecognized Dimension [model_dof_init] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    ctx_model_param%param_dof%ndof = ndof
    
    !! init basis function at this order
    call basis_construct(dim_domain,order,                              &
                         ctx_model_param%param_dof%basis,ctx_err)
    !! loop over all cell
    do i_cell = 1, ctx_mesh%n_cell_loc
      !! allocate arrays
      call array_allocate(ctx_model_param%param_dof%mval(i_cell),ndof,ctx_err)
      ctx_model_param%param_dof%mval(i_cell)%array = 0.d0 
      !! get the values ------------------------------------------------ 
      if(order > 0) then
        !! compute the positions for this cell
        allocate(dof_coo(dim_domain,ndof))
        dof_coo = 0.d0
        call discretization_dof_coo_loc_elem(ctx_mesh,i_cell,order,ndof,&
                                             dof_coo,ctx_err)
        !! get the sep values for all of the coordinates
        do i_dof = 1, ndof
          call model_representation_eval_sep(ctx_model_param%param_sep, &
                                             dim_domain,                &
                                             dof_coo(:,i_dof),sep_val,  &
                                             ctx_err)
          ctx_model_param%param_dof%mval(i_cell)%array(i_dof) = sep_val
        end do
        !! also update the piecewise constant part
        ctx_model_param%pconstant(i_cell) = dble(1.d0/ndof              &
                  * sum(ctx_model_param%param_dof%mval(i_cell)%array(:)))
        deallocate(dof_coo)
      !! order 0 -------------------------------------------------------
      else 
        allocate(dof_coo(dim_domain,1))
        !! compute the positions for this cell
        dof_coo(:,1) = ctx_mesh%cell_bary(:,i_cell)
        call model_representation_eval_sep(ctx_model_param%param_sep,   &
                                           dim_domain,dof_coo(:,1),     &
                                           sep_val,ctx_err)
        ctx_model_param%param_dof%mval(i_cell)%array(1) = sep_val
        ctx_model_param%pconstant(i_cell)               = sep_val
        deallocate(dof_coo)  
      endif
      
    end do
    
    !! -----------------------------------------------------------------
    return
  end subroutine model_representation_dof_init

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine evaluates the value at a given position for
  !>  the model dof representation. We assume piecewise-constant
  !>  per dof, such that we simple take the value of the closest
  !>  dof.
  !> @details
  !
  !> @param[in]    ctx_model_param : model parameter
  !> @param[in]    i_cell          : current cell
  !> @param[in]    xpt             : point to look for
  !> @param[out]   out_val         : output value
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine model_representation_dof_eval_refelem_solo(ctx_model_param,&
                                                i_cell,xpt,out_val,ctx_err)

    implicit none

    type(t_model_param)     ,intent(in)   :: ctx_model_param
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real(kind=8)            ,intent(in)   :: xpt(:)
    real(kind=8)            ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=RKIND_POL)      :: val_loc
    integer                   :: i_dof
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0
    out_val      = 0.d0

    !! evaluate using the basis function, here we directly have the 
    !! position on the reference element.
    do i_dof = 1, ctx_model_param%param_dof%ndof
      val_loc = 0.d0
      call polynomial_eval(ctx_model_param%param_dof%basis%pol(i_dof),  &
                           !! force kind of pol
                           real(xpt,kind=RKIND_POL),val_loc,ctx_err)
      out_val = out_val + dble(                                         &
                ctx_model_param%param_dof%mval(i_cell)%array(i_dof)*val_loc)
    end do

    return
  end subroutine model_representation_dof_eval_refelem_solo

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine evaluates the value at a given position for
  !>  the model dof representation. We assume piecewise-constant
  !>  per dof, such that we simple take the value of the closest
  !>  dof.
  !> @details
  !
  !> @param[in]    mesh            : mesh context
  !> @param[in]    ctx_model_param : model parameter
  !> @param[in]    i_cell          : current cell
  !> @param[in]    xpt             : point to look for
  !> @param[out]   out_val         : output value
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine model_representation_dof_eval_refelem_multi(               &
                                                ctx_model_param,i_cell, &
                                                xpt,npt,out_val,ctx_err)

    implicit none

    type(t_model_param)     ,intent(in)   :: ctx_model_param
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real(kind=8)            ,intent(in)   :: xpt(:,:)
    integer                 ,intent(in)   :: npt
    real(kind=8)            ,intent(out)  :: out_val(:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=RKIND_POL)      :: val_loc
    integer                   :: i_pt, i_dof
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0
    out_val      = 0.d0

    !! evaluate using the basis function, here we directly have the 
    !! position on the reference element.
    do i_dof = 1, ctx_model_param%param_dof%ndof
      do i_pt=1,npt
        val_loc = 0.d0
        call polynomial_eval(ctx_model_param%param_dof%basis%pol(i_dof),&
                             real(xpt(:,i_pt),kind=RKIND_POL),val_loc,ctx_err)
        out_val(i_pt) = out_val(i_pt) + dble(                           &
                  ctx_model_param%param_dof%mval(i_cell)%array(i_dof)*val_loc) 
      end do
    end do

    return
  end subroutine model_representation_dof_eval_refelem_multi

end module m_model_representation_dof
