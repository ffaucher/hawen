!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_parameter_evaluation.f90
!
!> @author
!> F. Faucher [Inria Bordeaux -- Team Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to evaluate a type model,
!> depending on the representation it has, e.g.
!> piecewise-constant or piecewise polynomial.
!>
!
!------------------------------------------------------------------------------
module m_model_parameter_evaluation

  !! module used -------------------------------------------------------
  use m_raise_error,                 only: raise_error, t_error
  use m_define_precision,            only: IKIND_MESH, RKIND_POL
  use m_polynomial,                  only: polynomial_eval
  use m_ctx_model_representation,    only: t_model_param,                 &
                                           model_representation_eval_sep, &   
                                           tag_MODEL_PPOLY, tag_MODEL_SEP,&
                                           tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_model_representation_dof,    only: model_representation_dof_eval_refelem
  !! -------------------------------------------------------------------

  implicit none

  private
  public  :: model_parameter_evaluation

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model parameter depending on the representation.
  !
  !> @param[in]    ctx_model    : model parameter context
  !> @param[in]    format_model : format of the representation
  !> @param[in]    dim_domain   : domain dimension
  !> @param[in]    i_cell       : which cell
  !> @param[in]    xpt_refelem  : position on the reference element
  !> @param[in]    xpt_gb       : position on the global    element
  !> @param[out]   v            : output value at the given position
  !> @param[inout] ctx_err      : context error
  !----------------------------------------------------------------------------
  subroutine model_parameter_evaluation(ctx_model_param,format_model,   &
                                        dim_domain, i_cell,xpt_refelem, &
                                        xpt_gb,v,ctx_err)

    implicit none

    type(t_model_param)     ,intent(in)   :: ctx_model_param
    character(len=*)        ,intent(in)   :: format_model
    integer                 ,intent(in)   :: dim_domain
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:)
    real   (kind=8)         ,intent(in)   :: xpt_gb     (:)
    real   (kind=8)         ,intent(out)  :: v
    type(t_error)           ,intent(inout):: ctx_err
    !!
    real  (kind=RKIND_POL)                :: mtemp
    real  (kind=RKIND_POL), allocatable   :: xloc(:)
    !! -----------------------------------------------------------------
    
    v = 0.d0

    !! 
    select case(trim(adjustl(format_model)))
      !! ---------------------------------------------------------------
      case(tag_MODEL_PCONSTANT) ! piecewise-constant -------------------
        v         = dble(ctx_model_param%pconstant(i_cell))

      case(tag_MODEL_PPOLY)    ! piecewise-polynomial ------------------
        allocate(xloc(dim_domain))
        xloc = real(xpt_refelem,kind=RKIND_POL) !! ref elem position. 
        call polynomial_eval(ctx_model_param%ppoly(i_cell),xloc,mtemp,ctx_err)
        v    = dble(mtemp)
        deallocate(xloc)

      case(tag_MODEL_SEP)      ! sep format ----------------------------
        !! using global position
        call model_representation_eval_sep(ctx_model_param%param_sep,   &
                                           dim_domain,xpt_gb,v,ctx_err)
    
      case(tag_MODEL_DOF)      ! dof format ----------------------------
        call model_representation_dof_eval_refelem(ctx_model_param,i_cell, &
                                                   xpt_refelem,v,ctx_err)
      case default
        ctx_err%msg   = "** ERROR:unrecognized parameterization"  //    &
                        " [model_eval_format] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    return
    
  end subroutine model_parameter_evaluation


end module m_model_parameter_evaluation
