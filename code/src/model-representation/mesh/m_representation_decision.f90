!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_representation_decision.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the decision for a given representation 
!> namely, we group element together to make patch where the specific 
!> representation will be applied.
!> For instance we can reduce the global number of unknown by
!>   - grouping similar vales 
!>   - using area of boxes
!> 
!
!------------------------------------------------------------------------------
module m_representation_decision

  use m_define_precision,      only : IKIND_MESH
  use m_raise_error,           only : raise_error, t_error
  use m_ctx_parallelism,       only : t_parallelism
  use m_reduce_sum,            only : reduce_sum
  use m_barrier,               only : barrier
  use m_broadcast,             only : broadcast
  use m_ctx_domain,            only : t_domain
  use m_project_array,         only : project_array_local_cell_to_global
  use m_mesh_gather_neigh,     only : mesh_gather_all_neigh

  implicit none

  private  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine defines the decision for model reduction
  !>  by grouping adjacent values
  interface decision_neigh
     module procedure dec_neigh_array1d_real8
  end interface
  !---------------------------------------------------------------------  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine defines the decision for model reduction
  !>  by grouping in a structured grid
  interface decision_structured
     module procedure dec_struct_array1d_real4
  end interface
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine recursively tries neighbors 
  interface recursive_test_neigh
     module procedure recursive_test_neigh_real8
     module procedure recursive_test_neigh_real4
  end interface
  !---------------------------------------------------------------------  

  public :: representation_decision_compression
    
  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine compute the decision according to the input key
  !> @details
  !
  !> @param[in]    paral           : parallelism context
  !> @param[in]    mesh            : mesh context
  !> @param[in]    str_method      : method to group elements
  !> @param[in]    reduction_tol   : between 0 and 1, how much we reduce.
  !> @param[in]    model_in(:)     : model, constant per element
  !> @param[inout] decision(:)     : output decision from local element to
  !>                                 sub-domain index
  !> @param[out]   n_subdomain_out : output number of sub-domain
  !> @param[in]    box_[xyz]       : size of the box for structured compress.
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine representation_decision_compression(ctx_paral,ctx_mesh,    &
                                     str_method,reduction_tol,model_in, &
                                     decision,n_subdomain_out,box_x,    &
                                     box_y,box_z,ctx_err)

    implicit none
    type(t_parallelism)                 ,intent(in)   :: ctx_paral
    type(t_domain)                      ,intent(in)   :: ctx_mesh
    character(len=*)                    ,intent(in)   :: str_method
    real                                ,intent(in)   :: reduction_tol
    real   (kind=8)         ,allocatable,intent(in)   :: model_in(:)
    integer(kind=IKIND_MESH),allocatable,intent(inout):: decision(:)
    integer(kind=IKIND_MESH)            ,intent(out)  :: n_subdomain_out
    real   (kind=4)                     ,intent(in)   :: box_x,box_y,box_z
    type(t_error)                       ,intent(out)  :: ctx_err
    !!
    ctx_err%ierr=0
    
    n_subdomain_out=0
    decision       =0

    !! -----------------------------------------------------------------
    !! Compute the decision array depending on the method
    !! -----------------------------------------------------------------
    select case(trim(adjustl(str_method)))
      !! just tests adjacent cells
      case('neighbors')
        call decision_neigh(ctx_paral,ctx_mesh,model_in,reduction_tol,  &
                            decision,n_subdomain_out,ctx_err)
      case('structured')
        call decision_structured(ctx_mesh,decision,box_x,box_y,box_z,   &
                                 n_subdomain_out,ctx_err)
      !! unknown selection
      case default
        ctx_err%msg   = "** ERROR: unknown decision format for the " // &
                        "representation [representation_decision] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return
  end subroutine representation_decision_compression


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine group adjacent values to reduce number of coeff.
  !> @details
  !
  !> @param[in]    mesh          : mesh context
  !> @param[inout] model         : model given by cell
  !> @param[in]    reduction     : between 0 and 1, how much we reduce.
  !> @param[out]   ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine dec_neigh_array1d_real8(ctx_paral,ctx_mesh,model,reduction,&
                                     decision,n_subdomain,ctx_err)

    implicit none

    type(t_parallelism)                   ,intent(in)    :: ctx_paral
    type(t_domain)                        ,intent(in)    :: ctx_mesh
    real(kind=8)              ,allocatable,intent(in)    :: model(:)
    real                                  ,intent(in)    :: reduction
    integer(kind=IKIND_MESH)  ,allocatable,intent(inout) :: decision(:)
    integer(kind=IKIND_MESH)              ,intent(out)   :: n_subdomain
    type(t_error)                         ,intent(out)   :: ctx_err
    !! local 
    integer(kind=IKIND_MESH)              :: n_target,ngroup
    integer                               :: iiter,niter
    integer(kind=IKIND_MESH)              :: n_cell_gb,ic,n_cell_loc
    integer(kind=IKIND_MESH)              :: ighost
    integer(kind=IKIND_MESH), allocatable :: cell_neigh_gb    (:,:)
    integer(kind=IKIND_MESH), allocatable :: decision_gb      (:)
    real   (kind=8)         , allocatable :: model_loc(:),model_gb(:)
    real   (kind=8)                       :: refval
    real   (kind=8)                       :: tol
    logical                               :: flag_ok
    !!
    ctx_err%ierr=0
    
    if(reduction > 1 .or. reduction <= 0) then
      ctx_err%msg   = "** ERROR: reduction coefficient must be "     // &
                      "between 0 and 1 [piecewise_constant_reduction] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    n_cell_gb = ctx_mesh%n_cell_gb
    n_cell_loc= ctx_mesh%n_cell_loc
    ighost    = n_cell_loc
    n_target  = nint(dble(n_cell_gb)*reduction)
    !! global  decision 
    allocate(decision_gb(n_cell_gb))
    decision_gb = 0
    !! -----------------------------------------------------------------
    !! 1) assemble onto master 
    !!    a) mesh neigbors infos
    !!    b) model
    !! -----------------------------------------------------------------
    !! mesh neighbors on master only 
    !! master only
    if(ctx_paral%master) then
      allocate(cell_neigh_gb(ctx_mesh%n_neigh_per_cell,n_cell_gb))
      allocate(model_gb (n_cell_gb))
    else
      allocate(cell_neigh_gb(1,1))
      allocate(model_gb     (1))
    endif  
    call mesh_gather_all_neigh(ctx_paral,ctx_mesh,cell_neigh_gb,.true.,ctx_err)

    !! global model representation
    allocate(model_loc(n_cell_gb))
    call project_array_local_cell_to_global(ctx_mesh,model,model_loc,ctx_err)   
    
    call reduce_sum(model_loc,model_gb,int(n_cell_gb),0,                &
                    ctx_paral%communicator,ctx_err)
    deallocate(model_loc)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 2) loop over cell and its neighbors, with an initial tolerance 
    !!    that is refined to reach the expected value
    !! -----------------------------------------------------------------
    if(ctx_paral%master) then
      niter  = 200   !! no more than 200 tries
      flag_ok=.false.
      iiter  = 1

      !! arbitrary initial value for tol. tol indicates how close values
      !! have to be to be in the same group, i.e., when it is small, 
      !! values have to be very close.
      tol = reduction/10.
      !! every cell has a number the associated of group, two cells 
      !! in the same group MUST be connected by a path
      !! ---------------------------------------------------------------
      !! loop until ok or maximum iteration reached
      do while(iiter <= niter .and. .not. flag_ok)
        ngroup      = 0
        decision_gb = 0
        do ic=1,n_cell_gb
          if(decision_gb(ic) <= 0) then !! only if not already done
            !! this is a new value
            ngroup          = ngroup + 1
            decision_gb(ic) = ngroup
            refval          = model_gb(ic)
            !! recursive check of all neighbors
            call recursive_test_neigh(cell_neigh_gb,model_gb,           &
                                      decision_gb,ic,ngroup,            &
                                      ctx_mesh%n_neigh_per_cell,        &
                                      refval,tol,ctx_err)
          end if
        end do

        !! do we have the right number, 5% is ok 
        if (ngroup >= 0.95*dble(n_target) .and.                         &
            ngroup <= 1.05*dble(n_target)) then
          flag_ok=.true.
        else
          !! too small or too low
          if(ngroup < n_target) then
            tol = tol*0.92  !! loosen tolerance
          else
            tol = tol*1.07  !! tighten tolerance
          end if 
        end if
        iiter = iiter + 1
      end do
      n_subdomain = ngroup
    end if
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! 3) send decision to everyone
    !! -----------------------------------------------------------------
    call barrier(ctx_paral%communicator,ctx_err)
    call broadcast(n_subdomain,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(decision_gb,int(n_cell_gb),0,ctx_paral%communicator,ctx_err)
    
    !! convert global decision to local decision -----------------------
    do ic=1,n_cell_loc
      decision(ic) = decision_gb(ctx_mesh%index_loctoglob_cell(ic))
    end do
    deallocate(decision_gb)
    !! -----------------------------------------------------------------
    deallocate(model_gb)
    deallocate(cell_neigh_gb)
    
    return
  end subroutine dec_neigh_array1d_real8


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine group in a structured decomposition
  !> @details
  !
  !> @param[in]    mesh          : mesh context
  !> @param[inout] decision      : decision array
  !> @param[in]    box_[xyz]     : size in x y z 
  !> @param[out]   n_subdomain   : final number of subdomains
  !> @param[out]   ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine dec_struct_array1d_real4(ctx_mesh,decision,box_x,box_y,    &
                                      box_z,n_subdomain,ctx_err)

    implicit none

    type(t_domain)                       ,intent(in)    :: ctx_mesh
    integer(kind=IKIND_MESH) ,allocatable,intent(inout) :: decision(:)
    real                                 ,intent(in)    :: box_x,box_y,box_z
    integer(kind=IKIND_MESH)             ,intent(out)   :: n_subdomain
    type(t_error)                        ,intent(out)   :: ctx_err
    !! local 
    integer                               :: igroup,ix,iy,iz,nx,ny,nz
    integer(kind=IKIND_MESH)              :: ic,n_cell_loc
    real(kind=8)                          :: bar_x,bar_y,bar_z
    !!
    ctx_err%ierr=0
    
    !! check with dimensions
    if(box_x <= 0.) then
      ctx_err%msg   = "** ERROR: size in X must be > 0 for structured"//&
                      " compression [piecewise_constant_reduction] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(box_z <= 0. .and. ctx_mesh%dim_domain > 1) then
      ctx_err%msg   = "** ERROR: size in Z must be > 0 for structured"//&
                      " compression [piecewise_constant_reduction] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(box_y <= 0. .and. ctx_mesh%dim_domain > 2) then
      ctx_err%msg   = "** ERROR: size in Y must be > 0 for structured"//&
                      " compression [piecewise_constant_reduction] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    
    !! decompose the global domain to see the total number of subdomains
    nx = ceiling(dble(ctx_mesh%bounds_xmax-ctx_mesh%bounds_xmin) / dble(box_x))
    ny = 1
    nz = 1 
    if(ctx_mesh%dim_domain > 1) &
    nz = ceiling(dble(ctx_mesh%bounds_zmax-ctx_mesh%bounds_zmin) / dble(box_z))
    if(ctx_mesh%dim_domain > 2) &
    ny = ceiling(dble(ctx_mesh%bounds_ymax-ctx_mesh%bounds_ymin) / dble(box_y))
    !! -----------------------------------------------------------------
    n_subdomain = nx*ny*nz
    
    !! create decision 
    n_cell_loc= ctx_mesh%n_cell_loc
    select case(ctx_mesh%dim_domain)
      case(1)
        do ic = 1, n_cell_loc
          !! depending on the barycenter      
          bar_x = dble(ctx_mesh%cell_bary(1,ic))
          !! corresponding ix iy iz for group number
          ix = 1 + nint(dble(bar_x-ctx_mesh%bounds_xmin)/dble(box_x))
          if(ix > nx) ix = nx !! in case of boundary position
          igroup = ix
          decision(ic) = igroup
        end do

      case(2)
        do ic = 1, n_cell_loc
          !! depending on the barycenter      
          bar_x = dble(ctx_mesh%cell_bary(1,ic))
          bar_z = dble(ctx_mesh%cell_bary(2,ic))
          !! corresponding ix iy iz for group number
          ix = 1 + nint(dble(bar_x-ctx_mesh%bounds_xmin)/dble(box_x))
          iz = 1 + nint(dble(bar_z-ctx_mesh%bounds_zmin)/dble(box_z))
          if(ix > nx) ix = nx !! in case of boundary position
          if(iz > nz) iz = nz !! in case of boundary position
          igroup = (ix-1)*nz + iz
          decision(ic) = igroup
        end do

      case(3)
        do ic = 1, n_cell_loc
          !! depending on the barycenter      
          bar_x = dble(ctx_mesh%cell_bary(1,ic))
          bar_y = dble(ctx_mesh%cell_bary(2,ic))
          bar_z = dble(ctx_mesh%cell_bary(3,ic))
          !! corresponding ix iy iz for group number
          ix = 1 + nint(dble(bar_x-ctx_mesh%bounds_xmin)/dble(box_x))
          iy = 1 + nint(dble(bar_y-ctx_mesh%bounds_ymin)/dble(box_y))
          iz = 1 + nint(dble(bar_z-ctx_mesh%bounds_zmin)/dble(box_z))
          if(ix > nx) ix = nx !! in case of boundary position
          if(iy > ny) iy = ny !! in case of boundary position
          if(iz > nz) iz = nz !! in case of boundary position
          igroup = (ix-1)*nz*ny + (iy-1)*nz + iz
          decision(ic) = igroup
        end do
      case default 
       ctx_err%msg   = "** ERROR: Incorrect dimension [piecws_cst_reduction] **"
       ctx_err%ierr  = -1
       ctx_err%critic=.true.
       call raise_error(ctx_err)
    end select

    return
  end subroutine dec_struct_array1d_real4
  
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine test values from neighbors to neighbors
  !> @details
  !
  !> @param[in]    cell_neigh      : list of cell neighbors
  !> @param[in]    model           : model given by cell
  !> @param[inout] decision        : decision array to group cell
  !> @param[in]    icell           : current cell
  !> @param[in]    igroup          : current group
  !> @param[in]    n_neigh_per_cell: number of neighbors
  !> @param[in]    refval          : reference value to test
  !> @param[in]    tol             : acceptable tolerance
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  recursive subroutine recursive_test_neigh_real4(cell_neigh,model,     &
                                            decision,icell,igroup,      &
                                            n_neigh_per_cell,refval,tol,&
                                            ctx_err)

    implicit none
    integer(kind=IKIND_MESH) ,allocatable,intent(in)    :: cell_neigh(:,:)
    real(kind=4)             ,allocatable,intent(in)    :: model(:)
    integer(kind=IKIND_MESH) ,allocatable,intent(inout) :: decision(:)
    integer(kind=IKIND_MESH)             ,intent(in)    :: icell,igroup
    integer                              ,intent(in)    :: n_neigh_per_cell
    real(kind=4)                         ,intent(in)    :: refval
    real(kind=4)                         ,intent(in)    :: tol
    type(t_error)                        ,intent(out)   :: ctx_err
    !! local 
    integer                               :: ineigh
    integer(kind=IKIND_MESH)              :: icell_neigh
    real   (kind=4)                       :: testval,test_tol
    !!
    ctx_err%ierr=0
  
    !! -----------------------------------------------------------------
    !! 1) we check all the direct neighbors
    !! -----------------------------------------------------------------
    do ineigh=1,n_neigh_per_cell
      !! check that it is not boundary
      if(cell_neigh(ineigh,icell) > 0) then 
        icell_neigh = cell_neigh(ineigh,icell)
        !! check that it does not already have a group attached
        if(decision(icell_neigh) <= 0 ) then
          !! test if it is in the current group or not 
          testval = model(icell_neigh)
          !! is it in the same group
          test_tol = abs(testval-refval)/min(refval,testval)
          if(test_tol <= tol) then
            decision(icell_neigh) = igroup
            !! ---------------------------------------------------------
            !! 2) we check the neighbors of the neighbors using 
            !!    recusrive call
            !! ---------------------------------------------------------
            call recursive_test_neigh(cell_neigh,model,decision,        &
                                      icell_neigh,igroup,               &
                                      n_neigh_per_cell,refval,tol,ctx_err)
          end if
        end if
      end if
    end do
   
    return
  end subroutine recursive_test_neigh_real4
  
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine test values from neighbors to neighbors
  !> @details
  !
  !> @param[in]    cell_neigh      : list of cell neighbors
  !> @param[in]    model           : model given by cell
  !> @param[inout] decision        : decision array to group cell
  !> @param[in]    icell           : current cell
  !> @param[in]    igroup          : current group
  !> @param[in]    n_neigh_per_cell: number of neighbors
  !> @param[in]    refval          : reference value to test
  !> @param[in]    tol             : acceptable tolerance
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  recursive subroutine recursive_test_neigh_real8(cell_neigh,model,     &
                                            decision,icell,igroup,      &
                                            n_neigh_per_cell,refval,tol,&
                                            ctx_err)

    implicit none
    integer(kind=IKIND_MESH) ,allocatable,intent(in)    :: cell_neigh(:,:)
    real(kind=8)             ,allocatable,intent(in)    :: model(:)
    integer(kind=IKIND_MESH) ,allocatable,intent(inout) :: decision(:)
    integer(kind=IKIND_MESH)             ,intent(in)    :: icell,igroup
    integer                              ,intent(in)    :: n_neigh_per_cell
    real(kind=8)                         ,intent(in)    :: refval
    real(kind=8)                         ,intent(in)    :: tol
    type(t_error)                        ,intent(out)   :: ctx_err
    !! local 
    integer                               :: ineigh
    integer(kind=IKIND_MESH)              :: icell_neigh
    real   (kind=8)                       :: testval,test_tol
    !!
    ctx_err%ierr=0
  
    !! -----------------------------------------------------------------
    !! 1) we check all the direct neighbors
    !! -----------------------------------------------------------------
    do ineigh=1,n_neigh_per_cell
      !! check that it is not boundary
      if(cell_neigh(ineigh,icell) > 0) then 
        icell_neigh = cell_neigh(ineigh,icell)
        !! check that it does not already have a group attached
        if(decision(icell_neigh) <= 0 ) then
          !! test if it is in the current group or not 
          testval = model(icell_neigh)
          !! is it in the same group
          test_tol = abs(testval-refval)/min(refval,testval)
          if(test_tol <= tol) then
            decision(icell_neigh) = igroup
            !! ---------------------------------------------------------
            !! 2) we check the neighbors of the neighbors using 
            !!    recusrive call
            !! ---------------------------------------------------------
            call recursive_test_neigh(cell_neigh,model,decision,        &
                                      icell_neigh,igroup,               &
                                      n_neigh_per_cell,refval,tol,ctx_err)
          end if
        end if
      end if
    end do
   
    return
  end subroutine recursive_test_neigh_real8

end module m_representation_decision
