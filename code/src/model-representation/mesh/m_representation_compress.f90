!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_representation_compress.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compress the input model from a decision array 
!> that indicates how the values must be grouped.
!
!------------------------------------------------------------------------------
module m_representation_compress

  use m_define_precision,     only : IKIND_MESH, RKIND_POL
  use m_raise_error,          only : raise_error, t_error
  use m_ctx_parallelism,      only : t_parallelism
  use m_allreduce_sum,        only : allreduce_sum
  !> for piecewise-polynomial
  use m_reduce_sum,           only : reduce_sum
  use m_barrier,              only : barrier
  use m_broadcast,            only : broadcast
  use m_ctx_domain,           only : t_domain
  use m_lapack_solve,         only : lapack_solve_system
  use m_array_types,          only : t_array2d_real,t_array1d_real,     &
                                     array_allocate,array_deallocate
  use m_polynomial,           only : t_polynomial,polynomial_construct, &
                                     polynomial_eval,polynomial_clean
  use m_dg_lagrange_dof_coordinates,                                    &
                              only : discretization_dof_coo_loc_elem,   &
                                     discretization_dof_coo_ref_elem
  use m_ctx_model_representation,                                       &
                              only: tag_MODEL_PPOLY, tag_MODEL_PCONSTANT

  implicit none

  private
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes model reduction using a decision array
  interface model_apply_compression_pconstant
     module procedure apply_compress_pconstant_1Dreal4
     module procedure apply_compress_pconstant_1Dreal8
  end interface
  !>  The subroutine computes model reduction using a decision array
  interface model_apply_compression_ppoly
     module procedure apply_compress_ppoly_1Dreal4
     module procedure apply_compress_ppoly_1Dreal8
  end interface
  !--------------------------------------------------------------------- 
  
  public :: model_apply_compression_pconstant
  public :: model_apply_compression_ppoly
   
  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine compute the model from decision representation
  !> @details
  !
  !> @param[in]    ctx_paral     : parallelism context
  !> @param[in]    str_method    : representation
  !> @param[in]    decision      : local cell number to global group
  !> @param[inout] model         : model values given by cell
  !> @param[in]    ngroup        : number of group
  !> @param[in]    ncell_loc     : number of cell loc
  !> @param[out]   ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine apply_compress_pconstant_1Dreal4(ctx_paral,str_method,     &
                                             decision,model,ngroup,     &
                                             n_cell_loc,ctx_err)

    implicit none
    type(t_parallelism)                 ,intent(in)    :: ctx_paral
    character(len=*)                    ,intent(in)    :: str_method
    integer(kind=IKIND_MESH),allocatable,intent(in)    :: decision(:)
    real(kind=4)            ,allocatable,intent(inout) :: model   (:)
    integer(kind=IKIND_MESH)            ,intent(in)    :: ngroup
    integer(kind=IKIND_MESH)            ,intent(in)    :: n_cell_loc
    type(t_error)                       ,intent(out)   :: ctx_err
    !!
    integer(kind=4),allocatable :: model_coeff_count(:),model_coeff_count_gb(:)
    real   (kind=4),allocatable :: model_coeff_sum  (:),model_coeff_sum_gb  (:)
    integer(kind=IKIND_MESH)    :: ic, igroup
    ctx_err%ierr=0
    
    !! depending on the method
    select case(trim(adjustl(str_method)))

      !! compute piecewise-constant reprensentation
      case(tag_MODEL_PCONSTANT)

        ! ---------------------------------------------------------
        !! 1) compute on each mpi proc, the global sum of all contribu.
        ! ---------------------------------------------------------
        allocate(model_coeff_sum  (ngroup))
        allocate(model_coeff_count(ngroup))
        model_coeff_sum   = 0.0
        model_coeff_count = 0
        do ic=1,n_cell_loc
          igroup = decision(ic)
          model_coeff_sum  (igroup) = model_coeff_sum  (igroup) + model(ic)
          model_coeff_count(igroup) = model_coeff_count(igroup) + 1
        end do
        
        ! ---------------------------------------------------------
        !! 2) assemble all mpi infos
        ! ---------------------------------------------------------
        allocate(model_coeff_sum_gb  (ngroup))
        allocate(model_coeff_count_gb(ngroup))
        call allreduce_sum(model_coeff_sum  ,model_coeff_sum_gb  ,      &
                           int(ngroup),ctx_paral%communicator,ctx_err)
        deallocate(model_coeff_sum)
        call allreduce_sum(model_coeff_count,model_coeff_count_gb,      &
                           int(ngroup),ctx_paral%communicator,ctx_err)
        deallocate(model_coeff_count)
        
        ! ---------------------------------------------------------
        !! 3) everyone gets its local value
        ! ---------------------------------------------------------
        model_coeff_sum_gb  = model_coeff_sum_gb  / model_coeff_count_gb
        do ic=1,n_cell_loc
          igroup = decision(ic)
          model(ic) = model_coeff_sum_gb(igroup)
        end do
        deallocate(model_coeff_sum_gb  )
        deallocate(model_coeff_count_gb)        
        
      !! unknown
      case default
        ctx_err%msg   = "** ERROR: unknown '"//trim(adjustl(str_method))//&
                        "' representation [representation_compute] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return
  end subroutine apply_compress_pconstant_1Dreal4


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine compute the model from decision representation
  !> @details
  !
  !> @param[in]    ctx_paral     : parallelism context
  !> @param[in]    str_method    : representation
  !> @param[in]    decision      : local cell number to global group
  !> @param[inout] model         : model values given by cell
  !> @param[in]    ngroup        : number of group
  !> @param[in]    ncell_loc     : number of cell loc
  !> @param[out]   ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine apply_compress_pconstant_1Dreal8(ctx_paral,str_method,     &
                                             decision,model,ngroup,     &
                                             n_cell_loc,ctx_err)

    implicit none
    type(t_parallelism)                 ,intent(in)    :: ctx_paral
    character(len=*)                    ,intent(in)    :: str_method
    integer(kind=IKIND_MESH),allocatable,intent(in)    :: decision(:)
    real(kind=8)            ,allocatable,intent(inout) :: model   (:)
    integer(kind=IKIND_MESH)            ,intent(in)    :: ngroup
    integer(kind=IKIND_MESH)            ,intent(in)    :: n_cell_loc
    type(t_error)                       ,intent(out)   :: ctx_err
    !!
    integer(kind=8),allocatable :: model_coeff_count(:),model_coeff_count_gb(:)
    real   (kind=8),allocatable :: model_coeff_sum  (:),model_coeff_sum_gb  (:)
    integer(kind=IKIND_MESH)    :: ic, igroup
    !!
    ctx_err%ierr=0
    
    !! depending on the method
    select case(trim(adjustl(str_method)))

      !! compute piecewise-constant reprensentation
      case(tag_MODEL_PCONSTANT)

        ! ---------------------------------------------------------
        !! 1) compute on each mpi proc, the global sum of all contribu.
        ! ---------------------------------------------------------
        allocate(model_coeff_sum  (ngroup))
        allocate(model_coeff_count(ngroup))
        model_coeff_sum   = 0.0
        model_coeff_count = 0
        do ic=1,n_cell_loc
          igroup = decision(ic)
          model_coeff_sum  (igroup) = model_coeff_sum  (igroup) + model(ic)
          model_coeff_count(igroup) = model_coeff_count(igroup) + 1
        end do
        
        ! ---------------------------------------------------------
        !! 2) assemble all mpi infos
        ! ---------------------------------------------------------
        allocate(model_coeff_sum_gb  (ngroup))
        allocate(model_coeff_count_gb(ngroup))
        call allreduce_sum(model_coeff_sum  ,model_coeff_sum_gb  ,      &
                           int(ngroup),ctx_paral%communicator,ctx_err)
        deallocate(model_coeff_sum)
        call allreduce_sum(model_coeff_count,model_coeff_count_gb,      &
                           int(ngroup),ctx_paral%communicator,ctx_err)
        deallocate(model_coeff_count)
        
        ! ---------------------------------------------------------
        !! 3) everyone gets its local value
        ! ---------------------------------------------------------
        model_coeff_sum_gb  = model_coeff_sum_gb  / model_coeff_count_gb
        do ic=1,n_cell_loc
          igroup = decision(ic)
          model(ic) = model_coeff_sum_gb(igroup)
        end do
        deallocate(model_coeff_sum_gb  )
        deallocate(model_coeff_count_gb)        
        
      !! unknown
      case default
        ctx_err%msg   = "** ERROR: unknown '"//trim(adjustl(str_method))//&
                        "' representation [representation_compute] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return
  end subroutine apply_compress_pconstant_1Dreal8


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine compute the model using a piecewise-polynomial
  !>  representation from the decision representation which gives the
  !>  sub-group.
  !> @details
  !
  !> @param[in]    ctx_paral     : parallelism context
  !> @param[in]    ctx_mesh      : mesh context
  !> @param[in]    str_method    : representation
  !> @param[in]    decision      : local cell number to global group
  !> @param[in]    ngroup        : number of group
  !> @param[in]    p_order       : order of polynomials
  !> @param[inout] model         : model values given by cell
  !> @param[inout] ppoly         : polynomial on each cell.
  !> @param[out]   ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine apply_compress_ppoly_1Dreal8(ctx_paral,ctx_mesh,str_method,&
                                          mformat,decision,ngroup,      &
                                          p_order,model,ppoly,mmin,mmax,&
                                          ctx_err)

    implicit none
    type(t_parallelism)                 ,intent(in)    :: ctx_paral
    type(t_domain)                      ,intent(in)    :: ctx_mesh
    character(len=*)                    ,intent(in)    :: str_method
    character(len=*)                    ,intent(in)    :: mformat
    integer(kind=IKIND_MESH),allocatable,intent(in)    :: decision(:)
    integer(kind=IKIND_MESH)            ,intent(in)    :: ngroup
    integer                             ,intent(in)    :: p_order
    type(t_polynomial),allocatable      ,intent(inout) :: ppoly(:)
    real(kind=8)            ,allocatable,intent(inout) :: model(:)
    real(kind=8)                        ,intent(in)    :: mmin,mmax
    type(t_error)                       ,intent(out)   :: ctx_err
    !! for mpi communication assembling
    integer                              :: n, ndof_vol, j, k
    integer(kind=IKIND_MESH)             :: i_cell, i_cell_gb, i_group
    integer(kind=IKIND_MESH)             :: i_c
    integer(kind=4)                      :: ncell
    integer(kind=IKIND_MESH),allocatable :: cell_group(:),cell_count(:)
    real   (kind=8)         ,allocatable :: cell_bary (:,:),cell_val(:) 
    integer(kind=IKIND_MESH),allocatable :: cell_group_gb(:),cell_count_gb(:)
    integer(kind=IKIND_MESH),allocatable :: counter(:)
    real   (kind=8)         ,allocatable :: cell_bary_gb(:,:),cell_val_gb(:) 
    !! linear system & poly
    type(t_array2d_real), allocatable  :: matA(:)
    type(t_array1d_real), allocatable  :: matB(:)
    real(kind=8)        , allocatable  :: sol (:)
    type(t_polynomial)                 :: poly_temp
    type(t_polynomial)  , allocatable  :: ppoly_group(:)
    real(kind=RKIND_POL)               :: myval
    logical                            :: flag_adj
    !! then on cells
    real(kind=8)        , allocatable :: matAc(:,:),matBc(:),solc(:)
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=RKIND_POL), allocatable :: xptref(:,:)
    
    ctx_err%ierr=0
    
    !! sanity check depending on the method
    if(trim(adjustl(str_method)) .ne. tag_MODEL_PPOLY) then
      ctx_err%msg   = "** ERROR: '"//trim(adjustl(str_method))//&
                      "' representation is inconsistant "     //&
                      "[representation_compute] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! -----------------------------------------------------------------
    !! 0. 
    !! if we have a constant model, or order 0 Polynomial, then we 
    !! simply respect the piecewise-constant compression
    !! -----------------------------------------------------------------
    if(trim(adjustl(mformat)) .eq. 'constant' .or. p_order <= 0) then

      if(p_order < 0) then !! order < 0 ==> error
        ctx_err%ierr=-1
        ctx_err%msg   = "** ERROR: polynomial order for the " //      &
                        "model must be > 0 [model_read_ppoly] **"
        ctx_err%critic=.true.
        return !! non-shared error 
      end if
      
      do i_cell = 1, ctx_mesh%n_cell_loc
        call polynomial_construct(ctx_mesh%dim_domain,p_order,        &
                                  ppoly(i_cell),ctx_err)
        !! zero except the first to the constant 
        ppoly(i_cell)%coeff_pol(:)=0.0
        ppoly(i_cell)%coeff_pol(1)=real(model(i_cell), kind=RKIND_POL)

        !! adjust with min and max 
        if(ppoly(i_cell)%coeff_pol(1)<mmin) ppoly(i_cell)%coeff_pol(1)=mmin
        if(ppoly(i_cell)%coeff_pol(1)>mmax) ppoly(i_cell)%coeff_pol(1)=mmax
      end do

    !! -----------------------------------------------------------------
    !! model which varies
    else
    !! -----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! 1) To assemble the polynomial on each group of cells, we need
      !!    - the barycenter coordinates of every cell
      !!    - the value of the paramter on every cell 
      !!    - the sub-group in which the current cell belongs to.
      ! ----------------------------------------------------------------
      !! out of simplicity, the master will take all of these information.
      ! ----------------------------------------------------------------
      allocate(cell_bary (ctx_mesh%dim_domain,ctx_mesh%n_cell_gb))
      allocate(cell_val  (ctx_mesh%n_cell_gb))
      allocate(cell_group(ctx_mesh%n_cell_gb))
      allocate(cell_count(ngroup))
      cell_bary  = 0.d0
      cell_val   = 0.d0
      cell_group = 0
      cell_count = 0
      
      !! each proc fills his infos
      do i_cell = 1, ctx_mesh%n_cell_loc
        i_cell_gb = ctx_mesh%index_loctoglob_cell(i_cell)
        cell_bary (:,i_cell_gb)=dble(ctx_mesh%cell_bary(:,i_cell))
        cell_val    (i_cell_gb)=dble(model(i_cell))
        cell_group  (i_cell_gb)=decision(i_cell) 
        cell_count  (decision(i_cell)) = cell_count(decision(i_cell)) + 1
      end do
  
      !! only master takes it all from here on
      if(ctx_paral%master) then
        allocate(cell_bary_gb (ctx_mesh%dim_domain,ctx_mesh%n_cell_gb))
        allocate(cell_val_gb  (ctx_mesh%n_cell_gb))
        allocate(cell_group_gb(ctx_mesh%n_cell_gb))
        allocate(cell_count_gb(ctx_mesh%n_cell_gb))
      else
        allocate(cell_bary_gb (1,1))
        allocate(cell_val_gb  (1))
        allocate(cell_group_gb(1))
        allocate(cell_count_gb(1))
      end if 
      !! reduce so that only master keeps the result
      n = int(ctx_mesh%dim_domain*ctx_mesh%n_cell_gb)
      call reduce_sum(cell_bary,cell_bary_gb,n,0,ctx_paral%communicator,ctx_err)
      n = int(ctx_mesh%n_cell_gb)
      call reduce_sum(cell_val,cell_val_gb    ,n,0,ctx_paral%communicator,ctx_err)
      call reduce_sum(cell_group,cell_group_gb,n,0,ctx_paral%communicator,ctx_err)
      n = int(ngroup)
      call reduce_sum(cell_count,cell_count_gb,n,0,ctx_paral%communicator,ctx_err)
  
      deallocate(cell_bary)
      deallocate(cell_val )
      deallocate(cell_group)
      deallocate(cell_count)
  
      ! ----------------------------------------------------------------
      !!
      !! 2) Master creates a polynomial for each of the subgroup.
      !! 
      !! for all cell we want the over-determined system made of
      !! e.g. in order 1 in 2D
      !! for all cell in group
      !!   \alpha (bary_x) + \beta (bary_z) + \gamma = val_cell
      !! end
      !! ==> [(n_cell_in_group) x (ndof_vol)) [X] = [B]
      ! ----------------------------------------------------------------
      if(ctx_paral%master) then
        !! nfreedom depends on the input order.
        !!  to build the linear system
        call polynomial_construct(ctx_mesh%dim_domain,p_order,poly_temp,ctx_err)
        ndof_vol = poly_temp%size_coeff
  
        !! loop over all group for allocation
        allocate(matA(ngroup))
        allocate(matB(ngroup))
        do i_group = 1, ngroup
          ncell = int(cell_count_gb(i_group),kind=4)
          !! over-determined system.
          call array_allocate(matA(i_group),ncell,ndof_vol,ctx_err)
          call array_allocate(matB(i_group),ncell,ctx_err)
        end do
      
        !! loop over all cells to assemble the linear systems.
        allocate(counter(ngroup))
        counter = 0
        do i_cell = 1, ctx_mesh%n_cell_gb
          i_group          = cell_group_gb(i_cell)
          counter(i_group) = counter(i_group) + 1
  
          !! matrix A given from the polynomial inherent structures, that 
          !! we evaluate at the barycenter position.
          i_c  = counter(i_group)
          do j = 1, ndof_vol
            poly_temp%coeff_pol(:) = 0.0 
            poly_temp%coeff_pol(j) = 1.0 !! 1 position only
            myval = 0.d0
            call polynomial_eval(poly_temp,                             &
                       real(cell_bary_gb(:,i_cell),kind=RKIND_POL),     &
                       myval,ctx_err)
            matA(i_group)%array(i_c,j) = dble(myval)
          end do
          
          !! adjust with min/max values
          matB(i_group)%array(i_c) = cell_val_gb(i_cell)
          if(cell_val_gb(i_cell) < mmin) matB(i_group)%array(i_c) = mmin
          if(cell_val_gb(i_cell) > mmax) matB(i_group)%array(i_c) = mmax
        end do
        
        deallocate(counter)
        
        ! ---------------------------------------------------------
        !! 3. we solve all linear systems
        ! ---------------------------------------------------------
        allocate(ppoly_group(ngroup))
        allocate(sol(ndof_vol))
        !! loop over all group for allocation
        flag_adj = .false.
        do i_group = 1, ngroup
          !! create empty polynomial
          call polynomial_construct(ctx_mesh%dim_domain,p_order,          &
                                    ppoly_group(i_group),ctx_err)
        
        
          !! if it is constant, we can move to the next cell.
          if( (maxval(matB(i_group)%array) - minval(matB(i_group)%array)) &
               < tiny(1.0) ) then
            !! zero except the first to the constant 
            ppoly_group(i_group)%coeff_pol(:)=0.0
            !! using the constant value
            ppoly_group(i_group)%coeff_pol(1)=real(matB(i_group)%array(1),&
                                                   kind=RKIND_POL)
          else
            !! solve the over-determined dense linear system
            ncell = int(cell_count_gb(i_group),kind=4)
            sol = 0.d0
            if(ncell >= ndof_vol) then
              call lapack_solve_system(dble(matA(i_group)%array),       &
                                       dble(matB(i_group)%array),ncell, &
                                       ndof_vol,flag_adj,sol,ctx_err)
            else !! adjust to the number of unknowns ?
              call lapack_solve_system(                                 &
                          dble(matA(i_group)%array(1:ncell,1:ncell)),   &
                          dble(matB(i_group)%array(1:ncell)),           &
                          ncell,ncell,flag_adj,sol,ctx_err)
            
            end if
            ppoly_group(i_group)%coeff_pol(:) = real(sol,kind=RKIND_POL)
          endif
          !! clean this group mat
          call array_deallocate(matA(i_group))
          call array_deallocate(matB(i_group))
        end do
        deallocate(sol)
        deallocate(matA)
        deallocate(matB)
        call polynomial_clean(poly_temp)
  
      else   !! allocate for broadcast ---------------------------------
        allocate(ppoly_group(ngroup))
        do i_group = 1, ngroup
          !! create empty polynomial
          call polynomial_construct(ctx_mesh%dim_domain,p_order,          &
                                    ppoly_group(i_group),ctx_err)
        end do
        ndof_vol = ppoly_group(1)%size_coeff
      end if !! end if master ------------------------------------------
      
      deallocate(cell_bary_gb )
      deallocate(cell_val_gb  )
      deallocate(cell_group_gb)
      deallocate(cell_count_gb)
      call barrier(ctx_paral%communicator,ctx_err)
      
      !! broadcast to everyone
      do i_group = 1, ngroup
        call broadcast(ppoly_group(i_group)%coeff_pol(:),ndof_vol,0,      &
                       ctx_paral%communicator,ctx_err)
      end do
  
  
      ! ----------------------------------------------------------------
      !!
      !!
      !! 4) The polynomial on the group is transfered towards each
      !!    cell, respecting the order.
      !!
      !!
      ! ----------------------------------------------------------------
      call polynomial_construct(ctx_mesh%dim_domain,p_order,poly_temp,ctx_err)
      ndof_vol = poly_temp%size_coeff
      allocate(matAc(ndof_vol,ndof_vol))
      allocate(matBc(ndof_vol))
      allocate(solc (ndof_vol))
      allocate(xptref (ctx_mesh%dim_domain,ndof_vol))
      allocate(dof_coo(ctx_mesh%dim_domain,ndof_vol))
      
      !! the A matrix is the same for everyone 
      !! value  of the model at each of the dof, using ref. elem coo
      call discretization_dof_coo_ref_elem(ctx_mesh,p_order,ndof_vol,     &
                                           ctx_mesh%dim_domain,dof_coo,   &
                                           ctx_err)
      xptref = real(dof_coo,kind=RKIND_POL)
      do j = 1, ndof_vol
        poly_temp%coeff_pol(:) = 0.0 ; poly_temp%coeff_pol(j) = 1.0
        !! matrix A given from the polynomial inherent structures
        do k = 1, ndof_vol
          !! evaluate the polynomial, the dimension selection is 
          !! dealt with in the subroutine directly
          myval = 0.d0
          call polynomial_eval(poly_temp,xptref(:,k),myval,ctx_err)
          matAc(k,j) = dble(myval)
        end do
      end do
      call polynomial_clean(poly_temp)
      !! ---------------------------------------------------------------
      !! barycenter coordinates for the reference element is: 
      xptref = 0.d0
      xptref(:,1) = 1.d0 / dble(ctx_mesh%dim_domain+1)

      do i_cell = 1, ctx_mesh%n_cell_loc
        matBc = 0.d0
        call polynomial_construct(ctx_mesh%dim_domain,p_order,          &
                                  ppoly(i_cell),ctx_err)
  
        !! *********************************************************
        !! position of the dof on the current element for the model
        !! values and before it was on the reference element for the matrix.
        !! *********************************************************
        call discretization_dof_coo_loc_elem(ctx_mesh,i_cell,p_order,   &
                                             ndof_vol,dof_coo,ctx_err)
  
        ! -----------------------------------------------------
        !! the rhs is given by the model values.
        !! ---------------------------------------------------------
        i_group = decision(i_cell)
        do j = 1, ndof_vol
          call polynomial_eval(ppoly_group(i_group),                    &
                               real(dof_coo(:,j),kind=RKIND_POL),myval,ctx_err)
          matBc(j) = dble(myval)
        end do
  
        !! if it is constant, we can move to the next cell.
        if( (maxval(matBc) - minval(matBc)) < tiny(1.0) ) then
          !! zero except the first to the constant 
          ppoly(i_cell)%coeff_pol(:)=0.0
          ppoly(i_cell)%coeff_pol(1)=real(matBc(1),kind=RKIND_POL)
          cycle
        end if 
        ! ---------------------------------------------------------
  
        !! solve the dense linear system
        call lapack_solve_system(matAc,matBc,ndof_vol,ndof_vol,         &
                                .false.,solc,ctx_err)
        !! adjust
        ppoly(i_cell)%coeff_pol(:) = real(solc,kind=RKIND_POL)

        call polynomial_eval(ppoly(i_cell),xptref(:,1),myval,ctx_err)
        model(i_cell) = dble(myval)
        ! ---------------------------------------------------------
      end do
       
      deallocate(matAc  )
      deallocate(matBc  )
      deallocate(solc   )
      deallocate(dof_coo) 
      deallocate(xptref )

    !! -----------------------------------------------------------------
    end if !! end if model varies per cell and order > 0
    !! -----------------------------------------------------------------

    return
  end subroutine apply_compress_ppoly_1Dreal8


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine compute the model using a piecewise-polynomial
  !>  representation from the decision representation which gives the
  !>  sub-group.
  !> @details
  !
  !> @param[in]    ctx_paral     : parallelism context
  !> @param[in]    ctx_mesh      : mesh context
  !> @param[in]    str_method    : representation
  !> @param[in]    decision      : local cell number to global group
  !> @param[in]    ngroup        : number of group
  !> @param[in]    p_order       : order of polynomials
  !> @param[inout] model         : model values given by cell
  !> @param[inout] ppoly         : polynomial on each cell.
  !> @param[out]   ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine apply_compress_ppoly_1Dreal4(ctx_paral,ctx_mesh,str_method,&
                                          mformat,decision,ngroup,      &
                                          p_order,model,ppoly,mmin,mmax,&
                                          ctx_err)

    implicit none
    type(t_parallelism)                 ,intent(in)    :: ctx_paral
    type(t_domain)                      ,intent(in)    :: ctx_mesh
    character(len=*)                    ,intent(in)    :: str_method
    character(len=*)                    ,intent(in)    :: mformat
    integer(kind=IKIND_MESH),allocatable,intent(in)    :: decision(:)
    integer(kind=IKIND_MESH)            ,intent(in)    :: ngroup
    integer                             ,intent(in)    :: p_order
    type(t_polynomial),allocatable      ,intent(inout) :: ppoly(:)
    real(kind=4)            ,allocatable,intent(inout) :: model(:)
    real(kind=4)                        ,intent(in)    :: mmin,mmax
    type(t_error)                       ,intent(out)   :: ctx_err
    !! for mpi communication assembling
    integer                              :: n, ndof_vol, j, k
    integer(kind=IKIND_MESH)             :: i_cell, i_cell_gb, i_group
    integer(kind=IKIND_MESH)             :: i_c
    integer(kind=4)                      :: ncell
    integer(kind=IKIND_MESH),allocatable :: cell_group(:),cell_count(:)
    real   (kind=8)         ,allocatable :: cell_bary (:,:),cell_val(:) 
    integer(kind=IKIND_MESH),allocatable :: cell_group_gb(:),cell_count_gb(:)
    integer(kind=IKIND_MESH),allocatable :: counter(:)
    real   (kind=8)         ,allocatable :: cell_bary_gb(:,:),cell_val_gb(:) 
    !! linear system & poly
    type(t_array2d_real), allocatable  :: matA(:)
    type(t_array1d_real), allocatable  :: matB(:)
    real(kind=8)        , allocatable  :: sol (:)
    type(t_polynomial)                 :: poly_temp
    type(t_polynomial)  , allocatable  :: ppoly_group(:)
    real(kind=RKIND_POL)               :: myval
    logical                            :: flag_adj
    !! then on cells
    real(kind=8)        , allocatable :: matAc(:,:),matBc(:),solc(:)
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=RKIND_POL), allocatable :: xptref(:,:)
    
    ctx_err%ierr=0
    
    !! sanity check depending on the method
    if(trim(adjustl(str_method)) .ne. tag_MODEL_PPOLY) then
      ctx_err%msg   = "** ERROR: '"//trim(adjustl(str_method))//&
                      "' representation is inconsistant "     //&
                      "[representation_compute] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! -----------------------------------------------------------------
    !! 0. 
    !! if we have a constant model, or order 0 Polynomial, then we 
    !! simply respect the piecewise-constant compression
    !! -----------------------------------------------------------------
    if(trim(adjustl(mformat)) .eq. 'constant' .or. p_order <= 0) then

      if(p_order < 0) then !! order < 0 ==> error
        ctx_err%ierr=-1
        ctx_err%msg   = "** ERROR: polynomial order for the " //      &
                        "model must be > 0 [model_read_ppoly] **"
        ctx_err%critic=.true.
        return !! non-shared error 
      end if
      
      do i_cell = 1, ctx_mesh%n_cell_loc
        call polynomial_construct(ctx_mesh%dim_domain,p_order,        &
                                  ppoly(i_cell),ctx_err)
        !! zero except the first to the constant 
        ppoly(i_cell)%coeff_pol(:)=0.0
        ppoly(i_cell)%coeff_pol(1)=real(model(i_cell), kind=RKIND_POL)

        !! adjust with min and max 
        if(ppoly(i_cell)%coeff_pol(1)<mmin) ppoly(i_cell)%coeff_pol(1)=mmin
        if(ppoly(i_cell)%coeff_pol(1)>mmax) ppoly(i_cell)%coeff_pol(1)=mmax
      end do

    !! -----------------------------------------------------------------
    !! model which varies
    else
    !! -----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! 1) To assemble the polynomial on each group of cells, we need
      !!    - the barycenter coordinates of every cell
      !!    - the value of the paramter on every cell 
      !!    - the sub-group in which the current cell belongs to.
      ! ----------------------------------------------------------------
      !! out of simplicity, the master will take all of these information.
      ! ----------------------------------------------------------------
      allocate(cell_bary (ctx_mesh%dim_domain,ctx_mesh%n_cell_gb))
      allocate(cell_val  (ctx_mesh%n_cell_gb))
      allocate(cell_group(ctx_mesh%n_cell_gb))
      allocate(cell_count(ngroup))
      cell_bary  = 0.d0
      cell_val   = 0.d0
      cell_group = 0
      cell_count = 0
      
      !! each proc fills his infos
      do i_cell = 1, ctx_mesh%n_cell_loc
        i_cell_gb = ctx_mesh%index_loctoglob_cell(i_cell)
        cell_bary (:,i_cell_gb)=dble(ctx_mesh%cell_bary(:,i_cell))
        cell_val    (i_cell_gb)=dble(model(i_cell))
        cell_group  (i_cell_gb)=decision(i_cell) 
        cell_count  (decision(i_cell)) = cell_count(decision(i_cell)) + 1
      end do
  
      !! only master takes it all from here on
      if(ctx_paral%master) then
        allocate(cell_bary_gb (ctx_mesh%dim_domain,ctx_mesh%n_cell_gb))
        allocate(cell_val_gb  (ctx_mesh%n_cell_gb))
        allocate(cell_group_gb(ctx_mesh%n_cell_gb))
        allocate(cell_count_gb(ctx_mesh%n_cell_gb))
      else
        allocate(cell_bary_gb (1,1))
        allocate(cell_val_gb  (1))
        allocate(cell_group_gb(1))
        allocate(cell_count_gb(1))
      end if 
      !! reduce so that only master keeps the result
      n = int(ctx_mesh%dim_domain*ctx_mesh%n_cell_gb)
      call reduce_sum(cell_bary,cell_bary_gb,n,0,ctx_paral%communicator,ctx_err)
      n = int(ctx_mesh%n_cell_gb)
      call reduce_sum(cell_val,cell_val_gb    ,n,0,ctx_paral%communicator,ctx_err)
      call reduce_sum(cell_group,cell_group_gb,n,0,ctx_paral%communicator,ctx_err)
      n = int(ngroup)
      call reduce_sum(cell_count,cell_count_gb,n,0,ctx_paral%communicator,ctx_err)
  
      deallocate(cell_bary)
      deallocate(cell_val )
      deallocate(cell_group)
      deallocate(cell_count)
  
      ! ----------------------------------------------------------------
      !!
      !! 2) Master creates a polynomial for each of the subgroup.
      !! 
      !! for all cell we want the over-determined system made of
      !! e.g. in order 1 in 2D
      !! for all cell in group
      !!   \alpha (bary_x) + \beta (bary_z) + \gamma = val_cell
      !! end
      !! ==> [(n_cell_in_group) x (ndof_vol)) [X] = [B]
      ! ----------------------------------------------------------------
      if(ctx_paral%master) then
        !! nfreedom depends on the input order.
        !!  to build the linear system
        call polynomial_construct(ctx_mesh%dim_domain,p_order,poly_temp,ctx_err)
        ndof_vol = poly_temp%size_coeff
  
        !! loop over all group for allocation
        allocate(matA(ngroup))
        allocate(matB(ngroup))
        do i_group = 1, ngroup
          ncell = int(cell_count_gb(i_group),kind=4)
          !! over-determined system.
          call array_allocate(matA(i_group),ncell,ndof_vol,ctx_err)
          call array_allocate(matB(i_group),ncell,ctx_err)
        end do
      
        !! loop over all cells to assemble the linear systems.
        allocate(counter(ngroup))
        counter = 0
        do i_cell = 1, ctx_mesh%n_cell_gb
          i_group          = cell_group_gb(i_cell)
          counter(i_group) = counter(i_group) + 1
  
          !! matrix A given from the polynomial inherent structures, that 
          !! we evaluate at the barycenter position.
          i_c  = counter(i_group)
          do j = 1, ndof_vol
            poly_temp%coeff_pol(:) = 0.0 
            poly_temp%coeff_pol(j) = 1.0 !! 1 position only
            myval = 0.d0
            call polynomial_eval(poly_temp,                             &
                 real(cell_bary_gb(:,i_cell),kind=RKIND_POL),           &
                 myval,ctx_err)
            matA(i_group)%array(i_c,j) = dble(myval)
          end do
          
          !! adjust with min/max values
          matB(i_group)%array(i_c) = cell_val_gb(i_cell)
          if(cell_val_gb(i_cell) < mmin) matB(i_group)%array(i_c) = mmin
          if(cell_val_gb(i_cell) > mmax) matB(i_group)%array(i_c) = mmax
        end do
        
        deallocate(counter)
        
        ! ---------------------------------------------------------
        !! 3. we solve all linear systems
        ! ---------------------------------------------------------
        allocate(ppoly_group(ngroup))
        allocate(sol(ndof_vol))
        !! loop over all group for allocation
        flag_adj = .false.
        do i_group = 1, ngroup
          !! create empty polynomial
          call polynomial_construct(ctx_mesh%dim_domain,p_order,          &
                                    ppoly_group(i_group),ctx_err)
        
        
          !! if it is constant, we can move to the next cell.
          if( (maxval(matB(i_group)%array) - minval(matB(i_group)%array)) &
               < tiny(1.0) ) then
            !! zero except the first to the constant 
            ppoly_group(i_group)%coeff_pol(:)=0.0
            !! using the constant value
            ppoly_group(i_group)%coeff_pol(1)=real(matB(i_group)%array(1),&
                                                   kind=RKIND_POL)
          else
            !! solve the over-determined dense linear system
            ncell = int(cell_count_gb(i_group),kind=4)
            sol = 0.d0
            if(ncell >= ndof_vol) then
              !! force double
              call lapack_solve_system(dble(matA(i_group)%array),       &
                                       dble(matB(i_group)%array),ncell, &
                                       ndof_vol,flag_adj,sol,ctx_err)
            else !! adjust to the number of unknowns ?
              call lapack_solve_system(                                 &
                          dble(matA(i_group)%array(1:ncell,1:ncell)),   &
                          dble(matB(i_group)%array(1:ncell)),           &
                          ncell,ncell,flag_adj,sol,ctx_err)
            
            end if
            ppoly_group(i_group)%coeff_pol(:) = real(sol,kind=RKIND_POL)
          endif
          !! clean this group mat
          call array_deallocate(matA(i_group))
          call array_deallocate(matB(i_group))
        end do
        deallocate(sol)
        deallocate(matA)
        deallocate(matB)
        call polynomial_clean(poly_temp)
  
      else   !! allocate for broadcast ---------------------------------
        allocate(ppoly_group(ngroup))
        do i_group = 1, ngroup
          !! create empty polynomial
          call polynomial_construct(ctx_mesh%dim_domain,p_order,          &
                                    ppoly_group(i_group),ctx_err)
        end do
        ndof_vol = ppoly_group(1)%size_coeff
      end if !! end if master ------------------------------------------
      
      deallocate(cell_bary_gb )
      deallocate(cell_val_gb  )
      deallocate(cell_group_gb)
      deallocate(cell_count_gb)
      call barrier(ctx_paral%communicator,ctx_err)
      
      !! broadcast to everyone
      do i_group = 1, ngroup
        call broadcast(ppoly_group(i_group)%coeff_pol(:),ndof_vol,0,      &
                       ctx_paral%communicator,ctx_err)
      end do
  
  
      ! ----------------------------------------------------------------
      !!
      !!
      !! 4) The polynomial on the group is transfered towards each
      !!    cell, respecting the order.
      !!
      !!
      ! ----------------------------------------------------------------
      call polynomial_construct(ctx_mesh%dim_domain,p_order,poly_temp,ctx_err)
      ndof_vol = poly_temp%size_coeff
      allocate(matAc(ndof_vol,ndof_vol))
      allocate(matBc(ndof_vol))
      allocate(solc (ndof_vol))
      allocate(xptref (ctx_mesh%dim_domain,ndof_vol))
      allocate(dof_coo(ctx_mesh%dim_domain,ndof_vol))
      
      !! the A matrix is the same for everyone 
      !! value  of the model at each of the dof, using ref. elem coo
      call discretization_dof_coo_ref_elem(ctx_mesh,p_order,ndof_vol,     &
                                           ctx_mesh%dim_domain,dof_coo,   &
                                           ctx_err)
      xptref = real(dof_coo,kind=RKIND_POL)
      do j = 1, ndof_vol
        poly_temp%coeff_pol(:) = 0.0 ; poly_temp%coeff_pol(j) = 1.0
        !! matrix A given from the polynomial inherent structures
        do k = 1, ndof_vol
          !! evaluate the polynomial, the dimension selection is 
          !! dealt with in the subroutine directly
          myval = 0.d0
          call polynomial_eval(poly_temp,xptref(:,k),myval,ctx_err)
          matAc(k,j) = dble(myval)
        end do
      end do
      call polynomial_clean(poly_temp)
      !! ---------------------------------------------------------------
      !! barycenter coordinates for the reference element is: 
      xptref = 0.d0
      xptref(:,1) = 1.d0 / dble(ctx_mesh%dim_domain+1)

      do i_cell = 1, ctx_mesh%n_cell_loc
        matBc = 0.d0
        call polynomial_construct(ctx_mesh%dim_domain,p_order,          &
                                  ppoly(i_cell),ctx_err)
  
        !! *********************************************************
        !! position of the dof on the current element for the model
        !! values and before it was on the reference element for the matrix.
        !! *********************************************************
        call discretization_dof_coo_loc_elem(ctx_mesh,i_cell,p_order,   &
                                             ndof_vol,dof_coo,ctx_err)
  
        ! -----------------------------------------------------
        !! the rhs is given by the model values.
        !! ---------------------------------------------------------
        i_group = decision(i_cell)
        do j = 1, ndof_vol
          call polynomial_eval(ppoly_group(i_group),                    &
                               real(dof_coo(:,j),kind=RKIND_POL),       &
                               myval,ctx_err)
          matBc(j) = dble(myval)
        end do
  
        !! if it is constant, we can move to the next cell.
        if( (maxval(matBc) - minval(matBc)) < tiny(1.0) ) then
          !! zero except the first to the constant 
          ppoly(i_cell)%coeff_pol(:)=0.0
          ppoly(i_cell)%coeff_pol(1)=real(matBc(1),kind=RKIND_POL)
          cycle
        end if 
        ! ---------------------------------------------------------
  
        !! solve the dense linear system
        call lapack_solve_system(matAc,matBc,ndof_vol,ndof_vol,         &
                                .false.,solc,ctx_err)
        !! adjust
        ppoly(i_cell)%coeff_pol(:) = real(solc,kind=RKIND_POL)

        call polynomial_eval(ppoly(i_cell),xptref(:,1),myval,ctx_err)
        model(i_cell) = real(myval)
        ! ---------------------------------------------------------
      end do
       
      deallocate(matAc  )
      deallocate(matBc  )
      deallocate(solc   )
      deallocate(dof_coo) 
      deallocate(xptref )

    !! -----------------------------------------------------------------
    end if !! end if model varies per cell and order > 0
    !! -----------------------------------------------------------------

    return
  end subroutine apply_compress_ppoly_1Dreal4
end module m_representation_compress
