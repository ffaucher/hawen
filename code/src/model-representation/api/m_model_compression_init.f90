!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_representation_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module initializes the model representation e.g.,
!>  -> piecewise constant
!>  -> piecewise linear
!>  -> splines
!
!------------------------------------------------------------------------------
module m_model_compression_init

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_ctx_parallelism,          only: t_parallelism
  use m_ctx_domain,               only: t_domain
  use m_ctx_model_representation, only: t_model_param, tag_MODEL_PCONSTANT
  use m_representation_decision,  only: representation_decision_compression
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: model_compression_init
 
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> initialize model representation by computing the "decision" that
  !> maps every local element to its sub-domain
  !
  !> @param[in]    paral             : parallelism context
  !> @param[in]    domain            : domain context
  !> @param[inout] ctx_model_param   : representation context
  !> @param[in]    str_representation: representation keyword
  !> @param[in]    model_in(:)       : model, constant per element
  !> @param[in]    mformat           : format of the model, cannot work with cst
  !> @param[out]   ctx_err           : error context
  !----------------------------------------------------------------------------
  subroutine model_compression_init(ctx_paral,ctx_domain,ctx_model_param, &
                                    str_representation,model_in,mformat,  &
                                    ctx_err)
    implicit none
    type(t_parallelism)      ,intent(in)    :: ctx_paral
    type(t_domain)           ,intent(in)    :: ctx_domain
    type(t_model_param)      ,intent(inout) :: ctx_model_param
    character(len=*)         ,intent(in)    :: str_representation
    real(kind=8),allocatable ,intent(inout) :: model_in(:)
    character(len=*)         ,intent(in)    :: mformat
    type(t_error)            ,intent(inout) :: ctx_err
    !!
   
    allocate(ctx_model_param%decision(ctx_domain%n_cell_loc))
    ctx_model_param%decision = 0

    !! -----------------------------------------------------------------
    !! Formalism for compression 
    !! -----------------------------------------------------------------
    if(.not. ctx_model_param%compression_flag) then
      ! no compression
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: inconsistant call to compression_init " //&
                   "[model_compression_init] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    else

      select case(trim(adjustl(str_representation)))
        case(tag_MODEL_PCONSTANT)
          !! the method can be per neighbors or per structured/box
          select case(ctx_model_param%compression_str)
            !! NEIGHBORS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            case('neigh','NEIGH','adjacent','neighbors','NEIGHBORS','ADJACENT')

              !! cannot compress if compression, except if box
              if(mformat == 'constant') then
                ctx_err%ierr=-1
                ctx_err%msg ="** ERROR: compression cannot work with model " // &
                             "given as a constant [model_representation_init] "
                ctx_err%critic=.true.
                call raise_error(ctx_err)
              end if

              ctx_model_param%compression_str = 'neighbors'
              if(ctx_model_param%compression_tol >= 1 .or.              &
                 ctx_model_param%compression_tol < 0) then
                ctx_err%ierr=-1
                ctx_err%msg ="** ERROR: compression coeff must be <1 and >0 " //    &
                             "[model_compression_init] "
                ctx_err%critic=.true.
                call raise_error(ctx_err)
              end if
            
            !! STRUCTURED >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            case('box','BOX','structured','STRUCTURED')
              ctx_model_param%compression_str = 'structured'
            
            !! UNKNOWN >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            case default
              ctx_err%ierr=-1
              ctx_err%msg ="** ERROR: unrecognized model compression '"// &
                           trim(adjustl(ctx_model_param%compression_str))   //   &
                           "' for representation [model_compression_init]"
              ctx_err%critic=.true.
              call raise_error(ctx_err)
          end select
  
        case default
          ctx_err%ierr=-1
          ctx_err%msg ="** ERROR: unrecognized model representation " //  &
                       "[model_compression_init] "
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select   
    end if
    
    
    !! -----------------------------------------------------------------
    !! compute the decision
    !! -----------------------------------------------------------------
    select case(trim(adjustl(str_representation)))
      case(tag_MODEL_PCONSTANT)
        call representation_decision_compression(ctx_paral,ctx_domain,  &
                            trim(adjustl(ctx_model_param%compression_str)),&
                            ctx_model_param%compression_tol,            &
                            model_in,                                   &
                            ctx_model_param%decision,                   &
                            ctx_model_param%n_subdomain_gb,             &
                            ctx_model_param%compression_box_x,          &
                            ctx_model_param%compression_box_y,          &
                            ctx_model_param%compression_box_z,ctx_err)
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** ERROR: model representation not supported " // &
                     "for compression [model_compression_init] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select   
    
    return
  end subroutine model_compression_init

end module m_model_compression_init
