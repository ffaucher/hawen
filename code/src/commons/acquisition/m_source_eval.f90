!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_source_eval.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the source coefficient
!
!------------------------------------------------------------------------------
module m_source_eval
  
  use m_raise_error,       only : raise_error, t_error
  use m_ctx_parallelism,   only : t_parallelism
  use m_ctx_acquisition,   only : t_acquisition

  implicit none

  private  
  public :: source_eval
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> compute the source according to Ricker Order 2 wavelet
  !
  !> @param[inout] ctx_aq        : acquisition context contains the source infos
  !> @param[in]    frequency     : required for the fourier transform
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine source_eval(ctx_aq,freq,ctx_err)
    type(t_acquisition),intent(inout):: ctx_aq
    complex(kind=8)    ,intent(in)   :: freq
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    integer                     :: nt=10000,k
    real   (kind=8)             :: tol,tmax,dt
    real   (kind=8),parameter   :: pi=4.d0*datan(1.d0)
    real   (kind=8)             :: t
    real   (kind=8),allocatable :: wavelet(:)
    complex(kind=8)             :: coeff,cval

    ctx_err%ierr = 0
    
    tol=tiny(1.0)  ! should be 1.e-38 in simple precision

    if(ctx_aq%src_info%flag_constant) then
      !! if the source is forced, the coefficient is already here
    else
      !! compute the ricker source
      !! maximal time is given when exp(-pi² fpeak² tmax²) < tol
      !! here, tmax=(tmax-tpeak); so this is the time after the peak
      tmax = dble(dsqrt(-log(tol)/(pi**2. * ctx_aq%src_info%freqpeak**2)))
      tmax = tmax + ctx_aq%src_info%timepeak
      dt   = real((tmax - ctx_aq%src_info%time0) / real(nt-1))
      allocate(wavelet(nt))
      wavelet = 0.0
      coeff=(pi**2)*(ctx_aq%src_info%freqpeak**2)
      do k=1,nt
        t = ctx_aq%src_info%time0 + (k-1)*dt - ctx_aq%src_info%timepeak
        wavelet(k) = dble((1.0 - 2.0*coeff*(t**2)) * exp(-coeff*(t**2)))
      enddo

      !! compute the Fourier transform
      cval = cmplx(0.,0.)
      do k=1,nt
        t = ctx_aq%src_info%time0 + (k-1)*dt !! - ctx_aq%src_info%timepeak
        !! the code takes the convention 
        !!           dt ==> - i \omega, 
        !! meaning that the Fourier transform is 
        !!    F(\omega) = \int f(t) exp(i \omega t).
        coeff=exp(freq * t)
        cval = cval + wavelet(k)*coeff
      enddo
      !! integral discretization 
      cval = cval * dt 
      !! normalize to avoid too small values ?
      !! cval = cval / abs(cval)
      !! scaling parameter
      ctx_aq%src_info%current_val = dcmplx(cval*ctx_aq%src_info%amplitude)
      deallocate(wavelet)
    end if

    return
  end subroutine source_eval

end module m_source_eval
