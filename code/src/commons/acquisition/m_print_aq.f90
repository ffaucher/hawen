!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_aq.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for acquisition
!
!------------------------------------------------------------------------------
module m_print_aq

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_ctx_acquisition,  only : t_acquisition
  use m_print_basics,     only : separator,indicator,cast_memory
  implicit none
  
  private
  public :: print_info_aq_init,print_info_aq,print_info_aq_comp
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the acquisition
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   srcfile       : source file
  !> @param[in]   rcvfile       : receiver file
  !> @param[in]   srcformat     : source format
  !> @param[in]   nsrc_gb       : total number of sources
  !> @param[in]   nsrc_loc      : number of sources  used
  !> @param[in]   nrcv          : number of receiver per src
  !> @param[in]   flag_same_rcv : indicates if receivers are fixed
  !> @param[in]   mem_gb        : global memory for the acquisition
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_aq_init(ctx_paral,srcfile,rcvfile,srcformat,    &
                                nsrc_gb,nsrc_loc,nrcv,flag_same_rcv,    &
                                mem_gb,ctx_err)

    type(t_parallelism) , intent(in)   :: ctx_paral
    character(len=*)    , intent(in)   :: srcfile,rcvfile,srcformat
    integer             , intent(in)   :: nsrc_gb,nsrc_loc
    integer, allocatable, intent(in)   :: nrcv(:)
    logical             , intent(in)   :: flag_same_rcv
    integer(kind=8)     , intent(in)   :: mem_gb
    type(t_error)       , intent(out)  :: ctx_err
    !!
    character(len=64) :: str_mem
    character(len=7)  :: frmt='(a,i10)'
    
    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      call cast_memory(mem_gb,str_mem)
      write(6,'(a)')    separator
      write(6,'(2a)')   indicator, " Acquisition information "
      write(6,'(2a)') "- source file                : ",trim(adjustl(srcfile))
      write(6,frmt)   "- number of sources in file  : ",nsrc_gb
      write(6,frmt)   "- number of sources used     : ",nsrc_loc
      if(flag_same_rcv) then
        if(nrcv(1) > 0) then
          write(6,'(a)')  "- the receivers are fixed (same for all sources)"
          write(6,'(2a)') "- receiver file              : ",trim(rcvfile)
          write(6,frmt)   "- number of receivers in file: ",nrcv(1)
        else
          write(6,'(a)')  "- no receivers in the acquisition "
        end if
      else
        if(minval(nrcv) > 0) then
          write(6,'(a)')  "- the receivers change for each source"
          write(6,'(2a)') "- receiver base file name    : ",trim(rcvfile)
          write(6,frmt)   "- number of receivers min    : ",minval(nrcv)
          write(6,frmt)   "- number of receivers max    : ",maxval(nrcv)
        else
          write(6,'(a)')  "- no receivers in the acquisition "
        end if
      end if  
      
      write(6,'(2a)') "- source characterization    : ",trim(adjustl(srcformat))
      write(6,'(2a)') "- memory for acquisition/core: ",trim(adjustl(str_mem))
    endif
    return
  end subroutine print_info_aq_init

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print acquisition details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   ctx_aq        : acquisition context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_aq(ctx_paral,ctx_aq,ctx_err)

    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_acquisition),intent(in)   :: ctx_aq
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    integer                          :: k,j
    
    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      write(6,'(2a)')   indicator, " Acquisition sources details "
      write(6,'(a,1i9)') "- total n_sources in aq. file: ",ctx_aq%nsrc_gb
      write(6,'(a,1i9)') "- number of sources used     : ",ctx_aq%nsrc_loc
      write(6,'(a,3i6)') "- [first / last] index       : ", &
                         ctx_aq%isrc_gb_start,ctx_aq%isrc_gb_end
      write(6,'(a,3i6)') "- index step                 : ", ctx_aq%isrc_step
      write(6,'(a)')    " --------------------------------------------------------------"
      write(6,'(a)')    " Index | Npoint |     X     |     Y      |     Z     |  Nrcv   "
      write(6,'(a)')    " --------------------------------------------------------------"
      do k=1,ctx_aq%nsrc_loc
        if(ctx_aq%sources(k)%nsim.eq.1) then
          write(6,'(2i6,a,3es13.4,i8)') k, 1, "  ", ctx_aq%sources(k)%x(1), &
                                                    ctx_aq%sources(k)%y(1), &
                                                    ctx_aq%sources(k)%z(1), &
                                                    ctx_aq%sources(k)%rcv%nrcv
        else
          write(6,'(2i6,a,i12)') k, ctx_aq%sources(k)%nsim,                 &
                                 "                                   ",     &
                                 ctx_aq%sources(k)%rcv%nrcv

          do j=1,ctx_aq%sources(k)%nsim
            write(6,'(a,3es13.4)') "             |",ctx_aq%sources(k)%x(j), &
                                                    ctx_aq%sources(k)%y(j), &
                                                    ctx_aq%sources(k)%z(j)
          enddo
        endif
      enddo
      
      write(6,'(a)')    separator
    endif
    return
  end subroutine print_info_aq

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print acquisition details for sources and receivers components
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   ctx_aq        : acquisition context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_aq_comp(ctx_paral,ctx_aq,ctx_err)

    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_acquisition),intent(in)   :: ctx_aq
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    integer                          :: k
    character(len=512)               :: str
    
    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      write(6,'(a,i4,a)') "- sources are on   ",ctx_aq%ncomponent_src,    &
                          " components: "
      str="    - "// trim(adjustl(ctx_aq%src_component(1)))
      do k=2, ctx_aq%ncomponent_src
        str = trim(str) // ", "// trim(adjustl(ctx_aq%src_component(k)))
      enddo
      write(6,'(a)') trim(str)
      
      !if(ctx_aq%ncomponent_rcv > 0) then
      !  write(6,'(a,i4,a)') "- receivers record ", ctx_aq%ncomponent_rcv, &
      !                      " components: " 
      !  str="    - "// trim(adjustl(ctx_aq%rcv_component(1)))
      !  do k=2, ctx_aq%ncomponent_rcv
      !    str = trim(str) // ", "// trim(adjustl(ctx_aq%rcv_component(k)))
      !  enddo
      !  write(6,'(a)') trim(str)
      !else
      !  write(6,'(a)') "- NO COMPONENT RECORDED BY RECEIVER" 
      !endif
    
    endif
    return
  end subroutine print_info_aq_comp
  
end module m_print_aq
