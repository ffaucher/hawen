!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
! MODULE        : m_acquisition_init
! AFFILIATION   : Faculty of Mathematics, University of Vienna
!> @author
!> F. Faucher
!
! DESCRIPTION:
!> the module is used to read the acquisition: sources and receivers location
!
!------------------------------------------------------------------------------
module m_acquisition_init
  
  use m_raise_error,       only : raise_error, t_error
  use m_ctx_parallelism,   only : t_parallelism
  use m_ctx_acquisition,   only : t_acquisition, SRC_DIRAC, SRC_PW,     & 
                                  SRC_DIRAC_DERIV, SRC_DIRAC_DIV,       &
                                  SRC_DIRAC_DERIVX, SRC_DIRAC_DERIVY,   &
                                  SRC_DIRAC_DERIVZ, SRC_ANALYTIC, SRC_GAUSSIAN
  use m_tag_namefield,     only : field_tag_formalism
  use m_read_parameters_aq,only : read_parameters_aq,                   &
                                  read_parameters_aq_src_charact
  use m_print_aq,          only : print_info_aq_init,print_info_aq,     &
                                  print_info_aq_comp
  implicit none

  private  
  public :: acquisition_init
  private:: acquisition_set
    
  contains

  !---------------------------------------------------------------------------
  !> @author F. Faucher
  !> @brief
  !> init acquisition information and allocation
  !
  !> @param[in]   ctx_paral     : parallelism context
  !> @param[out]  ctx_aq        : acquisition context
  !> @param[in]   parameter_file : parameter file
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------------
  subroutine acquisition_init(ctx_paral,ctx_aq,parameter_file,ctx_err)
    
    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_acquisition),intent(out)  :: ctx_aq
    character(len=*)   ,intent(in)   :: parameter_file
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    integer                        :: src_ncomp,isrc_gb_start
    integer                        :: isrc_gb_end,isrc_step
    character(len=512)             :: srcfile,rcvfile,srcformat
    character(len=512)             :: rcvfile_n,normethod
    character(len=512),allocatable :: src_comp(:)
    logical                        :: verb_aq,src_depth_force
    logical                        :: rcv_depth_force,flag_same_rcv
    real                           :: src_depth_val,rcv_depth_val

    ctx_err%ierr = 0

    call read_parameters_aq(ctx_paral,parameter_file,srcfile,rcvfile,   &
                            flag_same_rcv,rcvfile_n,                    &
                            srcformat,src_comp,src_ncomp,isrc_gb_start, &
                            isrc_gb_end,isrc_step,src_depth_force,      &
                            src_depth_val,rcv_depth_force,              &
                            rcv_depth_val,normethod,                    &
                            verb_aq,ctx_err)
    
    call acquisition_set(ctx_paral,ctx_aq,srcfile,rcvfile,flag_same_rcv,&
                         rcvfile_n,                                     &
                         srcformat,src_comp,src_ncomp,isrc_gb_start,    &
                         isrc_gb_end,isrc_step,src_depth_force,         &
                         src_depth_val,rcv_depth_force,rcv_depth_val,   &
                         normethod,verb_aq,ctx_err)
    call print_info_aq_comp(ctx_paral,ctx_aq,ctx_err)
    if(ctx_aq%verbose) call print_info_aq(ctx_paral,ctx_aq,ctx_err)

    ctx_aq%src_info%gaussian_variance = 0.d0
    call read_parameters_aq_src_charact(parameter_file,ctx_aq%src_info%time0, &
                         ctx_aq%src_info%timepeak,ctx_aq%src_info%freqpeak,   &
                         ctx_aq%src_info%amplitude,                           &
                         ctx_aq%src_info%flag_constant,                       &
                         ctx_aq%src_info%current_val,                         &
                         ctx_aq%src_info%gaussian_variance,ctx_err)

    return
  end subroutine acquisition_init
  
  !---------------------------------------------------------------------------
  !> @author F. Faucher
  !> @brief
  !> The subroutine reads the acquisition informations: sources and 
  !> receivers from file given in the input parameter
  !> @param[in]   ctx_paral       : parallel context
  !> @param[out]  mesh            : acquisition context
  !> @param[in]   file_src        : file for sources
  !> @param[in]   file_rcv        : file for receivers
  !> @param[in]   flag_same_rcv   : indicates if the rcv are fixed 
  !> @param[in]   file_rcv_n      : file for receivers normal (may be empty)
  !> @param[in]   srcformat       : source format
  !> @param[in]   indexes         : isrc_gb_start,isrc_gb_end,isrc_step
  !> @param[in]   src_depth_force : flag to know if force source depth
  !> @param[in]   rcv_depth_force : flag to know if force rcv depth
  !> @param[in]   src_depth_val   : force source depth val
  !> @param[in]   rcv_depth_val   : force rcv depth val
  !> @param[in]   verbose         : verbose flag
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine acquisition_set(ctx_paral,ctx_aq,file_src,file_rcv,        &
                             flag_same_rcv,file_rcv_n,srcformat,        &
                             src_comp,src_ncomp,isrc_gb_start,          &
                             isrc_gb_end,isrc_step,src_depth_force,     &
                             src_depth_val,rcv_depth_force,             &
                             rcv_depth_val,normethod,verbose,ctx_err)

    implicit none

    type(t_parallelism)          ,intent(in)   :: ctx_paral
    type(t_acquisition)          ,intent(out)  :: ctx_aq
    character(len=*)             ,intent(in)   :: file_src
    character(len=*)             ,intent(in)   :: file_rcv
    logical                      ,intent(in)   :: flag_same_rcv
    character(len=*)             ,intent(in)   :: file_rcv_n
    character(len=*)             ,intent(in)   :: srcformat
    character(len=*)             ,intent(in)   :: normethod
    character(len=*),allocatable, intent(in)   :: src_comp(:)
    integer                      ,intent(in)   :: src_ncomp
    integer                      ,intent(in)   :: isrc_gb_start,isrc_gb_end
    integer                      ,intent(in)   :: isrc_step
    logical                      ,intent(in)   :: src_depth_force
    logical                      ,intent(in)   :: rcv_depth_force
    real                         ,intent(in)   :: src_depth_val
    real                         ,intent(in)   :: rcv_depth_val
    logical                      ,intent(in)   :: verbose
    type(t_error)                ,intent(out)  :: ctx_err
    !! local    
    integer                :: j,k,cline,nsim,counter,nrcv_gb
    integer, parameter     :: unit_src=120, unit_rcv=121, unit_rcv_n=122
    integer                :: int_size = 4,real_size = 4
    integer, allocatable   :: nrcv(:)
    real   (kind=8)        :: src_loc(30720) !! 3*10240 maximum
    real   (kind=8)        :: planewave_dir(3)
    real   (kind=8)        :: planewave_norm   
    integer(kind=8)        :: mem_count
    logical                :: file_exists
    character(len=512)     :: file_rcv_loc, stamp_src, file_rcv_n_loc
    
    ctx_err%ierr  = 0
    ctx_aq%src_file=trim(adjustl(file_src))
    ctx_aq%rcv_file=trim(adjustl(file_rcv))
    ctx_aq%verbose =verbose
    
    !! we first deal with the source infos
    inquire(file=trim(adjustl(file_src)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** INPUT SOURCE FILE " // trim(adjustl(file_src)) // &
                     " DOEST NOT EXIST"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    !! dirac, gaussian source     
    select case(trim(adjustl(srcformat)))
      case('dirac','Dirac','DIRAC','delta-dirac','DELTA-DIRAC','Delta-Dirac')
        ctx_aq%src_type = SRC_DIRAC

      case('dirac-deriv','dirac-derivative','Dirac-Derivative',         &
           'DIRAC-DERIVATIVE','delta-dirac-derivative','DELTA-DIRAC-DERIVATIVE')
        ctx_aq%src_type = SRC_DIRAC_DERIV

      case('dirac-deriv_x','dirac-derivative_x','Dirac-Derivative_x',   &
           'DIRAC-DERIVATIVE_x','delta-dirac-derivative_x',             &
           'DELTA-DIRAC-DERIVATIVE_x')
        ctx_aq%src_type = SRC_DIRAC_DERIVX
      case('dirac-deriv_y','dirac-derivative_y','Dirac-Derivative_y',   &
           'DIRAC-DERIVATIVE_y','delta-dirac-derivative_y',             &
           'DELTA-DIRAC-DERIVATIVE_y')
        ctx_aq%src_type = SRC_DIRAC_DERIVY
      case('dirac-deriv_z','dirac-derivative_z','Dirac-Derivative_z',   &
           'DIRAC-DERIVATIVE_z','delta-dirac-derivative_z',             &
           'DELTA-DIRAC-DERIVATIVE_z')
        ctx_aq%src_type = SRC_DIRAC_DERIVZ

      case('dirac-div','dirac-divergence','Dirac-Div','Dirac-Divegence',&
           'DIRAC-DIV','delta-dirac-divergence',                        &
           'DELTA-DIRAC-DIVERGENCE','delta-dirac-div','DELTA-DIRAC-DIV')
        ctx_aq%src_type = SRC_DIRAC_DIV
      
      case('gaussian','Gaussian','GAUSSIAN')
        ctx_aq%src_type = SRC_GAUSSIAN

      case('plane-wave','planewave','PLANEWAVE','Planewave','Plane-Wave')
        ctx_aq%src_type = SRC_PW
      case('analytic','ANALYTIC','Analytic')
        ctx_aq%src_type = SRC_ANALYTIC
      case default
        ctx_err%msg   = "** ERROR: the source format "//                &
                        trim(adjustl(srcformat))//" is not " //         &
                        "recognized, 'dirac' is the most common one"//  &
                        " [acquisition_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    !! Open files
    open (unit=unit_src,file=trim(adjustl(ctx_aq%src_file)))
    !! Read number of sources and receivers per source,
    read(unit_src,*) ctx_aq%nsrc_gb
    !! check from first and last 
    ctx_aq%isrc_gb_start = isrc_gb_start
    ctx_aq%isrc_gb_end   = min(isrc_gb_end,ctx_aq%nsrc_gb)
    ctx_aq%isrc_step     = isrc_step
    if(ctx_aq%isrc_gb_start > ctx_aq%nsrc_gb) then
      ctx_err%msg   = "** ERROR: The first source to be considered is " // &
                      " higher than the total number of source [aq_set] **" 
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! count actual number of source
    ctx_aq%nsrc_loc = floor(real(ctx_aq%isrc_gb_end-ctx_aq%isrc_gb_start)/ &
                                 ctx_aq%isrc_step) + 1
    !! -----------------------------------------------------------------

    !! receivers per source --------------------------------------------
    ctx_aq%flag_same_rcv = flag_same_rcv
    allocate(nrcv(ctx_aq%nsrc_loc))
    nrcv = 0
        
    !! receivers infos may be empty
    if(trim(adjustl(file_rcv)) == '') then
      ctx_aq%flag_rcv=.false.
      nrcv = 0
    else
      ctx_aq%flag_rcv=.true.
      
      !! per source of global ------------------------------------------
      if(ctx_aq%flag_same_rcv) then 
        inquire(file=trim(adjustl(file_rcv)),exist=file_exists)
        if(.not. file_exists) then
          ctx_err%ierr=-1
          ctx_err%msg ="** INPUT RECEIVER FILE " // trim(adjustl(file_rcv)) // &
                         " DOEST NOT EXIST"
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        endif
        !! Open files
        open (unit=unit_rcv,file=trim(adjustl(ctx_aq%rcv_file)))
        !! here, we assume the same number of receivers per source
        read(unit_rcv,*) nrcv(1)
        do k=2,ctx_aq%nsrc_loc
          nrcv(k) = nrcv(1)
        end do
      
      !! per source  ---------------------------------------------------
      else
        j = 0
        do k=ctx_aq%isrc_gb_start,ctx_aq%isrc_gb_end,ctx_aq%isrc_step
          j = j + 1
          !! file given by file_rcv // source number
          write(stamp_src,'(i0)') k
          file_rcv_loc = trim(trim(adjustl(file_rcv))//trim(stamp_src))
          inquire(file=trim(adjustl(file_rcv_loc)),exist=file_exists)
          if(.not. file_exists) then
            ctx_err%ierr=-1
            ctx_err%msg ="** INPUT RECEIVER FILE " //                   &
                         trim(adjustl(file_rcv_loc)) // " DOEST NOT EXIST"
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          endif
          !! Open files
          open (unit=unit_rcv,file=trim(adjustl(file_rcv_loc)))
          !! here, we assume the same number of receivers per source
          read(unit_rcv,*) nrcv(j)
          close(unit_rcv)
        end do
        
      end if
      ! ----------------------------------------------------------------
      
    end if
    nrcv_gb = sum(nrcv)
    ! ------------------------------------------------------------------
    
    !! anticipate memory requirement
    mem_count=0
    mem_count = 7*int_size                        + &  !! 7 integers parameters
               (2*256 + 64)                       + &  !! characters
               !! 3 doubles + 1 integer per source
               (ctx_aq%nsrc_loc)*(3*(2*real_size) + int_size) + &
               !! 3 double for coo   | 
               !!+3 reals per normals| receivers are per sources 
               !!+1 integer          | 
               (nrcv_gb*7*(2*real_size) + int_size)           + &
               (src_ncomp)*64         + & !! src components
               (2*int_size)           !! int8 for memory
    ctx_aq%memory_gb=mem_count
    !! print init infos before allocating
    call print_info_aq_init(ctx_paral,ctx_aq%src_file,ctx_aq%rcv_file,  &
                            srcformat,ctx_aq%nsrc_gb,                   &
                            ctx_aq%nsrc_loc,nrcv,ctx_aq%flag_same_rcv,  &
                            ctx_aq%memory_gb,ctx_err)
    
    !! allocate and read infos source ----------------------------------
    allocate(ctx_aq%sources  (ctx_aq%nsrc_loc))
    do k=1,ctx_aq%nsrc_loc
      ctx_aq%sources(k)%rcv%nrcv     = nrcv(k)
      if(nrcv(k) > 0) then
        allocate(ctx_aq%sources(k)%rcv%x(nrcv(k)))
        allocate(ctx_aq%sources(k)%rcv%y(nrcv(k)))
        allocate(ctx_aq%sources(k)%rcv%z(nrcv(k)))
        allocate(ctx_aq%sources(k)%rcv%nx(nrcv(k)))
        allocate(ctx_aq%sources(k)%rcv%ny(nrcv(k)))
        allocate(ctx_aq%sources(k)%rcv%nz(nrcv(k)))
      end if
    enddo

   
    !! 1) first we read in case of multi-point source
    !! -----------------------------------------------------------------
    counter=0
    do k=1,ctx_aq%nsrc_gb
      read(unit_src,*) cline, nsim
      if(k >= ctx_aq%isrc_gb_start .and. k <= ctx_aq%isrc_gb_end) then
        if( mod(k-ctx_aq%isrc_gb_start,ctx_aq%isrc_step) .eq. 0) then
          counter=counter+1
          ctx_aq%sources(counter)%nsim=nsim
          allocate(ctx_aq%sources(counter)%x(nsim))
          allocate(ctx_aq%sources(counter)%y(nsim))
          allocate(ctx_aq%sources(counter)%z(nsim))

        end if
      end if
    enddo
    close(unit_src)
    !! 2) get infos for coordinates
    !! -----------------------------------------------------------------
    !! source
    counter=0
    open (unit=unit_src,file=trim(adjustl(ctx_aq%src_file)))
    read(unit_src,*) cline
    do k=1,ctx_aq%nsrc_gb
      
      if(k >= ctx_aq%isrc_gb_start .and. k <= ctx_aq%isrc_gb_end) then
        if( mod(k-ctx_aq%isrc_gb_start,ctx_aq%isrc_step) .eq. 0) then
          counter=counter+1
          read(unit_src,*) cline, nsim, src_loc(1:3*ctx_aq%sources(counter)%nsim)
          do j=1,nsim
            ctx_aq%sources(counter)%x(j)=dble(src_loc(3*(j-1)+1))
            ctx_aq%sources(counter)%y(j)=dble(src_loc(3*(j-1)+2))
            if(src_depth_force) then
              ctx_aq%sources(counter)%z(j)=dble(src_depth_val)
            else
              ctx_aq%sources(counter)%z(j)=dble(src_loc(3*(j-1)+3))
            end if
            
            !! for plane wave source, we must normalize the direction
            if(ctx_aq%src_type .eq. SRC_PW) then
              planewave_dir=[ctx_aq%sources(counter)%x(j),              &
                             ctx_aq%sources(counter)%y(j),              &
                             ctx_aq%sources(counter)%z(j)]
              planewave_norm = norm2(planewave_dir)
              if(planewave_norm < tiny(1.0)) then
                ctx_err%ierr=-1
                ctx_err%msg ="** ERROR: the plane-wave source has a "// &
                             "direction 0 [acquisition_init]**"
                ctx_err%critic=.true.
                call raise_error(ctx_err)
              end if
              ctx_aq%sources(counter)%x(j) = ctx_aq%sources(counter)%x(j)/&
                                             planewave_norm
              ctx_aq%sources(counter)%y(j) = ctx_aq%sources(counter)%y(j)/&
                                             planewave_norm
              ctx_aq%sources(counter)%z(j) = ctx_aq%sources(counter)%z(j)/&
                                             planewave_norm
            end if
            !! ---------------------------------------------------------
          enddo
        else
          read(unit_src,*) cline, nsim
        end if
      else
        read(unit_src,*) cline, nsim
      end if

    end do
    close(unit_src)
    !! -----------------------------------------------------------------
    
    !! only if there are receivers
    if(ctx_aq%flag_rcv) then

      !! ===============================================================
      !! receivers: 
      !!     a) we assume all sources have the same receivers
      !!     b) receivers change with the source
      !! ===============================================================
      !!--------------------------------------------------------------
      !!            WARNING FOR DIFFERENCE RCV PER SOURCES            
      !!--------------------------------------------------------------
      !!    If all sources do not have the same set of receivers, then
      !!    the reciprocity must check that they are the same.
      !!    Reciprocity (sum over observed sources) will not work if 
      !!    receivers do not coincide...
      !!--------------------------------------------------------------
      !!--------------------------------------------------------------
      !!--------------------------------------------------------------
      !!                        END WARNING                           
      !!--------------------------------------------------------------
      !!--------------------------------------------------------------

      !! loop over source
      do j=1,ctx_aq%nsrc_loc

        !! -------------------------------------------------------------        
        if(ctx_aq%flag_same_rcv) then

          file_rcv_n_loc = trim(adjustl(file_rcv_n))
          if(j .ne. 1) then
            !! copy
            ctx_aq%sources(j)%rcv%x(:) = ctx_aq%sources(1)%rcv%x(:) 
            ctx_aq%sources(j)%rcv%y(:) = ctx_aq%sources(1)%rcv%y(:) 
            ctx_aq%sources(j)%rcv%z(:) = ctx_aq%sources(1)%rcv%z(:)
            ctx_aq%sources(j)%rcv%nx(:)= ctx_aq%sources(1)%rcv%nx(:) 
            ctx_aq%sources(j)%rcv%ny(:)= ctx_aq%sources(1)%rcv%ny(:) 
            ctx_aq%sources(j)%rcv%nz(:)= ctx_aq%sources(1)%rcv%nz(:)  

            cycle
          end if

        !! -------------------------------------------------------------
        else
          !! file given by file_rcv // source number
          write(stamp_src,'(i0)') ctx_aq%isrc_gb_start + (j-1)*ctx_aq%isrc_step
          file_rcv_loc = trim(trim(adjustl(file_rcv))//trim(stamp_src))
          file_rcv_n_loc = trim(trim(adjustl(file_rcv_n))//trim(stamp_src))

          open (unit=unit_rcv,file=trim(adjustl(file_rcv_loc)))
          read(unit_rcv,*) cline !! total number for this source, already
          if(cline .ne. nrcv(j)) then
            ctx_err%ierr=-1
            ctx_err%msg ="** ERROR: Inconsistent number of receivers" //&
                         " [acquisition_int] **"
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
        end if
        !! -------------------------------------------------------------

        !! appropriate file is opened.      
        do k=1,ctx_aq%sources(j)%rcv%nrcv
        
          read(unit_rcv,*) cline, ctx_aq%sources(j)%rcv%x(k), &
                                  ctx_aq%sources(j)%rcv%y(k), &
                                  ctx_aq%sources(j)%rcv%z(k)
          if(rcv_depth_force) then
             ctx_aq%sources(j)%rcv%z(:)=rcv_depth_val
          end if
        enddo      
        close(unit_rcv)

        !! -------------------------------------------------------------
        !! compute the normal associated with every receivers: it can be 
        !! a flat surface, in which case we simply give the directions. 
        !! It can also be a file which, for each receivers, provide the 
        !! coordinates (x,y,z) of the normals.
        !! Remark: it is not a good idea to compute them by ourself as it 
        !!         needs the small surface around the receivers; which we 
        !!         cannot invent even if other receivers are close.
        !! -------------------------------------------------------------
        select case (trim(adjustl(normethod)))
          case('Z','z','flat','FLAT','Flat','flat_z','FLAT_Z','Flat_Z','Flat_z')
            ctx_aq%sources(j)%rcv%nx(:) = 0.d0
            ctx_aq%sources(j)%rcv%ny(:) = 0.d0
            ctx_aq%sources(j)%rcv%nz(:) = 1.d0
          case('X','x','flat_x','FLAT_X','Flat_X','Flat_x')
            ctx_aq%sources(j)%rcv%nx(:) = 1.d0
            ctx_aq%sources(j)%rcv%ny(:) = 0.d0
            ctx_aq%sources(j)%rcv%nz(:) = 0.d0
          case('Y','y','flat_y','FLAT_Y','Flat_Y','Flat_y')
            ctx_aq%sources(j)%rcv%nx(:) = 0.d0
            ctx_aq%sources(j)%rcv%ny(:) = 1.d0
            ctx_aq%sources(j)%rcv%nz(:) = 0.d0
          
          !! reading the receivers normals from a file.
          case('text','txt','file','txtfile','TEXT','TXT','text-file')
        
            inquire(file=trim(adjustl(file_rcv_n_loc)),exist=file_exists)
            if(.not. file_exists) then
              ctx_err%ierr=-1
              ctx_err%msg ="** ERROR: Input file for receivers " //     &
                           "normal (" // trim(adjustl(file_rcv_n_loc))//& 
                           ") does not exist. [acquisition_init]"
              ctx_err%critic=.true.
              call raise_error(ctx_err)
            endif
            
            !! we can read now
            open (unit=unit_rcv_n,file=trim(adjustl(file_rcv_n_loc)))
            read(unit_rcv_n,*) cline
            if(cline .ne. ctx_aq%sources(j)%rcv%nrcv) then
              ctx_err%ierr=-1
              ctx_err%msg ="** ERROR: The number of receivers for " //  &
                           "their normals is inconsistent. [acquisition_init]"
              ctx_err%critic=.true.
              call raise_error(ctx_err)
            end if
            
            do k=1,ctx_aq%sources(j)%rcv%nrcv
              read(unit_rcv_n,*) cline, ctx_aq%sources(j)%rcv%nx(k),    &
                                        ctx_aq%sources(j)%rcv%ny(k),    &
                                        ctx_aq%sources(j)%rcv%nz(k)
            end do
            close(unit_rcv_n)

          case default
            ctx_err%ierr=-1
            ctx_err%msg ="** ERROR: Unrecognized method to defined the "//&
                         "normals at the receivers [acquisition_init]"
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
        !! ---------------------------------------------------------------

      !! ---------------------------------------------------------------
      end do !! end loop over source
      !! ---------------------------------------------------------------

    !!------------------------------------------------------------------
    end if !! end if receivers
    !!------------------------------------------------------------------

    !!------------------------------------------------------------------
    !! identify sources components
    ctx_aq%ncomponent_src = src_ncomp
    if(ctx_aq%ncomponent_src < 0 ) then
      ctx_err%msg   = "** ERROR: number of component for " // &
                      "source < 0 [acquisition_set] **   "
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    allocate(ctx_aq%src_component(ctx_aq%ncomponent_src))
    !! -----------------------------------------------------------------
    !! we standardize the keyword, but we do not check consistancy 
    !! here as we do not know the equation.
    !! -----------------------------------------------------------------
    call field_tag_formalism(ctx_aq%ncomponent_src,src_comp,            &
                             ctx_aq%src_component,ctx_err)
    !!------------------------------------------------------------------

    deallocate(nrcv)
    return
  end subroutine acquisition_set

end module m_acquisition_init
