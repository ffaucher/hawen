!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_aq.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read frequency parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_aq
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  use m_ctx_parallelism, only: t_parallelism
  use m_barrier,         only: barrier
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_aq, read_parameters_aq_src_charact
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read acquisition parameters 
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   parameter_file  : parameter file where infos are
  !> @param[out]  srcfile         : filename for the source acquisition
  !> @param[out]  rcvfile         : filename for the receiver acquisition
  !> @param[out]  flag_same_rcv   : indicates if the rcv are fixed
  !> @param[out]  rcvfile_n       : filename for the receiver normal
  !> @param[out]  srcformat       : source format keyword
  !> @param[out]  src_comp        : source components
  !> @param[out]  src_ncomp       : source n_components
  !> @param[out]  src_depth_force : flag to know if force source depth
  !> @param[out]  rcv_depth_force : flag to know if force rcv depth
  !> @param[out]  src_depth_val   : force source depth val
  !> @param[out]  rcv_depth_val   : force rcv depth val
  !> @param[out]  normethod       : method for the receivers normal
  !> @param[out]  aq_verb         : verbose aquisition flag
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_aq(ctx_paral,parameter_file,srcfile,rcvfile, &
                                flag_same_rcv,rcvfile_n,srcformat,        &
                                src_comp,src_ncomp,isrc_gb_start,         &
                                isrc_gb_end,isrc_step,src_depth_force,    &
                                src_depth_val,rcv_depth_force,            &
                                rcv_depth_val,normethod,aq_verb,ctx_err)
    type(t_parallelism)            ,intent(in)   :: ctx_paral
    character(len=*)               ,intent(in)   :: parameter_file
    character(len=512)             ,intent(out)  :: srcfile,rcvfile,rcvfile_n
    character(len=512)             ,intent(out)  :: srcformat,normethod
    character(len=512) ,allocatable,intent(inout):: src_comp(:)
    integer                        ,intent(out)  :: src_ncomp
    integer                        ,intent(out)  :: isrc_gb_start,isrc_gb_end
    integer                        ,intent(out)  :: isrc_step
    logical                        ,intent(out)  :: src_depth_force
    logical                        ,intent(out)  :: rcv_depth_force
    logical                        ,intent(out)  :: flag_same_rcv
    real                           ,intent(out)  :: src_depth_val
    real                           ,intent(out)  :: rcv_depth_val
    logical                        ,intent(out)  :: aq_verb
    type(t_error)                  ,intent(out)  :: ctx_err
    !! local
    integer            :: int_list   (512)
    real               :: real_list  (512)
    logical            :: logic_list (512)
    character(len=512) :: string_list(512)
    integer            :: nval

    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------
    !! Reading the acquisition files -----------------------------------
    !! -----------------------------------------------------------------    
    call read_parameter(parameter_file,'acquisition_rcv_normal','flat',  &
                        normethod,nval)
    call read_parameter(parameter_file,'file_acquisition_source'    ,'', &
                        srcfile,nval)
    call read_parameter(parameter_file,'file_acquisition_receiver'  ,'', &
                        rcvfile,nval)
    call read_parameter(parameter_file,'file_acquisition_receiver_normal',&
                        '',rcvfile_n,nval)
    call read_parameter(parameter_file,'acquisition_receiver_fixed',    &
                        .true.,logic_list,nval)
    flag_same_rcv=logic_list(1)
    if(trim(adjustl(srcfile)) .eq. '') then
      ctx_err%msg   = "** ERROR: No acquisition source file given **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    !! possibility for no receivers
    !! if(trim(adjustl(rcvfile)) .eq. '') then
    !!   flag_no_receiver = .true.
    !!   ctx_err%msg   = "** ERROR: No acquisition receiver file given **"
    !!   ctx_err%ierr  = -1
    !!   ctx_err%critic=.true.
    !!   call raise_error(ctx_err)
    !! endif
    call read_parameter(parameter_file,'source_type','dirac',srcformat,nval)
    
    call read_parameter(parameter_file,'source_first',1,int_list,nval)
    isrc_gb_start=int_list(1)
    call read_parameter(parameter_file,'source_last',2000000000,int_list,nval)
    isrc_gb_end  =int_list(1)
    call read_parameter(parameter_file,'source_step' ,1,int_list,nval)
    isrc_step    =int_list(1)
    
    !! number of component affected by the source and their list
    call read_parameter(parameter_file,'source_component','p',string_list, &
                        nval)
    src_ncomp = nval
    allocate(src_comp(src_ncomp))
    src_comp(:) = string_list(1:nval)
    
    !! number of component saved by the receivers and their list
    ! call read_parameter(parameter_file,'receiver_component','',string_list,nval)
    ! rcv_ncomp = nval
    ! if(nval > 0) then
    !   allocate(rcv_comp(rcv_ncomp))
    !   rcv_comp(:) = string_list(1:nval)
    ! end if

    !! We can force the depth of the sources and receivers
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'src_depth',0.,real_list,nval,   &
                        o_flag_found=src_depth_force)
    src_depth_val=real_list(1)
    call read_parameter(parameter_file,'rcv_depth',0.,real_list,nval,   &
                        o_flag_found=rcv_depth_force)
    rcv_depth_val=real_list(1)

    !!
    call read_parameter(parameter_file,'acquisition_verbose',.false.,  &
                        logic_list,nval)
    aq_verb=logic_list(1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------      
    !! mpi_barrier waits for every processors
    call barrier(ctx_paral%communicator,ctx_err)
    
    return
  end subroutine read_parameters_aq

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read the source characterization
  !
  !> @param[in]   parameter_file  : parameter file where infos are
  !> @param[out]  src_time0       : time the source starts
  !> @param[out]  src_tpeak       : time source is at peak
  !> @param[out]  src_fpeak       : frequency of the ricker source
  !> @param[out]  src_ampli       : amplitude
  !> @param[out]  flag_constant   : in case one wants constant source
  !> @param[out]  src_constant    : complex constant source
  !> @param[out]  gaussian_var    : variance if we have a Gaussian source
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_aq_src_charact(parameter_file,src_time0,     &
                             src_tpeak,src_fpeak,src_ampli,flag_constant, &
                             src_constant,gaussian_variance,ctx_err)
    character(len=*)               ,intent(in)   :: parameter_file
    real                           ,intent(out)  :: src_time0
    real                           ,intent(out)  :: src_tpeak
    real                           ,intent(out)  :: src_fpeak
    real                           ,intent(out)  :: src_ampli
    logical                        ,intent(out)  :: flag_constant
    complex(kind=8)                ,intent(out)  :: src_constant
    real   (kind=8)                ,intent(out)  :: gaussian_variance(3)
    type(t_error)                  ,intent(out)  :: ctx_err
    !! local
    real(kind=8)       :: dble_list(512)
    real               :: real_list(512)
    logical            :: logic_list(512)
    integer            :: nval

    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------
    !! Reading the source characterization
    !! -----------------------------------------------------------------    
    call read_parameter(parameter_file,'source_time0',0.,real_list,nval)
    src_time0 = real_list(1)
    call read_parameter(parameter_file,'source_timepeak',0.,real_list,nval)
    src_tpeak = real_list(1)
    call read_parameter(parameter_file,'source_freqpeak',0.,real_list,nval)
    src_fpeak = real_list(1)
    call read_parameter(parameter_file,'source_ampli',0.,real_list,nval)
    src_ampli = real_list(1)
    call read_parameter(parameter_file,'source_constant',.false.,logic_list,nval)
    flag_constant = logic_list(1)
    src_constant  = cmplx(0.,0.)
    if(flag_constant) then
      call read_parameter(parameter_file,'source_constant_real',0.,real_list,nval)
      src_constant=real_list(1)
      call read_parameter(parameter_file,'source_constant_imag',0.,real_list,nval)
      src_constant=dcmplx(real(src_constant), real_list(1))
      if(abs(src_constant) < 1E-16) then
        ctx_err%msg   = "** ERROR: constant source value is too small " // &
                        "[read_parameters_aq_src_characterization] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if

    !! -----------------------------------------------------------------
    !! this will only be used if we have a Gaussian source
    gaussian_variance = 0.d0
    call read_parameter(parameter_file,'source_gaussian_variance',0.d0, &
                        dble_list,nval)
    if(nval > 0) then
      gaussian_variance(1:min(3,nval)) = dble_list(1:min(3,nval))
    end if


    return
  end subroutine read_parameters_aq_src_charact

end module m_read_parameters_aq
