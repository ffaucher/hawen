add_library(acquisition
m_ctx_acquisition.f90
m_read_parameters_aq.f90
m_acquisition_init.f90
m_print_aq.f90
m_source_eval.f90
)
target_link_libraries(acquisition tag parameter_file parallelism)
install(TARGETS acquisition 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
