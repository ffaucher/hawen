!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_acquisition.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the sources and receivers in the acquisition
!
!------------------------------------------------------------------------------
module m_ctx_acquisition

  implicit none
  
  !> Undefined source is 0
  integer, parameter :: SRC_NONE        = 0
  !> Dirac source is 1 
  integer, parameter :: SRC_DIRAC       = 1
  integer, parameter :: SRC_DIRAC_DERIV = 2
  integer, parameter :: SRC_DIRAC_DIV   = 3
  integer, parameter :: SRC_DIRAC_DERIVX= 4
  integer, parameter :: SRC_DIRAC_DERIVY= 5
  integer, parameter :: SRC_DIRAC_DERIVZ= 6
  !> Planewave source is 2
  !> Note that different typs of planewaves can be chosen
  integer, parameter :: SRC_PW          = 20
  !> Analytic family: we impose a Dirichlet condition and a special function
  !> Note that different typs of analytic functions can be chosen
  integer, parameter :: SRC_ANALYTIC    = 30
  !> Using a Gaussian-type of source
  integer, parameter :: SRC_GAUSSIAN    = 40
  
  private
  public :: t_acquisition, acquisition_clean, SRC_DIRAC, SRC_NONE
  public :: acquisition_copy_source, t_device_src
  public :: SRC_DIRAC_DERIV, SRC_DIRAC_DIV
  public :: SRC_DIRAC_DERIVX, SRC_DIRAC_DERIVY, SRC_DIRAC_DERIVZ
  public :: SRC_ANALYTIC, SRC_PW, SRC_GAUSSIAN
  

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_device_rcv contains info on receivers
  !
  !> @param   nrcv        : number of receivers
  !> @param   x           : positions of x
  !> @param   y           : positions of y
  !> @param   z           : positions of z
  !> @param   n{x,y,z}    : normals direction
  !----------------------------------------------------------------------------
  type t_device_rcv
    !! number of receivers
    integer             :: nrcv
    !! positions in x
    real(kind=8),allocatable    :: x(:)
    !! positions in y
    real(kind=8),allocatable    :: y(:)
    !! positions in z
    real(kind=8),allocatable    :: z(:)
    !! the normal direction at each receiver position
    real(kind=8),allocatable    :: nx(:),ny(:),nz(:)
  end type t_device_rcv

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_device_src contains info on one source
  !
  !> @param    nsim         : number of simultaneous sources
  !> @param    x(:)         : positions of the multi-point x
  !> @param    y(:)         : positions of the multi-point y
  !> @param    z(:)         : positions of the multi-point z
  !> @param    rcv(:)       : the receivers associated to the source
  !----------------------------------------------------------------------------
  type t_device_src
    !! number of simultaneous sources
    integer                        :: nsim
    !! positions of multi-point x
    real(kind=8)      ,allocatable :: x(:)  
    !! positions of multi-point y
    real(kind=8)      ,allocatable :: y(:)
    !! positions of multi-point z
    real(kind=8)      ,allocatable :: z(:)
    !! the receivers for the source
    type(t_device_rcv)             :: rcv
  end type t_device_src

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_source contains info on the source value:
  !> it can be a order 2 Ricker wavelet with options.
  !> or a constant values given by the real and imaginary parts.
  !> @param time0            : time at which the source starts
  !> @param timepeak         : time peak for Ricker
  !> @param freqpeak         : frequency peak for Ricker
  !> @param amplitude        : scalar amplitude to apply
  !> @param current_val      : current source value
  !> @param flag_constant    : flag to indicate constant value
  !----------------------------------------------------------------------------
  type t_source_charac
    !! time at which the source starts
    real            :: time0
    !! time peak for Ricker
    real            :: timepeak
    !! frequency peak for Ricker
    real            :: freqpeak
    !! scalar amplitude to apply
    real            :: amplitude
    !! current source value
    complex(kind=8) :: current_val
    !! in case of input constant values
    logical         :: flag_constant
    !! in case we have a Gaussian source.
    real(kind=8)    :: gaussian_variance(3)
  end type t_source_charac

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_acquisition contains info on all the sources and receivers
  !
  !> @param       src_file    : source file read
  !> @param       rcv_file    : receiver file read
  !> @param       src_type    : source type
  !> @param       src_comp    : source component
  !> @param       nsrc        : total number of sources
  !> @param    ncomponent_src : total number of src component
  !> @param     isrc_gb_start : fist source in acquisition
  !> @param     isrc_gb_end   : last source in acquisition
  !> @param     isrc_step     : step source in acquisition
  !> @param       sources  (:): list of source
  !>                          : contains also the receivers
  !> @param   flag_rcv        : indicate if receivers are present
  !> @param   flag_same_rcv   : indicate if same rcv for all src
  !> @param   verbose         : indicate how much infos to print
  !----------------------------------------------------------------------------
  !----------------------------------------------------------------------------
  type t_acquisition
    !! source file read
    character(len=256)            :: src_file
    !! receiver file read
    character(len=256)            :: rcv_file
    !! source type
    integer                       :: src_type
    !! source component, e.g., pressure, velocity, sigma
    character(len=64),allocatable :: src_component(:)
    !! total number of sources
    integer                       :: nsrc_gb,nsrc_loc
    !! total number of src component
    integer                       :: ncomponent_src
    !! fist source in acquisition to take into account
    integer                       :: isrc_gb_start
    !! last source in acquisition to take into account
    integer                       :: isrc_gb_end  
    !! step source in acquisition for the computations
    integer                       :: isrc_step
    !! list of source
    type(t_device_src),allocatable:: sources  (:)
    !! source information
    type(t_source_charac)         :: src_info
    !! memory allocated
    integer(kind=8)               :: memory_gb
    !! indicate if receivers are present
    logical                       :: flag_rcv
    !! indicate if same receivers positions for all sources
    logical                       :: flag_same_rcv=.false.
    !! verbose flag for screen printing
    logical                       :: verbose
  end type t_acquisition
  
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean acquisition information and allocation
  !
  !> @param[inout] ctx_aq      : acquisition context
  !----------------------------------------------------------------------------
  subroutine acquisition_clean(ctx_aq)
    type(t_acquisition)    ,intent(inout)  :: ctx_aq
    !!
    integer :: k

    ctx_aq%src_file=''
    ctx_aq%rcv_file=''
    ctx_aq%src_type=SRC_NONE
    do k=1,ctx_aq%nsrc_loc
      if(allocated(ctx_aq%sources(k)%rcv%x)) deallocate(ctx_aq%sources(k)%rcv%x)
      if(allocated(ctx_aq%sources(k)%rcv%y)) deallocate(ctx_aq%sources(k)%rcv%y)
      if(allocated(ctx_aq%sources(k)%rcv%z)) deallocate(ctx_aq%sources(k)%rcv%z)
      if(allocated(ctx_aq%sources(k)%rcv%nx))deallocate(ctx_aq%sources(k)%rcv%nx)
      if(allocated(ctx_aq%sources(k)%rcv%ny))deallocate(ctx_aq%sources(k)%rcv%ny)
      if(allocated(ctx_aq%sources(k)%rcv%nz))deallocate(ctx_aq%sources(k)%rcv%nz)
      
      if(allocated(ctx_aq%sources(k)%x)) deallocate(ctx_aq%sources(k)%x)
      if(allocated(ctx_aq%sources(k)%y)) deallocate(ctx_aq%sources(k)%y)
      if(allocated(ctx_aq%sources(k)%z)) deallocate(ctx_aq%sources(k)%z)

    enddo

    ctx_aq%nsrc_gb =0
    ctx_aq%nsrc_loc=0
    ctx_aq%ncomponent_src=0
    ctx_aq%isrc_gb_start =0
    ctx_aq%isrc_gb_end   =0
    ctx_aq%isrc_step     =0
    ctx_aq%flag_same_rcv =.false.
    if(allocated(ctx_aq%sources))       deallocate(ctx_aq%sources)
    if(allocated(ctx_aq%src_component)) deallocate(ctx_aq%src_component)
    ctx_aq%memory_gb=0
    ctx_aq%verbose  =.false.
    ctx_aq%flag_rcv =.false.

    return
  end subroutine acquisition_clean

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean acquisition information and allocation
  !
  !> @param[inout] ctx_aq      : acquisition context
  !----------------------------------------------------------------------------
  subroutine acquisition_copy_source(ctx_in,ctx_out)

    implicit none

    type(t_device_src)    ,intent(in)  :: ctx_in
    type(t_device_src)    ,intent(out) :: ctx_out
   
    !! copy the main info
    ctx_out%nsim = ctx_in%nsim
    allocate(ctx_out%x     (ctx_out%nsim))
    allocate(ctx_out%y     (ctx_out%nsim))
    allocate(ctx_out%z     (ctx_out%nsim))
    ctx_out%x(:)      = ctx_in%x(:)      
    ctx_out%y(:)      = ctx_in%y(:)      
    ctx_out%z(:)      = ctx_in%z(:)      
    
    !! copy receivers informations
    ctx_out%rcv%nrcv  = ctx_in%rcv%nrcv
    allocate(ctx_out%rcv%x (ctx_out%rcv%nrcv))
    allocate(ctx_out%rcv%y (ctx_out%rcv%nrcv))
    allocate(ctx_out%rcv%z (ctx_out%rcv%nrcv))
    allocate(ctx_out%rcv%nx(ctx_out%rcv%nrcv))
    allocate(ctx_out%rcv%ny(ctx_out%rcv%nrcv))
    allocate(ctx_out%rcv%nz(ctx_out%rcv%nrcv))
    
    if(allocated(ctx_in%rcv%x )) ctx_out%rcv%x  = ctx_in%rcv%x
    if(allocated(ctx_in%rcv%y )) ctx_out%rcv%y  = ctx_in%rcv%y
    if(allocated(ctx_in%rcv%z )) ctx_out%rcv%z  = ctx_in%rcv%z
    if(allocated(ctx_in%rcv%nx)) ctx_out%rcv%nx = ctx_in%rcv%nx
    if(allocated(ctx_in%rcv%ny)) ctx_out%rcv%ny = ctx_in%rcv%ny
    if(allocated(ctx_in%rcv%nz)) ctx_out%rcv%nz = ctx_in%rcv%nz 

    return

  end subroutine acquisition_copy_source
  
end module m_ctx_acquisition
