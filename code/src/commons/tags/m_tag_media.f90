!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_tag_media
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> Definition of the tags for the different type of media.
!> These are only used if the problem consists of at least two media,
!> e.g., for acoustic-elastic coupling.
!
!------------------------------------------------------------------------------
module m_tag_media
  
  implicit none
  
  private
  
  public   :: tag_medium_acoustic,tag_medium_elastic
  !! for stellar problem
  public   :: tag_medium_interior,tag_medium_atmo
  !! count number of possibilities
  public   :: n_medium_tag
  !! interface
  public   :: tag_medium_interface_acoustic_to_elastic
  public   :: tag_medium_interface_elastic_to_acoustic
  public   :: tag_medium_interface_interior_to_atmo
  public   :: tag_medium_interface_atmo_to_interior


  !! Remark: INTEGER(1) is from    -128 to    127
  !!         INTEGER(2) is from -32 768 to 32 767
  !! list of tags, must be incremental for array counting the cells.
  integer(kind=4), parameter :: tag_medium_acoustic  =  1 
  integer(kind=4), parameter :: tag_medium_elastic   =  2
  integer(kind=4), parameter :: tag_medium_interior  =  3
  integer(kind=4), parameter :: tag_medium_atmo      =  4
  !! total number ---------------------------------------
  integer(kind=4), parameter :: n_medium_tag         =  4
  
  !! this is to indicate the interface 
  integer(kind=4), parameter :: tag_medium_interface_acoustic_to_elastic =-1
  integer(kind=4), parameter :: tag_medium_interface_elastic_to_acoustic =-2
  integer(kind=4), parameter :: tag_medium_interface_interior_to_atmo    =-11
  integer(kind=4), parameter :: tag_medium_interface_atmo_to_interior    =-12

end module m_tag_media
