!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_tag_namefield.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the list of tags for the fields name
!
!------------------------------------------------------------------------------
module m_tag_namefield

  use m_raise_error,            only : raise_error, t_error

  implicit none

  !> pressure field
  character(len=1), parameter    :: tag_FIELD_P       = 'p'
  !> displacement field
  character(len=2), parameter    :: tag_FIELD_UX      = 'ux'
  character(len=2), parameter    :: tag_FIELD_UY      = 'uy'
  character(len=2), parameter    :: tag_FIELD_UZ      = 'uz'
  !> velocity field
  character(len=2), parameter    :: tag_FIELD_VX      = 'vx'
  character(len=2), parameter    :: tag_FIELD_VY      = 'vy'
  character(len=2), parameter    :: tag_FIELD_VZ      = 'vz'
  !> sigmas field
  character(len=3), parameter    :: tag_FIELD_SXX     = 'sxx'
  character(len=3), parameter    :: tag_FIELD_SXY     = 'sxy'
  character(len=3), parameter    :: tag_FIELD_SXZ     = 'sxz'
  character(len=3), parameter    :: tag_FIELD_SYY     = 'syy'
  character(len=3), parameter    :: tag_FIELD_SYZ     = 'syz'
  character(len=3), parameter    :: tag_FIELD_SZZ     = 'szz'

  !> fields in the normal and tangential direction
  character(len=8), parameter    :: tag_FIELD_VNORMAL = 'normal_v'
  character(len=9), parameter    :: tag_FIELD_VTANGENT= 'tangent_v'

  !! helioseismology problem in 1D, the variables W and V are
  !! linked following [Pham et al. 2019, 2020]:
  !!        w = r u
  !!        v = r u'
  !! ==> r w' = w + r v
  !! ------------------
  character(len=1), parameter    :: tag_FIELD_U      = 'u'
  character(len=1), parameter    :: tag_FIELD_W      = 'w'
  character(len=1), parameter    :: tag_FIELD_V      = 'v'
  character(len=2), parameter    :: tag_FIELD_Asol   = 'al'
  character(len=2), parameter    :: tag_FIELD_Dsol   = 'dl'
  character(len=2), parameter    :: tag_FIELD_Bsol   = 'bl'
  character(len=2), parameter    :: tag_FIELD_Esol   = 'el'

  private

  public :: field_tag_formalism,field_directional_formalism

  public :: tag_FIELD_P,tag_FIELD_UX,tag_FIELD_UY,tag_FIELD_UZ
  public :: tag_FIELD_VX,tag_FIELD_VY,tag_FIELD_VZ,tag_FIELD_SXX
  public :: tag_FIELD_SXY,tag_FIELD_SXZ,tag_FIELD_SYY,tag_FIELD_SYZ
  public :: tag_FIELD_U,tag_FIELD_W,tag_FIELD_V,tag_FIELD_Asol,tag_FIELD_Dsol 
  public :: tag_FIELD_Bsol,tag_FIELD_SZZ,tag_FIELD_VNORMAL,tag_FIELD_VTANGENT
  public :: tag_FIELD_Esol

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> decide the tag from keyword
  !
  !> @param[in]    n_field       : number of field to process
  !> @param[in]    name_in       : input name
  !> @param[out]   tag_out       : output tag
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine field_tag_formalism(n_field,name_in,tag_out,ctx_err)
    implicit none

    integer                            ,intent(in)   :: n_field
    character(len=*) ,allocatable      ,intent(in)   :: name_in(:)
    character(len=*) ,allocatable      ,intent(inout):: tag_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer    :: k
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    do k=1,n_field
      select case(trim(adjustl(name_in(k))))
        case('P','p','PRESSURE','Pressure','pressure')
          tag_out(k) = tag_FIELD_P
        case('velocity_x','vx','Velocity_x','velocity_X','Velocity_X', &
             'VX','VELOCITY_X')
          tag_out(k) = tag_FIELD_VX
        case('velocity_y','vy','Velocity_y','velocity_Y','Velocity_Y', &
             'VY','VELOCITY_Y')
          tag_out(k) = tag_FIELD_VY
        case('velocity_z','vz','Velocity_z','velocity_Z','Velocity_Z', &
             'VZ','VELOCITY_Z')
          tag_out(k) = tag_FIELD_VZ
        case('ux','UX','Ux')
          tag_out(k) = tag_FIELD_UX
        case('uy','UY','Uy')
          tag_out(k) = tag_FIELD_UY
        case('uz','UZ','Uz')
          tag_out(k) = tag_FIELD_UZ
        case('sxx','SXX','sigma_xx','SIGMA_XX','Sigma_xx')
          tag_out(k) = tag_FIELD_SXX
        case('syy','SYY','sigma_yy','SIGMA_YY','Sigma_yy')
          tag_out(k) = tag_FIELD_SYY
        case('szz','SZZ','sigma_zz','SIGMA_ZZ','Sigma_zz')
          tag_out(k) = tag_FIELD_SZZ
        case('sxy','SXY','sigma_xy','SIGMA_XY','Sigma_xy')
          tag_out(k) = tag_FIELD_SXY
        case('sxz','SXZ','sigma_xz','SIGMA_XZ','Sigma_xz')
          tag_out(k) = tag_FIELD_SXZ
        case('syz','SYZ','sigma_yz','SIGMA_YZ','Sigma_yz')
          tag_out(k) = tag_FIELD_SYZ
        !! normal and tangent direction for inversion only
        case('normal_velocity','normal_v','NORMAL_V','NORMAL_VEL','normal_vel')
          tag_out(k) = tag_FIELD_VNORMAL
        case('tangent_velocity','tan_velocity','tangential_velocity',   &
             'tangent_v','tan_v','tangential_v','tangent_vel','tan_vel',&
             'tangential_vel')
          tag_out(k) = tag_FIELD_VTANGENT
        !! for Helio ODE in 1D
        case('U','u')
          tag_out(k) = tag_FIELD_U
        case('V','v')
          tag_out(k) = tag_FIELD_V
        case('W','w')
          tag_out(k) = tag_FIELD_W
        case('Al','al','aell','Asol','asol')
          tag_out(k) = tag_FIELD_Asol
        case('Dl','dl','dell','Dsol','dsol')
          tag_out(k) = tag_FIELD_Dsol
        case('Bl','bl','bell','Bsol','bsol')
          tag_out(k) = tag_FIELD_Bsol
        case('El','el','eell','Esol','esol')
          tag_out(k) = tag_FIELD_Esol
        case default
          ctx_err%msg   = "** ERROR: Unrecognized field name " // &
                          "[field_tag_formalism]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end do

    return

  end subroutine field_tag_formalism



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> in the case where we need the normal, we deduce the fields
  !> that are required, and they are ordered so that one can then
  !> directly apply the normal vector.
  !> For instance, for normal_v in 2D, it returns {vx, vz}, such 
  !> that the normal follows with vx.nx + vz.nz.
  !
  !> @param[in]    dim_n         : problem dimension
  !> @param[in]    field_in      : input names
  !> @param[in]    n{x,y,z}      : normals in x, y and z
  !> @param[out]   n_field       : number of field to process
  !> @param[out]   direction_out : directional vector to apply
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine field_directional_formalism(dim_n,field_in,nx,ny,nz,       &
                                         n_field,field_out,             &
                                         direction_out,ctx_err)

    implicit none

    integer                       , intent(in)   :: dim_n
    character(len=*)              , intent(in)   :: field_in
    real(kind=8)    , allocatable , intent(in)   :: nx(:)
    real(kind=8)    , allocatable , intent(in)   :: ny(:)
    real(kind=8)    , allocatable , intent(in)   :: nz(:)
    integer                       , intent(out)  :: n_field
    character(len=*), allocatable , intent(inout):: field_out(:)
    real(kind=8)    , allocatable , intent(inout):: direction_out(:,:)
    type(t_error)                 , intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! depends on the key that is given, if it is not a
    !! normal or a tangent, we simply give the in.
    select case (trim(adjustl(field_in)))

      !! ---------------------------------------------------------------
      !! if we have a normal direction, we keep the
      !! input for the direction
      !! ---------------------------------------------------------------
      case(tag_FIELD_VNORMAL)

        n_field = dim_n
        select case(dim_n)
          case(1)
            field_out(1)       = tag_FIELD_VX
            direction_out(1,:) = nx(:)
          case(2)
            field_out(1) = tag_FIELD_VX
            field_out(2) = tag_FIELD_VZ
            direction_out(1,:) = nx(:)
            direction_out(2,:) = nz(:)
          case(3)
            field_out(1) = tag_FIELD_VX
            field_out(2) = tag_FIELD_VY
            field_out(3) = tag_FIELD_VZ
            direction_out(1,:) = nx(:)
            direction_out(2,:) = ny(:)
            direction_out(3,:) = nz(:)

          case default
            ctx_err%msg   = "** ERROR: Inconsistent dimension in the "//&
                            "computation of the directional fields "  //&
                            "[field_directional_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error
        end select

      !! ---------------------------------------------------------------
      !! if we have a tangential direction, we need to adjust
      !! input normal direction to obtain the tangential ones.
      !! ---------------------------------------------------------------
      case(tag_FIELD_VTANGENT)

        n_field = dim_n
        select case(dim_n)
          case(1)
            field_out(1)     = tag_FIELD_VX
            direction_out(1,:) = nz(:)

          case(2)
            field_out(1) = tag_FIELD_VX
            field_out(2) = tag_FIELD_VZ
            !! revert compare to the normal, there is flexibility
            !! in the choice for the tangent, it can be +/-.
            direction_out(1,:) = nz(:)
            direction_out(2,:) =-nx(:)

          case(3)
            field_out(1) = tag_FIELD_VX
            field_out(2) = tag_FIELD_VY
            field_out(3) = tag_FIELD_VZ
            !! there is an ambiguity to obtain the tangent
            !! only from the normal here. There are infinite
            !! possibilities ?
            ctx_err%msg   = "** ERROR: The three-dimensional " //       &
                            "conversion of the normal to the tangent "//&
                            "is not implemented yet " //                &
                            "[field_directional_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error

          case default
            ctx_err%msg   = "** ERROR: Inconsistent dimension in the "//&
                            "computation of the directional fields "  //&
                            "[field_directional_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error
        end select

      !! ---------------------------------------------------------------
      !! we keep the input fields
      !! ---------------------------------------------------------------
      case default
        n_field = 1
        field_out(1) = trim(adjustl(field_in))
        direction_out= 1.d0
      end select
      !! ---------------------------------------------------------------

    return

  end subroutine field_directional_formalism

end module m_tag_namefield
