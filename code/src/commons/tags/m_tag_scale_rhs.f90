!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_tag_scale_rhs.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the list of tags for scaling of the right-hand 
!> side, for instance in helioseismology, the radius, i.e., distance
!> to the source must be multiplied to the rhs.
!
!------------------------------------------------------------------------------
module m_tag_scale_rhs

  use m_raise_error,            only : raise_error, t_error

  implicit none

  !> identity: we do not multiply by anything
  integer, parameter    :: tag_RHS_SCALE_ID       =  0
  !> here we multiply by the distance to the point
  integer, parameter    :: tag_RHS_SCALE_R        = 10
  integer, parameter    :: tag_RHS_SCALE_R2       = 11
  !> here we multiply by the distance in x only
  integer, parameter    :: tag_RHS_SCALE_X        = 20
  integer, parameter    :: tag_RHS_SCALE_X2       = 21
  !> here we use a constant
  integer, parameter    :: tag_RHS_SCALE_INV2PI   = 71 ! 1/(2pi)
  integer, parameter    :: tag_RHS_SCALE_XINV2PI  = 72 ! x/(2pi)


  private

  public :: tag_RHS_SCALE_ID
  public :: tag_RHS_SCALE_R, tag_RHS_SCALE_R2
  public :: tag_RHS_SCALE_X, tag_RHS_SCALE_X2
  public :: tag_RHS_SCALE_INV2PI, tag_RHS_SCALE_XINV2PI

  contains 

end module m_tag_scale_rhs
