!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_tag_bc
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> Definition of the coundary conditions tags
!
!------------------------------------------------------------------------------
module m_tag_bc
  
  implicit none
  private
  public   :: tag_absorbing,tag_pml,tag_freesurface
  public   :: tag_ghost_cell,tag_wallsurface,tag_pml_circular
  public   :: tag_dirichlet,tag_neumann,tag_planewave
  public   :: tag_center_neumann,tag_deltapdirichlet_dldirichlet
  public   :: tag_aldirichlet_dlrobin,tag_deltapdirichlet_dlrobin
  public   :: tag_alrbc_dlrobin,tag_alrbc_dldirichlet,tag_deltapdirichlet
  public   :: tag_lagrangep, tag_lagrangep_dlrobin
  public   :: tag_analytic_orthorhombic,n_boundary_tag
  public   :: tag_planewave_isoP,tag_planewave_isoS,tag_analytic_elasticiso
  public   :: tag_planewave_vtiYSH,tag_planewave_vtiYSV,tag_planewave_vtiYqP
  public   :: tag_planewave_scatt

  !> tag for boundaries are 
  !> - absorbing boundary condition
  !> - free surface ==> \sigma.n     = 0 (e.g. pressure=0)
  !> - wall surface ==> displacement = 0
  integer, parameter :: tag_absorbing      = -1 
  integer, parameter :: tag_freesurface    =-10
  integer, parameter :: tag_wallsurface    =-20

  integer, parameter :: tag_dirichlet      =-30
  integer, parameter :: tag_neumann        =-40
  !! planewave cases
  integer, parameter :: tag_planewave      =-50 ! using the same for all components
  integer, parameter :: tag_planewave_isoP =-51 ! elastic isotropic P-wave
  integer, parameter :: tag_planewave_isoS =-52 ! elastic isotropic S-wave
  integer, parameter :: tag_planewave_vtiYSH =-53 ! VTI in the y-axis with qSH
  integer, parameter :: tag_planewave_vtiYSV =-54 ! VTI in the y-axis with qSV
  integer, parameter :: tag_planewave_vtiYqP =-55 ! VTI in the y-axis with qP
  ! using a scattered field condition to compute global wavefield
  ! ugb = uinc + uscat, where uinc is a planewave and only uscat verifies
  !                     the Sommerfeld radiation boundary condition
  integer, parameter :: tag_planewave_scatt  =-56 
  
  !! we impose the analytic solution of 
  !!   - elastic orthorhombic media
  !!   - elastic isotropic    media
  integer, parameter :: tag_analytic_orthorhombic = -101
  integer, parameter :: tag_analytic_elasticiso   = -102


  integer, parameter :: tag_pml_circular   =-60
  !> the last one because we increment (negatively)
  integer, parameter :: tag_pml            =-70
  
  !> tag comparisons for helio at r=0
  integer, parameter :: tag_center_neumann =-80
  !> tag comparisons for helio at rmax 
  integer, parameter :: tag_aldirichlet_dlrobin         =-90
  integer, parameter :: tag_deltapdirichlet_dldirichlet =-91
  integer, parameter :: tag_deltapdirichlet_dlrobin     =-92
  integer, parameter :: tag_alrbc_dlrobin               =-93
  integer, parameter :: tag_alrbc_dldirichlet           =-94
  integer, parameter :: tag_deltapdirichlet             =-95
  integer, parameter :: tag_lagrangep                   =-96
  integer, parameter :: tag_lagrangep_dlrobin           =-97
  
  !> indicate the neighbor cell is elsewhere
  integer, parameter :: tag_ghost_cell     = 0

  !> total number of boundary conditions available, ignoring the ghost.
  !  -------------------------------------------------------------------
  integer, parameter :: n_boundary_tag     = 25
  !  -------------------------------------------------------------------

end module m_tag_bc
