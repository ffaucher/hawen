/* This are the C routine to compute 
 *  -> Airy functions with Complex parameters
 * using ARB library.
 * */
#include "acb.h"
#include "arb.h"
#include "arf.h"
#include "acb_hypgeom.h"

static int const PREC_MAX=16384; // = 1024*16 
static int const PREC_MIN=  128;

/* Compute the Airy function A and A', 
 * B could also be computed but we send
 * NULL not to.
 * */
void compute_airyA_and_derivative_arb
                              (const double  z_x, const double  z_y, 
                               double *  A_x, double *  A_y,
                               double * dA_x, double * dA_y)
{
    slong prec;
    acb_t      z ;             // Airy parameter
    acb_t      A, dA ;         // Airy functions in arb form.
    
    // to convert to double precision.
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(A) ;
    acb_init(dA);    
    acb_init(z);
    
    acb_set_d_d(z,z_x,z_y);

    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        // compute Airy and derivative 
        acb_hypgeom_airy(A,dA,NULL,NULL, z, prec);

        if (acb_is_finite( A) && acb_rel_accuracy_bits( A) >= 53 && 
            acb_is_finite(dA) && acb_rel_accuracy_bits(dA) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("WARNING PREC_MAX is reached, the result is probably false");
    }
    
    /* retrieve real and imaginary parts of the result */
    arb_init(tmp1_x);
    arb_init(tmp1_y);
    arf_init(tmp2_x);
    arf_init(tmp2_y);
    
    acb_get_real(tmp1_x, A);
    acb_get_imag(tmp1_y, A);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *A_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *A_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    
    acb_get_real(tmp1_x,dA);
    acb_get_imag(tmp1_y,dA);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *dA_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *dA_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(z);
    acb_clear(A);
    acb_clear(dA);
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);

}

/* Compute the ratio of Airy functions
 *        A'/A, 
 * B could also be computed but we send
 * NULL not to.
 * */
void compute_airy_dA_over_A_arb(const double  z_x, const double  z_y, 
                                double * ratio_x, double *  ratio_y)
{
    slong prec;
    acb_t      z ;             // Airy parameter
    acb_t      A, dA, ratio;         // Airy functions in arb form.
    
    // to convert to double precision.
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(A)    ;
    acb_init(dA)   ;    
    acb_init(ratio);    
    acb_init(z)    ;
    
    acb_set_d_d(z,z_x,z_y);

    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        // compute Airy and derivative 
        acb_hypgeom_airy(A,dA,NULL,NULL, z, prec);
        acb_div(ratio,dA,A, prec);   // A' / A
        
        if (acb_is_finite(ratio) && acb_rel_accuracy_bits(ratio) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("WARNING PREC_MAX is reached, the result is probably false");
    }
    
    /* retrieve real and imaginary parts of the result */
    arb_init(tmp1_x);
    arb_init(tmp1_y);
    arf_init(tmp2_x);
    arf_init(tmp2_y);
    
    acb_get_real(tmp1_x, ratio);
    acb_get_imag(tmp1_y, ratio);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *ratio_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *ratio_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(z);
    acb_clear(A);
    acb_clear(dA);
    acb_clear(ratio);
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);

}

