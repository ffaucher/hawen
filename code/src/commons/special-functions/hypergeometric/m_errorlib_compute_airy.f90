!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_whittaker.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the Airy function but without
!> library ARB for hypergeometric functions, it only raise ERROR
!
!------------------------------------------------------------------------------
module m_compute_airy
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine returns an error.
  interface compute_Airyfunction_and_derivative
     module procedure compute_Airyfunction_and_derivative_double_complex
  end interface compute_Airyfunction_and_derivative
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine returns an error
  interface compute_Airyfunction_dA_over_A
     module procedure compute_Airyfunction_dA_over_A_double_complex
  end interface compute_Airyfunction_dA_over_A
  !--------------------------------------------------------------------- 
  
  public :: compute_Airyfunction_and_derivative
  public :: compute_Airyfunction_dA_over_A
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Airy for double complex using ARB.
  !
  !> @param[in]   z            : parameters of the Airy functions
  !> @param[out]  A            : Airy(z)
  !> @param[out]  A'           : Airy'(z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_Airyfunction_and_derivative_double_complex(z,A,dA,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: z
    complex(kind=8)    ,intent(out)  :: A, dA
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0
    
    A = 0.d0 ; dA = 0.d0

    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Airy functions cannot be computed "      //&
                    "without ARB library, you need to use the "         //&
                    "DEPENDENCY_ARB flag in the makefile [compute_Airy] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_Airyfunction_and_derivative_double_complex


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Airy A'/A for double complex using ARB.
  !
  !> @param[in]   z            : parameters of the Airy functions
  !> @param[out]  ratio        : Airy'(z) / Airy(z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_Airyfunction_dA_over_A_double_complex(z,ratio,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: z
    complex(kind=8)    ,intent(out)  :: ratio
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0
    
    ratio = 0.d0
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Airy functions cannot be computed "      //&
                    "without ARB library, you need to use the "         //&
                    "DEPENDENCY_ARB flag in the makefile [compute_Airy] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_Airyfunction_dA_over_A_double_complex
end module m_compute_airy
