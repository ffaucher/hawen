!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_bessel_intrinsic.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the bessel function with
!> intrinsic Fortran 
!> WARNING ==> only supports real parameters, therefore, we prefer to 
!>             to rely on ARB library.
!
!------------------------------------------------------------------------------
module m_compute_bessel
  !! module used -------------------------------------------------------
  use m_raise_error,     only : raise_error, t_error
  use, intrinsic             :: ieee_arithmetic
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes Bessel function using ARB
  interface compute_besselJ
     module procedure compute_besselJ_double_complex
     module procedure compute_besselJ_simple_complex
     module procedure compute_besselJ_double_real
     module procedure compute_besselJ_simple_real
     module procedure compute_besselJ_double_complex_intNu
     module procedure compute_besselJ_simple_complex_intNu
     module procedure compute_besselJ_double_real_intNu
     module procedure compute_besselJ_simple_real_intNu
  end interface compute_besselJ
  interface compute_besselY
     module procedure compute_besselY_double_complex
     module procedure compute_besselY_simple_complex
     module procedure compute_besselY_double_real
     module procedure compute_besselY_simple_real
     module procedure compute_besselY_double_complex_intNu
     module procedure compute_besselY_simple_complex_intNu
     module procedure compute_besselY_double_real_intNu
     module procedure compute_besselY_simple_real_intNu
  end interface compute_besselY
  !--------------------------------------------------------------------- 

  public :: compute_besselJ,compute_besselY
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for double complex using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_complex_intNu(nu,z,bessel_val,ctx_err)
    implicit none

    complex(kind=8)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    
    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports real parameters, you have to use "// &
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------

    return
  end subroutine compute_besselJ_double_complex_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for double complex using Intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_complex_intNu(nu,z,bessel_val,ctx_err)
    implicit none

    complex(kind=8)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    
    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if    
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports real parameters, you have to use "// &
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    return

  end subroutine compute_besselY_double_complex_intNu



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for simplex complex using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_complex_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    
    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports real parameters, you have to use "// &
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    return

  end subroutine compute_besselJ_simple_complex_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for simple complex using intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_complex_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports real parameters, you have to use "// &
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------

    return
  end subroutine compute_besselY_simple_complex_intNu


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=4)    ,intent(in)   :: z
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    select case(nu)
      case(0)
        bessel_val = bessel_j0(z)
      case(1)
        bessel_val = bessel_j1(z)
      case default
        bessel_val = bessel_jn(nu,z)
    end select

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_intrinsic] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_simple_real_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using Intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=4)    ,intent(in)   :: z
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    select case(nu)
      case(0)
        bessel_val = bessel_y0(z)
      case(1)
        bessel_val = bessel_y1(z)
      case default
        bessel_val = bessel_yn(nu,z)
    end select

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_simple_real_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=8)    ,intent(in)   :: z
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    select case(nu)
      case(0)
        bessel_val = bessel_j0(z)
      case(1)
        bessel_val = bessel_j1(z)
      case default
        bessel_val = bessel_jn(nu,z)
    end select

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_double_real_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using Intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=8)    ,intent(in)   :: z
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    select case(nu)
      case(0)
        bessel_val = bessel_y0(z)
      case(1)
        bessel_val = bessel_y1(z)
      case default
        bessel_val = bessel_yn(nu,z)
    end select

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_double_real_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for double complex using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_complex(nu,z,bessel_val,ctx_err)
    implicit none

    complex(kind=8)    ,intent(in)   :: z,nu
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    
    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------

    return
  end subroutine compute_besselJ_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for double complex using Intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_complex(nu,z,bessel_val,ctx_err)
    implicit none

    complex(kind=8)    ,intent(in)   :: z,nu
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    
    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    return

  end subroutine compute_besselY_double_complex



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for simplex complex using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_complex(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: z,nu
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    
    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    return

  end subroutine compute_besselJ_simple_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for simple complex using intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_complex(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: z,nu
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------

    return
  end subroutine compute_besselY_simple_complex


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=4)    ,intent(in)   :: z,nu
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    return
  end subroutine compute_besselJ_simple_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using Intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=4)    ,intent(in)   :: z,nu
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_simple_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using Intrinsic: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=8)    ,intent(in)   :: z,nu
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_double_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using Intrinsic: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=8)    ,intent(in)   :: z,nu
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err

    bessel_val = 0.0
    if(real(nu) > 0 .and. real(z)>0) then
      !! to prevent warning compilation
    end if
    
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Bessel intrinsic Fortran function "    //&
                    "only supports integer order, you have to use "   //&
                    " ARB library, compiling with the "               //&
                    "DEPENDENCY_ARB flag in the makefile [compute_besselY] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    !! return error at upper level
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_double_real
end module m_compute_bessel
