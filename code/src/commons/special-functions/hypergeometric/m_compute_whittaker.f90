!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_whittaker.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the whittaker function
!> for complex parameters using the library ARB for hypergeometric
!> functions
!
!------------------------------------------------------------------------------
module m_compute_whittaker
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  !! because arb uses C compilation
  use iso_c_binding
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  Link with the C function of ARB
  interface 

    !! this is for whittakerW at high precision -----------------
    subroutine compute_whittakerW_ARB(a_x,a_y,b_x,b_y,z_x,z_y,u_x,u_y,asymp) &
               bind(C, name="compute_whittakerW_arb")
        use iso_c_binding
        real   (c_double), value  :: a_x, a_y, b_x, b_y, z_x, z_y
        real   (c_double)         :: u_x, u_y
        integer(c_int)   , value  :: asymp
    end subroutine compute_whittakerW_ARB

    !! this is for whittakerM at high precision -----------------
    subroutine compute_whittakerM_ARB(a_x,a_y,b_x,b_y,z_x,z_y,u_x,u_y) &
               bind(C, name="compute_whittakerM_arb")
        use iso_c_binding
        real   (c_double), value  :: a_x, a_y, b_x, b_y, z_x, z_y
        real   (c_double)         :: u_x, u_y
    end subroutine compute_whittakerM_ARB
    
  end interface

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes hypergeometric function using ARB
  interface compute_whittakerW
     module procedure compute_whittakerW_double_complex
     module procedure compute_whittakerW_double_complex_bcx
  end interface
  interface compute_whittakerM
     module procedure compute_whittakerM_double_complex
  end interface
  !--------------------------------------------------------------------- 

  public :: compute_whittakerW, compute_whittakerM
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker \f$W\f$ special function \f$W(a,b,z)\f$ for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]  whittaker     : \f$W(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_whittakerW_double_complex(a,b,z,whittaker,ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: whittaker
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    real(c_double)     :: a_re,a_im,z_re,z_im,b_re,u_re,u_im,b_im

    ctx_err%ierr  = 0

    !! because of C we need to split real and imaginary parts 
    whittaker = 0.0
    a_re = real (a)
    a_im = aimag(a)
    b_re = real (b)
    b_im = 0.0
    z_re = real (z)
    z_im = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_whittakerW_ARB(a_re,a_im,b_re,b_im,z_re,z_im,u_re,u_im,0)
    whittaker = dcmplx(u_re, u_im)

    !! using hypergeometric --------------------------------------------
    !! x    = dcmplx(0.5d0 + b - a)
    !! a_re = real (x)
    !! a_im = aimag(x)
    !! b_re = real (1.d0 + 2.d0* b)
    !! b_im = 0.0
    !! call compute_hypergeometric(a_re,a_im,b_re,z_re, z_im,u_re,u_im)
    !! whittaker = exp( -z / 2.d0 ) * z ** ( b + 0.5d0 ) * dcmplx( u_re, u_im )

    !! -----------------------------------------------------------------
    if(isnan(real(whittaker)) .or. abs(whittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Whittaker W function returns "//&
                      "NaN or Infinity [compute_whittakerW] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_whittakerW_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker \f$W\f$ special function \f$W(a,b,z)\f$ for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]  whittaker     : \f$W(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_whittakerW_double_complex_bcx(a,b,z,whittaker,ctx_err)
    complex(kind=8)    ,intent(in)   :: a,b,z
    complex(kind=8)    ,intent(out)  :: whittaker
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    real(c_double)     :: a_re,a_im,z_re,z_im,b_re,u_re,u_im,b_im

    ctx_err%ierr  = 0

    !! because of C we need to split real and imaginary parts 
    whittaker = 0.0
    a_re = real (a)
    a_im = aimag(a)
    b_re = real (b)
    b_im = aimag(b)
    z_re = real (z)
    z_im = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_whittakerW_ARB(a_re,a_im,b_re,b_im,z_re,z_im,u_re,u_im,0)
    whittaker = dcmplx(u_re, u_im)

    !! using hypergeometric --------------------------------------------
    !! x    = dcmplx(0.5d0 + b - a)
    !! a_re = real (x)
    !! a_im = aimag(x)
    !! b_re = real (1.d0 + 2.d0* b)
    !! b_im = 0.0
    !! call compute_hypergeometric(a_re,a_im,b_re,z_re, z_im,u_re,u_im)
    !! whittaker = exp( -z / 2.d0 ) * z ** ( b + 0.5d0 ) * dcmplx( u_re, u_im )

    !! -----------------------------------------------------------------
    if(isnan(real(whittaker)) .or. abs(whittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Whittaker W function returns "//&
                      "NaN or Infinity [compute_whittakerW] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_whittakerW_double_complex_bcx
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker \f$M\f$ special function \f$M(a,b,z)\f$ for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]  whittaker     : \f$M(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_whittakerM_double_complex(a,b,z,whittaker,ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: whittaker
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    real(c_double)     :: a_re,a_im,z_re,z_im,b_re,u_re,u_im,b_im

    ctx_err%ierr  = 0
    whittaker = 0.0

    !! because of C we need to split real and imaginary parts 
    whittaker = 0.0
    a_re = real (a)
    a_im = aimag(a)
    b_re = real (b)
    b_im = 0.0
    z_re = real (z)
    z_im = aimag(z)
    call compute_whittakerM_ARB(a_re,a_im,b_re,b_im,z_re,z_im,u_re,u_im)
    whittaker = dcmplx(u_re, u_im)

    !! -----------------------------------------------------------------
    if(isnan(real(whittaker)) .or. abs(whittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Whittaker M function returns "//&
                      "NaN or Infinity [compute_whittakerM] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_whittakerM_double_complex

end module m_compute_whittaker
