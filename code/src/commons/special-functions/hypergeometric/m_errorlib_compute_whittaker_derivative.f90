!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_whittaker.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the whittaker function but without
!> library ARB for hypergeometric functions, it only raise ERROR
!
!------------------------------------------------------------------------------
module m_compute_whittaker_derivative
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine returns error without ARB
  interface compute_dwhittakerW
     module procedure compute_dwhittakerW_double_complex
  end interface
  interface compute_dwhittakerM
     module procedure compute_dwhittakerM_double_complex
  end interface
  interface compute_dwhittakerW_over_W
     module procedure compute_dwhittakerW_over_W_double_complex
     module procedure compute_dwhittakerW_over_W_double_complex_bcx
  end interface
  !--------------------------------------------------------------------- 

  public :: compute_dwhittakerW, compute_dwhittakerM, compute_dwhittakerW_over_W
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker returns error because it needs ARB
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]  whittaker     : \f$W(a,b,z)\f$
  !> @param[out]  dw            : \f$W'(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerW_double_complex(a,b,z,whittaker,dw,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: whittaker,dw
    type(t_error)      ,intent(out)  :: ctx_err


    !! to prevent warning compilation only 
    if(real(a) < real(b) .and. real(z) > real(b)) then
    endif
    
    whittaker     = 0.0
    dw            = 0.0
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Whittaker W' function cannot be computed "//&
                    "without ARB library, you need to use the "          //&
                    "DEPENDENCY_ARB flag in the makefile [compute_whittaker] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------
    return
  end subroutine compute_dwhittakerW_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker returns error because it needs ARB
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]  whittaker     : \f$M(a,b,z)\f$
  !> @param[out]  dw            : \f$M'(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerM_double_complex(a,b,z,whittaker,dw,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: whittaker,dw
    type(t_error)      ,intent(out)  :: ctx_err


    !! to prevent warning compilation only 
    if(real(a) < real(b) .and. real(z) > real(b)) then
    endif
    
    whittaker     = 0.0
    dw            = 0.0
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Whittaker M' function cannot be computed "//&
                    "without ARB library, you need to use the "          //&
                    "DEPENDENCY_ARB flag in the makefile [compute_whittaker] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_dwhittakerM_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the ratio whittaker \f$W'(a,b,z)/W(a,b,z) \f$  special 
  !> function for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a,b,z         : coefficients for the computations
  !> @param[out]  ratio         :  \f$W'(a,b,z)/W(a,b,z) \f$ 
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerW_over_W_double_complex(a,b,z,ratio_whittaker, &
                                                       ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: ratio_whittaker
    type(t_error)      ,intent(out)  :: ctx_err

    ratio_whittaker = 0.d0
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Whittaker W'/W function cannot be computed "//&
                    "without ARB library, you need to use the "            //&
                    "DEPENDENCY_ARB flag in the makefile [compute_whittaker] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------

    return
  end subroutine compute_dwhittakerW_over_W_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the ratio whittaker \f$W'(a,b,z)/W(a,b,z) \f$  special 
  !> function for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a,b,z         : coefficients for the computations
  !> @param[out]  ratio         :  \f$W'(a,b,z)/W(a,b,z) \f$ 
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerW_over_W_double_complex_bcx(a,b,z,           &
                                                           ratio_whittaker, &
                                                           ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z,b
    complex(kind=8)    ,intent(out)  :: ratio_whittaker
    type(t_error)      ,intent(out)  :: ctx_err

    ratio_whittaker = 0.d0
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Whittaker W'/W function cannot be computed "//&
                    "without ARB library, you need to use the "            //&
                    "DEPENDENCY_ARB flag in the makefile [compute_whittaker] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------

    return
  end subroutine compute_dwhittakerW_over_W_double_complex_bcx
end module m_compute_whittaker_derivative
