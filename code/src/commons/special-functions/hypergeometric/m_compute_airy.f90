!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_airy.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the Airy functions
!> for complex parameters using the library ARB for 
!> hypergeometric functions
!> Fortran intrinsic does not exist...
!
!------------------------------------------------------------------------------
module m_compute_airy
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use, intrinsic            :: ieee_arithmetic
  !! because arb uses C compilation
  use iso_c_binding
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  Link with the C function of ARB
  interface 
    !! this is for complex Airy and derivatives at high precision
    subroutine compute_Airy_ARB(z_x,z_y,A_x,A_y,dA_x,dA_y)       &
               bind(C, name="compute_airyA_and_derivative_arb")
        use iso_c_binding
        real   (c_double), value  :: z_x, z_y
        real   (c_double)         :: A_x, A_y, dA_x, dA_y
    end subroutine compute_Airy_ARB
    !! this is for complex A'/A
    subroutine compute_dA_over_A_ARB(z_x,z_y,ratio_x,ratio_y)       &
               bind(C, name="compute_airy_dA_over_A_arb")
        use iso_c_binding
        real   (c_double), value  :: z_x, z_y
        real   (c_double)         :: ratio_x, ratio_y
    end subroutine compute_dA_over_A_ARB

  end interface

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes Airy and its derivative using ARB
  interface compute_Airyfunction_and_derivative
     module procedure compute_Airyfunction_and_derivative_double_complex
  end interface compute_Airyfunction_and_derivative

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes Airy A'/A using ARB
  interface compute_Airyfunction_dA_over_A
     module procedure compute_Airyfunction_dA_over_A_double_complex
  end interface compute_Airyfunction_dA_over_A
  !--------------------------------------------------------------------- 

  public :: compute_Airyfunction_and_derivative
  public :: compute_Airyfunction_dA_over_A
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Airy for double complex using ARB.
  !
  !> @param[in]   z            : parameters of the Airy functions
  !> @param[out]  A            : Airy(z)
  !> @param[out]  A'           : Airy'(z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_Airyfunction_and_derivative_double_complex(z,A,dA,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: z
    complex(kind=8)    ,intent(out)  :: A, dA
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: z_re,z_im,a_re,a_im,da_re,da_im

    ctx_err%ierr  = 0
    
    A = 0.d0 ; dA = 0.d0
    
    !! because of C we need to split real and imaginary parts 
    z_re  = real (z)
    z_im  = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_Airy_ARB(z_re,z_im,a_re,a_im,da_re,da_im)
    A  = dcmplx( a_re,  a_im)
    dA = dcmplx(da_re, da_im)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real( A)) .or. abs( A) > Huge(1.D0) .or.             &
       ieee_is_nan(real(dA)) .or. abs(dA) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Airy function returns " // &
                      "NaN or Infinity [compute_Airy_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_Airyfunction_and_derivative_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Airy A'/A for double complex using ARB.
  !
  !> @param[in]   z            : parameters of the Airy functions
  !> @param[out]  ratio        : Airy'(z) / Airy(z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_Airyfunction_dA_over_A_double_complex(z,ratio,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: z
    complex(kind=8)    ,intent(out)  :: ratio
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: z_re,z_im,r_re,r_im

    ctx_err%ierr  = 0
    
    ratio = 0.d0
    
    !! because of C we need to split real and imaginary parts 
    z_re  = real (z)
    z_im  = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_dA_over_A_ARB(z_re,z_im,r_re,r_im)
    ratio = dcmplx(r_re, r_im)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(ratio)) .or. abs(ratio) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Airy function returns " // &
                      "NaN or Infinity [compute_Airy_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_Airyfunction_dA_over_A_double_complex
  
end module m_compute_airy
