!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_bessel.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the bessel function with
!> for complex parameters using the library ARB for hypergeometric
!> functions
!> Fortran intrinsic only supports real parameters...
!
!------------------------------------------------------------------------------
module m_compute_bessel
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use, intrinsic            :: ieee_arithmetic
  !! because arb uses C compilation
  use iso_c_binding
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  Link with the C function of ARB
  interface 
    !! this is for complex Bessel at high precision
    subroutine compute_besselJ_ARB(nu_x,nu_y,z_x,z_y,out_x,out_y)       &
               bind(C, name="compute_bessel_j")
        use iso_c_binding
        real   (c_double), value  :: nu_x, nu_y, z_x, z_y
        real   (c_double)         :: out_x, out_y
    end subroutine compute_besselJ_ARB

    !! this is for complex Bessel at high precision
    subroutine compute_besselY_ARB(nu_x,nu_y,z_x,z_y,out_x,out_y)       &
               bind(C, name="compute_bessel_y")
        use iso_c_binding
        real   (c_double), value  :: nu_x, nu_y, z_x, z_y
        real   (c_double)         :: out_x, out_y
    end subroutine compute_besselY_ARB

  end interface

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes Bessel function using ARB
  interface compute_besselJ
     module procedure compute_besselJ_double_complex
     module procedure compute_besselJ_simple_complex
     module procedure compute_besselJ_double_real
     module procedure compute_besselJ_simple_real
     module procedure compute_besselJ_double_complex_intNu
     module procedure compute_besselJ_simple_complex_intNu
     module procedure compute_besselJ_double_real_intNu
     module procedure compute_besselJ_simple_real_intNu
  end interface compute_besselJ
  interface compute_besselY
     module procedure compute_besselY_double_complex
     module procedure compute_besselY_simple_complex
     module procedure compute_besselY_double_real
     module procedure compute_besselY_simple_real
     module procedure compute_besselY_double_complex_intNu
     module procedure compute_besselY_simple_complex_intNu
     module procedure compute_besselY_double_real_intNu
     module procedure compute_besselY_simple_real_intNu
  end interface compute_besselY
  !--------------------------------------------------------------------- 

  public :: compute_besselJ,compute_besselY
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for double complex using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_complex(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: nu,z
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = real (nu)
    nu_im = aimag(nu)
    z_re  = real (z)
    z_im  = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dcmplx(u_re, u_im)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for double complex using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_complex(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: nu,z
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = real (nu)
    nu_im = aimag(nu)
    z_re  = real (z)
    z_im  = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dcmplx(u_re, u_im)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_double_complex



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for double complex using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_complex(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: nu,z
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(real (nu))
    nu_im = dble(aimag(nu))
    z_re  = dble(real (z) )
    z_im  = dble(aimag(z) )
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = cmplx(real(u_re,kind=4), real(u_im,kind=4))

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_simple_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for double complex using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_complex(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: nu,z
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(real (nu))
    nu_im = dble(aimag(nu))
    z_re  = dble(real (z) )
    z_im  = dble(aimag(z) )
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = cmplx(real(u_re,kind=4), real(u_im,kind=4))

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_simple_complex


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=4)    ,intent(in)   :: nu,z
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = real(u_re,kind=4)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_simple_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=4)    ,intent(in)   :: nu,z
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = real(u_re,kind=4)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_simple_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=8)    ,intent(in)   :: nu,z
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dble(u_re)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_double_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_real(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=8)    ,intent(in)   :: nu,z
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dble(u_re)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_double_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for double complex using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_complex_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = real (nu)
    nu_im = 0.0
    z_re  = real (z)
    z_im  = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dcmplx(u_re, u_im)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_double_complex_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for double complex using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_complex_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = real (nu)
    nu_im = 0.0
    z_re  = real (z)
    z_im  = aimag(z)
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dcmplx(u_re, u_im)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_double_complex_intNu



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for double complex using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_complex_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(real (nu))
    nu_im = 0.0
    z_re  = dble(real (z) )
    z_im  = dble(aimag(z) )
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = cmplx(real(u_re,kind=4), real(u_im,kind=4))

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_simple_complex_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for double complex using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_complex_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    complex(kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(real (nu))
    nu_im = 0.0
    z_re  = dble(real (z) )
    z_im  = dble(aimag(z) )
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = cmplx(real(u_re,kind=4), real(u_im,kind=4))

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_simple_complex_intNu


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_simple_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=4)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = real(u_re,kind=4)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_simple_real_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_simple_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=4)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=4)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = real(u_re,kind=4)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_simple_real_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel J for real using ARB: BesselJ_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselJ_double_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=8)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselJ_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dble(u_re)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.0)) then
      ctx_err%msg   = "** ERROR: BesselJ function returns " // &
                      "NaN or Infinity [compute_besselJ_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselJ_double_real_intNu

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Bessel Y for real using ARB: BesselY_\nu(z)
  !
  !> @param[in]   nu,z         : parameters of the bessel
  !> @param[out]  bessel       : Bessel(nu,z)
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine compute_besselY_double_real_intNu(nu,z,bessel_val,ctx_err)

    implicit none

    real   (kind=8)    ,intent(in)   :: z
    integer(kind=4)    ,intent(in)   :: nu
    real   (kind=8)    ,intent(out)  :: bessel_val
    type(t_error)      ,intent(out)  :: ctx_err
    !! local C variable
    real(c_double)     :: nu_re,nu_im,z_re,z_im,u_re,u_im

    ctx_err%ierr  = 0
    !! because of C we need to split real and imaginary parts 
    bessel_val = 0.0
    nu_re = dble(nu)
    nu_im = 0.d0
    z_re  = dble(z)
    z_im  = 0.d0
    !! 0 because we do not use asymptot.
    call compute_besselY_ARB(nu_re,nu_im,z_re,z_im,u_re,u_im)
    bessel_val = dble(u_re)

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(bessel_val)) .or. abs(bessel_val) > Huge(1.0)) then
      ctx_err%msg   = "** ERROR: BesselY function returns " // &
                      "NaN or Infinity [compute_besselY_arb] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! upper level to raise error for everyone call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_besselY_double_real_intNu

end module m_compute_bessel
