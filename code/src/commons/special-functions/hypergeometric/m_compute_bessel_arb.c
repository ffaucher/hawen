/* This are the C routine to compute 
 *  -> Bessel functions with Complex parameters
 * using ARB library.
 * */
#include "acb.h"
#include "arb.h"
#include "arf.h"
#include "acb_hypgeom.h"

#define PREC_MIN 128

void compute_bessel_y(const double nu_x, const double nu_y, 
                      const double z_x, const double z_y, 
                      double * res_x, double * res_y)
{

    slong prec = PREC_MIN;
    acb_t nu, z, res;
    arb_t res_imag, res_re;
    arf_t tmp_x, tmp_y;
    /* initialisation */
    acb_init(nu); 
    acb_init(z);
    acb_init(res);

    arb_init(res_imag);
    arb_init(res_re);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* 
     * z <- z_x + i * z_y
     * nu <- nu_x + i * nu_y
     */
    acb_set_d_d(nu, nu_x, nu_y);
    acb_set_d_d(z, z_x, z_y);

    acb_hypgeom_bessel_y(res, nu, z, prec);

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(nu); 
    acb_clear(z);
    acb_clear(res);

    arb_clear(res_imag);
    arb_clear(res_re);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}

void compute_bessel_j(const double nu_x, const double nu_y, 
                      const double z_x, const double z_y, 
                      double * res_x, double * res_y)
{

    slong prec = PREC_MIN;
    acb_t nu, z, res;
    arb_t res_imag, res_re;
    arf_t tmp_x, tmp_y;
    /* initialisation */
    acb_init(nu); 
    acb_init(z);
    acb_init(res);

    arb_init(res_imag);
    arb_init(res_re);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* 
     * z <- z_x + i * z_y
     * nu <- nu_x + i * nu_y
     */
    acb_set_d_d(nu, nu_x, nu_y);
    acb_set_d_d(z, z_x, z_y);

    acb_hypgeom_bessel_j(res, nu, z, prec);

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(nu); 
    acb_clear(z);
    acb_clear(res);

    arb_clear(res_imag);
    arb_clear(res_re);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}
