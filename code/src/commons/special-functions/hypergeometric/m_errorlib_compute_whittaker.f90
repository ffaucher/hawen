!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_whittaker.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the whittaker function but without
!> library ARB for hypergeometric functions, it only raise ERROR
!
!------------------------------------------------------------------------------
module m_compute_whittaker
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine returns error without ARB
  interface compute_whittakerW
     module procedure compute_whittakerW_double_complex
  end interface
  interface compute_whittakerM
     module procedure compute_whittakerM_double_complex
  end interface
  !--------------------------------------------------------------------- 

  public :: compute_whittakerW, compute_whittakerM
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker returns error because it needs ARB
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]  whittaker     : \f$W(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_whittakerW_double_complex(a,b,z,whittaker,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: whittaker
    type(t_error)      ,intent(out)  :: ctx_err


    !! to prevent warning compilation only 
    if(real(a) < real(b) .and. real(z) > real(b)) then
    endif
    
    whittaker     = 0.0
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Whittaker W function cannot be computed "//&
                    "without ARB library, you need to use the "         //&
                    "DEPENDENCY_ARB flag in the makefile [compute_whittaker] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------
    return
  end subroutine compute_whittakerW_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker returns error because it needs ARB
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]  whittaker     : \f$M(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_whittakerM_double_complex(a,b,z,whittaker,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: whittaker
    type(t_error)      ,intent(out)  :: ctx_err


    !! to prevent warning compilation only 
    if(real(a) < real(b) .and. real(z) > real(b)) then
    endif
    
    whittaker     = 0.0
    !! -----------------------------------------------------------------
    ctx_err%msg   = "** ERROR: Whittaker M function cannot be computed "//&
                    "without ARB library, you need to use the "         //&
                    "DEPENDENCY_ARB flag in the makefile [compute_whittaker] **"
    ctx_err%ierr  = -100
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_whittakerM_double_complex

end module m_compute_whittaker
