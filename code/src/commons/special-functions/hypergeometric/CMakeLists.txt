if (ARB_FOUND)
  set(special_functions_sources
  m_compute_whittaker_arb.c
  m_compute_whittaker.f90
  m_compute_whittaker_derivative.f90
  m_compute_bessel_arb.c
  m_compute_bessel.f90
  m_compute_airy_arb.c
  m_compute_airy.f90
  )
  add_library(special_functions ${special_functions_sources})
  target_link_libraries(special_functions ${ARB_LIB} ${FLINT_LIB} raise_error) 
  install(TARGETS special_functions 
          ARCHIVE DESTINATION lib
          LIBRARY DESTINATION lib)
  target_include_directories(special_functions PRIVATE ${ARB_INCLUDE_PATH} ${FLINT_INCLUDE_PATH})
else() # without ARB
  set(special_functions_sources 
  m_errorlib_compute_whittaker.f90
  m_errorlib_compute_whittaker_derivative.f90
  m_errorlib_compute_airy.f90
  m_compute_bessel_intrinsic.f90
  )
  add_library(special_functions ${special_functions_sources})
  target_link_libraries(special_functions raise_error) 
  install(TARGETS special_functions 
          ARCHIVE DESTINATION lib
          LIBRARY DESTINATION lib)

endif()
