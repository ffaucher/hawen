!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_whittaker_derivative.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the derivative of whittaker functions
!> \f$W\f$ and \f$M\f$for complex parameters using the library ARB for hypergeometric
!> functions
!
!------------------------------------------------------------------------------
module m_compute_whittaker_derivative
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use, intrinsic            :: ieee_arithmetic
  !! because arb uses C compilation
  use iso_c_binding
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  Link with the C function of ARB
  interface 

    !! this is for whittakerW at high precision -----------------
    subroutine compute_whittakerW_ARB(a_x,a_y,b_x,b_y,z_x,z_y,u_x,u_y,asymp) &
               bind(C, name="compute_whittakerW_arb")
        use iso_c_binding
        real   (c_double), value  :: a_x, a_y, b_x, b_y, z_x, z_y
        real   (c_double)         :: u_x, u_y
        integer(c_int)   , value  :: asymp
    end subroutine compute_whittakerW_ARB

    !! this is for whittakerM at high precision -----------------
    subroutine compute_whittakerM_ARB(a_x,a_y,b_x,b_y,z_x,z_y,u_x,u_y) &
               bind(C, name="compute_whittakerM_arb")
        use iso_c_binding
        real   (c_double), value  :: a_x, a_y, b_x, b_y, z_x, z_y
        real   (c_double)         :: u_x, u_y
    end subroutine compute_whittakerM_ARB

    !! this is for the ratio directly
    !! with high precision ---------------------------------------------
    subroutine compute_whittakerWprime_over_W_ARB(a_x,a_y,b_x,b_y,z_x,  &
                                                  z_y,u_x,u_y) &
               bind(C, name="compute_wprime_over_w_arb")
        use iso_c_binding
        real   (c_double), value  :: a_x, a_y, b_x, b_y, z_x, z_y
        real   (c_double)         :: u_x, u_y
    end subroutine compute_whittakerWprime_over_W_ARB
    
  end interface

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes Whittaker \f$W\f$ function using ARB
  interface compute_dwhittakerW
     module procedure compute_dwhittakerW_double_complex
  end interface
  !>  The subroutine computes Whittaker \f$M\f$ function using ARB
  interface compute_dwhittakerM
     module procedure compute_dwhittakerM_double_complex
  end interface
  !>  The subroutine computes Whittaker function using ARB
  interface compute_dwhittakerW_over_W
     module procedure compute_dwhittakerW_over_W_double_complex
     module procedure compute_dwhittakerW_over_W_double_complex_bcx
  end interface
  !--------------------------------------------------------------------- 

  public :: compute_dwhittakerW, compute_dwhittakerM, compute_dwhittakerW_over_W
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker \f$W\f$ and \f$W'\f$ special function for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]   whittaker    : \f$W (a,b,z)\f$
  !> @param[out]  dwhittaker    : \f$W'(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerW_double_complex(a,b,z,whittaker,        &
                                                dwhittaker,ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  ::  whittaker
    complex(kind=8)    ,intent(out)  :: dwhittaker
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    complex(kind=8)    :: whittaker1 
    real(c_double)     :: a_re,a_im,z_re,z_im,b_re,u_re,u_im,b_im

    ctx_err%ierr  = 0

    !! because of C we need to split real and imaginary parts 
     whittaker = 0.0
    dwhittaker = 0.0
    a_re = real (a)
    a_im = aimag(a)
    b_re = dble (b)
    b_im = 0.0
    z_re = real (z)
    z_im = aimag(z)

    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    !! evaluation in W(a,b,z) and W(a+1,b,z) for derivative
    !! 0 because we do not use asymptot.
    call compute_whittakerW_ARB(a_re,a_im,b_re,b_im,z_re,z_im,u_re,u_im,0)
    whittaker = dcmplx(u_re, u_im)
    call compute_whittakerW_ARB(1.+a_re,a_im,b_re,b_im,z_re,z_im,u_re,u_im,0)
    whittaker1= dcmplx(u_re, u_im)

    dwhittaker = -whittaker1 / z + (0.5 - a/z) * whittaker

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(whittaker)) .or. abs(whittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Whittaker W function returns "//&
                      "NaN or Infinity [compute_dwhittakerW] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(dwhittaker)) .or. abs(dwhittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Whittaker W' function returns "//&
                      "NaN or Infinity [compute_dwhittakerW] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------    
    return
  end subroutine compute_dwhittakerW_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute whittaker \f$M(a,b,z)\f$ and \f$M'(a,b,z)\f$ special functions for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a             : coefficient for the computations
  !> @param[in]   b             : coefficient for the computations
  !> @param[in]   z             : coefficient for the computations
  !> @param[out]   whittaker    : \f$M(a,b,z)\f$
  !> @param[out]  dwhittaker    : \f$M'(a,b,z)\f$
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerM_double_complex(a,b,z,whittaker,        &
                                                dwhittaker,ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  ::  whittaker
    complex(kind=8)    ,intent(out)  :: dwhittaker
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    complex(kind=8)    :: whittaker1
    real   (kind=8)    :: lmode
    real(c_double)     :: a_re,a_im,z_re,z_im,b_re,u_re,u_im,b_im

    ctx_err%ierr  = 0
     whittaker = 0.0
    dwhittaker = 0.0

    !! because of C we need to split real and imaginary parts 
    whittaker = 0.0
    a_re = real (a)
    a_im = aimag(a)
    b_re = dble (b)
    b_im = 0.0
    z_re = real (z)
    z_im = aimag(z)

    !! -----------------------------------------------------------------
    !! evaluation in 
    !! M(a-1/2,b-1/2=l,z) 
    !! M(a    ,b=l+1/2,z) for derivative
    call compute_whittakerM_ARB(a_re,a_im,b_re,b_im,z_re,z_im,u_re,u_im)
    whittaker = dcmplx(u_re, u_im)
    call compute_whittakerM_ARB(a_re-0.5,a_im,b_re-0.5,b_im,z_re,z_im,u_re,u_im)
    whittaker1= dcmplx(u_re, u_im)

    lmode = b_re - 0.5 !! b = l + 0.5 
    dwhittaker = (-0.5 - lmode/z) * whittaker + 1. / sqrt(z) * whittaker1

    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(whittaker)) .or. abs(whittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Whittaker M function returns "//&
                      "NaN or Infinity [compute_dwhittakerM] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(dwhittaker)) .or. abs(dwhittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Whittaker M' function returns "//&
                      "NaN or Infinity [compute_dwhittakerM] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine compute_dwhittakerM_double_complex


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the ratio whittaker \f$W'(a,b,z)/W(a,b,z) \f$  special 
  !> function for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a,b,z         : coefficients for the computations
  !> @param[out]  ratio         :  \f$W'(a,b,z)/W(a,b,z) \f$ 
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerW_over_W_double_complex(a,b,z,ratio_whittaker, &
                                                       ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z
    real   (kind=8)    ,intent(in)   :: b
    complex(kind=8)    ,intent(out)  :: ratio_whittaker
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    real(c_double)     :: a_re,a_im,z_re,z_im,b_re,u_re,u_im,b_im

    ctx_err%ierr  = 0

    !! because of C we need to split real and imaginary parts 
    ratio_whittaker = 0.d0
    a_re = real (a)
    a_im = aimag(a)
    b_re = dble (b)
    b_im = 0.0
    z_re = real (z)
    z_im = aimag(z)

    !! evaluation 
    call compute_whittakerWprime_over_W_ARB(a_re,a_im,b_re,b_im,        &
                                            z_re,z_im,u_re,u_im)
    ratio_whittaker = dcmplx(u_re, u_im)
    
    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(ratio_whittaker)) .or. abs(ratio_whittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Ratio Whittaker W'/W returns "//&
                      "NaN or Infinity [compute_dwhittakerW_over_W] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------    
    return
  end subroutine compute_dwhittakerW_over_W_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the ratio whittaker \f$W'(a,b,z)/W(a,b,z) \f$  special 
  !> function for double complex
  !> using ARB hypergeometric functions
  !
  !> @param[in]   a,b,z         : coefficients for the computations
  !> @param[out]  ratio         :  \f$W'(a,b,z)/W(a,b,z) \f$ 
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine compute_dwhittakerW_over_W_double_complex_bcx(a,b,z,       &
                                               ratio_whittaker,ctx_err)
    complex(kind=8)    ,intent(in)   :: a,z,b
    complex(kind=8)    ,intent(out)  :: ratio_whittaker
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    real(c_double)     :: a_re,a_im,z_re,z_im,b_re,u_re,u_im,b_im

    ctx_err%ierr  = 0

    !! because of C we need to split real and imaginary parts 
    ratio_whittaker = 0.d0
    a_re = real (a)
    a_im = aimag(a)
    b_re = real (b)
    b_im = aimag(b)
    z_re = real (z)
    z_im = aimag(z)

    !! evaluation 
    call compute_whittakerWprime_over_W_ARB(a_re,a_im,b_re,b_im,        &
                                            z_re,z_im,u_re,u_im)
    ratio_whittaker = dcmplx(u_re, u_im)
    
    !! -----------------------------------------------------------------
    if(ieee_is_nan(real(ratio_whittaker)) .or. abs(ratio_whittaker) > Huge(1.D0)) then
      ctx_err%msg   = "** ERROR: Ratio Whittaker W'/W returns "//&
                      "NaN or Infinity [compute_dwhittakerW_over_W] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------    
    return
  end subroutine compute_dwhittakerW_over_W_double_complex_bcx
end module m_compute_whittaker_derivative
