/* This are the C routine to compute 
 *  -> hypergeometric functions 
 *  -> Whittaker function at arbitrary precision
 * using ARB library.
 * */
#include "acb.h"
#include "acb_hypgeom.h"
#include "arb.h"
#include "arf.h"


int const PREC_MAX=16384; // = 1024*16 
int const PREC_MIN=   64;
int const N_ASYMP =   20;

void compute_hypergeometric(const double m, const double n, const double z_x, const double z_y, double * res_x, double *res_y)
{
    slong prec = PREC_MIN;

    acb_t m_c, n_c, z, res;
    arb_t res_re, res_imag;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(m_c); 
    acb_init(n_c);
    acb_init(z);
    acb_init(res);
   
    arb_init(res_re);
    arb_init(res_imag);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y
     * m_c <- m + i * 0
     * n_c <- n + i * 0
     */
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(m_c, m, 0.);
    acb_set_d_d(n_c, n, 0.);
    
    /* computes the U(m, n, z) function */
    acb_hypgeom_u(res, m_c, n_c, z, prec);

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(m_c); 
    acb_clear(n_c);
    acb_clear(z);
    acb_clear(res);

    arb_clear(res_re); 
    arb_clear(res_imag);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
    flint_cleanup();
}



void whittakerW_with_prec(const double k_x, const double k_y, 
                       const double m_x, const double m_y,
                       const double z_x, const double z_y, 
                       acb_t *res, slong prec,  const int asymp)
{

    acb_t k, m, z, t, u, v, a, b, two;
    arb_t half;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(k); 
    acb_init(m);
    acb_init(z);
    acb_init(*res);
    acb_init(t);
    acb_init(u);
    acb_init(v);
    acb_init(a);
    acb_init(b);
    acb_init(two);
 
    arb_init(half);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y
     * k <- k_x + i * k_y
     * m <- m_x + i * m_y
     * half = 1/2
     */
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(k, k_x, k_y);
    acb_set_d_d(m, m_x, m_y);
    acb_set_d_d(two, 2., 0.);
    arb_set_d(half, 0.5);

    /* u = m + 1/2 */
    acb_add_arb(u, m, half, prec);  
     
    /* a = 1/2 + m - k */
    acb_sub(a, u, k, prec);

    /* b = 1 + 2m*/
    acb_one(b);
    acb_addmul(b, two, m, prec);

    /* computes the U(a, b, z) function */
    if (asymp == 0) 
    {
        acb_hypgeom_u(*res, a, b, z, prec);
    }
    else
    {
       if (acb_hypgeom_u_use_asymp(z, prec))
       {
           printf("WARNING : we use asymptotical but it does not seems to be necessary\n");
       }
       acb_hypgeom_u_asymp(*res, a, b, z, N_ASYMP, prec); /* Kontuz : normally it computes U*(a,b, z) = z^{a} U(a,b,z) */
    }


    /* computes t = z^{m+1/2}$ */
    acb_pow(t, z, u, prec);

    /* computes  u = exp(-z/2) */
    acb_neg(a, z);
    acb_mul_arb(v, a, half, prec);
    acb_exp(u, v, prec);

    /*  v = exp(-z/2) * z^{m+1/2} */
    acb_mul(v, u, t, prec);

    /* t  = exp(z/2) * z^{m+1/2} * U(a, b, z)*/
    acb_mul(t, v, *res, prec);

    acb_set(*res, t);

    /* cleaning */  
    acb_clear(m); 
    acb_clear(k);
    acb_clear(z);
    acb_clear(t);
    acb_clear(u);
    acb_clear(v);
    acb_clear(a);
    acb_clear(b);
    acb_clear(two);

    arb_clear(half);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}

/* 
 * computes the whittaker functions W_{k,m}(z) = exp(-z/2) * z^{m+1/2} * U(1/2 +m - k , 1 + 2m, z) 
 * where U is the * Confluent Hypergeometric Function of the Second Kind
 * [http://mathworld.wolfram.com/ConfluentHypergeometricFunctionoftheSecondKind.html]
 * 
 * Input parameters : 
 * -----------------
 * k = k_x + i * k_y
 * m = m_x + i * m_y
 * z = z_x + i * z_y
 * asymp : int (representing a boolean) to use or not asymptotical evaluation of U
 * Ouput parameter :
 * ----------------
 * W_{k,m}(z) = res_x + i * res_y
 *
 */

void compute_whittakerW_arb(const double k_x, const double k_y, 
                       const double m_x, const double m_y,
                       const double z_x, const double z_y, 
                       double * res_x, double *res_y, 
                       const int asymp)
{
    slong prec;
    acb_t res;
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(res);

    arb_init(tmp1_x);
    arb_init(tmp1_y);

    arf_init(tmp2_x);
    arf_init(tmp2_y);

    acb_init(res);
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        whittakerW_with_prec(k_x, k_y, m_x, m_y, z_x, z_y, &res, prec, asymp);
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("******************** WARNING ******************** \n");
        printf("* PREC_MAX=%d reached, Whittaker probably false   \n",PREC_MAX);
        printf("******************** WARNING ******************** \n");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(tmp1_x, res);
    acb_get_imag(tmp1_y, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(res);
    
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);
    flint_cleanup();
}



static void whittakerM_with_prec(const double k_x, const double k_y, 
                                  const double m_x, const double m_y,
                                  const double z_x, const double z_y, 
                                  acb_t *res, slong prec)
{

    acb_t k, m, z, t, u, v, a, b, two;
    arb_t half;
    arf_t tmp_x, tmp_y;
    /* use M(a,b,z)=\frac{_1F_1(a,b,z)}{Γ(b)} */
    int regularized = 1; 

    /* initialisation */
    acb_init(k); 
    acb_init(m);
    acb_init(z);
    acb_init(*res);
    acb_init(t);
    acb_init(u);
    acb_init(v);
    acb_init(a);
    
    acb_init(b);
    acb_init(two);
 
    arb_init(half);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y
     * k <- k_x + i * k_y
     * m <- m_x + i * m_y
     * half = 1/2
     */
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(k, k_x, k_y);
    acb_set_d_d(m, m_x, m_y);
    acb_set_d_d(two, 2., 0.);
    arb_set_d(half, 0.5);

    /* u = m + 1/2 */
    acb_add_arb(u, m, half, prec);  
     
    /* a = 1/2 + m - k */
    acb_sub(a, u, k, prec);

    /* b = 1 + 2m*/
    acb_one(b);
    acb_addmul(b, two, m, prec);

    /* computes the U(a, b, z) function */
    acb_hypgeom_m(*res, a, b, z, regularized, prec);

    /* computes t = z^{m+1/2}$ */
    acb_pow(t, z, u, prec);

    /* computes  u = exp(-z/2) */
    acb_neg(a, z);
    acb_mul_arb(v, a, half, prec);
    acb_exp(u, v, prec);

    /*  v = exp(-z/2) * z^{m+1/2} */
    acb_mul(v, u, t, prec);

    /* t  = exp(z/2) * z^{m+1/2} * U(a, b, z)*/
    acb_mul(t, v, *res, prec);

    acb_set(*res, t);

    /* cleaning */  
    acb_clear(m); 
    acb_clear(k);
    acb_clear(z);
    acb_clear(t);
    acb_clear(u);
    acb_clear(v);
    acb_clear(a);
    acb_clear(b);
    acb_clear(two);

    arb_clear(half);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}

/* 
 * computes the whittaker function M_{k,m}(z) = exp(-z/2) * z^{m+1/2} * M(1/2 +m - k , 1 + 2m, z) 
 * where M(a,b,z) is the * Confluent Hypergeometric Function of the First Kind normalized by г(b)
 * 
 * Input parameters : 
 * -----------------
 * k = k_x + i * k_y
 * m = m_x + i * m_y
 * z = z_x + i * z_y
 * Ouput parameter :
 * ----------------
 * M_{k,m}(z) = res_x + i * res_y
 *
 */

void compute_whittakerM_arb(const double k_x, const double k_y, 
                            const double m_x, const double m_y,
                            const double z_x, const double z_y, 
                            double * res_x, double *res_y)
{
    slong prec;
    acb_t res;
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(res);

    arb_init(tmp1_x);
    arb_init(tmp1_y);

    arf_init(tmp2_x);
    arf_init(tmp2_y);

    acb_init(res);
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        whittakerM_with_prec(k_x, k_y, m_x, m_y, z_x, z_y, &res, prec);
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("******************** WARNING ******************** \n");
        printf("* PREC_MAX=%d reached, Whittaker probably false   \n",PREC_MAX);
        printf("******************** WARNING ******************** \n");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(tmp1_x, res);
    acb_get_imag(tmp1_y, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(res);
    
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);
    flint_cleanup();
}


/* 
 * computes the whittaker ratio W'_{k,m}(z) / W{k,m}(z) = (1/2 - (m-1/2)/z) - 1/sqrt(z) * W_{k+1/2,m-1/2}(z)/W_{k,m}(z)
 * 
 * Input parameters : 
 * -----------------
 * k = k_x + i * k_y
 * m = m_x + i * m_y
 * z = z_x + i * z_y
 *
 */

void compute_wprime_over_w_arb(const double k_x, const double k_y, 
                               const double m_x, const double m_y,
                               const double z_x, const double z_y, 
                               double * res_x, double *res_y)
{
    slong prec;
    acb_t      a, b, z ;                // whittaker parameters
    acb_t      w1, w2, wprime ;         // whittaker values
    acb_t      part1, part2, half, res; // utilitaries array
    // to convert to double precision.
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(a);
    acb_init(b);    
    acb_init(z);
    acb_init(res);
    acb_init(part1);
    acb_init(part2);
    acb_init(w1);
    acb_init(w2);
    acb_init(wprime);
    acb_init(half);
    
    acb_set_d_d(half, 0.5, 0);
    acb_set_d_d(a, k_x, k_y);
    acb_set_d_d(b, m_x, m_y);
    acb_set_d_d(z, z_x, z_y);

    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        // compute Whittaker W(a,b,z)
        whittakerW_with_prec(    k_x, k_y, m_x, m_y, z_x, z_y, &w1, prec, 0);
        // compute Whittaker W(a+1,b,z)
        whittakerW_with_prec(1.0+k_x, k_y, m_x, m_y, z_x, z_y, &w2, prec, 0);
        
        // the derivative is W'(a,b,z) = -w2 / z + (0.5 - a / z) * w1
        acb_div(part1 , w2   , z    , prec);   // w2/z
        acb_div(part2 , a    , z    , prec);   // a /z
        acb_sub(part2 , half , part2, prec);   // 0.5 - a/z
        acb_mul(part2 , part2, w1   , prec);   // (0.5 - a/z)*w1
        acb_sub(wprime, part2, part1, prec);
        
        // compute the ratio and set in the results
        acb_div(res   , wprime, w1  , prec);
        
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("WARNING PREC_MAX is reached, the result is probably false");
    }

    /* retrieve real and imaginary parts of the result */
    arb_init(tmp1_x);
    arb_init(tmp1_y);
    arf_init(tmp2_x);
    arf_init(tmp2_y);
    
    acb_get_real(tmp1_x, res);
    acb_get_imag(tmp1_y, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(res);
    acb_clear(z);
    acb_clear(part1);
    acb_clear(part2);
    acb_clear(half);
    acb_clear(a);
    acb_clear(b);
    
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);
    flint_cleanup();

}
