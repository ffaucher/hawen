add_library(analytic_function
m_compute_elasticity.f90
m_compute_gaussian.f90
)
target_link_libraries(analytic_function tag raise_error)
install(TARGETS analytic_function 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
