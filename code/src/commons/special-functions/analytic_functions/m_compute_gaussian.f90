!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_gaussian.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute a Gaussian function in one, two
!> and three dimensions, it uses the variance  \f$ \sigma \f$ and 
!> the center of the gaussian mu \f$ \mu \f$.
!
!------------------------------------------------------------------------------
module m_compute_gaussian

  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error

  use, intrinsic            :: ieee_arithmetic
  !! -------------------------------------------------------------------
  
  implicit none

  private 

  public :: compute_gaussian_1d
  public :: compute_gaussian_2d
  public :: compute_gaussian_3d

  contains  

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the Gaussian function in one dimension
  !> \f$ exp\Big( -1/2 (x-x_0)^2 / \sigma^2 \Big)\f$
  !> Note that here the maximum is 1.0, while one could also scale with
  !> \f$ \frac{1}{\sqrt{2\pi}} \f$
  !>   
  !> -------------------------------------------------------------------
  !> @param[in]   x            : position where to evaluate
  !> @param[in]   x0           : center of the gaussian
  !> @param[in]   sigma        : variance
  !> @param[out]  uval         : output value
  !---------------------------------------------------------------------
  subroutine compute_gaussian_1d(x,x0,sigma,uval)

    implicit none
    
    !! -----------------------------------------------------------------
    real   (kind=8)    ,intent(in)   :: x 
    real   (kind=8)    ,intent(in)   :: x0
    real   (kind=8)    ,intent(in)   :: sigma
    real   (kind=8)    ,intent(out)  :: uval
    !! -----------------------------------------------------------------

    !! possible scaling is 1.d0/(sigma*sqrt(2.d0*pi)) * 
    uval = exp(-1.d0/2.d0 * (x-x0)**2 / sigma**2)
    
    return
    
  end subroutine compute_gaussian_1d


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the Gaussian function in one dimension
  !> \f$ exp\Big( -1/2 (x-x_0)^2 / \sigma^2 \Big)\f$
  !>   
  !> -------------------------------------------------------------------
  !> @param[in]   x            : position where to evaluate
  !> @param[in]   x0           : center of the gaussian
  !> @param[in]   sigma        : variance
  !> @param[out]  uval         : output value
  !---------------------------------------------------------------------
  subroutine compute_gaussian_2d(x,x0,sigma,uval)

    implicit none
    
    !! -----------------------------------------------------------------
    real   (kind=8)    ,intent(in)   :: x (2)
    real   (kind=8)    ,intent(in)   :: x0(2)
    real   (kind=8)    ,intent(in)   :: sigma(2)
    real   (kind=8)    ,intent(out)  :: uval
    !! -----------------------------------------------------------------
    real   (kind=8) :: coeff
   
    coeff = 1.d0/2.d0* ( (x(1) - x0(1))/sigma(1) )**2                   &
          + 1.d0/2.d0* ( (x(2) - x0(2))/sigma(2) )**2
    uval  = exp(-coeff)
    
    return
    
  end subroutine compute_gaussian_2d

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the Gaussian function in one dimension
  !> \f$ exp\Big( -1/2 (x-x_0)^2 / \sigma^2 \Big)\f$
  !>   
  !> -------------------------------------------------------------------
  !> @param[in]   x            : position where to evaluate
  !> @param[in]   x0           : center of the gaussian
  !> @param[in]   sigma        : variance
  !> @param[out]  uval         : output value
  !---------------------------------------------------------------------
  subroutine compute_gaussian_3d(x,x0,sigma,uval)

    implicit none
    
    !! -----------------------------------------------------------------
    real   (kind=8)    ,intent(in)   :: x (3)
    real   (kind=8)    ,intent(in)   :: x0(3)
    real   (kind=8)    ,intent(in)   :: sigma(3)
    real   (kind=8)    ,intent(out)  :: uval
    !! -----------------------------------------------------------------
    real   (kind=8) :: coeff
   
    coeff = 1.d0/2.d0* ( (x(1) - x0(1))/sigma(1) )**2                   &
          + 1.d0/2.d0* ( (x(2) - x0(2))/sigma(2) )**2                   &
          + 1.d0/2.d0* ( (x(3) - x0(3))/sigma(3) )**2
    uval  = exp(-coeff)
    
    return
    
  end subroutine compute_gaussian_3d
  
end module m_compute_gaussian
