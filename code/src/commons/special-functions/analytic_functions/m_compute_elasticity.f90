!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_compute_elasticity.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute analytic function in elasticity,
!> these can be used to impose a boundary conditions to the problem.
!
!------------------------------------------------------------------------------
module m_compute_elasticity
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_tag_namefield
  use, intrinsic            :: ieee_arithmetic
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes a naive Fourier transform
  interface analytic_function_elastic_orthorhombic
     module procedure analytic_function_elastic_orthorhombic_solo
     module procedure analytic_function_elastic_orthorhombic_multi
  end interface analytic_function_elastic_orthorhombic
  !--------------------------------------------------------------------- 
  interface analytic_function_elastic_isotropic
     module procedure analytic_function_elastic_isotropic_solo
     module procedure analytic_function_elastic_isotropic_multi
  end interface analytic_function_elastic_isotropic
  !--------------------------------------------------------------------- 

  public :: analytic_function_elastic_orthorhombic
  public :: analytic_function_elastic_isotropic
    
  contains  

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the solution for elastic orthorhombic material that
  !> has the elasticity tensor of the form
  !>            (  c11        -c66      -c55         0     0   0   )
  !>            ( -c66         c22      -c44         0     0   0   )
  !>            ( -c55        -c44       c33         0     0   0   )
  !>        C = (   0           0         0         c44    0   0   )
  !>            (   0           0         0          0    c55  0   )
  !>            (   0           0         0          0     0  c66  )
  !> -------------------------------------------------------------------
  !
  !> @param[in]   stiffC       : input stiffness tensor
  !> @param[in]   rho          : input density
  !> @param[in]   omega        : angular frequency
  !> @param[in]   xpt          : position of the 3D point
  !> @param[in]   xsrc         : source position
  !> @param[in]   src_comp     : source component
  !> @param[in]   ncomp        : number of component
  !> @param[out]  uval         : output value for ux uy uz 
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine analytic_function_elastic_orthorhombic_solo(Cstiff,rho,omega, &
                                                    xpt,xsrc,              &
                                                    src_comp,ncomp,        &
                                                    uval,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: Cstiff(6,6)
    real   (kind=8)    ,intent(in)   :: rho
    complex(kind=8)    ,intent(in)   :: omega
    real   (kind=8)    ,intent(in)   :: xpt(3)
    real   (kind=8)    ,intent(in)   :: xsrc(3)
    character(len=*)   ,intent(in)   :: src_comp(:)
    integer            ,intent(in)   :: ncomp
    complex(kind=8)    ,intent(out)  :: uval(3)
    type(t_error)      ,intent(inout):: ctx_err
    !! local variable
    real   (kind=8)            :: x(3),gkcrossgk(3,3,3),src_weight(3)
    real   (kind=8), parameter :: pi = 4.d0*datan(1.d0)
    complex(kind=8)            :: c11,c22,c33,c44,c55,c66
    complex(kind=8)            :: c1,c2,c3,c4,c5,c6
    complex(kind=8)            :: a(3),tau(3),Gtensor(3,3)
    integer(kind=4)            :: k

    ctx_err%ierr  = 0
    uval = 0.d0
    
    c11 = Cstiff(1,1)
    c22 = Cstiff(2,2)
    c33 = Cstiff(3,3)
    c44 = Cstiff(4,4)
    c55 = Cstiff(5,5)
    c66 = Cstiff(6,6)

    if((abs(c11) < tiny(1.0) .and. abs(c22) < tiny(1.0) .and. &
        abs(c33) < tiny(1.0) .and. abs(c44) < tiny(1.0) .and. &
        abs(c55) < tiny(1.0) .and. abs(c66) < tiny(1.0)) .or. &
        rho < tiny(1.0) ) then
      ctx_err%msg   = "** ERROR: the input stiffness tensor or density "// &
                      "only have zero entries "                         // &
                      "[analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    
    !! -----------------------------------------------------------------
    !! verification that we are in a orthorhombic case we the proper 
    !! pattern for C
    !! -----------------------------------------------------------------
    !! - check the zero enties
    x(1) = maxval(abs(Cstiff(4,1:3)))
    x(2) = maxval(abs(Cstiff(5,1:4)))
    x(3) = maxval(abs(Cstiff(6,1:5)))
    if(maxval(x) > tiny(1.0)) then
      ctx_err%msg   = "** ERROR: the input stiffness tensor does not " // &
                      "correspond to the expected shape of non-zero "  // &
                      "entries [analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! - check symmetry of nnz
    x(1) = abs(c66 + Cstiff(1,2))
    x(2) = abs(c44 + Cstiff(2,3))
    x(3) = abs(c55 + Cstiff(1,3))
    if(maxval(x) > tiny(1.0)) then
      ctx_err%msg   = "** ERROR: the input stiffness tensor does not " // &
                      "have the expected symmetry "                    // &
                      "[analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! which component are non-zeros
    src_weight = 0.d0
    do k=1,ncomp
      select case(trim(adjustl(src_comp(k))))
        case(tag_FIELD_UX)
          src_weight(1) = 1.d0
        case(tag_FIELD_UY)
          src_weight(2) = 1.d0
        case(tag_FIELD_UZ)
          src_weight(3) = 1.d0
        case default
          ctx_err%msg   = "** ERROR: the input source component is not "//&
                          "recognized, it should be ux, uy or uz. "     //&
                          "[analytic_function_elastic_orthorhombic]"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return
      end select
    end do
    if(maxval(src_weight) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: there is no source component " // &
                      "for the analytic function "              // &
                      "[analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------

    !! useful coefficients
    c1 = sqrt(c11/rho);
    c2 = sqrt(c22/rho);
    c3 = sqrt(c33/rho);
    c4 = sqrt(c44/rho);
    c5 = sqrt(c55/rho);
    c6 = sqrt(c66/rho);
    ! compute auxiliary variables: the cross product is always the same here.
    !! g1 = [1 0 0].'; !! ex
    !! g2 = [0 1 0].'; !! ey
    !! g3 = [0 0 1].'; !! ez
    !! g(:,1) = g1   ;   g(:,2) =   g2 ;   g(:,3) =  g3 ;
    !! !! compute g_k cross g_k
    !! for k=1:3
    !!   gk_cross_gk(:,:,k) = zeros(3,3);
    !!   for i=1:3 ; for j=1:3
    !!     gk_cross_gk(i,j,k) = g(i,k)*g(j,k) ;
    !!   end ; end
    !! end
    gkcrossgk = 0.d0
    gkcrossgk(1,1,1) = 1.d0
    gkcrossgk(2,2,2) = 1.d0
    gkcrossgk(3,3,3) = 1.d0
    
    !! position with respect to the source.
    x(:) = xpt(:) - xsrc(:)

    tau(1)=sqrt(x(1)**2/c1**2  + x(2)**2/c6**2  +  x(3)**2/c5**2) ;
    tau(2)=sqrt(x(1)**2/c6**2  + x(2)**2/c2**2  +  x(3)**2/c4**2) ;
    tau(3)=sqrt(x(1)**2/c5**2  + x(2)**2/c4**2  +  x(3)**2/c3**2) ;
    a(1)  = 1/(c1*c5*c6) ;
    a(2)  = 1/(c2*c4*c6) ;
    a(3)  = 1/(c3*c4*c5) ;
    
    Gtensor=0.d0
    do k=1,3
      Gtensor(:,:) = Gtensor(:,:) + 1.d0/(4.d0*pi*rho)*a(k)             &
                   !! omega already contains the imaginary unit here ...
                   * exp(omega*tau(k))/tau(k) * gkcrossgk(:,:,k)
    end do
    
    !! convert to u
    uval = matmul(Gtensor,src_weight)

    return
  end subroutine analytic_function_elastic_orthorhombic_solo

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the solution for elastic orthorhombic material that
  !> has the elasticity tensor of the form
  !>            (  c11        -c66      -c55         0     0   0   )
  !>            ( -c66         c22      -c44         0     0   0   )
  !>            ( -c55        -c44       c33         0     0   0   )
  !>        C = (   0           0         0         c44    0   0   )
  !>            (   0           0         0          0    c55  0   )
  !>            (   0           0         0          0     0  c66  )
  !> -------------------------------------------------------------------
  !
  !> @param[in]   stiffC       : input stiffness tensor
  !> @param[in]   rho          : input density
  !> @param[in]   omega        : angular frequency
  !> @param[in]   xpt          : positions of the 3D points
  !> @param[in]   xsrc         : source position
  !> @param[in]   src_comp     : source component
  !> @param[in]   ncomp        : number of component
  !> @param[in]   npt          : number of points
  !> @param[out]  uval         : output value for ux uy uz 
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine analytic_function_elastic_orthorhombic_multi(Cstiff,rho,omega,&
                                                    xpt,xsrc,              &
                                                    src_comp,ncomp,        &
                                                    npt,uval,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: Cstiff(6,6)
    real   (kind=8)    ,intent(in)   :: rho
    complex(kind=8)    ,intent(in)   :: omega
    real   (kind=8)    ,intent(in)   :: xpt(:,:)
    real   (kind=8)    ,intent(in)   :: xsrc(3)
    character(len=*)   ,intent(in)   :: src_comp(:)
    integer            ,intent(in)   :: ncomp
    integer            ,intent(in)   :: npt
    complex(kind=8)    ,intent(out)  :: uval(:,:)
    type(t_error)      ,intent(inout):: ctx_err
    !! local variable
    real   (kind=8)            :: x(3),gkcrossgk(3,3,3),src_weight(3)
    real   (kind=8), parameter :: pi = 4.d0*datan(1.d0)
    complex(kind=8)            :: c11,c22,c33,c44,c55,c66
    complex(kind=8)            :: c1,c2,c3,c4,c5,c6
    complex(kind=8)            :: a(3),tau(3),Gtensor(3,3)
    integer(kind=4)            :: k,j

    ctx_err%ierr  = 0
    uval = 0.d0
    
    c11 = Cstiff(1,1)
    c22 = Cstiff(2,2)
    c33 = Cstiff(3,3)
    c44 = Cstiff(4,4)
    c55 = Cstiff(5,5)
    c66 = Cstiff(6,6)
    if((abs(c11) < tiny(1.0) .and. abs(c22) < tiny(1.0) .and. &
        abs(c33) < tiny(1.0) .and. abs(c44) < tiny(1.0) .and. &
        abs(c55) < tiny(1.0) .and. abs(c66) < tiny(1.0)) .or. &
        rho < tiny(1.0) ) then
      ctx_err%msg   = "** ERROR: the input stiffness tensor or density "// &
                      "only have zero entries "                         // &
                      "[analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if


    !! -----------------------------------------------------------------
    !! verification that we are in a orthorhombic case we the proper 
    !! pattern for C
    !! -----------------------------------------------------------------
    !! - check the zero enties
    x(1) = maxval(abs(Cstiff(4,1:3)))
    x(2) = maxval(abs(Cstiff(5,1:4)))
    x(3) = maxval(abs(Cstiff(6,1:5)))
    if(maxval(x) > tiny(1.0)) then
      ctx_err%msg   = "** ERROR: the input stiffness tensor does not " // &
                      "correspond to the expected shape of non-zero "  // &
                      "entries [analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! - check symmetry of nnz
    x(1) = abs(c66 + Cstiff(1,2))
    x(2) = abs(c44 + Cstiff(2,3))
    x(3) = abs(c55 + Cstiff(1,3))
    if(maxval(x) > tiny(1.0)) then
      ctx_err%msg   = "** ERROR: the input stiffness tensor does not " // &
                      "have the expected symmetry "                    // &
                      "[analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! which component are non-zeros
    src_weight = 0.d0
    do k=1,ncomp
      select case(trim(adjustl(src_comp(k))))
        case(tag_FIELD_UX)
          src_weight(1) = 1.d0
        case(tag_FIELD_UY)
          src_weight(2) = 1.d0
        case(tag_FIELD_UZ)
          src_weight(3) = 1.d0
        case default
          ctx_err%msg   = "** ERROR: the input source component is not "//&
                          "recognized, it should be ux, uy or uz. "     //&
                          "[analytic_function_elastic_orthorhombic]"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return
      end select
    end do
    if(maxval(src_weight) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: there is no source component " // &
                      "for the analytic function "              // &
                      "[analytic_function_elastic_orthorhombic]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------

    !! useful coefficients
    c1 = sqrt(c11/rho);
    c2 = sqrt(c22/rho);
    c3 = sqrt(c33/rho);
    c4 = sqrt(c44/rho);
    c5 = sqrt(c55/rho);
    c6 = sqrt(c66/rho);
    ! compute auxiliary variables: the cross product is always the same here.
    !! g1 = [1 0 0].'; !! ex
    !! g2 = [0 1 0].'; !! ey
    !! g3 = [0 0 1].'; !! ez
    !! g(:,1) = g1   ;   g(:,2) =   g2 ;   g(:,3) =  g3 ;
    !! !! compute g_k cross g_k
    !! for k=1:3
    !!   gk_cross_gk(:,:,k) = zeros(3,3);
    !!   for i=1:3 ; for j=1:3
    !!     gk_cross_gk(i,j,k) = g(i,k)*g(j,k) ;
    !!   end ; end
    !! end
    gkcrossgk = 0.d0
    gkcrossgk(1,1,1) = 1.d0
    gkcrossgk(2,2,2) = 1.d0
    gkcrossgk(3,3,3) = 1.d0
    
    !! loop over all positions
    do j=1,npt
      !! position with respect to the source.
      x(:) = xpt(:,j) - xsrc(:)

      tau(1)=sqrt(x(1)**2/c1**2  + x(2)**2/c6**2  +  x(3)**2/c5**2) ;
      tau(2)=sqrt(x(1)**2/c6**2  + x(2)**2/c2**2  +  x(3)**2/c4**2) ;
      tau(3)=sqrt(x(1)**2/c5**2  + x(2)**2/c4**2  +  x(3)**2/c3**2) ;
      a(1)  = 1/(c1*c5*c6) ;
      a(2)  = 1/(c2*c4*c6) ;
      a(3)  = 1/(c3*c4*c5) ;
      
      Gtensor=0.d0
      !! as we know gcrossg we can simplify the loop
      do k=1,3
        Gtensor(:,:) = Gtensor(:,:) + 1.d0/(4.d0*pi*rho)*a(k)             &
        !! omega already contains the imaginary unit here ..............
                     * exp(omega*tau(k))/tau(k) * gkcrossgk(:,:,k)
      end do
      !! convert to u
      uval(:,j) = matmul(Gtensor,src_weight)

      ! Gtensor(1,1) = Gtensor(1,1) + 1.d0/(4.d0*pi*rho)*a(1)             &
      !                !! omega already contains the imaginary unit here .
      !                * exp(omega*tau(1))/tau(1)
      ! Gtensor(2,2) = Gtensor(2,2) + 1.d0/(4.d0*pi*rho)*a(2)             &
      !                !! omega already contains the imaginary unit here .
      !                * exp(omega*tau(2))/tau(2)
      ! Gtensor(3,3) = Gtensor(3,3) + 1.d0/(4.d0*pi*rho)*a(3)             &
      !                !! omega already contains the imaginary unit here .
      !                * exp(omega*tau(3))/tau(3)
      ! 
      ! uval(1,j) = Gtensor(1,1)*src_weight(1)
      ! uval(2,j) = Gtensor(2,2)*src_weight(2)
      ! uval(3,j) = Gtensor(3,3)*src_weight(3)
    end do
    
    return
  end subroutine analytic_function_elastic_orthorhombic_multi

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the solution for elastic isotropic material 
  !> -------------------------------------------------------------------
  !
  !> @param[in]   cp           : input vp
  !> @param[in]   cs           : input vs
  !> @param[in]   rho          : input density
  !> @param[in]   omega        : angular frequency
  !> @param[in]   xpt          : position of the 3D point
  !> @param[in]   xsrc         : source position
  !> @param[in]   src_comp     : source component
  !> @param[in]   ncomp        : number of component
  !> @param[out]  uval         : output value for ux uy uz 
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine analytic_function_elastic_isotropic_solo(cp,cs,rho,omega,  &
                                                      xpt,xsrc,         &
                                                      src_comp,ncomp,   &
                                                      uval,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: cp
    complex(kind=8)    ,intent(in)   :: cs
    real   (kind=8)    ,intent(in)   :: rho
    complex(kind=8)    ,intent(in)   :: omega
    real   (kind=8)    ,intent(in)   :: xpt(3)
    real   (kind=8)    ,intent(in)   :: xsrc(3)
    character(len=*)   ,intent(in)   :: src_comp(:)
    integer            ,intent(in)   :: ncomp
    complex(kind=8)    ,intent(out)  :: uval(3)
    type(t_error)      ,intent(inout):: ctx_err
    !! local variable
    integer                    :: k
    real   (kind=8)            :: x(3),dist,w,w2,wp,src_weight(3)
    complex(kind=8)            :: lambda,mu
    complex(kind=8)            :: Up,Us
    complex(kind=8)            :: dUpdx,dUpdy,dUpdz 
    complex(kind=8)            :: dUsdx,dUsdy,dUsdz 
    complex(kind=8)            :: d2Upd2x,d2Upd2y,d2Upd2z
    complex(kind=8)            :: d2Updxy,d2Updxz,d2Updyz
    complex(kind=8)            :: d2Usd2x,d2Usd2y,d2Usd2z
    complex(kind=8)            :: d2Usdxy,d2Usdxz,d2Usdyz
    complex(kind=8)            :: Gpx,Gpy,Gpz
    complex(kind=8)            :: Gsx,Gsy,Gsz
    complex(kind=8)            :: ikp,iks,e,u,v,vp,up0,vp0,up1
    !! parameter
    real   (kind=8), parameter :: pi    = 4.d0*datan(1.d0)
    
    ctx_err%ierr  = 0
    uval = 0.d0

    !! -----------------------------------------------------------------
    !! which component are non-zeros
    src_weight = 0.d0
    do k=1,ncomp
      select case(trim(adjustl(src_comp(k))))
        case(tag_FIELD_UX)
          src_weight(1) = 1.d0
        case(tag_FIELD_UY)
          src_weight(2) = 1.d0
        case(tag_FIELD_UZ)
          src_weight(3) = 1.d0
        case default
          ctx_err%msg   = "** ERROR: the input source component is not "//&
                          "recognized, it should be ux, uy or uz. "     //&
                          "[analytic_function_elastic_isotropy]"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return
      end select
    end do
    if(maxval(src_weight) < tiny(1.0)) then
      ctx_err%msg   = "** ERROR: there is no source component " // &
                      "for the analytic function "              // &
                      "[analytic_function_elastic_isotropy]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    !! -----------------------------------------------------------------
    
    !! 0. position w.r.t source.
    x(:) = xpt(:) - xsrc(:)
    dist = norm2(x)

    !! -----------------------------------------------------------------
    !! 1. compute the solution to the Helmholtz equation with (vp, vs)
    !! position with respect to the source.
    !! omega already contains the imaginary unit here ..................
    ikp   = omega/cp ;
    iks   = omega/cs ;
    Up = exp(dist*ikp) / (4.d0*pi*dist) ;
    Us = exp(dist*iks) / (4.d0*pi*dist) ;   

    !! 2. compute first-order derivative
    e     = exp(dist*ikp) / (4.d0*pi*(dist**3)) * (ikp*dist - 1.d0) ;
    dUpdx = x(1) * e;
    dUpdy = x(2) * e;
    dUpdz = x(3) * e;
    e     = exp(dist*iks) / (4.d0*pi*(dist**3)) * (iks*dist - 1.d0) ;
    dUsdx = x(1) * e;
    dUsdy = x(2) * e;
    dUsdz = x(3) * e;

    !! 2. compute second-order derivative
    !! -----------------------------------------------------------------
    !! Up
    !! -----------------------------------------------------------------
    e   = exp(ikp*dist);
    u   = e/(4.d0*pi*dist**3) ; 
    v   = (ikp*dist - 1.0);
    up0 = e/(4.d0*pi*dist**5) * (dist*ikp  - 3.0) ;
    vp0 = ikp / dist ;
    !! x-derivative ---------------------------------------------------- %
    w = x(1); 
      ! (dx . dx)
    w2= w; up1 = up0*w2 ; vp= vp0*w2 ; wp= 1.0;
    d2Upd2x = up1*v*w + u*vp*w + u*v*wp ;
      ! (dy . dx)
    w2= x(2); up1= up0*w2 ; vp= vp0*w2 ; wp= 0.0;
    d2Updxy = up1*v*w + u*vp*w + u*v*wp ;
      ! (dz . dx)
    w2= x(3); up1= up0*w2 ; vp= vp0*w2 ; wp= 0.0;
    d2Updxz = up1*v*w + u*vp*w + u*v*wp ;
    !! y-derivative ---------------------------------------------------- %
    w = x(2);
      ! (d. dy)
    w2= w; up1= up0*w2 ; vp= vp0*w2 ; wp= 1.0
    d2Upd2y = up1*v*w + u*vp*w + u*v*wp ;
      ! (dz . dy)
    w2= x(3); up1= up0*w2 ;  vp= vp0*w2 ;  wp= 0.0
    d2Updyz = up1*v*w + u*vp*w + u*v*wp ;
    !! z-derivative ---------------------------------------------------- %
    w = x(3);
      ! (d. dz)
    w2= w; up1= up0*w2 ;  vp= vp0*w2 ; wp= 1.0
    d2Upd2z = up1*v*w + u*vp*w + u*v*wp ;

    !! -----------------------------------------------------------------
    !! Us
    !! -----------------------------------------------------------------
    e   = exp(iks*dist);
    u   = e/(4.d0*pi*dist**3) ; 
    v   = (iks*dist - 1.0);
    up0 = e/(4.d0*pi*dist**5) * (dist*iks  - 3.0) ;
    vp0 = iks / dist ;
    !! x-derivative ---------------------------------------------------- %
    w = x(1); 
      ! (dx . dx)
    w2= w; up1= up0*w2 ; vp= vp0*w2 ; wp= 1.0;
    d2Usd2x = up1*v*w + u*vp*w + u*v*wp ;
      ! (dy . dx)
    w2= x(2); up1= up0*w2 ; vp= vp0*w2 ; wp= 0.0;
    d2Usdxy = up1*v*w + u*vp*w + u*v*wp ;
      ! (dz . dx)
    w2= x(3); up1= up0*w2 ; vp= vp0*w2 ; wp= 0.0;
    d2Usdxz = up1*v*w + u*vp*w + u*v*wp ;
    !! y-derivative ---------------------------------------------------- %
    w = x(2);
      ! (d. dy)
    w2= w; up1= up0*w2 ; vp= vp0*w2 ; wp= 1.0
    d2Usd2y = up1*v*w + u*vp*w + u*v*wp ;
      ! (dz . dy)
    w2= x(3); up1= up0*w2 ;  vp= vp0*w2 ;  wp= 0.0
    d2Usdyz = up1*v*w + u*vp*w + u*v*wp ;
    !! z-derivative ---------------------------------------------------- %
    w = x(3);
      ! (d. dz)
    w2= w; up1= up0*w2 ;  vp= vp0*w2 ; wp= 1.0
    d2Usd2z = up1*v*w + u*vp*w + u*v*wp ;

    !! ----------------------------------------------------------------- %
    !! Elastic Green's function
    !! ----------------------------------------------------------------- %
    mu     = cs*cs*rho;
    lambda = cp*cp*rho - 2.d0*mu;
    !! Here we use iks while we need ks, we have iks² = -ks², hence minus
    Gpx = 1./(mu*(-iks**2)) * (d2Upd2x*src_weight(1) + d2Updxy*src_weight(2) + d2Updxz*src_weight(3))
    Gpy = 1./(mu*(-iks**2)) * (d2Updxy*src_weight(1) + d2Upd2y*src_weight(2) + d2Updyz*src_weight(3))
    Gpz = 1./(mu*(-iks**2)) * (d2Updxz*src_weight(1) + d2Updyz*src_weight(2) + d2Upd2z*src_weight(3))
    !! Here we use iks while we need ks, we have iks² = -ks², hence minus
    Gsx =-1./mu *(Us + 1./(-iks**2) * (d2Usd2x*src_weight(1) + d2Usdxy*src_weight(2) +d2Usdxz*src_weight(3)))
    Gsy =-1./mu *(Us + 1./(-iks**2) * (d2Usdxy*src_weight(1) + d2Usd2y*src_weight(2) +d2Usdyz*src_weight(3)))
    Gsz =-1./mu *(Us + 1./(-iks**2) * (d2Usdxz*src_weight(1) + d2Usdyz*src_weight(2) +d2Usd2z*src_weight(3)))

    uval(1) = Gpx + Gsx
    uval(2) = Gpy + Gsy
    uval(3) = Gpz + Gsz

    return
  end subroutine analytic_function_elastic_isotropic_solo

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the solution for elastic isotropic material 
  !> -------------------------------------------------------------------
  !
  !> @param[in]   cp           : input vp
  !> @param[in]   cs           : input vs
  !> @param[in]   rho          : input density
  !> @param[in]   omega        : angular frequency
  !> @param[in]   xpt          : position of the 3D point
  !> @param[in]   xsrc         : source position
  !> @param[in]   src_comp     : source component
  !> @param[in]   ncomp        : number of component
  !> @param[out]  uval         : output value for ux uy uz 
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine analytic_function_elastic_isotropic_multi(cp,cs,rho,omega, &
                                                      xpt,xsrc,         &
                                                      src_comp,ncomp,   &
                                                      npt,uval,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: cp
    complex(kind=8)    ,intent(in)   :: cs
    real   (kind=8)    ,intent(in)   :: rho
    complex(kind=8)    ,intent(in)   :: omega
    real   (kind=8)    ,intent(in)   :: xpt(:,:)
    real   (kind=8)    ,intent(in)   :: xsrc(3)
    character(len=*)   ,intent(in)   :: src_comp(:)
    integer            ,intent(in)   :: ncomp
    integer            ,intent(in)   :: npt
    complex(kind=8)    ,intent(out)  :: uval(:,:)
    type(t_error)      ,intent(inout):: ctx_err
    !! local
    integer(kind=4)            :: j
    complex(kind=8)            :: uloc(3)

    ctx_err%ierr  = 0
    uval = 0.d0
    
    !! loop over all positions
    do j=1,npt
      call analytic_function_elastic_isotropic_solo(cp,cs,rho,omega,    &
                                                    xpt(:,j),xsrc,      &
                                                    src_comp,ncomp,     &
                                                    uloc,ctx_err)
      if(ctx_err%ierr .ne. 0) return !! non-shared error.
      uval(:,j) = uloc 
    end do
    return
  end subroutine analytic_function_elastic_isotropic_multi

end module m_compute_elasticity
