!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_Fourier_transform_naive.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the Fourier transform of a signal.
!> This is an highly un-optimized routine that should not be used
!> in general.
!
!------------------------------------------------------------------------------
module m_Fourier_transform_naive
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use, intrinsic            :: ieee_arithmetic
  !! -------------------------------------------------------------------
  
  implicit none
  private 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes a naive Fourier transform
  interface compute_Fourier_transform_naive
     module procedure Fourier_transform_naive_1D_double_complex
     module procedure Fourier_transform_naive_1D_simple_complex
  end interface compute_Fourier_transform_naive
  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes a naive inverse Fourier transform
  interface compute_Fourier_inverse_naive
     module procedure Fourier_inverse_naive_1D_double_complex
  end interface compute_Fourier_inverse_naive
  !--------------------------------------------------------------------- 

  public :: compute_Fourier_transform_naive, compute_Fourier_inverse_naive
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Fourier transform for double complex signals.
  !
  !> @param[in]   signal       : input signal
  !> @param[in]   x            : discretization of the signals
  !> @param[in]   nx           : number of data points
  !> @param[out]  Fsignal      : Fourier transform
  !> @param[out]  Fx           : output interval of Fourier variable
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine Fourier_transform_naive_1D_double_complex(signal,x,nx,     &
                                                       Fsignal,Fx,      &
                                                       ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: signal(:)
    real   (kind=8)    ,intent(in)   :: x(:)
    integer(kind=4)    ,intent(in)   :: nx
    real   (kind=8)    ,intent(out)  :: Fx(:)
    complex(kind=8)    ,intent(out)  :: Fsignal(:)
    type(t_error)      ,intent(inout):: ctx_err
    !! local variable
    real(kind=8), parameter    :: pi = 4.d0*datan(1.d0)
    real(kind=8), allocatable  :: dx(:)
    integer(kind=4)            :: nxhalf,k,j

    ctx_err%ierr  = 0
    Fx      = 0.d0
    Fsignal = 0.d0
    allocate(dx(nx))
    dx=0.d0
    dx(1) = x(2)-x(1) !! we assume the first one is the same as after...
    do k=2,nx
      dx(k) = x(k)-x(k-1)
    end do
    
    
    !! depending if it an even or odd number. --------------------------
    if(mod(nx,2) == 0) then
      nxhalf  =  nx/2 ;
      do k=1,nx
        Fx(k)= -nxhalf + k-1
             ! [0:nrcv_2-1   -nrcv_2:1:-1];
      end do
    else
      nxhalf =  (nx-1)/2 ;
      Fx(nx) = nxhalf
      do k=1,nx-1
        Fx(k)= -nxhalf + k-1
      end do
    end if
    !! scale with length
    Fx = Fx / (x(nx)-x(1) + dx(1))
    !! -----------------------------------------------------------------

    Fsignal = 0.d0 
    do k=1,nx
      do j=1,nx
        Fsignal(k) = Fsignal(k) + signal(j)                              &
                                * exp(-2.d0*dcmplx(0.,1.)*pi*Fx(k)*x(j)) &
                                !! integral so step size
                                * dx(k)
      end do
    end do
    
    return
  end subroutine Fourier_transform_naive_1D_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute Fourier transform for simple complex signals.
  !
  !> @param[in]   signal       : input signal
  !> @param[in]   x            : discretization of the signals
  !> @param[in]   nx           : number of data points
  !> @param[out]  Fsignal      : Fourier transform
  !> @param[out]  Fx           : output interval of Fourier variable
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine Fourier_transform_naive_1D_simple_complex(signal,x,nx,     &
                                                       Fsignal,Fx,      &
                                                       ctx_err)

    implicit none

    complex(kind=4)    ,intent(in)   :: signal(:)
    real   (kind=8)    ,intent(in)   :: x(:)
    integer(kind=4)    ,intent(in)   :: nx
    real   (kind=8)    ,intent(out)  :: Fx(:)
    complex(kind=4)    ,intent(out)  :: Fsignal(:)
    type(t_error)      ,intent(inout):: ctx_err
    !! local variable
    real(kind=8), parameter    :: pi = 4.d0*datan(1.d0)
    real(kind=8), allocatable  :: dx(:)
    !! dble precision operation
    complex(kind=8), allocatable :: Fsignal_temp(:)
    integer(kind=4)              :: nxhalf,k,j

    ctx_err%ierr  = 0
    Fx      = 0.d0
    Fsignal = 0.d0
    allocate(dx(nx))
    allocate(Fsignal_temp(nx))
    dx=0.d0
    dx(1) = x(2)-x(1) !! we assume the first one is the same as after...
    do k=2,nx
      dx(k) = x(k)-x(k-1)
    end do
    
    
    !! depending if it an even or odd number. --------------------------
    if(mod(nx,2) == 0) then
      nxhalf  =  nx/2 ;
      do k=1,nx
        Fx(k)= -nxhalf + k-1
             ! [0:nrcv_2-1   -nrcv_2:1:-1];
      end do
    else
      nxhalf =  (nx-1)/2 ;
      Fx(nx) = nxhalf
      do k=1,nx-1
        Fx(k)= -nxhalf + k-1
      end do
    end if
    !! scale with length
    Fx = Fx / (x(nx)-x(1) + dx(1))
    !! -----------------------------------------------------------------

    Fsignal = 0.d0 
    Fsignal_temp=0.d0
    do k=1,nx
      do j=1,nx
        Fsignal_temp(k) = Fsignal_temp(k) + signal(j)                   &
                                * exp(-2.d0*dcmplx(0.,1.)*pi*Fx(k)*x(j))&
                                !! integral so step size
                                * dx(k)
      end do
    end do
    Fsignal = cmplx(Fsignal_temp,kind=4)
    deallocate(Fsignal_temp)
    
    return
  end subroutine Fourier_transform_naive_1D_simple_complex



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute inverse Fourier transform for double complex signals.
  !
  !> @param[in]   signal       : input signal
  !> @param[in]   x            : discretization of the signals
  !> @param[in]   nx           : number of data points
  !> @param[out]  Fsignal      : Fourier transform
  !> @param[out]  Fx           : output interval of Fourier variable
  !> @param[out]  ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine Fourier_inverse_naive_1D_double_complex(signal,x,nx,       &
                                                     Fsignal,Fx,Fx0,ctx_err)

    implicit none

    complex(kind=8)    ,intent(in)   :: signal(:)
    real   (kind=8)    ,intent(in)   :: x(:)
    integer(kind=4)    ,intent(in)   :: nx
    real   (kind=8)    ,intent(out)  :: Fx(:)
    real   (kind=8)    ,intent(in)   :: Fx0
    complex(kind=8)    ,intent(out)  :: Fsignal(:)
    type(t_error)      ,intent(inout):: ctx_err
    !! local variable
    real(kind=8), parameter    :: pi = 4.d0*datan(1.d0)
    real(kind=8), allocatable  :: dx(:)
    integer(kind=4)            :: nxhalf,k,j

    ctx_err%ierr  = 0
    Fx      = 0.d0
    Fsignal = 0.d0
    allocate(dx(nx))
    dx=0.d0
    dx(1) = x(2)-x(1) !! we assume the first one is the same as after...
    do k=2,nx
      dx(k) = x(k)-x(k-1)
    end do
    
    
    !! depending if it an even or odd number. --------------------------
    if(mod(nx,2) == 0) then
      nxhalf  =  nx/2 ;
      do k=1,nx
        Fx(k)= -nxhalf + k-1
             ! [0:nrcv_2-1   -nrcv_2:1:-1];
      end do
    else
      nxhalf =  (nx-1)/2 ;
      Fx(nx) = nxhalf
      do k=1,nx-1
        Fx(k)= -nxhalf + k-1
      end do
    end if
    !! scale with length
    Fx = Fx / (x(nx)-x(1) + dx(1))
    !! start from Fx0 **************************************************
    Fx = Fx + abs(minval(Fx)) + Fx0
    !! *****************************************************************
    !! -----------------------------------------------------------------

    Fsignal = 0.d0 
    do k=1,nx
      do j=1,nx
        Fsignal(k) = Fsignal(k) + signal(j)                              &
                                * exp( 2.d0*dcmplx(0.,1.)*pi*Fx(k)*x(j)) &
                                !! integral so step size
                                * dx(k)
      end do
    end do
    
    return
  end subroutine Fourier_inverse_naive_1D_double_complex


end module m_Fourier_transform_naive
