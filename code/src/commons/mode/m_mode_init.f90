!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mode_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module init the list of modes, if the equation has 
!
!------------------------------------------------------------------------------
module m_mode_init

  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_print_mode,               only : print_mode_list
  use m_read_parameters_mode,     only : read_parameters_mode
 
  implicit none

  private
  public  :: mode_init, mode_init_with_group
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init mode information
  !
  !> @param[in]    ctx_paral       : parallelism context
  !> @param[inout] mode            : mode
  !> @param[out]   nmode           : number of mode
  !> @param[in]    parameter_file  : parameter file
  !> @param[in]    flag_mode       : is there a mode in the equation
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mode_init(ctx_paral,mode,nmode,parameter_file,flag_mode,ctx_err)
    
    implicit none
    
    type(t_parallelism),intent(in)   :: ctx_paral
    integer,allocatable,intent(inout):: mode(:)
    integer            ,intent(out)  :: nmode
    character(len=*)   ,intent(in)   :: parameter_file
    logical            ,intent(in)   :: flag_mode
    type(t_error)      ,intent(out)  :: ctx_err 
    
    ctx_err%ierr  = 0

    if(flag_mode) then
      call read_parameters_mode(parameter_file,mode,nmode,ctx_err)
    else
      allocate(mode(1))
      mode=0
      nmode=1
    endif
    call print_mode_list(ctx_paral,mode,nmode,flag_mode,ctx_err)

    return
  end subroutine mode_init

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init mode information with group information, for inversion
  !
  !> @param[in]    ctx_paral       : parallelism context
  !> @param[inout] mode       : mode
  !> @param[out]   nmode           : number of mode
  !> @param[out]   nmode_per_group : number of mode per group
  !> @param[in]    parameter_file  : parameter file
  !> @param[in]    flag_mode       : is there a mode in the equation
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mode_init_with_group(ctx_paral,mode,nmode,nmode_per_group, &
                                  parameter_file,flag_mode,ctx_err)
    
    implicit none
    
    type(t_parallelism),intent(in)   :: ctx_paral
    integer,allocatable,intent(inout):: mode(:)
    integer            ,intent(out)  :: nmode,nmode_per_group
    character(len=*)   ,intent(in)   :: parameter_file
    logical            ,intent(in)   :: flag_mode
    type(t_error)      ,intent(out)  :: ctx_err 
    
    ctx_err%ierr  = 0

    if(flag_mode) then
      call read_parameters_mode(parameter_file,mode,nmode,ctx_err,      &
                                o_n_mode_per_group=nmode_per_group)
    else
      allocate(mode(1))
      mode=0
    endif
    call print_mode_list(ctx_paral,mode,nmode,flag_mode,ctx_err,        &
                         o_n_mode_per_group=nmode_per_group)

    return
  end subroutine mode_init_with_group

end module m_mode_init
