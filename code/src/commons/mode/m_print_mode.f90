!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_mode.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for modes
!
!------------------------------------------------------------------------------
module m_print_mode

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_print_basics,     only : separator,indicator, marker
  implicit none
  
  private
  public :: print_mode_list,print_mode_welcome
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print welcome mode infos
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   mode          : array of mode
  !> @param[in]   nmode         : number of mode
  !> @param[in]   imode         : current of mode
  !> @param[in]   flag_mode     : flag mode for eq
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_mode_welcome(ctx_paral,mode,nmode,imode,flag_mode,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    integer            ,intent(in)   :: mode(:)
    integer            ,intent(in)   :: imode,nmode
    logical            ,intent(in)   :: flag_mode
    type(t_error)      ,intent(out)  :: ctx_err
    
    ctx_err%ierr  = 0
    
    !! do we have modes in the equation or not.
    if(flag_mode) then
      if(ctx_paral%master) then
      
        write(6,'(a)') marker
        write(6,'(a,i7,a,i7)')      "********** solving for mode      ", &
                                    imode," / ",nmode
        write(6,'(a,i7)') "********** mode : ", mode(imode)

        write(6,'(a)') marker
        !write(6,'(a)') separator
      end if
    end if
    return
  end subroutine print_mode_welcome
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print the list of integer mode
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   mode          : array of mode
  !> @param[in]   nmode         : number of mode
  !> @param[in]   flag_mode     : flag mode for eq
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_mode_list(ctx_paral,mode,nmode,flag_mode,  &
                                  ctx_err,o_n_mode_per_group)
    type(t_parallelism),intent(in)   :: ctx_paral
    integer            ,intent(in)   :: mode(:)
    integer            ,intent(in)   :: nmode
    logical            ,intent(in)   :: flag_mode
    type(t_error)      ,intent(out)  :: ctx_err
    integer, optional  ,intent(in)   :: o_n_mode_per_group
    !!
    integer          :: k
    
    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      if(.not. flag_mode) then
        write(6,'(2a)')   indicator, "  No mode dependency for this equation "
      else
        write(6,'(2a)')   indicator, "  list of modes "
        do k=1,nmode
          write(6,'(a,i7)') "- mode                       : ", mode(k)
        enddo
        write(6,'(a)'   ) "------------------------------"
        write(6,'(a,i7)') "- Total number of mode       : ",nmode
        if(present(o_n_mode_per_group)) then
          write(6,'(a,i7)') "- Group size for inversion   : ",o_n_mode_per_group
        end if
        write(6,'(a)')    separator
      endif
    endif
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_mode_list

end module m_print_mode
