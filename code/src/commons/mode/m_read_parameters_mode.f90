!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_mode.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read mode parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_mode
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_mode
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read the mode list from parameter file
  !
  !> @param[in]   parameter_file : parameter file where infos are
  !> @param[out]  mode          : array for the integer modes
  !> @param[out]  nfreq         : total number of mode
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_mode(parameter_file,mode,nmode,ctx_err, &
                                  o_n_mode_per_group)
    character(len=*)   ,intent(in)   :: parameter_file
    integer,allocatable,intent(out)  :: mode(:)
    integer            ,intent(out)  :: nmode
    type(t_error)      ,intent(out)  :: ctx_err
    integer, optional  ,intent(out)  :: o_n_mode_per_group
    !! local
    integer            :: ilist(512),nval,istart,iend,istep,k1
    logical            :: flag(3)

    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------
    !! Reading the modes -----------------------------------------------
    !! -----------------------------------------------------------------

    !! allow a bypass for very large number of modes
    flag = .false. 
    call read_parameter(parameter_file,'mode_list_min' ,0,ilist,nval,   &
                        o_flag_found=flag(1))
    istart = ilist(1)
    call read_parameter(parameter_file,'mode_list_max' ,0,ilist,nval,   &
                        o_flag_found=flag(2))
    iend  = ilist(1)
    call read_parameter(parameter_file,'mode_list_step',0,ilist,nval,   &
                        o_flag_found=flag(3))
    istep = ilist(1)

    !! only if all keyword are found
    if(all(flag .eqv. .true.) .and. istep > 0) then
      nmode = (iend-istart)/istep + 1
      allocate(mode(nmode))
      do k1=1,nmode
        mode(k1) = istart + (k1-1) * istep
      end do

    else !! traditional way
      call read_parameter(parameter_file,'mode_list',0,ilist,nval)
      !! count the number of modes
      nmode = nval
      allocate(mode(nmode))
      mode(1:nmode)=ilist(1:nmode)
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    if(present(o_n_mode_per_group)) then
      call read_parameter(parameter_file,'fwi_mode_group',1,ilist,nval)
      o_n_mode_per_group = ilist(1)
      if(o_n_mode_per_group > nmode) o_n_mode_per_group = nmode
      if(o_n_mode_per_group < 1    ) then
        ctx_err%msg   = "** ERROR: you need at least one mode " // &
                        "per group [read_parameters_mode] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine read_parameters_mode

end module m_read_parameters_mode
