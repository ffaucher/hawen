!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_frequency_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module init the list of frequency
!
!------------------------------------------------------------------------------
module m_frequency_init

  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_print_freq,               only : print_frequency_list
  use m_read_parameters_freq,     only : read_parameters_freq
 
  implicit none

  private
  public  :: frequency_init, frequency_init_with_group
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init frequency information
  !
  !> @param[in]    ctx_paral       : parallelism context
  !> @param[inout] frequency       : frequency
  !> @param[out]   nfreq           : number of frequency
  !> @param[in]    parameter_file  : parameter file
  !> @param[in]    flag_freq       : is there a frequency in the equation
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine frequency_init(ctx_paral,frequency,nfreq,parameter_file,   &
                            flag_freq,ctx_err)
    
    implicit none
    
    type(t_parallelism),intent(in)   :: ctx_paral
    real,allocatable   ,intent(inout):: frequency(:,:)
    integer            ,intent(out)  :: nfreq
    character(len=*)   ,intent(in)   :: parameter_file
    logical            ,intent(in)   :: flag_freq
    type(t_error)      ,intent(out)  :: ctx_err 
    
    ctx_err%ierr  = 0

    if(flag_freq) then
      call read_parameters_freq(parameter_file,frequency,nfreq,ctx_err)
    else
      allocate(frequency(1,2))
      frequency=0.0
      nfreq    =1
    endif
    call print_frequency_list(ctx_paral,frequency,nfreq,flag_freq,ctx_err)

    return
  end subroutine frequency_init

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init frequency information with group information, for inversion
  !
  !> @param[in]    ctx_paral       : parallelism context
  !> @param[inout] frequency       : frequency
  !> @param[out]   nfreq           : number of frequency
  !> @param[out]   nfreq_per_group : number of frequency per group
  !> @param[in]    parameter_file  : parameter file
  !> @param[in]    flag_freq       : is there a frequency in the equation
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine frequency_init_with_group(ctx_paral,frequency,nfreq,       &
                                       nfreq_per_group,parameter_file,  &
                                       flag_freq,ctx_err)
    
    implicit none
    
    type(t_parallelism),intent(in)   :: ctx_paral
    real,allocatable   ,intent(inout):: frequency(:,:)
    integer            ,intent(out)  :: nfreq,nfreq_per_group
    character(len=*)   ,intent(in)   :: parameter_file
    logical            ,intent(in)   :: flag_freq
    type(t_error)      ,intent(out)  :: ctx_err 
    
    ctx_err%ierr  = 0

    if(flag_freq) then
      call read_parameters_freq(parameter_file,frequency,nfreq,ctx_err, &
                                o_n_freq_per_group=nfreq_per_group)
    else
      allocate(frequency(1,2))
      frequency=0.0
    endif
    call print_frequency_list(ctx_paral,frequency,nfreq,flag_freq,      &
                              ctx_err,o_n_freq_per_group=nfreq_per_group)

    return
  end subroutine frequency_init_with_group

end module m_frequency_init
