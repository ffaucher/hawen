!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_freq.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for frequencies
!
!------------------------------------------------------------------------------
module m_print_freq

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_print_basics,     only : separator,indicator, marker
  implicit none
  
  private
  public :: print_frequency_list,print_frequency_welcome, convert_frequency_unit
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> convert a frequency into the proper (SI) unit. For instance, if the
  !> original frequency is < 1, we shall use mHz instead of Hz.
  !
  !> @param[inout]   f       : original (unscaled) frequency
  !> @param[inout]   unt     : characters for the unit
  !----------------------------------------------------------------------------
  subroutine convert_frequency_unit(f,unt)
    
    implicit none
    
    real               ,intent(inout) :: f
    character(len=4)   ,intent(out)   :: unt
    
    unt="  Hz"
    !! milli   m
    !! micro   \f$\mu\f$
    !! nano    n
    !! pico    p
    !! femto   f
    !! atto    a
    !! zepto   z
    !! yocto   y
    if(f < 0.1) then !! too low
      f=f*1000. ; unt=" mHz"
      if(f < 0.1) then
        f=f*1000. ; unt=" uHz"
        if(f < 0.1) then
          f=f*1000. ; unt=" nHz"
          if(f < 0.1) then
            f=f*1000. ; unt=" pHz"
            if(f < 0.1) then
              f=f*1000. ; unt=" fHz"
              if(f < 0.1) then
                f=f*1000. ; unt=" aHz"
                if(f < 0.1) then
                  f=f*1000. ; unt=" zHz"
                  if(f < 0.1) then
                    f=f*1000. ; unt=" yHz"
                  end if
                end if
              end if
            end if
          end if
        end if
      end if
    !! yotta  Y
    !! zetta  Z
    !! exa    E
    !! peta   P
    !! tera   T
    !! giga   G
    !! mega   M
    !! kilo   k
    else if(f>1000.) then !! too high
      f=f/1000. ; unt=" kHz"
      if(f>1000.) then
        f=f/1000. ; unt=" MHz"
        if(f>1000.) then
          f=f/1000. ; unt=" GHz"
          if(f>1000.) then
            f=f/1000. ; unt=" THz"
            if(f>1000.) then
              f=f/1000. ; unt=" PHz"
              if(f>1000.) then
                f=f/1000. ; unt=" EHz"
                if(f>1000.) then
                  f=f/1000. ; unt=" ZHz"
                  if(f>1000.) then
                    f=f/1000. ; unt=" YHz"
                  end if
                end if
              end if
            end if
          end if
        end if
      end if
    endif
    
    return
  end subroutine convert_frequency_unit  


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print welcome frequency infos
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   frequency     : array of frequency
  !> @param[in]   nfreq         : number of frequency
  !> @param[in]   ifreq         : current of frequency
  !> @param[in]   flag_freq     : flag frequency for eq
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_frequency_welcome(ctx_paral,frequency,nfreq,ifreq,flag_freq,ctx_err)
    
    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    real               ,intent(in)   :: frequency(:,:)
    integer            ,intent(in)   :: ifreq,nfreq
    logical            ,intent(in)   :: flag_freq
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    real             :: f
    character(len=4) :: unt
    
    ctx_err%ierr  = 0
    
    if(flag_freq) then
      if(ctx_paral%master) then
      
        !! get memory unit for printing
        f=frequency(ifreq,1)
        call convert_frequency_unit(f,unt)
      
        write(6,'(a)') marker
        write(6,'(a,i7,a,i7)')      "********** solving for frequency ", &
                                    ifreq," / ",nfreq
        write(6,'(a,f12.4,a,f0.4)') "********** complex frequency : ",   &
                                    f,trim(unt)//" +   ", frequency(ifreq,2)

        write(6,'(a)') marker
        write(6,'(a)') separator
      end if
    end if
    return
  end subroutine print_frequency_welcome
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print the list of complex frequency
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   frequency     : array of frequency
  !> @param[in]   nfreq         : number of frequency
  !> @param[in]   flag_freq     : flag frequency for eq
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_frequency_list(ctx_paral,frequency,nfreq,flag_freq,  &
                                  ctx_err,o_n_freq_per_group)
    type(t_parallelism),intent(in)   :: ctx_paral
    real               ,intent(in)   :: frequency(:,:)
    integer            ,intent(in)   :: nfreq
    logical            ,intent(in)   :: flag_freq
    type(t_error)      ,intent(out)  :: ctx_err
    integer, optional  ,intent(in)   :: o_n_freq_per_group
    !!
    integer          :: k
    real             :: f
    character(len=4) :: unt
    
    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      if(.not. flag_freq) then
        write(6,'(2a)')   indicator, "  No frequency dependency for this equation "
      else
        write(6,'(2a)')   indicator, "  Complex frequencies: " //         &
                                     "(Fourier)     +  (Laplace) "
        do k=1,nfreq
          !! get memory unit for printing
          f=frequency(k,1)
          call convert_frequency_unit(f,unt)
     
          write(6,'(a,f12.4,a,f0.4)') "- complex frequency          : ", &
                                      f,trim(unt)//" +   ", frequency(k,2)
        enddo
        write(6,'(a)'   ) "------------------------------"
        write(6,'(a,i7)') "- Total number of frequency  : ",nfreq
        if(present(o_n_freq_per_group)) then
          write(6,'(a,i7)') "- Group size for inversion   : ",o_n_freq_per_group
        end if
        write(6,'(a)')    separator
      endif
    endif
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_frequency_list

end module m_print_freq
