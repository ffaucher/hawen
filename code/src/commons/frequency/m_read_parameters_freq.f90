!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_freq.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read frequency parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_freq
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_freq
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read the frequency list from parameter file
  !
  !> @param[in]   parameter_file : parameter file where infos are
  !> @param[out]  frequency     : array for the complex frequencies
  !> @param[out]  nfreq         : total number of frequency
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_freq(parameter_file,frequency,nfreq,ctx_err, &
                                  o_n_freq_per_group)
    character(len=*)   ,intent(in)   :: parameter_file
    real,allocatable   ,intent(out)  :: frequency(:,:)
    integer            ,intent(out)  :: nfreq
    type(t_error)      ,intent(out)  :: ctx_err
    integer, optional  ,intent(out)  :: o_n_freq_per_group
    !! local
    real(kind=8)              :: rlist1(512)
    real(kind=8)              :: rlist2(512)
    real(kind=8)              :: rmin,rmax,rstep
    real(kind=8),allocatable  :: freq_fourier(:),freq_laplace(:)
    integer                   :: nfreq_fourier,nfreq_laplace
    integer                   :: k1,k2,nval1,ilist(512),nval
    logical                   :: flag(3)

    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------
    !! Reading the frequencies
    !! -----------------------------------------------------------------
    !! Fourier
    call read_parameter(parameter_file,'frequency_fourier',0.d0,rlist1,nval1)
    
    !! allow a bypass for very large number of frequency
    flag = .false.  
    call read_parameter(parameter_file,'frequency_fourier_min' ,0.d0,     &
                        rlist2,nval,o_flag_found=flag(1))
    rmin  = rlist2(1)
    call read_parameter(parameter_file,'frequency_fourier_max' ,0.d0,     &
                        rlist2,nval,o_flag_found=flag(2))
    rmax  = rlist2(1)
    call read_parameter(parameter_file,'frequency_fourier_step',0.d0,     &
                        rlist2,nval,o_flag_found=flag(3))
    rstep = rlist2(1)
    !! only if all keyword are found
    if(all(flag .eqv. .true.) .and. rstep > tiny(1.0)) then
      !! count how many and compute them
      nfreq_fourier = nint((rmax-rmin)/rstep) + 1
      allocate(freq_fourier(nfreq_fourier))
      do k1=1,nfreq_fourier
        freq_fourier(k1) = dble(rmin + (k1-1)*rstep)
      end do
    else !! just use the first option 
      nfreq_fourier = nval1
      allocate(freq_fourier(nfreq_fourier))
      do k1=1,nfreq_fourier
        freq_fourier(k1) = rlist1(k1)
      end do    
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! Laplace
    call read_parameter(parameter_file,'frequency_laplace',0.d0,rlist1,nval1)
    flag = .false.  
    call read_parameter(parameter_file,'frequency_laplace_min' ,0.d0,   &
                        rlist2,nval,o_flag_found=flag(1))
    rmin  = rlist2(1)
    call read_parameter(parameter_file,'frequency_laplace_max' ,0.d0,   &
                        rlist2,nval,o_flag_found=flag(2))
    rmax  = rlist2(1)
    call read_parameter(parameter_file,'frequency_laplace_step',0.d0,   &
                        rlist2,nval,o_flag_found=flag(3))
    rstep = rlist2(1)
    !! only if all keyword are found
    if(all(flag .eqv. .true.) .and. rstep > tiny(1.0)) then
      !! count how many and compute them
      nfreq_laplace = nint((rmax-rmin)/rstep) + 1
      allocate(freq_laplace(nfreq_laplace))
      do k1=1,nfreq_laplace
        freq_laplace(k1) = dble(rmin + (k1-1)*rstep)
      end do
    else !! just use the first option 
      nfreq_laplace = nval1
      allocate(freq_laplace(nfreq_laplace))
      do k1=1,nfreq_laplace
        freq_laplace(k1) = rlist1(k1)
      end do
    end if
    !! -----------------------------------------------------------------

    !! there must be at least one frequency
    if(nfreq_laplace==1 .and. nfreq_fourier==1) then
      if(abs(freq_fourier(1)) < tiny(1.d0) .and.                        &
         abs(freq_laplace(1)) < tiny(1.d0)) then
         ctx_err%msg   = "** WARNING: zero frequency may lead to " //   &
                         "singular matrix or NaN values depending" //   &
                         " on the problem [read_parameters_freq] **"
         ctx_err%ierr  = 0
         ctx_err%critic=.false.
         call raise_error(ctx_err)
         ctx_err%msg=''
       endif
    endif

    !! count the total number of complex frequencies
    if(nfreq_fourier==1) then
      nfreq=nfreq_laplace
      allocate(frequency(nfreq,2))
      frequency(1:nfreq,2)=real(freq_laplace(1:nfreq))
      frequency(1:nfreq,1)=real(freq_fourier(1)      )
    elseif(nfreq_laplace==1) then
      nfreq=nfreq_fourier
      allocate(frequency(nfreq,2))
      frequency(1:nfreq,1)=real(freq_fourier(1:nfreq))
      frequency(1:nfreq,2)=real(freq_laplace(1)      )
    else
      nfreq=nfreq_laplace*nfreq_fourier
      allocate(frequency(nfreq,2))
      do k1=1,nfreq_fourier ; do k2=1,nfreq_laplace
        frequency((k1-1)*nfreq_laplace+k2,1)=real(freq_fourier(k1))
        frequency((k1-1)*nfreq_laplace+k2,2)=real(freq_laplace(k2))
      enddo ; enddo 
    endif
    !! -----------------------------------------------------------------
    if(present(o_n_freq_per_group)) then
      call read_parameter(parameter_file,'fwi_frequency_group',1,ilist,nval)
      o_n_freq_per_group = ilist(1)
      if(o_n_freq_per_group > nfreq) o_n_freq_per_group = nfreq
      if(o_n_freq_per_group < 1    ) then
        ctx_err%msg   = "** ERROR: you need at least one frequency " // &
                        "per group [read_parameters_freq] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    !! -----------------------------------------------------------------
    
    return
  end subroutine read_parameters_freq

end module m_read_parameters_freq
