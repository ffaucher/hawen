!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for init infos
!
!------------------------------------------------------------------------------
module m_print_init

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_print_basics,     only : separator,indicator,cast_memory
  implicit none
  
  private
  public :: print_init 
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print init info
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_init(ctx_paral,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_error)      ,intent(inout):: ctx_err

    call print_info_welcome_modeling(ctx_paral,ctx_err)
    call print_info_parall          (ctx_paral,ctx_err)
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_init
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print welcome message for modeling
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_welcome_modeling(ctx_paral,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      write(6,'(a)')    separator
      write(6,'(a)')    separator
      write(6,'(a)')    "  _________________________________________"
      write(6,'(a)')    " |                                         |"
      write(6,'(a)')    " |        _________________________        |"
      write(6,'(a)')    " |       |   WELCOME APPLICATION   |       |"
      write(6,'(a)')    " |       |_________________________|       |"
      write(6,'(a)')    " |                                         |"
      write(6,'(a)')    " |        Harmonic Wave Modeling           |"
      write(6,'(a)')    " |                                         |"
      write(6,'(a)')    " |_________________________________________|"
      write(6,'(a)')    separator
      write(6,'(a)')    separator
    endif
    return
  end subroutine print_info_welcome_modeling
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print parallelism info on screen
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_parall(ctx_paral,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    character(len=64) :: str_mem

    ctx_err%ierr  = 0
    
    if(ctx_paral%master) then
      write(6,'(2a)')   indicator, " MPI parallelism creation "
      write(6,'(a,i7)') "- number of nodes            : ", ctx_paral%n_node
      write(6,'(a,i7)') "- number of processors total : ", ctx_paral%nproc
      write(6,'(a,i7)') "- number of thread per proc  : ", ctx_paral%nthread
      if(ctx_paral%mem_per_node > 0) then
        call cast_memory(ctx_paral%mem_per_node,str_mem)
        write(6,'(2a)')   "- memory per node            : ",trim(adjustl(str_mem))
        call cast_memory(ctx_paral%mem_all_node,str_mem)
        write(6,'(2a)')   "- total memory available     : ",trim(adjustl(str_mem))
      end if

      write(6,'(a)')    separator
    endif

    return
  end subroutine print_info_parall

end module m_print_init
