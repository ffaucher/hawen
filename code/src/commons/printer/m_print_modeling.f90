!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_modeling.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for modeling infos
!
!------------------------------------------------------------------------------
module m_print_modeling

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_print_basics,     only : separator,indicator,cast_time,marker
  implicit none
  
  private
  public :: print_time_freq, print_time_mode
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print timer after a single frequency 
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   flag_freq     : check if frequency in this equation
  !> @param[in]   nfreq         : total number of frequency
  !> @param[in]   ifreq         : last frequency done
  !> @param[in]   timeloc       : time to solve this frequency
  !> @param[in]   timegb        : global time since frequency loop
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_time_freq(ctx_paral,flag_freq,nfreq,ifreq,timeloc,   &
                             timegb,ctx_err)
    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    logical            ,intent(in)   :: flag_freq
    integer            ,intent(in)   :: nfreq,ifreq
    real(kind=8)       ,intent(in)   :: timeloc, timegb
    type(t_error)      ,intent(inout):: ctx_err
    
    character(len=64)   :: str_time,locstr
    character(len=2)    :: fmti
    character(len=32)   :: fmtstr
    real     (kind=8)   :: time_estimate
    integer             :: isize
    
    if(ctx_err%ierr .ne. 0) return

    if(ctx_paral%master .and. flag_freq) then 
      isize = 0
      if(nfreq < 1000000) isize = 6
      if(nfreq < 100000)  isize = 5
      if(nfreq < 10000 )  isize = 4
      if(nfreq < 1000  )  isize = 3
      if(nfreq < 100   )  isize = 2
      if(nfreq < 10    )  isize = 1
      
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      
      fmtstr = '(a,'//fmti//',a,'//fmti//',a)'

      write(6,'(a)') marker
      call cast_time(timeloc,str_time)
      write(6,fmtstr)"- computations done for freq. ", ifreq," / ",     &
                     nfreq,": "//trim(adjustl(str_time))
      !! estimate times using linear progression 
      if(ifreq < nfreq) then
        time_estimate = timegb * dble(nfreq) / dble(ifreq) - timegb
        call cast_time(time_estimate,str_time)
        !! should be of length 35 + 2*isize
        locstr = ""
        locstr = "- estimated remaining time (linear)"
        write(6,'(2a)') locstr(1:35+2*isize-2)//": "// trim(adjustl(str_time))
      end if
      !! write(6,'(a)') marker
    end if

    return
  end subroutine print_time_freq

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print timer after a mode (all frequencies are computed)
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   flag_freq     : check if frequency in this equation
  !> @param[in]   nfreq         : total number of frequency
  !> @param[in]   ifreq         : last frequency done
  !> @param[in]   timeloc       : time to solve this frequency
  !> @param[in]   timegb        : global time since frequency loop
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_time_mode(ctx_paral,flag_freq,flag_mode,nfreq,nmode, &
                             imode,timeloc,timegb,ctx_err)
    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    logical            ,intent(in)   :: flag_freq,flag_mode
    integer            ,intent(in)   :: nmode,imode,nfreq
    real(kind=8)       ,intent(in)   :: timeloc, timegb
    type(t_error)      ,intent(inout):: ctx_err
    
    character(len=2)    :: fmti
    character(len=13)   :: fmtstr
    character(len=64)   :: str_time,locstr
    real     (kind=8)   :: time_estimate
    integer             :: isize,strlength
    
    
    if(ctx_err%ierr .ne. 0) return

    if(ctx_paral%master .and. flag_mode) then 
      isize = 0
      if(nmode < 1000000) isize = 6
      if(nmode < 100000)  isize = 5
      if(nmode < 10000 )  isize = 4
      if(nmode < 1000  )  isize = 3
      if(nmode < 100   )  isize = 2
      if(nmode < 10    )  isize = 1
      
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      write(fmti,'(a,i1)') 'i',isize
      
      fmtstr = '(a,'//fmti//',a,'//fmti//',a)'
      
      write(6,'(a)') marker
      call cast_time(timeloc,str_time)
      if(flag_freq .and. (nfreq > 1)) then
        write(6,fmtstr)"- computations done for mode ", imode," / ",    &
                       nmode," (all freq): "//trim(adjustl(str_time))
        strlength = 35 + 10 + 2*isize
      else
        write(6,fmtstr)"- computations done for mode  ", imode," / ",   &
                       nmode,": "//trim(adjustl(str_time))
        strlength = 35 + 2*isize
      end if

      !! estimate times using linear progression 
      if(imode < nmode) then
        time_estimate = timegb * dble(nmode) / dble(imode) - timegb
        call cast_time(time_estimate,str_time)
        locstr = ""
        locstr = "- estimated remaining time (linear)"
        write(6,'(2a)') locstr(1:strlength-2)//": "// trim(adjustl(str_time))
      end if
      !! write(6,'(a)') marker
    end if

    return
  end subroutine print_time_mode

end module m_print_modeling
