add_library(printer
m_print_basics.f90
m_print_init.f90
m_print_modeling.f90
)
target_link_libraries(printer parallelism)
install(TARGETS printer 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
