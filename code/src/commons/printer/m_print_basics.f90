!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_basics.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing
!
!------------------------------------------------------------------------------
module m_print_basics

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  implicit none
  
  !> separator on screen
  character(len=80) :: separator='----------------------------------------'// &
                                 '----------------------------------------'
  !> marker on screen
  character(len=60) :: marker   ='****************************************'// &
                                 '********************'
  !> indicaator on screen
  character(len=11) :: indicator='==========>'

  !> interface depending on the size of int.
  interface cast_memory
     module procedure cast_memory_int8
  end interface cast_memory
  
  !! ======================================
  !! list of units
  !!
  !! yotta   Y
  !! zetta   Z
  !! exa     E
  !! peta    P
  !! tera    T
  !! giga    G
  !! mega    M
  !! kilo    k
  !! ----
  !! milli   m
  !! micro   \f$\mu\f$
  !! nano    n
  !! pico    p
  !! femto   f
  !! atto    a
  !! zepto   z
  !! yocto   y
  !! ======================================
  
  private
  public :: separator,indicator,marker,cast_memory,cast_time
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> appropriate memory format, e.g., GiB, MiB.
  !
  !> @param[in]   mem_in     : input memory (in Bytes)
  !> @param[out]  str_out    : output memory string (in mem_unit)
  !> @param[in]   opt_div    : optional integer to indicate if it has
  !>                           already been divide, e.g., 1 means that
  !>                           the input memory as already been divided
  !>                           by 1024 once.
  !----------------------------------------------------------------------------
  subroutine cast_memory_int8(mem_in,str_out,opt_div)
    integer (kind=8)   ,intent(in)   :: mem_in
    character(len=*)   ,intent(inout):: str_out
    integer (kind=4)   ,optional, intent(in) :: opt_div
    
    !! local
    integer  (kind=8)   :: mem_out_i8
    integer  (kind=4)   :: div,i_div,div_start
    real     (kind=8)   :: mem_out_r8
    character(len=4)    :: mem_unit
    character(len=16)   :: stamp
    
    str_out=''
    div=0
    if(present(opt_div)) then
      div = opt_div
    end if
    
    !! initialization
    if(div==0) then !! at least KiB
      mem_out_i8 = int8(mem_in/1024)
      mem_out_r8 = dble(mem_in)/1024.d0
      div=1
    else
      mem_out_i8 = int8(mem_in)
      mem_out_r8 = dble(mem_in)
    end if
    
    div_start = div
    do i_div=div_start,8      
      if(mem_out_i8 > 1024) then
        div = div + 1
        mem_out_r8 = dble(mem_out_i8)/1024.d0
        mem_out_i8 = int8(mem_out_i8 /1024)
      else
        continue
      end if
    end do

    mem_unit='    '
    select case(div)
      case(1)
        mem_unit=' KiB'
      case(2)
        mem_unit=' MiB'
      case(3)
        mem_unit=' GiB'
      case(4)
        mem_unit=' TiB'
      case(5)
        mem_unit=' PiB'
      case(6)
        mem_unit=' EiB'
      case(7)
        mem_unit=' ZiB'
      case(8)
        mem_unit=' YiB'
    end select

    !! if(mem_out_i8 > 1024) then
    !!    mem_out_r8 = dble(mem_out_i8) / 1024.d0
    !!    mem_out_i8 = int8(mem_out_i8/1024)
    !!    mem_unit=' MiB'
    !! endif
    !! if(mem_out_i8 > 1024) then
    !!    mem_out_r8 = dble(mem_out_i8) / 1024.d0
    !!    mem_out_i8 = int8(mem_out_i8/1024)
    !!    mem_unit=' GiB'
    !! endif
    !! if(mem_out_i8 > 1024) then
    !!    mem_out_r8 = dble(mem_out_i8) / 1024.d0
    !!    mem_out_i8 = int8(mem_out_i8/1024)
    !!    mem_unit=' TiB'
    !! endif
    write(stamp,'(f0.3)')  mem_out_r8
    write(str_out,'(2a)')  trim(stamp) // mem_unit

    return
  end subroutine cast_memory_int8

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> appropriate time format
  !
  !> @param[in]   time_in    : input time (in sec)
  !> @param[out]  str_out    : output time string
  !----------------------------------------------------------------------------
  subroutine cast_time(time_in,str_out)
    real(kind=8)       ,intent(in)   :: time_in
    character(len=*)   ,intent(inout):: str_out
    !! local
    real(kind=8)     :: sec
    integer          :: days,hours,minutes
    character(len=10):: stamp
    
    days   =floor(time_in/(60.*60.*24))
    hours  =floor((time_in-real(days)*60.*60.*24)/(60.*60))
    minutes=floor((time_in-real(days)*60.*60.*24-real(hours)*60.*60.)/(60))
    sec    =time_in-real(days)*60.*60.*24-real(hours)*60.*60.-real(minutes)*60.
    
    str_out=''
    if(days.ne.0) then
      !! very special case
      if(days>=1000) then
        write(stamp,'(i10)')     days
      else
        write(stamp,'(i4)')     days
      endif
      write(str_out,'(a,a)') trim(stamp) // ' days '
    endif
    if(hours .ne.0 .or. days.ne. 0) then
      write(stamp,'(i3)')  hours
      write(str_out,'(a)') trim(adjustl(str_out)) // &
                           trim(stamp) // ' hrs '
    endif
    if(minutes.ne.0 .or. hours  .ne.0 .or. days.ne. 0) then
      write(stamp,'(i3)')  minutes
      write(str_out,'(a)') trim(adjustl(str_out)) // &
                           trim(stamp) // ' min '
    endif
    if(time_in < 60) then 
      write(stamp,'(f6.3)')  sec
    else
      write(stamp,'(i3)')  int(sec)
    endif
    write(str_out,'(a)') trim(adjustl(str_out)) // trim(stamp) // ' sec '

    if(time_in < 1.d-1) then ! given in millisecond
      write(stamp,'(f6.3)')  sec*1.d3
      write(str_out,'(a)') trim(stamp) // ' millisec '
    end if  
    return
  end subroutine cast_time
  
end module m_print_basics
