!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_conversion_spherical_coo.f90
!
!> @author 
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION: 
!> @brief
!> The module is used to convert vector and matrices defined in the 
!> spherical coordinates system into cylindrical coordinates or cartesian.
!
!------------------------------------------------------------------------------
module m_conversion_spherical_coo

  use m_raise_error,      only : t_error

  implicit none
  
  private
  
  !! spherical to cylindrical
  public :: convertcoo_sph_to_cyl_matrix
  public :: convertcoo_sph_to_cyl_vector
  !! spherical to cartesian
  public :: convertcoo_sph_to_cart_vector
  public :: convertcoo_sph_to_cart_matrix
    
  contains  

  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Convert matrix given into spherical coordinate system into 
  !> cylindrical ones, for 3 dimensions problem.
  !
  !> @param[in]   xcyl          : input cylindrical coordinates (\eta,\phi,z)
  !> @param[in]   mat_sph       : input matrix in spherical coordinates
  !> @param[out]  mat_cyl       : output matrix in cylindrical coordinates.
  !----------------------------------------------------------------------------
  subroutine convertcoo_sph_to_cyl_matrix(xcyl,mat_sph,mat_cyl)

    implicit none

    real   (kind=8)         ,intent(in)   :: xcyl   (3)
    real   (kind=8)         ,intent(in)   :: mat_sph(3,3)
    real   (kind=8)         ,intent(out)  :: mat_cyl(3,3)
    !!
    real  (kind=8)                        :: r,dxdr,dydr,dzdr
    real  (kind=8)                        :: P(3,3),matP(3,3)
    !! -----------------------------------------------------------------

    mat_cyl = 0.d0
    r = norm2(xcyl)
    if(r > tiny(1.0)) then
      dxdr = xcyl(1) / r
      dydr = xcyl(2) / r
      dzdr = xcyl(3) / r
    else !! they are all zero.
      dxdr = 1.d0
      dydr = 1.d0
      dzdr = 1.d0
    end if
    !! -----------------------------------------------------------------
    
    !! convert 
    !!        ( x/r   0   z/r) 
    !!  cyl = (  0    1    0 ) sph
    !!        ( z/r   0  -x/r) 
    !!
    P = 0.d0
    P(1,1) = dxdr ;                 P(1,3) = dzdr
                     P(2,2) = 1.d0 ;
    P(3,1) = dzdr ;                 P(3,3) =-dxdr
    matP    = matmul(mat_sph,P) 
    mat_cyl = matmul(P      ,matP) 

    return
    
  end subroutine convertcoo_sph_to_cyl_matrix


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Convert vector given into spherical coordinate system into 
  !> cylindrical ones, for 3 dimensions problem.
  !
  !> @param[in]   xcyl          : input cylindrical coordinates (\eta,\phi,z)
  !> @param[in]   vect_sph      : input vector in spherical coordinates
  !> @param[out]  vect_cyl      : output vector in cylindrical coordinates.
  !----------------------------------------------------------------------------
  subroutine convertcoo_sph_to_cyl_vector(xcyl,vect_sph,vect_cyl)

    implicit none

    real   (kind=8)         ,intent(in)   :: xcyl    (3)
    real   (kind=8)         ,intent(in)   :: vect_sph(3)
    real   (kind=8)         ,intent(out)  :: vect_cyl(3)
    !!
    real  (kind=8)                        :: r,dxdr,dydr,dzdr
    real  (kind=8)                        :: P(3,3)
    !! -----------------------------------------------------------------

    vect_cyl = 0.d0
    r = norm2(xcyl)
    if(r > tiny(1.0)) then
      dxdr = xcyl(1) / r
      dydr = xcyl(2) / r
      dzdr = xcyl(3) / r
    else !! they are all zero.
      dxdr = 1.d0
      dydr = 1.d0
      dzdr = 1.d0
    end if
    !! -----------------------------------------------------------------
    
    !! convert 
    !!        ( x/r   0   z/r) 
    !!  cyl = (  0    1    0 ) sph
    !!        ( z/r   0  -x/r) 
    !!
    P = 0.d0
    P(1,1) = dxdr ;                 P(1,3) = dzdr
                    P(2,2) = 1.d0 ;
    P(3,1) = dzdr ;                 P(3,3) =-dxdr
    vect_cyl = matmul(P,vect_sph) 
   
    return
    
  end subroutine convertcoo_sph_to_cyl_vector

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Convert vector given into spherical coordinate system into 
  !> cartesian ones, for 3 dimensions problem.
  !
  !> @param[in]   xcart         : input cartesian coordinates (x,y,z)
  !> @param[in]   vect_sph      : input vector in spherical coordinates
  !> @param[out]  vect_cart     : output vector in cartesian coordinates.
  !----------------------------------------------------------------------------
  subroutine convertcoo_sph_to_cart_vector(xcart,vect_sph,vect_cart)

    implicit none

    real   (kind=8)         ,intent(in)   :: xcart    (3)
    real   (kind=8)         ,intent(in)   :: vect_sph (3)
    real   (kind=8)         ,intent(out)  :: vect_cart(3)
    !!
    real  (kind=8)                        :: r,rxy
    real  (kind=8)                        :: P(3,3)
    !! -----------------------------------------------------------------

    vect_cart = 0.d0
    r   = norm2(xcart     )
    rxy = norm2(xcart(1:2))

    !! convert 
    P = 0.d0    
    if(rxy > tiny(1.0)) then   
      P(1,1) = xcart(1)/r 
      P(1,2) =-xcart(2)/rxy 
      P(1,3) = xcart(1)*xcart(3)/(r*rxy)
      
      P(2,1) = xcart(2)/r
      P(2,2) = xcart(1)/rxy 
      P(2,3) = xcart(2)*xcart(3)/(r*rxy)
      
      P(3,1) = xcart(3)/r
      P(3,2) = 0.d0
      P(3,3) =-rxy/r
    else
      P(1,1) = 1.d0 
      P(1,2) = 1.d0 
      P(1,3) = 1.d0
      
      P(2,1) = 1.d0
      P(2,2) = 1.d0
      P(2,3) =-1.d0
      
      P(3,1) =-1.d0
      P(3,2) = 1.d0
      P(3,3) = 0.d0
    end if
    !!        ( x/r            y/r           z/r ) 
    !!  cyl = ( xz/(r rxy)    yz/(r rxy)  -rxy/r ) sph
    !!        (-y/rxy          x/rxy          0  ) 
    !!
    vect_cart = matmul(P,vect_sph) 
   
    return
    
  end subroutine convertcoo_sph_to_cart_vector

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Convert vector given into spherical coordinate system into 
  !> cartesian ones, for 3 dimensions problem.
  !
  !> @param[in]   xcart         : input cartesian coordinates (x,y,z)
  !> @param[in]   mat_sph      : input matrix in spherical coordinates
  !> @param[out]  mat_cart     : output matrix in cartesian coordinates.
  !----------------------------------------------------------------------------
  subroutine convertcoo_sph_to_cart_matrix(xcart,mat_sph,mat_cart)

    implicit none

    real   (kind=8)         ,intent(in)   :: xcart   (3)
    real   (kind=8)         ,intent(in)   :: mat_sph (3,3)
    real   (kind=8)         ,intent(out)  :: mat_cart(3,3)
    !!
    real  (kind=8)                        :: r,rxy
    real  (kind=8)                        :: P(3,3),matP(3,3)
    !! -----------------------------------------------------------------

    mat_cart = 0.d0
    r   = norm2(xcart     )
    rxy = norm2(xcart(1:2))

    !! convert 
    P = 0.d0    
    if(rxy > tiny(1.0)) then   
      P(1,1) = xcart(1)/r 
      P(1,2) =-xcart(2)/rxy 
      P(1,3) = xcart(1)*xcart(3)/(r*rxy)
      
      P(2,1) = xcart(2)/r
      P(2,2) = xcart(1)/rxy 
      P(2,3) = xcart(2)*xcart(3)/(r*rxy)
      
      P(3,1) = xcart(3)/r
      P(3,2) = 0.d0
      P(3,3) =-rxy/r
    else
      P(1,1) = 1.d0 
      P(1,2) = 1.d0 
      P(1,3) = 1.d0
      
      P(2,1) = 1.d0
      P(2,2) = 1.d0
      P(2,3) =-1.d0
      
      P(3,1) =-1.d0
      P(3,2) = 1.d0
      P(3,3) = 0.d0
    end if
    !!        ( x/r            y/r           z/r ) 
    !!  cyl = ( xz/(r rxy)    yz/(r rxy)  -rxy/r ) sph
    !!        (-y/rxy          x/rxy          0  ) 
    !!
    matP     = matmul(mat_sph,P) 
    mat_cart = matmul(P      ,matP) 
   
    return
    
  end subroutine convertcoo_sph_to_cart_matrix
  
end module m_conversion_spherical_coo
