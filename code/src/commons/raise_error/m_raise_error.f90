!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_raise_error.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with unexpected error in the code
!
!------------------------------------------------------------------------------
module m_raise_error

  use m_error_terminate, only: critical_error_terminate_program
  
  implicit none
  private
  public :: raise_error, t_error
  
  !---------------------------------------------------------------------------  
  ! DESCRIPTION: 
  !> @brief
  !> type for error context handler
  !
  !> @details
  !> @param    ierr   : integer error code (=0 if no error)
  !> @param    msg    : message for error
  !> @param    critic : logical = .true. if the error is critical, so 
  !>                              the code will hard stop.
  !---------------------------------------------------------------------------  
  type t_error
    integer             :: ierr   !< error code
    character(len=512)  :: msg    !< error message
    logical             :: critic !< indicates critical error
  end type t_error
  
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Handle error
  !> @details
  !> It indicates error on screen and stops program if it is a critical error
  !
  !> @param[in]  ctx_err: error context
  !----------------------------------------------------------------------------
  subroutine raise_error(ctx_err)
    type(t_error)     :: ctx_err
    
    !! -----------------------------------------------------------------
    !! error message below ---------------------------------------------
    write(6,'(a)')    " "
    write(6,'(a)')    " ################################################################################"
    write(6,'(a)')    " ################################################################################"
    if(ctx_err%critic) then
      write(6,'(a)')    " ############### PROGRAM ERROR "
      write(6,'(a,i12)')" ## IERR VAL: ", ctx_err%ierr
      write(6,'(a)')    " ## MESSAGE : "
      write(6,'(2a)')   " ", trim(ctx_err%msg)
    else
      write(6,'(a)')    " ############### PROGRAM WARNING "
      write(6,'(2a)')   " ## MESSAGE : ", trim(adjustl(ctx_err%msg))
    end if
    write(6,'(a)')    " ################################################################################"
    write(6,'(a)')    " ################################################################################"
    write(6,'(a)')    " "
    if(ctx_err%critic) then
      call critical_error_terminate_program()
    endif
    return
  end subroutine raise_error  
end module m_raise_error
