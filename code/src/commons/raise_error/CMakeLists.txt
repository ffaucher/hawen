add_library(error_terminate ${PROJECT_SOURCE_DIR}/code/src/commons/parallelism/mpi/m_error_terminate.f90)
target_link_libraries(error_terminate MPI::MPI_Fortran)
install(TARGETS error_terminate 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
add_library(raise_error m_raise_error.f90)
target_link_libraries(raise_error PUBLIC error_terminate)
install(TARGETS raise_error 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
