!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_sep.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read parameters for header SEP file
!
!------------------------------------------------------------------------------
module m_read_parameters_sep
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_sep
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters from SEP header file 
  !
  !> @param[in]   header_sep    : file location
  !> @param[out]  n{x,y,z}      : number of nodes in X Y and Z
  !> @param[out]  {x,y,z}min    : origins in X Y and Z
  !> @param[out]  d{x,y,z}      : step size in X Y and Z
  !> @param[out]  binfile       : link to the binary model
  !> @param[out]  binformat     : binary model format
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_sep(header_sep,nx,ny,nz,xmin,ymin,zmin,    &
                                 dx,dy,dz,binfile,binformat,ctx_err)
    implicit none
    character(len=*)   ,intent(in)   :: header_sep
    integer            ,intent(out)  :: nx,ny,nz
    real(kind=8)       ,intent(out)  :: dx,dy,dz
    real(kind=8)       ,intent(out)  :: xmin,ymin,zmin
    character(len=*)   ,intent(out)  :: binfile,binformat
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    integer            :: itemp1(512),nval,ntemp
    real     (kind=8)  :: rtemp1(512),dtemp
    character(len=512) :: buffer

    ctx_err%ierr  = 0
      
    call read_parameter(header_sep,'n1',0,itemp1,nval)
    nx = itemp1(1)
    call read_parameter(header_sep,'n2',0,itemp1,nval)
    ny = itemp1(1)
    call read_parameter(header_sep,'n3',0,itemp1,nval)
    nz = itemp1(1)

    call read_parameter(header_sep,'o1',0.d0,rtemp1,nval)
    xmin = rtemp1(1)
    call read_parameter(header_sep,'o2',0.d0,rtemp1,nval)
    ymin = rtemp1(1)
    call read_parameter(header_sep,'o3',0.d0,rtemp1,nval)
    zmin = rtemp1(1)
    
    call read_parameter(header_sep,'d1',0.d0,rtemp1,nval)
    dx = rtemp1(1)
    call read_parameter(header_sep,'d2',0.d0,rtemp1,nval)
    dy = rtemp1(1)
    call read_parameter(header_sep,'d3',0.d0,rtemp1,nval)
    dz = rtemp1(1)
    !! if 2D, we force to have z as 2nd dimension
    if(ny > 1 .and. nz <=1) then
      ntemp = nz
      dtemp = dz
      dz    = dy
      nz    = nz
      dy    = dtemp
      ny    = ntemp
    end if


    !! model name and binary format
    binfile=''
    call read_parameter(header_sep,'in','',buffer,nval)
    binfile = trim(adjustl(buffer))
    binformat=''
    call read_parameter(header_sep,'data_format','',buffer,nval)
    binformat = trim(adjustl(buffer))
    
    !! constitency 
    if(nx==0 .or. ny==0 .or. nz==0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** SEP file named "// trim(adjustl(header_sep)) // &
                   " has 0 dimensions for n1, n2 AND/OR n3 [mesh_read_cartesian] "
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    endif
    if(dx<0. .or. dy<0 .or. dz<0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** SEP file named "// trim(adjustl(header_sep)) // &
                   " has negative step d1, d2 AND/OR d3 [mesh_read_cartesian] "
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    endif
          
    return
  end subroutine read_parameters_sep

end module m_read_parameters_sep
