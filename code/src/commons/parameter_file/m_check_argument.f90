!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_check_argument.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module check the input argument after lauching the program
!
!------------------------------------------------------------------------------
module m_check_argument

  use m_raise_error,      only : raise_error, t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_parallelism_init, only : end_parallelism
  implicit none
   
  private
  public :: check_input_argument
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> check input argument when lauching the program
  !> @details
  !> Verify that the input parameter file is given by the user or raise error
  !
  !> @param[in]   paral         : parallel context
  !> @param[out]  parameter_file: name of the parameter file
  !> @param[out]  check_flag    : flag to only check the parameter file
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine check_input_argument(paral,parameter_file,check_flag,ctx_err)
    type(t_parallelism)           :: paral
    character(len=*),intent(out)  :: parameter_file
    logical         ,intent(out)  :: check_flag
    type(t_error)   ,intent(out)  :: ctx_err
    !! local
    integer                       :: narg,iarg,file_length
    character(len=512)            :: local_arg
    logical                       :: file_exists

    ctx_err%ierr  = 0
    parameter_file=''
    check_flag    =.false.
    !! check number of argument ------------------------------------------
    narg=command_argument_count()
    if(narg<=0)then
      ctx_err%msg   = "** MISSING INPUT PARAMETER FILE **" 
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    do iarg=1,narg
      call get_command_argument(iarg,local_arg)
      !! different specific keyword --------------------------------------
      select case(adjustl(local_arg))
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !! HELP INFO
        case("--help","-h")
          if(paral%master) then
            write(6,'(a)')    " ============================== PROGRAM HELP    "
            write(6,'(a)')    " The program must be launch indicating the      "
            write(6,'(a)')    " input parameter file preceeded by the keyword  "
            write(6,'(a)')    " 'parameter='                         "
            write(6,'(a)')    " '-------------------------------     "
            write(6,'(a)')    " One can use the flag --check or -c   "
            write(6,'(a)')    " to only check the parameter without solving any"
            write(6,'(a)')    " wave problem                         "
            write(6,'(a)')    " '-------------------------------     "
            write(6,'(a)')    " ============================== END PROGRAM HELP"
          endif
          call barrier(paral%communicator,ctx_err)
          call end_parallelism(paral,ctx_err)
          stop
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !! FLAG CHECK PARAM FILE ONLY 
        case("--check","-c")
          check_flag=.true.
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !! GET FILENAME FOR PARAMETER FILE
        case default
          if(local_arg(1:10).eq.'parameter=') then
            file_length=len_trim(adjustl(local_arg)) - 10
            if(file_length > 0) then
              parameter_file=local_arg(11:10+file_length)
              inquire(file=trim(adjustl(parameter_file)),exist=file_exists)
              if(.not. file_exists) then
                ctx_err%ierr=-1
                ctx_err%msg ="** INPUT PARAMETER FILE "          //        &
                             trim(adjustl(parameter_file))       //        &
                             " DOEST NOT EXIST"
                ctx_err%critic=.true.      
                call raise_error(ctx_err)
              endif
            endif
          endif
      end select
    end do

    !! raise error if no parameter file
    if(adjustl(trim(parameter_file)).eq.'') then
      ctx_err%ierr=-1
      ctx_err%msg ="** MISSING INPUT PARAMETER FILE "
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif
    
    return
  end subroutine check_input_argument
end module m_check_argument
