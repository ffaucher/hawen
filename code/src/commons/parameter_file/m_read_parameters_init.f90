!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read init parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_init
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  use m_ctx_parallelism, only: t_parallelism
  use m_barrier,         only: barrier
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_init
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read the init from parameter file
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   parameter_file : parameter file where infos are
  !> @param[out]  dm            : problem dimension
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_init(ctx_paral,parameter_file,dm,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    character(len=*)   ,intent(in)   :: parameter_file
    integer            ,intent(out)  :: dm
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    integer            :: int_list(512)
    integer            :: nval

    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------
    !! Reading the basic infos -----------------------------------------
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'dimension',0,int_list,nval)
    dm=int_list(1)
    
    !! error dimension
    if(dm < 1 .or. dm > 3) then
        ctx_err%msg   = "** ERROR: dimension must be 1, 2 or 3 **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)    
    endif
    
    !! mpi_barrier waits for every processors
    call barrier(ctx_paral%communicator,ctx_err)
    
    return
  end subroutine read_parameters_init

end module m_read_parameters_init
