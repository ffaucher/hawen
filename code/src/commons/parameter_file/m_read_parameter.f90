!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameter.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read parameter value from and input parameter
!
!------------------------------------------------------------------------------
module m_read_parameter
  
  implicit none

  integer, parameter :: local_size=512   
  private
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Read parameter in the input file as keyword=value
  !
  !> @param[in]  inputfile   : inputfile
  !> @param[in]  keyword     : keyword we are looking for
  !> @param[in]  default_val : default_value
  !> @param[out] valp        : value found in the parameter
  !> @param[out] nval        : number of values in list
  !----------------------------------------------------------------------------
  interface read_parameter
     module procedure read_parameter_int4
     module procedure read_parameter_real4
     module procedure read_parameter_int8
     module procedure read_parameter_real8
     module procedure read_parameter_one_string
     module procedure read_parameter_list_string
     module procedure read_parameter_logical
  end interface

  public :: read_parameter
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> find the appropriate string that correspond to the keyword
  !> in the parameter file it reads as keyword=string_out
  !
  !> @param[in]  inputfile        : inputfile
  !> @param[in]  keyword          : keyword we are looking for
  !> @param[in]  string_out       : output string of interest
  !----------------------------------------------------------------------------
  subroutine string_of_interrest(inputfile,keyword,string_out)
    character(len=*)         ,intent(in)   :: inputfile,keyword
    character(len=*)         ,intent(inout):: string_out
    !! local
    integer                       :: io
    logical                       :: end_file,stop_count
    character(len=10*local_size)  :: current_line
    integer                       :: key_len,lin_len,k,iend

    string_out=''
    key_len=len_trim(adjustl(keyword)) + 1
    lin_len=0
    !! -----------------------------------------------------------------
    !! open file -------------------------------------------------------
    open (unit=10,file=trim(adjustl(inputfile)))
    end_file=.false.
    !! read ------------------------------------------------------------
    do while(.not. end_file)
      read(10,"(a)",iostat=io)  current_line
      if(is_iostat_end(io)) then
        end_file=.true.
      else
        !! check line is non-empty or not commentary
        if(len_trim(adjustl(current_line)).ne.0 .and. &
           current_line(1:1) .ne. "#") then
          !! check if it is the good keyword
          if(current_line(1:key_len) .eq. &
             trim(trim(adjustl(keyword))//"=")) then
            !! get line length
            lin_len=len_trim(adjustl(current_line))
            !! get end position for the current param
            iend=0 ; stop_count=.false.
            do k=key_len+1,lin_len
              !! while no space or comment "#"
              if(current_line(k:k) .ne. ""  .and. &
                 current_line(k:k) .ne. "#" .and. .not. stop_count) then
                iend = k
              else
                stop_count = .true.
              endif
            enddo
            if(iend>key_len) then
              string_out=trim(adjustl(current_line(key_len+1:iend)))
            endif
          endif
        endif
      endif
    end do
    !! close -----------------------------------------------------------
    close (unit=10)
    return
  end subroutine string_of_interrest

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> find the number of parameters in the string, they are delimited
  !> by commas
  !
  !> @param[in]  char_list        : string containing the list
  !> @param[in]  nval             : output number of values
  !----------------------------------------------------------------------------
  subroutine count_param_in_char(char_list,nval)
    character(len=*)         ,intent(in)  :: char_list
    integer                  ,intent(out) :: nval
    !! local
    integer                       :: k,line_length

    nval=1
    line_length=len_trim(adjustl(char_list))
    do k=1,(line_length-1)
      if(char_list(k:k) .eq. ",") then
        nval=nval+1
      endif
    enddo
    return
  end subroutine count_param_in_char

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> find the position of a parameter in the string starting from istart
  !> @details
  !> start to search from istart
  !
  !> @param[in]  char_list        : string containing the list
  !> @param[in]  istart           : first index position
  !> @param[out] iend             : last index position
  !----------------------------------------------------------------------------
  subroutine get_param_index(char_list,istart,iend)
    character(len=*)         ,intent(in)    :: char_list
    integer                  ,intent(in)    :: istart
    integer                  ,intent(out)   :: iend
    !! local
    integer                       :: k,line_length
    logical                       :: found
    
    line_length=len_trim(adjustl(char_list))
    iend=line_length
    found=.false.
    do k=istart,line_length
      if(char_list(k:k) .eq. "," .and. .not. found) then
        iend=k-1
        found=.true.
      endif
    enddo
    return
  end subroutine get_param_index
  
  !! read list of integer ---------------------------------------------------
  subroutine read_parameter_int4(inputfile,keyword,default_val,valp,nval, & 
                                 o_flag_found)
    character(len=*),intent(in)   :: inputfile,keyword
    integer         ,intent(in)   :: default_val
    integer         ,intent(out)  :: valp(:)
    integer         ,intent(out)  :: nval
    logical, optional,intent(out) :: o_flag_found
    !! local
    character(len=local_size)     :: char_val
    integer                       :: k,i1,i2
    logical                       :: flag_found

    flag_found = .false.
    valp=default_val
    nval=1
    call string_of_interrest(inputfile,keyword,char_val)
    if(trim(adjustl(char_val)) .ne. '' .and. &
       len_trim(adjustl(char_val)).ne. 0) then
      flag_found=.true.
      !! count number of parameters
      call count_param_in_char(char_val,nval)
      i1=1
      do k=1,nval
        call get_param_index(char_val,i1,i2)
        read(char_val(i1:i2),*) valp(k)
        i1=i2+2
      enddo
    else
      !! default value    
    endif

    if(present(o_flag_found)) o_flag_found=flag_found
    return
  end subroutine read_parameter_int4
  
  !! -------------------------------------------------------------------
  !> read list of real
  !>
  !! -------------------------------------------------------------------
  subroutine read_parameter_real4(inputfile,keyword,default_val,valp,  &
                                  nval,o_flag_found)
    character(len=*) ,intent(in)   :: inputfile,keyword
    real             ,intent(in)   :: default_val
    real             ,intent(out)  :: valp(:)
    integer          ,intent(out)  :: nval
    logical, optional,intent(out)  :: o_flag_found
    !! local
    character(len=10*local_size)  :: char_val
    integer                       :: k,i1,i2
    logical                       :: flag_found

    flag_found = .false.
    
    valp=default_val
    nval=1
    call string_of_interrest(inputfile,keyword,char_val)
    if(trim(adjustl(char_val)) .ne. '' .and. &
       len_trim(adjustl(char_val)).ne. 0) then
      flag_found=.true.
      !! count number of parameters
      call count_param_in_char(char_val,nval)
      i1=1
      do k=1,nval
        call get_param_index(char_val,i1,i2)
        read(char_val(i1:i2),*) valp(k)
        i1=i2+2
      enddo
    else
      !! default value with flag_found=.false.
    endif
    
    if(present(o_flag_found)) o_flag_found=flag_found
    
    return
  end subroutine read_parameter_real4
  
  !! read one string ---------------------------------------------------
  subroutine read_parameter_one_string(inputfile,keyword,default_val,valp,nval)
    character(len=*)         ,intent(in)   :: inputfile,keyword
    character(len=*)         ,intent(in)   :: default_val
    character(len=local_size),intent(out)  :: valp
    integer                  ,intent(out)  :: nval
    !! local
    character(len=local_size)     :: char_val
    integer                       :: i1,i2


    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    !! Rk: in the read_parameter_list_string below
    !!     we allow to return zero value, maybe we
    !!     should do the same here
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    valp=trim(adjustl(default_val))
    nval=1
    call string_of_interrest(inputfile,keyword,char_val)
    if(trim(adjustl(char_val)) .ne. '' ) then
      valp=''
      !! count number of parameters
      call count_param_in_char(char_val,nval)
      i1=1
      call get_param_index(char_val,i1,i2)
      valp(i1:i2) = char_val(i1:i2)
    else
      !! default value    
    endif
    !! if several strings, only the first
    if(nval>1) then
      write(6,'(a)') "  ##########################################"
      write(6,'(a)') "  ### WARNING PARAMETER FILE              ##"
      write(6,'(a)') "  ##########################################"
      write(6,'(a)') "  ### SEVERAL STRINGS GIVEN FOR "
      write(6,'(2a)')"  ### KEYWORD: ", keyword
      write(6,'(a)') "  ### ONLY THE FIRST ONE IS KEPT"
      write(6,'(a)') "  ##########################################"
    endif
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    !! Rk: in the read_parameter_list_string below
    !!     we allow to return zero value, maybe we
    !!     should do the same here
    nval=1
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------

    return
  end subroutine read_parameter_one_string
  
  !! read list of string -----------------------------------------------
  subroutine read_parameter_list_string(inputfile,keyword,default_val,valp,nval)
    character(len=*)         ,intent(in)   :: inputfile,keyword
    character(len=*)         ,intent(in)   :: default_val
    character(len=local_size),intent(out)  :: valp(:)
    integer                  ,intent(out)  :: nval
    !! local
    character(len=local_size)     :: char_val
    integer                       :: i1,i2,k

    valp=trim(adjustl(default_val))
    nval=1
    call string_of_interrest(inputfile,keyword,char_val)
    if(trim(adjustl(char_val)) .ne. '' ) then
      valp=''
      !! count number of parameters
      call count_param_in_char(char_val,nval)
      i1=1
      do k=1,nval
        call get_param_index(char_val,i1,i2)
        valp(k) = char_val(i1:i2)
        i1=i2+2
      enddo
    else
      !! default value    
      !! no default value
      if(trim(adjustl(default_val)) .eq. '') nval=0
    endif
    

    return
  end subroutine read_parameter_list_string
  
  !! read list of logical ----------------------------------------------
  subroutine read_parameter_logical(inputfile,keyword,default_val,valp,nval)
    character(len=*),intent(in)   :: inputfile,keyword
    logical         ,intent(in)   :: default_val
    logical         ,intent(out)  :: valp(:)
    integer         ,intent(out)  :: nval
    !! local
    character(len=local_size)     :: char_val
    integer                       :: k,i1,i2

    valp=default_val
    nval=1
    call string_of_interrest(inputfile,keyword,char_val)
    if(trim(adjustl(char_val)) .ne. '' ) then
      !! count number of parameters
      call count_param_in_char(char_val,nval)
      i1=1
      do k=1,nval
        call get_param_index(char_val,i1,i2)
        if(char_val(i1:i2).eq. "true") then
          valp(k) = .true.
        elseif(char_val(i1:i2).eq. "false") then
          valp(k) = .false.
        else
          write(6,'(a)') "  ##########################################"
          write(6,'(a)') "  ### WARNING PARAMETER FILE              ##"
          write(6,'(a)') "  ##########################################"
          write(6,'(a)') "  ### UNRECOGNIZED LOGICAL LIST ITEM FOR "
          write(6,'(2a)')"  ### KEYWORD: ", keyword
          write(6,'(2a)')"  ### VALUE  : ", char_val(i1:i2)
          write(6,'(a)') "  ### IT WILL BE REPLACED BY FALSE"
          write(6,'(a)') "  ##########################################"
        endif                
        i1=i2+2
      enddo
    else
      !! default value    
    endif

    return
  end subroutine read_parameter_logical
  
  
  !! read list of integer8---------------------------------------------------
  subroutine read_parameter_int8(inputfile,keyword,default_val,valp,nval, & 
                                 o_flag_found)
    character(len=*),intent(in)   :: inputfile,keyword
    integer(kind=8) ,intent(in)   :: default_val
    integer(kind=8) ,intent(out)  :: valp(:)
    integer         ,intent(out)  :: nval
    logical, optional,intent(out)  :: o_flag_found
    !! local
    character(len=local_size)     :: char_val
    integer                       :: k,i1,i2
    logical                       :: flag_found

    flag_found = .false.
    valp=default_val
    nval=1
    call string_of_interrest(inputfile,keyword,char_val)
    if(trim(adjustl(char_val)) .ne. '' .and. &
       len_trim(adjustl(char_val)).ne. 0) then
      flag_found=.true.
      !! count number of parameters
      call count_param_in_char(char_val,nval)
      i1=1
      do k=1,nval
        call get_param_index(char_val,i1,i2)
        read(char_val(i1:i2),*) valp(k)
        i1=i2+2
      enddo
    else
      !! default value    
    endif

    if(present(o_flag_found)) o_flag_found=flag_found
    return
  end subroutine read_parameter_int8
  
  !! read list of real8-------------------------------------------------
  subroutine read_parameter_real8(inputfile,keyword,default_val,valp,   &
                                  nval,o_flag_found)
    character(len=*),intent(in)   :: inputfile,keyword
    real(kind=8)    ,intent(in)   :: default_val
    real(kind=8)    ,intent(out)  :: valp(:)
    integer         ,intent(out)  :: nval
    logical, optional,intent(out) :: o_flag_found
    !! local
    character(len=10*local_size)  :: char_val
    integer                       :: k,i1,i2
    logical                       :: flag_found

    flag_found = .false.
    valp=default_val
    nval=1
    call string_of_interrest(inputfile,keyword,char_val)
    if(trim(adjustl(char_val)) .ne. '' .and. &
       len_trim(adjustl(char_val)).ne. 0) then
      flag_found=.true.
      !! count number of parameters
      call count_param_in_char(char_val,nval)
      i1=1
      do k=1,nval
        call get_param_index(char_val,i1,i2)
        read(char_val(i1:i2),*) valp(k)
        i1=i2+2
      enddo
    else
      !! default value    
    endif

    if(present(o_flag_found)) o_flag_found=flag_found
    return
  end subroutine read_parameter_real8

  
end module m_read_parameter
