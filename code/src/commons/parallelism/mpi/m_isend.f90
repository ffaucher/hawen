!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_isend.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to isend stuff
!> @details
!> It uses mpi parallelism intrinsic function
!
!------------------------------------------------------------------------------
module m_isend

  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none

  private
  private :: mpi_error_handler

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> ISend parameter
  !
  !> @param[in]  val         : parameter to isend
  !> @param[in]  nval        : number of parameters
  !> @param[in]  whereto     : where to isend
  !> @param[in]  tag         : tag
  !> @param[in]  communicator: communicator ID for barrier
  !> @param[out] request     : requested ID for process
  !> @param[out] ctx_err     : context when raising error
  !----------------------------------------------------------------------------
  interface isend
     module procedure isend_value_char_parallelism
     module procedure isend_values_char_parallelism
     module procedure isend_value_int4_parallelism
     module procedure isend_values_int4_parallelism
     module procedure isend_4dvalues_int4_parallelism
     module procedure isend_3dvalues_int4_parallelism
     module procedure isend_2dvalues_int4_parallelism
     module procedure isend_value_int8_parallelism
     module procedure isend_values_int8_parallelism
     module procedure isend_3dvalues_int8_parallelism
     module procedure isend_2dvalues_int8_parallelism
     module procedure isend_value_real4_parallelism
     module procedure isend_values_real4_parallelism
     module procedure isend_3dvalues_real4_parallelism
     module procedure isend_2dvalues_real4_parallelism
     module procedure isend_value_real8_parallelism
     module procedure isend_values_real8_parallelism
     module procedure isend_3dvalues_real8_parallelism
     module procedure isend_2dvalues_real8_parallelism
     module procedure isend_value_complex8_parallelism
     module procedure isend_values_complex8_parallelism
     module procedure isend_3dvalues_complex8_parallelism
     module procedure isend_value_complex16_parallelism
     module procedure isend_values_complex16_parallelism
     module procedure isend_3dvalues_complex16_parallelism
     module procedure isend_2dvalues_complex8_parallelism
     module procedure isend_4dvalues_real4_parallelism
  end interface

  public  ::  isend
contains

  !! Send a single char
  subroutine isend_value_char_parallelism(val_char,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    character(len=*),intent(inout)      :: val_char
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_char,nval*len(val_char),mpi_character,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_value_char_parallelism

  !! Send an array of char
  subroutine isend_values_char_parallelism(val_char,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    character(len=*),intent(inout)      :: val_char(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_char,nval*len(val_char),mpi_character,whereto,tag, &
                   communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_values_char_parallelism

  !! Send a single int4
  subroutine isend_value_int4_parallelism(val_int,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    integer,intent(inout)               :: val_int
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_value_int4_parallelism

  !! Send an array of int4
  subroutine isend_values_int4_parallelism(val_int,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    integer,intent(inout)               :: val_int(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_values_int4_parallelism

  !! Send a 3d array of int4
  subroutine isend_3dvalues_int4_parallelism(val_int,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_3dvalues_int4_parallelism

  !! Send a 2d array of int4
  subroutine isend_2dvalues_int4_parallelism(val_int,nval,whereto,tag, &
                                             communicator,request,ctx_err)

    integer,allocatable,intent(inout)   :: val_int(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_2dvalues_int4_parallelism

  !! Send a 4d array of int4
  subroutine isend_4dvalues_int4_parallelism(val_int,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_4dvalues_int4_parallelism

  !! Send a single int8
  subroutine isend_value_int8_parallelism(val_int,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    integer(kind=8),intent(inout)       :: val_int
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_value_int8_parallelism

  !! Send an array of int8
  subroutine isend_values_int8_parallelism(val_int,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    integer(kind=8),intent(inout)       :: val_int(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_values_int8_parallelism

  !! Send a 3d array of int8
  subroutine isend_3dvalues_int8_parallelism(val_int,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    integer(kind=8),intent(inout),allocatable:: val_int(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_3dvalues_int8_parallelism

  !! Send a 2d array of int8
  subroutine isend_2dvalues_int8_parallelism(val_int,nval,whereto,tag, &
                                             communicator,request,ctx_err)

    integer(kind=8),allocatable,intent(inout)   :: val_int(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_int,nval,mpi_integer8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_2dvalues_int8_parallelism

  !! Send a single real4
  subroutine isend_value_real4_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real,intent(inout)                  :: val_re
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_value_real4_parallelism

  !! Send an array of real4
  subroutine isend_values_real4_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real,intent(inout)                  :: val_re(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_values_real4_parallelism

  !! Send a 3d array of real4
  subroutine isend_3dvalues_real4_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_3dvalues_real4_parallelism

  !! Send a 2d array of real4
  subroutine isend_2dvalues_real4_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_2dvalues_real4_parallelism

  !! Send a 4d array of real4
  subroutine isend_4dvalues_real4_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_4dvalues_real4_parallelism

  !! Send a single real8
  subroutine isend_value_real8_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real(kind=8),intent(inout)          :: val_re
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_value_real8_parallelism

  !! Send an array of real8
  subroutine isend_values_real8_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real(kind=8),intent(inout)          :: val_re(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_values_real8_parallelism

  !! Send a 3d array of real8
  subroutine isend_3dvalues_real8_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real(kind=8),intent(inout),allocatable:: val_re(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_3dvalues_real8_parallelism

  !! Send a 2d array of real8
  subroutine isend_2dvalues_real8_parallelism(val_re,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    real(kind=8),intent(inout),allocatable:: val_re(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_re,nval,mpi_real8,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_2dvalues_real8_parallelism
  
  !! Send a single complex8
  subroutine isend_value_complex8_parallelism(val_cx,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    complex,intent(inout)               :: val_cx
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_cx,nval,mpi_complex,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_value_complex8_parallelism

  !! Send an array of complex8
  subroutine isend_values_complex8_parallelism(val_cx,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    complex,intent(inout)               :: val_cx(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_cx,nval,mpi_complex,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_values_complex8_parallelism

  !! Send a 2d array of complex8
  subroutine isend_2dvalues_complex8_parallelism(val_cx,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    complex,intent(inout),allocatable   :: val_cx(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_cx,nval,mpi_complex,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_2dvalues_complex8_parallelism

  !! Send a 3d array of complex8
  subroutine isend_3dvalues_complex8_parallelism(val_cx,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    complex,intent(inout),allocatable   :: val_cx(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_cx,nval,mpi_complex,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_3dvalues_complex8_parallelism

  !! Send a single complex16
  subroutine isend_value_complex16_parallelism(val_cx,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    complex(kind=8),intent(inout)       :: val_cx
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_cx,nval,mpi_complex16,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_value_complex16_parallelism

  !! Send an array of complex16
  subroutine isend_values_complex16_parallelism(val_cx,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    complex(kind=8),intent(inout)       :: val_cx(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_cx,nval,mpi_complex16,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_values_complex16_parallelism

  !! Send an array of complex16
  subroutine isend_3dvalues_complex16_parallelism(val_cx,nval,whereto,tag, &
                                              communicator,request,ctx_err)

    complex(kind=8),intent(inout),allocatable:: val_cx(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: whereto,tag,communicator
    integer,intent(out)                 :: request
    type(t_error),intent(out)           :: ctx_err

    call mpi_isend(val_cx,nval,mpi_complex16,whereto,tag,communicator,request,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine isend_3dvalues_complex16_parallelism

  !! ===================================================================
  !! error handler
  !! ===================================================================
  subroutine mpi_error_handler(ctx_err)

    type(t_error),intent(inout)           :: ctx_err
    
    select case(ctx_err%ierr)
      case(MPI_SUCCESS)
        !! No error; MPI routine completed successfully.
      case(MPI_ERR_COMM)
        !! Invalid communicator. A common error is to use a null communicator 
        !! in a call (not even allowed in MPI_Comm_rank).
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_ISEND: " // &
                     " INVALID COMMUNICATOR ** "
        ctx_err%critic=.true.
      case(MPI_ERR_TYPE)
        !! Invalid datatype argument. Additionally, this error can occur 
        !! if an uncommitted MPI_Datatype (see MPI_Type_commit) is used in 
        !! a communication call.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_ISEND: " // &
                     " INVALID MPI_DATATYPE ** "
        ctx_err%critic=.true.
      case(MPI_ERR_COUNT)
        !! Invalid count argument. Count arguments must be non-negative; 
        !! a count of zero is often valid.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_ISEND: " // &
                     " INVALID COUNT ** "
        ctx_err%critic=.true.
      case(MPI_ERR_TAG)
        !! Invalid tag argument. Tags must be non-negative; tags in a receive 
        !! (MPI_Recv, MPI_Irecv, MPI_Sendrecv, etc.) may also be MPI_ANY_TAG. 
        !! The largest tag value is available through the the attribute MPI_TAG_UB.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_ISEND: " // &
                     " INVALID RECV_TAG ** "
        ctx_err%critic=.true.
      case(MPI_ERR_RANK)
        !! Invalid source or destination rank. Ranks must be between zero and 
        !! the size of the communicator minus one; ranks in a receive 
        !! (MPI_Recv, MPI_Irecv, MPI_Sendrecv, etc.) may also be MPI_ANY_SOURCE.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_ISEND: " // &
                     " INVALID RECV SOURCE RANK ** "
        ctx_err%critic=.true.
      case(MPI_ERR_INTERN)
        !! This error is returned when some part of the MPICH implementation 
        !! is unable to acquire memory.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_ISEND: " // &
                     " UNABLE TO ACQUIRE MEMORY ** "
        ctx_err%critic=.true.
      case default
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_ISEND: " // &
                     " UNKNOWN ERROR ** "
        ctx_err%critic=.true.
    end select
    return
  end subroutine mpi_error_handler

end module m_isend
