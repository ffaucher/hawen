!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_barrier.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module is used to barrier processors 
!> @details
!> It uses mpi parallelism intrinsic function
!
!------------------------------------------------------------------------------
module m_barrier

  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none
   
  private

  interface barrier
     module procedure barrier_main
  end interface

  public  ::  barrier
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Barrier for mpi parallelism
  !
  !> @param[in]  communicator: communicator ID for barrier
  !> @param[out] ctx_err     : context when raising error
  !----------------------------------------------------------------------------
  subroutine barrier_main(communicator,ctx_err)

   integer,intent(in)                  :: communicator
   type(t_error),intent(inout)         :: ctx_err
   integer                             :: i_err
   !! in case of a non-shared error
   call mpi_barrier(communicator,i_err)
   
   if(ctx_err%ierr .eq. 0) ctx_err%ierr = i_err

   if (ctx_err%ierr.ne.0) then
     ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BARRIER **"
     ctx_err%critic=.true.      
     call raise_error(ctx_err)
   endif
   return
  end subroutine barrier_main
end module m_barrier
