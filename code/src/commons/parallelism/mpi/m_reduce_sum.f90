!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_reduce_sum.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to sum parameters on one processor
!> @details
!> It uses mpi parallelism intrinsic function MPI_REDUCE
!
!------------------------------------------------------------------------------
module m_reduce_sum

  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none

  private

  character(len=*), parameter :: &
      message_error="** ERROR DURING SUBROUTINE MPI_REDUCE (MPI_SUM) **"

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Sum processors parameters
  !
  !> @param[in]  val_in      : input parameter to sum
  !> @param[out] val_out     : output summed parameter
  !> @param[in]  nval        : number of parameters
  !> @param[in]  master      : what processor will keep the parameter
  !> @param[in]  communicator: communicator ID for barrier
  !> @param[out] ctx_err     : context when raising error
  !----------------------------------------------------------------------------
  interface reduce_sum
     module procedure reduce_sum_value_int4
     module procedure reduce_sum_values_int4
     module procedure reduce_sum_3dvalues_int4
     module procedure reduce_sum_value_int8
     module procedure reduce_sum_values_int8
     module procedure reduce_sum_3dvalues_int8
     module procedure reduce_sum_value_real4
     module procedure reduce_sum_values_real4
     module procedure reduce_sum_3dvalues_real4
     module procedure reduce_sum_value_real8
     module procedure reduce_sum_values_real8
     module procedure reduce_sum_3dvalues_real8
     module procedure reduce_sum_value_complex8
     module procedure reduce_sum_values_complex8
     module procedure reduce_sum_3dvalues_complex8
     module procedure reduce_sum_value_complex16
     module procedure reduce_sum_values_complex16
     module procedure reduce_sum_3dvalues_complex16
     module procedure reduce_sum_4dvalues_real4
     module procedure reduce_sum_2dvalues_complex8
     module procedure reduce_sum_2dvalues_complex16
     module procedure reduce_sum_2dvalues_int8
     module procedure reduce_sum_2dvalues_int4
     module procedure reduce_sum_2dvalues_real4
     module procedure reduce_sum_2dvalues_real8
  end interface

  public  ::  reduce_sum
contains

  !! reduce a single int4
  subroutine reduce_sum_value_int4(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(in)                  :: val_in
    integer,intent(out)                 :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_reduce(val_in,val_out,nval,mpi_integer,mpi_sum,master,  &
                      communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_value_int4

  !! reduce an array of int4
  subroutine reduce_sum_values_int4(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(in)                  :: val_in(:)
    integer,intent(out)                 :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_integer,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_values_int4

  !! reduce an array of int8
  subroutine reduce_sum_values_int8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    integer(kind=8),intent(in)                  :: val_in(:)
    integer(kind=8),intent(out)                 :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_integer8,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_values_int8


  !! reduce a 3D array of int4
  subroutine reduce_sum_3dvalues_int4(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_in(:,:,:)
    integer,intent(inout),allocatable   :: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_integer,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_3dvalues_int4


  !! reduce a single int8
  subroutine reduce_sum_value_int8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    integer(kind=8),intent(in)          :: val_in
    integer(kind=8),intent(out)         :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_reduce(val_in,val_out,nval,mpi_integer8,mpi_sum,master,  &
                      communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_value_int8

  !! reduce a 3d array of int8
  subroutine reduce_sum_3dvalues_int8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout),allocatable:: val_in(:,:,:)
    integer(kind=8),intent(inout),allocatable:: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_integer8,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_3dvalues_int8


  !! reduce a single real4
  subroutine reduce_sum_value_real4(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    real,intent(in)                     :: val_in
    real,intent(out)                    :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_reduce(val_in,val_out,nval,mpi_real,mpi_sum,master,  &
                      communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_value_real4

  !! reduce an array of real4
  subroutine reduce_sum_values_real4(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    real,intent(in)                     :: val_in(:)
    real,intent(out)                    :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_real,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_values_real4

  !! reduce a 3d array of real4
  subroutine reduce_sum_3dvalues_real4(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    real,intent(inout),allocatable      :: val_in(:,:,:)
    real,intent(inout),allocatable      :: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_real,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_3dvalues_real4


  !! reduce a 4d array of real4
  subroutine reduce_sum_4dvalues_real4(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    real,intent(inout),allocatable      :: val_in(:,:,:,:)
    real,intent(inout),allocatable      :: val_out(:,:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_real,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_4dvalues_real4


  !! reduce a single real8
  subroutine reduce_sum_value_real8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    real(kind=8),intent(in)             :: val_in
    real(kind=8),intent(out)            :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_reduce(val_in,val_out,nval,mpi_real8,mpi_sum,master,  &
                      communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_value_real8

  !! reduce an array of real8
  subroutine reduce_sum_values_real8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    real(kind=8),intent(in)             :: val_in(:)
    real(kind=8),intent(out)            :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_real8,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_values_real8

  !! reduce an array of real8
  subroutine reduce_sum_3dvalues_real8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    real(kind=8),intent(inout),allocatable:: val_in(:,:,:)
    real(kind=8),intent(inout),allocatable:: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_real8,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_3dvalues_real8


  !! reduce a single complex8
  subroutine reduce_sum_value_complex8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(in)                  :: val_in
    complex,intent(out)                 :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_reduce(val_in,val_out,nval,mpi_complex,mpi_sum,master,  &
                      communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_value_complex8

  !! reduce an array of complex8
  subroutine reduce_sum_values_complex8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(in)                  :: val_in(:)
    complex,intent(out)                 :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_complex,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_values_complex8

  !! reduce a 2d array of complex8
  subroutine reduce_sum_2dvalues_complex8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(inout)               :: val_in(:,:)
    complex,intent(inout)               :: val_out(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_complex,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_2dvalues_complex8

  !! reduce a 2d array of int4
  subroutine reduce_sum_2dvalues_int4(val_in,val_out,nval,master,    &
                                                  communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_in(:,:)
    integer,intent(inout),allocatable   :: val_out(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_integer,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_2dvalues_int4
  !! reduce a 2d array of int8
  subroutine reduce_sum_2dvalues_int8(val_in,val_out,nval,master,    &
                                                  communicator,ctx_err)

    integer(kind=8),intent(inout),allocatable   :: val_in(:,:)
    integer(kind=8),intent(inout),allocatable   :: val_out(:,:)
    integer,intent(in)                          :: nval
    integer,intent(in)                          :: master,communicator
    type(t_error),intent(out)                   :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_integer8,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_2dvalues_int8

  !! reduce a 2d array of real8
  subroutine reduce_sum_2dvalues_real8(val_in,val_out,nval,master,    &
                                                  communicator,ctx_err)

    real(kind=8),intent(inout),allocatable      :: val_in(:,:)
    real(kind=8),intent(inout),allocatable      :: val_out(:,:)
    integer,intent(in)                          :: nval
    integer,intent(in)                          :: master,communicator
    type(t_error),intent(out)                   :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_real8,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_2dvalues_real8

  !! reduce a 2d array of real4
  subroutine reduce_sum_2dvalues_real4(val_in,val_out,nval,master,    &
                                                  communicator,ctx_err)

    real(kind=4),intent(inout),allocatable      :: val_in(:,:)
    real(kind=4),intent(inout),allocatable      :: val_out(:,:)
    integer,intent(in)                          :: nval
    integer,intent(in)                          :: master,communicator
    type(t_error),intent(out)                   :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_real,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_2dvalues_real4
  
  !! reduce a 2d array of complex16
  subroutine reduce_sum_2dvalues_complex16(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout)       :: val_in(:,:)
    complex(kind=8),intent(inout)       :: val_out(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_complex16,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_2dvalues_complex16

  !! reduce an array of complex8
  subroutine reduce_sum_3dvalues_complex8(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(inout),allocatable   :: val_in(:,:,:)
    complex,intent(inout),allocatable   :: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_complex,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_3dvalues_complex8


  !! reduce a single complex16
  subroutine reduce_sum_value_complex16(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(in)          :: val_in
    complex(kind=8),intent(out)         :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_reduce(val_in,val_out,nval,mpi_complex16,mpi_sum,master,  &
                      communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_value_complex16

  !! reduce an array of complex16
  subroutine reduce_sum_values_complex16(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(in)          :: val_in(:)
    complex(kind=8),intent(out)         :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_complex16,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_values_complex16

  !! reduce an array of complex16
  subroutine reduce_sum_3dvalues_complex16(val_in,val_out,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout),allocatable:: val_in(:,:,:)
    complex(kind=8),intent(inout),allocatable:: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_reduce(val_in,val_out,nval,mpi_complex16,mpi_sum,master,  &
                    communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine reduce_sum_3dvalues_complex16

end module m_reduce_sum
