!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_parallelism.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines of the context for mpi parallelism
!
!------------------------------------------------------------------------------
module m_ctx_parallelism

  use mpi
  
  implicit none
  
  private
  public :: t_parallelism
  

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_parallelism contains info on the mpi parallelism
  !
  !> @param myrank      : rank of the processor,
  !> @param communicator: communicator ID for mpi,
  !> @param nproc       : total number of processors in the cluster,
  !> @param nthread     : number of threads per processor
  !> @param master      : indicates the master processor.
  !----------------------------------------------------------------------------
  type t_parallelism
  
    !> rank of the processor,
    integer  :: myrank
    
    !> communicator ID for mpi,
    integer  :: communicator
    
    !> total number of processors in the cluster,
    integer  :: nproc
    
    !> number of threads per processor
    integer  :: nthread
    
    !> indicates the master processor.
    logical  :: master
    
    !> number of nodes used 
    integer(kind=4) :: n_node
    
    !> indicates the global memory per proc
    integer(kind=8) :: mem_per_node
    integer(kind=8) :: mem_all_node
  
  end type t_parallelism
  
  contains
end module m_ctx_parallelism
