!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_error_terminate.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module ends the program after critical error assuming mpi parallelization
!
!------------------------------------------------------------------------------
module m_error_terminate

  use mpi
  
  implicit none
  private
  public :: critical_error_terminate_program
  
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Kill program after an error by terminating mpi first
  !
  !----------------------------------------------------------------------------
  subroutine critical_error_terminate_program()
    integer :: ierr
     
    call MPI_finalize(ierr)
    stop
    return
  end subroutine critical_error_terminate_program
end module m_error_terminate
