!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_allreduce_min.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to get the min parameter over the processors
!> @details
!> It uses mpi parallelism intrinsic function MPI_ALLREDUCE
!
!------------------------------------------------------------------------------
module m_allreduce_min

  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none

  private

  character(len=*), parameter :: &
      message_error="** ERROR DURING SUBROUTINE MPI_REDUCE (MPI_MIN) **"

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Sum processors parameters
  !
  !> @param[in]  val_in      : input parameter to sum
  !> @param[out] val_out     : output summed parameter
  !> @param[in]  nval        : number of parameters
  !> @param[in]  communicator: communicator ID for barrier
  !> @param[out] ctx_err     : context when raising error
  !----------------------------------------------------------------------------
  interface allreduce_min
     module procedure allreduce_min_value_int4_parallelism
     module procedure allreduce_min_values_int4_parallelism
     module procedure allreduce_min_3dvalues_int4_parallelism
     module procedure allreduce_min_value_int8_parallelism
     module procedure allreduce_min_values_int8_parallelism
     module procedure allreduce_min_3dvalues_int8_parallelism
     module procedure allreduce_min_value_real4_parallelism
     module procedure allreduce_min_values_real4_parallelism
     module procedure allreduce_min_3dvalues_real4_parallelism
     module procedure allreduce_min_value_real8_parallelism
     module procedure allreduce_min_values_real8_parallelism
     module procedure allreduce_min_3dvalues_real8_parallelism
     module procedure allreduce_min_value_complex8_parallelism
     module procedure allreduce_min_values_complex8_parallelism
     module procedure allreduce_min_3dvalues_complex8_parallelism
     module procedure allreduce_min_value_complex16_parallelism
     module procedure allreduce_min_values_complex16_parallelism
     module procedure allreduce_min_3dvalues_complex16_parallelism
  end interface

  public  ::  allreduce_min
contains
  

  !! reduce all a single int4 
  subroutine allreduce_min_value_int4_parallelism(val_in,val_out,nval, &
                                                  communicator,ctx_err)

    integer,intent(in)                  :: val_in
    integer,intent(out)                 :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_allreduce(val_in,val_out,nval,mpi_integer,mpi_min, &
                         communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_value_int4_parallelism

  !! reduce all an array of int4 
  subroutine allreduce_min_values_int4_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    integer,intent(in)                  :: val_in(:)
    integer,intent(out)                 :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_integer,mpi_min, &
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_values_int4_parallelism

  !! reduce all a 3d array of int4 
  subroutine allreduce_min_3dvalues_int4_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_in(:,:,:)
    integer,intent(inout),allocatable   :: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_integer,mpi_min,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_3dvalues_int4_parallelism
  

  !! reduce all a single int8 
  subroutine allreduce_min_value_int8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    integer(kind=8),intent(in)          :: val_in
    integer(kind=8),intent(out)         :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_allreduce(val_in,val_out,nval,mpi_integer8,mpi_min, &
                         communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_value_int8_parallelism

  !! reduce all an array of int8 
  subroutine allreduce_min_values_int8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    integer(kind=8),intent(in)          :: val_in(:)
    integer(kind=8),intent(in)          :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_integer8,mpi_min,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_values_int8_parallelism

  !! reduce all a 3d array of int8 
  subroutine allreduce_min_3dvalues_int8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    integer(kind=8),intent(inout),allocatable:: val_in(:,:,:)
    integer(kind=8),intent(inout),allocatable:: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_integer8,mpi_min,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_3dvalues_int8_parallelism
  

  !! reduce all a single real4 
  subroutine allreduce_min_value_real4_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    real,intent(in)                     :: val_in
    real,intent(out)                    :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_allreduce(val_in,val_out,nval,mpi_real,mpi_min, &
                         communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_value_real4_parallelism

  !! reduce all an array of real4 
  subroutine allreduce_min_values_real4_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    real,intent(in)                     :: val_in(:)
    real,intent(out)                    :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_real,mpi_min, &
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_values_real4_parallelism

  !! reduce all a 3d array of real4 
  subroutine allreduce_min_3dvalues_real4_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    real,intent(inout),allocatable      :: val_in(:,:,:)
    real,intent(inout),allocatable      :: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_real,mpi_min, &
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_3dvalues_real4_parallelism
  

  !! reduce all a single real8 
  subroutine allreduce_min_value_real8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    real(kind=8),intent(in)             :: val_in
    real(kind=8),intent(out)            :: val_out
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else    
      call mpi_allreduce(val_in,val_out,nval,mpi_real8,mpi_min, &
                         communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_value_real8_parallelism

  !! reduce all an array of real8 
  subroutine allreduce_min_values_real8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    real(kind=8),intent(in)             :: val_in(:)
    real(kind=8),intent(out)            :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err


    call mpi_allreduce(val_in,val_out,nval,mpi_real8,mpi_min, & 
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_values_real8_parallelism

  !! reduce all a 3d array of real8 
  subroutine allreduce_min_3dvalues_real8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    real(kind=8),intent(inout),allocatable:: val_in(:,:,:)
    real(kind=8),intent(inout),allocatable:: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_real8,mpi_min, &
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_3dvalues_real8_parallelism
  

  !! reduce all a single complex8 
  subroutine allreduce_min_value_complex8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    complex,intent(in)                  :: val_in
    complex,intent(out)                 :: val_out
    integer,intent(in)                  :: communicator
    integer,intent(in)                  :: nval
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else        
      call mpi_allreduce(val_in,val_out,nval,mpi_complex,mpi_min,       &
                         communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_value_complex8_parallelism

  !! reduce all an array of complex8 
  subroutine allreduce_min_values_complex8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    complex,intent(in)                  :: val_in(:)
    complex,intent(out)                 :: val_out(:)
    integer,intent(in)                  :: communicator
    integer,intent(in)                  :: nval
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_complex,mpi_min, &
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_values_complex8_parallelism

  !! reduce all a 3d array of complex8 
  subroutine allreduce_min_3dvalues_complex8_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    complex,intent(inout),allocatable   :: val_in(:,:,:)
    complex,intent(inout),allocatable   :: val_out(:,:,:)
    integer,intent(in)                  :: communicator
    integer,intent(in)                  :: nval
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_complex,mpi_min,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_3dvalues_complex8_parallelism
  

  !! reduce all a single complex16 
  subroutine allreduce_min_value_complex16_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    complex(kind=8),intent(in)          :: val_in
    complex(kind=8),intent(out)         :: val_out
    integer,intent(in)                  :: communicator
    integer,intent(in)                  :: nval
    type(t_error),intent(out)           :: ctx_err

    if(nval.ne.1) then
      ctx_err%ierr=1
    else
      call mpi_allreduce(val_in,val_out,nval,mpi_complex16,mpi_min, & 
                         communicator,ctx_err%ierr)
    endif
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_value_complex16_parallelism

  !! reduce all an array of complex16 
  subroutine allreduce_min_values_complex16_parallelism(val_in,val_out,nval,    &
                                                  communicator,ctx_err)

    complex(kind=8),intent(in)          :: val_in(:)
    complex(kind=8),intent(out)         :: val_out(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_complex16,mpi_min, &
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_values_complex16_parallelism 
  

  !! reduce all a 3d array of complex16 
  subroutine allreduce_min_3dvalues_complex16_parallelism(val_in,val_out,nval,&
                                                          communicator,ctx_err)

    complex(kind=8),intent(inout),allocatable:: val_in(:,:,:)
    complex(kind=8),intent(inout),allocatable:: val_out(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_allreduce(val_in,val_out,nval,mpi_complex16,mpi_min,&
                       communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg =message_error
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine allreduce_min_3dvalues_complex16_parallelism 
  
end module m_allreduce_min
