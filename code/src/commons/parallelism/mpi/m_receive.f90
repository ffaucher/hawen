!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_receive.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to receive stuff
!> @details
!> It uses mpi parallelism intrinsic function
!
!------------------------------------------------------------------------------
module m_receive

  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none

  private 
  private :: mpi_error_handler

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Receive parameter
  !
  !> @param[in]  val         : parameter to receive
  !> @param[in]  nval        : number of parameters
  !> @param[in]  from        : where to receive from
  !> @param[in]  tag         : tag
  !> @param[in]  communicator: communicator ID for barrier
  !> @param[out] ctx_err     : context when raising error
  !----------------------------------------------------------------------------
  interface receive
     module procedure receive_value_char_parallelism
     module procedure receive_values_char_parallelism
     module procedure receive_value_int4_parallelism
     module procedure receive_values_int4_parallelism
     module procedure receive_4dvalues_int4_parallelism
     module procedure receive_3dvalues_int4_parallelism
     module procedure receive_2dvalues_int4_parallelism
     module procedure receive_2dvalues_int8_parallelism
     module procedure receive_value_int8_parallelism
     module procedure receive_values_int8_parallelism
     module procedure receive_3dvalues_int8_parallelism
     module procedure receive_value_real4_parallelism
     module procedure receive_values_real4_parallelism
     module procedure receive_3dvalues_real4_parallelism
     module procedure receive_value_real8_parallelism
     module procedure receive_values_real8_parallelism
     module procedure receive_3dvalues_real8_parallelism
     module procedure receive_value_complex8_parallelism
     module procedure receive_values_complex8_parallelism
     module procedure receive_3dvalues_complex8_parallelism
     module procedure receive_value_complex16_parallelism
     module procedure receive_values_complex16_parallelism
     module procedure receive_3dvalues_complex16_parallelism
     module procedure receive_2dvalues_complex8_parallelism
     module procedure receive_4dvalues_real4_parallelism
     module procedure receive_2dvalues_real4_parallelism
     module procedure receive_2dvalues_real8_parallelism
  end interface

  public  ::  receive
contains

  !! Send a single char
  subroutine receive_value_char_parallelism(val_char,nval,from,tag, &
                                              communicator,ctx_err)

    character(len=*),intent(inout)      :: val_char
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_char,nval*len(val_char),mpi_character,from,tag,   &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_value_char_parallelism

  !! Send an array of char
  subroutine receive_values_char_parallelism(val_char,nval,from,tag, &
                                              communicator,status,ctx_err)

    character(len=*),intent(inout)      :: val_char(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_char,nval*len(val_char),mpi_character,from,tag, &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_values_char_parallelism

  !! Send a single int4
  subroutine receive_value_int4_parallelism(val_int,nval,from,tag, &
                                              communicator,ctx_err)

    integer,intent(inout)               :: val_int
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_value_int4_parallelism

  !! Send an array of int4
  subroutine receive_values_int4_parallelism(val_int,nval,from,tag, &
                                              communicator,ctx_err)

    integer,intent(inout)               :: val_int(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_values_int4_parallelism

  !! Send a 3d array of int4
  subroutine receive_3dvalues_int4_parallelism(val_int,nval,from,tag, &
                                              communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_3dvalues_int4_parallelism

  !! Send a 2d array of int4
  subroutine receive_2dvalues_int4_parallelism(val_int,nval,from,tag, &
                                               communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_2dvalues_int4_parallelism
  !! Send a 2d array of int8
  subroutine receive_2dvalues_int8_parallelism(val_int,nval,from,tag, &
                                               communicator,ctx_err)

    integer(kind=8),intent(inout),allocatable   :: val_int(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_2dvalues_int8_parallelism

  !! Send a 4d array of int4
  subroutine receive_4dvalues_int4_parallelism(val_int,nval,from,tag, &
                                              communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err
    call mpi_recv(val_int,nval,mpi_integer,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_4dvalues_int4_parallelism

  !! Send a single int8
  subroutine receive_value_int8_parallelism(val_int,nval,from,tag, &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout)       :: val_int
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_value_int8_parallelism

  !! Send an array of int8
  subroutine receive_values_int8_parallelism(val_int,nval,from,tag, &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout)       :: val_int(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_values_int8_parallelism

  !! Send a 3d array of int8
  subroutine receive_3dvalues_int8_parallelism(val_int,nval,from,tag, &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout),allocatable:: val_int(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_int,nval,mpi_integer8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_3dvalues_int8_parallelism

  !! Send a single real4
  subroutine receive_value_real4_parallelism(val_re,nval,from,tag, &
                                              communicator,ctx_err)

    real,intent(inout)                  :: val_re
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_re,nval,mpi_real,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_value_real4_parallelism

  !! Send an array of real4
  subroutine receive_values_real4_parallelism(val_re,nval,from,tag, &
                                              communicator,ctx_err)

    real,intent(inout)                  :: val_re(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_re,nval,mpi_real,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_values_real4_parallelism

  !! Send a 3d array of real4
  subroutine receive_3dvalues_real4_parallelism(val_re,nval,from,tag, &
                                              communicator,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_re,nval,mpi_real,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_3dvalues_real4_parallelism

  !! Send a 4d array of real4
  subroutine receive_4dvalues_real4_parallelism(val_re,nval,from,tag, &
                                              communicator,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_re,nval,mpi_real,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_4dvalues_real4_parallelism

  !! Send a single real8
  subroutine receive_value_real8_parallelism(val_re,nval,from,tag, &
                                              communicator,ctx_err)

    real(kind=8),intent(inout)          :: val_re
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_re,nval,mpi_real8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_value_real8_parallelism

  !! Send an array of real8
  subroutine receive_values_real8_parallelism(val_re,nval,from,tag, &
                                              communicator,ctx_err)

    real(kind=8),intent(inout)          :: val_re(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_re,nval,mpi_real8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_values_real8_parallelism

  !! Send a 3d array of real8
  subroutine receive_3dvalues_real8_parallelism(val_re,nval,from,tag, &
                                              communicator,ctx_err)

    real(kind=8),intent(inout),allocatable:: val_re(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_re,nval,mpi_real8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_3dvalues_real8_parallelism

  !! Send a single complex8
  subroutine receive_value_complex8_parallelism(val_cx,nval,from,tag, &
                                              communicator,ctx_err)

    complex,intent(inout)               :: val_cx
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_cx,nval,mpi_complex,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_value_complex8_parallelism

  !! Send an array of complex8
  subroutine receive_values_complex8_parallelism(val_cx,nval,from,tag, &
                                              communicator,ctx_err)

    complex,intent(inout)               :: val_cx(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_cx,nval,mpi_complex,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_values_complex8_parallelism

  !! Send a 2d array of complex8
  subroutine receive_2dvalues_complex8_parallelism(val_cx,nval,from,tag, &
                                              communicator,ctx_err)

    complex,intent(inout),allocatable   :: val_cx(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_cx,nval,mpi_complex,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_2dvalues_complex8_parallelism

  !! Send a 2d array of real8
  subroutine receive_2dvalues_real8_parallelism(val_r,nval,from,tag, &
                                              communicator,ctx_err)

    real(kind=8),intent(inout)               :: val_r(:,:)
    integer,intent(in)                       :: nval
    integer,intent(in)                       :: from,tag,communicator
    integer                                  :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)                :: ctx_err

    call mpi_recv(val_r,nval,mpi_real8,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_2dvalues_real8_parallelism

  !! Send a 2d array of real4
  subroutine receive_2dvalues_real4_parallelism(val_r,nval,from,tag, &
                                              communicator,ctx_err)

    real(kind=4),intent(inout)               :: val_r(:,:)
    integer,intent(in)                       :: nval
    integer,intent(in)                       :: from,tag,communicator
    integer                                  :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)                :: ctx_err

    call mpi_recv(val_r,nval,mpi_real4,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_2dvalues_real4_parallelism

  !! Send a 3d array of complex8
  subroutine receive_3dvalues_complex8_parallelism(val_cx,nval,from,tag, &
                                              communicator,ctx_err)

    complex,intent(inout),allocatable   :: val_cx(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_cx,nval,mpi_complex,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_3dvalues_complex8_parallelism

  !! Send a single complex16
  subroutine receive_value_complex16_parallelism(val_cx,nval,from,tag, &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout)       :: val_cx
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_cx,nval,mpi_complex16,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_value_complex16_parallelism

  !! Send an array of complex16
  subroutine receive_values_complex16_parallelism(val_cx,nval,from,tag, &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout)       :: val_cx(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_cx,nval,mpi_complex16,from,tag,     &
                  communicator,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_values_complex16_parallelism

  !! Send an array of complex16
  subroutine receive_3dvalues_complex16_parallelism(val_cx,nval,from,tag, &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout),allocatable:: val_cx(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: from,tag,communicator
    integer                             :: status(MPI_STATUS_SIZE)
    type(t_error),intent(out)           :: ctx_err

    call mpi_recv(val_cx,nval,mpi_complex16,from,tag,     &
                  communicator,status,ctx_err%ierr)
                  
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine receive_3dvalues_complex16_parallelism

  !! ===================================================================
  !! error handler
  !! ===================================================================
  subroutine mpi_error_handler(ctx_err)

    type(t_error),intent(inout)           :: ctx_err
    
    select case(ctx_err%ierr)
      case(MPI_SUCCESS)
        !! No error; MPI routine completed successfully.
      case(MPI_ERR_COMM)
        !! Invalid communicator. A common error is to use a null communicator 
        !! in a call (not even allowed in MPI_Comm_rank).
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_RECV: " // &
                     " INVALID COMMUNICATOR ** "
        ctx_err%critic=.true.
      case(MPI_ERR_TYPE)
        !! Invalid datatype argument. Additionally, this error can occur 
        !! if an uncommitted MPI_Datatype (see MPI_Type_commit) is used in 
        !! a communication call.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_RECV: " // &
                     " INVALID MPI_DATATYPE ** "
        ctx_err%critic=.true.
      case(MPI_ERR_COUNT)
        !! Invalid count argument. Count arguments must be non-negative; 
        !! a count of zero is often valid.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_RECV: " // &
                     " INVALID COUNT ** "
        ctx_err%critic=.true.
      case(MPI_ERR_TAG)
        !! Invalid tag argument. Tags must be non-negative; tags in a receive 
        !! (MPI_Recv, MPI_Irecv, MPI_Sendrecv, etc.) may also be MPI_ANY_TAG. 
        !! The largest tag value is available through the the attribute MPI_TAG_UB.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_RECV: " // &
                     " INVALID RECV_TAG ** "
        ctx_err%critic=.true.
      case(MPI_ERR_RANK)
        !! Invalid source or destination rank. Ranks must be between zero and 
        !! the size of the communicator minus one; ranks in a receive 
        !! (MPI_Recv, MPI_Irecv, MPI_Sendrecv, etc.) may also be MPI_ANY_SOURCE.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_RECV: " // &
                     " INVALID RECV SOURCE RANK ** "
        ctx_err%critic=.true.
      case default
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_RECV: " // &
                     " UNKNOWN ERROR ** "
        ctx_err%critic=.true.
    end select
    return
  end subroutine mpi_error_handler

end module m_receive
