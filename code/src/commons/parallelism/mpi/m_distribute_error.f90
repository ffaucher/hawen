!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_distribute_error.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to share error among processors
!> @details
!> It uses mpi parallelism
!
!------------------------------------------------------------------------------
module m_distribute_error

  use mpi
  use m_raise_error,    only : raise_error, t_error
  use m_allreduce_min,  only : allreduce_min
  implicit none

  private

  public  ::  distribute_error

contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> distribute error code
  !> @details
  !> In case one processor has failed, the error is distributed among everyone
  !
  !> @param[in]  ctx_err      : error context
  !> @param[in]  communicator : mpi communicator 
  !----------------------------------------------------------------------------
  subroutine distribute_error(ctx_err,communicator)

    integer,intent(in)                  :: communicator
    type(t_error),intent(inout)         :: ctx_err
    !! local
    integer                             :: i_err_code, i_err_loc
    
    !! barrier to wait for everyone
    call mpi_barrier(communicator,i_err_code)
    !! send global error code
    i_err_loc = -max(abs(i_err_code),abs(ctx_err%ierr))
    call allreduce_min(i_err_loc,i_err_code,1,communicator,ctx_err)

    !! global shared code
    if (i_err_code .ne. 0) then
      if(i_err_loc == 0) then !! no error originally 
        ctx_err%ierr = i_err_code
        ctx_err%msg = "** (error on another processor) **"
      
      else 
        ctx_err%ierr = i_err_loc
        ctx_err%msg = trim(adjustl(ctx_err%msg)) //                     &
                      " ** (error on this processor) **"
      end if

      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine distribute_error

end module m_distribute_error
