!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_time.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module is used to obtain the time 
!> @details
!> It uses mpi parallelism intrinsic function
!
!------------------------------------------------------------------------------
module m_time


  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none
   
  private
  interface time
     module procedure get_time
  end interface
  public  ::  time
contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Obtain time from mpi intrinsic function
  !
  !> @param[out] t: time
  !----------------------------------------------------------------------------
  subroutine get_time(t)
 
    real(kind=8),intent(out)            :: t
    t=MPI_Wtime()
    return
  end subroutine get_time
end module m_time
