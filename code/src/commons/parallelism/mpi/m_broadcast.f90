!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_broadcast.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to broadcast parameter
!> @details
!> It uses mpi parallelism intrinsic function
!
!------------------------------------------------------------------------------
module m_broadcast

  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none

  private

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Broadcast parameter
  !
  !> @param[in]  val         : parameter to broadcast
  !> @param[in]  nval        : number of parameters
  !> @param[in]  master      : what processor has the parameter
  !> @param[in]  communicator: communicator ID for barrier
  !> @param[out] ctx_err     : context when raising error
  !----------------------------------------------------------------------------
  interface broadcast
     module procedure broadcast_value_char_parallelism
     module procedure broadcast_values_char_parallelism
     module procedure broadcast_value_int4_parallelism
     module procedure broadcast_values_int4_parallelism
     module procedure broadcast_4dvalues_int4_parallelism
     module procedure broadcast_3dvalues_int4_parallelism
     module procedure broadcast_value_int8_parallelism
     module procedure broadcast_values_int8_parallelism
     module procedure broadcast_3dvalues_int8_parallelism
     module procedure broadcast_value_real4_parallelism
     module procedure broadcast_values_real4_parallelism
     module procedure broadcast_3dvalues_real4_parallelism
     module procedure broadcast_2dvalues_real4_parallelism
     module procedure broadcast_value_real8_parallelism
     module procedure broadcast_values_real8_parallelism
     module procedure broadcast_3dvalues_real8_parallelism
     module procedure broadcast_2dvalues_real8_parallelism
     module procedure broadcast_value_complex8_parallelism
     module procedure broadcast_values_complex8_parallelism
     module procedure broadcast_3dvalues_complex8_parallelism
     module procedure broadcast_value_complex16_parallelism
     module procedure broadcast_values_complex16_parallelism
     module procedure broadcast_3dvalues_complex16_parallelism
     module procedure broadcast_2dvalues_complex8_parallelism
     module procedure broadcast_2dvalues_complex4_parallelism
     module procedure broadcast_2dvalues_int8_parallelism
     module procedure broadcast_2dvalues_int4_parallelism
     module procedure broadcast_4dvalues_real4_parallelism
     !! quadruple
     module procedure broadcast_values_real16_parallelism
  end interface

  public  ::  broadcast
contains

  !! Broadcast a single char
  subroutine broadcast_value_char_parallelism(val_char,nval,master,    &
                                              communicator,ctx_err)

    character(len=*),intent(inout)      :: val_char
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_char,nval*len(val_char),mpi_character,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_value_char_parallelism

  !! Broadcast an array of char
  subroutine broadcast_values_char_parallelism(val_char,nval,master,    &
                                              communicator,ctx_err)

    character(len=*),intent(inout)      :: val_char(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_char,nval*len(val_char),mpi_character,master,    &
                   communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_char_parallelism

  !! Broadcast a single int4
  subroutine broadcast_value_int4_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(inout)               :: val_int
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_value_int4_parallelism

  !! Broadcast an array of int4
  subroutine broadcast_values_int4_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(inout)               :: val_int(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_int4_parallelism

  !! Broadcast a 3d array of int4
  subroutine broadcast_3dvalues_int4_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_3dvalues_int4_parallelism

  !! Broadcast a 4d array of int4
  subroutine broadcast_4dvalues_int4_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err
    call mpi_bcast(val_int,nval,mpi_integer,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_4dvalues_int4_parallelism

  !! Broadcast a single int8
  subroutine broadcast_value_int8_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout)       :: val_int
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_value_int8_parallelism

  !! Broadcast an array of int8
  subroutine broadcast_values_int8_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout)       :: val_int(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_int8_parallelism

  !! Broadcast a 3d array of int8
  subroutine broadcast_3dvalues_int8_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout),allocatable:: val_int(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_3dvalues_int8_parallelism

  !! Broadcast a single real4
  subroutine broadcast_value_real4_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real,intent(inout)                  :: val_re
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_value_real4_parallelism

  !! Broadcast an array of real4
  subroutine broadcast_values_real4_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real,intent(inout)                  :: val_re(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_real4_parallelism

  !! Broadcast a 3d array of real4
  subroutine broadcast_3dvalues_real4_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_3dvalues_real4_parallelism

  !! Broadcast a 2d array of real4
  subroutine broadcast_2dvalues_real4_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_2dvalues_real4_parallelism

  !! Broadcast a 2d array of int4
  subroutine broadcast_2dvalues_int4_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer,intent(inout),allocatable   :: val_int(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_2dvalues_int4_parallelism

  !! Broadcast a 2d array of int4
  subroutine broadcast_2dvalues_int8_parallelism(val_int,nval,master,    &
                                              communicator,ctx_err)

    integer(kind=8),intent(inout),allocatable   :: val_int(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_int,nval,mpi_integer8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_2dvalues_int8_parallelism
  
  !! Broadcast a 4d array of real4
  subroutine broadcast_4dvalues_real4_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real,intent(inout),allocatable      :: val_re(:,:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_4dvalues_real4_parallelism

  !! Broadcast a single real8
  subroutine broadcast_value_real8_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real(kind=8),intent(inout)          :: val_re
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_value_real8_parallelism

  !! Broadcast an array of real8
  subroutine broadcast_values_real8_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real(kind=8),intent(inout)          :: val_re(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_real8_parallelism

  !! Broadcast an array of real16 
  subroutine broadcast_values_real16_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real(kind=16),intent(inout)         :: val_re(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    !! broadcast double precision
    call mpi_bcast(val_re,nval,mpi_2double_precision,master,            &
                   communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_real16_parallelism

  !! Broadcast a 3d array of real8
  subroutine broadcast_3dvalues_real8_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real(kind=8),intent(inout),allocatable:: val_re(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_3dvalues_real8_parallelism

  !! Broadcast a 2d array of real8
  subroutine broadcast_2dvalues_real8_parallelism(val_re,nval,master,    &
                                              communicator,ctx_err)

    real(kind=8),intent(inout),allocatable:: val_re(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_re,nval,mpi_real8,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_2dvalues_real8_parallelism

  !! Broadcast a single complex8
  subroutine broadcast_value_complex8_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(inout)               :: val_cx
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_value_complex8_parallelism

  !! Broadcast an array of complex8
  subroutine broadcast_values_complex8_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(inout)               :: val_cx(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_complex8_parallelism

  !! Broadcast a 2d array of complex8
  subroutine broadcast_2dvalues_complex8_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout),allocatable   :: val_cx(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex16,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_2dvalues_complex8_parallelism

  !! Broadcast a 2d array of complex4
  subroutine broadcast_2dvalues_complex4_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(inout),allocatable   :: val_cx(:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_2dvalues_complex4_parallelism

  !! Broadcast a 3d array of complex8
  subroutine broadcast_3dvalues_complex8_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex,intent(inout),allocatable   :: val_cx(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_3dvalues_complex8_parallelism

  !! Broadcast a single complex16
  subroutine broadcast_value_complex16_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout)       :: val_cx
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex16,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_value_complex16_parallelism

  !! Broadcast an array of complex16
  subroutine broadcast_values_complex16_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout)       :: val_cx(:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex16,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_values_complex16_parallelism

  !! Broadcast an array of complex16
  subroutine broadcast_3dvalues_complex16_parallelism(val_cx,nval,master,    &
                                              communicator,ctx_err)

    complex(kind=8),intent(inout),allocatable:: val_cx(:,:,:)
    integer,intent(in)                  :: nval
    integer,intent(in)                  :: master,communicator
    type(t_error),intent(out)           :: ctx_err

    call mpi_bcast(val_cx,nval,mpi_complex16,master,communicator,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_BCAST **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    return
  end subroutine broadcast_3dvalues_complex16_parallelism

end module m_broadcast
