!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_parallelism_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module initializes and terminates mpi parallelism
!
!------------------------------------------------------------------------------
module m_parallelism_init

  use mpi
  use omp_lib

  use m_ctx_parallelism, only: t_parallelism
  use m_raise_error,     only: t_error, raise_error
  implicit none
  private
  public :: init_parallelism, end_parallelism
  public :: init_parallelism_selfproc
  
  contains
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Initialize mpi parallelism
  !
  !> @param[out] paral  : context for parallelism
  !> @param[out] ctx_err: context to raise error if needed
  !
  !----------------------------------------------------------------------------
  subroutine init_parallelism(paral,ctx_err)
    
    implicit none

    type(t_parallelism),intent(out) :: paral
    type(t_error)      ,intent(out) :: ctx_err
    !! local
    integer                     :: nprocs,myrank,code_mpi_omp
    !! to read available memory info
    character(len=13),parameter :: filemem = '/proc/meminfo'
    logical                     :: file_exists
    integer                     :: k,istart,io
    logical                     :: end_file
    character(len=2048)         :: current_line
    integer                     :: lin_len

    !!  Init open MP number of threads
    !$OMP PARALLEL DEFAULT(shared)
    paral%nthread     =omp_get_num_threads()
    !$OMP END PARALLEL

    !! Initialize the general parallel process: mpi + OpenMP
    !! ierr is raised if there is an error
    !! call MPI_INIT(ctx_err%ierr)
    call MPI_INIT_THREAD(MPI_THREAD_SERIALIZED,code_mpi_omp,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_INIT IN [m_mpi_init] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    if(MPI_THREAD_SERIALIZED .ne. code_mpi_omp) then
       ctx_err%ierr=-1
       ctx_err%msg ="** WARNING: MPI_THREAD_SERIALIZED not supported "
       ctx_err%critic=.false.
       call raise_error(ctx_err)
       ctx_err%ierr=0
       ctx_err%msg =""
    endif 
    !! Get what is the total number of working processors: nprocs
    !! as well as the gloal communicator between processors: this 
    !! is the global variable MPI_COMM_WORLD
    call MPI_COMM_SIZE(MPI_COMM_WORLD,nprocs,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_COMM_SIZE "//&
                   "IN M_MPI_INIT.F90 **"
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif

    !! For each processors, get the local rank of the current one
    call MPI_COMM_RANK(MPI_COMM_WORLD,myrank,ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_COMM_RANK "//&
                   "IN M_MPI_INIT.F90 **"
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif
         
    !! save infos in type
    paral%myrank      =myrank
    paral%communicator=MPI_COMM_WORLD
    paral%nproc       =nprocs

    if(myrank.eq.0) paral%master=.true.
    if(myrank.ne.0) paral%master=.false.
    
    !! count number of node on mpi archi.
    call count_mpi_node(paral%n_node,ctx_err)
    !! -----------------------------------------------------------------
    !! we read the total allowed memory that can be used 
    !! from file /proc/meminfo.
    !! a) does the file exists on this architecture.
    paral%mem_per_node = 0
    paral%mem_all_node = 0
    inquire(file=trim(adjustl(filemem)),exist=file_exists)
    
    !! b) if it exists we open and read for the MemTotal line
    if(file_exists) then
      
      open (unit=10,file=trim(adjustl(filemem)))
      end_file=.false.
      !! read
      do while(.not. end_file)
        read(10,"(a)",iostat=io)  current_line
        if(is_iostat_end(io)) then
          end_file=.true.
        else
          !! check if it is the Memory global
          if(current_line(1:9) .eq. "MemTotal:") then
            
            !! get line length
            lin_len=len_trim(adjustl(current_line))
            !! get starting position 
            istart=10
            do k=istart,lin_len
              !! while a space, up until a letter
              if(current_line(k:k) .eq. " ")  then
                istart = istart + 1
              else
                exit
              end if
            end do
            !! it ends with space and kB
            read(current_line(istart:lin_len-3),*) paral%mem_per_node
            exit
          end if
        end if
      end do
    end if ! end if file exists
    !! the memory is given in kB
    paral%mem_per_node = paral%mem_per_node * 1000
    paral%mem_all_node = paral%mem_per_node * paral%n_node
    !! -----------------------------------------------------------------
    
    return
  end subroutine init_parallelism


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> this subroutine counts the number of NODE in the current
  !> architecture. It can be useful to anticipate memory available
  !
  !> @param[out] n_node  : number of node on the current machine.
  !> @param[out] ctx_err : context to raise error if needed
  !
  !----------------------------------------------------------------------------
  subroutine count_mpi_node(n_node,ctx_err)

    implicit none

    integer            ,intent(out) :: n_node
    type(t_error)      ,intent(out) :: ctx_err
    !! local
    integer              :: shmcomm, rank, is_rank0
  
    call MPI_COMM_SPLIT_TYPE(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, 0, &
                             MPI_INFO_NULL, shmcomm, ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING MPI_COMM_SPLIT_TYPE "
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif
    
    call MPI_COMM_RANK(shmcomm, rank, ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING MPI_COMM_RANK"
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif

    if (rank == 0) then
       is_rank0 = 1
    else
       is_rank0 = 0
    end if
    call MPI_ALLREDUCE(is_rank0, n_node, 1, MPI_INTEGER, MPI_SUM, &
                       MPI_COMM_WORLD, ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING MPI_ALLREDUCE"
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif

    call MPI_COMM_FREE(shmcomm, ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING MPI_COMM_FREE"
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif

    return
  end subroutine count_mpi_node


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Initialize a sub-mpi parallelism on one proc only
  !> need to use mpi_comm_split if sub-group of processors.
  !
  !> @param[out] paral  : context for parallelism
  !> @param[out] ctx_err: context to raise error if needed
  !----------------------------------------------------------------------------
  subroutine init_parallelism_selfproc(paral,ctx_err)
    type(t_parallelism),intent(out)   :: paral
    type(t_error)      ,intent(inout) :: ctx_err

    if(ctx_err%ierr .ne. 0) return

    !!  Init open MP number of threads
    !$OMP PARALLEL DEFAULT(shared)
    paral%nthread     =omp_get_num_threads()
    !$OMP END PARALLEL
    
    !! the only difference is that we take communicator self.
    !! save infos in type
    paral%myrank      =0
    paral%communicator=MPI_COMM_SELF
    paral%nproc       =1
    paral%master      =.true.

    return
  end subroutine init_parallelism_selfproc



  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> End mpi parallelism
  !
  !> @param[out] paral  : context for parallelism
  !> @param[out] ctx_err: context to raise error if needed
  !----------------------------------------------------------------------------
  subroutine end_parallelism(paral,ctx_err)

    implicit none

    type(t_parallelism),intent(inout) :: paral
    type(t_error)      ,intent(out) :: ctx_err

    call MPI_finalize(ctx_err%ierr)
    if (ctx_err%ierr.ne.0) then
      ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_FINALIZE "//&
                   "IN M_MPI_INIT.F90 **"
      ctx_err%critic=.true.      
      call raise_error(ctx_err)
    endif
    !! reset parallelism
    paral%myrank      =0
    paral%communicator=0
    paral%nproc       =0
    paral%nthread     =0
    paral%master      =.false.
    return
  end subroutine end_parallelism
  
end module m_parallelism_init
