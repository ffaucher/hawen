!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_wait.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to wait
!> @details
!> It uses mpi parallelism intrinsic function
!
!------------------------------------------------------------------------------
module m_wait

  use mpi
  use m_raise_error, only : raise_error, t_error
  implicit none

  private
  private :: mpi_error_handler

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Wait
  !
  !> @param[in]  request     : request to wait for
  !> @param[out] ctx_err     : context when raising error
  !----------------------------------------------------------------------------
  public  ::  wait
contains

  subroutine wait(request,ctx_err)

    integer,intent(inout)               :: request
    type(t_error),intent(out)           :: ctx_err
    integer                             :: status(MPI_STATUS_SIZE)

    call mpi_wait(request,status,ctx_err%ierr)
    call mpi_error_handler(ctx_err)
    if (ctx_err%ierr.ne.0) call raise_error(ctx_err)
    return
  end subroutine wait


  !! ===================================================================
  !! error handler
  !! ===================================================================
  subroutine mpi_error_handler(ctx_err)

    type(t_error),intent(inout)           :: ctx_err
    
    select case(ctx_err%ierr)
      case(MPI_SUCCESS)
        !! No error; MPI routine completed successfully.
      case(MPI_ERR_REQUEST)
        !! Invalid MPI_Request. Either null or, in the case of a MPI_Start 
        !! or MPI_Startall, not a persistent request.
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_WAIT: " // &
                     " INVALID REQUEST ** "
        ctx_err%critic=.true.
      case(MPI_ERR_ARG)
        !! Invalid argument. Some argument is invalid and is not identified by 
        !! a specific error class (e.g., MPI_ERR_RANK).
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_WAIT: " // &
                     " INVALID ARGUMENT ** "
        ctx_err%critic=.true.
      case default
        ctx_err%msg ="** ERROR DURING SUBROUTINE MPI_WAIT: " // &
                     " UNKNOWN ERROR ** "
        ctx_err%critic=.true.
    end select
    return
  end subroutine mpi_error_handler

end module m_wait
