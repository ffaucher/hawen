add_library(ctx_equation ${PROJECT_SOURCE_DIR}/code/src/discretization-operator/api_equation/m_ctx_equation.f90)
install(TARGETS ctx_equation
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
set(discretization_operator_sources
${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/model/m_model_list.f90
${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/model/m_ctx_model.f90
${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/model/m_model_eval.f90
${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/shared/m_equation_infos.f90
${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/shared/m_wavelength_utils.f90
)

if (${DISCRETIZATION} STREQUAL hdg)
  set(discretization_operator_sources 
     ${discretization_operator_sources}
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/shared/m_matrix_utils.f90
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/shared/m_create_matrix_refelem.f90
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/shared/m_create_matrix_quadrature.f90
   )
endif() 
set(discretization_operator_sources ${discretization_operator_sources} 
${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/shared/m_create_matrix.f90
)
add_library(discretization_operator ${discretization_operator_sources})
target_link_libraries(discretization_operator model_representation special_functions hdg_utils ctx_equation raise_error)
install(TARGETS discretization_operator 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)

if(${PROBLEM} STREQUAL "inversion")
  set(gradient_sources
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/utils_inversion/m_parameter_gradient.f90
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/utils_inversion/m_grad_metric.f90
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/${PROBLEM}/m_reciprocity_formula.f90)
  if (${DISCRETIZATION} STREQUAL "hdg")
    set(gradient_sources ${gradient_sources}
    ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/${PROBLEM}/m_matrix_utils_derivative.f90
    )
  endif()
  set(gradient_sources ${gradient_sources}
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/${PROBLEM}/m_create_gradient_refelem.f90
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/${PROBLEM}/m_create_gradient_quadrature.f90
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/${PROBLEM}/m_create_gradient.f90
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROPAGATOR}/${DISCRETIZATION}/${PROBLEM}/m_create_pseudoh.f90
  ) 
add_library(gradient ${gradient_sources})
target_link_libraries(gradient discretization_operator parameter_file ctx_discretization raise_error)
install(TARGETS gradient 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
endif()

