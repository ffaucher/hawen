!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_eval.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to evaluate the model coefficients, in particular
!> to deal with piecewise-polynomials representation and with attenuation
!>
!
!------------------------------------------------------------------------------
module m_model_eval

  !! module used -------------------------------------------------------
  use m_raise_error,               only: raise_error, t_error
  use m_define_precision,          only: IKIND_MESH, RKIND_POL
  use m_ctx_domain,                only: t_domain
  use m_ctx_model,                 only: t_model
  use m_ctx_model_representation,  only: model_representation_eval_sep
  use m_model_representation_dof,  only: model_representation_dof_eval_refelem
  use m_model_list
  use m_polynomial,                only: polynomial_eval, polynomial_eval_array
  use m_dg_lagrange_dof_coordinates, only: discretization_dof_coo_ref_elem, &
                                           discretization_dof_coo_loc_elem
  !! -------------------------------------------------------------------
  implicit none

  private

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_piecewise_constant
     module procedure model_eval_piecewise_constant_raw
     module procedure model_eval_piecewise_constant_all
  end interface
  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_piecewise_polynomial
     module procedure model_eval_piecewise_polynomial_raw
     module procedure model_eval_piecewise_polynomial_all
  end interface
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise constant representation
  interface model_get_piecewise_constant
     module procedure model_get_pconstant_real4
     module procedure model_get_pconstant_real8
  end interface
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_sep
     module procedure model_eval_sep_raw
     module procedure model_eval_sep_all
  end interface
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation for dof representation
  interface model_eval_dof_refelem
     module procedure model_eval_dof_raw_refelem_solo
     module procedure model_eval_dof_raw_refelem_multi
     module procedure model_eval_dof_all_refelem_solo
     module procedure model_eval_dof_all_refelem_multi
  end interface model_eval_dof_refelem
  interface model_eval_dof_index
     module procedure model_eval_dof_raw_solo_idof
  end interface model_eval_dof_index
  !--------------------------------------------------------------------- 
 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model for dof representation
  interface model_get_pdof
     module procedure model_get_pdof_real4
     module procedure model_get_pdof_real8
  end interface
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise polynomial representation
  interface model_get_piecewise_polynomial
     module procedure model_get_ppoly_real4
     module procedure model_get_ppoly_real8
  end interface
  !--------------------------------------------------------------------- 
  
  public  :: model_eval_piecewise_constant, model_eval_piecewise_polynomial
  public  :: model_get_piecewise_constant, model_get_piecewise_polynomial
  public  :: model_eval_sep, model_eval_dof_refelem
  public  :: model_eval_dof_index
  public  :: model_get_pdof
  
  contains
 
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-constant, here we just get
  !> the main, real models
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : which cell
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_constant_raw(ctx_model,strmodel,      &
                                               i_cell,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
        out_val = dble(ctx_model%vp%pconstant(i_cell))
      case(model_RHO     )
        out_val = dble(ctx_model%rho%pconstant(i_cell))
      case(model_BULK)    !! bulk modulus = rho*cp²
        out_val = dble(ctx_model%rho%pconstant(i_cell) *              &
                      (ctx_model%vp%pconstant(i_cell)**2))
      case(model_IMP)     !! Impedance    = sqrt(kappa * rho) = rho*cp
        out_val = dble(ctx_model%rho%pconstant(i_cell) *              &
                      (ctx_model%vp%pconstant(i_cell)))

      !! viscosity -----------------------------------------------------
      case(model_VISCOUS)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF,model_visco_Maxw,model_visco_plaw,      &
                 model_visco_KSB,model_visco_Szabo)
              out_val = ctx_model%visco(1)%pconstant(i_cell)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if  

      case(model_TAU_SIGMA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_CC,model_visco_Zener)
              out_val = ctx_model%visco(1)%pconstant(i_cell)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV)
              out_val = ctx_model%visco(1)%pconstant(i_cell)
            case(model_visco_CC,model_visco_Zener,model_visco_KSB)
              out_val = ctx_model%visco(2)%pconstant(i_cell)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_FPOWER)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_plaw,model_visco_Szabo)
              out_val = ctx_model%visco(2)%pconstant(i_cell)
            case(model_visco_CC,model_visco_KSB)
              out_val = ctx_model%visco(3)%pconstant(i_cell)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if
      !! ---------------------------------------------------------------

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval]"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_val < 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_piecewise_constant_raw

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-constant, here we adjust for 
  !> attenuation
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   out_vp     : complex-valued output vp
  !> @param[out]   out_rho    : output value for rho
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_constant_all(ctx_model,i_cell,freq,   &
                                               out_vp,out_rho,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    complex(kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_rho
    type(t_error)           ,intent(inout):: ctx_err
    !! local
    real   (kind=8)             :: kappa0,tauE,tauS,pw,eta,omega
    complex(kind=8)             :: kappa,freqpart
    !! -----------------------------------------------------------------

    out_vp = dcmplx(dble(ctx_model%vp%pconstant (i_cell)), 0.d0)
    out_rho=        dble(ctx_model%rho%pconstant(i_cell))
    call model_eval_piecewise_constant_raw(ctx_model,model_BULK,i_cell, &
                                           kappa0,ctx_err)
    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))

        !! *************************************************************
        !! *************************************************************
        !!
        !! WARNING: frequency already contains the imaginary unit
        !!
        !! *************************************************************
        !! *************************************************************

        !! c = c0 ( 1 - i/(2Q) ) ---------------------------------------        
        case(model_visco_KF)
          !! using Quality factor Q: V = Vp( 1 + i / (2*Q))
          if(abs(ctx_model%visco(1)%pconstant(i_cell))>tiny(1.d0)) then
            out_vp = out_vp * (1.d0 - dcmplx(0.d0,1.d0) /               &
                              (2.d0*dble(ctx_model%visco(1)%pconstant(i_cell))))
          end if       

        !! kappa = kappa_0 (1 - i omega tau_epsilon) -------------------
        case(model_visco_KV   )
          tauE = dble(ctx_model%visco(1)%pconstant(i_cell))
          kappa = kappa0*(1.d0-freq*tauE)
          out_vp= sqrt(kappa/out_rho)

        !! kappa =-i omega kappa_0 visco / (kappa_0 - i omega visco) ---
        case(model_visco_Maxw )
          eta   = dble(ctx_model%visco(1)%pconstant(i_cell))
          kappa = -freq*kappa0*eta/(kappa0 - freq*eta)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = kappa_0 * ( 1 + (-i omega tau_eps  )^beta) 
        !!                 / ( 1 + (-i omega tau_sigma)^beta) -----------
        case(model_visco_CC   )
          tauS = dble(ctx_model%visco(1)%pconstant(i_cell))
          tauE = dble(ctx_model%visco(2)%pconstant(i_cell))
          pw   = dble(ctx_model%visco(3)%pconstant(i_cell))
          kappa = kappa0 * (1.d0 + (-freq*tauE)**pw)/(1.d0 + (-freq*tauS)**pw)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = kappa_0 * ( 1 - i omega tau_eps  ) 
        !!                 / ( 1 - i omega tau_sigma) ------------------
        case(model_visco_Zener)
          tauS = dble(ctx_model%visco(1)%pconstant(i_cell))
          tauE = dble(ctx_model%visco(2)%pconstant(i_cell))
          !! validity condition 
          if(tauE < tauS) then
            ctx_err%msg   = "** ERROR: Compatibility conditions for "// &
                            "the acoustic Zener viscosity model "    // &
                            "are not satisfied [model_eval]"
            ctx_err%ierr  = -1
            return !! non-shared error.
          end if
          kappa = kappa0 * (1.d0 - freq*tauE) / (1.d0 - freq*tauS)
          out_vp= sqrt(kappa/out_rho)

        !! power-law ---------------------------------------------------
        case(model_visco_plaw )
          eta  = dble(ctx_model%visco(1)%pconstant(i_cell))
          pw   = dble(ctx_model%visco(2)%pconstant(i_cell))
          if(pw <= 0 .or. pw > 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for Kramers-"    // &
                            "Koning relation in the power law "          // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          !! change imaginary and real part of the frequency 
          !! using + for the imaginary part now.
          freqpart = dcmplx(aimag(freq), -real(freq))
          omega    = aimag(freq)
          out_vp =freqpart * out_vp /                                   &
                 (freqpart + dcmplx(0.d0,1.d0)*out_vp*eta*abs(omega)**(1.d0-pw))

        !! kappa = kappa_0 / ( 1 + eta (-i omega)^(beta-1)) ------------
        case(model_visco_Szabo)
          eta  = dble(ctx_model%visco(1)%pconstant(i_cell))
          pw   = dble(ctx_model%visco(2)%pconstant(i_cell))
          if(pw <= 0 .or. pw >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified Szabo model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /(1.d0 + eta*((-freq)**(pw-1.d0)))
          out_vp= sqrt(kappa/out_rho)

        !! kappa = kappa_0  / (1 + eta/sqrt(1+(-i omega tau)^beta))² ---
        case(model_visco_KSB)
          eta  = dble(ctx_model%visco(1)%pconstant(i_cell))
          tauE = dble(ctx_model%visco(2)%pconstant(i_cell))
          pw   = dble(ctx_model%visco(3)%pconstant(i_cell))
          if(pw <= 0 .or. pw >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified KSB model of "                     // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /((1.d0 + eta/sqrt(1.d0 + (-freq*tauE)**pw))**2)
          out_vp= sqrt(kappa/out_rho)

        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
      end select
    end if  
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(real(out_vp) .le. 0.d0 .or. out_rho .le. 0.d0 .or. aimag(out_vp)>0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_piecewise_constant_all

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a saved sep 
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_sep_raw(ctx_model,strmodel,xpt,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)      :: v, r

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
        call model_representation_eval_sep(ctx_model%vp%param_sep,      &
                                           ctx_model%dim_domain,xpt,    &
                                           out_val,ctx_err)
      case(model_RHO     )
        call model_representation_eval_sep(ctx_model%rho%param_sep,     &
                                           ctx_model%dim_domain,xpt,    &
                                           out_val,ctx_err)
       
      !! viscosity -----------------------------------------------------
      case(model_VISCOUS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF,model_visco_Maxw,model_visco_plaw,      &
                 model_visco_Szabo,model_visco_KSB)
              call model_representation_eval_sep(                       &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        out_val,ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if  

      case(model_TAU_SIGMA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_CC,model_visco_Zener)
              call model_representation_eval_sep(                       &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        out_val,ctx_err)            
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV)
              call model_representation_eval_sep(                       &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        out_val,ctx_err)
            case(model_visco_CC,model_visco_Zener,model_visco_KSB)
              call model_representation_eval_sep(                       &
                                        ctx_model%visco(2)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        out_val,ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_FPOWER)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_plaw,model_visco_Szabo)
              call model_representation_eval_sep(                       &
                                        ctx_model%visco(2)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        out_val,ctx_err)
            case(model_visco_CC,model_visco_KSB)
              call model_representation_eval_sep(                       &
                                        ctx_model%visco(3)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        out_val,ctx_err)              
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if
      !! ---------------------------------------------------------------

      case(model_BULK)    !! bulk modulus = rho*cp²
        call model_representation_eval_sep(ctx_model%vp%param_sep,      &
                                           ctx_model%dim_domain,xpt,    &
                                           v,ctx_err)
        call model_representation_eval_sep(ctx_model%rho%param_sep,     &
                                           ctx_model%dim_domain,xpt,    &
                                           r,ctx_err)
        out_val = dble(r * (v**2))
      case(model_IMP)     !! Impedance    = sqrt(kappa * rho) = rho*cp
        call model_representation_eval_sep(ctx_model%vp%param_sep,      &
                                           ctx_model%dim_domain,xpt,    &
                                           v,ctx_err)
        call model_representation_eval_sep(ctx_model%rho%param_sep,     &
                                           ctx_model%dim_domain,xpt,    &
                                           r,ctx_err)
        out_val = dble(r * v)
      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval]"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_val < 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_sep_raw


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for sep saved
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    xpt        : coordinates
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   out_vp     : output value
  !> @param[out]   out_rho    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_sep_all(ctx_model,xpt,freq,out_vp,out_rho,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    real   (kind=8)         ,intent(in)   :: xpt(:)
    complex(kind=8)         ,intent(in)   :: freq
    complex(kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_rho
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8)      :: v, q, kappa0, power, tauE, tauS, omega, eta
    complex(kind=8)      :: kappa, freqpart

    call model_representation_eval_sep(ctx_model%vp%param_sep,          &
                                       ctx_model%dim_domain,xpt,v,ctx_err)
    call model_representation_eval_sep(ctx_model%rho%param_sep,         &
                                       ctx_model%dim_domain,xpt,        &
                                       out_rho,ctx_err)
    out_vp = dcmplx(v,0.d0)
    kappa0 = dble(out_rho*v*v)
    q      = 0.d0

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))

        !! *************************************************************
        !! *************************************************************
        !!
        !! WARNING: frequency already contains the imaginary unit
        !!
        !! *************************************************************
        !! *************************************************************
        
        !! c = c0 ( 1 - i/(2Q) ) ---------------------------------------        
        case(model_visco_KF)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        q,ctx_err)
          if(abs(q)>tiny(1.d0)) then
            out_vp = v * (1.d0 - dcmplx(0.d0,1.d0) / (2.d0*dble(q)))
          end if

        !! kappa = kappa_0 (1 - i omega tau_epsilon) -------------------
        case(model_visco_KV   )
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        tauE,ctx_err)
          kappa = kappa0*(1.d0-freq*tauE)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = i omega kappa_0 visco / (kappa_0 - i omega visco) ---
        case(model_visco_Maxw )
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        eta,ctx_err)
          kappa = -freq*kappa0*eta/(kappa0 - freq*eta)
          out_vp= sqrt(kappa/out_rho)
          
        !! kappa = kappa_0 * ( 1 + (-i omega tau_eps  )^beta) 
        !!                 / ( 1 + (-i omega tau_sigma)^beta) -----------
        case(model_visco_CC   )
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        tauS,ctx_err)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(2)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        tauE,ctx_err)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(3)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        power,ctx_err)
          
          kappa = kappa0 * (1.d0 + (-freq*tauE)**power)                 &
                         / (1.d0 + (-freq*tauS)**power)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = kappa_0 * ( 1 - i omega tau_eps  ) 
        !!                 / ( 1 - i omega tau_sigma) ------------------
        case(model_visco_Zener)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        tauS,ctx_err)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(2)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        tauE,ctx_err)
          !! validity condition 
          if(tauE < tauS) then
            ctx_err%msg   = "** ERROR: Compatibility conditions for "// &
                            "the acoustic Zener viscosity model "    // &
                            "are not satisfied [model_eval]"
            ctx_err%ierr  = -1
            return !! non-shared error.
          end if
          kappa = kappa0 * (1.d0 - freq*tauE) / (1.d0 - freq*tauS)
          out_vp= sqrt(kappa/out_rho)

        !! power-law ---------------------------------------------------
        case(model_visco_plaw )
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        eta,ctx_err)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(2)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        power,ctx_err)
          if(power <= 0 .or. power > 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for Kramers-"    // &
                            "Koning relation in the power law "          // &
                            "attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          !! change imaginary and real part of the frequency 
          !! using + for the imaginary part now.
          freqpart = dcmplx(aimag(freq), -real(freq))
          omega    = aimag(freq)
          out_vp =freqpart * out_vp /                                   &
                 (freqpart +                                            &
                  dcmplx(0.d0,1.d0)*out_vp*eta*abs(omega)**(1.d0-power))

        !! Szabo model -------------------------------------------------
        !! kappa = kappa_0 / ( 1 + eta (-i omega)^(beta-1)) ------------
        case(model_visco_Szabo)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        eta,ctx_err)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(2)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        power,ctx_err)
          if(power <= 0 .or. power >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified Szabo model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /(1.d0 + eta*((-freq)**(power-1.d0)))
          out_vp= sqrt(kappa/out_rho)

        !! KSB model ---------------------------------------------------
        !! kappa = kappa_0  / (1 + eta/sqrt(1+(-i omega tau)^beta))² ---
        case(model_visco_KSB)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(1)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        eta,ctx_err)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(2)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        tauE,ctx_err)
          call model_representation_eval_sep(                           &
                                        ctx_model%visco(3)%param_sep,   &
                                        ctx_model%dim_domain,xpt,       &
                                        power,ctx_err)
          if(power <= 0 .or. power >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified KSB model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /((1.d0 + eta/sqrt(1.d0 + (-freq*tauE)**power))**2)
          out_vp= sqrt(kappa/out_rho)

        !! error -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
      end select
    end if

    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(real(out_vp) .le. 0.d0 .or. out_rho .le. 0.d0 .or.               &
      aimag(out_vp) > 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "    // &
                      trim(adjustl(ctx_err%msg))
      ctx_err%ierr  = -1
    end if
    return

  end subroutine model_eval_sep_all

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation
  !
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_refelem_solo(ctx_model,i_cell,strmodel, &
                                               xpt,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)      :: v, r

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
       call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,  &
                                                  xpt,out_val,ctx_err)
      case(model_RHO     )
       call model_representation_dof_eval_refelem(ctx_model%rho,i_cell, &
                                                  xpt,out_val,ctx_err)
                                                 
      !! viscosity -----------------------------------------------------
      case(model_VISCOUS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF,model_visco_Maxw,model_visco_plaw,      &
                 model_visco_KSB,model_visco_Szabo)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(1),    &
                                                 i_cell,xpt,out_val,    &
                                                 ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if  

      case(model_TAU_SIGMA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_CC,model_visco_Zener)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(1),    &
                                                 i_cell,xpt,out_val,    &
                                                 ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(1),    &
                                                 i_cell,xpt,out_val,    &
                                                 ctx_err)

            case(model_visco_CC,model_visco_Zener,model_visco_KSB)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(2),    &
                                                 i_cell,xpt,out_val,    &
                                                 ctx_err)

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_FPOWER)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_plaw,model_visco_Szabo)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(2),    &
                                                 i_cell,xpt,out_val,    &
                                                 ctx_err)

            case(model_visco_CC,model_visco_KSB)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(3),    &
                                                 i_cell,xpt,out_val,    &
                                                 ctx_err)
           
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if
      !! ---------------------------------------------------------------

      case(model_BULK)    !! bulk modulus = rho*cp²
        call model_representation_dof_eval_refelem(ctx_model%vp,        &
                                           i_cell,xpt,v,ctx_err)
        call model_representation_dof_eval_refelem(ctx_model%rho,       &
                                           i_cell,xpt,r,ctx_err)
        out_val = dble(r * (v**2))
      case(model_IMP)     !! Impedance    = sqrt(kappa * rho) = rho*cp
        call model_representation_dof_eval_refelem(ctx_model%vp,        &
                                           i_cell,xpt,v,ctx_err)
        call model_representation_dof_eval_refelem(ctx_model%rho,       &
                                           i_cell,xpt,r,ctx_err)
        out_val = dble(r * v)
      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval]"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_val < 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_dof_raw_refelem_solo
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation at a given index
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_solo_idof(ctx_model,i_cell,strmodel,    &
                                          i_dof,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    integer(kind=4)         ,intent(in)   :: i_dof
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)      :: v, r

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
       out_val=dble(ctx_model%vp%param_dof%mval(i_cell)%array(i_dof))
      case(model_RHO     )
       out_val=dble(ctx_model%rho%param_dof%mval(i_cell)%array(i_dof))
                                                 
      !! viscosity -----------------------------------------------------
      case(model_VISCOUS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF,model_visco_Maxw,model_visco_plaw,      &
                 model_visco_KSB,model_visco_Szabo)
              out_val=dble(                                             &
                      ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof))
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if  

      case(model_TAU_SIGMA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_CC,model_visco_Zener)
              out_val=dble(                                             &
                      ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof))
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV)
              out_val=dble(                                             &
                      ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof))

            case(model_visco_CC,model_visco_Zener,model_visco_KSB)
              out_val=dble(                                             &
                      ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof))

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_FPOWER)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_plaw,model_visco_Szabo)
              out_val=dble(                                             &
                      ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof))

            case(model_visco_CC,model_visco_KSB)
              out_val=dble(                                             &
                      ctx_model%visco(3)%param_dof%mval(i_cell)%array(i_dof))
           
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if
      !! ---------------------------------------------------------------

      case(model_BULK)    !! bulk modulus = rho*cp²
        v=dble(ctx_model%vp%param_dof%mval(i_cell)%array(i_dof) )
        r=dble(ctx_model%rho%param_dof%mval(i_cell)%array(i_dof))
        out_val = dble(r * (v**2))
      case(model_IMP)     !! Impedance    = sqrt(kappa * rho) = rho*cp
        v=dble(ctx_model%vp%param_dof%mval(i_cell)%array(i_dof) )
        r=dble(ctx_model%rho%param_dof%mval(i_cell)%array(i_dof))
        out_val = dble(r * v)
      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval]"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_val < 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_dof_raw_solo_idof


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation
  !
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_refelem_multi(ctx_model,i_cell,         &
                                                strmodel,xpt,npt,       &
                                                out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:,:)
    integer                 ,intent(in)   :: npt
    real   (kind=8)         ,intent(out)  :: out_val(:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8), allocatable  :: v(:), r(:)

    out_val = 0.d0 
    allocate(v(npt))
    allocate(r(npt))


    select case(trim(adjustl(strmodel)))
      case(model_VP      )
       call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,  &
                                          xpt,npt,out_val,ctx_err)
      case(model_RHO     )
       call model_representation_dof_eval_refelem(ctx_model%rho,i_cell, &
                                          xpt,npt,out_val,ctx_err)
                                                 
      !! viscosity -----------------------------------------------------
      case(model_VISCOUS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF,model_visco_Maxw,model_visco_plaw,      &
                 model_visco_KSB,model_visco_Szabo)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(1),    &
                                                 i_cell,xpt,npt,out_val,&
                                                 ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if  

      case(model_TAU_SIGMA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_CC,model_visco_Zener)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(1),    &
                                                 i_cell,xpt,npt,out_val,&
                                                 ctx_err)           
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(1),    &
                                                 i_cell,xpt,npt,out_val,&
                                                 ctx_err)

            case(model_visco_CC,model_visco_Zener,model_visco_KSB)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(2),    &
                                                 i_cell,xpt,npt,out_val,&
                                                 ctx_err)

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_FPOWER)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_plaw,model_visco_Szabo)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(2),    &
                                                 i_cell,xpt,npt,out_val,&
                                                 ctx_err)

            case(model_visco_CC,model_visco_KSB)
              call model_representation_dof_eval_refelem(               &
                                                 ctx_model%visco(3),    &
                                                 i_cell,xpt,npt,out_val,&
                                                 ctx_err)
           
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if
      !! ---------------------------------------------------------------

      case(model_BULK)    !! bulk modulus = rho*cp²
        call model_representation_dof_eval_refelem(ctx_model%vp,        &
                                           i_cell,xpt,npt,v,ctx_err)
        call model_representation_dof_eval_refelem(ctx_model%rho,       &
                                           i_cell,xpt,npt,r,ctx_err)
        out_val = dble(r * (v**2))
      case(model_IMP)     !! Impedance    = sqrt(kappa * rho) = rho*cp
        call model_representation_dof_eval_refelem(ctx_model%vp,        &
                                           i_cell,xpt,npt,v,ctx_err)
        call model_representation_dof_eval_refelem(ctx_model%rho,       &
                                           i_cell,xpt,npt,r,ctx_err)
        out_val = dble(r * v)
      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval]"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    deallocate(v)
    deallocate(r)
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(minval(out_val) < 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_dof_raw_refelem_multi


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for the dof representation
  !
  !> @param[in]    ctx_mesh   : mesh context
  !> @param[in]    ctx_model  : model context
  !> @param[in]    xpt        : coordinates
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   out_vp     : output value
  !> @param[out]   out_rho    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_all_refelem_solo(ctx_model,i_cell,xpt,freq, &
                                             out_vp,out_rho,ctx_err)

    implicit none
    type(t_model)           ,intent(in)   :: ctx_model
    real   (kind=8)         ,intent(in)   :: xpt(:)
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    complex(kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_rho
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8)      :: v, q, kappa0, power, tauE, tauS, omega, eta
    complex(kind=8)      :: kappa, freqpart

    call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,     &
                                               xpt,v,ctx_err)
    call model_representation_dof_eval_refelem(ctx_model%rho,i_cell,    &
                                               xpt,out_rho,ctx_err)

    out_vp = dcmplx(v,0.d0)
    kappa0 = dble(out_rho*v*v)
    q      = 0.d0

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))

        !! *************************************************************
        !! *************************************************************
        !!
        !! WARNING: frequency already contains the imaginary unit
        !!
        !! *************************************************************
        !! *************************************************************
        
        !! c = c0 ( 1 - i/(2Q) ) ---------------------------------------        
        case(model_visco_KF)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        q,ctx_err)
          if(abs(q)>tiny(1.d0)) then
            out_vp = v * (1.d0 - dcmplx(0.d0,1.d0) / (2.d0*dble(q)))
          end if

        !! kappa = kappa_0 (1 - i omega tau_epsilon) -------------------
        case(model_visco_KV   )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        tauE,ctx_err)
          kappa = kappa0*(1.d0-freq*tauE)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = i omega kappa_0 visco / (kappa_0 - i omega visco) ---
        case(model_visco_Maxw )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        eta,ctx_err)
          kappa = -freq*kappa0*eta/(kappa0 - freq*eta)
          out_vp= sqrt(kappa/out_rho)
          
        !! kappa = kappa_0 * ( 1 + (-i omega tau_eps  )^beta) 
        !!                 / ( 1 + (-i omega tau_sigma)^beta) -----------
        case(model_visco_CC   )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        tauS,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        tauE,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(3),i_cell,xpt,  &
                                        power,ctx_err)

          kappa = kappa0 * (1.d0 + (-freq*tauE)**power)                 &
                         / (1.d0 + (-freq*tauS)**power)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = kappa_0 * ( 1 - i omega tau_eps  ) 
        !!                 / ( 1 - i omega tau_sigma) ------------------
        case(model_visco_Zener)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        tauS,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        tauE,ctx_err)
          !! validity condition 
          if(tauE < tauS) then
            ctx_err%msg   = "** ERROR: Compatibility conditions for "// &
                            "the acoustic Zener viscosity model "    // &
                            "are not satisfied [model_eval]"
            ctx_err%ierr  = -1
            return !! non-shared error.
          end if
          kappa = kappa0 * (1.d0 - freq*tauE) / (1.d0 - freq*tauS)
          out_vp= sqrt(kappa/out_rho)

        !! power-law ---------------------------------------------------
        case(model_visco_plaw )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        eta,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        power,ctx_err)
          if(power <= 0 .or. power > 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for Kramers-"    // &
                            "Koning relation in the power law "          // &
                            "attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          !! change imaginary and real part of the frequency 
          !! using + for the imaginary part now.
          freqpart = dcmplx(aimag(freq), -real(freq))
          omega    = aimag(freq)
          out_vp =freqpart * out_vp /                                   &
                 (freqpart +                                            &
                  dcmplx(0.d0,1.d0)*out_vp*eta*abs(omega)**(1.d0-power))

        !! Szabo model -------------------------------------------------
        !! kappa = kappa_0 / ( 1 + eta (-i omega)^(beta-1)) ------------
        case(model_visco_Szabo)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        eta,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        power,ctx_err)
          if(power <= 0 .or. power >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified Szabo model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /(1.d0 + eta*((-freq)**(power-1.d0)))
          out_vp= sqrt(kappa/out_rho)

        !! KSB model ---------------------------------------------------
        !! kappa = kappa_0  / (1 + eta/sqrt(1+(-i omega tau)^beta))² ---
        case(model_visco_KSB)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        eta,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        tauE,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(3),i_cell,xpt,  &
                                        power,ctx_err)
          if(power <= 0 .or. power >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified KSB model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /((1.d0 + eta/sqrt(1.d0 + (-freq*tauE)**power))**2)
          out_vp= sqrt(kappa/out_rho)

        !! ERROR -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end select
    end if

    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(real(out_vp) .le. 0.d0 .or. out_rho .le. 0.d0 .or.               &
      aimag(out_vp) > 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "    // &
                      trim(adjustl(ctx_err%msg))
      ctx_err%ierr  = -1
    end if
    return

  end subroutine model_eval_dof_all_refelem_solo

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for the dof representation
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    xpt        : coordinates
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   out_vp     : output value
  !> @param[out]   out_rho    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_all_refelem_multi(ctx_model,i_cell,xpt,npt, &
                                      freq,out_vp,out_rho,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    real   (kind=8)         ,intent(in)   :: xpt(:,:)
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    integer                 ,intent(in)   :: npt
    complex(kind=8)         ,intent(in)   :: freq
    complex(kind=8)         ,intent(out)  :: out_vp(:)
    real   (kind=8)         ,intent(out)  :: out_rho(:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8),allocatable :: v(:), q(:), kappa0(:), power(:)
    real   (kind=8),allocatable :: tauE(:), tauS(:), eta(:)
    real   (kind=8)             :: omega
    complex(kind=8)             :: freqpart
    complex(kind=8),allocatable :: kappa(:)

    allocate(v(npt))
    allocate(q(npt))
    allocate(kappa0(npt))
    allocate(power(npt))
    allocate(tauS(npt))
    allocate(tauE(npt))
    allocate(eta(npt))
    allocate(kappa(npt))

    call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,     &
                                       xpt,npt,v,ctx_err)
    call model_representation_dof_eval_refelem(ctx_model%rho,i_cell,    &
                                       xpt,npt,out_rho,ctx_err)

    out_vp = dcmplx(v,0.d0)
    kappa0 = dble(out_rho*v*v)
    q      = 0.d0

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))

        !! *************************************************************
        !! *************************************************************
        !!
        !! WARNING: frequency already contains the imaginary unit
        !!
        !! *************************************************************
        !! *************************************************************
        
        !! c = c0 ( 1 - i/(2Q) ) ---------------------------------------        
        case(model_visco_KF)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,q,ctx_err)
          if(maxval(abs(q))>tiny(1.d0)) then
            out_vp = v * (1.d0 - dcmplx(0.d0,1.d0) / (2.d0*dble(q)))
          end if

        !! kappa = kappa_0 (1 - i omega tau_epsilon) -------------------
        case(model_visco_KV   )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,tauE,ctx_err)
          kappa = kappa0*(1.d0-freq*tauE)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = i omega kappa_0 visco / (kappa_0 - i omega visco) ---
        case(model_visco_Maxw )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,eta,ctx_err)
          kappa = -freq*kappa0*eta/(kappa0 - freq*eta)
          out_vp= sqrt(kappa/out_rho)
          
        !! kappa = kappa_0 * ( 1 + (-i omega tau_eps  )^beta) 
        !!                 / ( 1 + (-i omega tau_sigma)^beta) -----------
        case(model_visco_CC   )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,tauS,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        npt,tauE,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(3),i_cell,xpt,  &
                                        npt,power,ctx_err)

          kappa = kappa0 * (1.d0 + (-freq*tauE)**power)                 &
                         / (1.d0 + (-freq*tauS)**power)
          out_vp= sqrt(kappa/out_rho)

        !! kappa = kappa_0 * ( 1 - i omega tau_eps  ) 
        !!                 / ( 1 - i omega tau_sigma) ------------------
        case(model_visco_Zener)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,tauS,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        npt,tauE,ctx_err)
          !! validity condition 
          if(minval(tauE) < maxval(tauS)) then
            ctx_err%msg   = "** ERROR: Compatibility conditions for "// &
                            "the acoustic Zener viscosity model "    // &
                            "are not satisfied [model_eval]"
            ctx_err%ierr  = -1
            return !! non-shared error.
          end if
          kappa = kappa0 * (1.d0 - freq*tauE) / (1.d0 - freq*tauS)
          out_vp= sqrt(kappa/out_rho)

        !! power-law ---------------------------------------------------
        case(model_visco_plaw )
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,eta,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        npt,power,ctx_err)
          if(minval(power) <= 0 .or. maxval(power) > 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for Kramers-"    // &
                            "Koning relation in the power law "          // &
                            "attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          !! change imaginary and real part of the frequency 
          !! using + for the imaginary part now.
          freqpart = dcmplx(aimag(freq), -real(freq))
          omega    = aimag(freq)
          out_vp =freqpart * out_vp /                                   &
                 (freqpart +                                            &
                  dcmplx(0.d0,1.d0)*out_vp*eta*abs(omega)**(1.d0-power))

        !! Szabo model -------------------------------------------------
        !! kappa = kappa_0 / ( 1 + eta (-i omega)^(beta-1)) ------------
        case(model_visco_Szabo)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,eta,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        npt,power,ctx_err)
          if(minval(power) <= 0 .or. maxval(power) >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified Szabo model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /(1.d0 + eta*((-freq)**(power-1.d0)))
          out_vp= sqrt(kappa/out_rho)

        !! KSB model ---------------------------------------------------
        !! kappa = kappa_0  / (1 + eta/sqrt(1+(-i omega tau)^beta))² ---
        case(model_visco_KSB)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(1),i_cell,xpt,  &
                                        npt,eta,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(2),i_cell,xpt,  &
                                        npt,tauE,ctx_err)
          call model_representation_dof_eval_refelem(                   &
                                        ctx_model%visco(3),i_cell,xpt,  &
                                        npt,power,ctx_err)
          if(minval(power) <= 0 .or. maxval(power) >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified KSB model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /((1.d0 + eta/sqrt(1.d0 + (-freq*tauE)**power))**2)
          out_vp= sqrt(kappa/out_rho)

        !! ERROR -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end select
    end if

    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(minval(real(out_vp)) .le. 0.d0 .or. minval(out_rho) .le. 0.d0    &
       .or. maxval(aimag(out_vp)) > 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "    // &
                      trim(adjustl(ctx_err%msg))
      ctx_err%ierr  = -1
    end if
    return

  end subroutine model_eval_dof_all_refelem_multi


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-polynomial, here we just get
  !> the main, real models
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : which cell
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_polynomial_raw(ctx_model,strmodel,i_cell, &
                                                 xpt,out_val,ctx_err)
    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !!
    real(kind=RKIND_POL)                :: v,vp,rho
    real(kind=RKIND_POL), allocatable   :: x(:)
    !! 
    !! -----------------------------------------------------------------

    allocate(x(ctx_model%dim_domain))
    x = real(xpt,kind=RKIND_POL)

    out_val = 0.d0

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
        call polynomial_eval(ctx_model%vp%ppoly(i_cell),x,v,ctx_err)
        out_val = dble(v)
        if(out_val < ctx_model%vp_min) out_val = ctx_model%vp_min
        if(out_val > ctx_model%vp_max) out_val = ctx_model%vp_max
        
      case(model_RHO     )
        call polynomial_eval(ctx_model%rho%ppoly(i_cell),x,v,ctx_err)
        out_val = dble(v)

        if(out_val < ctx_model%rho_min) out_val = ctx_model%rho_min
        if(out_val > ctx_model%rho_max) out_val = ctx_model%rho_max

      !! viscosity -----------------------------------------------------
      case(model_VISCOUS)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF,model_visco_Maxw,model_visco_plaw,      &
                 model_visco_Szabo,model_visco_KSB)
              call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,v,ctx_err)
              out_val = dble(v)
              if(out_val < ctx_model%visco_min(1)) out_val=ctx_model%visco_min(1)
              if(out_val > ctx_model%visco_max(1)) out_val=ctx_model%visco_max(1)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if  

      case(model_TAU_SIGMA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_CC,model_visco_Zener)
              call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,v,ctx_err)
              out_val = dble(v)
              if(out_val < ctx_model%visco_min(1)) out_val=ctx_model%visco_min(1)
              if(out_val > ctx_model%visco_max(1)) out_val=ctx_model%visco_max(1)

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_CC,model_visco_Zener,model_visco_KSB)
              call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),x,v,ctx_err)
              out_val = dble(v)
              if(out_val < ctx_model%visco_min(2)) out_val=ctx_model%visco_min(2)
              if(out_val > ctx_model%visco_max(2)) out_val=ctx_model%visco_max(2)
            case(model_visco_KV)
              call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,v,ctx_err)
              out_val = dble(v)
              if(out_val < ctx_model%visco_min(1)) out_val=ctx_model%visco_min(1)
              if(out_val > ctx_model%visco_max(1)) out_val=ctx_model%visco_max(1)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if

      case(model_FPOWER)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_plaw,model_visco_Szabo)
              call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),x,v,ctx_err)
              out_val = dble(v)
              if(out_val < ctx_model%visco_min(2)) out_val=ctx_model%visco_min(2)
              if(out_val > ctx_model%visco_max(2)) out_val=ctx_model%visco_max(2)
            case(model_visco_CC,model_visco_KSB)
              call polynomial_eval(ctx_model%visco(3)%ppoly(i_cell),x,v,ctx_err)
              out_val = dble(v)
              if(out_val < ctx_model%visco_min(3)) out_val=ctx_model%visco_min(3)
              if(out_val > ctx_model%visco_max(3)) out_val=ctx_model%visco_max(3)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end if
      !! ---------------------------------------------------------------

      case(model_BULK)    !! bulk modulus = rho*cp²
        call polynomial_eval(ctx_model%vp%ppoly (i_cell),x,vp,ctx_err)
        call polynomial_eval(ctx_model%rho%ppoly(i_cell),x,rho,ctx_err)
        if(vp  < ctx_model%vp_min)  vp  = ctx_model%vp_min
        if(vp  > ctx_model%vp_max)  vp  = ctx_model%vp_max
        if(rho < ctx_model%rho_min) rho = ctx_model%rho_min
        if(rho > ctx_model%rho_max) rho = ctx_model%rho_max
        
        out_val = dble(rho*(vp**2))
        
      case(model_IMP)     !! Impedance    = sqrt(kappa * rho) = rho*cp
        call polynomial_eval(ctx_model%vp%ppoly (i_cell),x,vp,ctx_err)
        call polynomial_eval(ctx_model%rho%ppoly(i_cell),x,rho,ctx_err)
        if(vp  < ctx_model%vp_min)  vp  = ctx_model%vp_min
        if(vp  > ctx_model%vp_max)  vp  = ctx_model%vp_max
        if(rho < ctx_model%rho_min) rho = ctx_model%rho_min
        if(rho > ctx_model%rho_max) rho = ctx_model%rho_max

        out_val = dble(rho*vp)

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval]"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_val < 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_piecewise_polynomial_raw

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-polynomial in a given point, 
  !> where we adjust for attenuation.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : cell in which the x belongs to
  !> @param[in]    x          : which position in this cell
  !> @param[in]    freq       : frequency, only for viscosity model
  !> @param[out]   out_vp     : output value
  !> @param[out]   out_rho    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_polynomial_all(ctx_model,i_cell,      &
                                  xpt,freq,out_vp,out_rho,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:)
    complex(kind=8)         ,intent(in)   :: freq
    complex(kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_rho
    type(t_error)           ,intent(inout):: ctx_err
    !!
    real   (kind=RKIND_POL)                :: vp,rho,q,tauS,tauE,power,eta
    real   (kind=RKIND_POL)                :: kappa0, omega
    complex(kind=RKIND_POL)                :: kappa, freqpart
    real   (kind=RKIND_POL), allocatable   :: x(:)
    !! 

    allocate(x(ctx_model%dim_domain))
    x = real(xpt,kind=RKIND_POL)
    
    call polynomial_eval(ctx_model%vp%ppoly (i_cell),x,vp ,ctx_err)
    call polynomial_eval(ctx_model%rho%ppoly(i_cell),x,rho,ctx_err)
    if(vp  < ctx_model%vp_min)  vp  = real(ctx_model%vp_min ,kind=RKIND_POL)
    if(vp  > ctx_model%vp_max)  vp  = real(ctx_model%vp_max ,kind=RKIND_POL)
    if(rho < ctx_model%rho_min) rho = real(ctx_model%rho_min,kind=RKIND_POL)
    if(rho > ctx_model%rho_max) rho = real(ctx_model%rho_max,kind=RKIND_POL)
    
    out_vp  = dcmplx(dble(vp),0.d0)
    out_rho =        dble(rho)
    kappa0  = dble(out_rho * out_vp * out_vp)

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        !! c = c0 ( 1 - i/(2Q) ) ---------------------------------------
        case(model_visco_KF)
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,q,ctx_err)
          if(q < ctx_model%visco_min(1)) q=ctx_model%visco_min(1)
          if(q > ctx_model%visco_max(1)) q=ctx_model%visco_max(1)
          !! using Quality factor Q: V = Vp( 1 + i / (2*Q))
          if(abs(q) > tiny(1.d0)) then
            out_vp = dcmplx(out_vp * (1.d0 - dcmplx(0.d0,1.d0) / (2.d0*dble(q))))
          end if
        
        !! kappa = kappa_0 (1 - i omega tau_epsilon) -------------------
        case(model_visco_KV   )
          tauE = dble(ctx_model%visco(1)%pconstant(i_cell))
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,tauE,ctx_err)
          if(tauE < ctx_model%visco_min(1)) tauE=ctx_model%visco_min(1)
          if(tauE > ctx_model%visco_max(1)) tauE=ctx_model%visco_max(1)
          kappa = kappa0*(1.d0-freq*tauE)
          out_vp= dcmplx(sqrt(kappa/out_rho))

        !! kappa = i omega kappa_0 visco / (kappa_0 - i omega visco) ---
        case(model_visco_Maxw )
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,eta,ctx_err)
          if(eta < ctx_model%visco_min(1)) eta=ctx_model%visco_min(1)
          if(eta > ctx_model%visco_max(1)) eta=ctx_model%visco_max(1)
          kappa = -freq*kappa0*eta/(kappa0 - freq*eta)
          out_vp= dcmplx(sqrt(kappa/out_rho))

        !! kappa = kappa_0 * ( 1 + (-i omega tau_eps  )^beta) 
        !!                 / ( 1 + (-i omega tau_sigma)^beta) -----------
        case(model_visco_CC   )
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,tauS,ctx_err)
          if(tauS < ctx_model%visco_min(1)) tauS=ctx_model%visco_min(1)
          if(tauS > ctx_model%visco_max(1)) tauS=ctx_model%visco_max(1)
          call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),x,tauE,ctx_err)
          if(tauE < ctx_model%visco_min(2)) tauE=ctx_model%visco_min(2)
          if(tauE > ctx_model%visco_max(2)) tauE=ctx_model%visco_max(2)
          call polynomial_eval(ctx_model%visco(3)%ppoly(i_cell),x,power,ctx_err)
          if(power < ctx_model%visco_min(3)) power=ctx_model%visco_min(3)
          if(power > ctx_model%visco_max(3)) power=ctx_model%visco_max(3)

          kappa = kappa0 * (1.d0 + (-freq*tauE)**power)                 &
                         / (1.d0 + (-freq*tauS)**power)
          out_vp= dcmplx(sqrt(kappa/out_rho))

        !! kappa = kappa_0 * ( 1 - i omega tau_eps  ) 
        !!                 / ( 1 - i omega tau_sigma) ------------------
        case(model_visco_Zener)
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,tauS,ctx_err)
          if(tauS < ctx_model%visco_min(1)) tauS=ctx_model%visco_min(1)
          if(tauS > ctx_model%visco_max(1)) tauS=ctx_model%visco_max(1)
          call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),x,tauE,ctx_err)
          if(tauE < ctx_model%visco_min(2)) tauE=ctx_model%visco_min(2)
          if(tauE > ctx_model%visco_max(2)) tauE=ctx_model%visco_max(2)

          !! validity condition 
          if(tauE < tauS) then
            ctx_err%msg   = "** ERROR: Compatibility conditions for "// &
                            "the acoustic Zener viscosity model "    // &
                            "are not satisfied [model_eval]"
            ctx_err%ierr  = -1
            return !! non-shared error.
          end if
          kappa = kappa0 * (1.d0 - freq*tauE) / (1.d0 - freq*tauS)
          out_vp= dcmplx(sqrt(kappa/out_rho))

        !! power-law ---------------------------------------------------
        case(model_visco_plaw )
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,eta,ctx_err)
          if(eta < ctx_model%visco_min(1)) eta=ctx_model%visco_min(1)
          if(eta > ctx_model%visco_max(1)) eta=ctx_model%visco_max(1)
          call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),x,power,ctx_err)
          if(power < ctx_model%visco_min(2)) power=ctx_model%visco_min(2)
          if(power > ctx_model%visco_max(2)) power=ctx_model%visco_max(2)
          if(power <= 0 .or. power > 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for Kramers-"    // &
                            "Koning relation in the power law "          // &
                            "attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if

          !! change imaginary and real part of the frequency 
          !! using + for the imaginary part now.
          freqpart = dcmplx(aimag(freq), -real(freq))
          omega    = aimag(freq)
          out_vp =dcmplx(freqpart * out_vp /                            &
                 (freqpart +                                            &
                 dcmplx(0.d0,1.d0)*out_vp*eta*abs(omega)**(1.d0-power)))

        !! Szabo model -------------------------------------------------
        !! kappa = kappa_0 / ( 1 + eta (-i omega)^(beta-1)) ------------
        case(model_visco_Szabo)
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,eta,ctx_err)
          if(eta < ctx_model%visco_min(1)) eta=ctx_model%visco_min(1)
          if(eta > ctx_model%visco_max(1)) eta=ctx_model%visco_max(1)
          call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),x,power,ctx_err)
          if(power < ctx_model%visco_min(2)) power=ctx_model%visco_min(2)
          if(power > ctx_model%visco_max(2)) power=ctx_model%visco_max(2)
          if(power <= 0 .or. power >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified Szabo model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /(1.d0 + eta*((-freq)**(power-1.d0)))
          out_vp= dcmplx(sqrt(kappa/out_rho))

        !! KSB model ---------------------------------------------------
        !! kappa = kappa_0  / (1 + eta/sqrt(1+(-i omega tau)^beta))² ---
        case(model_visco_KSB)
          call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),x,eta,ctx_err)
          if(eta < ctx_model%visco_min(1)) eta=ctx_model%visco_min(1)
          if(eta > ctx_model%visco_max(1)) eta=ctx_model%visco_max(1)
          call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),x,tauE,ctx_err)
          if(tauE < ctx_model%visco_min(2)) tauE=ctx_model%visco_min(2)
          if(tauE > ctx_model%visco_max(2)) tauE=ctx_model%visco_max(2)
          call polynomial_eval(ctx_model%visco(3)%ppoly(i_cell),x,power,ctx_err)
          if(power < ctx_model%visco_min(3)) power=ctx_model%visco_min(3)
          if(power > ctx_model%visco_max(3)) power=ctx_model%visco_max(3)
          if(power <= 0 .or. power >= 1) then
            ctx_err%msg   = "** ERROR: value for power of the frequency "// &
                            "must be in between 0 and 1 for the "        // &
                            "modified KSB model of "                   // &
                            "acoustic attenuation [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          kappa = kappa0 /((1.d0 + eta/sqrt(1.d0 + (-freq*tauE)**power))**2)
          out_vp= dcmplx(sqrt(kappa/out_rho))

        !! ERROR -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
      end select
    end if  
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(real(out_vp) .le. 0.d0 .or. out_rho .le. 0.d0 .or. aimag(out_vp)>0.) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for acoustic medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_piecewise_polynomial_all

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pconstant_real4(ctx_model,model_out,strmodel,    &
                                       freq,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4), allocatable          ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(IKIND_MESH)  :: i_cell
    real(kind=8)         :: tempval
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! loop over all cells 
    do i_cell = 1, ctx_model%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle
      
      !! piecewise constant evaluation 
      call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,     &
                                         tempval,ctx_err)
      !! save in model
      model_out(i_cell) = real(tempval)
    end do
    
    return
  end subroutine model_get_pconstant_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pconstant_real8(ctx_model,model_out,strmodel,    &
                                       freq,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8), allocatable          ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(IKIND_MESH)  :: i_cell
    real(kind=8)         :: tempval
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! loop over all cells 
    do i_cell = 1, ctx_model%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle
      
      !! piecewise constant evaluation 
      call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,     &
                                         tempval,ctx_err)
      !! save in model
      model_out(i_cell) = tempval
    end do
    
    return
  end subroutine model_get_pconstant_real8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_ppoly_real4(ctx_domain,ctx_model,model_out,      &
                                   strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k,nvisco
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = max(ctx_model%vp%ppoly_order,ctx_model%rho%ppoly_order)
    ndof_vol = max(ctx_model%vp%ppoly(1)%size_coeff,                    &
                   ctx_model%rho%ppoly(1)%size_coeff)
    if(ctx_model%flag_viscous) then
      nvisco = size(ctx_model%visco) 
      do k=1,nvisco
        order    = max(   order,ctx_model%visco(k)%ppoly_order)    
        ndof_vol = max(ndof_vol,ctx_model%visco(k)%ppoly(1)%size_coeff)
      end do
    end if

    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: at least one of the piecewise-" //     &
                      "polynomial model must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0    
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell ............................................
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo,ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol      
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call model_eval_piecewise_polynomial(ctx_model,strmodel,i_cell, &
                                             dof_coo(:,k),tempval,ctx_err)
        model_out(i1 + k) = real(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    !! -----------------------------------------------------------------
    
    return
  end subroutine model_get_ppoly_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_ppoly_real8(ctx_domain,ctx_model,model_out,      &
                                   strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k,nvisco
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = max(ctx_model%vp%ppoly_order,ctx_model%rho%ppoly_order)
    ndof_vol = max(ctx_model%vp%ppoly(1)%size_coeff,                    &
                   ctx_model%rho%ppoly(1)%size_coeff)
    if(ctx_model%flag_viscous) then
      nvisco = size(ctx_model%visco) 
      do k=1,nvisco
        order    = max(   order,ctx_model%visco(k)%ppoly_order)    
        ndof_vol = max(ndof_vol,ctx_model%visco(k)%ppoly(1)%size_coeff)
      end do
    end if

    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: at least one of the piecewise-" //     &
                      "polynomial model must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0    
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell ............................................
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo,ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol      
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call model_eval_piecewise_polynomial(ctx_model,strmodel,i_cell, &
                                             dof_coo(:,k),tempval,ctx_err)
        model_out(i1 + k) = real(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    !! -----------------------------------------------------------------
    
    return
  end subroutine model_get_ppoly_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for a parameter 
  !> given per dof.
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pdof_real4(ctx_domain,ctx_model,model_out,       &
                                  strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol,k,nvisco
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = max(ctx_model%vp%param_dof%ndof,ctx_model%rho%param_dof%ndof)   
    order    = max(ctx_model%vp%param_dof%order,ctx_model%rho%param_dof%order)   
    
    if(ctx_model%flag_viscous) then
      nvisco = size(ctx_model%visco) 
      do k=1,nvisco
        ndof_vol = max(ndof_vol, ctx_model%visco(k)%param_dof%ndof)
        order    = max(order   , ctx_model%visco(k)%param_dof%order)
      end do
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    if(ndof_vol == 1) then
      do i_cell = 1, ctx_domain%n_cell_loc
        call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,   &
                                           tempval,ctx_err)
        model_out(i_cell) = real(tempval,kind=4)
      end do
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol, &
                                             ctx_domain%dim_domain,     &
                                             dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        call model_eval_dof_raw_refelem_multi(ctx_model,i_cell,         &
                                      strmodel,dof_coo,ndof_vol,out_val,&
                                      ctx_err)
        model_out(i_gb1:i_gb2) = real(out_val(:),kind=4)
      end do
      deallocate(dof_coo)
      deallocate(out_val)      
    end if
    
    return
  end subroutine model_get_pdof_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for a parameter 
  !> given per dof.
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pdof_real8(ctx_domain,ctx_model,model_out,       &
                                  strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol,k,nvisco
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = max(ctx_model%vp%param_dof%ndof,ctx_model%rho%param_dof%ndof)   
    order    = max(ctx_model%vp%param_dof%order,ctx_model%rho%param_dof%order)   
    
    if(ctx_model%flag_viscous) then
      nvisco = size(ctx_model%visco) 
      do k=1,nvisco
        ndof_vol = max(ndof_vol, ctx_model%visco(k)%param_dof%ndof)
        order    = max(order   , ctx_model%visco(k)%param_dof%order)
      end do
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    if(ndof_vol == 1) then
      do i_cell = 1, ctx_domain%n_cell_loc
        call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,   &
                                           tempval,ctx_err)
        model_out(i_cell) = real(tempval,kind=8)
      end do
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_loc_elem(ctx_domain,i_cell,order,   &
                                             ndof_vol,dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        call model_eval_dof_raw_refelem_multi(ctx_model,i_cell,         &
                                      strmodel,dof_coo,ndof_vol,out_val,&
                                      ctx_err)
        model_out(i_gb1:i_gb2) = real(out_val(:),kind=8)
      end do
      deallocate(dof_coo)
      deallocate(out_val)      
    end if
    
    return
  end subroutine model_get_pdof_real8
  
end module m_model_eval
