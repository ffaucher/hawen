!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_list.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines of the context for the model parameters for the
!> HELMHOLTZ ACOUSTIC EQUATION
!
!------------------------------------------------------------------------------
module m_model_list

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  !> list of model names
  character(len=2), parameter :: model_VP      = 'vp'
  character(len=3), parameter :: model_RHO     = 'rho'
  character(len=4), parameter :: model_BULK    = 'bulk'
  character(len=9), parameter :: model_IMP     = 'impedance'
  !! models of attenuation
  !! Kolsky--Futterman, Kelvin--Voigt, Maxwell, power-law, Zener and Cole-Cole
  character(len=5), parameter  :: model_VISCOUS   = 'visco'
  !! power-law and Cole-Cole models only
  character(len=10), parameter :: model_FPOWER    = 'freq-power'    
  !! Zener and Cole-Cole models only
  character(len=9),  parameter :: model_TAU_SIGMA = 'tau-sigma' 
  character(len=11), parameter :: model_TAU_EPS   = 'tau-epsilon' 
  !! Quality factors
  character(len=8),  parameter :: model_QFACTOR   = 'Qfactor'
  
  !! list of viscosity name 
  character(len=16),  parameter :: model_visco_KF    = 'Kolsky-Futterman'
  character(len=12),  parameter :: model_visco_KV    = 'Kelvin-Voigt'
  character(len=9) ,  parameter :: model_visco_CC    = 'Cole-Cole'
  character(len=18),  parameter :: model_visco_Zener = 'Zener-model_simple'
  character(len=7) ,  parameter :: model_visco_Maxw  = 'Maxwell'
  character(len=9) ,  parameter :: model_visco_plaw  = 'power-law'
  character(len=14),  parameter :: model_visco_Szabo = 'modified-Szabo'
  character(len=3) ,  parameter :: model_visco_KSB   = 'KSB'
  
  private
  public  :: model_VP, model_RHO, model_VISCOUS, model_formalism
  public  :: model_BULK, model_IMP, model_FPOWER, model_TAU_EPS
  public  :: model_TAU_SIGMA, model_QFACTOR
  public  :: model_visco_KF, model_visco_KV, model_visco_CC
  public  :: model_visco_Zener, model_visco_Maxw, model_visco_plaw
  public  :: model_visco_Szabo, model_visco_KSB  

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Formalise models name with appropriate TAG for HELMHOLTZ ACOUSTIC
  !
  !> @param[in]    dm             : dimension
  !> @param[in]    flag_viscous   : indicates if viscosity
  !> @param[in]    viscous_model  : viscosity model name
  !> @param[in]    n_model        : number of physical model parameter
  !> @param[in]    name_model     : name of physical model parameter
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_formalism(dm,flag_viscous,viscous_model,n_model,     &
                             name_model_in,name_model_out,ctx_err)
    implicit none

    integer                            ,intent(in)   :: dm
    logical                            ,intent(in)   :: flag_viscous
    character(len=32)                  ,intent(in)   :: viscous_model
    integer                            ,intent(in)   :: n_model
    character(len=32),allocatable      ,intent(in)   :: name_model_in (:)
    character(len=32),allocatable      ,intent(inout):: name_model_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer    :: k,l
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(dm < 1 .or. dm > 3) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helmholtz equation in acoustic medium "      //  &
                      "[model_formalism] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    
    !! check and count models
    if(flag_viscous) then
      if(n_model < 3 .or. n_model > 6) then
        ctx_err%msg   = "** ERROR: INCORRECT NUMBER OF MODEL   " //  &
                        "[model_formalism] **"
        ctx_err%ierr  = -1
      end if
      if(trim(adjustl(viscous_model)) .ne. model_visco_KF       .and.   &
         trim(adjustl(viscous_model)) .ne. model_visco_KV       .and.   &
         trim(adjustl(viscous_model)) .ne. model_visco_Zener    .and.   &
         trim(adjustl(viscous_model)) .ne. model_visco_Maxw     .and.   &
         trim(adjustl(viscous_model)) .ne. model_visco_CC       .and.   &
         trim(adjustl(viscous_model)) .ne. model_visco_Szabo    .and.   &
         trim(adjustl(viscous_model)) .ne. model_visco_KSB      .and.   &
         trim(adjustl(viscous_model)) .ne. model_visco_plaw)  then
        ctx_err%msg   = "** ERROR: viscosity model for helmholtz "// &
                        "equation in acoustic medium not "        // &
                        "recognized [model_formalism] **"
        ctx_err%ierr  = -1
      end if
    else
      if(n_model .ne. 2) then
        ctx_err%msg   = "** ERROR: INCORRECT NUMBER OF MODEL   " //  &
                        "[model_formalism] **"
        ctx_err%ierr  = -1
      end if
    endif
    
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if

    !! retrieve tag for all models
    do k=1,n_model
      select case(trim(adjustl(name_model_in(k))))
        case('velocity','vp','Velocity','Vp','VP','Cp','c','cp','VELOCITY','CP')
          name_model_out(k) = model_VP
        case('rho','RHO','Rho','DENSITY','Density','density')
          name_model_out(k) = model_RHO
        case('bulk','BULK','Bulk','bulk-modulus','BULK-MODULUS','kappa', &
             'Bulk-Modulus','Kappa','KAPPA')
          name_model_out(k) = model_BULK
        case('impedance','Impedance','IMPEDANCE')
          name_model_out(k) = model_IMP
        case('viscosity','Viscosity','VISCOSITY','Visco','visco','VISCO')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if          
          name_model_out(k) = model_VISCOUS
        case('tau-sigma','TAU-SIGMA','Tau-sigma')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if          
          name_model_out(k) = model_TAU_SIGMA
        case('tau-eps','TAU-EPS','Tau-eps','tau-epsilon','TAU-EPSILON', &
             'Tau-epsilon')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          name_model_out(k) = model_TAU_EPS
        case('frequency-power','freq-power','FREQ-POWER','Freq-Power',  &
             'attennuation-freq-power', 'attennuation-frequency-power')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          name_model_out(k) = model_FPOWER
        case('QFACTOR','Qfactor','qfactor')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          name_model_out(k) = model_QFACTOR
        case default 
          ctx_err%msg   = "** ERROR: model name "         // &
                          trim(adjustl(name_model_in(k))) // &
                          " in acoustic not "             // &
                          "recognized [model_formalism] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      
      !! -----------------------------------------------------------------
      !! need to check that 
      !!   1) it is different from any other one 
      !!   2) in case of viscosity, one of the model must be the viscosity
      !! -----------------------------------------------------------------
      do l=1,k-1
        if(trim(adjustl(name_model_out(k))) == &
           trim(adjustl(name_model_out(l)))) then
          ctx_err%msg   = "** ERROR: acoustic model "      //           &
                          trim(adjustl(name_model_out(k))) //           &
                          " is repeated twice [model_formalism] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)        
        endif
      end do
    end do

    !! -----------------------------------------------------------------
    !! check that viscosity if present
    !! -----------------------------------------------------------------
    if(flag_viscous) then
      ctx_err%ierr=-1
      select case(trim(adjustl(viscous_model)))
        case(model_visco_CC)
          if((any(name_model_out == model_FPOWER)         .and.         &
              any(name_model_out == model_TAU_SIGMA)      .and.         &
              any(name_model_out == model_TAU_EPS)))   ctx_err%ierr=0
        case(model_visco_Zener)
          if((any(name_model_out == model_TAU_SIGMA)      .and.         &
              any(name_model_out == model_TAU_EPS)))   ctx_err%ierr=0
        case(model_visco_KV)
          if((any(name_model_out == model_TAU_EPS)))   ctx_err%ierr=0
        case(model_visco_Maxw,model_visco_KF)
          if((any(name_model_out == model_VISCOUS)))   ctx_err%ierr=0
        case(model_visco_plaw)
          if((any(name_model_out == model_VISCOUS ) .and.               &
              any(name_model_out == model_FPOWER  )))  ctx_err%ierr=0
        case(model_visco_Szabo)
          if((any(name_model_out == model_VISCOUS)      .and.           &
              any(name_model_out == model_FPOWER)))   ctx_err%ierr=0
        case(model_visco_KSB)
          if((any(name_model_out == model_VISCOUS)      .and.           &
              any(name_model_out == model_TAU_EPS)      .and.           & 
              any(name_model_out == model_FPOWER)))   ctx_err%ierr=0
        case default

      end select
            
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: viscoacoustic requires all of the "//&
                        "considered viscosity parameters [model_formalism] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine model_formalism

end module m_model_list
