!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_parameter_gradient.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the routine to convert the reference gradient
!> towards the gradient w.r.t the wanted physical parameters for the
!> HELMHOLTZ ACOUSTIC EQUATION.
!
!------------------------------------------------------------------------------
module m_parameter_gradient

  !! module used -------------------------------------------------------
  use m_define_precision,         only: IKIND_MESH
  use m_raise_error,              only: raise_error, t_error
  use m_ctx_model,                only: t_model
  use m_model_eval,               only: model_eval_piecewise_constant,  &
                                        model_eval_dof_index
  use m_model_list
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,tag_MODEL_DOF
  !! -------------------------------------------------------------------
  implicit none

  private
  public   :: parameter_gradient

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical parameter
  !> for the HELMHOLTZ ACOUSTIC propagation
  !> The input gradient is given w.r.t:
  !>  a) 1/kappa
  !>  b) rho
  !>  c) viscosity
  !! -------------------------------------------------------------------
  !> we have the followin relation
  !>   cp = sqrt(kappa / rho)
  !>  imp = sqrt(kappa * rho)
  !! -----------------------------
  !> for the derivative we have the
  !>  \f$ \partial_m1 \tilde{J} (m1, m2) =\partial_K J \partial_{m1} K 
  !>    + \partial_\rho J \partial_{m1} \rho  \f$
  !>
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    freq           : angular frequency
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_pseudoh : pseudo-hessian need power squared
  !----------------------------------------------------------------------------
  subroutine parameter_gradient(ctx_model,grad_in,grad_out,str_model,   &
                                freq,ctx_err,o_flag_pseudoh)
    implicit none

    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    logical         ,optional          ,intent(in)   :: o_flag_pseudoh
    !! 
    integer    :: power
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    if(abs(freq) > tiny(1.0)) then
      !! frequency is not needed here, we just avoid warning at 
      !! compilation
    end if 
    
    
    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power = 2
    end if
    !! 
    select case (trim(adjustl(ctx_model%format_disc))) 
      case(tag_MODEL_PCONSTANT) 
        call parameter_gradient_pconstant(ctx_model,grad_in,grad_out,   &
                                          str_model,power,ctx_err)
      case(tag_MODEL_DOF) 
        call parameter_gradient_dof(ctx_model,grad_in,grad_out,         &
                                    str_model,power,ctx_err)
      case default
        ctx_err%msg   = "** ERROR: only piecewise-constant or dof are"//&
                        " supported to parametrize the gradient " //    &
                        " [parameter_gradient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine parameter_gradient
    
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical parameter
  !> for the HELMHOLTZ ACOUSTIC propagation
  !> The input gradient is given w.r.t:
  !>  a) 1/kappa
  !>  b) rho
  !>  c) viscosity
  !! -------------------------------------------------------------------
  !> we have the followin relation
  !> \f$  cp = \sqrt{\kappa / \rho}\f$
  !> \f$ imp = \sqrt{\kappa * \rho}\f$
  !! -----------------------------
  !> for the derivative we have the
  !>  \f$ \partial_m1 \tilde{J} (m1, m2) =\partial_K J \partial_{m1} K 
  !>    + \partial_\rho J \partial_{m1} \rho  \f$
  !>
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    power          : power = 2 if pseudo-hessian
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parameter_gradient_pconstant(ctx_model,grad_in,grad_out,   &
                                          str_model,power,ctx_err)
    implicit none

    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    integer                            ,intent(in)   :: power
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)              :: imp,cp,rho,kappa,gradkappa
    real(kind=8),allocatable  :: gradc(:),gradr(:),gradk(:),gradi(:)
    integer                   :: k,n_coeff_loc
    integer(kind=IKIND_MESH)  :: c
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(ctx_model%dim_domain < 1 .and. ctx_model%dim_domain > 3) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helmholtz equation in acoustic medium "      //  &
                      "[parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! *****************************************************************
    !! USING PIECEWISE-CONSTANT MODEL REPRESENTATION 
    !! *****************************************************************
    n_coeff_loc = ctx_model%n_cell_loc
    !! *****************************************************************

    !! =================================================================
    !! 
    !! in the case of attenuation 
    !!
    !! =================================================================
    if(ctx_model%flag_viscous) then
      !! only deal with the attenuation models here
      do k=1,ctx_model%n_model
        select case(trim(adjustl(str_model(k))))
          !! -----------------------------------------------------------
          case(model_VISCOUS)
            !! only some visous models allows it
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KF,model_visco_Maxw,model_visco_plaw,    &
                   model_visco_KSB,model_visco_Szabo)
                grad_out(:,k) =grad_in(:,3)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_EPS)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV   )
                grad_out(:,k) =grad_in(:,3)
              case(model_visco_CC   ,model_visco_Zener, model_visco_KSB)
                grad_out(:,k) =grad_in(:,4)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_SIGMA)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC   ,model_visco_Zener)
                grad_out(:,k) =grad_in(:,3)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          
          !! -----------------------------------------------------------
          case(model_FPOWER)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC,model_visco_KSB)
                grad_out(:,k) =grad_in(:,5)
              case(model_visco_plaw,model_visco_Szabo)
                grad_out(:,k) =grad_in(:,4)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
        
        end select !! end for the current model
      end do
    end if
    !! =================================================================

    !! -----------------------------------------------------------------
    !! if we want (kappa,rho) then we can leave it is ok
    if(any(str_model == model_BULK) .and. any(str_model == model_RHO)) then
      !! we already have the gradient then
      do k=1,ctx_model%n_model
        if(ctx_err%ierr .ne. 0) cycle !! non-shared error
        
        select case(trim(adjustl(str_model(k))))
          case(model_BULK)
            !! we only convert 1/bulk to bulk
            do c=1,ctx_model%n_cell_loc !! piecewise-constant here 
              if(ctx_err%ierr .ne. 0) cycle !! non-shared error

              !! we use the RAW models 
              call model_eval_piecewise_constant(ctx_model,model_BULK,c,&
                                                 kappa,ctx_err)
              if(ctx_err%ierr .ne. 0) cycle
              
              grad_out(c,k) =real(grad_in(c,1) * (-1.d0/(kappa)**2)**power)
            end do

          case(model_RHO)
            grad_out(:,k) = grad_in(:,2)

          case(model_VISCOUS,model_TAU_SIGMA,model_TAU_EPS,model_FPOWER)
            !! attenuation has already been done at the beginning.

          case default
            ctx_err%msg   = "** ERROR: Unrecognized model name "    //  &
                            "[parameter_gradient] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end do

    !! -----------------------------------------------------------------
    !! else we need to compute the appropriate scalings
    else
      !! PARAMETRIZATION:   (BULK, CP) ---------------------------------
      !! \rho = \kappa / cp²
      !!     (o) \partial_K \rho = 1/cp²
      !!     (o) \partial_C \rho =-2 kappa /cp³
      if(any(str_model == model_BULK) .and. any(str_model == model_VP)) then
        allocate(gradc(n_coeff_loc))
        allocate(gradk(n_coeff_loc))
        gradc=0.0
        gradk=0.0
        do c=1,ctx_model%n_cell_loc !! piecewise-constant here 
          if(ctx_err%ierr .ne. 0) cycle

          !! we use the RAW models 
          call model_eval_piecewise_constant(ctx_model,model_BULK,c,    &
                                             kappa,ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_VP  ,c,cp, &
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle

          !! get back the derivative w.r.t. kappa
          gradkappa = grad_in(c,1)*(-1./(kappa**2))
          gradk(c) = dble(gradkappa +grad_in(c,2)*( 1./(cp*cp)      )**power)
          gradc(c) = dble(          +grad_in(c,2)*(-2.*kappa/(cp**3))**power)
        end do

      !! PARAMETRIZATION:   (BULK, IMP) --------------------------------
      !! \rho = I² / \kappa
      !!     (o) \partial_K \rho = -I²/\kappa²
      !!     (o) \partial_I \rho =  2I/\kappa
      elseif(any(str_model==model_BULK) .and. any(str_model==model_IMP)) then
        allocate(gradi(n_coeff_loc))
        allocate(gradk(n_coeff_loc))
        gradi=0.0
        gradk=0.0
        do c=1,n_coeff_loc
          if(ctx_err%ierr .ne. 0) cycle

          !! we use the RAW models 
          call model_eval_piecewise_constant(ctx_model,model_BULK,c,    &
                                             kappa,ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_IMP ,c,imp,&
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle

          !! get back the derivative w.r.t. kappa
          gradkappa = grad_in(c,1)*(-1./(kappa**2))
          gradk(c) = dble(gradkappa+grad_in(c,2) * &
                         (-imp*imp/(kappa*kappa))**power)
          gradi(c) = dble(         +grad_in(c,2) * &
                         (2*imp/kappa           )**power)
        end do

      !! PARAMETRIZATION:   (CP, RHO) ----------------------------------
      !! \kappa = cp² * rho
      !!     (o) \partial_R \kappa = cp²
      !!     (o) \partial_C \kappa = 2 cp rho
      elseif(any(str_model==model_VP) .and. any(str_model==model_RHO)) then
        allocate(gradc(n_coeff_loc))
        allocate(gradr(n_coeff_loc))
        gradc=0.0
        gradr=0.0
        do c=1,n_coeff_loc
          if(ctx_err%ierr .ne. 0) cycle

          !! we use the RAW models 
          call model_eval_piecewise_constant(ctx_model,model_BULK,c,    &
                                             kappa,ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_VP  ,c,cp, &
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_RHO ,c,rho,&
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle

          !imp  = dsqrt(kappa*rho)
          !! get back the derivative w.r.t. kappa
          gradkappa = grad_in(c,1)*(-1./(kappa**2))
          gradc(c) = dble(gradkappa*(2*cp*rho)**power               )
          gradr(c) = dble(gradkappa*(   cp*cp)**power + grad_in(c,2))
        end do

      !! PARAMETRIZATION:   (IMP, RHO) ---------------------------------
      !! \kappa = I² / rho
      !!     (o) \partial_R \kappa = -I²/rho²
      !!     (o) \partial_I \kappa = 2I /rho
      elseif(any(str_model==model_IMP) .and. any(str_model==model_RHO)) then
        allocate(gradi(n_coeff_loc))
        allocate(gradr(n_coeff_loc))
        gradi=0.0
        gradr=0.0
        do c=1,n_coeff_loc
          !! we use the RAW models 
          call model_eval_piecewise_constant(ctx_model,model_BULK,c,    &
                                             kappa,ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_IMP ,c,imp,&
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_RHO ,c,rho,&
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle

          !! get back the derivative w.r.t. kappa
          gradkappa = grad_in(c,1)*(-1./(kappa**2))
          gradi(c) = dble(gradkappa*(         2*imp/rho)**power               )
          gradr(c) = dble(gradkappa*(-imp*imp/(rho*rho))**power + grad_in(c,2))
        end do

      !! PARAMETRIZATION:   (IMP, CP) ----------------------------------
      !!  rho = I / cp
      !! kappa= I cp
      !!     (o) \partial_C \kappa = I      (o) \partial_C \rho =-I/cp²
      !!     (o) \partial_I \kappa = cp     (o) \partial_I \rho = 1/cp
      elseif(any(str_model==model_VP) .and. any(str_model==model_IMP)) then
        allocate(gradi(n_coeff_loc))
        allocate(gradc(n_coeff_loc))
        gradi=0.0
        gradc=0.0
        do c=1,n_coeff_loc
          !! we use the RAW models 
          call model_eval_piecewise_constant(ctx_model,model_BULK,c,    &
                                             kappa,ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_VP  ,c,cp, &
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle
          
          call model_eval_piecewise_constant(ctx_model,model_IMP ,c,imp,&
                                             ctx_err)
          if(ctx_err%ierr .ne. 0) cycle

          !! get back the derivative w.r.t. kappa
          gradkappa = grad_in(c,1)*(-1./(kappa**2))
          gradc(c) = dble(gradkappa   *(imp)**power + &
                          grad_in(c,2)*(-imp/(cp*cp))**power)
          gradi(c) = dble(gradkappa   *(cp )**power + &
                          grad_in(c,2)*(   1/cp)**power)
        end do

      else
        ctx_err%msg   = "** ERROR: Unrecognized physical " //           &
                        "parametrization "                 //           &
                        "[parameter_gradient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if

      !! get the output gradient >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      do k=1,ctx_model%n_model
        select case(trim(adjustl(str_model(k))))
          case(model_BULK)
            grad_out(:,k) = real(gradk(:))
          case(model_RHO)
            grad_out(:,k) = real(gradr(:))
          case(model_VP)
            grad_out(:,k) = real(gradc(:))
          case(model_IMP)
            grad_out(:,k) = real(gradi(:))
          case(model_VISCOUS,model_TAU_SIGMA,model_TAU_EPS,model_FPOWER)
            !! attenuation has already been done at the beginning.
          
          case default
            ctx_err%msg   = "** ERROR: Unrecognized model name "    //  &
                            "[parameter_gradient] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end do
      ! ----------------------------------------------------------------

      if(allocated(gradc)) deallocate(gradc)
      if(allocated(gradr)) deallocate(gradr)
      if(allocated(gradk)) deallocate(gradk)
      if(allocated(gradi)) deallocate(gradi)
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine parameter_gradient_pconstant

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical parameter
  !> for the HELMHOLTZ ACOUSTIC propagation
  !> The input gradient is given w.r.t:
  !>  a) 1/kappa
  !>  b) rho
  !>  c) viscosity
  !! -------------------------------------------------------------------
  !> we have the followin relation
  !> \f$  cp = \sqrt{\kappa / \rho}\f$
  !> \f$ imp = \sqrt{\kappa * \rho}\f$
  !! -----------------------------
  !> for the derivative we have the
  !>  \f$ \partial_m1 \tilde{J} (m1, m2) =\partial_K J \partial_{m1} K 
  !>    + \partial_\rho J \partial_{m1} \rho  \f$
  !>
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    power          : power = 2 if pseudo-hessian
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parameter_gradient_dof(ctx_model,grad_in,grad_out,         &
                                    str_model,power,ctx_err)
    implicit none

    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    integer                            ,intent(in)   :: power
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)              :: imp,cp,rho,kappa,gradkappa
    real(kind=8),allocatable  :: gradc(:),gradr(:),gradk(:),gradi(:)
    integer                   :: k,n_coeff_loc
    integer(kind=IKIND_MESH)  :: c, ind_gb, myind
    integer(kind=4)           :: k_model
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(ctx_model%dim_domain < 1 .and. ctx_model%dim_domain > 3) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helmholtz equation in acoustic medium "      //  &
                      "[parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! *****************************************************************
    !! USING DOF MODEL REPRESENTATION
    !! *****************************************************************
    n_coeff_loc = ctx_model%vp%param_dof%ndof*ctx_model%n_cell_loc
    !! *****************************************************************

    !! =================================================================
    !! 
    !! in the case of attenuation 
    !!
    !! =================================================================
    if(ctx_model%flag_viscous) then
      !! only deal with the attenuation models here
      do k=1,ctx_model%n_model
        select case(trim(adjustl(str_model(k))))
          !! -----------------------------------------------------------
          case(model_VISCOUS)
            !! only some visous models allows it
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KF,model_visco_Maxw,model_visco_plaw,    &
                   model_visco_KSB, model_visco_Szabo)
                grad_out(:,k) =grad_in(:,3)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_EPS)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV   )
                grad_out(:,k) =grad_in(:,3)
              case(model_visco_CC   ,model_visco_Zener, model_visco_KSB)
                grad_out(:,k) =grad_in(:,4)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_SIGMA)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC   ,model_visco_Zener)
                grad_out(:,k) =grad_in(:,3)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          
          !! -----------------------------------------------------------
          case(model_FPOWER)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC,model_visco_KSB)
                grad_out(:,k) =grad_in(:,5)
              case(model_visco_plaw,model_visco_Szabo)
                grad_out(:,k) =grad_in(:,4)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
        
        end select !! end for the current model
      end do
    end if
    !! =================================================================

    !! -----------------------------------------------------------------
    !! if we want (kappa,rho) then we can leave it is ok
    if(any(str_model == model_BULK) .and. any(str_model == model_RHO)) then
      !! we already have the gradient then
      do k=1,ctx_model%n_model
        if(ctx_err%ierr .ne. 0) cycle !! non-shared error
        
        select case(trim(adjustl(str_model(k))))
          case(model_BULK)
            !! we only convert 1/bulk to bulk
            do c=1,ctx_model%n_cell_loc !! each cell has several dof
              ind_gb = (c-1)*ctx_model%vp%param_dof%ndof
              if(ctx_err%ierr .ne. 0) cycle !! non-shared error

              !! we use the RAW models
              do k_model=1,ctx_model%vp%param_dof%ndof
                call model_eval_dof_index(ctx_model,c,model_BULK,k_model,kappa,ctx_err)
                if(ctx_err%ierr .ne. 0) cycle
                grad_out(ind_gb+k_model,k) =                            &
                real(grad_in(ind_gb+k_model,1) * (-1.d0/(kappa)**2)**power)
              end do
            end do

          case(model_RHO)
            grad_out(:,k) = grad_in(:,2)

          case(model_VISCOUS,model_TAU_SIGMA,model_TAU_EPS,model_FPOWER)
            !! attenuation has already been done at the beginning.

          case default
            ctx_err%msg   = "** ERROR: Unrecognized model name "    //  &
                            "[parameter_gradient] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end do

    !! -----------------------------------------------------------------
    !! else we need to compute the appropriate scalings
    else

      !! PARAMETRIZATION:   (BULK, CP) ---------------------------------
      !! \rho = \kappa / cp²
      !!     (o) \partial_K \rho = 1/cp²
      !!     (o) \partial_C \rho =-2 kappa /cp³
      if(any(str_model == model_BULK) .and. any(str_model == model_VP)) then
        allocate(gradc(n_coeff_loc))
        allocate(gradk(n_coeff_loc))
        gradc=0.0
        gradk=0.0
        do c=1,ctx_model%n_cell_loc !! each cell has several dof
          if(ctx_err%ierr .ne. 0) cycle

          ind_gb = (c-1)*ctx_model%vp%param_dof%ndof
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
 
          !! we use the RAW models
          do k_model=1,ctx_model%vp%param_dof%ndof
            call model_eval_dof_index(ctx_model,c,model_BULK,k_model,   &
                                      kappa,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_VP,k_model,cp,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle

            !! get back the derivative w.r.t. kappa
            myind = ind_gb+k_model
            gradkappa = grad_in(myind,1)*(-1./(kappa**2))
            gradk(myind)=dble(gradkappa +grad_in(myind,2)*( 1./(cp*cp))**power)
            gradc(myind)=dble(grad_in(myind,2)*(-2.*kappa/(cp**3))**power)

          end do
        end do

      !! PARAMETRIZATION:   (BULK, IMP) --------------------------------
      !! \rho = I² / \kappa
      !!     (o) \partial_K \rho = -I²/\kappa²
      !!     (o) \partial_I \rho =  2I/\kappa
      elseif(any(str_model==model_BULK) .and. any(str_model==model_IMP)) then
        allocate(gradi(n_coeff_loc))
        allocate(gradk(n_coeff_loc))
        gradi=0.0
        gradk=0.0
        do c=1,ctx_model%n_cell_loc !! each cell has several dof
          if(ctx_err%ierr .ne. 0) cycle

          ind_gb = (c-1)*ctx_model%vp%param_dof%ndof
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
  
          !! we use the RAW models
          do k_model=1,ctx_model%vp%param_dof%ndof
            call model_eval_dof_index(ctx_model,c,model_BULK,k_model,   &
                                      kappa,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_IMP ,k_model,   &
                                      imp,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
      
            !! get back the derivative w.r.t. kappa
            myind = ind_gb+k_model
            !! get back the derivative w.r.t. kappa
            gradkappa = grad_in(myind,1)*(-1./(kappa**2))
            gradk(myind) = dble(gradkappa+grad_in(myind,2) * &
                               (-imp*imp/(kappa*kappa))**power)
            gradi(myind) = dble(         +grad_in(myind,2) * &
                               (2*imp/kappa           )**power)

          end do
        end do

      !! PARAMETRIZATION:   (CP, RHO) ----------------------------------
      !! \kappa = cp² * rho
      !!     (o) \partial_R \kappa = cp²
      !!     (o) \partial_C \kappa = 2 cp rho
      elseif(any(str_model==model_VP) .and. any(str_model==model_RHO)) then
        allocate(gradc(n_coeff_loc))
        allocate(gradr(n_coeff_loc))
        gradc=0.0
        gradr=0.0
        do c=1,ctx_model%n_cell_loc !! each cell has several dof
          ind_gb = (c-1)*ctx_model%vp%param_dof%ndof
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
  
          !! we use the RAW models
          do k_model=1,ctx_model%vp%param_dof%ndof
            call model_eval_dof_index(ctx_model,c,model_BULK,k_model,   &
                                      kappa,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_VP ,k_model,    &
                                      cp,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_RHO,k_model,    &
                                      rho,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
      
            !! get back the derivative w.r.t. kappa
            myind = ind_gb+k_model
            !! get back the derivative w.r.t. kappa
            gradkappa = grad_in(myind,1)*(-1./(kappa**2))
            gradc(myind) = dble(gradkappa*(2*cp*rho)**power)
            gradr(myind) = dble(gradkappa*(   cp*cp)**power + grad_in(myind,2))

          end do
        end do

      !! PARAMETRIZATION:   (IMP, RHO) ---------------------------------
      !! \kappa = I² / rho
      !!     (o) \partial_R \kappa = -I²/rho²
      !!     (o) \partial_I \kappa = 2I /rho
      elseif(any(str_model==model_IMP) .and. any(str_model==model_RHO)) then
        allocate(gradi(n_coeff_loc))
        allocate(gradr(n_coeff_loc))
        gradi=0.0
        gradr=0.0
        do c=1,ctx_model%n_cell_loc !! each cell has several dof
          if(ctx_err%ierr .ne. 0) cycle

          ind_gb = (c-1)*ctx_model%vp%param_dof%ndof
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
  
          !! we use the RAW models
          do k_model=1,ctx_model%vp%param_dof%ndof
            call model_eval_dof_index(ctx_model,c,model_BULK,k_model,kappa,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_IMP ,k_model,imp,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_RHO ,k_model,rho,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
          
            !! get back the derivative w.r.t. kappa
            myind = ind_gb+k_model
            !! get back the derivative w.r.t. kappa
            gradkappa = grad_in(myind,1)*(-1./(kappa**2))
            gradi(myind) = dble(gradkappa*(         2*imp/rho)**power)
            gradr(myind) = dble(gradkappa*(-imp*imp/(rho*rho))**power + &
                                grad_in(myind,2))
          end do
        end do

      !! PARAMETRIZATION:   (IMP, CP) ----------------------------------
      !!  rho = I / cp
      !! kappa= I cp
      !!     (o) \partial_C \kappa = I      (o) \partial_C \rho =-I/cp²
      !!     (o) \partial_I \kappa = cp     (o) \partial_I \rho = 1/cp
      elseif(any(str_model==model_VP) .and. any(str_model==model_IMP)) then
        allocate(gradi(n_coeff_loc))
        allocate(gradc(n_coeff_loc))
        gradi=0.0
        gradc=0.0
        do c=1,ctx_model%n_cell_loc !! each cell has several dof
          if(ctx_err%ierr .ne. 0) cycle

          ind_gb = (c-1)*ctx_model%vp%param_dof%ndof
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
  
          !! we use the RAW models
          do k_model=1,ctx_model%vp%param_dof%ndof
            call model_eval_dof_index(ctx_model,c,model_BULK,k_model,kappa,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_IMP ,k_model,imp,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            call model_eval_dof_index(ctx_model,c,model_VP  ,k_model,cp,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
          
            !! get back the derivative w.r.t. kappa
            myind = ind_gb+k_model
            !! get back the derivative w.r.t. kappa
            gradkappa = grad_in(myind,1)*(-1./(kappa**2))
            gradc(myind) = dble(gradkappa   *(imp)**power + &
                            grad_in(myind,2)*(-imp/(cp*cp))**power)
            gradi(myind) = dble(gradkappa   *(cp )**power + &
                            grad_in(myind,2)*(   1.d0/cp)**power)

          end do
        end do

      else
        ctx_err%msg   = "** ERROR: Unrecognized physical " //           &
                        "parametrization "                 //           &
                        "[parameter_gradient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if

      !! get the output gradient >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      do k=1,ctx_model%n_model
        select case(trim(adjustl(str_model(k))))
          case(model_BULK)
            grad_out(:,k) = real(gradk(:))
          case(model_RHO)
            grad_out(:,k) = real(gradr(:))
          case(model_VP)
            grad_out(:,k) = real(gradc(:))
          case(model_IMP)
            grad_out(:,k) = real(gradi(:))
          case(model_VISCOUS,model_TAU_SIGMA,model_TAU_EPS,model_FPOWER)
            !! attenuation has already been done at the beginning.
          
          case default
            ctx_err%msg   = "** ERROR: Unrecognized model name "    //  &
                            "[parameter_gradient] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end do
      ! ----------------------------------------------------------------

      if(allocated(gradc)) deallocate(gradc)
      if(allocated(gradr)) deallocate(gradr)
      if(allocated(gradk)) deallocate(gradk)
      if(allocated(gradi)) deallocate(gradi)
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine parameter_gradient_dof

end module m_parameter_gradient
