!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_update.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the routine to update the model depending on 
!> the parametrization for HELMHOLTZ ACOUSTIC EQUATION.
!
!------------------------------------------------------------------------------
module m_model_update

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_define_precision,         only: IKIND_MESH
  use m_ctx_parallelism,          only: t_parallelism
  use m_ctx_model,                only: t_model
  use m_model_eval,               only: model_eval_piecewise_constant,  &
                                        model_eval_dof_index,           &
                                        model_get_piecewise_constant
  use m_model_list
  use m_grad_model_function,      only: apply_model_function_inv,       &
                                        apply_model_function
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT, tag_MODEL_DOF
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: model_update

  contains


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for ACOUSTIC, at the end we give back the (cp,rho).
  !> 
  !> we can work with cp, rho, bulk-modulus or the impedance.
  !> depending on the couple, we update accordingly the selected
  !> parameter, respecting given min and max values
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[in]    freq           : current frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_update(ctx_paral,ctx_model,search,alpha,param_current, &
                          tag_function,parametrization,freq,ctx_err)
    implicit none
   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case(trim(adjustl(ctx_model%format_disc)))
      case(tag_MODEL_PCONSTANT) 
        call model_update_pconstant(ctx_paral,ctx_model,search,alpha,   &
                                    param_current,tag_function,         &
                                    parametrization,freq,ctx_err)
      case(tag_MODEL_DOF) 
        call model_update_pconstant_dof(ctx_paral,ctx_model,search,     &
                                    alpha,param_current,tag_function,   &
                                    parametrization,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: only piecewise-constant model " //&
                        " supported [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return

  end subroutine model_update


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for piecewise-constant, at the end we give back the (cp,rho).
  !> 
  !> we can work with cp, rho, bulk-modulus or the impedance.
  !> depending on the couple, we update accordingly the selected
  !> parameter, respecting given min and max values
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[in]    freq           : current frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_update_pconstant(ctx_paral,ctx_model,search,alpha,   &
                                    param_current,tag_function,         &
                                    parametrization,freq,ctx_err)
    implicit none
   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ic, n_coeff_loc,im,nvisco
    real(kind=4),allocatable  :: model(:),bulk(:),imp(:)
    real(kind=4)              :: tol=tiny(1.0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0


    !! piecewise-constant representation *******************************
    n_coeff_loc = ctx_model%n_cell_loc
    !! *****************************************************************
    allocate(model(n_coeff_loc))
    model = 0.0
    !! -----------------------------------------------------------------
    !! 1) we get the parameter to be update
    !! PIECEWISE-CONSTANT **********************************************
    call model_get_piecewise_constant(ctx_model,model,                  & 
                                      trim(adjustl(param_current)),freq,&
                                      ctx_err)
    call apply_model_function(ctx_paral,model,tag_function,ctx_err)

    !! 2) we update model function
    model(:) = model(:) - alpha * search(:)
    !! 3) we get the identity model back
    call apply_model_function_inv(ctx_paral,model,tag_function,ctx_err)
    !! 4) we get (Vp,Rho) from the current selected parametrization

    !! -----------------------------------------------------------------
    !! depending on the parametrization we need to update
    !! the ctx_model which contains Vp and Rho
    !!
    !! -----------------------------------------------------------------


                !***************************************!
                ! Everything is piecewise-constant here !
                !                                       !
                !***************************************!

    !! =================================================================
    !! 
    !! in the case of attenuation first
    !!
    !! =================================================================
    if (trim(adjustl(param_current)) == model_VISCOUS   .or.              &
        trim(adjustl(param_current)) == model_TAU_EPS   .or.              &
        trim(adjustl(param_current)) == model_TAU_SIGMA .or.              &
        trim(adjustl(param_current)) == model_FPOWER) then
      if(ctx_model%flag_viscous) then
        !! only deal with the attenuation models here
        select case(trim(adjustl(param_current)))
          !! -----------------------------------------------------------
          case(model_VISCOUS)
            !! only some visous models allows it
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KF,model_visco_Maxw,model_visco_plaw,    &
                   model_visco_KSB,model_visco_Szabo)
                ctx_model%visco(1)%pconstant(:) = model(:)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_EPS)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV   )
                ctx_model%visco(1)%pconstant(:) = model(:)
              case(model_visco_CC   ,model_visco_Zener)
                !! validity condition tauEps > tauSIGMA
                do ic = 1, n_coeff_loc
                  if(model(ic) < ctx_model%visco(1)%pconstant(ic))      &
                     model(ic) = real(ctx_model%visco(1)%pconstant(ic))
                end do
                ctx_model%visco(2)%pconstant(:) = model(:)
              case(model_visco_KSB)
                ctx_model%visco(2)%pconstant(:) = model(:)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_SIGMA)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC   ,model_visco_Zener)
                !! validity condition tauEps > tauSIGMA
                do ic = 1, n_coeff_loc
                  if(model(ic) > ctx_model%visco(2)%pconstant(ic))      &
                     model(ic) = real(ctx_model%visco(2)%pconstant(ic))
                end do
                ctx_model%visco(1)%pconstant(:) = model(:)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          
          !! -----------------------------------------------------------
          case(model_FPOWER)
            !! force between 0 and 1. ----------------------------------
            do ic = 1, n_coeff_loc
              if(model(ic) <= 0) model(ic) = tiny(1.0)
              if(model(ic) >  1) model(ic) = 1.
            end do

            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC,model_visco_KSB)
                ctx_model%visco(3)%pconstant(:) = model(:)
              case(model_visco_plaw,model_visco_Szabo)
                ctx_model%visco(2)%pconstant(:) = model(:)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
        
        end select !! end for the current model
      else
        ctx_err%msg   = "** ERROR: Inconsistent use of  acoustic " //   &
                        "models for attenuation [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
      end if !! end if attenuation 
    !! =================================================================

    !! =================================================================    
    !! for the non-attenuated model, it depends on the parametrization
    else
      !! load models in case 
      allocate(bulk(n_coeff_loc))
      allocate(imp (n_coeff_loc))
      bulk = 0.0
      imp  = 0.0
      call model_get_piecewise_constant(ctx_model,bulk,model_BULK,freq,ctx_err)
      call model_get_piecewise_constant(ctx_model,imp ,model_IMP ,freq,ctx_err)

      ! ----------------------------------------------------------------
      !! parametrization Vp,Rho
      if(any(parametrization == model_RHO) .and.                        &
         any(parametrization == model_VP))  then
        select case(trim(adjustl(param_current)))
          case(model_RHO)
            ctx_model%rho%pconstant(:) = model
          case(model_VP)
            ctx_model%vp%pconstant(:)  = model
          case default
            ctx_err%ierr = -1
        end select
      ! ----------------------------------------------------------------
      !! parametrization Vp,Bulk
      !! Rho = K / Vp²
      elseif(any(parametrization == model_BULK) .and.                   &
             any(parametrization == model_VP))  then
        select case(trim(adjustl(param_current)))
          case(model_BULK)
            ctx_model%rho%pconstant(:) = model / ctx_model%vp%pconstant**2
          case(model_VP)
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%rho%pconstant(ic) = bulk(ic) / model(ic)**2
              else
                ctx_model%rho%pconstant(ic) = ctx_model%rho_max
              end if
            end do
            ctx_model%vp%pconstant  = model
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Vp,Imp
      !! Rho = I / Vp
      elseif(any(parametrization == model_IMP) .and.                    &
             any(parametrization == model_VP))  then
        select case(trim(adjustl(param_current)))
          case(model_IMP)
            ctx_model%rho%pconstant = model / ctx_model%vp%pconstant
          case(model_VP)
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%rho%pconstant(ic) = imp(ic) / model(ic)
              else
                ctx_model%rho%pconstant(ic) = ctx_model%rho_max
              end if
            end do
            ctx_model%vp%pconstant  = model
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Bulk,Rho
      !! Vp = sqrt(Kappa/Rho)
      elseif(any(parametrization == model_BULK) .and.                   &
             any(parametrization == model_RHO))  then
        select case(trim(adjustl(param_current)))
          case(model_BULK)
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%vp%pconstant(ic)  = sqrt(model(ic) /          &
                                              ctx_model%rho%pconstant(ic))
              else
                ctx_model%vp%pconstant(ic) = ctx_model%vp_min
              end if
            end do
          case(model_RHO)
            ctx_model%vp%pconstant  = sqrt(bulk / model)
            ctx_model%rho%pconstant = model
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Bulk,Imp
      !! Vp = Kappa/Imp
      !! Rho= I² / Kappa
      elseif(any(parametrization == model_BULK) .and.                   &
             any(parametrization == model_IMP))  then
        select case(trim(adjustl(param_current)))
          case(model_BULK)
            ctx_model%vp%pconstant  = model  / imp
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%rho%pconstant(ic) = imp(ic)**2 / model(ic)
              else
                ctx_model%rho%pconstant(ic) = ctx_model%rho_max
              end if
            end do
            
          case(model_IMP)
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%vp%pconstant(ic) = bulk(ic)    / model(ic)
              else
                ctx_model%vp%pconstant(ic) = ctx_model%vp_max
              end if
            end do
            ctx_model%rho%pconstant = model**2/ bulk
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Imp, Rho
      !! Vp = Imp/Rho
      elseif(any(parametrization == model_RHO) .and.                    &
             any(parametrization == model_IMP))  then
        select case(trim(adjustl(param_current)))
          case(model_RHO)
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%vp%pconstant(ic) = imp(ic) / model(ic)
              else
                ctx_model%vp%pconstant(ic) = ctx_model%vp_max
              end if
            end do
            ctx_model%rho%pconstant = model
          case(model_IMP)
            ctx_model%vp%pconstant  = model  / ctx_model%rho%pconstant
          case default
            ctx_err%ierr = -1
        end select
      
      ! ----------------------------------------------------------------
      !! Error
      else
        ctx_err%msg   = "** ERROR: Unrecognized model name [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      deallocate(bulk)
      deallocate(imp)
    end if
    ! ----------------------------------------------------------------
    deallocate(model)

    !! in case of error ------------------------------------------------
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: Unrecognized parametrization [model_update] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 

    !! in case of error ------------------------------------------------
    !! if everything is ok so far, we have to adjust min and max values
    !! -----------------------------------------------------------------
    do ic = 1, n_coeff_loc
      if(ctx_model%vp%pconstant(ic) < ctx_model%vp_min)                 &
         ctx_model%vp%pconstant(ic) = ctx_model%vp_min
      if(ctx_model%vp%pconstant(ic) > ctx_model%vp_max)                 &
         ctx_model%vp%pconstant(ic) = ctx_model%vp_max
      if(ctx_model%rho%pconstant(ic)< ctx_model%rho_min)                &
         ctx_model%rho%pconstant(ic)= ctx_model%rho_min
      if(ctx_model%rho%pconstant(ic)> ctx_model%rho_max)                &
         ctx_model%rho%pconstant(ic)= ctx_model%rho_max
    end do
    if(ctx_model%flag_viscous) then
      nvisco = size(ctx_model%visco)
      do im = 1, nvisco
        do ic = 1, n_coeff_loc
          if(ctx_model%visco(im)%pconstant(ic)< ctx_model%visco_min(im)) &
             ctx_model%visco(im)%pconstant(ic) =ctx_model%visco_min(im)
          if(ctx_model%visco(im)%pconstant(ic)> ctx_model%visco_max(im)) &
             ctx_model%visco(im)%pconstant(ic) =ctx_model%visco_max(im)
        end do
      end do
    end if 
    !! -----------------------------------------------------------------
    return
  end subroutine model_update_pconstant


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for model represented per dof, at the end we give back 
  !> the models (cp,rho).
  !> 
  !> we can work with cp, rho, bulk-modulus or the impedance.
  !> depending on the couple, we update accordingly the selected
  !> parameter, respecting given min and max values
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_update_pconstant_dof(ctx_paral,ctx_model,search,     &
                                        alpha,param_current,            &
                                        tag_function,parametrization,ctx_err)
    implicit none
   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ic, n_coeff_loc,im,nvisco
    integer                   :: n1, n2, n3
    real(kind=4),allocatable  :: model(:),bulk(:),imp(:),vp(:),rho(:)
    real(kind=4)              :: tol=tiny(1.0)
    real(kind=8)              :: average
    integer(kind=IKIND_MESH)  :: igb1, igb2, i_cell, ind_gb
    integer(kind=4)           :: npt_per_cell
    real(kind=8)              :: mval
    integer(kind=4)           :: k_model
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0


    !! dof model representation ****************************************
    !! it must be the same for all models
    n1 = size(search)
    n2 = ctx_model%vp%param_dof%ndof  *ctx_model%n_cell_loc
    n3 = ctx_model%rho%param_dof%ndof *ctx_model%n_cell_loc
    !! they must all be the same
    if(n1 .ne. n2 .or. n1 .ne. n3 .or. n2 .ne. n3) then
      ctx_err%msg   = "** ERROR: Inconsistent number of parameters " // &
                      "for the model representation [model_update] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    n_coeff_loc = n1
    npt_per_cell= ctx_model%vp%param_dof%ndof
    allocate(vp   (n_coeff_loc))
    allocate(rho  (n_coeff_loc))
    allocate(model(n_coeff_loc))
    model = 0.0
    !! *****************************************************************

    !! -----------------------------------------------------------------
    !! 1) we get the parameter to be update
    !! Using dof *******************************************************
    !! we manually get the vp and rho.
    do i_cell = 1, ctx_model%n_cell_loc
      igb1 = (i_cell-1)*npt_per_cell + 1
      igb2 =     igb1 + npt_per_cell - 1
      
      vp (igb1:igb2) = real(ctx_model%vp%param_dof%mval(i_cell)%array(:) )
      rho(igb1:igb2) = real(ctx_model%rho%param_dof%mval(i_cell)%array(:))
    end do
   
    select case(trim(adjustl(param_current)))
      case(model_VP)
        model = vp
      case(model_RHO)
        model = rho
      case default
        !! get model.
        do i_cell = 1, ctx_model%n_cell_loc !! each cell has several dof
          ind_gb = (i_cell-1)*ctx_model%vp%param_dof%ndof
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
    
          !! we use the RAW models
          do k_model=1,ctx_model%vp%param_dof%ndof
            call model_eval_dof_index(ctx_model,i_cell,                &
                                      trim(adjustl(param_current)),    &
                                      k_model,mval,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            model(ind_gb+k_model) = real(mval,kind=4)
          end do
        end do
    end select
    !! -----------------------------------------------------------------
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR during model_eval [model_update] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    call apply_model_function(ctx_paral,model,tag_function,ctx_err)

    !! 2) we update model function
    model(:) = model(:) - alpha * search(:)
    !! 3) we get the identity model back
    call apply_model_function_inv(ctx_paral,model,tag_function,ctx_err)
    !! 4) we get (Vp,Rho) from the current selected parametrization

    !! -----------------------------------------------------------------
    !! depending on the parametrization we need to update
    !! the ctx_model which contains Vp and Rho
    !!
    !! -----------------------------------------------------------------

    !! =================================================================
    !! 
    !! in the case of attenuation first
    !!
    !! =================================================================
    if (trim(adjustl(param_current)) == model_VISCOUS   .or.              &
        trim(adjustl(param_current)) == model_TAU_EPS   .or.              &
        trim(adjustl(param_current)) == model_TAU_SIGMA .or.              &
        trim(adjustl(param_current)) == model_FPOWER) then

      if(ctx_model%flag_viscous) then
        !! only deal with the attenuation models here
        select case(trim(adjustl(param_current)))
          !! -----------------------------------------------------------
          case(model_VISCOUS)
            !! only some visous models allows it
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KF,model_visco_Maxw,model_visco_plaw,    &
                   model_visco_KSB,model_visco_Szabo)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(1)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average =dble( 1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(1)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(1)%pconstant(i_cell) = average
                end do
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_EPS)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV   )
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(1)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(1)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(1)%pconstant(i_cell) = average
                end do

              case(model_visco_CC   ,model_visco_Zener)
                !! validity condition tauEps > tauSIGMA
                do i_cell = 1, ctx_model%n_cell_loc
                  do ic = 1, npt_per_cell
                    igb1 = (i_cell-1)*npt_per_cell + ic
                    if(model(igb1) < ctx_model%visco(1)%pconstant(i_cell)) then
                      model(igb1) = real(ctx_model%visco(1)%pconstant(i_cell))
                    end if
                  end do
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(2)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(2)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(2)%pconstant(i_cell) = average
                end do
              case(model_visco_KSB)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(2)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(2)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(2)%pconstant(i_cell) = average
                end do

              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! -----------------------------------------------------------
          case(model_TAU_SIGMA)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC   ,model_visco_Zener)
                !! validity condition tauEps > tauSIGMA
                do i_cell = 1, ctx_model%n_cell_loc
                  do ic = 1, npt_per_cell
                    igb1 = (i_cell-1)*npt_per_cell + ic
                    if(model(igb1) > ctx_model%visco(2)%pconstant(i_cell)) then
                       model(igb1) = real(ctx_model%visco(2)%pconstant(i_cell))
                    end if
                  end do
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(1)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(1)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(1)%pconstant(i_cell) = average
                end do
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          
          !! -----------------------------------------------------------
          case(model_FPOWER)
            !! force between 0 and 1. ----------------------------------
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                if(model(igb1) <= 0) model(igb1) = tiny(1.0)
                if(model(igb1) >  1) model(igb1) = 1.
              end do
            end do
 
 
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_CC,model_visco_KSB)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(3)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(3)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(3)%pconstant(i_cell) = average
                end do
                
              case(model_visco_plaw,model_visco_Szabo)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(2)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(2)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(2)%pconstant(i_cell) = average
                end do
              case default
                ctx_err%msg   = "** ERROR: Inconsistent acoustic " //   &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
        
        end select !! end for the current model
      else
        ctx_err%msg   = "** ERROR: Inconsistent use of  acoustic " //   &
                        "models for attenuation [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
      end if !! end if attenuation 
    !! =================================================================

    !! =================================================================    
    !! for the non-attenuated model, it depends on the parametrization
    else
      !! load models in case 
      allocate(bulk(n_coeff_loc))
      allocate(imp (n_coeff_loc))
      bulk = 0.0
      imp  = 0.0
      bulk = rho*vp*vp
      imp  = rho*vp

      ! ----------------------------------------------------------------
      !! parametrization Vp,Rho
      if(any(parametrization == model_RHO) .and.                        &
         any(parametrization == model_VP))  then
        select case(trim(adjustl(param_current)))
          case(model_RHO)          
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%rho%param_dof%mval(i_cell)%array(:)=model(igb1:igb2)
            end do
          
          case(model_VP)
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%vp%param_dof%mval(i_cell)%array(:)=model(igb1:igb2)
            end do
          case default
            ctx_err%ierr = -1
        end select
      ! ----------------------------------------------------------------
      !! parametrization Vp,Bulk
      !! Rho = K / Vp²
      elseif(any(parametrization == model_BULK) .and.                   &
             any(parametrization == model_VP))  then
        select case(trim(adjustl(param_current)))
          case(model_BULK)
            
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%rho%param_dof%mval(i_cell)%array(:)=model(igb1:igb2) &
               / (ctx_model%vp%param_dof%mval(i_cell)%array(:)**2)
            end do


          case(model_VP)
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                if(model(igb1) > tol) then
                  ctx_model%rho%param_dof%mval(i_cell)%array(ic) =      &
                    bulk(igb1) / model(igb1)**2
                else
                  ctx_model%rho%param_dof%mval(i_cell)%array(ic) =      &
                    ctx_model%rho_max
                end if
                
                ctx_model%vp%param_dof%mval(i_cell)%array(ic)  = model(igb1)
              end do              
            end do
            
            
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Vp,Imp
      !! Rho = I / Vp
      elseif(any(parametrization == model_IMP) .and.                    &
             any(parametrization == model_VP))  then
        select case(trim(adjustl(param_current)))
          case(model_IMP)
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%rho%pconstant = model / ctx_model%vp%pconstant
              
              ctx_model%rho%param_dof%mval(i_cell)%array(:)=model(igb1:igb2) &
              / ctx_model%vp%param_dof%mval(i_cell)%array(:)
            end do
            
          case(model_VP)
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                
                if(model(igb1) > tol) then
                  ctx_model%rho%param_dof%mval(i_cell)%array(ic) =      &
                    imp(igb1) / model(igb1)
                else
                  ctx_model%rho%param_dof%mval(i_cell)%array(ic) =      &
                    ctx_model%rho_max
                end if
                ctx_model%vp%param_dof%mval(i_cell)%array(ic) = model(igb1)
              end do
            end do
            
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Bulk,Rho
      !! Vp = sqrt(Kappa/Rho)
      elseif(any(parametrization == model_BULK) .and.                   &
             any(parametrization == model_RHO))  then
        select case(trim(adjustl(param_current)))
          case(model_BULK)
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                if(model(igb1) > tol) then
                  ctx_model%vp%param_dof%mval(i_cell)%array(ic) =       &
                  sqrt(model(igb1) /                                    &
                  ctx_model%rho%param_dof%mval(i_cell)%array(ic))
                else
                  ctx_model%vp%param_dof%mval(i_cell)%array(ic) =       &
                    ctx_model%vp_min
                end if
              end do
            end do

          case(model_RHO)
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*ctx_model%vp%param_dof%ndof + 1
              igb2 = igb1 + ctx_model%vp%param_dof%ndof - 1

              ctx_model%vp%param_dof%mval(i_cell)%array =               &
               sqrt(bulk(igb1:igb2)/model(igb1:igb2))
              ctx_model%rho%param_dof%mval(i_cell)%array = model(igb1:igb2)
            end do
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Bulk,Imp
      !! Vp = Kappa/Imp
      !! Rho= I² / Kappa
      elseif(any(parametrization == model_BULK) .and.                   &
             any(parametrization == model_IMP))  then
        select case(trim(adjustl(param_current)))
          case(model_BULK)
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                ctx_model%vp%param_dof%mval(i_cell)%array(ic)  =        &
                  model(igb1)  / imp(igb1)
                if(model(igb1) > tol) then
                  ctx_model%rho%param_dof%mval(i_cell)%array(ic) =      &
                    imp(igb1)**2 / model(igb1)
                else
                  ctx_model%rho%param_dof%mval(i_cell)%array(ic)  =     &
                    ctx_model%rho_max
                end if
              end do
            end do
            
          case(model_IMP)
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                if(model(igb1) > tol) then
                  ctx_model%vp%param_dof%mval(i_cell)%array(ic) =       &
                    bulk(igb1)    / model(igb1)
                else
                  ctx_model%vp%param_dof%mval(i_cell)%array(ic) =       &
                    ctx_model%vp_max
                end if
                ctx_model%rho%param_dof%mval(i_cell)%array(ic) =        &
                    model(igb1)**2/ bulk(igb1)
              end do
            end do
            
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization Imp, Rho
      !! Vp = Imp/Rho
      elseif(any(parametrization == model_RHO) .and.                    &
             any(parametrization == model_IMP))  then
        select case(trim(adjustl(param_current)))
          case(model_RHO)
            
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                if(model(igb1) > tol) then
                  ctx_model%vp%param_dof%mval(i_cell)%array(ic) =       &
                    imp(igb1) / model(igb1)
                else
                  ctx_model%vp%param_dof%mval(i_cell)%array(ic) =       &
                    ctx_model%vp_max
                end if
                ctx_model%rho%param_dof%mval(i_cell)%array(ic) = model(igb1)
              end do
            end do
            
          case(model_IMP)
            do i_cell = 1, ctx_model%n_cell_loc
              do ic = 1, npt_per_cell
                igb1 = (i_cell-1)*npt_per_cell + ic
                ctx_model%vp%param_dof%mval(i_cell)%array(ic) =         &
                  model(igb1) / ctx_model%rho%param_dof%mval(i_cell)%array(ic)
              end do
            end do
          case default
            ctx_err%ierr = -1
        end select
      
      ! ----------------------------------------------------------------
      !! Error
      else
        ctx_err%msg   = "** ERROR: Unrecognized model name [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      deallocate(bulk)
      deallocate(imp)
    end if
    ! ----------------------------------------------------------------
    deallocate(model)
    deallocate(vp)
    deallocate(rho)

    !! in case of error ------------------------------------------------
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: Unrecognized parametrization [model_update] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 

    !! if everything is ok so far, we have to adjust min and max values
    !! as well as the piecewise-constant averaged value.
    !! -----------------------------------------------------------------
    do i_cell = 1, ctx_model%n_cell_loc
      do ic=1, npt_per_cell
        if(ctx_model%vp%param_dof%mval(i_cell)%array(ic) < ctx_model%vp_min) &
           ctx_model%vp%param_dof%mval(i_cell)%array(ic) = ctx_model%vp_min
        if(ctx_model%vp%param_dof%mval(i_cell)%array(ic) > ctx_model%vp_max) &
           ctx_model%vp%param_dof%mval(i_cell)%array(ic) = ctx_model%vp_max
        if(ctx_model%rho%param_dof%mval(i_cell)%array(ic)< ctx_model%rho_min)&
           ctx_model%rho%param_dof%mval(i_cell)%array(ic)= ctx_model%rho_min
        if(ctx_model%rho%param_dof%mval(i_cell)%array(ic)> ctx_model%rho_max)&
           ctx_model%rho%param_dof%mval(i_cell)%array(ic)= ctx_model%rho_max
      end do
      average = dble(                                                   &
                sum(ctx_model%rho%param_dof%mval(i_cell)%array)/npt_per_cell)
      ctx_model%rho%pconstant(i_cell) = average
      average = dble(                                                   &
                sum(ctx_model%vp%param_dof%mval(i_cell)%array)/npt_per_cell)
      ctx_model%vp%pconstant(i_cell) = average
    end do
    
    if(ctx_model%flag_viscous) then
      nvisco = size(ctx_model%visco)
      do im = 1, nvisco
        do i_cell = 1, ctx_model%n_cell_loc
          do ic=1, npt_per_cell
            if(ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) <   &
                                               ctx_model%visco_min(im)) &
               ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) =   &
                                               ctx_model%visco_min(im)
            if(ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) >   &
                                               ctx_model%visco_max(im)) &
               ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) =   &
                                               ctx_model%visco_max(im)
          end do
          average =dble(                                                &
                  sum(ctx_model%visco(im)%param_dof%mval(i_cell)%array) &
                  / npt_per_cell)
          ctx_model%visco(im)%pconstant(i_cell) = average          
        end do
      end do
    end if 
    !! -----------------------------------------------------------------
    return
  end subroutine model_update_pconstant_dof

end module m_model_update
