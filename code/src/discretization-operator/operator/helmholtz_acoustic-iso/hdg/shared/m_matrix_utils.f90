!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_utils.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> HELMHOLTZ ACOUSTIC propagator using the
!> HDG discretization
!
!------------------------------------------------------------------------------
module m_matrix_utils

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH, RKIND_POL
  !> inverse matrix with Lapack
  use m_lapack_inverse,         only : lapack_matrix_inverse
  !> for the computation with quadrature rules 
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_array_types,            only: t_array3d_real
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_piecewise_constant,    &
                                      model_eval_piecewise_polynomial,  &
                                      model_eval_sep, model_eval_dof_refelem
  use m_ctx_model_representation, only: tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_PCONSTANT, tag_MODEL_DOF
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: hdg_build_Ainv_1D_line,hdg_build_C_1D_line
  public :: hdg_build_Ainv_2D_tri, hdg_build_C_2D_tri
  public :: hdg_build_Ainv_3D_tet, hdg_build_C_3D_tet
  public :: hdg_build_Ainv_1D_line_quadrature
  public :: hdg_build_Ainv_2D_tri_quadrature
  public :: hdg_build_Ainv_3D_tet_quadrature
  public :: hdg_build_quadrature_int_1D
  public :: hdg_build_quadrature_int_2D
  public :: hdg_build_quadrature_int_3D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ACOUSTIC 
  !> in 1D based upon segment discretization 
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    rho,kappa      : models parameter
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_1D_line(Ainv,vol_phi_phi,vol_dphi_phi,      &
                                    sumface_phii_phij,inv_jac,det_jac,  &
                                    kappa,rho,ndof_vol,freq,pml_coeff,  &
                                    ctx_err)
    implicit none
    

    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij(:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: kappa
    real   (kind=8)               ,intent(in)   :: rho
    complex(kind=8)               ,intent(in)   :: freq
    complex(kind=8)               ,intent(in)   :: pml_coeff(:)
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !! local
    real   (kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: dphii_phij(1)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n

    
    ctx_err%ierr = 0
    dim_sol =      2 !! pressure + 1D velocity 
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        ! ---------------------------------------------------------
        !! REFERENCE element, we have: 
        !! ---------------------------
        !!   \int_Ke  \nabla \phi_i dKe 
        !! = \int_Ke JF^{-T} \nabla \hat{\phi}_i dKe, 
        !!   avec \hat{\phi} reference element 
        !! = inv_jac(1,1)*dxphi + inv_jac(2,1)*dzphi     !! X-part
        !! + inv_jac(1,2)*dxphi + inv_jac(2,2)*dzphi     !! Z-part
        ! ---------------------------------------------------------
        massterm     = vol_phi_phi(i,j) * det_jac
        dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1)
        dphii_phij   = dphii_phij*det_jac*pml_coeff(1)
        ! ---------------------------------------------------------
        !! The fourier transform gives -i \omega
        !! The laplace transform gives -s
        !! Here we have freq = s + i \omega
        !! >>>>>>>>>>>>>>>> => -freq
        A(i         , j)          = dcmplx(-rho*freq*massterm)
        A(i         , j+ndof_vol) = dcmplx(-dphii_phij(1)    )

        dphii_phij(1)= vol_dphi_phi(j,i,1)*inv_jac(1,1)
        dphii_phij   = dphii_phij*det_jac*pml_coeff(1)
          
        A(i+ndof_vol, j)            = dcmplx(dphii_phij(1))
        !! + penalization * (\phi, \phi) over the face 
        A(i+ndof_vol, j+ndof_vol) = dcmplx(-freq/kappa*massterm +     &
                                             sumface_phii_phij(i,j))
        
      enddo
    enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)
    
    return
  end subroutine hdg_build_Ainv_1D_line

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ACOUSTIC 
  !> in 1D based upon segment discretization, if we use the 
  !> quadrature rule, the multiplication by rho and kappa has
  !> already been done.
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_1D_line_quadrature(Ainv,vol_phi_phi_kappa,  &
                              vol_phi_phi_rho,vol_dphi_phi,             &
                              sumface_phii_phij,inv_jac,det_jac,        &
                              ndof_vol,freq,pml_coeff,ctx_err)

    implicit none

    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    complex(kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_kappa(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_rho  (:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij(:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: freq
    complex(kind=8)               ,intent(in)   :: pml_coeff(:)
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !! local
    complex(kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: dphii_phij(1)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n

    
    ctx_err%ierr = 0
    dim_sol =      2 !! pressure + 1D velocity 
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        ! ---------------------------------------------------------
        !! REFERENCE element, we have: 
        !! ---------------------------
        !!   \int_Ke  \nabla \phi_i dKe 
        !! = \int_Ke JF^{-T} \nabla \hat{\phi}_i dKe, 
        !!   avec \hat{\phi} reference element 
        !! = inv_jac(1,1)*dxphi + inv_jac(2,1)*dzphi     !! X-part
        !! + inv_jac(1,2)*dxphi + inv_jac(2,2)*dzphi     !! Z-part
        ! ---------------------------------------------------------
        dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1)
        dphii_phij   = dphii_phij*det_jac*pml_coeff(1)
        ! ---------------------------------------------------------
        !! The fourier transform gives -i \omega
        !! The laplace transform gives -s
        !! Here we have freq = s + i \omega
        !! >>>>>>>>>>>>>>>> => -freq
        massterm     = dcmplx(vol_phi_phi_rho(i,j) * det_jac)
        A(i         , j)          = dcmplx(-freq*massterm)
        A(i         , j+ndof_vol) = dcmplx(-dphii_phij(1)    )

        dphii_phij(1)= vol_dphi_phi(j,i,1)*inv_jac(1,1)
        dphii_phij   = dphii_phij*det_jac*pml_coeff(1)
          
        A(i+ndof_vol, j)            = dcmplx(dphii_phij(1))
        !! + penalization * (\phi, \phi) over the face
        massterm     = dcmplx(vol_phi_phi_kappa(i,j) * det_jac)
        A(i+ndof_vol, j+ndof_vol) = dcmplx(-freq*massterm +     &
                                            sumface_phii_phij(i,j))
        
      enddo
    enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)
    
    return
  end subroutine hdg_build_Ainv_1D_line_quadrature

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ACOUSTIC 
  !> in 2D based upon triangle discretization 
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    rho,kappa      : models parameter
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_2D_tri(Ainv,vol_phi_phi,vol_dphi_phi,       &
                                   sumface_phii_phij,inv_jac,det_jac,   &
                                   kappa,rho,ndof_vol,freq,pml_coeff,ctx_err)

    implicit none

    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij(:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: kappa
    real   (kind=8)               ,intent(in)   :: rho
    complex(kind=8)               ,intent(in)   :: freq
    complex(kind=8)               ,intent(in)   :: pml_coeff(:)
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !! local
    real   (kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: dphii_phij(2)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n
    
    ctx_err%ierr = 0
    dim_sol =      3 !! pressure + 2D velocity 
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        ! ---------------------------------------------------------
        !! REFERENCE element, we have: 
        !! ---------------------------
        !!   \int_Ke  \nabla \phi_i dKe 
        !! = \int_Ke JF^{-T} \nabla \hat{\phi}_i dKe, 
        !!   avec \hat{\phi} reference element 
        !! = inv_jac(1,1)*dxphi + inv_jac(2,1)*dzphi     !! X-part
        !! + inv_jac(1,2)*dxphi + inv_jac(2,2)*dzphi     !! Z-part
        ! ---------------------------------------------------------
        massterm     = vol_phi_phi(i,j) * det_jac
        dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,1)
        dphii_phij(2)= vol_dphi_phi(i,j,1)*inv_jac(1,2) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,2)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(:) = dphii_phij(:) * pml_coeff(:)
        
        ! ---------------------------------------------------------
        !! The fourier transform gives -i \omega
        !! The laplace transform gives -s
        !! Here we have freq = s + i \omega
        !! >>>>>>>>>>>>>>>> => -freq
        A(i         , j)            = dcmplx(-rho*freq*massterm)
        A(i         , j+2*ndof_vol) = dcmplx(-dphii_phij(1)    ) 
        A(i+ndof_vol, j+  ndof_vol) = dcmplx(-rho*freq*massterm)
        A(i+ndof_vol, j+2*ndof_vol) = dcmplx(-dphii_phij(2)    )

        dphii_phij(1)= vol_dphi_phi(j,i,1)*inv_jac(1,1) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,1)
        dphii_phij(2)= vol_dphi_phi(j,i,1)*inv_jac(1,2) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,2)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(:) = dphii_phij(:) * pml_coeff(:)

        A(i+2*ndof_vol, j)            = dcmplx(dphii_phij(1))
        A(i+2*ndof_vol, j+  ndof_vol) = dcmplx(dphii_phij(2))
        !! + penalization * (\phi, \phi) over the face 
        A(i+2*ndof_vol, j+2*ndof_vol) = dcmplx(-freq/kappa*massterm +   &
                                               sumface_phii_phij(i,j))
        
      enddo
    enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)
    
    return
  end subroutine hdg_build_Ainv_2D_tri


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ACOUSTIC 
  !> in 2D based upon triangle discretization, if we use the 
  !> quadrature rule, the multiplication by rho and kappa has
  !> already been done.
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_2D_tri_quadrature(Ainv,vol_phi_phi_kappa,  &
                              vol_phi_phi_rho,vol_dphi_phi,             &
                              sumface_phii_phij,inv_jac,det_jac,        &
                              ndof_vol,freq,pml_coeff,ctx_err)

    implicit none

    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    complex(kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_kappa(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_rho  (:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij(:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: freq
    complex(kind=8)               ,intent(in)   :: pml_coeff(:)
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !! local
    complex(kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: dphii_phij(2)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n
    
    ctx_err%ierr = 0
    dim_sol =      3 !! pressure + 2D velocity 
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        ! ---------------------------------------------------------
        !! REFERENCE element, we have: 
        !! ---------------------------
        !!   \int_Ke  \nabla \phi_i dKe 
        !! = \int_Ke JF^{-T} \nabla \hat{\phi}_i dKe, 
        !!   avec \hat{\phi} reference element 
        !! = inv_jac(1,1)*dxphi + inv_jac(2,1)*dzphi     !! X-part
        !! + inv_jac(1,2)*dxphi + inv_jac(2,2)*dzphi     !! Z-part
        ! ---------------------------------------------------------
        dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,1)
        dphii_phij(2)= vol_dphi_phi(i,j,1)*inv_jac(1,2) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,2)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(:) = dphii_phij(:) * pml_coeff(:)

        ! ---------------------------------------------------------
        !! The fourier transform gives -i \omega
        !! The laplace transform gives -s
        !! Here we have freq = s + i \omega
        !! >>>>>>>>>>>>>>>> => -freq
        massterm     = dcmplx(vol_phi_phi_rho(i,j) * det_jac)
        A(i         , j)            = dcmplx(-freq*massterm)
        A(i         , j+2*ndof_vol) = dcmplx(-dphii_phij(1)    ) 
        A(i+ndof_vol, j+  ndof_vol) = dcmplx(-freq*massterm)
        A(i+ndof_vol, j+2*ndof_vol) = dcmplx(-dphii_phij(2)    )

        dphii_phij(1)= vol_dphi_phi(j,i,1)*inv_jac(1,1) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,1)
        dphii_phij(2)= vol_dphi_phi(j,i,1)*inv_jac(1,2) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,2)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(:) = dphii_phij(:) * pml_coeff(:)
        
        massterm     = dcmplx(vol_phi_phi_kappa(i,j) * det_jac)
        A(i+2*ndof_vol, j)            = dcmplx(dphii_phij(1))
        A(i+2*ndof_vol, j+  ndof_vol) = dcmplx(dphii_phij(2))
        !! + penalization * (\phi, \phi) over the face 
        A(i+2*ndof_vol, j+2*ndof_vol) = dcmplx(-freq*massterm +         &
                                               sumface_phii_phij(i,j))
        
      enddo
    enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)
    
    return
  end subroutine hdg_build_Ainv_2D_tri_quadrature


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ACOUSTIC 
  !> in 3D based upon tetrahedral discretization 
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    rho,kappa      : models parameter
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_3D_tet(Ainv,vol_phi_phi,vol_dphi_phi,       &
                                   sumface_phii_phij,inv_jac,det_jac,   &
                                   kappa,rho,ndof_vol,freq,pml_coeff,ctx_err)

    implicit none

    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij(:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: kappa
    real   (kind=8)               ,intent(in)   :: rho
    complex(kind=8)               ,intent(in)   :: freq
    complex(kind=8)               ,intent(in)   :: pml_coeff(:)
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !! local
    real   (kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: dphii_phij(3)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n

    ctx_err%ierr = 0
    dim_sol =      4 !! pressure + 3D velocity 
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        ! ---------------------------------------------------------
        !! REFERENCE element, we have: 
        !! ---------------------------
        !!   \int_Ke  \nabla \phi_i dKe 
        !! = \int_Ke JF^{-T} \nabla \hat{\phi}_i dKe, 
        !!   avec \hat{\phi} reference element 
        !! = inv_jac(1,1)*dxphi + inv_jac(2,1)*dyphi + inv_jac(3,1)*dzphi     !! X-part
        !! + inv_jac(1,2)*dxphi + inv_jac(2,2)*dyphi + inv_jac(3,2)*dzphi     !! Y-part
        !! + inv_jac(1,3)*dxphi + inv_jac(2,3)*dyphi + inv_jac(3,3)*dzphi     !! Z-part
        ! ---------------------------------------------------------
        massterm     = vol_phi_phi(i,j) * det_jac
        dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,1) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,1)
        dphii_phij(2)= vol_dphi_phi(i,j,1)*inv_jac(1,2) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,2) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,2)
        dphii_phij(3)= vol_dphi_phi(i,j,1)*inv_jac(1,3) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,3) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,3)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(1:3) = dphii_phij(1:3) * pml_coeff(1:3)
       
        ! ---------------------------------------------------------
        !! The fourier transform gives -i \omega
        !! The laplace transform gives -s
        !! Here we have freq = s + i \omega
        !! >>>>>>>>>>>>>>>> => -freq
        A(i           , j)            = dcmplx(-rho*freq*massterm)
        A(i           , j+3*ndof_vol) = dcmplx(-dphii_phij(1)    )
        A(i+  ndof_vol, j+  ndof_vol) = dcmplx(-rho*freq*massterm)
        A(i+  ndof_vol, j+3*ndof_vol) = dcmplx(-dphii_phij(2)    )
        A(i+2*ndof_vol, j+2*ndof_vol) = dcmplx(-rho*freq*massterm)
        A(i+2*ndof_vol, j+3*ndof_vol) = dcmplx(-dphii_phij(3)    )                                             

        dphii_phij(1)= vol_dphi_phi(j,i,1)*inv_jac(1,1) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,1) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,1)
        dphii_phij(2)= vol_dphi_phi(j,i,1)*inv_jac(1,2) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,2) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,2)
        dphii_phij(3)= vol_dphi_phi(j,i,1)*inv_jac(1,3) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,3) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,3)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(1:3) = dphii_phij(1:3) * pml_coeff(1:3)

        A(i+3*ndof_vol, j           ) = dcmplx(dphii_phij(1))
        A(i+3*ndof_vol, j+  ndof_vol) = dcmplx(dphii_phij(2))
        A(i+3*ndof_vol, j+2*ndof_vol) = dcmplx(dphii_phij(3))
        A(i+3*ndof_vol, j+3*ndof_vol) = dcmplx(-freq/kappa*massterm +   &
                                               sumface_phii_phij(i,j))
        
      enddo
    enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)

    return
  end subroutine hdg_build_Ainv_3D_tet

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ACOUSTIC 
  !> in 3D based upon tetrahedral discretization, if we use the 
  !> quadrature rule, the multiplication by rho and kappa has
  !> already been done.
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_3D_tet_quadrature(Ainv,vol_phi_phi_kappa,  &
                              vol_phi_phi_rho,vol_dphi_phi,             &
                              sumface_phii_phij,inv_jac,det_jac,        &
                              ndof_vol,freq,pml_coeff,ctx_err)

    implicit none

    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    complex(kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_kappa(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_rho  (:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij(:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: freq
    complex(kind=8)               ,intent(in)   :: pml_coeff(:)
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !! local
    complex(kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: dphii_phij(3)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n

    ctx_err%ierr = 0
    dim_sol =      4 !! pressure + 3D velocity 
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        ! ---------------------------------------------------------
        !! REFERENCE element, we have: 
        !! ---------------------------
        !!   \int_Ke  \nabla \phi_i dKe 
        !! = \int_Ke JF^{-T} \nabla \hat{\phi}_i dKe, 
        !!   avec \hat{\phi} reference element 
        !! = inv_jac(1,1)*dxphi + inv_jac(2,1)*dyphi + inv_jac(3,1)*dzphi     !! X-part
        !! + inv_jac(1,2)*dxphi + inv_jac(2,2)*dyphi + inv_jac(3,2)*dzphi     !! Y-part
        !! + inv_jac(1,3)*dxphi + inv_jac(2,3)*dyphi + inv_jac(3,3)*dzphi     !! Z-part
        ! ---------------------------------------------------------
        dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,1) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,1)
        dphii_phij(2)= vol_dphi_phi(i,j,1)*inv_jac(1,2) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,2) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,2)
        dphii_phij(3)= vol_dphi_phi(i,j,1)*inv_jac(1,3) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,3) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,3)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(:) = dphii_phij(:) * pml_coeff(:)
       
        ! ---------------------------------------------------------
        !! The fourier transform gives -i \omega
        !! The laplace transform gives -s
        !! Here we have freq = s + i \omega
        !! >>>>>>>>>>>>>>>> => -freq
        massterm     = dcmplx(vol_phi_phi_rho(i,j) * det_jac)
        A(i           , j)            = dcmplx(-freq*massterm)
        A(i           , j+3*ndof_vol) = dcmplx(-dphii_phij(1)    )
        A(i+  ndof_vol, j+  ndof_vol) = dcmplx(-freq*massterm)
        A(i+  ndof_vol, j+3*ndof_vol) = dcmplx(-dphii_phij(2)    )
        A(i+2*ndof_vol, j+2*ndof_vol) = dcmplx(-freq*massterm)
        A(i+2*ndof_vol, j+3*ndof_vol) = dcmplx(-dphii_phij(3)    )
        
        dphii_phij(1)= vol_dphi_phi(j,i,1)*inv_jac(1,1) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,1) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,1)
        dphii_phij(2)= vol_dphi_phi(j,i,1)*inv_jac(1,2) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,2) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,2)
        dphii_phij(3)= vol_dphi_phi(j,i,1)*inv_jac(1,3) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,3) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,3)
        dphii_phij   = dphii_phij*det_jac
        !! adjust PML
        dphii_phij(:) = dphii_phij(:) * pml_coeff(:)

        massterm     = dcmplx(vol_phi_phi_kappa(i,j) * det_jac)
        A(i+3*ndof_vol, j           ) = dcmplx(dphii_phij(1))
        A(i+3*ndof_vol, j+  ndof_vol) = dcmplx(dphii_phij(2))
        A(i+3*ndof_vol, j+2*ndof_vol) = dcmplx(dphii_phij(3))
        A(i+3*ndof_vol, j+3*ndof_vol) = dcmplx(-freq*massterm +   &
                                               sumface_phii_phij(i,j))
        
      enddo
    enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)

    return
  end subroutine hdg_build_Ainv_3D_tet_quadrature

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix C for HDG in HELMHOLTZ ACOUSTIC
  !> in 1D based upon line discretization 
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_C_1D_line(C,F,penalization,ndof_vol,ndof_face_neigh,  &
                                ndf_edges_elem,normal,pml_coeff)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: C(:,:)
    complex(kind=8), allocatable     ,intent(in)   :: F(:,:,:)
    real   (kind=8)                                :: penalization   (2)
    integer                          ,intent(in)   :: ndof_face_neigh(2)
    integer                          ,intent(in)   :: ndf_edges_elem (3)
    integer                          ,intent(in)   :: ndof_vol
    real(kind=8)                     ,intent(in)   :: normal(1,2)
    complex(kind=8)                  ,intent(in)   :: pml_coeff(:)
    !!
    integer         :: i, j, i_face

    !! using PML, we have coeff_pml v.n = coeff_pml v.n + tau ...
    C=0.0
    do i_face=1,2
      do i=1,ndof_vol ; do j=1,ndof_face_neigh(i_face)
        C(           i,ndf_edges_elem(i_face)+j)=F(i_face,i,j)*         &
                                                 normal(1,i_face)*pml_coeff(1)
        C(1*ndof_vol+i,ndf_edges_elem(i_face)+j)=-penalization(i_face)*F(i_face,i,j)
      enddo ; enddo
    enddo
    return
  end subroutine hdg_build_C_1D_line

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix C for HDG in HELMHOLTZ ACOUSTIC
  !> in 2D based upon triangle discretization 
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_C_2D_tri(C,F,penalization,ndof_vol,ndof_face_neigh,  &
                                ndf_edges_elem,n,pml_coeff)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: C(:,:)
    complex(kind=8), allocatable     ,intent(in)   :: F(:,:,:)
    complex(kind=8)                  ,intent(in)   :: penalization(3)
    integer                          ,intent(in)   :: ndof_face_neigh(3)
    integer                          ,intent(in)   :: ndf_edges_elem (4)
    integer                          ,intent(in)   :: ndof_vol
    real(kind=8)                     ,intent(in)   :: n(2,3)
    complex(kind=8)                  ,intent(in)   :: pml_coeff(:)
    !!
    integer         :: i, j, i_face

    C=0.0
    do i_face=1,3
      do i=1,ndof_vol ; do j=1,ndof_face_neigh(i_face)
        C(           i,ndf_edges_elem(i_face)+j)=F(i_face,i,j)*n(1,i_face)*pml_coeff(1)
        C(  ndof_vol+i,ndf_edges_elem(i_face)+j)=F(i_face,i,j)*n(2,i_face)*pml_coeff(2)
        C(2*ndof_vol+i,ndf_edges_elem(i_face)+j)=-penalization(i_face)*F(i_face,i,j)
      enddo ; enddo
    enddo

    return
  end subroutine hdg_build_C_2D_tri

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix C for HDG in HELMHOLTZ ACOUSTIC
  !> in 3D based upon tetrahedral discretization 
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_C_3D_tet(C,F,penalization,ndof_vol,ndof_face_neigh,  &
                                ndf_edges_elem,n,pml_coeff)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: C(:,:)
    complex(kind=8), allocatable     ,intent(in)   :: F(:,:,:)
    real   (kind=8)                                :: penalization   (4)
    integer                          ,intent(in)   :: ndof_face_neigh(4)
    integer                          ,intent(in)   :: ndf_edges_elem (5)
    integer                          ,intent(in)   :: ndof_vol
    real(kind=8)                     ,intent(in)   :: n(3,4)
    complex(kind=8)                  ,intent(in)   :: pml_coeff(:)
    !!
    integer         :: i, j, i_face

    C=0.0
    do i_face=1,4
      do i=1,ndof_vol ; do j=1,ndof_face_neigh(i_face)
        C(           i,ndf_edges_elem(i_face)+j)=F(i_face,i,j)*n(1,i_face)*pml_coeff(1)
        C(  ndof_vol+i,ndf_edges_elem(i_face)+j)=F(i_face,i,j)*n(2,i_face)*pml_coeff(2)
        C(2*ndof_vol+i,ndf_edges_elem(i_face)+j)=F(i_face,i,j)*n(3,i_face)*pml_coeff(3)
        C(3*ndof_vol+i,ndf_edges_elem(i_face)+j)=-penalization(i_face)*F(i_face,i,j)
      enddo ; enddo
    enddo

    return
  end subroutine hdg_build_C_3D_tet


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Create the local integral over the current cell to obtain
  !> quantities needed on the local element matrices
  !> for Helmholtz acoustic, we need 
  !>    \int_Ke rho(x) \phi_i(x) \phi_j(x)
  !>    \int_Ke d\phi_i(x) \phi_j(x)
  !>    \int_Ke (1 / kappa) \phi_i(x) \phi_j(x)
  !> On the face, we also need 
  !>    \int_DKe \phi_i(x) \xi_j(x)
  !> 
  !   REMARK: **********************************************************
  !>  ********** Everything is still computed on the reference element
  !>  ********** we just incorporate a possibility for model variation
  !>  ********** that is, the multiplication with det_jac for the local
  !>  ********** element remains in the creation of matrix A and C.
  !   END REMARK: ******************************************************
  !>
  !> @param[in]    ctx_dg          : DG context
  !> @param[in]    ctx_model       : model parameters context
  !> @param[in]    i_cell          : current cell
  !> @param[in]    i_o_cell        : cell order for volume matrices
  !> @param[in]    freq            : frequency, only for viscosity model
  !> @param[out]   vol_phi_phi_rho : \int_Ke rho(x) \phi_i(x) \phi_j(x)
  !> @param[out]   vol_phi_phi_ka  : \int_Ke (1/kappa) \phi_i(x) \phi_j(x)
  !> @param[out]   vol_dphi_phi    : \int_Ke d\phi_i(x) \phi_j(x)
  !> @param[out]   face_phi_xi     : \int_DKe \phi_i(x) \xi_j(x)
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_quadrature_int_1D(ctx_dg,ctx_mesh,model,i_cell,  &
                                  i_o_cell,freq,vol_phi_phi_rho,        &
                                  vol_phi_phi_kappa,vol_dphi_phi,       &
                                  face_phi_xi,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer                            ,intent(in)    :: i_o_cell
    complex(kind=8)                    ,intent(in)    :: freq
    !! these are the matrices that are computed here
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_rho(:,:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_kappa(:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_dphi_phi(:,:,:)
    type(t_array3d_real)   ,allocatable,intent(inout) :: face_phi_xi(:,:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    integer                     :: k, i, j
    integer                     :: n_dof
    real   (kind=8),allocatable :: rho(:),xquad_all(:,:)
    complex(kind=8),allocatable :: vel_all(:)
    real   (kind=8)             :: rho_loc
    real   (kind=RKIND_POL)     :: quad_rho
    real   (kind=RKIND_POL)     :: quad_dphi(1) !! one per dim
    complex(kind=RKIND_POL)     :: quad_kap
    complex(kind=8)             :: vel
    complex(kind=8),allocatable :: kappa(:)
    integer                     :: iquad,i_dim
    !! for face integral
    integer                     :: l, n_dof_l
    real   (kind=8)             :: xpt(1),quad_pt(1),node_coo(1,2)
    real   (kind=RKIND_POL)     :: quad_face(2) !! 2 faces for segment
    
    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then 
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! evaluate the model parameters in all quadrature points 
    allocate(rho  (ctx_dg%quadGL_npts))
    allocate(kappa(ctx_dg%quadGL_npts))
    rho = 0.d0 ; kappa = 0.d0

    select case(trim(adjustl(model%format_disc)))
      case(tag_MODEL_PCONSTANT)
        !! evaluate the models at this cell with attenuation 
        call model_eval_piecewise_constant(model,i_cell,freq,vel,rho_loc,ctx_err)
          rho  (:) = rho_loc
          kappa(:) = rho_loc * (vel**2)

      case(tag_MODEL_PPOLY)   
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the reference 
          !! element coordinates directly. 
          !! ***********************************************************
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          call model_eval_piecewise_polynomial(model,i_cell,xpt,freq,   &
                                               vel,rho_loc,ctx_err)
          rho  (iquad) = rho_loc
          kappa(iquad) = rho_loc * (vel**2)
        end do

      case(tag_MODEL_SEP)
        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the local
          !! position
          !! ***********************************************************
          !! find its global position
          !! we have jac1D * (x_ref)^T + (x)_node1 = (x)al
          quad_pt(:) = dble  (ctx_dg%quadGL_pts(:,iquad))
          xpt    (:) = matmul(dble(ctx_dg%jac(:,:,i_cell)), quad_pt) +  &
                       dble(node_coo(:,1))
          call model_eval_sep(model,xpt,freq,vel,rho_loc,ctx_err)
          rho  (iquad) = rho_loc
          kappa(iquad) = rho_loc * (vel**2)
       end do

      case(tag_MODEL_DOF)
        !! compute the position of all quadrature points
        allocate(xquad_all(ctx_mesh%dim_domain,ctx_dg%quadGL_npts))
        allocate(vel_all  (ctx_dg%quadGL_npts))
        xquad_all = 0.d0
        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! local position coordinates
          !! ***********************************************************
          !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          xquad_all(:,iquad) = xpt(:)
       end do
       !! compute the models value
       call model_eval_dof_refelem(model,i_cell,xquad_all,              & 
                           ctx_dg%quadGL_npts,freq,vel_all,rho,ctx_err)
       kappa(:) = rho * (vel_all**2)
       deallocate(xquad_all)
       deallocate(vel_all  )

      case default
        ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                        " [build_quadrature] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select 

    if(ctx_err%ierr .ne. 0) return
    
    !! -----------------------------------------------------------------
    !! Create local matrices, only the face matrix must adapted to 
    !! the different order because of neighbors, otherwize, we 
    !! actually only need the current cell order to deal with.
    !! -----------------------------------------------------------------
    k = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)
    vol_phi_phi_rho   = 0.d0
    vol_phi_phi_kappa = 0.d0
    vol_dphi_phi      = 0.d0
    ! ----------------------------------------------------------------
    !!  VOLUME MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ----------------------------------------------------------------
    do i=1, n_dof ; do j=1, n_dof
      quad_rho = 0.d0
      quad_kap = 0.d0
      quad_dphi= 0.d0
     
      do iquad=1,ctx_dg%quadGL_npts
        !! REMARK: **********************************************************
        !! ********** Everything is still computed on the reference element
        !! ********** we just incorporate a possibility for model variation
        !! ********** that is, the multiplication with det_jac for the local
        !! ********** element remains in the creation of matrix A and C.
        !! END REMARK: ******************************************************

        !! for the volumic integrals
        !! the precomputation already has the weight.
        quad_rho = quad_rho + rho(iquad) *                            &
                              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        quad_kap = quad_kap + 1.d0/kappa(iquad) *                     &
                              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)

        !!  dPhi.Phi
        do i_dim=1,ctx_dg%dim_domain
          quad_dphi(i_dim) = quad_dphi(i_dim) +                       &
                             ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,i_dim)
        end do
    
      end do
      !! *************************************************************
      !! IF WE WANT TO HAVE DIRECTLY ON THE REFERENCE ELEMENT, WE CAN
      !! MULTIPLY BY THE AREA.
      !! INSTEAD, WE REMAIN ON THE REFERENCE ELEMENT, THIS SIMPLY 
      !! CORRESPONDS TO THE DET_JAC THAT WE USE LATER ON IN A & C.
      ! ---------------------------------------------------------
      !!     vol_matrix
      vol_phi_phi_rho  (i,j)  =  real(quad_rho ,kind=RKIND_POL)
      vol_phi_phi_kappa(i,j)  = cmplx(quad_kap ,kind=RKIND_POL)
      !! reorder
      vol_dphi_phi     (i,j,:)=  real(quad_dphi,kind=RKIND_POL)
    end do ; end do


     
    ! ----------------------------------------------------------------
    !!  SURFACE MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! double loop on the order for the surface matrices,
    !! only one point in 1D
    ! ----------------------------------------------------------------
    do k = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order(k)
      
      do l = 1, ctx_dg%n_different_order
        n_dof_l = ctx_dg%n_dof_per_order (l)
        face_phi_xi(k,l)%array = 0.d0

        do i=1, n_dof ; do j=1, n_dof_l
          
          quad_face = 0.d0
          do iquad=1,ctx_dg%quadGL_face_npts
            !! weight is already taken into account, all faces at once here
            quad_face = quad_face +                                     &
                        ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,:)
          end do
          !! update all faces.
          face_phi_xi(k,l)%array(i,j,:)=real(quad_face(:),kind=RKIND_POL)

        end do ; end do 
      end do !! end second loop on the different orders.     
    end do !! end first loop on the different orders.

    deallocate(kappa)
    deallocate(rho)

    return
  end subroutine hdg_build_quadrature_int_1D
  
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Create the local integral over the current cell to obtain
  !> quantities needed on the local element matrices
  !> for Helmholtz acoustic, we need 
  !>    \int_Ke rho(x) \phi_i(x) \phi_j(x)
  !>    \int_Ke d\phi_i(x) \phi_j(x)
  !>    \int_Ke (1 / kappa) \phi_i(x) \phi_j(x)
  !> On the face, we also need 
  !>    \int_DKe \phi_i(x) \xi_j(x)
  !> 
  !   REMARK: **********************************************************
  !>  ********** Everything is still computed on the reference element
  !>  ********** we just incorporate a possibility for model variation
  !>  ********** that is, the multiplication with det_jac for the local
  !>  ********** element remains in the creation of matrix A and C.
  !   END REMARK: ******************************************************
  !>
  !> @param[in]    ctx_dg          : DG context
  !> @param[in]    ctx_model       : model parameters context
  !> @param[in]    i_cell          : current cell
  !> @param[in]    i_o_cell        : cell order for volume matrices
  !> @param[in]    freq            : frequency, only for viscosity model
  !> @param[out]   vol_phi_phi_rho : \int_Ke rho(x) \phi_i(x) \phi_j(x)
  !> @param[out]   vol_phi_phi_ka  : \int_Ke (1/kappa) \phi_i(x) \phi_j(x)
  !> @param[out]   vol_dphi_phi    : \int_Ke d\phi_i(x) \phi_j(x)
  !> @param[out]   face_phi_xi     : \int_DKe \phi_i(x) \xi_j(x)
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_quadrature_int_2D(ctx_dg,ctx_mesh,model,i_cell,  &
                                  i_o_cell,freq,                        &
                                  vol_phi_phi_rho,vol_phi_phi_kappa,    &
                                  vol_dphi_phi,face_phi_xi,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer                            ,intent(in)    :: i_o_cell
    complex(kind=8)                    ,intent(in)    :: freq
    !! these are the matrices that are computed here
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_rho(:,:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_kappa(:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_dphi_phi(:,:,:)
    type(t_array3d_real)   ,allocatable,intent(inout) :: face_phi_xi(:,:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    integer                     :: k, i, j
    integer                     :: n_dof
    real   (kind=8),allocatable :: rho(:),xquad_all(:,:)
    complex(kind=8),allocatable :: vel_all(:)
    real   (kind=8)             :: rho_loc
    real   (kind=RKIND_POL)     :: quad_rho
    real   (kind=RKIND_POL)     :: quad_dphi(2) !! one per dim
    complex(kind=RKIND_POL)     :: quad_kap
    complex(kind=8)             :: vel
    complex(kind=8),allocatable :: kappa(:)
    integer                     :: iquad,i_dim
    !! to evaluate the model in GL pts
    real(kind=8)                :: xpt(2),quad_pt(2),node_coo(2,3)
    !! for face integral
    integer                     :: l, n_dof_l
    real   (kind=RKIND_POL)     :: quad_face(3) !! 3 faces for triangles
    
    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then 
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! evaluate the model parameters in all quadrature points 
    allocate(rho  (ctx_dg%quadGL_npts))
    allocate(kappa(ctx_dg%quadGL_npts))
    rho = 0.d0 ; kappa = 0.d0

    select case(trim(adjustl(model%format_disc)))
      case(tag_MODEL_PCONSTANT)
        !! evaluate the models at this cell with attenuation 
        call model_eval_piecewise_constant(model,i_cell,freq,vel,       &
                                           rho_loc,ctx_err)
          rho  (:) = rho_loc
          kappa(:) = rho_loc * (vel**2)

      case(tag_MODEL_PPOLY)   
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the reference 
          !! element coordinates directly. 
          !! ***********************************************************
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          call model_eval_piecewise_polynomial(model,i_cell,xpt,freq,   &
                                               vel,rho_loc,ctx_err)
          rho  (iquad) = rho_loc
          kappa(iquad) = rho_loc * (vel**2)

        end do

      case(tag_MODEL_SEP)
        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the local
          !! position
          !! ***********************************************************
          !! find its global position
          !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al
          quad_pt(:) = dble  (ctx_dg%quadGL_pts(:,iquad))
          xpt    (:) = matmul(dble(ctx_dg%jac(:,:,i_cell)), quad_pt) +  &
                       dble(node_coo(:,1))
          call model_eval_sep(model,xpt,freq,vel,rho_loc,ctx_err)
          rho  (iquad) = rho_loc
          kappa(iquad) = rho_loc * (vel**2)
       end do

      case(tag_MODEL_DOF)
        !! compute the position of all quadrature points
        allocate(xquad_all(ctx_mesh%dim_domain,ctx_dg%quadGL_npts))
        allocate(vel_all  (ctx_dg%quadGL_npts))
        xquad_all = 0.d0
        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! local position coordinates
          !! ***********************************************************
          !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          xquad_all(:,iquad) = xpt(:)
       end do
       !! compute the models value
       call model_eval_dof_refelem(model,i_cell,xquad_all,              & 
                           ctx_dg%quadGL_npts,freq,vel_all,rho,ctx_err)
       kappa(:) = rho * (vel_all**2)
       deallocate(xquad_all)
       deallocate(vel_all  )
       
      case default
        ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                        " [build_quadrature] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select 
    if(ctx_err%ierr .ne. 0) return
    
    !! -----------------------------------------------------------------
    !! Create local matrices, only the face matrix must adapted to 
    !! the different order because of neighbors, otherwize, we 
    !! actually only need the current cell order to deal with.
    !! -----------------------------------------------------------------
    k       = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)
    vol_phi_phi_rho  = 0.d0
    vol_phi_phi_kappa= 0.d0
    vol_dphi_phi     = 0.d0

    ! ----------------------------------------------------------------
    !!  VOLUME MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ----------------------------------------------------------------
    do i=1, n_dof ; do j=1, n_dof
      quad_rho = 0.d0
      quad_kap = 0.d0
      quad_dphi= 0.d0
     
      do iquad=1,ctx_dg%quadGL_npts
        !! REMARK: **********************************************************
        !! ********** Everything is still computed on the reference element
        !! ********** we just incorporate a possibility for model variation
        !! ********** that is, the multiplication with det_jac for the local
        !! ********** element remains in the creation of matrix A and C.
        !! END REMARK: ******************************************************

        !! for the volumic integrals
        !! the precomputation already has the weight.
        quad_rho = quad_rho + rho(iquad) *                            &
                              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        quad_kap = quad_kap + 1.d0/kappa(iquad) *                     &
                              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)

        !!  dPhi.Phi
        do i_dim=1,ctx_dg%dim_domain
          quad_dphi(i_dim) = quad_dphi(i_dim) +                       &
                             ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,i_dim)
        end do
    
      end do
      !! *************************************************************
      !! IF WE WANT TO HAVE DIRECTLY ON THE REFERENCE ELEMENT, WE CAN
      !! MULTIPLY BY THE AREA SCALED BY AREA OF THE REFERENCE ELEMENT.
      !! WITH 2D TRIANGLE, THE REFERENCE ELEMENT HAS SIZE 1*1/2 = 1/2.
      !! SO WE CAN MULTIPLY BY AREA*2
      !! INSTEAD, WE REMAIN ON THE REFERENCE ELEMENT, THIS SIMPLY 
      !! CORRESPONDS TO THE DET_JAC THAT WE USE LATER ON IN A & C.
      !! *************************************************************        
      !! call mesh_area_simplex(ctx_mesh%dim_domain,node_coo,area,ctx_err)
      !! quad_rho = quad_rho * area * 2
      ! ---------------------------------------------------------
      !!     vol_matrix
      vol_phi_phi_rho  (i,j)  =  real(quad_rho ,kind=RKIND_POL)
      vol_phi_phi_kappa(i,j)  = cmplx(quad_kap ,kind=RKIND_POL)
      !! reorder
      vol_dphi_phi     (i,j,:)=  real(quad_dphi,kind=RKIND_POL)
    end do ; end do

    !! -----------------------------------------------------------------
    !!  SURFACE MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !!  We only need to sum the pre-computed array and the 
    !!  passage towards the local element is made, as usual, in the 
    !!  creations of matrix A & B & L, etc.
    !! -----------------------------------------------------------------
    !! we need the second loop over the different orders ...............
    do k = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order(k)

      do l = 1, ctx_dg%n_different_order
        n_dof_l = ctx_dg%n_dof_per_order (l)
        face_phi_xi(k,l)%array = 0.d0

        do i=1, n_dof ; do j=1, n_dof_l
          
          quad_face = 0.d0
          do iquad=1,ctx_dg%quadGL_face_npts
            !! weight is already taken into account, all faces at once here
            quad_face = quad_face +                                     &
                        ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,:)
          end do
          !! update all faces.
          face_phi_xi(k,l)%array(i,j,:)=real(quad_face(:),kind=RKIND_POL)

        end do ; end do

      end do !! end second loop on the different orders.     
    end do !! end first loop on the different orders.

    deallocate(kappa)
    deallocate(rho)

    return
  end subroutine hdg_build_quadrature_int_2D
  

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Create the local integral over the current cell to obtain
  !> quantities needed on the local element matrices
  !> for Helmholtz acoustic, we need 
  !>    \int_Ke rho(x) \phi_i(x) \phi_j(x)
  !>    \int_Ke d\phi_i(x) \phi_j(x)
  !>    \int_Ke (1 / kappa) \phi_i(x) \phi_j(x)
  !> On the face, we also need 
  !>    \int_DKe \phi_i(x) \xi_j(x)
  !> 
  !   REMARK: **********************************************************
  !>  ********** Everything is still computed on the reference element
  !>  ********** we just incorporate a possibility for model variation
  !>  ********** that is, the multiplication with det_jac for the local
  !>  ********** element remains in the creation of matrix A and C.
  !   END REMARK: ******************************************************
  !>
  !> @param[in]    ctx_dg          : DG context
  !> @param[in]    ctx_model       : model parameters context
  !> @param[in]    i_cell          : current cell
  !> @param[in]    i_o_cell        : cell order for volume matrices
  !> @param[in]    freq            : frequency, only for viscosity model
  !> @param[out]   vol_phi_phi_rho : \int_Ke rho(x) \phi_i(x) \phi_j(x)
  !> @param[out]   vol_phi_phi_ka  : \int_Ke (1/kappa) \phi_i(x) \phi_j(x)
  !> @param[out]   vol_dphi_phi    : \int_Ke d\phi_i(x) \phi_j(x)
  !> @param[out]   face_phi_xi     : \int_DKe \phi_i(x) \xi_j(x)
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_quadrature_int_3D(ctx_dg,ctx_mesh,               &
                                  model,i_cell,i_o_cell,freq,           &
                                  vol_phi_phi_rho,vol_phi_phi_kappa,    &
                                  vol_dphi_phi,face_phi_xi,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer                            ,intent(in)    :: i_o_cell
    complex(kind=8)                    ,intent(in)    :: freq
    !! these are the matrices that are computed here
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_rho(:,:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_kappa(:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_dphi_phi(:,:,:)
    type(t_array3d_real)   ,allocatable,intent(inout) :: face_phi_xi(:,:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    integer                     :: k, i, j
    integer                     :: n_dof
    real   (kind=8),allocatable :: rho(:),xquad_all(:,:)
    complex(kind=8),allocatable :: vel_all(:)
    real   (kind=8)             :: rho_loc
    real   (kind=RKIND_POL)     :: quad_rho
    real   (kind=RKIND_POL)     :: quad_dphi(3) !! one per dim
    complex(kind=RKIND_POL)     :: quad_kap
    complex(kind=8)             :: vel
    complex(kind=8),allocatable :: kappa(:)
    integer                     :: iquad,i_dim
    !! to evaluate the model in GL pts
    real(kind=8)                :: xpt(3),node_coo(3,4),quad_pt(3)
    !! for face integral
    integer                     :: l, n_dof_l
    real   (kind=RKIND_POL)     :: quad_face(4) !! 4 faces for tetra
    
    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then 
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! evaluate the model parameters in all quadrature points 
    allocate(rho  (ctx_dg%quadGL_npts))
    allocate(kappa(ctx_dg%quadGL_npts))
    rho = 0.d0 ; kappa = 0.d0

    select case(trim(adjustl(model%format_disc)))

      case(tag_MODEL_PCONSTANT)
        !! evaluate the models at this cell with attenuation 
        call model_eval_piecewise_constant(model,i_cell,freq,vel,       &
                                           rho_loc,ctx_err)
          rho  (:) = rho_loc
          kappa(:) = rho_loc * (vel**2)

      case(tag_MODEL_PPOLY)   
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the reference 
          !! element coordinates directly. 
          !! ***********************************************************
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          call model_eval_piecewise_polynomial(model,i_cell,xpt,freq,   &
                                               vel,rho_loc,ctx_err)
          rho  (iquad) = rho_loc
          kappa(iquad) = rho_loc * (vel**2)
        end do


      case(tag_MODEL_SEP)
        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the local
          !! position
          !! ***********************************************************
          !! find its global position
          !! we have jac3D * (x_ref)^T + (x)_node1 = (x)al
          quad_pt(:) = dble  (ctx_dg%quadGL_pts(:,iquad))
          xpt    (:) = matmul(dble(ctx_dg%jac(:,:,i_cell)), quad_pt) +  &
                       dble(node_coo(:,1))

          call model_eval_sep(model,xpt,freq,vel,rho_loc,ctx_err)
          rho  (iquad) = rho_loc
          kappa(iquad) = rho_loc * (vel**2)
       end do

      case(tag_MODEL_DOF)
        !! compute the position of all quadrature points
        allocate(xquad_all(ctx_mesh%dim_domain,ctx_dg%quadGL_npts))
        allocate(vel_all  (ctx_dg%quadGL_npts))
        xquad_all = 0.d0
        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! local position coordinates
          !! ***********************************************************
          !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          xquad_all(:,iquad) = xpt(:)
       end do
       !! compute the models value
       call model_eval_dof_refelem(model,i_cell,xquad_all,              & 
                           ctx_dg%quadGL_npts,freq,vel_all,rho,ctx_err)
       kappa(:) = rho * (vel_all**2)
       deallocate(xquad_all)
       deallocate(vel_all  )

      case default
        ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                        " [build_quadrature] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select 

    if(ctx_err%ierr .ne. 0) return
    
    !! -----------------------------------------------------------------
    !! Create local matrices, only the face matrix must adapted to 
    !! the different order because of neighbors, otherwize, we 
    !! actually only need the current cell order to deal with.
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    !!  VOLUME MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! -----------------------------------------------------------------
    k       = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)
    vol_phi_phi_rho   = 0.d0
    vol_phi_phi_kappa = 0.d0
    vol_dphi_phi      = 0.d0

    do i=1, n_dof ; do j=1, n_dof
      quad_rho = 0.d0
      quad_kap = 0.d0
      quad_dphi= 0.d0
     
      do iquad=1,ctx_dg%quadGL_npts
        !! REMARK: **********************************************************
        !! ********** Everything is still computed on the reference element
        !! ********** we just incorporate a possibility for model variation
        !! ********** that is, the multiplication with det_jac for the local
        !! ********** element remains in the creation of matrix A and C.
        !! END REMARK: ******************************************************

        !! for the volumic integrals
        !! the precomputation already has the weight.
        quad_rho = quad_rho + rho(iquad) *                            &
                              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        quad_kap = quad_kap + 1.d0/kappa(iquad) *                     &
                              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)

        !!  dPhi.Phi
        do i_dim=1,ctx_dg%dim_domain
          quad_dphi(i_dim) = quad_dphi(i_dim) +                       &
                             ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,i_dim)
        end do
    
      end do
      !! *************************************************************
      !! WE REMAIN ON THE REFERENCE ELEMENT, THIS SIMPLY 
      !! CORRESPONDS TO THE DET_JAC THAT WE USE LATER ON.
      !! *************************************************************
      ! ---------------------------------------------------------
      !!     vol_matrix
      vol_phi_phi_rho  (i,j)  =  real(quad_rho ,kind=RKIND_POL)
      vol_phi_phi_kappa(i,j)  = cmplx(quad_kap ,kind=RKIND_POL)
      !! reorder
      vol_dphi_phi     (i,j,:)=  real(quad_dphi,kind=RKIND_POL)
    end do ; end do

    !! -----------------------------------------------------------------
    !!  SURFACE MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !!  We only need to sum the pre-computed array and the 
    !!  passage towards the local element is made, as usual, in the 
    !!  creations of matrix A & B & L, etc.
    !! -----------------------------------------------------------------
    !! we need the second loop over the different orders ...............
    do k = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order(k)
     
      do l = 1, ctx_dg%n_different_order
        n_dof_l = ctx_dg%n_dof_per_order (l)
        face_phi_xi(k,l)%array = 0.d0

        do i=1, n_dof ; do j=1, n_dof_l
          
          quad_face = 0.d0
          do iquad=1,ctx_dg%quadGL_face_npts
            !! weight is already taken into account, all faces at once here
            quad_face = quad_face +                                     &
                        ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,:)
          end do
          !! update all faces.
          face_phi_xi(k,l)%array(i,j,:)=real(quad_face(:),kind=RKIND_POL)

        end do ; end do 

      end do !! end second loop on the different orders.          
    end do !! end first loop on the different orders.

    deallocate(kappa)
    deallocate(rho)

    return
  end subroutine hdg_build_quadrature_int_3D
  
end module m_matrix_utils
