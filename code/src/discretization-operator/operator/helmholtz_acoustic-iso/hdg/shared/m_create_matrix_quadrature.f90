!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_matrix_quadrature.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> HELMHOLTZ ACOUSTIC propagator using the 
!> HDG discretization with quadrature rules to 
!> approximate the integrals.
!
!------------------------------------------------------------------------------
module m_create_matrix_quadrature

  !! module used -------------------------------------------------------
  use omp_lib
  use, intrinsic                   :: ieee_arithmetic
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MAT, IKIND_MESH, RKIND_MAT, &
                                      RKIND_POL
  use m_ctx_parallelism,        only: t_parallelism
  !> matrix and cartesian domain types
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_tag_bc
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_piecewise_constant
  use m_matrix_utils,           only: hdg_build_C_1D_line,              &
                                      hdg_build_C_2D_tri,               &
                                      hdg_build_C_3D_tet,               &
                                      hdg_build_quadrature_int_1D,      &
                                      hdg_build_quadrature_int_2D,      &
                                      hdg_build_quadrature_int_3D,      &
                                      hdg_build_Ainv_1D_line_quadrature,&
                                      hdg_build_Ainv_2D_tri_quadrature, &
                                      hdg_build_Ainv_3D_tet_quadrature
  use m_array_types,            only: t_array2d_real,t_array3d_real,    &
                                      t_array2d_complex,array_allocate, &
                                      array_deallocate
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: create_matrix_quadrature_1D
  public  :: create_matrix_quadrature_2D
  public  :: create_matrix_quadrature_3D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from HELMHOLTZ ACOUSTIC using HDG in 1D
  !
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[in]    model          : model parameters
  !> @param[inout] ctx_dg         : type DG
  !> @param[out]   dim_nnz_loc    : local nonzeros
  !> @param[inout] ilin(:)        : matrix I
  !> @param[inout] icol(:)        : matrix J
  !> @param[inout] Aval(:)        : matrix values
  !> @param[in]    frequency      : frequency
  !> @param[in]    flag_inv       : indicates inversion
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_quadrature_1D(ctx_mesh,model,ctx_dg,         &
                                         dim_nnz_loc,Alin,Acol,Aval,    &
                                         freq,flag_inv,ctx_err)

    implicit none
    type(t_domain)                     ,intent(in)      :: ctx_mesh
    type(t_model)                      ,intent(in)      :: model
    type(t_discretization)             ,intent(inout)   :: ctx_dg
    integer(kind=8)                    ,intent(out)     :: dim_nnz_loc
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Alin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Acol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout)   :: Aval(:)
    complex(kind=8)                    ,intent(in)      :: freq
    logical                            ,intent(in)      :: flag_inv
    type(t_error)                      ,intent(inout)   :: ctx_err
    !! local
    integer(kind=IKIND_MAT)             :: nnz_exact
    real   (kind=8)                     :: tol
    !! hdg
    complex(kind=8), allocatable :: coeff(:,:)
    complex(kind=8), allocatable :: hdgB(:,:)
    complex(kind=8), allocatable :: hdgC(:,:)
    complex(kind=8), allocatable :: hdgL(:,:)
    complex(kind=8), allocatable :: sumface_phii_phij(:,:)
    complex(kind=8), allocatable :: face_phii_xij(:,:,:)
    complex(kind=8), allocatable :: hdgAinv(:,:)
    complex(kind=8)              :: temp,vel,kappa
    real   (kind=8)              :: penalization(2) !! 1D segment, 2 faces
    real   (kind=8)              :: rho
    !! local for quadrature rules
    type(t_array2d_real)   ,allocatable :: vol_phi_phi_rho(:)
    type(t_array2d_complex),allocatable :: vol_phi_phi_kappa(:)
    type(t_array3d_real)   ,allocatable :: vol_dphi_phi(:)
    type(t_array3d_real)   ,allocatable :: face_phi_phi(:,:)

    integer(kind=IKIND_MESH) :: i_cell,face,neigh,index_face
    integer(kind=IKIND_MAT)  :: iline, icol
    integer        :: order,i_orient,dim_var,ndof_vol_l
    integer        :: i_order_cell,ndof_vol,ndof_max_vol
    integer        :: ndof_max_face,dim_sol,ndof_face_tot,ndof_face,n
    integer        :: i,j,index_face_dofi,index_face_dofj
    integer        :: i_o_neigh,ipos,jpos,iface,jface
    integer        :: ndf_edges_elem (3) !! 1D segment, 2 faces + 1 to sum
    integer        :: i_order_neigh  (2) !! 1D segment, 2 faces
    integer        :: ndof_face_neigh(2) !! 1D segment, 2 faces
    integer        :: ndof_vol_neigh (2) !! 1D segment, 2 faces
    !! for symmetrization of the matrix even with Dirichlet boundary.
    integer        :: ind_sym
    logical        :: flag_info_sym(2)   !! 1D segment, 2 faces
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! init informations on the matrix and hdg infos
    !! -----------------------------------------------------------------
    Alin = 0  ; Acol=0    ; Aval= 0.0

    nnz_exact = 0
    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif
    !! compute max order dof
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    dim_var       = ctx_dg%dim_var
    !! make sure it is zero at start as it is updated.
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(coeff,hdgB,hdgC,hdgAinv),    &
    !$OMP&         PRIVATE(vel,temp,penalization,kappa,rho,i_cell,j),   &
    !$OMP&         PRIVATE(face,neigh,iline,icol,ndf_edges_elem,order), &
    !$OMP&         PRIVATE(jpos,i_order_cell,ndof_face_tot,ndof_face),  &
    !$OMP&         PRIVATE(index_face_dofi,index_face_dofj,ndof_vol),   &
    !$OMP&         PRIVATE(i_o_neigh,ipos,iface,jface,i_order_neigh),   &
    !$OMP&         PRIVATE(ndof_face_neigh,ndof_vol_neigh,i_orient,n,i),&
    !$OMP&         PRIVATE(vol_phi_phi_rho,vol_phi_phi_kappa)          ,&
    !$OMP&         PRIVATE(vol_dphi_phi,face_phi_phi),                  &
    !$OMP&         PRIVATE(flag_info_sym,ind_sym,ndof_vol_l),           &
    !$OMP&         PRIVATE(hdgL,sumface_phii_phij,face_phii_xij,index_face)
    !! local array, the 2 is because of the 2 segment faces
    allocate(coeff  (2*dim_var*ndof_max_face,2*dim_var*ndof_max_face))
    allocate(hdgL   (2*dim_var*ndof_max_face,2*dim_var*ndof_max_face))
    allocate(hdgB   (2*dim_var*ndof_max_face,  dim_sol*ndof_max_vol ))
    allocate(hdgC   (  dim_sol*ndof_max_vol ,2*dim_var*ndof_max_face))
    allocate(hdgAinv(  dim_sol*ndof_max_vol ,  dim_sol*ndof_max_vol ))
    !! for integrations of basis functions
    allocate(face_phii_xij    (2,ndof_max_vol,ndof_max_face))
    allocate(sumface_phii_phij(  ndof_max_vol,ndof_max_vol ))
    !! for quadrature, once and for all
    allocate(vol_phi_phi_kappa(ctx_dg%n_different_order))
    allocate(vol_phi_phi_rho  (ctx_dg%n_different_order))
    allocate(vol_dphi_phi     (ctx_dg%n_different_order))
    allocate(face_phi_phi     (ctx_dg%n_different_order,ctx_dg%n_different_order))
    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! size depends if derivative or not
      call array_allocate(vol_phi_phi_kappa(i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi_rho  (i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_dphi_phi(i),ndof_vol,ndof_vol,            &
                          ctx_dg%dim_domain,ctx_err)
      do j = 1, ctx_dg%n_different_order
        ndof_vol_l = ctx_dg%n_dof_per_order(j)
        call array_allocate(face_phi_phi(i,j),ndof_vol,ndof_vol_l,      &
                             ctx_mesh%n_neigh_per_cell,ctx_err)
      end do
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do i_cell=1,ctx_mesh%n_cell_loc
   
      if(ctx_err%ierr .ne. 0) cycle
      
      ctx_dg%hdg%Ainv(i_cell)%array = 0.d0
      ctx_dg%hdg%Q   (i_cell)%array = 0.d0
      ctx_dg%hdg%X   (i_cell)%array = 0.d0
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array = 0.d0
        ctx_dg%hdg%D(i_cell)%array = 0.d0
      endif

      coeff=0.0; hdgAinv=0.0; hdgB=0.0; hdgC=0.0; hdgL=0.0
      sumface_phii_phij =0.0; face_phii_xij =0.0
      flag_info_sym = .false.
      ! --------------------------------------------------------->
      !! initialization of edges informations and model parameters
      i_order_cell = ctx_dg%order_to_index      (ctx_mesh%order(i_cell))
      ndof_vol     = ctx_dg%n_dof_per_order     (i_order_cell)
      !!  WARNING >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !! in one-dimension, the order of the face must be one because 
      !! there is only one point anyway.
      ndof_face    = 1 !! ctx_dg%n_dof_face_per_order(1)
      ! ----------------------------------------------------------------
     
      call hdg_build_quadrature_int_1D(ctx_dg,ctx_mesh,model,i_cell,    &
                     i_order_cell,freq,                                 &
                     vol_phi_phi_rho(i_order_cell)%array,               &
                     vol_phi_phi_kappa(i_order_cell)%array,             &
                     vol_dphi_phi(i_order_cell)%array,face_phi_phi,ctx_err)
      
      !! for boundary conditions and penalization only, we approximate 
      !! with the cell value for simplicity
      call model_eval_piecewise_constant(model,i_cell,freq,vel,rho,ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
     
      !!  TO CLARIFY >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      if(ctx_dg%penalization < 0) then
        penalization = 1./rho
      else
        penalization = ctx_dg%penalization
      end if
      ! ----------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! initialize face informations with neighbors
      !! ndf_edges_elem: for each face we store the incremental ndof
      !! ndf_edges_elem(3) = total number of dof on the edges of the element
      !! ---------------------------------------------------------------
      ndf_edges_elem(1)=0
      do iface=1,ctx_mesh%n_neigh_per_cell
        face                    = ctx_mesh%cell_face   (iface,i_cell)
        order                   = ctx_mesh%order_face  (face)
        i_order_neigh  (iface)  = ctx_dg%order_to_index(order)
        ndof_face_neigh(iface)  = ctx_dg%n_dof_face_per_order(i_order_neigh(iface))
        ndof_vol_neigh (iface)  = ctx_dg%n_dof_per_order     (i_order_neigh(iface))
        ndf_edges_elem (iface+1)=ndf_edges_elem(iface) + ndof_face_neigh(iface)
      end do
      ndof_face_tot = ndf_edges_elem(3)
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Computation of some utility matrices for the faces: we need
      !!
      !! (o) \sum_face penalization(face) (\phi_i, \phi_j)_face 
      !!                   => sumface_phii_phij(i,j), needed for A
      !! (o) (\phii, \xij) over the face for each face 
      !!                   => face_phii_xij(iface,i,j), needed for C
      !! (o) matrix L, see below, it is then adjusted if boundary cond.
      !! (o) matrix B, see below
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        !! local index of the face, for the pml coefficient ------------
        index_face  = ctx_mesh%cell_face(iface,i_cell)

        do i=1, ndof_face_neigh(iface)
          index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)

          !! L is block-diagonal with ndof_face x ndof_face blocks and
          !! is defined by - \tau (\Xi, \Xi) over the face
          !! -----------------------------------------------------------
          do j=1, ndof_face_neigh(iface)
            index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
            temp = dcmplx(face_phi_phi(i_o_neigh,i_o_neigh)%            &
                          array(index_face_dofi,index_face_dofj,iface)* &
                          ctx_dg%det_jac_face(iface,i_cell))
            hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j) =     &
                                                -penalization(iface)*temp
         
          enddo

          !! B is has one block per face, each block is of size 
          !! Ndof_face x Ndof_vol and the content is given by 
          !! (\Xi, \Phi) over the face
          !! -----------------------------------------------------------
          do j=1,ndof_vol
            !! careful that i is the face and j the vol ................
            temp = dcmplx(face_phi_phi(i_order_cell,i_o_neigh)%         &
                          array(j,index_face_dofi,iface) *              &
                   ctx_dg%det_jac_face(iface,i_cell))
            face_phii_xij(iface,j,i) = temp
            hdgB(ndf_edges_elem(iface)+i,           j) = temp*          &
              ctx_dg%normal(1,iface,i_cell)*ctx_dg%pml_face_coeff(1,index_face)
            hdgB(ndf_edges_elem(iface)+i,  ndof_vol+j) = temp*          &
                                                         penalization(iface)
          end do
        end do
        
        !! -------------------------------------------------------------
        !! Here we compute the term 
        !! \sum_face penalization(face) * (\phi_i, \phi_j)_face
        !! -------------------------------------------------------------
        do i = 1, ndof_face
          index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
          do j = 1, ndof_face
            index_face_dofj = ctx_dg%face_map(i_order_cell)%array(j,iface)
            !! the face_matrix must use order 1 here >>>>>>>>>>>>>>>>>>>
            temp = ctx_dg%det_jac_face(iface,i_cell) *                  &
                   dcmplx(face_phi_phi(i_order_cell,i_order_cell)%      &
                          array(index_face_dofi,index_face_dofj,iface))
            sumface_phii_phij(index_face_dofi,index_face_dofj) =        &
            sumface_phii_phij(index_face_dofi,index_face_dofj) + temp * &
                                                      penalization(iface) 
          end do
        end do

      end do

      ! ----------------------------------------------------------------
      !! adjust B in case we have an actual boundary
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        neigh     = ctx_mesh%cell_neigh(iface,i_cell)
        
        select case (neigh)
          !! absorbing boundary conditions given by
          !! 0 = \partial_n p - i\omega c^{-1} p
          !! replacing \partial_n p from Euler gives
          !!   i\omega\rho v.n - i\omega c^{-1} p = 0
          !!   v.n - (\rho c)^{-1} p = 0
          case(tag_absorbing)

            !! ---------------------------------------------------------
            select case(trim(adjustl(ctx_mesh%Zimpedance)))
              case('Sommerfeld_0')
                !! - Z (\xi,\xi) over the face
                ! ------------------------------------>
                !! in 1D the face is order 1 all the time
                ! ------------------------------------>
                do i=1, ndof_face
                  index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
                  do j=1,ndof_face
                    index_face_dofj = ctx_dg%face_map(i_order_cell)%array(j,iface)
                    temp = dcmplx(face_phi_phi(i_order_cell,i_order_cell)%     &
                                  array(index_face_dofi,index_face_dofj,iface)*&
                                  ctx_dg%det_jac_face(iface,i_cell))
                
                    !! update L with - Z (\xi,\xi)
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)=&
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)-&
                                                      temp*(1./(rho*vel))
                  end do
                end do

              case default
                ctx_err%msg   = "** ERROR: Unrecognized Impedance "  // &
                                "condition "//                          &
                                trim(adjustl(ctx_mesh%Zimpedance))//    &
                                " for Robin Boundary Condition [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
            end select
            !! ---------------------------------------------------------

          !! other types
          case(tag_freesurface,tag_ghost_cell,tag_wallsurface,          &
               tag_neumann,tag_dirichlet)
            !! TAG_GHOST ==> nothing special

            !! TAG_WALL = TAG_NEUMANN
            !!    ==> \partial_\nu Pressure = 0, i.e., displacement = 0
            !!               nothing because Neuman BC is automatic

            !! TAG_FREE = TAG_DIRICHLET
            !!           ==> !! -------------------------- <!
                             !! it will be done at the end <!
                             !! -------------------------- <!
          
          !! unsupported BC ............................................
          case(tag_planewave,tag_pml_circular,tag_pml,                  &
               tag_center_neumann,tag_planewave_scatt,                  &
               tag_aldirichlet_dlrobin,tag_deltapdirichlet_dldirichlet, &
               tag_analytic_orthorhombic)
            ctx_err%msg   = "** ERROR: Unsupported " //                 &
                            "boundary condition [create_matrix]"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle
          
          case default

        end select
      
      end do
      if(ctx_err%ierr .ne. 0) cycle !! non-shared error

      ! ----------------------------------------------------------------
      ! construct HDG matrices A and C 
      ! ----------------------------------------------------------------
      n = ndof_vol*dim_sol
      !! construction Ainv
      call hdg_build_Ainv_1D_line_quadrature(hdgAinv,                   &
                   vol_phi_phi_kappa(i_order_cell)%array,               &
                   vol_phi_phi_rho(i_order_cell)%array,                 &
                   vol_dphi_phi(i_order_cell)%array,                    &
                   sumface_phii_phij,ctx_dg%inv_jac(:,:,i_cell),        &
                   ctx_dg%det_jac(i_cell),ndof_vol,freq,                &
                   ctx_dg%pml_cell_coeff(:,i_cell),ctx_err)
      if(ctx_err%ierr .ne. 0) cycle

      !! construction C
      call hdg_build_C_1D_line(hdgC,face_phii_xij,penalization,ndof_vol,&
                               ndof_face_neigh,ndf_edges_elem,          &
                               ctx_dg%normal(:,:,i_cell),               &
                               ctx_dg%pml_cell_coeff(:,i_cell))

      !! update saved matrices
      ctx_dg%hdg%Ainv(i_cell)%array(1:n,1:n) = cmplx(hdgAinv(1:n,1:n),kind=RKIND_MAT)
      !! Q =-A^-1 C
      ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot) = cmplx(-matmul(  &
                   hdgAinv(1:n,1:n),hdgC(1:n,1:ndof_face_tot)),kind=RKIND_MAT)
      !! X =-B A^-1
      ctx_dg%hdg%X(i_cell)%array(1:ndof_face_tot,1:n) = cmplx(-matmul(  &
                  hdgB(1:ndof_face_tot,1:n),hdgAinv(1:n,1:n)),kind=RKIND_MAT)
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array(1:ndof_face_tot,1:n)=cmplx(          &
                     matmul(transpose(conjg(hdgC(1:n,1:ndof_face_tot))),&
                            transpose(conjg(hdgAinv(1:n,1:n)))), kind=RKIND_MAT)
        ctx_dg%hdg%D(i_cell)%array(1:n,1:ndof_face_tot)=cmplx(          &
            matmul(transpose(conjg(hdgAinv(1:n,1:n))),                  &
                   transpose(conjg(hdgB(1:ndof_face_tot,1:n)))), kind=RKIND_MAT)
      endif

      !! local matrix coeff = L - B*A^-1*C
      !!                    = L + B*Q
      coeff(1:ndof_face_tot,1:ndof_face_tot) =                &
                      hdgL(1:ndof_face_tot,1:ndof_face_tot) + &
                           matmul(hdgB(1:ndof_face_tot,1:n),  &
                           dcmplx(ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot)))


      !! -----------------------------------------------
      !! readjust the free surface case to identity
      !! it is similar to dirichlet and planewave
      !! because we impose the planewave with dirichlet.
      !! -----------------------------------------------
      do iface=1, ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        if(neigh .eq. tag_freesurface .or.       &
           neigh .eq. tag_dirichlet) then
          !! create a Identity matrix coefficients to impose free
          !! surface in pressure, i.e. pressure(x_\Gamma) = 0
          coeff(  ndf_edges_elem(iface)+1: &
                  ndf_edges_elem(iface)+ndof_face,:)=0.0
          !! -----------------------------------------------------------
          !! keep lines. coeff(:,ndf_edges_elem(iface)+1: &
          !! keep lines.         ndf_edges_elem(iface)+ndof_face)=0.0
          !! save lines to symmetrize the matrix.
          flag_info_sym(iface) = .true. 
          !! we can use ndof_face above because the face on the boundary
          !! anyway has the order of the cell, but it should be 
          !! ndof_face_neigh(iface)
          ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) =                 &
            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) +               &
            !! only the other faces.
            (ndof_face_tot - ndof_face_neigh(iface))*ndof_face_neigh(iface)
          !! -----------------------------------------------------------
          !! diagonal
          do i=1, ndof_face
            coeff(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+i) = 1.0
          end do
        end if
      end do
      !! allocate arrays if non-zeros
      if(ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) .ne. 0) then
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_line(i_cell),   &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_col(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_val(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
         ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array = 0.d0
      end if 
      ind_sym = 0
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Update global coefficients
      !! we find the appropriate index for the face first
      do iface=1,ctx_mesh%n_neigh_per_cell
        if(ctx_err%ierr .ne. 0) cycle
        
        do ipos=1,ndf_edges_elem(iface+1)-ndf_edges_elem(iface)
          if(ctx_err%ierr .ne. 0) cycle
          
          !! line info -------------------------------------------------
          i        = ndf_edges_elem(iface)+ipos
          iline    = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(iface,i_cell))
          i_o_neigh= i_order_neigh(iface)
          i_orient = ctx_mesh%cell_face_orientation(iface,i_cell)
          iline=iline + ctx_dg%hdg%idof_face(i_orient,i_o_neigh)%array(ipos)

          !! col info --------------------------------------------------
          do jface=1,ctx_mesh%n_neigh_per_cell
            if(ctx_err%ierr .ne. 0) cycle
            
            do jpos=1,ndf_edges_elem(jface+1)-ndf_edges_elem(jface)
              if(ctx_err%ierr .ne. 0) cycle

              j        = ndf_edges_elem(jface)+jpos
              icol     = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(jface,i_cell))
              i_o_neigh= i_order_neigh(jface)
              i_orient = ctx_mesh%cell_face_orientation(jface,i_cell)
              icol=icol + ctx_dg%hdg%idof_face (i_orient,i_o_neigh)%array(jpos)

              !! save Dirichlet BC infos to make the matrix symmetric    
              !! by modifying the rhs.
              if( flag_info_sym(jface) .and. (.not.flag_info_sym(iface))) then
                ind_sym = ind_sym + 1
                ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array(ind_sym) = &
                  int(iline, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array(ind_sym) = &
                  int(icol, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array(ind_sym) = &
                                       cmplx(coeff(i,j),kind=RKIND_POL)
                !! this coeff can be set to zero now.
                coeff(i,j) = 0.d0
              end if
              
              !! update symmetric or not
              if(abs(coeff(i,j))>tol) then
                !! The BC do not remove the symmetry of the matrix
                if(icol >= iline) then
                  !! the matrix may not be symmetric because of 
                  !! Dirichlet boundary Conditions... if(icol >= iline) then
                  !! also because if PML.
                  !$OMP CRITICAL
                  nnz_exact       = nnz_exact + 1
                  Alin(nnz_exact) = iline
                  Acol(nnz_exact) = icol
                  Aval(nnz_exact) = cmplx(coeff(i,j),kind=RKIND_MAT)
                  !$OMP END CRITICAL
                  
                  !! check too high values 
                  if(real (Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.aimag(Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.ieee_is_nan(real (Aval(nnz_exact))) .or.                &
                     ieee_is_nan(aimag(Aval(nnz_exact)))) then
                    write(ctx_err%msg,'(a,i0,a,es9.2,a)') "** ERROR: "//  &
                          "the matrix has entry infinity or NaN."//       &
                          " Current matrix precision kind is ", RKIND_MAT,&
                          " (8 at most), which amounts to a"     //       &
                          " maximal value of ",                           &
                          huge(real(1.0,kind=RKIND_MAT)), " [create_matrix]"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    cycle
                  end if
                end if
              end if

            end do
          end do
        end do
      end do
      ! ----------------------------------------------------------------

    end do
    !$OMP END DO

    dim_nnz_loc = nnz_exact

    deallocate(hdgL             )
    deallocate(coeff            )
    deallocate(face_phii_xij    )
    deallocate(sumface_phii_phij)
    deallocate(hdgC             )
    deallocate(hdgB             )
    deallocate(hdgAinv          )
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi_kappa(i))
      call array_deallocate(vol_phi_phi_rho  (i))
      call array_deallocate(vol_dphi_phi(i))
      do j = 1, ctx_dg%n_different_order
        call array_deallocate(face_phi_phi(i,j))
      end do
    end do
    deallocate(vol_phi_phi_kappa)
    deallocate(vol_phi_phi_rho  )
    deallocate(vol_dphi_phi     )
    deallocate(face_phi_phi     )
    !$OMP END PARALLEL
    
    return
  end subroutine create_matrix_quadrature_1D
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from HELMHOLTZ ACOUSTIC using HDG in 2D
  !
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[in]    model          : model parameters
  !> @param[inout] ctx_dg         : type DG
  !> @param[out]   dim_nnz_loc    : local nonzeros
  !> @param[inout] ilin(:)        : matrix I
  !> @param[inout] icol(:)        : matrix J
  !> @param[inout] Aval(:)        : matrix values
  !> @param[in]    frequency      : frequency
  !> @param[in]    flag_inv       : indicates inversion
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_quadrature_2D(ctx_mesh,model,ctx_dg,         &
                                         dim_nnz_loc,Alin,Acol,Aval,    &
                                         freq,flag_inv,ctx_err)

    implicit none
    type(t_domain)                     ,intent(in)      :: ctx_mesh
    type(t_model)                      ,intent(in)      :: model
    type(t_discretization)             ,intent(inout)   :: ctx_dg
    integer(kind=8)                    ,intent(out)     :: dim_nnz_loc
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Alin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Acol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout)   :: Aval(:)
    complex(kind=8)                    ,intent(in)      :: freq
    logical                            ,intent(in)      :: flag_inv
    type(t_error)                      ,intent(inout)   :: ctx_err
    !! local
    integer(kind=IKIND_MAT)             :: nnz_exact
    real   (kind=8)                     :: tol
    !! hdg
    complex(kind=8), allocatable :: coeff(:,:)
    complex(kind=8), allocatable :: hdgB(:,:)
    complex(kind=8), allocatable :: hdgC(:,:)
    complex(kind=8), allocatable :: hdgL(:,:)
    complex(kind=8), allocatable :: sumface_phii_phij(:,:)
    complex(kind=8), allocatable :: face_phii_xij(:,:,:)
    complex(kind=8), allocatable :: hdgAinv(:,:)
    complex(kind=8)              :: temp,vel,kappa
    complex(kind=8)              :: penalization(3) !! 2D triangle, 3 faces
    real   (kind=8)              :: rho
    !! local for quadrature rules
    type(t_array2d_real)   ,allocatable :: vol_phi_phi_rho(:)
    type(t_array2d_complex),allocatable :: vol_phi_phi_kappa(:)
    type(t_array3d_real)   ,allocatable :: vol_dphi_phi(:)
    type(t_array3d_real)   ,allocatable :: face_phi_phi(:,:)

    integer(kind=IKIND_MESH) :: i_cell,face,neigh,index_face
    integer(kind=IKIND_MAT)  :: iline, icol
    integer        :: ndf_edges_elem(4),order,i_orient,dim_var
    integer        :: i_order_cell,ndof_vol,ndof_max_vol,ndof_vol_l
    integer        :: ndof_max_face,dim_sol,ndof_face_tot,ndof_face,n
    integer        :: i,j,index_face_dofi,index_face_dofj
    integer        :: i_o_neigh,ipos,jpos,iface,jface
    integer        :: i_order_neigh  (3) !! 2D triangle, 3 faces
    integer        :: ndof_face_neigh(3) !! 2D triangle, 3 faces
    integer        :: ndof_vol_neigh (3) !! 2D triangle, 3 faces
    !! for symmetrization of the matrix even with Dirichlet boundary.
    integer        :: ind_sym
    logical        :: flag_info_sym(3)   !! 2D triangle, 3 faces
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! init informations on the matrix and hdg infos
    !! -----------------------------------------------------------------
    Alin = 0  ; Acol=0    ; Aval= 0.0

    nnz_exact = 0
    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif
    !! compute max order dof
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    dim_var       = ctx_dg%dim_var
    !! make sure it is zero at start as it is updated.
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(coeff,hdgB,hdgC,hdgAinv),    &
    !$OMP&         PRIVATE(vel,temp,penalization,kappa,rho,i_cell,j),   &
    !$OMP&         PRIVATE(face,neigh,iline,icol,ndf_edges_elem,order), &
    !$OMP&         PRIVATE(jpos,i_order_cell,ndof_face_tot,ndof_face),  &
    !$OMP&         PRIVATE(index_face_dofi,index_face_dofj,ndof_vol),   &
    !$OMP&         PRIVATE(i_o_neigh,ipos,iface,jface,i_order_neigh),   &
    !$OMP&         PRIVATE(ndof_face_neigh,ndof_vol_neigh,i_orient,n,i),&
    !$OMP&         PRIVATE(vol_phi_phi_rho,vol_phi_phi_kappa,ind_sym),  &
    !$OMP&         PRIVATE(vol_dphi_phi,face_phi_phi,flag_info_sym),    &
    !$OMP&         PRIVATE(index_face,ndof_vol_l),                      &
    !$OMP&         PRIVATE(hdgL,sumface_phii_phij,face_phii_xij)
    !! local array, the 3 is because of the 3 triangle faces
    allocate(coeff  (3*dim_var*ndof_max_face,3*dim_var*ndof_max_face))
    allocate(hdgL   (3*dim_var*ndof_max_face,3*dim_var*ndof_max_face))
    allocate(hdgB   (3*dim_var*ndof_max_face,  dim_sol*ndof_max_vol ))
    allocate(hdgC   (  dim_sol*ndof_max_vol ,3*dim_var*ndof_max_face))
    allocate(hdgAinv(  dim_sol*ndof_max_vol ,  dim_sol*ndof_max_vol ))
    !! for integrations of basis functions
    allocate(face_phii_xij    (3,ndof_max_vol,ndof_max_face))
    allocate(sumface_phii_phij(  ndof_max_vol,ndof_max_vol ))
    !! for quadrature, once and for all
    allocate(vol_phi_phi_kappa(ctx_dg%n_different_order))
    allocate(vol_phi_phi_rho  (ctx_dg%n_different_order))
    allocate(vol_dphi_phi     (ctx_dg%n_different_order))
    allocate(face_phi_phi     (ctx_dg%n_different_order,ctx_dg%n_different_order))
    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! size depends if derivative or not
      call array_allocate(vol_phi_phi_kappa(i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi_rho  (i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_dphi_phi(i),ndof_vol,ndof_vol,            &
                          ctx_dg%dim_domain,ctx_err)
      do j = 1, ctx_dg%n_different_order
        ndof_vol_l = ctx_dg%n_dof_per_order(j)
        call array_allocate(face_phi_phi(i,j),ndof_vol,ndof_vol_l,      &
                             ctx_mesh%n_neigh_per_cell,ctx_err)
      end do
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do i_cell=1,ctx_mesh%n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      
      ctx_dg%hdg%Ainv(i_cell)%array = 0.d0
      ctx_dg%hdg%Q   (i_cell)%array = 0.d0
      ctx_dg%hdg%X   (i_cell)%array = 0.d0
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array = 0.d0
        ctx_dg%hdg%D(i_cell)%array = 0.d0
      endif

      coeff=0.0; hdgAinv=0.0; hdgB=0.0; hdgC=0.0; hdgL=0.0
      sumface_phii_phij =0.0; face_phii_xij =0.0
      flag_info_sym = .false.
      ! --------------------------------------------------------->
      !! initialization of edges informations and model parameters
      i_order_cell = ctx_dg%order_to_index      (ctx_mesh%order(i_cell))
      ndof_vol     = ctx_dg%n_dof_per_order     (i_order_cell)
      ndof_face    = ctx_dg%n_dof_face_per_order(i_order_cell)

      call hdg_build_quadrature_int_2D(ctx_dg,ctx_mesh,model,i_cell,    &
                     i_order_cell,freq,                                 &
                     vol_phi_phi_rho(i_order_cell)%array,               &
                     vol_phi_phi_kappa(i_order_cell)%array,             &
                     vol_dphi_phi(i_order_cell)%array,face_phi_phi,ctx_err)
      
      !! for boundary conditions and penalization only, we approximate 
      !! with the cell value for simplicity
      call model_eval_piecewise_constant(model,i_cell,freq,vel,rho,ctx_err)
      if(ctx_err%ierr .ne. 0) cycle

      !!  TO CLARIFY >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      if(ctx_dg%penalization < 0) then
        penalization = 1./rho
      else
        penalization = ctx_dg%penalization
      end if
      ! ----------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! initialize face informations with neighbors
      !! ndf_edges_elem: for each face we store the incremental ndof
      !! ndf_edges_elem(4) = total number of dof on the edges of the element
      !! ---------------------------------------------------------------
      ndf_edges_elem(1)=0
      do iface=1,ctx_mesh%n_neigh_per_cell
        face                    = ctx_mesh%cell_face   (iface,i_cell)
        order                   = ctx_mesh%order_face  (face)
        i_order_neigh  (iface)  = ctx_dg%order_to_index(order)
        ndof_face_neigh(iface)  = ctx_dg%n_dof_face_per_order(i_order_neigh(iface))
        ndof_vol_neigh (iface)  = ctx_dg%n_dof_per_order     (i_order_neigh(iface))
        ndf_edges_elem (iface+1)=ndf_edges_elem(iface) + ndof_face_neigh(iface)
      end do
      ndof_face_tot = ndf_edges_elem(4)
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Computation of some utility matrices for the faces: we need
      !!
      !! (o) \sum_face penalization(face) (\phi_i, \phi_j)_face 
      !!                   => sumface_phii_phij(i,j), needed for A
      !! (o) (\phii, \xij) over the face for each face 
      !!                   => face_phii_xij(iface,i,j), needed for C
      !! (o) matrix L, see below, it is then adjusted if boundary cond.
      !! (o) matrix B, see below
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        !! local index of the face, for the pml coefficient ------------
        index_face  = ctx_mesh%cell_face(iface,i_cell)

        do i=1, ndof_face_neigh(iface)
          index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)

          !! L is block-diagonal with ndof_face x ndof_face blocks and
          !! is defined by - \tau (\Xi, \Xi) over the face
          !! -----------------------------------------------------------
          do j=1, ndof_face_neigh(iface)
            index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
            temp = dcmplx(face_phi_phi(i_o_neigh,i_o_neigh)%            &
                          array(index_face_dofi,index_face_dofj,iface)* &
                          ctx_dg%det_jac_face(iface,i_cell))
            hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j) =     &
                                                -penalization(iface)*temp
         
          enddo

          !! B is has one block per face, each block is of size 
          !! Ndof_face x Ndof_vol and the content is given by 
          !! (\Xi, \Phi) over the face
          !! -----------------------------------------------------------
          do j=1,ndof_vol
            !! careful that i is the face and j the vol ................
            temp = dcmplx(face_phi_phi(i_order_cell,i_o_neigh)%         &
                          array(j,index_face_dofi,iface) *              &
                   ctx_dg%det_jac_face(iface,i_cell))
            face_phii_xij(iface,j,i) = temp
            hdgB(ndf_edges_elem(iface)+i,           j) = temp*          &
              ctx_dg%normal(1,iface,i_cell)*ctx_dg%pml_face_coeff(1,index_face)
            hdgB(ndf_edges_elem(iface)+i,  ndof_vol+j) = temp*          & 
              ctx_dg%normal(2,iface,i_cell)*ctx_dg%pml_face_coeff(2,index_face)
            hdgB(ndf_edges_elem(iface)+i,2*ndof_vol+j) = temp*          &
                                                         penalization(iface)
          end do
        end do

        !! -------------------------------------------------------------
        !! Here we compute the term 
        !! \sum_face penalization(face) * (\phi_i, \phi_j)_face
        !! -------------------------------------------------------------
        !! we can use ndof_face because it is the discretization of 
        !! the volume quantity p, which is defined by the cell order
        do i = 1, ndof_face
          index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
          do j = 1, ndof_face
            index_face_dofj = ctx_dg%face_map(i_order_cell)%array(j,iface)
            temp = ctx_dg%det_jac_face(iface,i_cell) *                  &
                   dcmplx(face_phi_phi(i_order_cell,i_order_cell)%      &
                          array(index_face_dofi,index_face_dofj,iface))
            sumface_phii_phij(index_face_dofi,index_face_dofj) =        &
            sumface_phii_phij(index_face_dofi,index_face_dofj) + temp * &
                                                      penalization(iface) 
          end do
        end do

      end do

      ! ----------------------------------------------------------------
      !! adjust B in case we have an actual boundary
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        neigh     = ctx_mesh%cell_neigh(iface,i_cell)
        
        select case (neigh)
          !! absorbing boundary conditions given by
          !! 0 = \partial_n p - i\omega c^{-1} p
          !! replacing \partial_n p from Euler gives
          !!   i\omega\rho v.n - i\omega c^{-1} p = 0
          !!   v.n - (\rho c)^{-1} p = 0
          case(tag_absorbing,tag_planewave_scatt)

            !! ---------------------------------------------------------
            select case(trim(adjustl(ctx_mesh%Zimpedance)))
              case('Sommerfeld_0')
                !! - Z (\xi,\xi) over the face
                !! we can use ndof_face above because the face on the 
                !! boundary anyway has the order of the cell, it could be 
                !! ndof_face_neigh(iface)
                do i=1, ndof_face
                  index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
                  do j=1,ndof_face
                    index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
                    temp = dcmplx(face_phi_phi(i_order_cell,i_order_cell)%     &
                                  array(index_face_dofi,index_face_dofj,iface)*&
                                  ctx_dg%det_jac_face(iface,i_cell))
                
                    !! update L with - Z (\xi,\xi)
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)=&
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)-&
                                                      temp*(1./(rho*vel))
                  end do
                end do

              case default
                ctx_err%msg   = "** ERROR: Unrecognized Impedance "  // &
                                "condition "//                          &
                                trim(adjustl(ctx_mesh%Zimpedance))//    &
                                " for Robin Boundary Condition [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
            end select
            !! ---------------------------------------------------------

          !! other types
          case(tag_freesurface,tag_ghost_cell,tag_wallsurface,          &
               tag_neumann,tag_dirichlet,tag_planewave)
            !! TAG_GHOST ==> nothing special

            !! TAG_WALL = TAG_NEUMANN
            !!    ==> \partial_\nu Pressure = 0, i.e., displacement = 0
            !!               nothing because Neuman BC is automatic

            !! TAG_FREE = TAG_DIRICHLET = TAG_PlANEWAVE
            !!           ==> !! -------------------------- <!
                             !! it will be done at the end <!
                             !! -------------------------- <!
          !! unsupported BC ............................................
          case(tag_pml_circular,tag_pml,tag_center_neumann,             &
               tag_aldirichlet_dlrobin,tag_deltapdirichlet_dldirichlet, &
               tag_analytic_orthorhombic)
            ctx_err%msg   = "** ERROR: Unsupported " //                 &
                            "boundary condition [create_matrix]"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle

          case default

        end select
      
      end do
      if(ctx_err%ierr .ne. 0) cycle !! non-shared error

      ! ----------------------------------------------------------------
      ! construct HDG matrices A and C 
      ! ----------------------------------------------------------------
      n = ndof_vol*dim_sol
      !! construction Ainv
      call hdg_build_Ainv_2D_tri_quadrature(hdgAinv,                    &
                   vol_phi_phi_kappa(i_order_cell)%array,               &
                   vol_phi_phi_rho(i_order_cell)%array,                 &
                   vol_dphi_phi(i_order_cell)%array,                    &
                   sumface_phii_phij,ctx_dg%inv_jac(:,:,i_cell),        &
                   ctx_dg%det_jac(i_cell),ndof_vol,freq,                &
                   ctx_dg%pml_cell_coeff(:,i_cell),ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
      
      !! construction C
      call hdg_build_C_2D_tri(hdgC,face_phii_xij,penalization,ndof_vol, &
                      ndof_face_neigh,ndf_edges_elem,                   &
                      ctx_dg%normal(:,:,i_cell),ctx_dg%pml_cell_coeff(:,i_cell))
      !! update saved matrices
      ctx_dg%hdg%Ainv(i_cell)%array(1:n,1:n) = cmplx(hdgAinv(1:n,1:n),kind=RKIND_MAT)
      !! Q =-A^-1 C
      ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot) = cmplx(-matmul(  &
                   hdgAinv(1:n,1:n),hdgC(1:n,1:ndof_face_tot)),kind=RKIND_MAT)
      !! X =-B A^-1
      ctx_dg%hdg%X(i_cell)%array(1:ndof_face_tot,1:n) = cmplx(-matmul(  &
                  hdgB(1:ndof_face_tot,1:n),hdgAinv(1:n,1:n)),kind=RKIND_MAT)
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array(1:ndof_face_tot,1:n)=cmplx(          &
                     matmul(transpose(conjg(hdgC(1:n,1:ndof_face_tot))),&
                            transpose(conjg(hdgAinv(1:n,1:n)))), kind=RKIND_MAT)
        ctx_dg%hdg%D(i_cell)%array(1:n,1:ndof_face_tot)=cmplx(          &
            matmul(transpose(conjg(hdgAinv(1:n,1:n))),                  &
                   transpose(conjg(hdgB(1:ndof_face_tot,1:n)))), kind=RKIND_MAT)
      endif

      !! local matrix coeff = L - B*A^-1*C
      !!                    = L + B*Q
      coeff(1:ndof_face_tot,1:ndof_face_tot) =                &
                      hdgL(1:ndof_face_tot,1:ndof_face_tot) + &
                           matmul(hdgB(1:ndof_face_tot,1:n),  &
                           dcmplx(ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot)))


      !! -----------------------------------------------
      !! readjust the free surface case to identity
      !! it is similar to dirichlet and planewave
      !! because we impose the planewave with dirichlet.
      !! -----------------------------------------------
      do iface=1, ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        if(neigh .eq. tag_freesurface .or.  &
           neigh .eq. tag_planewave   .or.  &
           neigh .eq. tag_dirichlet) then

          !! -------------------------------------------------------------- %
          !! The local block will be replaced by Identity to maintain the 
          !! symmetry of the matrix. However, using the weak form, we do
          !! not readily have the identity but 
          !!   \integral \Lambda \xi_j  = \integral g \xi_j   \forall j
          !! where g is my source function. That is, we have in matrix form:
          !!             hdgL \Lambda = S.
          !! 
          !! Here we save the matrix hdgL, and we will solve the local
          !! problem when we create the source. Hence we will readily
          !! have the values of Lambda, and we can use and identity block
          !! that allows us to maintain the symmetry of the matrix.
          !!
          !!  e.g., original local system would be 
          !!  (x x x)                  (x x x)                      (x x  0)
          !!  (x x x)\Lambda , and now (x x x)\Lambda, and we need  (x x  0) \Lambda
          !!  (x x x)                  (0 0 L)                      (0 0 Id)
          !!no dirichlet           dirichlet face 3               for symmetry
          !! -------------------------------------------------------------- %
          
          !! -----------------------------------------------------------
          !! replace the block of the considered face with 0.
          coeff(  ndf_edges_elem(iface)+1: &
                  ndf_edges_elem(iface)+ndof_face,:)=0.0

          !! info regarding the symmetry
          flag_info_sym(iface) = .true. 
          !! Here we count coefficients that are NOT on the line we will
          !! modify, i.e., on the columns of the other lines.
          
          !! we could use ndof_face because the face is on the boundary anyway
          ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) =                 &
            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) +               &
            !! only the other faces.
            (ndof_face_tot - ndof_face_neigh(iface))*ndof_face_neigh(iface)

          !! We have saved the local block, we can set the identity 
          do i=1, ndof_face
            coeff(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+i) = 1.0
          end do
          !! -----------------------------------------------------------
        end if
        
        !! -------------------------------------------------------------
        !! save model information for planewaves
        !! -------------------------------------------------------------
        if(neigh .eq. tag_planewave_scatt) then
          face = ctx_mesh%cell_face(iface,i_cell)       
          call array_allocate(ctx_dg%face_velocity(face),2,ctx_err)
          !! remark that we could save for all dof on the face ...
          ctx_dg%face_velocity(face)%array(1) = vel
          ctx_dg%face_velocity(face)%array(2) = rho ! used for HDG scale
        end if

        if(neigh .eq. tag_planewave) then
          !! ---------------------------------------------------------
          !! we save the hdgL block for the weak form on the boundary.
          !! we also save the velocity on the face in case we have a 
          !! planewave. In acoustic there is only one Vp
          !! ---------------------------------------------------------
          face = ctx_mesh%cell_face(iface,i_cell)       
          call array_allocate(ctx_dg%face_velocity(face),1,ctx_err)
          !! remark that we could save for all dof on the face ...
          ctx_dg%face_velocity(face)%array(1) = vel
          do i=1,dim_var
            call array_allocate(ctx_dg%ctx_symmetrize_mat%f_matbc(face,i),&
                                ndof_face_neigh(iface),                   &
                                ndof_face_neigh(iface),ctx_err)
            ctx_dg%ctx_symmetrize_mat%f_matbc(face,i)%array = 0.d0
          end do
          !! because the matrix is symmetric.
          ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize = .true.
          i_o_neigh = i_order_neigh(iface)
          !! this is the block hdgL without the penalization: \int \xi_i \xi_j 
          do i=1, ndof_face_neigh(iface)
            index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)
            do j=1, ndof_face_neigh(iface)
              index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
              temp = dcmplx(face_phi_phi(i_o_neigh,i_o_neigh)%            &
                            array(index_face_dofi,index_face_dofj,iface)* &
                            ctx_dg%det_jac_face(iface,i_cell))
            
              !! only one variable in acoustic.
              ctx_dg%ctx_symmetrize_mat%f_matbc(face,1)%array(i,j)    =temp
              !!coeff(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)=temp
            end do
          end do

        end if !! end if planewave

      end do !! end loop over faces.

      !! allocate arrays if non-zeros: these are for off-diagonal infos.
      if(ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) .ne. 0) then
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_line(i_cell),   &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_col(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_val(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
         ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array = 0.d0
      end if 
      ind_sym = 0
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Update global coefficients
      !! we find the appropriate index for the face first
      do iface=1,ctx_mesh%n_neigh_per_cell
        if(ctx_err%ierr .ne. 0) cycle
        
        do ipos=1,ndf_edges_elem(iface+1)-ndf_edges_elem(iface)
          if(ctx_err%ierr .ne. 0) cycle
          
          !! line info -------------------------------------------------
          i        = ndf_edges_elem(iface)+ipos
          iline    = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(iface,i_cell))
          i_o_neigh= i_order_neigh(iface)
          i_orient = ctx_mesh%cell_face_orientation(iface,i_cell)
          iline=iline + ctx_dg%hdg%idof_face(i_orient,i_o_neigh)%array(ipos)

          !! col info --------------------------------------------------
          do jface=1,ctx_mesh%n_neigh_per_cell
            if(ctx_err%ierr .ne. 0) cycle
            
            do jpos=1,ndf_edges_elem(jface+1)-ndf_edges_elem(jface)
              if(ctx_err%ierr .ne. 0) cycle

              j        = ndf_edges_elem(jface)+jpos
              icol     = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(jface,i_cell))
              i_o_neigh= i_order_neigh(jface)
              i_orient = ctx_mesh%cell_face_orientation(jface,i_cell)
              icol=icol + ctx_dg%hdg%idof_face (i_orient,i_o_neigh)%array(jpos)
              
              !! save Dirichlet BC infos to make the matrix symmetric by modifying the rhs.
              if( flag_info_sym(jface) .and. (.not.flag_info_sym(iface))) then
                ind_sym = ind_sym + 1
                ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array(ind_sym) = &
                  int(iline, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array(ind_sym) = &
                  int(icol, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array(ind_sym) = &
                                       cmplx(coeff(i,j),kind=RKIND_POL)
                !! this coeff can be set to zero now.
                coeff(i,j) = 0.d0
              end if
 

              !! update symmetric or not
              if(abs(coeff(i,j))>tol) then
                !! The BC do not remove the symmetry of the matrix
                if(icol >= iline) then
                  !$OMP CRITICAL
                  nnz_exact       = nnz_exact + 1
                  Alin(nnz_exact) = iline
                  Acol(nnz_exact) = icol
                  Aval(nnz_exact) = cmplx(coeff(i,j),kind=RKIND_MAT)
                  !$OMP END CRITICAL

                  !! check too high values 
                  if(real (Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.aimag(Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.ieee_is_nan(real (Aval(nnz_exact))) .or.                &
                     ieee_is_nan(aimag(Aval(nnz_exact)))) then
                    write(ctx_err%msg,'(a,i0,a,es9.2,a)') "** ERROR: "//  &
                          "the matrix has entry infinity or NaN."//       &
                          " Current matrix precision kind is ", RKIND_MAT,&
                          " (8 at most), which amounts to a"     //       &
                          " maximal value of ",                           &
                          huge(real(1.0,kind=RKIND_MAT)), " [create_matrix]"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    cycle
                  end if
                end if
              end if

            end do
          end do
        end do
      end do
      ! ----------------------------------------------------------------

    end do
    !$OMP END DO

    dim_nnz_loc = nnz_exact

    deallocate(hdgL             )
    deallocate(coeff            )
    deallocate(face_phii_xij    )
    deallocate(sumface_phii_phij)
    deallocate(hdgC             )
    deallocate(hdgB             )
    deallocate(hdgAinv          )
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi_kappa(i))
      call array_deallocate(vol_phi_phi_rho  (i))
      call array_deallocate(vol_dphi_phi(i))
      do j = 1, ctx_dg%n_different_order
        call array_deallocate(face_phi_phi(i,j))
      end do
    end do
    deallocate(vol_phi_phi_kappa)
    deallocate(vol_phi_phi_rho  )
    deallocate(vol_dphi_phi     )
    deallocate(face_phi_phi     )
    !$OMP END PARALLEL

    return
  end subroutine create_matrix_quadrature_2D


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from HELMHOLTZ ACOUSTIC using HDG in 3D
  !
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[in]    model          : model parameters
  !> @param[inout] ctx_dg         : type DG
  !> @param[out]   dim_nnz_loc    : local nonzeros
  !> @param[inout] ilin(:)        : matrix I
  !> @param[inout] icol(:)        : matrix J
  !> @param[inout] Aval(:)        : matrix values
  !> @param[in]    frequency      : frequency
  !> @param[in]    flag_inv       : indicates inversion
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_quadrature_3D(ctx_mesh,model,ctx_dg,         &
                                         dim_nnz_loc,Alin,Acol,Aval,    &
                                         freq,flag_inv,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)      :: ctx_mesh
    type(t_model)                      ,intent(in)      :: model
    type(t_discretization)             ,intent(inout)   :: ctx_dg
    integer(kind=8)                    ,intent(out)     :: dim_nnz_loc
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Alin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Acol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout)   :: Aval(:)
    complex(kind=8)                    ,intent(in)      :: freq
    logical                            ,intent(in)      :: flag_inv
    type(t_error)                      ,intent(inout)   :: ctx_err
    !! local
    integer(kind=IKIND_MAT)             :: nnz_exact
    real   (kind=8)                     :: tol
    !! hdg
    complex(kind=8), allocatable :: coeff(:,:)
    complex(kind=8), allocatable :: hdgB(:,:)
    complex(kind=8), allocatable :: hdgC(:,:)
    complex(kind=8), allocatable :: hdgL(:,:)
    complex(kind=8), allocatable :: sumface_phii_phij(:,:)
    complex(kind=8), allocatable :: face_phii_xij(:,:,:)
    complex(kind=8), allocatable :: hdgAinv(:,:)
    complex(kind=8)              :: temp,vel,kappa
    real   (kind=8)              :: penalization(4) !! 3D tetra, 4 faces
    real   (kind=8)              :: rho
    !! local for quadrature rules
    type(t_array2d_real)   ,allocatable :: vol_phi_phi_rho(:)
    type(t_array2d_complex),allocatable :: vol_phi_phi_kappa(:)
    type(t_array3d_real)   ,allocatable :: vol_dphi_phi(:)
    type(t_array3d_real)   ,allocatable :: face_phi_phi(:,:)

    integer(kind=IKIND_MESH) :: i_cell,face,neigh,index_face
    integer(kind=IKIND_MAT)  :: iline, icol
    integer        :: order,i_orient,dim_var
    integer        :: i_order_cell,ndof_vol,ndof_max_vol
    integer        :: ndof_max_face,dim_sol,ndof_face_tot,ndof_face,n
    integer        :: i,j,index_face_dofi,index_face_dofj
    integer        :: i_o_neigh,ipos,jpos,iface,jface,ndof_vol_l
    integer        :: ndf_edges_elem (5) !! 3D tetra, 4 faces + 1 to sum
    integer        :: i_order_neigh  (4) !! 3D tetra, 4 faces
    integer        :: ndof_face_neigh(4) !! 3D tetra, 4 faces
    integer        :: ndof_vol_neigh (4) !! 3D tetra, 4 faces
    !! for symmetrization of the matrix even with Dirichlet boundary.
    integer        :: ind_sym
    logical        :: flag_info_sym(4)   !! 3D tetra, 4 faces
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! init informations on the matrix and hdg infos
    !! -----------------------------------------------------------------
    Alin = 0  ; Acol=0    ; Aval= 0.0

    nnz_exact = 0
    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif
    !! compute max order dof
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    dim_var       = ctx_dg%dim_var
    !! make sure it is zero at start as it is updated.
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(coeff,hdgB,hdgC,hdgAinv),    &
    !$OMP&         PRIVATE(vel,temp,penalization,kappa,rho,i_cell,j),   &
    !$OMP&         PRIVATE(face,neigh,iline,icol,ndf_edges_elem,order), &
    !$OMP&         PRIVATE(jpos,i_order_cell,ndof_face_tot,ndof_face),  &
    !$OMP&         PRIVATE(index_face_dofi,index_face_dofj,ndof_vol),   &
    !$OMP&         PRIVATE(i_o_neigh,ipos,iface,jface,i_order_neigh),   &
    !$OMP&         PRIVATE(ndof_face_neigh,ndof_vol_neigh,i_orient,n,i),&
    !$OMP&         PRIVATE(vol_phi_phi_rho,vol_phi_phi_kappa)          ,&
    !$OMP&         PRIVATE(vol_dphi_phi,face_phi_phi),                  &
    !$OMP&         PRIVATE(flag_info_sym,ind_sym,ndof_vol_l),           &
    !$OMP&         PRIVATE(hdgL,sumface_phii_phij,face_phii_xij,index_face)
    !! local array the 4 is because of the 4 tetra faces
    allocate(coeff  (4*dim_var*ndof_max_face,4*dim_var*ndof_max_face))
    allocate(hdgL   (4*dim_var*ndof_max_face,4*dim_var*ndof_max_face))
    allocate(hdgB   (4*dim_var*ndof_max_face,  dim_sol*ndof_max_vol ))
    allocate(hdgC   (  dim_sol*ndof_max_vol ,4*dim_var*ndof_max_face))
    allocate(hdgAinv(  dim_sol*ndof_max_vol ,  dim_sol*ndof_max_vol ))
    !! for integrations of basis functions
    allocate(face_phii_xij    (4,ndof_max_vol,ndof_max_face))
    allocate(sumface_phii_phij(  ndof_max_vol,ndof_max_vol ))
    !! for quadrature, once and for all
    allocate(vol_phi_phi_kappa(ctx_dg%n_different_order))
    allocate(vol_phi_phi_rho  (ctx_dg%n_different_order))
    allocate(vol_dphi_phi     (ctx_dg%n_different_order))
    allocate(face_phi_phi     (ctx_dg%n_different_order,ctx_dg%n_different_order))
    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! size depends if derivative or not
      call array_allocate(vol_phi_phi_kappa(i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi_rho  (i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_dphi_phi(i),ndof_vol,ndof_vol,            &
                          ctx_dg%dim_domain,ctx_err)
      do j = 1, ctx_dg%n_different_order
        ndof_vol_l = ctx_dg%n_dof_per_order(j)
        call array_allocate(face_phi_phi(i,j),ndof_vol,ndof_vol_l,      &
                             ctx_mesh%n_neigh_per_cell,ctx_err)
      end do
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do i_cell=1,ctx_mesh%n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      
      ctx_dg%hdg%Ainv(i_cell)%array = 0.d0
      ctx_dg%hdg%Q   (i_cell)%array = 0.d0
      ctx_dg%hdg%X   (i_cell)%array = 0.d0
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array = 0.d0
        ctx_dg%hdg%D(i_cell)%array = 0.d0
      endif

      coeff=0.0; hdgAinv=0.0; hdgB=0.0; hdgC=0.0; hdgL=0.0
      sumface_phii_phij =0.0; face_phii_xij =0.0
      flag_info_sym = .false.
      ! --------------------------------------------------------->
      !! initialization of edges informations and model parameters
      i_order_cell = ctx_dg%order_to_index      (ctx_mesh%order(i_cell))
      ndof_vol     = ctx_dg%n_dof_per_order     (i_order_cell)
      ndof_face    = ctx_dg%n_dof_face_per_order(i_order_cell)
     
      call hdg_build_quadrature_int_3D(ctx_dg,ctx_mesh,                 &
                     model,i_cell,i_order_cell,freq,                    &
                     vol_phi_phi_rho(i_order_cell)%array,               &
                     vol_phi_phi_kappa(i_order_cell)%array,             &
                     vol_dphi_phi(i_order_cell)%array,face_phi_phi,ctx_err)
      
      !! for boundary conditions and penalization only, we approximate 
      !! with the cell value for simplicity
      call model_eval_piecewise_constant(model,i_cell,freq,vel,rho,ctx_err)
      if(ctx_err%ierr .ne. 0) cycle

      !!  TO CLARIFY >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      if(ctx_dg%penalization < 0) then
        penalization = 1./rho
      else
        penalization = ctx_dg%penalization
      end if
      ! ----------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! initialize face informations with neighbors
      !! ndf_edges_elem: for each face we store the incremental ndof
      !! ndf_edges_elem(5) = total number of dof on the edges of the element
      !! ---------------------------------------------------------------
      ndf_edges_elem(1)=0
      do iface=1,ctx_mesh%n_neigh_per_cell
        face                    = ctx_mesh%cell_face   (iface,i_cell)
        order                   = ctx_mesh%order_face  (face)
        i_order_neigh  (iface)  = ctx_dg%order_to_index(order)
        ndof_face_neigh(iface)  = ctx_dg%n_dof_face_per_order(i_order_neigh(iface))
        ndof_vol_neigh (iface)  = ctx_dg%n_dof_per_order     (i_order_neigh(iface))
        ndf_edges_elem (iface+1)=ndf_edges_elem(iface) + ndof_face_neigh(iface)
      end do
      ndof_face_tot = ndf_edges_elem(5)
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Computation of some utility matrices for the faces: we need
      !!
      !! (o) \sum_face penalization(face) (\phi_i, \phi_j)_face 
      !!                   => sumface_phii_phij(i,j), needed for A
      !! (o) (\phii, \xij) over the face for each face 
      !!                   => face_phii_xij(iface,i,j), needed for C
      !! (o) matrix L, see below, it is then adjusted if boundary cond.
      !! (o) matrix B, see below
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        !! local index of the face, for the pml coefficient ------------
        index_face  = ctx_mesh%cell_face(iface,i_cell)

        do i=1, ndof_face_neigh(iface)
          index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)

          !! L is block-diagonal with ndof_face x ndof_face blocks and
          !! is defined by - \tau (\Xi, \Xi) over the face
          !! -----------------------------------------------------------
          do j=1, ndof_face_neigh(iface)
            index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
            temp = dcmplx(face_phi_phi(i_o_neigh,i_o_neigh)%            &
                          array(index_face_dofi,index_face_dofj,iface)* &
                          ctx_dg%det_jac_face(iface,i_cell))
            hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j) =     &
                                                -penalization(iface)*temp
         
          enddo

          !! B is has one block per face, each block is of size 
          !! Ndof_face x Ndof_vol and the content is given by 
          !! (\Xi, \Phi) over the face
          !! -----------------------------------------------------------
          do j=1,ndof_vol
            !! careful that i is the face and j the vol ................
            temp = dcmplx(face_phi_phi(i_order_cell,i_o_neigh)%         &
                          array(j,index_face_dofi,iface) *              &
                   ctx_dg%det_jac_face(iface,i_cell))
            face_phii_xij(iface,j,i) = temp
            hdgB(ndf_edges_elem(iface)+i,           j) = temp*          &
              ctx_dg%normal(1,iface,i_cell)*ctx_dg%pml_face_coeff(1,index_face)
            hdgB(ndf_edges_elem(iface)+i,  ndof_vol+j) = temp*          & 
              ctx_dg%normal(2,iface,i_cell)*ctx_dg%pml_face_coeff(2,index_face)
            hdgB(ndf_edges_elem(iface)+i,2*ndof_vol+j) = temp*          & 
              ctx_dg%normal(3,iface,i_cell)*ctx_dg%pml_face_coeff(3,index_face)
            hdgB(ndf_edges_elem(iface)+i,3*ndof_vol+j) = temp*          &
                                                         penalization(iface)
          end do
        end do

        !! -------------------------------------------------------------
        !! Here we compute the term 
        !! \sum_face penalization(face) * (\phi_i, \phi_j)_face
        !! -------------------------------------------------------------
        do i = 1, ndof_face
          index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
          do j = 1, ndof_face
            index_face_dofj = ctx_dg%face_map(i_order_cell)%array(j,iface)
            temp = ctx_dg%det_jac_face(iface,i_cell) *                  &
                   dcmplx(face_phi_phi(i_order_cell,i_order_cell)%      &
                          array(index_face_dofi,index_face_dofj,iface))
            sumface_phii_phij(index_face_dofi,index_face_dofj) =        &
            sumface_phii_phij(index_face_dofi,index_face_dofj) + temp * &
                                                      penalization(iface) 
          end do
        end do

      end do

      ! ----------------------------------------------------------------
      !! adjust B in case we have an actual boundary
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        neigh     = ctx_mesh%cell_neigh(iface,i_cell)
        
        select case (neigh)
          !! absorbing boundary conditions given by
          !! 0 = \partial_n p - i\omega c^{-1} p
          !! replacing \partial_n p from Euler gives
          !!   i\omega\rho v.n - i\omega c^{-1} p = 0
          !!   v.n - (\rho c)^{-1} p = 0
          case(tag_absorbing,tag_planewave_scatt)

            !! ---------------------------------------------------------
            select case(trim(adjustl(ctx_mesh%Zimpedance)))
              case('Sommerfeld_0')
                !! - Z (\xi,\xi) over the face
                do i=1, ndof_face
                  index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
                  do j=1,ndof_face
                    index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
                    temp = dcmplx(face_phi_phi(i_order_cell,i_order_cell)%     &
                                  array(index_face_dofi,index_face_dofj,iface)*&
                                  ctx_dg%det_jac_face(iface,i_cell))
                
                    !! update L with - Z (\xi,\xi)
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)=&
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)-&
                                                      temp*(1./(rho*vel))
                  end do
                end do

              case default
                ctx_err%msg   = "** ERROR: Unrecognized Impedance "  // &
                                "condition "//                          &
                                trim(adjustl(ctx_mesh%Zimpedance))//    &
                                " for Robin Boundary Condition [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
            end select
            !! ---------------------------------------------------------

          !! other types
          case(tag_freesurface,tag_ghost_cell,tag_wallsurface,          &
               tag_neumann,tag_dirichlet,tag_planewave)
            !! TAG_GHOST ==> nothing special

            !! TAG_WALL = TAG_NEUMANN
            !!    ==> \partial_\nu Pressure = 0, i.e., displacement = 0
            !!               nothing because Neuman BC is automatic

            !! TAG_FREE = TAG_DIRICHLET = TAG_PlANEWAVE
            !!           ==> !! -------------------------- <!
                             !! it will be done at the end <!
                             !! -------------------------- <!
          !! unsupported BC ............................................
          case(tag_pml_circular,tag_pml,tag_center_neumann,             &
               tag_aldirichlet_dlrobin,tag_deltapdirichlet_dldirichlet, &
               tag_analytic_orthorhombic)
            ctx_err%msg   = "** ERROR: Unsupported " //                 &
                            "boundary condition [create_matrix]"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle

          case default

        end select
      
      end do
      if(ctx_err%ierr .ne. 0) cycle !! non-shared error

      ! ----------------------------------------------------------------
      ! construct HDG matrices A and C 
      ! ----------------------------------------------------------------
      n = ndof_vol*dim_sol
      !! construction Ainv
      !! construction Ainv
      call hdg_build_Ainv_3D_tet_quadrature(hdgAinv,                    &
                   vol_phi_phi_kappa(i_order_cell)%array,               &
                   vol_phi_phi_rho(i_order_cell)%array,                 &
                   vol_dphi_phi(i_order_cell)%array,                    &
                   sumface_phii_phij,ctx_dg%inv_jac(:,:,i_cell),        &
                   ctx_dg%det_jac(i_cell),ndof_vol,freq,                &
                   ctx_dg%pml_cell_coeff(:,i_cell),ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
      
      !! construction C
      call hdg_build_C_3D_tet(hdgC,face_phii_xij,penalization,ndof_vol, &
                      ndof_face_neigh,ndf_edges_elem,                   &
                      ctx_dg%normal(:,:,i_cell),ctx_dg%pml_cell_coeff(:,i_cell))
      !! update saved matrices
      ctx_dg%hdg%Ainv(i_cell)%array(1:n,1:n) = cmplx(hdgAinv(1:n,1:n),kind=RKIND_MAT)
      !! Q =-A^-1 C
      ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot) = cmplx(-matmul(  &
                   hdgAinv(1:n,1:n),hdgC(1:n,1:ndof_face_tot)),kind=RKIND_MAT)
      !! X =-B A^-1
      ctx_dg%hdg%X(i_cell)%array(1:ndof_face_tot,1:n) = cmplx(-matmul(  &
                  hdgB(1:ndof_face_tot,1:n),hdgAinv(1:n,1:n)),kind=RKIND_MAT)
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array(1:ndof_face_tot,1:n)=cmplx(          &
                     matmul(transpose(conjg(hdgC(1:n,1:ndof_face_tot))),&
                            transpose(conjg(hdgAinv(1:n,1:n)))), kind=RKIND_MAT)
        ctx_dg%hdg%D(i_cell)%array(1:n,1:ndof_face_tot)=cmplx(          &
            matmul(transpose(conjg(hdgAinv(1:n,1:n))),                  &
                   transpose(conjg(hdgB(1:ndof_face_tot,1:n)))), kind=RKIND_MAT)
      endif

      !! local matrix coeff = L - B*A^-1*C
      !!                    = L + B*Q
      coeff(1:ndof_face_tot,1:ndof_face_tot) =                &
                      hdgL(1:ndof_face_tot,1:ndof_face_tot) + &
                           matmul(hdgB(1:ndof_face_tot,1:n),  &
                           dcmplx(ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot)))


      !! -----------------------------------------------
      !! readjust the free surface case to identity
      !! it is similar to dirichlet and planewave
      !! because we impose the planewave with dirichlet.
      !! -----------------------------------------------
      do iface=1, ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        if(neigh .eq. tag_freesurface .or. &
           neigh .eq. tag_planewave   .or. &
           neigh .eq. tag_dirichlet) then

          !! -------------------------------------------------------------- %
          !! The local block will be replaced by Identity to maintain the 
          !! symmetry of the matrix. However, using the weak form, we do
          !! not readily have the identity but 
          !!   \integral \Lambda \xi_j  = \integral g \xi_j   \forall j
          !! where g is my source function. That is, we have in matrix form:
          !!             hdgL \Lambda = S.
          !! 
          !! Here we save the matrix hdgL, and we will solve the local
          !! problem when we create the source. Hence we will readily
          !! have the values of Lambda, and we can use and identity block
          !! that allows us to maintain the symmetry of the matrix.
          !!
          !!  e.g., original local system would be 
          !!  (x x x)                  (x x x)                      (x x  0)
          !!  (x x x)\Lambda , and now (x x x)\Lambda, and we need  (x x  0) \Lambda
          !!  (x x x)                  (0 0 L)                      (0 0 Id)
          !!no dirichlet           dirichlet face 3               for symmetry
          !! -------------------------------------------------------------- %
          
          !! replace the block of the considered face with 0.
          coeff(  ndf_edges_elem(iface)+1: &
                  ndf_edges_elem(iface)+ndof_face,:)=0.0

          !! info regarding the symmetry
          flag_info_sym(iface) = .true. 
          !! Here we count coefficients that are NOT on the line we will
          !! modify, i.e., on the columns of the other lines.
          
          !! we could use ndof_face because the face is on the boundary anyway
          ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) =                 &
            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) +               &
            !! only the other faces.
            (ndof_face_tot - ndof_face_neigh(iface))*ndof_face_neigh(iface)

          !! We have saved the local block, we can set the identity 
          do i=1, ndof_face
            coeff(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+i) = 1.0
          end do
          !! -----------------------------------------------------------
        end if
        
        !! -------------------------------------------------------------
        !! save model information for planewaves
        !! -------------------------------------------------------------
        if(neigh .eq. tag_planewave_scatt) then
          face = ctx_mesh%cell_face(iface,i_cell)       
          call array_allocate(ctx_dg%face_velocity(face),2,ctx_err)
          !! remark that we could save for all dof on the face ...
          ctx_dg%face_velocity(face)%array(1) = vel
          ctx_dg%face_velocity(face)%array(2) = rho ! used for HDG scale
        end if

        if(neigh .eq. tag_planewave) then
          !! -----------------------------------------------------------        
          !! we save the hdgL block for the weak form on the boundary.
          !! we also save the velocity on the face in case we have a 
          !! planewave. In acoustic there is only one Vp
          !! -----------------------------------------------------------
          face = ctx_mesh%cell_face(iface,i_cell)           
          call array_allocate(ctx_dg%face_velocity(face),1,ctx_err)
          !! remark that we could save for all dof on the face ...
          ctx_dg%face_velocity(face)%array(1) = vel
          do i=1,dim_var
            call array_allocate(ctx_dg%ctx_symmetrize_mat%f_matbc(face,i),&
                                ndof_face_neigh(iface),                   &
                                ndof_face_neigh(iface),ctx_err)
            ctx_dg%ctx_symmetrize_mat%f_matbc(face,i)%array = 0.d0
          end do
  
          !! because the matrix can be symmetric so we will have some
          !! operations to perform later on.
          ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize = .true.
          i_o_neigh = i_order_neigh(iface)
          !! this is the block hdgL without the penalization: \int \xi_i \xi_j 
          do i=1, ndof_face_neigh(iface)
            index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)
            do j=1, ndof_face_neigh(iface)
              index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
              temp = dcmplx(face_phi_phi(i_o_neigh,i_o_neigh)%            &
                            array(index_face_dofi,index_face_dofj,iface)* &
                            ctx_dg%det_jac_face(iface,i_cell))
            
              !! only one variable in acoustic, we save the current block.
              ctx_dg%ctx_symmetrize_mat%f_matbc(face,1)%array(i,j) = temp
              !! coeff(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)=temp
            end do
          end do
          
        end if !! end if planewave

      end do !! end loop over faces.

      !! allocate arrays if non-zeros
      if(ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) .ne. 0) then
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_line(i_cell),   &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_col(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_val(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
         ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array = 0.d0
      end if 
      ind_sym = 0
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Update global coefficients
      !! we find the appropriate index for the face first
      do iface=1,ctx_mesh%n_neigh_per_cell
        if(ctx_err%ierr .ne. 0) cycle
        
        do ipos=1,ndf_edges_elem(iface+1)-ndf_edges_elem(iface)
          if(ctx_err%ierr .ne. 0) cycle
          
          !! line info -------------------------------------------------
          i        = ndf_edges_elem(iface)+ipos
          iline    = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(iface,i_cell))
          i_o_neigh= i_order_neigh(iface)
          i_orient = ctx_mesh%cell_face_orientation(iface,i_cell)
          iline=iline + ctx_dg%hdg%idof_face(i_orient,i_o_neigh)%array(ipos)

          !! col info --------------------------------------------------
          do jface=1,ctx_mesh%n_neigh_per_cell
            if(ctx_err%ierr .ne. 0) cycle
            
            do jpos=1,ndf_edges_elem(jface+1)-ndf_edges_elem(jface)
              if(ctx_err%ierr .ne. 0) cycle

              j        = ndf_edges_elem(jface)+jpos
              icol     = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(jface,i_cell))
              i_o_neigh= i_order_neigh(jface)
              i_orient = ctx_mesh%cell_face_orientation(jface,i_cell)
              icol=icol + ctx_dg%hdg%idof_face (i_orient,i_o_neigh)%array(jpos)

              !! save Dirichlet BC infos to make the matrix symmetric    
              !! by modifying the rhs.
              if( flag_info_sym(jface) .and. (.not.flag_info_sym(iface))) then
                ind_sym = ind_sym + 1
                ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array(ind_sym) = &
                  int(iline, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array(ind_sym) = &
                  int(icol, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array(ind_sym) = &
                                       cmplx(coeff(i,j),kind=RKIND_POL)
                !! this coeff can be set to zero now.
                coeff(i,j) = 0.d0
              end if

              !! update symmetric or not
              if(abs(coeff(i,j))>tol) then
                !! The BC do not remove the symmetry of the matrix
                if(icol >= iline) then
                  !$OMP CRITICAL
                  nnz_exact       = nnz_exact + 1
                  Alin(nnz_exact) = iline
                  Acol(nnz_exact) = icol
                  Aval(nnz_exact) = cmplx(coeff(i,j),kind=RKIND_MAT)
                  !$OMP END CRITICAL

                  !! check too high values 
                  if(real (Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.aimag(Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.ieee_is_nan(real (Aval(nnz_exact))) .or.                &
                     ieee_is_nan(aimag(Aval(nnz_exact)))) then
                    write(ctx_err%msg,'(a,i0,a,es9.2,a)') "** ERROR: "//  &
                          "the matrix has entry infinity or NaN."//       &
                          " Current matrix precision kind is ", RKIND_MAT,&
                          " (8 at most), which amounts to a"     //       &
                          " maximal value of ",                           &
                          huge(real(1.0,kind=RKIND_MAT)), " [create_matrix]"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    cycle
                  end if
                end if
              end if

            end do
          end do
        end do
      end do
      ! ----------------------------------------------------------------

    end do
    !$OMP END DO

    dim_nnz_loc = nnz_exact

    deallocate(hdgL             )
    deallocate(coeff            )
    deallocate(face_phii_xij    )
    deallocate(sumface_phii_phij)
    deallocate(hdgC             )
    deallocate(hdgB             )
    deallocate(hdgAinv          )
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi_kappa(i))
      call array_deallocate(vol_phi_phi_rho  (i))
      call array_deallocate(vol_dphi_phi(i))
      do j = 1, ctx_dg%n_different_order
        call array_deallocate(face_phi_phi(i,j))
      end do
    end do
    deallocate(vol_phi_phi_kappa)
    deallocate(vol_phi_phi_rho  )
    deallocate(vol_dphi_phi     )
    deallocate(face_phi_phi     )
    !$OMP END PARALLEL
    
    return
  end subroutine create_matrix_quadrature_3D

end module m_create_matrix_quadrature
