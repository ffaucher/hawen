!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_gradient.f90
!
!> @author
!> J. Dutheil [Inria Makutu]
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the gradient of the cost function w.r.t
!> the physical coefficient in the context of cross-correlation inversion.
!> Here we now have two forward fields and two backward fields that are 
!> combined to assemble the gradient.
!
!------------------------------------------------------------------------------
module m_create_gradient_CC

  !! module used -------------------------------------------------------
  use m_raise_error,                only: raise_error, t_error
  use m_distribute_error,           only: distribute_error
  use m_ctx_parallelism,            only: t_parallelism
  use m_ctx_domain,                 only: t_domain
  use m_ctx_discretization,         only: t_discretization
  use m_ctx_model,                  only: t_model
  use m_ctx_field,                  only: t_field
  use m_create_gradient_quadrature_CC, only:                            &
                                    create_gradient_quadrature_pconstant_CC 
  use m_ctx_model_representation,   only: tag_MODEL_PCONSTANT, tag_MODEL_DOF

  implicit none

  private
  public  :: create_gradient_CC

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create gradient for cross-correlation data
  !>
  !
  !> @param[in]    ctx_paral         : parallel context
  !> @param[in]    ctx_mesh          : mesh grid type
  !> @param[in]    ctx_model         : model parameters
  !> @param[in]    ctx_discretization: type DG for reference matrix
  !> @param[in]    ctx_field_fwd_1   : forward field 1 
  !> @param[in]    ctx_field_fwd_2   : forward field 2 
  !> @param[in]    ctx_field_bwd_1   : backward field 1
  !> @param[in]    ctx_field_bwd_2   : backward field 2
  !> @param[in]    frequency         : frequency
  !> @param[inout] gradient          : output computed gradient
  !> @param[in]    tag_metric        : indicates the inner product metric
  !> @param[out]   flag_computed     : indicates if the gradient has been computed
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_CC(ctx_paral,ctx_mesh,ctx_discretization,  &
                                ctx_model,ctx_field_fwd_1,              &
                                ctx_field_fwd_2,ctx_field_bwd_1,        &
                                ctx_field_bwd_2,angular_freq,gradient,  &
                                tag_metric,flag_computed,ctx_err)

    implicit none

    type(t_parallelism)              ,intent(in)      :: ctx_paral
    type(t_domain)                   ,intent(in)      :: ctx_mesh
    type(t_discretization)           ,intent(in)      :: ctx_discretization
    type(t_model)                    ,intent(in)      :: ctx_model
    type(t_field)                    ,intent(in)      :: ctx_field_fwd_1
    type(t_field)                    ,intent(in)      :: ctx_field_fwd_2
    type(t_field)                    ,intent(in)      :: ctx_field_bwd_1
    type(t_field)                    ,intent(in)      :: ctx_field_bwd_2
    complex(kind=8)                  ,intent(in)      :: angular_freq
    real   (kind=4)      ,allocatable,intent(inout)   :: gradient(:,:)
    integer                          ,intent(in)      :: tag_metric
    logical                          ,intent(out)     :: flag_computed
    type(t_error)                    ,intent(inout)   :: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    gradient      = 0.0
    flag_computed = .false.

    !! -----------------------------------------------------------------
    !! create the gradient depending on
    !!  - the dimension
    !!  - the model representation
    !!  - the method (quadrature / reference element)
    !! not every combination is compatible. 
    !! -----------------------------------------------------------------
    if(ctx_discretization%flag_eval_int_quadrature) then
      select case(trim(adjustl(ctx_model%format_disc)))
        case(tag_MODEL_PCONSTANT)
          !! already for 2 and 3D.
          call create_gradient_quadrature_pconstant_CC(                 &
                                          ctx_discretization,ctx_mesh,  &
                                          ctx_model,                    &
                                          ctx_field_fwd_1%field,        &
                                          ctx_field_fwd_2%field,        &
                                          ctx_field_bwd_1%field,        &
                                          ctx_field_bwd_2%field,        &
                                          angular_freq,                 &
                                          ctx_field_fwd_1%n_src,        &
                                          gradient,                     &
                                          tag_metric,ctx_err)
          !! computation is done
          flag_computed = .true.

        case(tag_MODEL_DOF)      
          !! can be computed with default routine 
          flag_computed = .false.

        case default
          ctx_err%msg   = "** ERROR: unsupported model "         //     &
                          "representation [create_gradient] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    
    else
      !! can be computed with default routine 
      flag_computed = .false.
    end if
    
    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 

    return

  end subroutine create_gradient_CC

end module m_create_gradient_CC
