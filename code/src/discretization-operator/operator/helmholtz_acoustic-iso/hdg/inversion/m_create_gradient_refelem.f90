!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_gradient_refelem.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the gradient of the cost function w.r.t
!> the physical coefficient, assuming piecewise constant representation
!> (1 value per cell) for the HELMHOLTZ ACOUSTIC propagator using HDG
!> discretization:
!>
!> The reference parameters are
!>      1/kappa
!>        rho
!
!------------------------------------------------------------------------------
module m_create_gradient_refelem

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: RKIND_MAT,RKIND_MESH,IKIND_MESH
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_piecewise_constant
  use m_mesh_simplex_area,      only: mesh_area_simplex
  use m_grad_metric,            only: tag_GRADMETRIC_ID,                &
                                tag_GRADMETRIC_AREA,tag_GRADMETRIC_MASS,&
                                tag_GRADMETRIC_MASS_INV
  use m_model_list
  use m_matrix_utils_derivative,only: hdg_build_dA_2D_tri,              &
                                      hdg_build_dA_3D_tet

  implicit none

  private
  public  :: create_gradient_refelem_2D, create_gradient_refelem_3D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the gradient of the misfit, with respect to 1/\kappa and \rho
  !> for HDG in HELMHOLTZ ACOUSTIC in 2D based upon triangle discretization
  !> grad(:,:,1) = dA / d{1/\kappa}
  !> grad(:,:,2) = dA / d\rho
  !> if viscosity
  !> grad(:,:,3) = dA / d\viscosity
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_bwd         : backward wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] gradient          : gradient arrays
  !> @param[in]    tag_metric        : gradient metric
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_refelem_2D(ctx_dg,ctx_mesh,ctx_model,      &
                                        field_fwd,field_bwd,freq,n_shot,&
                                        gradient,tag_metric,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd (:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_bwd (:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: gradient(:,:)
    integer                            ,intent(in)    :: tag_metric
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2
    integer                     :: iloc1,iloc2,n,idof,isol
    complex(kind=8),allocatable :: sol_fwd_loc(:),sol_bwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8)             :: gradval(5),u,v,uprime,vprime,freqloc
    complex(kind=8)             :: w,wprime
    real   (kind=8)             :: Q,c,rho,tauE,tauS,eta,power,kappa0
    complex(kind=8)             :: grad_kappa
    !! for metric only
    integer                     :: lwork,jdof,ierr,n_visco,i_model
    integer        ,allocatable :: ipiv(:)
    real   (kind=8)             :: area
    real(kind=RKIND_MESH),allocatable :: coo_node(:,:)
    complex(kind=8),allocatable :: metric_weight(:,:),sol_bwd_weigh(:)
    complex(kind=8),allocatable :: work(:)
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! we compute size max so that we have only one allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_order_cell,icell,n,idof),  &
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,isol),  &
    !$OMP&         PRIVATE(sol_fwd_loc,sol_bwd_loc,dA,dAU,gradval,u,v), &
    !$OMP&         PRIVATE(uprime,vprime,freqloc,Q,c,rho),              &
    !$OMP&         PRIVATE(metric_weight,jdof,work,lwork,i_model),      &
    !$OMP&         PRIVATE(ipiv,ierr,sol_bwd_weigh,coo_node,area),      &
    !$OMP&         PRIVATE(tauE,tauS,eta,power,kappa0,grad_kappa),      &
    !$OMP&         PRIVATE(n_visco,w,wprime)
    !! local array, the 2 is for the two acoustic parameters
    !!              the 3 is for the three triangle faces
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,2))
    allocate(dAU          (dim_sol*ndof_max_vol,2))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_weigh(dim_sol*ndof_max_vol))
    allocate(metric_weight(dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(work         (dim_sol*ndof_max_vol))
    allocate(ipiv         (dim_sol*ndof_max_vol))
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))
    !! -----------------------------------------------------------------
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      if(ctx_err%ierr .ne. 0) cycle
      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      n            = ndof_vol*dim_sol

      ! ----------------------------------------------------------------
      !! Compute the appropriate weighting matrix in case we
      !! do not have the identity metric for inner product
      ! ----------------------------------------------------------------
      !! THE MATRIX MUST BE SYMMETRIC POSITIVE DEFINITE
      ! ----------------------------------------------------------------
      metric_weight = 0.0
      select case(tag_metric)
        !! identity matrix ---------------------------------------------
        case(tag_GRADMETRIC_ID       )
          do idof=1,n
            metric_weight(idof,idof) = 1.0
          end do

        !! identity matrix with 1./area on the diagonal ----------------
        case(tag_GRADMETRIC_AREA     )
          coo_node(:,:)=ctx_mesh%x_node(:,ctx_mesh%cell_node(:,icell))
          call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)
          do idof=1,n
            metric_weight(idof,idof) = 1./area
          end do

        !! mass matrix -------------------------------------------------
        case(tag_GRADMETRIC_MASS,tag_GRADMETRIC_MASS_INV)
          do idof=1,ndof_vol ; do jdof=1,ndof_vol
            u = dcmplx(ctx_dg%vol_matrix_mass(i_order_cell)%array(idof,jdof) * &
                       ctx_dg%det_jac(icell))
            do isol=1,dim_sol
              metric_weight(idof+(isol-1)*ndof_vol, &
                            jdof+(isol-1)*ndof_vol) = u
            end do
          end do ; enddo
          if(tag_metric == tag_GRADMETRIC_MASS_INV) then
            !! inverse of M --------------------------------------------
            lwork=n
            call ZGETRF(n,n,metric_weight(1:n,1:n),n,ipiv,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg   = "** ERROR during ZGETRF in HDG create grad"
              cycle
            end if
            call ZGETRI(n,metric_weight(1:n,1:n),n,ipiv,work,lwork,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg  = "** ERROR during ZGETRI in HDG create grad"
              cycle
            end if
          end if
        !! stiffness matrix --------------------------------------------
        ! ---------------------------------------------------------
        !! does not provide a symmetric definitive positive
        !! matrix
        ! ---------------------------------------------------------
        !! error -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized grad. metric [create_grad]**"
          ctx_err%ierr  = -1
      end select
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! loop over all shots--------------------------------------------
      do i_src = 1, n_shot

        if(ctx_err%ierr .ne. 0) cycle

        !! multipler offset
        sol_fwd_loc  =0.0
        sol_bwd_loc  =0.0
        sol_bwd_weigh=0.0
        dA           =0.0
        dAU          =0.0

        ! ---------------------------------------------------------
        !! get the local solution
        igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
                    ctx_dg%offsetdof_sol_loc(icell)  + 1)
        igb2 = igb1 + ndof_vol - 1
        do isol=1,dim_sol
          iloc1 = (isol-1)*ndof_vol + 1
          iloc2 = iloc1  + ndof_vol - 1
          sol_fwd_loc(iloc1:iloc2) = dcmplx(field_fwd(igb1:igb2,isol))
          sol_bwd_loc(iloc1:iloc2) = dcmplx(field_bwd(igb1:igb2,isol))
        end do
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !! the gradient is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell
        call hdg_build_dA_2D_tri(dA,                                    &
                            ctx_dg%vol_matrix_mass(i_order_cell)%array, &
                            ctx_dg%det_jac(icell),ndof_vol,freq)
        !!> dC = 0 in acoustic  !! call hdg_build_dC_2D_tri

        !! compute dA U, for all parameters: 1/kappa and rho
        !! -------------------------------------------------------------
        dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))
        dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))
        !! apply metric onto the backward field
        sol_bwd_weigh(1:n) = matmul(metric_weight(1:n,1:n),sol_bwd_loc(1:n))

        gradval   = 0.0
        do idof=1,n !! loop over all ndof * dim_solution
          !! gradient values
          gradval(1) = gradval(1) + conjg(dAU(idof,1))*sol_bwd_weigh(idof)
          gradval(2) = gradval(2) + conjg(dAU(idof,2))*sol_bwd_weigh(idof)
        end do
        !! average
        gradval    = dcmplx(gradval   / dble(ndof_vol))


        !! ============================================================
        !! in the case of viscosity
        !!
        !! The viscosity model modifies the complex bulk modulus kappa.
        !! For the gradient we use:
        !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
        !!
        !! WARNING: we also have to change the gradient with respect
        !!          to the native bulk modulus: kappa0, which is the
        !!          default output.
        !! ============================================================
        if(ctx_model%flag_viscous) then

          ! ---------------------------------------------------------
          !! only piecewise-constant is supported with reference-element
          !! we use the RAW models
          call model_eval_piecewise_constant(ctx_model,model_BULK,icell,&
                                             kappa0,ctx_err)
          call model_eval_piecewise_constant(ctx_model,model_VP  ,icell,&
                                             c,ctx_err)
          call model_eval_piecewise_constant(ctx_model,model_RHO ,icell,&
                                             rho,ctx_err)
          !! gradient w.r.t to kappa instead of 1/kappa
          grad_kappa =-gradval(1) / (kappa0**2)

          if(ctx_err%ierr .ne. 0) cycle

          select case(trim(adjustl(ctx_model%viscous_model)))

            case(model_visco_KF)
              !! we have V = Vp( 1 + i / (2*Q)) which correspond with
              !! 1/Kappa_Viscous = (1/(rho c²)) (4Q²-4iQ-1) / (4Q²+1)²
              !! in this case we derive w.r.t:
              !! 1/kappa = 1/(rho c²)
              !!   Q
              !! derive w.r.t. Q
              call model_eval_piecewise_constant(ctx_model,             &
                                                 model_VISCOUS,icell,Q,ctx_err)
              if(ctx_err%ierr .ne. 0) cycle

              u     =(4.*Q*Q*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))
              uprime=4.*16.*Q**3 - 2.*4.*Q - 3.*16.*dcmplx(0.,1.)*Q*Q
              v     =(4.*Q*Q+1.)**2
              vprime=2.*8.*Q*(4.*Q*Q + 1)

              gradval(3) = gradval(1) * 1./(rho*c*c)*(uprime*v - u*vprime)/(v*v)
              gradval(1) = gradval(1) * (4.*Q*Q /((4.*Q*Q+1.)**2)) *    &
                           (4.*Q*Q-4*dcmplx(0.,1.)*Q-1.)

            !! kappa = kappa_0 (1 - i omega tau_epsilon) ---------------
            case(model_visco_KV   )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              gradval(3) = grad_kappa * (     - freq*kappa0)
              gradval(1) = grad_kappa * (1.d0 - freq*tauE)
              !! change to 1/kappa0
              gradval(1) =-gradval(1)  * (kappa0**2)

            !! kappa =-i omega kappa_0 visco / (kappa_0 - i omega visco)
            case(model_visco_Maxw )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =-freq*kappa0*eta
              v     = kappa0 - freq*eta
              uprime=-freq*kappa0
              vprime=-freq
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              uprime=-freq*eta
              vprime= 1.d0
              gradval(1) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! kappa = kappa_0 * ( 1 + (-i omega tau_eps  )^beta)
            !!                 / ( 1 + (-i omega tau_sigma)^beta) ------
            case(model_visco_CC   )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_SIGMA,icell,tauS,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0 * (1.d0 + (-freq*tauE)**power)
              v     = (1.d0 + (-freq*tauS)**power)
              !! Tau_epsilon
              uprime= power * (-freq) * kappa0 * (-freq*tauE)**(power-1.d0)
              vprime= 0.d0
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! Tau_sigma
              uprime= 0.d0
              vprime= power * (-freq) * (-freq*tauS)**(power-1.d0)
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! power
              uprime=  kappa0 * (-freq*tauE)**power * log(-freq*tauE)
              vprime=           (-freq*tauS)**power * log(-freq*tauS)
              gradval(5) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! kappa0
              gradval(1) = grad_kappa * (1.d0 + (-freq*tauE)**power)    &
                                      / (1.d0 + (-freq*tauS)**power)
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! kappa = kappa_0 * ( 1 - i omega tau_eps  )
            !!                 / ( 1 - i omega tau_sigma) ------------------
            case(model_visco_Zener)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_SIGMA,icell,tauS,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0 * (1.d0 + (-freq*tauE))
              v     =           (1.d0 + (-freq*tauS))
              !! Tau_epsilon
              uprime=-freq * kappa0
              vprime= 0.d0
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! Tau_sigma
              uprime= 0.d0
              vprime=-freq
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! kappa0
              gradval(1) = grad_kappa * (1.d0 + (-freq*tauE))           &
                                      / (1.d0 + (-freq*tauS))
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! KSB model -----------------------------------------------
            !! kappa = kappa_0  / (1 + eta/sqrt(1+(-i omega tau)^beta))²
            case(model_visco_KSB)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0
              w     = sqrt(1.d0 + (-freq*tauE)**power)
              v     = (1.d0 + eta/w)**2
              !! Eta
              uprime= 0.d0
              vprime= 2.d0                                              &
                    * 1.d0/sqrt(1.d0 + (-freq*tauE)**power)             &
                    * (1.d0 + eta/sqrt(1.d0 + (-freq*tauE)**power))
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! TauE
              wprime = 1.d0/2.d0                                        &
                     * (power*(-freq)*(-freq*tauE)**(power-1.d0))       &
                     * (1.d0 + (-freq*tauE)**power)**(-1.d0/2.d0)
              !! we derive v = (1 + eta/w)²
              !!     dv = 2 (-eta w' / w²) (1 + eta/w)
              uprime= 0.d0
              vprime= 2.d0 * (-eta*wprime/(w**2) )* (1.d0+ eta/w)
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! Power
              !! derivative of f^g is given by 
              !!     = g f^(g-1) df + f^g ln(f) dg
              !! where df = 0 and dg = 1 here
              uprime= 0.d0
              wprime = 1.d0/2.d0                                        &
                     * ((-freq*tauE)**power * log(-freq*tauE))          &
                     * (1.d0 + (-freq*tauE)**power)**(-1.d0/2.d0)
              !! we derive v = (1 + eta/w)²
              !!     dv = 2 (-eta w' / w²) (1 + eta/w)
              vprime= 2.d0 * (-eta*wprime/(w**2) )* (1.d0+ eta/w)
              gradval(5) = grad_kappa * (uprime*v - u*vprime)/(v**2)

              !! adjust kappa0
              gradval(1) = grad_kappa * 1.d0/v
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)
              
            !! Szabo model ---------------------------------------------
            !! kappa = kappa_0 / ( 1 + eta (-i omega)^(pw-1)) ----------
            case(model_visco_Szabo)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0 
              v     = 1.d0 + eta*((-freq)**(power-1.d0))
              !! Eta
              uprime= 0.d0
              vprime= (-freq)**(power-1.d0)
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! power
              !! derivative of f^g is given by 
              !!     = g f^(g-1) df + f^g ln(f) dg
              !! we have v = 1 + eta * f^g
              !!        dv = d(f^g).
              !! where df = 0 and dg = 1
              uprime = 0.d0
              vprime = eta*((-freq)**(power-1.d0) * log(-freq))
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! adjust kappa0
              gradval(1) = grad_kappa * 1.d0/v
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! power-law -----------------------------------------------
            case(model_visco_plaw )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              ctx_err%msg   = "** ERROR: the derivations for the " //   &
                              "power-law acoustic viscosity model is "//&
                              "not implemented yet [grad] **"
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [grad]"
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
          end select

        end if
        ! ---------------------------------------------------------
        !! save value, we sum over the shots here
        gradient(icell,1) = gradient(icell,1)+real(gradval   (1),kind=4)
        gradient(icell,2) = gradient(icell,2)+real(gradval   (2),kind=4)
        if(ctx_model%flag_viscous) then
          n_visco = size(ctx_model%visco)
          do i_model = 3, 2+n_visco
            gradient(icell,i_model) = gradient(icell,i_model)           &
                                    + real(  gradval(i_model),kind=4)
          end do
        end if
        ! ---------------------------------------------------------
      end do !! end loop over shots
    end do !! end loop over cells
    !$OMP END DO

    deallocate(dA         )
    deallocate(sol_fwd_loc)
    deallocate(sol_bwd_loc)
    deallocate(sol_bwd_weigh)
    deallocate(dAU        )
    deallocate(coo_node   )
    deallocate(work       )
    deallocate(ipiv       )
    deallocate(metric_weight)
    !$OMP END PARALLEL

    return
  end subroutine create_gradient_refelem_2D

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the gradient of the misfit, with respect to 1/\kappa and \rho
  !> for HDG in HELMHOLTZ ACOUSTIC in 3D based upon triangle discretization
  !> grad(:,:,1) = dA / d{1/\kappa}
  !> grad(:,:,2) = dA / d\rho
  !> if viscosity
  !> grad(:,:,3) = dA / d\viscosity
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_bwd         : backward wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] gradient          : gradient arrays
  !> @param[in]    tag_metric        : gradient metric
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_refelem_3D(ctx_dg,ctx_mesh,ctx_model,      &
                                        field_fwd,field_bwd,freq,n_shot,&
                                        gradient,tag_metric,ctx_err)

    implicit none
    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd (:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_bwd (:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: gradient(:,:)
    integer                            ,intent(in)    :: tag_metric
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2
    integer                     :: iloc1,iloc2,n,idof,isol
    complex(kind=8),allocatable :: sol_fwd_loc(:),sol_bwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8)             :: gradval(5),u,v,uprime,vprime,freqloc
    complex(kind=8)             :: w,wprime
    real   (kind=8)             :: Q,c,rho,tauE,tauS,eta,power,kappa0
    complex(kind=8)             :: grad_kappa
    !! for metric only
    integer                     :: lwork,jdof,ierr,n_visco,i_model
    integer        ,allocatable :: ipiv(:)
    real   (kind=8)             :: area
    real(kind=RKIND_MESH),allocatable :: coo_node(:,:)
    complex(kind=8),allocatable :: metric_weight(:,:),sol_bwd_weigh(:)
    complex(kind=8),allocatable :: work(:)
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! we compute size max so that we have only ONE allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_order_cell,icell,n,idof),  &
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,isol),  &
    !$OMP&         PRIVATE(sol_fwd_loc,sol_bwd_loc,dA,dAU,gradval,u,v), &
    !$OMP&         PRIVATE(uprime,vprime,freqloc,Q,c,rho),              &
    !$OMP&         PRIVATE(metric_weight,jdof,work,lwork,i_model),      &
    !$OMP&         PRIVATE(ipiv,ierr,sol_bwd_weigh,coo_node,area),      &
    !$OMP&         PRIVATE(tauE,tauS,eta,power,kappa0,grad_kappa),      &
    !$OMP&         PRIVATE(n_visco,w,wprime)
    !! local array, the 2 is for the two acoustic parameters
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,2))
    allocate(dAU          (dim_sol*ndof_max_vol,2))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_weigh(dim_sol*ndof_max_vol))
    allocate(metric_weight(dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(work         (dim_sol*ndof_max_vol))
    allocate(ipiv         (dim_sol*ndof_max_vol))
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))
    !! -----------------------------------------------------------------
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      if(ctx_err%ierr .ne. 0) cycle
      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      n            = ndof_vol*dim_sol

      ! ----------------------------------------------------------------
      !! Compute the appropriate weighting matrix in case we
      !! do not have the identity metric for inner product
      ! ----------------------------------------------------------------
      !! THE MATRIX MUST BE SYMMETRIC POSITIVE DEFINITE
      ! ----------------------------------------------------------------
      metric_weight = 0.0
      select case(tag_metric)
        !! identity matrix ---------------------------------------------
        case(tag_GRADMETRIC_ID       )
          do idof=1,n
            metric_weight(idof,idof) = 1.0
          end do

        !! identity matrix with 1./area on the diagonal ----------------
        case(tag_GRADMETRIC_AREA     )
          coo_node(:,:)=ctx_mesh%x_node(:,ctx_mesh%cell_node(:,icell))
          call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)
          do idof=1,n
            metric_weight(idof,idof) = 1./area
          end do

        !! mass matrix -------------------------------------------------
        case(tag_GRADMETRIC_MASS,tag_GRADMETRIC_MASS_INV)
          do idof=1,ndof_vol ; do jdof=1,ndof_vol
            u = dcmplx(ctx_dg%vol_matrix_mass(i_order_cell)%array(idof,jdof) * &
                       ctx_dg%det_jac(icell))
            do isol=1,dim_sol
              metric_weight(idof+(isol-1)*ndof_vol, &
                            jdof+(isol-1)*ndof_vol) = u
            end do
          end do ; enddo
          if(tag_metric == tag_GRADMETRIC_MASS_INV) then
            !! inverse of M --------------------------------------------
            lwork=n
            call ZGETRF(n,n,metric_weight(1:n,1:n),n,ipiv,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg   = "** ERROR during ZGETRF in HDG create grad"
              cycle
            end if
            call ZGETRI(n,metric_weight(1:n,1:n),n,ipiv,work,lwork,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg  = "** ERROR during ZGETRI in HDG create grad"
              cycle
            end if
          end if
        !! stiffness matrix --------------------------------------------
        ! ---------------------------------------------------------
        !! does not provide a symmetric definitive positive
        !! matrix
        ! ---------------------------------------------------------
        !! error -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized grad. metric [create_grad]**"
          ctx_err%ierr  = -1
      end select
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! loop over all shots--------------------------------------------
      do i_src = 1, n_shot

        if(ctx_err%ierr .ne. 0) cycle

        !! multipler offset
        sol_fwd_loc  =0.0
        sol_bwd_loc  =0.0
        sol_bwd_weigh=0.0
        dA           =0.0
        dAU          =0.0

        ! ---------------------------------------------------------
        !! get the local solution
        igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
               ctx_dg%offsetdof_sol_loc(icell)  + 1)
        igb2 = igb1 + ndof_vol - 1
        do isol=1,dim_sol
          iloc1 = (isol-1)*ndof_vol + 1
          iloc2 = iloc1  + ndof_vol - 1
          sol_fwd_loc(iloc1:iloc2) = dcmplx(field_fwd(igb1:igb2,isol))
          sol_bwd_loc(iloc1:iloc2) = dcmplx(field_bwd(igb1:igb2,isol))
        end do
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !! the gradient is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell
        call hdg_build_dA_3D_tet(dA,                                    &
                            ctx_dg%vol_matrix_mass(i_order_cell)%array, &
                            ctx_dg%det_jac(icell),ndof_vol,freq)
        !!> dC = 0 in acoustic  !! call hdg_build_dC_2D_tri

        !! compute dA U, for all parameters: 1/kappa and rho
        !! -------------------------------------------------------------
        dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))
        dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))
        !! apply metric onto the backward field
        sol_bwd_weigh(1:n) = matmul(metric_weight(1:n,1:n),sol_bwd_loc(1:n))

        gradval   = 0.0
        do idof=1,n !! loop over all ndof * dim_solution
          !! gradient values
          gradval(1) = gradval(1) + conjg(dAU(idof,1))*sol_bwd_weigh(idof)
          gradval(2) = gradval(2) + conjg(dAU(idof,2))*sol_bwd_weigh(idof)
        end do
        !! average
        gradval    = dcmplx(gradval   / dble(ndof_vol))


        !! ============================================================
        !! in the case of viscosity
        !!
        !! The viscosity model modifies the complex bulk modulus kappa.
        !! For the gradient we use:
        !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
        !!
        !! WARNING: we also have to change the gradient with respect
        !!          to the native bulk modulus: kappa0, which is the
        !!          default output.
        !! ============================================================
        if(ctx_model%flag_viscous) then

          ! ---------------------------------------------------------
          !! only piecewise-constant is supported with reference-element
          !! we use the RAW models
          call model_eval_piecewise_constant(ctx_model,model_BULK,icell,&
                                             kappa0,ctx_err)
          call model_eval_piecewise_constant(ctx_model,model_VP  ,icell,&
                                             c,ctx_err)
          call model_eval_piecewise_constant(ctx_model,model_RHO ,icell,&
                                             rho,ctx_err)
          !! gradient w.r.t to kappa instead of 1/kappa
          grad_kappa =-gradval(1) / (kappa0**2)

          if(ctx_err%ierr .ne. 0) cycle

          select case(trim(adjustl(ctx_model%viscous_model)))

            case(model_visco_KF)
              !! we have V = Vp( 1 + i / (2*Q)) which correspond with
              !! 1/Kappa_Viscous = (1/(rho c²)) (4Q²-4iQ-1) / (4Q²+1)²
              !! in this case we derive w.r.t:
              !! 1/kappa = 1/(rho c²)
              !!   Q
              !! derive w.r.t. Q
              call model_eval_piecewise_constant(ctx_model,             &
                                                 model_VISCOUS,icell,Q,ctx_err)
              if(ctx_err%ierr .ne. 0) cycle

              u     =(4.*Q*Q*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))
              uprime=4.*16.*Q**3 - 2.*4.*Q - 3.*16.*dcmplx(0.,1.)*Q*Q
              v     =(4.*Q*Q+1.)**2
              vprime=2.*8.*Q*(4.*Q*Q + 1)

              gradval(3) = gradval(1) * 1./(rho*c*c)*(uprime*v - u*vprime)/(v*v)
              gradval(1) = gradval(1) * (4.*Q*Q /((4.*Q*Q+1.)**2)) *    &
                           (4.*Q*Q-4*dcmplx(0.,1.)*Q-1.)

            !! kappa = kappa_0 (1 - i omega tau_epsilon) ---------------
            case(model_visco_KV   )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              gradval(3) = grad_kappa * (     - freq*kappa0)
              gradval(1) = grad_kappa * (1.d0 - freq*tauE)
              !! change to 1/kappa0
              gradval(1) =-gradval(1)  * (kappa0**2)

            !! kappa =-i omega kappa_0 visco / (kappa_0 - i omega visco)
            case(model_visco_Maxw )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =-freq*kappa0*eta
              v     = kappa0 - freq*eta
              uprime=-freq*kappa0
              vprime=-freq
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              uprime=-freq*eta
              vprime= 1.d0
              gradval(1) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! kappa = kappa_0 * ( 1 + (-i omega tau_eps  )^beta)
            !!                 / ( 1 + (-i omega tau_sigma)^beta) ------
            case(model_visco_CC   )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_SIGMA,icell,tauS,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0 * (1.d0 + (-freq*tauE)**power)
              v     = (1.d0 + (-freq*tauS)**power)
              !! Tau_epsilon
              uprime= power * (-freq) * kappa0 * (-freq*tauE)**(power-1.d0)
              vprime= 0.d0
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! Tau_sigma
              uprime= 0.d0
              vprime= power * (-freq) * (-freq*tauS)**(power-1.d0)
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! power
              uprime=  kappa0 * (-freq*tauE)**power * log(-freq*tauE)
              vprime=           (-freq*tauS)**power * log(-freq*tauS)
              gradval(5) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! kappa0
              gradval(1) = grad_kappa * (1.d0 + (-freq*tauE)**power)    &
                                      / (1.d0 + (-freq*tauS)**power)
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! kappa = kappa_0 * ( 1 - i omega tau_eps  )
            !!                 / ( 1 - i omega tau_sigma) ------------------
            case(model_visco_Zener)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_SIGMA,icell,tauS,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0 * (1.d0 + (-freq*tauE))
              v     =           (1.d0 + (-freq*tauS))
              !! Tau_epsilon
              uprime=-freq * kappa0
              vprime= 0.d0
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! Tau_sigma
              uprime= 0.d0
              vprime=-freq
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! kappa0
              gradval(1) = grad_kappa * (1.d0 + (-freq*tauE))           &
                                      / (1.d0 + (-freq*tauS))
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! KSB model -----------------------------------------------
            !! kappa = kappa_0  / (1 + eta/sqrt(1+(-i omega tau)^beta))²
            case(model_visco_KSB)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_TAU_EPS,icell,tauE,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0
              w     = sqrt(1.d0 + (-freq*tauE)**power)
              v     = (1.d0 + eta/w)**2
              !! Eta
              uprime= 0.d0
              vprime= 2.d0                                              &
                    * 1.d0/sqrt(1.d0 + (-freq*tauE)**power)             &
                    * (1.d0 + eta/sqrt(1.d0 + (-freq*tauE)**power))
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! TauE
              wprime = 1.d0/2.d0                                        &
                     * (power*(-freq)*(-freq*tauE)**(power-1.d0))       &
                     * (1.d0 + (-freq*tauE)**power)**(-1.d0/2.d0)
              !! we derive v = (1 + eta/w)²
              !!     dv = 2 (-eta w' / w²) (1 + eta/w)
              uprime= 0.d0
              vprime= 2.d0 * (-eta*wprime/(w**2) )* (1.d0+ eta/w)
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! Power
              !! derivative of f^g is given by 
              !!     = g f^(g-1) df + f^g ln(f) dg
              !! where df = 0 and dg = 1 here
              uprime= 0.d0
              wprime = 1.d0/2.d0                                        &
                     * ((-freq*tauE)**power * log(-freq*tauE))          &
                     * (1.d0 + (-freq*tauE)**power)**(-1.d0/2.d0)
              !! we derive v = (1 + eta/w)²
              !!     dv = 2 (-eta w' / w²) (1 + eta/w)
              vprime= 2.d0 * (-eta*wprime/(w**2) )* (1.d0+ eta/w)
              gradval(5) = grad_kappa * (uprime*v - u*vprime)/(v**2)

              !! adjust kappa0
              gradval(1) = grad_kappa * 1.d0/v
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)
              
            !! Szabo model ---------------------------------------------
            !! kappa = kappa_0 / ( 1 + eta (-i omega)^(pw-1)) ----------
            case(model_visco_Szabo)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              
              !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
              u     =  kappa0 
              v     = 1.d0 + eta*((-freq)**(power-1.d0))
              !! Eta
              uprime= 0.d0
              vprime= (-freq)**(power-1.d0)
              gradval(3) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! power
              !! derivative of f^g is given by 
              !!     = g f^(g-1) df + f^g ln(f) dg
              !! we have v = 1 + eta * f^g
              !!        dv = d(f^g).
              !! where df = 0 and dg = 1
              uprime = 0.d0
              vprime = eta*((-freq)**(power-1.d0) * log(-freq))
              gradval(4) = grad_kappa * (uprime*v - u*vprime)/(v**2)
              !! adjust kappa0
              gradval(1) = grad_kappa * 1.d0/v
              !! change to 1/kappa0
              gradval(1) =-gradval(1) * (kappa0**2)

            !! power-law -----------------------------------------------
            case(model_visco_plaw )
              call model_eval_piecewise_constant(ctx_model,             &
                              model_VISCOUS,icell,eta,ctx_err)
              call model_eval_piecewise_constant(ctx_model,             &
                              model_FPOWER,icell,power,ctx_err)
              ctx_err%msg   = "** ERROR: the derivations for the " //   &
                              "power-law acoustic viscosity model is "//&
                              "not implemented yet [grad] **"
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [grad]"
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
          end select

        end if
        ! ---------------------------------------------------------
        !! save value, we sum over the shots here
        gradient(icell,1) = gradient(icell,1)+real(gradval   (1),kind=4)
        gradient(icell,2) = gradient(icell,2)+real(gradval   (2),kind=4)
        if(ctx_model%flag_viscous) then
          n_visco = size(ctx_model%visco)
          do i_model = 3, 2+n_visco
            gradient(icell,i_model) = gradient(icell,i_model)           &
                                    + real(  gradval(i_model),kind=4)
          end do
        end if
        ! ---------------------------------------------------------
      end do !! end loop over shots
    end do !! end loop over cells
    !$OMP END DO

    deallocate(dA         )
    deallocate(sol_fwd_loc)
    deallocate(sol_bwd_loc)
    deallocate(sol_bwd_weigh)
    deallocate(dAU        )
    deallocate(coo_node   )
    deallocate(work       )
    deallocate(ipiv       )
    deallocate(metric_weight)
    !$OMP END PARALLEL

    return
  end subroutine create_gradient_refelem_3D
end module m_create_gradient_refelem
