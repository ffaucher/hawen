!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_pseudoh.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the Gauss-Newton pseudo-hessian: 
!>         J(m) = 1/2 || Ru - d ||²
!>   dmJ    = dm(u)* R* (Ru - d)
!>   d²m(J) = dm(u)* R* R dm(u)  + d²m(u)* R* (Ru - d).
!> Gauss-Newton assumes that d²m(u) = 0
!>   HGN  = dm(u)* R* R dm(u)
!>
!> we replace with dm(u) = - P^{-1} dm(P) u to obtain  
!>   HGN  = (P^{-1} dm(P) u)* R* R (P^{-1} dm(P) u)
!>   HGN  = u* dm(P)* P^{-*} R* R P^{-1} dm(P) u
!>
!> The pseudo-Hessian assumes that P^{-*} R* R P^{-1} = Identity:
!> PSEUDO-HESSIAN = u* dm(P)* dm(P) u
!>
!> Remark that it also depends on the choice of cost function, which
!>        we consider outside of this routine. The two parameters of 
!>        reference are here: 
!>      1/kappa
!>        rho
!
!------------------------------------------------------------------------------
module m_create_pseudoh

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: RKIND_MAT,IKIND_MESH
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_sum,          only: allreduce_sum
  use m_allreduce_min,          only: allreduce_min
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model
  use m_ctx_field,              only: t_field
  use m_matrix_utils_derivative,only: hdg_build_dA_2D_tri,              &
                                      hdg_build_dA_3D_tet
  use m_model_list
  use m_model_eval,             only: model_eval_piecewise_constant
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT

  implicit none

  private
  public  :: create_pseudoh
  private :: create_pseudoh_2D_refelem,create_pseudoh_3D_refelem

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create pseudoh for HELMHOLTZ ACOUSTIC  using HDG
  !> pseudoh of the misfit, with respect to:
  !> grad(:,:,1) = dA / d{1/\kappa}
  !> grad(:,:,2) = dA / d\rho
  !> if viscosity
  !> grad(:,:,3) = dA / d\viscosity
  !>
  !> In addition, we extract the forward problem, per cell, that can 
  !> be used to adjust to the correct model function later on.
  !>
  !
  !> @param[in]    ctx_paral         : parallel context
  !> @param[in]    ctx_mesh          : mesh grid type
  !> @param[in]    ctx_model         : model parameters
  !> @param[in]    ctx_discretization: type DG for reference matrix
  !> @param[in]    ctx_field_fwd     : forward field   
  !> @param[in]    field_fwd         : forward field depends on misfit
  !> @param[in]    field_scale       : scaling
  !> @param[in]    frequency         : frequency
  !> @param[inout] pseudoh           : output computed pseudoh
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_pseudoh(ctx_paral,ctx_mesh,ctx_discretization,      &
                             ctx_model,ctx_field_fwd,angular_freq,      &
                             pseudoh,ctx_err)
    implicit none

    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_discretization)             ,intent(in)    :: ctx_discretization
    type(t_model)                      ,intent(in)    :: ctx_model
    type(t_field)                      ,intent(in)    :: ctx_field_fwd
    complex(kind=8)                    ,intent(in)    :: angular_freq
    real   (kind=4)        ,allocatable,intent(inout) :: pseudoh (:,:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    pseudoh      = 0.0


    !! -----------------------------------------------------------------
    !! create the pseudo-hessian depending on
    !!  - the dimension
    !!  - the model representation
    !!  - the method (quadrature / reference element)
    !! not every combination are compatible. 
    !! -----------------------------------------------------------------
    if(ctx_discretization%flag_eval_int_quadrature) then
      ctx_err%msg   = "** ERROR: quadrature not ready [create_pseudoH] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    
    else
      !! only works if we have a picewise constant parameter ...........
      if(trim(adjustl(ctx_model%format_disc)) .ne. tag_MODEL_PCONSTANT) then
        ctx_err%msg   = "** ERROR: reference elements method require "//&
                        "piecewise-constant model [create_pseudoH] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      
      !! create according to the dimension
      select case(ctx_mesh%dim_domain)
        case(2)
          call create_pseudoh_2D_refelem(ctx_discretization,ctx_mesh,   &
                                 ctx_model,ctx_field_fwd%field,         &
                                 angular_freq,ctx_field_fwd%n_src,      &
                                 pseudoh,ctx_err)
    
        case(3)
          call create_pseudoh_3D_refelem(ctx_discretization,ctx_mesh,   &
                                 ctx_model,ctx_field_fwd%field,         &
                                 angular_freq,ctx_field_fwd%n_src,      &
                                 pseudoh,ctx_err)

        case default
          ctx_err%msg   = "** ERROR: Helmholtz acoustic equation "   // &
                          "for HDG needs dimension 2/3 [create_pseudoh] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if
    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 

    return

  end subroutine create_pseudoh


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the pseudoh of the misfit, with respect to 1/\kappa and \rho
  !> for HDG in HELMHOLTZ ACOUSTIC in 2D based upon triangle discretization
  !> grad(:,:,1) = dA / d{1/\kappa}
  !> grad(:,:,2) = dA / d\rho
  !> if viscosity
  !> grad(:,:,3) = dA / d\viscosity
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_scale       : scaling wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] pseudoh           : pseudoh arrays
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_pseudoh_2D_refelem(ctx_dg,ctx_mesh,ctx_model,field_fwd,   &
                               freq,n_shot,pseudoh,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd  (:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: pseudoh(:,:)
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell,ic
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2
    integer                     :: iloc1,iloc2,n,idof,isol,icell_src,k
    complex(kind=8),allocatable :: sol_fwd_loc(:),scale_fwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8),allocatable :: tempfwd(:)
    complex(kind=8)             :: tempval(3),u,v,uprime,vprime,freqloc
    real   (kind=8)             :: Q,c,rho
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! we compute size max so that we have only ONE allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    !! local array, the 2 is for the two acoustic parameters
    !!              the 3 is for the three triangle faces
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,2))
    allocate(dAU          (dim_sol*ndof_max_vol,2))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(tempfwd(dim_sol))
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_order_cell,icell,n,idof)  ,&
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,isol)  ,&
    !$OMP&         PRIVATE(sol_fwd_loc,dA,dAU,tempval,u,v)             ,&
    !$OMP&         PRIVATE(uprime,vprime,freqloc,Q,c,rho,icell_src,ic) ,&
    !$OMP&         PRIVATE(tempfwd,k,scale_fwd_loc)
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      if(ctx_err%ierr .ne. 0) cycle
      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      ! ----------------------------------------------------------------
      !! loop over all shots--------------------------------------------
      do i_src = 1, n_shot
        !! multipler offset
        sol_fwd_loc   =0.0
        dA            =0.0
        dAU           =0.0

        ! ---------------------------------------------------------
        !! get the local (cell) wavefields
        igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
                   ctx_dg%offsetdof_sol_loc(icell)  + 1)
        igb2 = igb1 + ndof_vol - 1
        do isol=1,dim_sol
          iloc1 = (isol-1)*ndof_vol + 1
          iloc2 = iloc1  + ndof_vol - 1
          sol_fwd_loc  (iloc1:iloc2) = dcmplx(field_fwd  (igb1:igb2,isol))
          !scale_fwd_loc(iloc1:iloc2) = dcmplx(field_scale(igb1:igb2,isol))
        end do
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !! the pseudoh is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell
        call hdg_build_dA_2D_tri(dA,                                    &
                            ctx_dg%vol_matrix_mass(i_order_cell)%array, &
                            ctx_dg%det_jac(icell),ndof_vol,freq)
        !!> dC = 0 in acoustic  !! call hdg_build_dC_2D_tri

        !! compute dA U, for all parameters: 1/kappa and rho
        n = ndof_vol*dim_sol

        !! -------------------------------------------------------------
        dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))! /scale_fwd_loc(1:n)
        dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))! /scale_fwd_loc(1:n)
        tempval   = 0.0
        tempfwd   = 0.0
        do idof=1,n !! loop over all ndof * dim_solution
          !! Pseudo-Hessian values, see on top of this file:
          !!  u* dm(P)* dm(P) u
          tempval(1) = tempval(1)+conjg(-dAU(idof,1))*(-dAU(idof,1))
          tempval(2) = tempval(2)+conjg(-dAU(idof,2))*(-dAU(idof,2))
        end do
        
        !! also compute the averaged forward field, for scaling later
        do k = 1, dim_sol
          do idof=1,ndof_vol
            tempfwd(k)    = tempfwd(k) + sol_fwd_loc(ndof_vol*(k-1)+idof)
          end do
        end do
        
        !! average
        tempval    = dcmplx(tempval   / dble(ndof_vol))
        tempfwd    = dcmplx(tempfwd   / dble(ndof_vol))

        ! ---------------------------------------------------------
        !! in the case of viscosity
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF)
              !! we have V = Vp( 1 + i / (2*Q)) which correspond with
              !! 1/Kappa_Viscous = (1/(rho c²)) (4Q²-4iQ-1) / (4Q²+1)²
              !! in this case we derive w.r.t:
              !! 1/kappa = 1/(rho c²)
              !!   Q
              !! we use the RAW models 
              call model_eval_piecewise_constant(ctx_model,model_VP,    &
                                                 icell,c,ctx_err)
              call model_eval_piecewise_constant(ctx_model,model_RHO,   &
                                                 icell,rho,ctx_err)
              call model_eval_piecewise_constant(ctx_model,model_VISCOUS,&
                                                 icell,Q,ctx_err)
              !! derive w.r.t. Q
              u     =(4.*Q*Q*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))
              uprime=4.*16.*Q**3 - 2.*4.*Q - 3.*16.*dcmplx(0.,1.)*Q*Q
              v     =(4.*Q*Q+1.)**2
              vprime=2.*8.*Q*(4.*Q*Q + 1)

              tempval(3) = tempval(1) *                                 &
                        conjg(1./(rho*c*c)*(uprime*v - u*vprime)/(v*v))*&
                             (1./(rho*c*c)*(uprime*v - u*vprime)/(v*v))
              tempval(1) = tempval(1) *                                 &
              conjg((4.*Q*Q /((4.*Q*Q+1.)**2))*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))* &
                   ((4.*Q*Q /((4.*Q*Q+1.)**2))*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))

            case(model_visco_KV,model_visco_CC,model_visco_Maxw,        &
                 model_visco_Zener,model_visco_plaw,model_visco_Szabo,  &
                 model_visco_KSB)
              ctx_err%msg   = "** ERROR: viscosity model not yet " //   &
                              "treated for pseudo-Hessian "
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model "
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
          end select
        end if
        ! ---------------------------------------------------------
        !! save value, we sum over the shots here
        pseudoh(icell,1) = pseudoh(icell,1)+real(tempval  (1),kind=4)
        pseudoh(icell,2) = pseudoh(icell,2)+real(tempval  (2),kind=4)
        if(ctx_model%flag_viscous) then
          pseudoh(icell,3) = pseudoh(icell,3)+real(tempval(3),kind=4)
        end if
        ! ---------------------------------------------------------
      end do !! end loop over shots
    end do !! end loop over cells
    !$OMP END DO
    !$OMP END PARALLEL

    deallocate(dA           )
    deallocate(sol_fwd_loc  )
    deallocate(dAU          )

    return
  end subroutine create_pseudoh_2D_refelem

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the pseudoh of the misfit, with respect to 1/\kappa and \rho
  !> for HDG in HELMHOLTZ ACOUSTIC in 3D based upon triangle discretization
  !> grad(:,:,1) = dA / d{1/\kappa}
  !> grad(:,:,2) = dA / d\rho
  !> if viscosity
  !> grad(:,:,3) = dA / d\viscosity
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_scale       : scaling wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] pseudoh          : pseudoh arrays
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_pseudoh_3D_refelem(ctx_dg,ctx_mesh,ctx_model,field_fwd,   &
                               freq,n_shot,pseudoh,ctx_err)

    implicit none
    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd(:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: pseudoh(:,:)
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell,ic
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2
    integer                     :: iloc1,iloc2,n,idof,isol,icell_src,k
    complex(kind=8),allocatable :: sol_fwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8),allocatable :: tempfwd(:)
    complex(kind=8)             :: tempval(3),u,v,uprime,vprime,freqloc
    real   (kind=8)             :: Q,c,rho
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! we compute size max so that we have only ONE allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    !! local array, the 2 is for the two acoustic parameters
    !!              the 3 is for the three triangle faces
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,2))
    allocate(dAU          (dim_sol*ndof_max_vol,2))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(tempfwd(dim_sol))
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_order_cell,icell,n,idof)  ,&
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,isol)  ,&
    !$OMP&         PRIVATE(sol_fwd_loc,dA,dAU,tempval,u,v)             ,&
    !$OMP&         PRIVATE(uprime,vprime,freqloc,Q,c,rho,icell_src,ic) ,&
    !$OMP&         PRIVATE(tempfwd,k)
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      if(ctx_err%ierr .ne. 0) cycle
      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      ! ----------------------------------------------------------------
      !! loop over all shots--------------------------------------------
      do i_src = 1, n_shot
        !! multipler offset
        sol_fwd_loc   =0.0
        dA            =0.0
        dAU           =0.0

        ! ---------------------------------------------------------
        !! get the local (cell) wavefields
        igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
                   ctx_dg%offsetdof_sol_loc(icell)  + 1)
        igb2 = igb1 + ndof_vol - 1
        do isol=1,dim_sol
          iloc1 = (isol-1)*ndof_vol + 1
          iloc2 = iloc1  + ndof_vol - 1
          sol_fwd_loc  (iloc1:iloc2) = dcmplx(field_fwd  (igb1:igb2,isol))
        end do
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !! the pseudoh is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell
        call hdg_build_dA_3D_tet(dA,                                    &
                            ctx_dg%vol_matrix_mass(i_order_cell)%array, &
                            ctx_dg%det_jac(icell),ndof_vol,freq)
        !!> dC = 0 in acoustic  !! call hdg_build_dC_2D_tri

        !! compute dA U, for all parameters: 1/kappa and rho
        n = ndof_vol*dim_sol

        !! -------------------------------------------------------------
        dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))
        dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))
        tempval   = 0.0
        tempfwd   = 0.0
        do idof=1,n !! loop over all ndof * dim_solution
          !! Pseudo-Hessian values, see on top of this file:
          !!  u* dm(P)* dm(P) u
          tempval(1) = tempval(1)+conjg(-dAU(idof,1))*(-dAU(idof,1))
          tempval(2) = tempval(2)+conjg(-dAU(idof,2))*(-dAU(idof,2))
        end do
        
        !! also compute the averaged forward field, for scaling later
        do k = 1, dim_sol
          do idof=1,ndof_vol
            tempfwd(k)    = tempfwd(k) + sol_fwd_loc(ndof_vol*(k-1)+idof)
          end do
        end do
        
        !! average
        tempval    = dcmplx(tempval   / dble(ndof_vol))
        tempfwd    = dcmplx(tempfwd   / dble(ndof_vol))

        ! ---------------------------------------------------------
        !! in the case of viscosity
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KF)
              !! we have V = Vp( 1 + i / (2*Q)) which correspond with
              !! 1/Kappa_Viscous = (1/(rho c²)) (4Q²-4iQ-1) / (4Q²+1)²
              !! in this case we derive w.r.t:
              !! 1/kappa = 1/(rho c²)
              !!   Q
              !! we use the RAW models 
              call model_eval_piecewise_constant(ctx_model,model_VP,    &
                                                 icell,c,ctx_err)
              call model_eval_piecewise_constant(ctx_model,model_RHO,   &
                                                 icell,rho,ctx_err)
              call model_eval_piecewise_constant(ctx_model,model_VISCOUS,&
                                                 icell,Q,ctx_err)
              !! derive w.r.t. Q
              u     =(4.*Q*Q*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))
              uprime=4.*16.*Q**3 - 2.*4.*Q - 3.*16.*dcmplx(0.,1.)*Q*Q
              v     =(4.*Q*Q+1.)**2
              vprime=2.*8.*Q*(4.*Q*Q + 1)

              tempval(3) = tempval(1) *                                 &
                        conjg(1./(rho*c*c)*(uprime*v - u*vprime)/(v*v))*&
                             (1./(rho*c*c)*(uprime*v - u*vprime)/(v*v))
              tempval(1) = tempval(1) *                                 &
              conjg((4.*Q*Q /((4.*Q*Q+1.)**2))*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))* &
                   ((4.*Q*Q /((4.*Q*Q+1.)**2))*(4.*Q*Q-4*dcmplx(0.,1.)*Q-1.))

            case(model_visco_KV,model_visco_CC,model_visco_Maxw,        &
                 model_visco_Zener,model_visco_plaw,model_visco_Szabo,  &
                 model_visco_KSB)
              ctx_err%msg   = "** ERROR: viscosity model not yet " //   &
                              "treated for pseudo-Hessian "
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model "
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
          end select
        end if
        ! ---------------------------------------------------------
        !! save value, we sum over the shots here
        pseudoh(icell,1) = pseudoh(icell,1)+real(tempval  (1),kind=4)
        pseudoh(icell,2) = pseudoh(icell,2)+real(tempval  (2),kind=4)
        if(ctx_model%flag_viscous) then
          pseudoh(icell,3) = pseudoh(icell,3)+real(tempval(3),kind=4)
        end if
        ! ---------------------------------------------------------
      end do !! end loop over shots
    end do !! end loop over cells
    !$OMP END DO
    !$OMP END PARALLEL

    deallocate(dA           )
    deallocate(sol_fwd_loc  )
    deallocate(dAU          )


    return
  end subroutine create_pseudoh_3D_refelem
end module m_create_pseudoh
