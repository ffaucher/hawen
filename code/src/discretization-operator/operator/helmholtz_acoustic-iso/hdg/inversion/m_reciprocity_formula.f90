!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_reciprocity_formula.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the reciprocity formula for 
!> acoustic formulation: it depends on the problem 
!> dimension but basically, we need all V and Pressure
!> 
!
!------------------------------------------------------------------------------
module m_reciprocity_formula

  use m_raise_error,              only : raise_error, t_error
  use m_define_precision,         only : RKIND_MAT
  use m_tag_namefield

  implicit none

  private
  public  :: reciprocity_fields, reciprocity_formula
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Provides the fields that are required to compute the reciprocity.
  !> deending on the receivers normal directions, not everything has 
  !> to be known.
  !
  !> @param[in]    dim_domain      : dimension of the domain
  !> @param[out]   name_field_out  : names of the fields for reciprocity
  !> @param[in]    normals_rcvx    : maximal normal direction in x
  !> @param[in]    normals_rcvy    : maximal normal direction in y
  !> @param[in]    normals_rcvz    : maximal normal direction in z
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine reciprocity_fields(dim_domain,name_field_out,normals_rcvx, &
                                normals_rcvy,normals_rcvz,ctx_err)
    
    implicit none
    
    integer                      ,intent(in)   :: dim_domain
    character(len=*),allocatable ,intent(inout):: name_field_out(:)
    real(kind=8)                 ,intent(in)   :: normals_rcvx
    real(kind=8)                 ,intent(in)   :: normals_rcvy
    real(kind=8)                 ,intent(in)   :: normals_rcvz
    type(t_error)                ,intent(out)  :: ctx_err
    !!
    real(kind=8) :: tol

    ctx_err%ierr  = 0
    tol = 1E-12

    !! -----------------------------------------------------------------
    !! Reciprocity formula depends on the dimension
    !! -----------------------------------------------------------------  
    select case(dim_domain)
      case(2)
        !! We respect the numbering of the equations -------------------
        name_field_out(1) = tag_FIELD_VX
        name_field_out(2) = tag_FIELD_VZ
        name_field_out(3) = tag_FIELD_P

        !! the rest depends on the normal, 
        if(normals_rcvx < tol) name_field_out(1) = '' !> remove vx
        if(normals_rcvz < tol) name_field_out(2) = '' !> remove vz
        !! consistancy 
        if(normals_rcvx < tol .and. normals_rcvz < tol) then
          ctx_err%msg   = "** ERROR: Normal receivers direction is below "// &
                          "tolerance [reciprocity_fields] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if 


      case(3)
        !! We respect the numbering of the equations -------------------
        name_field_out(1) = tag_FIELD_VX
        name_field_out(2) = tag_FIELD_VY
        name_field_out(3) = tag_FIELD_VZ
        name_field_out(4) = tag_FIELD_P

        !! the rest depends on the normal, 
        if(normals_rcvx < tol) name_field_out(1) = '' !> remove vx
        if(normals_rcvy < tol) name_field_out(2) = '' !> remove vy
        if(normals_rcvz < tol) name_field_out(3) = '' !> remove vz
        
        if(normals_rcvx < tol .and. normals_rcvy < tol .and. &
           normals_rcvz < tol) then
          ctx_err%msg   = "** ERROR: Normal receivers direction is below "// &
                          "tolerance [reciprocity_fields] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

      case default
        ctx_err%msg   = "** ERROR: Equation dimension must be 2/3 "// &
                        "[reciprocity_fields] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select    
    !! -----------------------------------------------------------------

    return
  end subroutine reciprocity_fields

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the reciprocity misfit functional and appropriate 
  !> difference for rhs 
  !
  !> @param[in]    dim_domain      : dimension of the domain
  !> @param[in]    obs             : observation
  !> @param[in]    sim             : simulation
  !> @param[in]    n_val           : number of receivers
  !> @param[out]   cost            : output cost value
  !> @param[inout] rhs             : rhs array for backpropagation
  !> @param[in]    nx              : normal direction in x
  !> @param[in]    ny              : normal direction in y
  !> @param[in]    nz              : normal direction in z
  !> @param[in]    flag_log        : indicates if log functional
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine reciprocity_formula(dim_domain,obs,sim,n_val,cost,rhs,     &
                                 nx,ny,nz,flag_log,ctx_err)
    
    implicit none
    
    integer                             ,intent(in)   :: dim_domain
    complex(kind=RKIND_MAT),allocatable ,intent(in)   :: obs(:,:),sim(:,:)
    integer                             ,intent(in)   :: n_val
    complex(kind=8)        ,allocatable ,intent(inout):: rhs(:,:)
    real   (kind=8)                     ,intent(inout):: cost
    real   (kind=8)        ,allocatable ,intent(in)   :: nx(:)
    real   (kind=8)        ,allocatable ,intent(in)   :: ny(:)
    real   (kind=8)        ,allocatable ,intent(in)   :: nz(:)
    logical                             ,intent(in)   :: flag_log
    type(t_error)                       ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8) :: tol
    complex(kind=8) :: field1, field2, delta, v1, v2
    integer         :: k
    !! -----------------------------------------------------------------

    tol     = 1.E-20 !! for log norm only
    rhs     = 0.0
    cost    = 0.0
    field1  = 0.0
    field2  = 0.0

    select case(dim_domain)
      ! ----------------------------------------------------------------
      ! ----------------------------------------------------------------
      !! TWO DIMENSIONS
      ! ----------------------------------------------------------------
      ! ----------------------------------------------------------------
      case(2)
        !! standard formulation
        if(.not. flag_log) then
          do k=1,n_val
            !! P_sim (VX_obs*NX + VZ_obs*NZ)
            field1 = field1 + dcmplx(sim(k,3)*(obs(k,1)*nx(k) + obs(k,2)*nz(k)))
            field2 = field2 + dcmplx(obs(k,3)*(sim(k,1)*nx(k) + sim(k,2)*nz(k)))
            rhs(k,1) =-conjg(obs(k,3)*nx(k))                    !! VX
            rhs(k,2) =-conjg(obs(k,3)*nz(k))                    !! VZ
            rhs(k,3) = conjg((obs(k,1)*nx(k) + obs(k,2)*nz(k))) !! P
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        !! LOG FORMULATION ---------------------------------------------
        else
          do k=1,n_val
            v1 = dcmplx(sim(k,3)*(obs(k,1)*nx(k) + obs(k,2)*nz(k)))
            v2 = dcmplx(obs(k,3)*(sim(k,1)*nx(k) + sim(k,2)*nz(k)))
            !! only if both are ok with log
            if(abs(v1) > tol .and. abs(v2) > tol) then
              field1 = field1 + log(v1)
              field2 = field2 + log(v2)
            
              !! the derivative makes the log desapear
              if(abs(sim(k,1))>tol) rhs(k,1) = & 
                                      -conjg(1./sim(k,1)*obs(k,3)*nx(k))!! VX
              if(abs(sim(k,2))>tol) rhs(k,2) = & 
                                      -conjg(1./sim(k,2)*obs(k,3)*nz(k))!! VZ
              if(abs(sim(k,3))>tol) rhs(k,3) = conjg(1./sim(k,3)*       &
                                    (obs(k,1)*nx(k) + obs(k,2)*nz(k)))  !! P
            end if
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        endif
      case(3)
        ! ---------------------------------------------------------
        ! ---------------------------------------------------------
        !! THREE DIMENSIONS
        ! ---------------------------------------------------------
        ! ---------------------------------------------------------
        !! standard formulation
        if(.not. flag_log) then
          do k=1,n_val
            !! P_sim (VX_obs*NX + VY_obs*NY + VZ_obs*NZ)
            v1 = dcmplx(sim(k,4)*(obs(k,1)*nx(k)+obs(k,2)*ny(k)+obs(k,3)*nz(k)))
            v2 = dcmplx(obs(k,4)*(sim(k,1)*nx(k)+sim(k,2)*ny(k)+sim(k,3)*nz(k)))
            field1 = field1 + v1
            field2 = field2 + v2
            rhs(k,1) =-conjg(obs(k,4)*nx(k))                                 !! VX
            rhs(k,2) =-conjg(obs(k,4)*ny(k))                                 !! VY
            rhs(k,3) =-conjg(obs(k,4)*nz(k))                                 !! VZ
            rhs(k,4) = conjg((obs(k,1)*nx(k)+obs(k,2)*ny(k)+obs(k,3)*nz(k))) !! P
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        !! LOG FORMULATION ---------------------------------------------
        else
          do k=1,n_val
            v1 = dcmplx(sim(k,4)*(obs(k,1)*nx(k)+obs(k,2)*ny(k)+obs(k,3)*nz(k)))
            v2 = dcmplx(obs(k,4)*(sim(k,1)*nx(k)+sim(k,2)*ny(k)+sim(k,3)*nz(k)))
            !! only if both are ok with log
            if(abs(v1) > tol .and. abs(v2) > tol) then
              field1 = field1 + log(v1)
              field2 = field2 + log(v2)
            
              !! the derivative makes the log desapear
              if(abs(sim(k,1)) > tol) rhs(k,1) =-conjg(1./sim(k,1)*     & 
                                                        obs(k,4)*nx(k)) !! VX
              if(abs(sim(k,2)) > tol) rhs(k,2) =-conjg(1./sim(k,2)*     & 
                                                        obs(k,4)*ny(k)) !! VY
              if(abs(sim(k,3)) > tol) rhs(k,3) =-conjg(1./sim(k,3)*     & 
                                                        obs(k,4)*nz(k)) !! VZ
              if(abs(sim(k,4)) > tol) rhs(k,4) = conjg(1./sim(k,4)*     &
                    (obs(k,1)*nx(k)+obs(k,2)*ny(k)+obs(k,3)*nz(k)))     !! P
            end if
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        endif
      case default
        ctx_err%msg   = "** ERROR: Equation dimension must be 2/3 "// &
                        "[reciprocity_formula] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        !! non-shared error
    end select    
    !! -----------------------------------------------------------------

    return
  end subroutine reciprocity_formula

end module m_reciprocity_formula
