!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_utils_derivative.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix derivative for
!> HELMHOLTZ ACOUSTIC propagator using the HDG 
!> discretization:
!> We derive with respect to the physical parameter: 1/\kappa and \rho
!> Note that in acoustic isotropic, only the matrix A contains the 
!> model parameters
!
!------------------------------------------------------------------------------
module m_matrix_utils_derivative

  !! module used -------------------------------------------------------
  use m_define_precision,       only: IKIND_MESH, RKIND_POL
  use m_ctx_discretization,     only: t_discretization
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: hdg_build_dA_2D_tri
  public :: hdg_build_dA_3D_tet

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the matrix derivative dA, with respect to \kappa and \rho
  !> for HDG in HELMHOLTZ ACOUSTIC in 2D based upon triangle discretization 
  !> dA(:,:,1) = dA / d{1/\kappa}
  !> dA(:,:,2) = dA / d\rho
  !
  !> @param[inout] dA             : dA w.r.t. parameters
  !> @param[in]    vol_mass_matrix: mass matrix
  !> @param[in]    det_jac        : jacobian determinant of current cell
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !----------------------------------------------------------------------------
  subroutine hdg_build_dA_2D_tri(dA,vol_mass_matrix,det_jac,ndof_vol,freq)
    implicit none
    complex(kind=8)        , allocatable, intent(inout):: dA(:,:,:)
    real   (kind=RKIND_POL)             , intent(in)   :: vol_mass_matrix(:,:)
    real   (kind=8)                     , intent(in)   :: det_jac
    complex(kind=8)                     , intent(in)   :: freq
    integer                             , intent(in)   :: ndof_vol
    !!
    real   (kind=8) :: massterm
    integer         :: i, j

    dA=0.0 
    do i=1, ndof_vol
      do j=1, ndof_vol
        massterm     = dble(vol_mass_matrix(i,j) * det_jac)
        !! derivative with respect to rho 
        dA(i           , j           ,2) = dcmplx(-freq*massterm)
        dA(i+ndof_vol  , j+  ndof_vol,2) = dcmplx(-freq*massterm)
        !! derivative with respect to 1/kappa
        dA(i+2*ndof_vol, j+2*ndof_vol,1) = dcmplx(-freq*massterm)
      enddo
    enddo

    return
  end subroutine hdg_build_dA_2D_tri


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the matrix derivative dA, with respect to \kappa and \rho
  !> for HDG in HELMHOLTZ ACOUSTIC in 3D based upon triangle discretization 
  !> dA(:,:,1) = dA / d{1/\kappa}
  !> dA(:,:,2) = dA / d\rho
  !
  !> @param[inout] ctx_dg         : type DG
  !> @param[inout] dA             : dA w.r.t. parameters
  !> @param[in]    sumE           : sumE
  !> @param[in]    i_cell         : current cell
  !> @param[in]    i_order        : current order index
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !----------------------------------------------------------------------------
  subroutine hdg_build_dA_3D_tet(dA,vol_mass_matrix,det_jac,ndof_vol,freq)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: dA  (:,:,:)
    real   (kind=RKIND_POL)          ,intent(in)   :: vol_mass_matrix(:,:)
    real   (kind=8)                  ,intent(in)   :: det_jac
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: ndof_vol
    !!
    real   (kind=8) :: massterm
    integer         :: i, j

    dA=0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        massterm     = dble(vol_mass_matrix(i,j)*det_jac)
        !! derivative with respect to \rho
        dA(i           , j           ,2) = dcmplx(-freq*massterm)
        dA(i+  ndof_vol, j+  ndof_vol,2) = dcmplx(-freq*massterm)
        dA(i+2*ndof_vol, j+2*ndof_vol,2) = dcmplx(-freq*massterm)
        !! derivative with respect to 1/\kappa
        dA(i+3*ndof_vol, j+3*ndof_vol,1) = dcmplx(-freq*massterm)

      enddo
    enddo

    return
  end subroutine hdg_build_dA_3D_tet

end module m_matrix_utils_derivative
