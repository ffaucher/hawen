!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_wavelength_utils.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for wavelength operation on the mesh,
!> computing the size of the element and using the polynomial
!> order on the current cell
!
!------------------------------------------------------------------------------
module m_wavelength_utils

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_equation,           only: t_equation
  use m_allreduce_max,          only: allreduce_max
  use m_allreduce_min,          only: allreduce_min
  use m_define_precision,       only: RKIND_MESH,IKIND_MESH
  use m_ctx_model,              only: t_model, compute_gamma_powerlaw
  use m_ctx_domain,             only: t_domain
  use m_tag_bc,                 only: tag_ghost_cell

  use m_ctx_discretization,     only: t_discretization
  use m_mesh_simplex_area,      only: mesh_area_simplex
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: compute_wavelength_order, compute_wavelength_dof_order

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the wavelength for the current wave problem
  !
  !> @param[in]    angular_freq         : angular frequency
  !> @param[in]    vel                  : velocity
  !> @param[in]    alpha                : alpha
  !> @param[in]    dalpha               : alpha'
  !> @param[in]    gamm                 : attenuation
  !> @param[in]    xpos                 : r position
  !> @param[in]    wl                   : output computed wavelength
  !----------------------------------------------------------------------------
  pure function compute_wavelength(angular_freq,vel,alpha,dalpha,gamm,xpos)   &
                result(wl)

    implicit none
    complex(kind=8)      ,intent(in)  :: angular_freq
    real   (kind=8)      ,intent(in)  :: vel,alpha,dalpha,gamm,xpos
    real   (kind=8)                   :: wl
    !!
    real   (kind=8)                   :: omega
    complex(kind=8)                   :: wavenumber,sigma,q
    
    ! ----------------------------------------------------------------
    !! The wavenumber is given by 
    !! k = sqrt( kappa² - q )
    !! with
    !! kappa = sigma / c
    !! q     = alpha²/4 + alpha'/2 + alpha/r 
    !! therefore the wavelength uses alpha, alpha' and r as well.
    ! ----------------------------------------------------------------

    wl = 0.d0
    !! -----------------------------------------------------------------
    !! compute q and deduce the wavenumber
    !! -----------------------------------------------------------------
    !! if the frequency is not zero 
    omega = aimag(angular_freq) !! only the "Fourier" part (2pi)
    sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*gamm/omega) * omega)
    if( omega > tiny(1.0)) then
      if(xpos > tiny(1.0)) then
        q = alpha**2/4. + dalpha/2. + alpha/xpos
      else 
        if(alpha < tiny(1.0)) then !! still ok 
          q = alpha**2/4. + dalpha/2.
        else
          !! q tends to infinity
          q = huge(1.0)
        end if
      end if
      !! wave-number
      wavenumber = dcmplx(sqrt(sigma**2/(vel**2) - q))
    else 
      !! there is a huge wavenumber ...
      wavenumber = huge(1.0)
    end if
    !! -----------------------------------------------------------------

    !! if it is imaginary or for small r we have an issue and replace by
    !! a traditional version... 
    if(real(wavenumber) < tiny(1.0)) then
      wavenumber = dcmplx(sqrt(sigma**2/(vel**2)))
    endif 
    
    !! 2 pi / wavenumber
    wl = dble(2.d0*4.d0*datan(1.d0)/wavenumber)
    !! **********************************************************
    !! using only the velocity 
    wl = 2.d0*4.d0*datan(1.d0)*vel/aimag(angular_freq)
    !! **********************************************************


    return
  end function
  !----------------------------------------------------------------------------
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the number of dof for all cells
  !
  !> @param[in]    ctx_paral         : contex parallelism
  !> @param[in]    ctx_model         : model values
  !> @param[in]    ctx_mesh          : mesh infos
  !> @param[in]    ctx_dg            : dg infos
  !> @param[in]    ctx_eq            : equation infos
  !> @param[in]    angular_freq      : angular frequency
  !> @param[out]   ndof_per_wl_order : number of dof per wl per order
  !> @param[out]   wavelength_min    : minimal wavelength
  !> @param[out]   wavelength_max    : maximal wavelength
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine compute_wavelength_dof_order(ctx_paral,ctx_model,ctx_mesh, &
                                          ctx_dg,ctx_eq,                &
                                          ndof_per_wl_order,            &
                                          wavelength_min,wavelength_max,&
                                          ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_model)                    ,intent(in)   :: ctx_model
    type(t_discretization)           ,intent(in)   :: ctx_dg
    type(t_domain)                   ,intent(in)   :: ctx_mesh
    type(t_equation)                 ,intent(in)   :: ctx_eq
    real   (kind=8)      ,allocatable,intent(inout):: ndof_per_wl_order(:)
    real   (kind=8)                  ,intent(out)  :: wavelength_min
    real   (kind=8)                  ,intent(out)  :: wavelength_max
    type(t_error)                    ,intent(inout):: ctx_err
    !!
    integer(kind=IKIND_MESH)            :: icell
    integer                             :: i_order,inode,k
    real(kind=RKIND_MESH),allocatable   :: coo_node(:,:)
    real(kind=8)         ,allocatable   :: wtemp(:)
    real(kind=8)                        :: area,wavelength,ndof
    real(kind=8)                        :: wlloc,min_ndof,xpos,gamm
    complex(kind=8)                     :: angular_freq
    !!
    ctx_err%ierr = 0

    !! get frequency 
    angular_freq = ctx_eq%current_angular_frequency

    !! for every order, we keep the minimal ndof per wavelength.
    !! we also compute the minimal and maximal wavelength
    !! restrict to the order that exists in the current mesh.
    ndof_per_wl_order =-1 
    wavelength_min    = 1D300
    wavelength_max    =-1D300
    !! initialize those that exist
    do i_order=1,ctx_dg%order_max_gb
      k=ctx_dg%order_to_index(i_order)
      if(k > 0) ndof_per_wl_order(i_order) = 1D300
    end do
  
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))   
    !! loop over all cells
    do icell=1,ctx_mesh%n_cell_loc
      do inode = 1, ctx_mesh%n_neigh_per_cell
        coo_node(:,inode)=ctx_mesh%x_node(:,ctx_mesh%cell_node(inode,icell))
      end do
      !! compute area of current cell
      call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)

      !! get how many dof in this cell
      i_order = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof    = dble(ctx_dg%n_dof_per_order(i_order))

      !! compute cell wavelength, it now depends on the gamma 
      !! and on the spherical position, where we use the 
      !! barycenter of the cell.
      xpos   = norm2(ctx_mesh%cell_bary(:,icell))
      gamm   = 0.0
      if(ctx_model%flag_viscous) then
        select case(trim(adjustl(ctx_model%viscous_model)))
          case('gamma')
            !! we only use the piecewise-constant representation per simplicity
            gamm = dble(ctx_model%gamm%pconstant(icell))
          case('gamma0_power-law')
            !! compute using the power law in m_ctx_model
            !! we only use the piecewise-constant representation per simplicity
            gamm = compute_gamma_powerlaw(                              &
                   dble((ctx_model%gamm%pconstant(icell))),angular_freq)
            
          case default
            ctx_err%msg   = "** ERROR: viscosity model for helio "// &
                            "scalar equation not recognized [wavelength] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end if
      !! we only use the piecewise-constant representation per simplicity
      wavelength = compute_wavelength(angular_freq,                     &
                            dble(ctx_model%vp    %pconstant(icell)),    &
                            dble(ctx_model%alpha %pconstant(icell)),    &
                            dble(ctx_model%dalpha%pconstant(icell)),gamm,xpos)

      !! adjust to one-dimension
      area = area**(1./real(ctx_mesh%dim_domain))
      ndof = ndof**(1./real(ctx_mesh%dim_domain))

      !! min ndof per wavelength
      min_ndof = min((ndof*wavelength/area),ndof_per_wl_order(ctx_mesh%order(icell)))
      ndof_per_wl_order(ctx_mesh%order(icell)) = min_ndof
      !! min and max wavelength
      wavelength_min = min(wavelength_min,wavelength)
      wavelength_max = max(wavelength_max,wavelength)

    end do
    deallocate(coo_node)

    !! all reduce
    wlloc = wavelength_min
    call allreduce_min(wlloc,wavelength_min,1,ctx_paral%communicator,ctx_err)
    wlloc = wavelength_max
    call allreduce_max(wlloc,wavelength_max,1,ctx_paral%communicator,ctx_err)
    allocate(wtemp(ctx_mesh%order_max))
    wtemp = ndof_per_wl_order
    call allreduce_min(wtemp,ndof_per_wl_order,ctx_mesh%order_max,      &
                       ctx_paral%communicator,ctx_err)
    deallocate(wtemp)
    !! -----------------------------------------------------------------

    return
  end subroutine compute_wavelength_dof_order

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> From the wavelength, we estimate the order that one should employ.
  !> We modify directly the mesh%order, which is already allocated and 
  !> use the maximal order given in ctx_mesh%order_max
  !> using following heuristic.
  !> NOTE That for this problem, it appears that we need 
  !>      more than the traditional acoustic wave equation.
  !> P1: >= 40 d.o.f. per wavelength
  !> P2: >= 20 d.o.f. per wavelength
  !> P3: >= 10 d.o.f. per wavelength
  !> P4: >=  7 d.o.f. per wavelength
  !> P5: >=  5 d.o.f. per wavelength
  !> P6: >=  4 d.o.f. per wavelength
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    ctx_model      : model values
  !> @param[in]    ctx_mesh       : mesh infos
  !> @param[in]    ctx_eq         : equation context
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine compute_wavelength_order(ctx_paral,ctx_model,ctx_mesh,     &
                                      ctx_eq,ctx_err)
    implicit none
    type(t_parallelism)              ,intent(in)   :: ctx_paral
    type(t_model)                    ,intent(in)   :: ctx_model
    type(t_domain)                   ,intent(inout):: ctx_mesh
    type(t_equation)                 ,intent(in)   :: ctx_eq
    type(t_error)                    ,intent(inout):: ctx_err
    !!
    integer(kind=IKIND_MESH)            :: icell,i_ghost,i_face,neigh
    integer                             :: i_order,inode,ineigh
    integer                             :: order_cell,order_neigh
    integer              ,allocatable   :: list_gb_order(:)
    integer              ,allocatable   :: list_loc_order(:)
    real(kind=8)         ,allocatable   :: ndof_try(:)
    real(kind=RKIND_MESH),allocatable   :: coo_node(:,:)
    real(kind=8)                        :: area,wavelength,ndof,xpos,gamm
    complex(kind=8)                     :: angular_freq
    !!
    ctx_err%ierr = 0

    !! get frequency 
    angular_freq = ctx_eq%current_angular_frequency

    !! consistency check
    if(ctx_mesh%order_max <= 0 .or. ctx_mesh%order_min <= 0) then
      ctx_err%msg   = "** ERROR: maximal order must be > 0 [wavelength_dof] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    !! compute number of dof per possible order
    allocate(ndof_try(ctx_mesh%order_max))
    ndof_try = 0   
    select case(ctx_mesh%dim_domain)
      case(1) !! 1 dimension -------------------------------------------
        !! note that in one-dimension, the order of the face is forced to
        !! be 1. this is done at the end 
        do i_order=1,ctx_mesh%order_max
          ndof_try(i_order) = (i_order+1)
        end do
      case(2) !! 2 dimensions ------------------------------------------
        do i_order=1,ctx_mesh%order_max
          ndof_try(i_order) = (i_order+2)*(i_order+1)/2
        end do
      case(3) !! 3 dimensions ------------------------------------------
        do i_order=1,ctx_mesh%order_max
          ndof_try(i_order) = (i_order+3)*(i_order+2)*(i_order+1)/6
        end do
      case default
        ctx_err%msg   = "** ERROR: Unrecognized Dimension [wavelength_dof] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! One-dimensional perspective
    ndof_try     = ndof_try**(1./real(ctx_mesh%dim_domain))
    ctx_mesh%order = -1
    !! -----------------------------------------------------------------
    
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))   
    !! loop over all cells
    do icell=1,ctx_mesh%n_cell_loc
      do inode = 1, ctx_mesh%n_neigh_per_cell
        coo_node(:,inode)=ctx_mesh%x_node(:,ctx_mesh%cell_node(inode,icell))
      end do
      !! compute area of current cell
      call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)
      !! adjust to one-dimension
      area = area**(1./real(ctx_mesh%dim_domain))

      !! compute cell wavelength, it now depends on the gamma 
      !! and on the spherical position, where we use the 
      !! barycenter of the cell.
      xpos   = norm2(ctx_mesh%cell_bary(:,icell))
      gamm   = 0.0
      if(ctx_model%flag_viscous) then
        select case(trim(adjustl(ctx_model%viscous_model)))
          case('gamma')
            !! we only use the piecewise-constant representation per simplicity
            gamm = dble(ctx_model%gamm%pconstant(icell))
          case('gamma0_power-law')
            !! compute using the power law in m_ctx_model
            !! we only use the piecewise-constant representation per simplicity
            gamm = compute_gamma_powerlaw(                              &
                   dble((ctx_model%gamm%pconstant(icell))),angular_freq)
            
          case default
            ctx_err%msg   = "** ERROR: viscosity model for helio "// &
                            "scalar equation not recognized [wavelength] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end if
      wavelength = compute_wavelength(angular_freq,                     &
                            dble(ctx_model%vp    %pconstant(icell)),    &
                            dble(ctx_model%alpha %pconstant(icell)),    &
                            dble(ctx_model%dalpha%pconstant(icell)),gamm,xpos)
      
      !! loop over possible order
      ctx_mesh%order(icell) = -1
      do i_order=ctx_mesh%order_min,ctx_mesh%order_max
        ndof = ndof_try(i_order)*wavelength/area
        if(ctx_mesh%order(icell) <= 0) then
          !! arbitrary values
          if(i_order == 1 .and. ndof >= 30) ctx_mesh%order(icell) = 1
          if(i_order == 2 .and. ndof >= 15) ctx_mesh%order(icell) = 2
          if(i_order == 3 .and. ndof >= 10) ctx_mesh%order(icell) = 3
          if(i_order == 4 .and. ndof >=  9) ctx_mesh%order(icell) = 4
          if(i_order == 5 .and. ndof >=  8) ctx_mesh%order(icell) = 5
          if(i_order == 6 .and. ndof >=  7) ctx_mesh%order(icell) = 6
          if(i_order == 7 .and. ndof >=  6) ctx_mesh%order(icell) = 7 
          if(i_order == 8 .and. ndof >=  5) ctx_mesh%order(icell) = 8
          if(i_order == 9 .and. ndof >=  4) ctx_mesh%order(icell) = 9
          if(i_order >=10 .and. ndof >=  4) ctx_mesh%order(icell) = i_order
          if(i_order == ctx_mesh%order_max) ctx_mesh%order(icell) =     &
                                            ctx_mesh%order_max
        end if
      end do
    end do
    deallocate(coo_node)
    deallocate(ndof_try)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! need to update ghost order and face order
    allocate(list_loc_order(ctx_mesh%n_cell_gb))
    allocate(list_gb_order (ctx_mesh%n_cell_gb))
    list_loc_order = -1
    list_gb_order  = -1
    do icell=1,ctx_mesh%n_cell_loc
      list_loc_order(ctx_mesh%index_loctoglob_cell(icell)) = ctx_mesh%order(icell)
    end do
    !! reduce
    call allreduce_max(list_loc_order,list_gb_order,                    &
                       int(ctx_mesh%n_cell_gb),ctx_paral%communicator,ctx_err)
    deallocate (list_loc_order)
    
    !! adjust ghost cells
    do icell = ctx_mesh%n_cell_loc+1,ctx_mesh%n_cell_loc+ctx_mesh%n_ghost_loc
      ctx_mesh%order(icell) = list_gb_order(ctx_mesh%index_loctoglob_cell(icell))
    end do
    deallocate(list_gb_order)
    
    !! face order: loop over cells
    i_ghost = ctx_mesh%n_cell_loc
    do icell=1,ctx_mesh%n_cell_loc
      do ineigh=1,ctx_mesh%n_neigh_per_cell
        !! local face number
        i_face = ctx_mesh%cell_face (ineigh,icell)

        !! the order is the max of the adjacent cell
        order_cell = ctx_mesh%order     (icell)
        neigh      = ctx_mesh%cell_neigh(ineigh,icell)
        !! carefull with ghost index
        if(neigh .eq. tag_ghost_cell) then
          i_ghost = i_ghost + 1 !! already init with ncell_loc
            neigh = i_ghost
        end if

        !! non-boundary: check adjacent cell ---------------------------
        if(neigh>0) then
          order_neigh                 = ctx_mesh%order     (neigh)
          ctx_mesh%order_face(i_face) = max(order_cell,order_neigh)
        !! boundary ----------------------------------------------------
        else
          ctx_mesh%order_face (i_face)= order_cell
        end if
      end do
    end do
    !! -----------------------------------------------------------------
    !! in one-dimension, the order of the face is forced to be one 
    !! -----------------------------------------------------------------
    if(ctx_mesh%dim_domain == 1) ctx_mesh%order_face = 1

    return
  end subroutine compute_wavelength_order

end module m_wavelength_utils
