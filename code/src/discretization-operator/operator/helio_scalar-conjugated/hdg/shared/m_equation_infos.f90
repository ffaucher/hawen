!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_equation_infos.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module init of the information on the helio conjugated 
!> equation in the case of HDG discretization, i.e. we use order 
!> 1 formulation 
!
!------------------------------------------------------------------------------
module m_equation_infos

  use m_raise_error,              only : raise_error, t_error
  use m_tag_namefield
  use m_tag_scale_rhs,            only : tag_RHS_SCALE_ID
  implicit none

  private
  public  :: equation_infos
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init equation information for HDG
  !
  !> @param[in]    dim_domain      : dimension of the domain
  !> @param[out]   freq_dep        : indicates if this equation has a 
  !>                                 frequency parameter (yes for helmholtz!)
  !> @param[out]   mode_dep        : indicates if this equation has a mode 
  !> @param[out]   dim_solution    : number of solution
  !> @param[out]   dim_var         : number of matrix variable
  !> @param[out]   name_sol        : names of the solution
  !> @param[out]   name_var        : names of the variable (traces)
  !> @param[out]   symmetric_matrix: symmetry of the discretized matrix
  !> @param[out]   tag_scale_rhs   : indicates if rhs scaling
  !> @param[out]   n_media         : indicates maximal number of different media
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine equation_infos(dim_domain,                                 &
                            frequency_dependency,mode_dependency,eqname,&
                            medium,dim_solution,dim_var,name_sol,       &
                            name_var,formulation_order,symmetric_matrix,&
                            tag_scale_rhs,n_media,ctx_err)
    
    implicit none
    
    integer                      ,intent(in)   :: dim_domain
    integer                      ,intent(out)  :: dim_solution,dim_var
    integer                      ,intent(out)  :: formulation_order
    character(len=32)            ,intent(out)  :: eqname,medium
    character(len=3) ,allocatable,intent(inout):: name_sol(:),name_var(:)
    logical                      ,intent(out)  :: frequency_dependency
    logical                      ,intent(out)  :: mode_dependency
    logical                      ,intent(out)  :: symmetric_matrix
    integer                      ,intent(out)  :: tag_scale_rhs
    integer                      ,intent(out)  :: n_media
    type(t_error)                ,intent(out)  :: ctx_err
    !!

    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! Helmholtz acoustic equation using HDG
    !! order 1 formulation
    !! -----------------------------------------------------------------
    eqname   ='helio-scalar-equation_conjugated'
    medium   ='acoustic'
    frequency_dependency=.true.
    mode_dependency     =.false.
    n_media  = 1 !! only one type of medium in this problem.

    !! -----------------------------------------------------------------
    !! because of the formulation, we must scale the RHS
    tag_scale_rhs = tag_RHS_SCALE_ID
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! In one-dimension, the HDG matrix is not symmetric because of the
    !! Dirichlet boundary conditions...
    !! symmetric_matrix    =.true.
    !! -----------------------------------------------------------------
    formulation_order= 1 !! order 2 formuation
    dim_var          = 1 !! only one matrix variable: the pressure
    allocate(name_var(dim_var))
    name_var(1) = tag_FIELD_W !! scalar unknown

    !! should be symmetric.
    symmetric_matrix = .true.
    !! ----------------------------------------------------

    !! the solution dimension depends on the domain dim
    select case(dim_domain)

      case(2)
        dim_solution     = 3 !! W, VX, VZ
        allocate(name_sol(dim_solution))        
        name_sol(1) = tag_FIELD_VX
        name_sol(2) = tag_FIELD_VZ
        name_sol(3) = tag_FIELD_W

      case(3)
        dim_solution     = 4 !! W, VX, VY, VZ
        allocate(name_sol(dim_solution))
        name_sol(1) = tag_FIELD_VX
        name_sol(2) = tag_FIELD_VY
        name_sol(3) = tag_FIELD_VZ
        name_sol(4) = tag_FIELD_W
     
      case default
        ctx_err%msg   = "** ERROR: Equation dimension must be 2/3D "// &
                        " for this problem [equation_infos_HDG] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)

    end select    
    !! -----------------------------------------------------------------

    return
  end subroutine equation_infos
end module m_equation_infos
