!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_utils.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> helio scalar conjugated propagator using the
!> HDG discretization
!
!------------------------------------------------------------------------------
module m_matrix_utils

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH, RKIND_POL
  !> inverse matrix with Lapack
  use m_lapack_inverse,         only : lapack_matrix_inverse
  !> for the computation with quadrature rules
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_array_types,            only: t_array3d_real,t_array3d_complex
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_piecewise_constant,    &
                                      model_eval_piecewise_polynomial,  &
                                      model_eval_sep,                   &
                                      model_eval_dof_refelem,           &
                                      upper_half_sqrt
  use m_ctx_model_representation, only: tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_mesh_simplex_interface, only: interface_3D_tetrahedron
  use m_mesh_convert_face_position,                                     &
                                only: mesh_convert_face_position_2D,    &
                                      mesh_convert_face_position_3D
  use m_mesh_simplex_area,      only: mesh_area_simplex
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: hdg_build_Ainv_3D_tet_quadrature
  public :: hdg_build_C_3D_tet
  public :: hdg_build_quadrature_int_3D
  public :: hdg_model_on_midface_3D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG
  !> in 3D based upon tetrahedral discretization, if we use the
  !> quadrature rule.
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_3D_tet_quadrature(Ainv,vol_phi_phi,         &
                                              vol_phi_phi_q,            &
                                              vol_dphi_phi,             &
                                              sumface_tau,              &
                                              inv_jac,det_jac,ndof_vol, &
                                              ctx_err)

    implicit none

    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    complex(kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_q(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi  (:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi (:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_tau  (:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !! local
    real   (kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: massterm_q
    complex(kind=RKIND_POL)     :: dphii_phij(3)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n

    ctx_err%ierr = 0
    dim_sol =      4 !! pressure + 3D displacement
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    do i=1, ndof_vol
      do j=1, ndof_vol
        ! ---------------------------------------------------------
        !! REFERENCE element, we have:
        !! ---------------------------
        !!   \int_Ke  \nabla \phi_i dKe
        !! = \int_Ke JF^{-T} \nabla \hat{\phi}_i dKe,
        !!   avec \hat{\phi} reference element
        !! = inv_jac(1,1)*dxphi + inv_jac(2,1)*dyphi + inv_jac(3,1)*dzphi !! X-part
        !! + inv_jac(1,2)*dxphi + inv_jac(2,2)*dyphi + inv_jac(3,2)*dzphi !! Y-part
        !! + inv_jac(1,3)*dxphi + inv_jac(2,3)*dyphi + inv_jac(3,3)*dzphi !! Z-part
        ! ---------------------------------------------------------
        dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,1) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,1)
        dphii_phij(2)= vol_dphi_phi(i,j,1)*inv_jac(1,2) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,2) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,2)
        dphii_phij(3)= vol_dphi_phi(i,j,1)*inv_jac(1,3) + &
                       vol_dphi_phi(i,j,2)*inv_jac(2,3) + &
                       vol_dphi_phi(i,j,3)*inv_jac(3,3)
        dphii_phij   = dphii_phij*det_jac
        
        massterm     = dble(vol_phi_phi(i,j) * det_jac)
        
        A(i           , j           ) = dcmplx(-massterm     )
        A(i           , j+3*ndof_vol) = dcmplx(-dphii_phij(1))
        
        A(i+  ndof_vol, j+  ndof_vol) = dcmplx(-massterm     )
        A(i+  ndof_vol, j+3*ndof_vol) = dcmplx(-dphii_phij(2))
        
        A(i+2*ndof_vol, j+2*ndof_vol) = dcmplx(-massterm     )
        A(i+2*ndof_vol, j+3*ndof_vol) = dcmplx(-dphii_phij(3))

        ! second part is composed of one equation.
        ! revert index j and i because it is the first variable that
        ! is derivated here. -------------------------------------------

        dphii_phij(1)= vol_dphi_phi(j,i,1)*inv_jac(1,1) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,1) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,1)
        dphii_phij(2)= vol_dphi_phi(j,i,1)*inv_jac(1,2) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,2) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,2)
        dphii_phij(3)= vol_dphi_phi(j,i,1)*inv_jac(1,3) + &
                       vol_dphi_phi(j,i,2)*inv_jac(2,3) + &
                       vol_dphi_phi(j,i,3)*inv_jac(3,3)
        dphii_phij   = dphii_phij*det_jac

        massterm_q   = dcmplx(vol_phi_phi_q(i,j)*det_jac)

        A(i+3*ndof_vol, j)            = dcmplx(-dphii_phij(1))
        A(i+3*ndof_vol, j+  ndof_vol) = dcmplx(-dphii_phij(2))
        A(i+3*ndof_vol, j+2*ndof_vol) = dcmplx(-dphii_phij(3))
        !! + penalization * (\phi, \phi) over the face
        A(i+3*ndof_vol, j+3*ndof_vol) = dcmplx(-massterm_q - sumface_tau(i,j))
      enddo
    enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)

    return
  end subroutine hdg_build_Ainv_3D_tet_quadrature

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix C for HDG in Helio scalar conjugated
  !> equation in ED based upon triangle discretization
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_C_3D_tet(C,F,F_tau_n,penalization,ndof_vol,      &
                                ndof_face_neigh,ndf_edges_elem,n)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: C(:,:)
    real   (kind=8), allocatable     ,intent(in)   :: F(:,:,:)
    complex(kind=8), allocatable     ,intent(in)   :: F_tau_n(:,:,:)
    real   (kind=8)                  ,intent(in)   :: penalization   (4)
    integer                          ,intent(in)   :: ndof_face_neigh(4)
    integer                          ,intent(in)   :: ndf_edges_elem (5)
    integer                          ,intent(in)   :: ndof_vol
    real(kind=8)                     ,intent(in)   :: n(3,4)
    !!
    integer         :: i, j, i_face

    C=0.0
    !! 4 tetra faces
    do i_face=1,4
      do i=1,ndof_vol ; do j=1,ndof_face_neigh(i_face)

        C(           i,ndf_edges_elem(i_face)+j)= F(i_face,i,j)*n(1,i_face)
        C(  ndof_vol+i,ndf_edges_elem(i_face)+j)= F(i_face,i,j)*n(2,i_face)
        C(2*ndof_vol+i,ndf_edges_elem(i_face)+j)= F(i_face,i,j)*n(3,i_face)
        !! the penalization is only for constant scalar one, otherwise, 
        !! it is set to 1.0 and everything is already contained in the 
        !! F_r array, that is, penalization is only a scaling factor here.
        C(3*ndof_vol+i,ndf_edges_elem(i_face)+j)= penalization(i_face)* &
                                                  F_tau_n(i_face,i,j)
      enddo ; enddo
    enddo

    return
  end subroutine hdg_build_C_3D_tet

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Create the local integral over the current cell to obtain
  !> quantities needed on the local element matrices
  !> for the conjugated helio problem, we need [Pham et al. 2019]
  !> the local element matrices only one term include the 1/r term:
  !> the volumic integral
  !> \int_Ke f(x) \phi_i(x) \phi_j(x)
  !> to work with the similar setup as the Helmholtz equation which
  !> uses 1/kappa, we now consider
  !>      1/kappa is identified with
  !>         freq eta²/c² - q/freq
  !>      using eta for the attenuation part
   !!     and q = alpha²/4 + alpha'/2 + alpha /|x|.
  !>
  !   REMARK: **********************************************************
  !>  ********** Everything is still computed on the reference element
  !>  ********** we just incorporate a possibility for model variation
  !>  ********** that is, the multiplication with det_jac for the local
  !>  ********** element remains in the creation of matrix A and C.
  !   END REMARK: ******************************************************
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[in]    i_o_cell          : cell order for volume matrices
  !> @param[in]    freq              : frequency (for attenuation)
  !> @param[in]    tag_penalization  : format for penalization
  !> @param[out]   vol_phi_phi       : \f$ \int_{Ke } \phi_i(x) \phi_j(x)          \f$
  !> @param[out]   vol_dphi_phi      : \f$ \int_{Ke } d\phi_i(x) \phi_j(x)         \f$
  !> @param[out]   face_phi_xi       : \f$ \int_{DKe}  \phi_i(x) \xi_j(x)          \f$
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_quadrature_int_3D(ctx_dg,ctx_mesh,model,i_cell,  &
                                  i_o_cell,freq,tag_penalization,       &
                                  !! volume matrices
                                  vol_phi_phi,                          &
                                  vol_phi_phi_q,                        &
                                  vol_dphi_phi,                         &
                                  !! face matrices
                                  face_phi_xi,                          &
                                  !! because of the jump condition
                                  face_phi_xi_tau,                      &
                                  !! error context
                                  ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer                            ,intent(in)    :: i_o_cell
    complex(kind=8)                    ,intent(in)    :: freq
    integer                            ,intent(in)    :: tag_penalization
    !! these are the matrices that are computed here
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi  (:,:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_q(:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_dphi_phi (:,:,:)
    !! face matrices
    type(t_array3d_real)   ,allocatable,intent(inout) :: face_phi_xi    (:,:)
    type(t_array3d_complex),allocatable,intent(inout) :: face_phi_xi_tau(:,:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    integer                     :: k, i, j
    integer                     :: n_dof
    !! models
    complex(kind=8),allocatable :: potential(:)
    real   (kind=8)             :: vel, gamm, alpha, dalpha
    real   (kind=8),allocatable :: quad_cp    (:)
    real   (kind=8),allocatable :: quad_alpha (:)
    real   (kind=8),allocatable :: quad_dalpha(:)
    real   (kind=8),allocatable :: quad_gamma (:)
    complex(kind=8)             :: sigma2
    !! integrals
    real   (kind=8)             :: w_phi_phi, w_dphi_phi(3)
    complex(kind=8)             :: intvol_mass(2)
    real   (kind=8)             :: intvol_dphi(3) !! one per dim
    integer                     :: iquad
    !! for face integral
    integer                     :: l,n_dof_l,iface
    !! geometry
    real(kind=8)                :: node_coo(3,4),quad_pt(3),pt(3),xpt(3)
    real(kind=8),allocatable    :: pos_r(:)
    !! coefficients for face integrals, real or complexes
    complex(kind=8),allocatable :: face_coeff_Cx(:,:,:)
    complex(kind=8),allocatable :: face_potential(:),face_k02(:)
    real   (kind=8)             :: area
    real   (kind=8)             :: face_node(3,3),xab,yab,zab,xac,yac,zac
    real   (kind=8)             :: intface_Re(4,1) ! nface, narray
    complex(kind=8)             :: intface_Cx(4,1) ! nface, narray

    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! -----------------------------------------------------------------
    !! evaluate the model parameters in all quadrature points
    !! -----------------------------------------------------------------
    allocate(potential  (ctx_dg%quadGL_npts))
    allocate(quad_cp    (ctx_dg%quadGL_npts))
    allocate(quad_alpha (ctx_dg%quadGL_npts))
    allocate(quad_dalpha(ctx_dg%quadGL_npts))
    allocate(quad_gamma (ctx_dg%quadGL_npts))
    !! -----------------------------------------------------------------
    potential  = 0.d0
    quad_cp    = 0.d0
    quad_alpha = 0.d0
    quad_dalpha= 0.d0
    quad_gamma = 0.d0
    !! -----------------------------------------------------------------
    node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))

    !! -----------------------------------------------------------------
    !!
    !! read model parameters on the quadrature points 
    !! depending on the discretization.
    !!
    !! -----------------------------------------------------------------
    select case(trim(adjustl(model%format_disc)))
      case(tag_MODEL_PCONSTANT)
        !! evaluate the models at this cell with attenuation
        call model_eval_piecewise_constant(model,i_cell,freq,vel,alpha, &
                                           dalpha,gamm,ctx_err)
        quad_cp    (:) =    vel
        quad_alpha (:) =  alpha
        quad_dalpha(:) = dalpha
        quad_gamma (:) =   gamm

      case(tag_MODEL_PPOLY)

        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the reference
          !! element coordinates directly.
          !! ***********************************************************
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))         
          call model_eval_piecewise_polynomial(model,i_cell,xpt,freq,vel,&
                                               alpha,dalpha,gamm,ctx_err)
          quad_cp    (iquad) =    vel
          quad_alpha (iquad) =  alpha
          quad_dalpha(iquad) = dalpha
          quad_gamma (iquad) =   gamm
        end do

      case(tag_MODEL_SEP)
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the local
          !! position
          !! ***********************************************************
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          !! find its global position
          !! we have jac3D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al
          quad_pt(:) = dble  (ctx_dg%quadGL_pts(:,iquad))
          pt     (:) = matmul(dble(ctx_dg%jac(:,:,i_cell)), quad_pt) +  &
                       dble(node_coo(:,1))
          call model_eval_sep(model,pt,freq,vel,alpha,dalpha,gamm,ctx_err)
          quad_cp    (iquad) =    vel
          quad_alpha (iquad) =  alpha
          quad_dalpha(iquad) = dalpha
          quad_gamma (iquad) =   gamm
        end do

      case(tag_MODEL_DOF)
        call model_eval_dof_refelem(model,i_cell,freq,                  &
                                    dble(ctx_dg%quadGL_pts),            &
                                    ctx_dg%quadGL_npts,quad_cp,         &
                                    quad_alpha,quad_dalpha,quad_gamma,  &
                                    ctx_err)

      case default
        ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                        " [build_quadrature] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    if(ctx_err%ierr .ne. 0) return

    !! sanity check in case of attenuation -----------------------------
    if(maxval(abs(quad_gamma)) > tiny(1.d0)) then
      if(abs(aimag(freq)) < tiny(1.d0)) then
        ctx_err%msg   = "** ERROR: the use of zero Fourier " //         &
                        "frequency is not compatible with current"//    &
                        " models of attenuation [build_quadrature] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
      end if    
    end if
    !! -----------------------------------------------------------------

    !! ---------------------------------------------------------------
    !! compute the global position of all of the quadrature points
    !! -----------------------------------------------------------------
    allocate(pos_r(ctx_dg%quadGL_npts))
    pos_r = 0.d0    
    do iquad=1,ctx_dg%quadGL_npts
      !! we have jac3D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al
      quad_pt(:) = dble  (ctx_dg%quadGL_pts(:,iquad))
      xpt    (:) = matmul(dble(ctx_dg%jac(:,:,i_cell)), quad_pt) +  &
                   dble(node_coo(:,1))
      pos_r  (iquad) = norm2(xpt)
    end do

    !! consistency check: r <= 0 is not ok for axisym.
    if(minval(pos_r) < tiny(1.d0)) then
      ctx_err%msg   = "** ERROR: spherical position r cannot "       // &
                      "be lower than or equal to zero [build_quadrature] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return !! non-shared error
    end if
    !! -----------------------------------------------------------------
    !! compute the potential -------------------------------------------
    do iquad=1,ctx_dg%quadGL_npts
      !! because of the i*i = -1
      sigma2 = -(freq**2)                                               &
               *(1.d0 + 2.d0*dcmplx(0.d0,1.d0)*quad_gamma(iquad)/aimag(freq))
      potential(iquad) = sigma2/(quad_cp(iquad)**2)                     &
                       - quad_alpha (iquad)**2/4.d0                     &
                       - quad_dalpha(iquad)   /2.d0                     &
                       - quad_alpha (iquad)   / pos_r(iquad)
    end do

    !! -----------------------------------------------------------------
    !! Create local matrices, only the face matrix must adapted to
    !! the different order because of neighbors, otherwize, we
    !! actually only need the current cell order to deal with.
    !! -----------------------------------------------------------------
    k       = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)
    vol_phi_phi     = 0.d0
    vol_phi_phi_q   = 0.d0
    vol_dphi_phi    = 0.d0
    
    ! ----------------------------------------------------------------
    !!  VOLUME MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ----------------------------------------------------------------
    do i=1, n_dof ; do j=1, n_dof

      intvol_mass(:) = 0.d0
      intvol_dphi(:) = 0.d0

      do iquad=1,ctx_dg%quadGL_npts
        !! REMARK: **********************************************************
        !! ********** Everything is still computed on the reference element
        !! ********** we just incorporate a possibility for model variation
        !! ********** that is, the multiplication with det_jac for the local
        !! ********** element remains in the creation of matrix A and C.
        !! END REMARK: ******************************************************

        !! for the volumic integrals
        !! the precomputation already has the weight.
        w_phi_phi     = ctx_dg%quadGL_phi_phi_w(k)%array (i,j,iquad)
        w_dphi_phi(1) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,1)
        w_dphi_phi(2) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,2)
        w_dphi_phi(3) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,3)
        
        intvol_mass(1) = intvol_mass(1) + w_phi_phi
        intvol_mass(2) = intvol_mass(2) + w_phi_phi*potential(iquad)
        !!  derivatives
        intvol_dphi(:)    = intvol_dphi(:) + w_dphi_phi(:)
      end do
      
      !!     vol_matrix
      vol_phi_phi    (i,j) =  real(intvol_mass(1),kind=RKIND_POL)
      vol_phi_phi_q  (i,j) = cmplx(intvol_mass(2),kind=RKIND_POL)
      vol_dphi_phi (i,j,:) =  real(intvol_dphi   ,kind=RKIND_POL)
    end do ; end do

    !! -----------------------------------------------------------------
    !!  SURFACE MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !!  We only need to sum the pre-computed array and the
    !!  passage towards the local element is made, as usual, in the
    !!  creations of matrix A & B & L, etc.
    !! -----------------------------------------------------------------

    !! compute the penalization coeff on face once and for all.
    !! by default, there is \f$ \int r \, \phi \, \tau \f$.
    !!   0 ==> constant penalization => \f$ \int r  \, \phi \f$
    !!   1 ==> r                     => \f$ \int r² \, \phi \f$
    !!   2 ==> 1/r                   => \f$ \int       \phi \f$
    allocate(face_coeff_Cx(4,ctx_dg%quadGL_face_npts,1)) !! 4 tetra face
    face_coeff_Cx = 0.d0    
    !! models along the face
    allocate(face_potential(ctx_dg%quadGL_face_npts))
    allocate(face_k02      (ctx_dg%quadGL_face_npts))
    !! loop over all faces to evaluate the beta_sigma and A first
    do iface=1,ctx_mesh%n_neigh_per_cell !! four tetra faces
      face_potential = 0.d0
      call hdg_model_on_face_3D_quad(ctx_dg,ctx_mesh,model,i_cell,      &
                                     iface,freq,face_potential,face_k02,&
                                     ctx_err)
                                          
      !! compute the weights depending on the penalization chosen.
      select case(tag_penalization)
        !! -------------------------------------------------------------
        !! default.
        !! -------------------------------------------------------------
        case(0)
          face_coeff_Cx(iface,:,1) = 1.d0
        
        !! -------------------------------------------------------------
        !! using -i \sqrt{V}
        !! -------------------------------------------------------------
        case(10)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = &
              - dcmplx(0.d0,1.d0)*upper_half_sqrt(face_potential(iquad))
          end do
        !! -------------------------------------------------------------
        !! using i \sqrt{V}
        !! -------------------------------------------------------------
        case(11)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = &
               dcmplx(0.d0,1.d0)*upper_half_sqrt(face_potential(iquad))
          end do
        !! -------------------------------------------------------------
        !! using -\sqrt{V}
        !! -------------------------------------------------------------
        case(12)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = -upper_half_sqrt(face_potential(iquad))
          end do
        !! -------------------------------------------------------------
        !! using \sqrt{V}
        !! -------------------------------------------------------------
        case(13)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = upper_half_sqrt(face_potential(iquad))
          end do

        !! -------------------------------------------------------------
        !! using -i k
        !! -------------------------------------------------------------
        case(20)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = &
              - dcmplx(0.d0,1.d0)*upper_half_sqrt(face_k02(iquad))
          end do
        !! -------------------------------------------------------------
        !! using i k
        !! -------------------------------------------------------------
        case(21)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = &
               dcmplx(0.d0,1.d0)*upper_half_sqrt(face_k02(iquad))
          end do
        !! -------------------------------------------------------------
        !! using -k
        !! -------------------------------------------------------------
        case(22)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = -upper_half_sqrt(face_k02(iquad))
          end do
        !! -------------------------------------------------------------
        !! using k
        !! -------------------------------------------------------------
        case(23)
          do iquad=1,ctx_dg%quadGL_face_npts
            face_coeff_Cx(iface,iquad,1) = upper_half_sqrt(face_k02(iquad))
          end do

        !! -------------------------------------------------------------
        !! using 1/area
        !! -------------------------------------------------------------
        case(50)
          call mesh_area_simplex(ctx_mesh%dim_domain,node_coo,area,ctx_err)
          face_coeff_Cx(iface,:,1) = 1.d0/area

        !! -------------------------------------------------------------
        !! using 1/area of the face
        !! -------------------------------------------------------------
        case(51)
          !! ***********************************************************
          !!
          !! To be verified
          !!
          !!
          !! ***********************************************************
          face_node(:,1) = ctx_mesh%x_node(:,ctx_mesh%cell_node(         &
                           interface_3D_tetrahedron(1,iface),i_cell))
          face_node(:,2) = ctx_mesh%x_node(:,ctx_mesh%cell_node(         &
                           interface_3D_tetrahedron(2,iface),i_cell))
          face_node(:,3) = ctx_mesh%x_node(:,ctx_mesh%cell_node(         &
                           interface_3D_tetrahedron(3,iface),i_cell))
          xab = face_node(1,2) - face_node(1,1)
          yab = face_node(2,2) - face_node(2,1)
          zab = face_node(3,2) - face_node(3,1)
          xac = face_node(1,3) - face_node(1,1)
          yac = face_node(2,3) - face_node(2,1)
          zac = face_node(3,3) - face_node(3,1)
          area=1.d0/2.d0*dsqrt( (yab*zac - zab*yac)**2 &
                               +(zab*xac - xab*zac)**2 &
                               +(xab*yac - yab*xac)**2)
          face_coeff_Cx(iface,:,1) = 1.d0/area
          !! need to be checked.
          !! ***********************************************************

        case default !! unknown
          ctx_err%msg   = "** ERROR:unrecognized penalization format "//  &
                          " [build_quadrature] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !! non-shared error
        end select
    end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! we need the second loop over the different orders ...............
    !! -----------------------------------------------------------------
    do k = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order(k)

      do l = 1, ctx_dg%n_different_order
        n_dof_l = ctx_dg%n_dof_per_order (l)

        face_phi_xi    (k,l)%array = 0.d0
        face_phi_xi_tau(k,l)%array = 0.d0
        do i=1, n_dof ; do j=1, n_dof_l
          intface_Re = 0.d0
          intface_Cx = 0.d0

          do iquad=1,ctx_dg%quadGL_face_npts
            !! weight is already taken into account, all faces at once here
            do iface=1,4 !! four faces
              !! raw
              intface_Re(iface,1) = intface_Re(iface,1) +               &
                   ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,iface)
              !! with coeffs
              intface_Cx(iface,1) = intface_Cx(iface,1) +               &
                   face_coeff_Cx(iface,iquad,1)         *               &
                   ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,iface)
            end do
          end do
          !! update all faces.
          face_phi_xi    (k,l)%array(i,j,:)=real (intface_Re(:,1),kind=RKIND_POL)
          face_phi_xi_tau(k,l)%array(i,j,:)=cmplx(intface_Cx(:,1),kind=RKIND_POL)
        end do ; end do

      end do !! end second loop on the different orders.
    end do !! end first loop on the different orders.

    deallocate(quad_cp      )
    deallocate(quad_alpha   )
    deallocate(quad_dalpha  )
    deallocate(quad_gamma   )
    deallocate(potential    )
    deallocate(pos_r        )
    deallocate(face_coeff_Cx)
    deallocate(face_potential)
    deallocate(face_k02)

    return
  end subroutine hdg_build_quadrature_int_3D


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Estimates models on the element face, this is used for the
  !> boundary conditions.
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_model_on_face_3D_quad(ctx_dg,ctx_mesh,model,i_cell,    &
                                       iface,freq,potential,k0squared,  &
                                       ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer(kind=4)                    ,intent(in)    :: iface
    complex(kind=8)                    ,intent(in)    :: freq
    complex(kind=8)                    ,intent(out)   :: potential(:)
    complex(kind=8)                    ,intent(out)   :: k0squared(:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    real   (kind=8)             :: gamm,vel,alpha,dalpha,r
    complex(kind=8)             :: sigma2
    real   (kind=8), allocatable:: quad_3dposition_gb (:,:)
    real   (kind=8), allocatable:: quad_3dposition_loc(:,:)
    integer                     :: iquad

    ctx_err%ierr=0

    !! =================================================================
    !! loop over all quad face points
    !! =================================================================
    potential = 0.d0
    k0squared = 0.d0
    
    !! convert the face quadrature positions to the 2D global mesh
    allocate(quad_3dposition_gb (3,ctx_dg%quadGL_face_npts))
    allocate(quad_3dposition_loc(3,ctx_dg%quadGL_face_npts))
    quad_3dposition_gb =0.d0
    quad_3dposition_loc=0.d0
    call mesh_convert_face_position_3D(                                 &
                 !! coordinates of the nodes of the tetra
                 dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
                 !! face index and inverse jacobian
                 iface,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
                 !! all the positions to treat
                 dble(ctx_dg%quadGL_face_pts(:,:)),                     &
                 ctx_dg%quadGL_face_npts,                               &
                 !! global and local positions.
                 quad_3dposition_gb,quad_3dposition_loc,ctx_err)

    do iquad=1,ctx_dg%quadGL_face_npts
      !! now that we have the position, we get the models value.
      select case(trim(adjustl(model%format_disc)))
        case(tag_MODEL_PCONSTANT)
          !! gives the same values as inthe cell
          call model_eval_piecewise_constant(model,i_cell,freq,vel,alpha, &
                                             dalpha,gamm,ctx_err)
        case(tag_MODEL_PPOLY)
          !! *****************************************************
          !! evaluate the models at this point, it uses the 
          !! reference element coordinates directly.
          !! *****************************************************
          call model_eval_piecewise_polynomial(model,i_cell,            &
                          quad_3dposition_loc(:,iquad),freq,vel,        &
                          alpha,dalpha,gamm,ctx_err)
        case(tag_MODEL_SEP)
          !! xhat is the global position.
          call model_eval_sep(model,quad_3dposition_gb(:,iquad),        &
                              freq,vel,alpha,dalpha,gamm,ctx_err)
      
        case(tag_MODEL_DOF)
          !! *****************************************************
          !! evaluate the models at this point, it uses the 
          !! reference element coordinates directly.
          !! *****************************************************
          call model_eval_dof_refelem(model,i_cell,freq,                &
                                      quad_3dposition_loc(:,iquad),vel, &
                                      alpha,dalpha,gamm,ctx_err)
                                                
        case default
          ctx_err%msg   = "** ERROR: unrecognized model " //       &
                          "parameterization [model_on_face_quad] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !! non-shared error
      end select

      !! evaluate potentials here: 
      r      = norm2(quad_3dposition_gb(:,iquad))
      sigma2 = -(freq**2) * (1.d0 + 2.d0*dcmplx(0.d0,1.d0)*gamm/aimag(freq))
      potential(iquad) = sigma2/(vel**2) - alpha**2/4.d0 - dalpha/2.d0 - alpha/r
      k0squared(iquad) = sigma2/(vel**2) 
    end do
    deallocate(quad_3dposition_loc)
    deallocate(quad_3dposition_gb )
    return

  end subroutine hdg_model_on_face_3D_quad

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Estimates models on the middle of the element face, 
  !> this is used for the boundary conditions.
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_model_on_midface_3D(ctx_dg,ctx_mesh,model,i_cell,iface,&
                                     freq,k2,kinf2,alpha,ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer(kind=4)                    ,intent(in)    :: iface
    complex(kind=8)                    ,intent(in)    :: freq
    complex(kind=8)                    ,intent(out)   :: k2
    complex(kind=8)                    ,intent(out)   :: kinf2
    real   (kind=8)                    ,intent(out)   :: alpha
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    real   (kind=8)             :: gamm,vel,dalpha,r
    complex(kind=8)             :: sigma2
    real(kind=8)                :: xpos_face(2)
    real(kind=8)                :: quad_3dposition_gb (3)
    real(kind=8)                :: quad_3dposition_loc(3)

    ctx_err%ierr=0

    !! =================================================================
    k2      = 0.d0
    kinf2   = 0.d0
    alpha   = 0.d0

    !! it corresponds to barycenter of the 2D face
    xpos_face(1) = 1.d0/3.d0
    xpos_face(2) = 1.d0/3.d0
    quad_3dposition_gb =0.d0
    quad_3dposition_loc=0.d0
    call mesh_convert_face_position_3D(                                 &
                 !! coordinates of the nodes of the tetra
                 dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
                 !! face index and inverse jacobian
                 iface,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
                 !! position to treat
                 xpos_face,                                             &
                 !! global and local positions.
                 quad_3dposition_gb,quad_3dposition_loc,ctx_err)
                 
    !! evaluate models
    !! now that we have the position, we get the models value.
    select case(trim(adjustl(model%format_disc)))
      case(tag_MODEL_PCONSTANT)
        !! gives the same values as inthe cell
        call model_eval_piecewise_constant(model,i_cell,freq,vel,alpha, &
                                           dalpha,gamm,ctx_err)
      case(tag_MODEL_PPOLY)
        !! *****************************************************
        !! evaluate the models at this point, it uses the 
        !! reference element coordinates directly.
        !! *****************************************************
        call model_eval_piecewise_polynomial(model,i_cell,              &
                        quad_3dposition_loc,freq,vel,alpha,dalpha,gamm, &
                        ctx_err)
      case(tag_MODEL_SEP)
        !! xhat is the global position.
        call model_eval_sep(model,quad_3dposition_gb,                   &
                            freq,vel,alpha,dalpha,gamm,ctx_err)
    
      case(tag_MODEL_DOF)
        !! *****************************************************
        !! evaluate the models at this point, it uses the 
        !! reference element coordinates directly.
        !! *****************************************************
        call model_eval_dof_refelem(model,i_cell,freq,                  &
                                    quad_3dposition_loc,vel,            &
                                    alpha,dalpha,gamm,ctx_err)
                                              
      case default
        ctx_err%msg   = "** ERROR: unrecognized model " //              &
                        "parameterization [model_on_midface] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    r      = norm2(quad_3dposition_gb)
    sigma2 = -(freq**2) * (1.d0 + 2.d0*dcmplx(0.d0,1.d0)*gamm/aimag(freq))
    k2    = sigma2/(vel**2) - alpha**2/4.d0 - dalpha/2.d0 - alpha/r
    kinf2 = sigma2/(vel**2)

    return

  end subroutine hdg_model_on_midface_3D

end module m_matrix_utils
