!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_matrix_quadrature.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for Galbrun's 
!> equation without rotation under axisymmetry, using the 
!> HDG discretization.
!>
!
!------------------------------------------------------------------------------
module m_create_matrix_quadrature

  !! module used -------------------------------------------------------
  use omp_lib
  use, intrinsic                   :: ieee_arithmetic
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MAT, IKIND_MESH, RKIND_MAT, &
                                      RKIND_POL
  use m_ctx_parallelism,        only: t_parallelism
  !> inverse matrix with Lapack
  use m_lapack_inverse,         only : lapack_matrix_inverse
  !> matrix and cartesian domain types
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_tag_bc,                 only: tag_freesurface, tag_pml,         &
                                      tag_wallsurface, tag_absorbing,   &
                                      tag_ghost_cell,                   &
                                      tag_dirichlet,tag_neumann,tag_planewave
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_piecewise_constant
  use m_matrix_utils,           only: hdg_build_C_3D_tet,               &
                                      hdg_build_quadrature_int_3D,      &
                                      hdg_build_Ainv_3D_tet_quadrature, &
                                      hdg_model_on_midface_3D
  use m_array_types,            only: t_array2d_real,t_array3d_real,    &
                                      t_array2d_complex,array_allocate, &
                                      array_deallocate,t_array3d_complex
  use m_mesh_simplex_interface, only: interface_3D_tetrahedron

  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: create_matrix_quadrature_3D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix for Galbun's problem using HDG in 3D
  !
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[in]    model          : model parameters
  !> @param[inout] ctx_dg         : type DG
  !> @param[out]   dim_nnz_loc    : local nonzeros
  !> @param[inout] ilin(:)        : matrix I
  !> @param[inout] icol(:)        : matrix J
  !> @param[inout] Aval(:)        : matrix values
  !> @param[in]    frequency      : frequency
  !> @param[in]    flag_inv       : indicates inversion
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_quadrature_3D(ctx_mesh,model,ctx_dg,         &
                                         dim_nnz_loc,Alin,Acol,Aval,    &
                                         freq,flag_inv,ctx_err)
    
    implicit none

    type(t_domain)                     ,intent(in)      :: ctx_mesh
    type(t_model)                      ,intent(in)      :: model
    type(t_discretization)             ,intent(inout)   :: ctx_dg
    integer(kind=8)                    ,intent(out)     :: dim_nnz_loc
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Alin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Acol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout)   :: Aval(:)
    complex(kind=8)                    ,intent(in)      :: freq
    logical                            ,intent(in)      :: flag_inv
    type(t_error)                      ,intent(inout)   :: ctx_err
    !! local
    integer(kind=IKIND_MAT)             :: nnz_exact
    real   (kind=8)                     :: tol
    !! hdg
    complex(kind=8), allocatable :: coeff(:,:)
    complex(kind=8), allocatable :: hdgB(:,:)
    complex(kind=8), allocatable :: hdgC(:,:)
    complex(kind=8), allocatable :: hdgL(:,:)
    complex(kind=8), allocatable :: sumface_phii_phij_tau(:,:)
    real   (kind=8), allocatable :: face_phii_xij(:,:,:)
    complex(kind=8), allocatable :: face_phii_xij_tau(:,:,:)
    complex(kind=8), allocatable :: hdgAinv(:,:)
    complex(kind=8)              :: temp
    complex(kind=8)              :: Zval,k2,kinf2
    real   (kind=8)              :: alpha,rpos
    !! local for quadrature rules
    type(t_array2d_real)   ,allocatable :: vol_phi_phi(:)
    type(t_array2d_complex),allocatable :: vol_phi_phi_q(:)
    type(t_array3d_real)   ,allocatable :: vol_dphi_phi(:)
    type(t_array3d_real)   ,allocatable :: face_phi_phi(:,:)
    type(t_array3d_complex),allocatable :: face_phi_phi_tau(:,:)
    integer(kind=IKIND_MESH) :: i_cell,face,neigh,index_face
    integer(kind=IKIND_MAT)  :: iline, icol
    integer        :: order,i_orient,dim_var
    integer        :: i_order_cell,ndof_vol,ndof_max_vol,ndof_vol_l
    integer        :: ndof_max_face,dim_sol,ndof_face_tot,ndof_face,n
    integer        :: i,j,index_face_dofi,index_face_dofj
    integer        :: i_o_neigh,ipos,jpos,iface,jface,tag_penalization
    real   (kind=8):: penalization(4)    !! 3D tetra, 4 faces
    integer        :: ndf_edges_elem (5) !! 3D tetra, 4 faces + 1 to sum
    integer        :: i_order_neigh  (4) !! 3D tetra, 4 faces
    integer        :: ndof_face_neigh(4) !! 3D tetra, 4 faces
    integer        :: ndof_vol_neigh (4) !! 3D tetra, 4 faces
    !! coordinates for spherical position 
    real(kind=8)   :: xnode1(3),xnode2(3),xnode3(3)
    !! for symmetrization of the matrix even with Dirichlet boundary.
    integer        :: ind_sym
    logical        :: flag_info_sym(4)   !! 3D tetra, 4 faces
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! init informations on the matrix and hdg infos
    !! -----------------------------------------------------------------
    Alin = 0  ; Acol=0    ; Aval= 0.0

    nnz_exact = 0
    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif
    !! compute max order dof
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    dim_var       = ctx_dg%dim_var
    !! make sure it is zero at start as it is updated.
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(index_face,iline,icol),       &
    !$OMP&         PRIVATE(hdgB,hdgC,hdgL,sumface_phii_phij_tau),        &
    !$OMP&         PRIVATE(face_phii_xij,face_phii_xij_tau),             &
    !$OMP&         PRIVATE(coeff,penalization,Zval,hdgAinv,temp),        &
    !$OMP&         PRIVATE(vol_phi_phi,vol_phi_phi_q),                   &
    !$OMP&         PRIVATE(vol_dphi_phi,face_phi_phi),                   &
    !$OMP&         PRIVATE(face_phi_phi_tau,i_cell,face,neigh),          &
    !$OMP&         PRIVATE(ndf_edges_elem,order,i_orient,i_order_cell),  &
    !$OMP&         PRIVATE(ndof_vol,ndof_vol_l,ndof_face_tot,ndof_face), &
    !$OMP&         PRIVATE(n,i,j,index_face_dofi,index_face_dofj),       &
    !$OMP&         PRIVATE(i_o_neigh,ipos,jpos,iface,jface),             &
    !$OMP&         PRIVATE(tag_penalization,i_order_neigh),              &
    !$OMP&         PRIVATE(ndof_face_neigh,ndof_vol_neigh,alpha,rpos),   &
    !$OMP&         PRIVATE(xnode1,xnode2,ind_sym,flag_info_sym,k2,kinf2)
    !! local array, the 4 is because of the 4 tetra faces
    allocate(coeff  (4*dim_var*ndof_max_face,4*dim_var*ndof_max_face))
    allocate(hdgL   (4*dim_var*ndof_max_face,4*dim_var*ndof_max_face))
    allocate(hdgB   (4*dim_var*ndof_max_face,  dim_sol*ndof_max_vol ))
    allocate(hdgC   (  dim_sol*ndof_max_vol ,4*dim_var*ndof_max_face))
    allocate(hdgAinv(  dim_sol*ndof_max_vol ,  dim_sol*ndof_max_vol ))
    !! for integrations of basis functions
    allocate(face_phii_xij          (4,ndof_max_vol,ndof_max_face))
    allocate(face_phii_xij_tau(4,ndof_max_vol,ndof_max_face))
    allocate(sumface_phii_phij_tau(ndof_max_vol,ndof_max_vol ))
    !! for quadrature, once and for all
    allocate(vol_phi_phi_q(ctx_dg%n_different_order))
    allocate(vol_phi_phi  (ctx_dg%n_different_order))
    allocate(vol_dphi_phi (ctx_dg%n_different_order))
    allocate(face_phi_phi (ctx_dg%n_different_order,ctx_dg%n_different_order))
    allocate(face_phi_phi_tau(ctx_dg%n_different_order,ctx_dg%n_different_order))
    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! size depends if derivative or not
      call array_allocate(vol_phi_phi_q(i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi  (i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_dphi_phi (i),ndof_vol,ndof_vol,           &
                          ctx_dg%dim_domain,ctx_err)
      do j = 1, ctx_dg%n_different_order
        ndof_vol_l = ctx_dg%n_dof_per_order(j)
        call array_allocate(face_phi_phi(i,j),ndof_vol,ndof_vol_l,      &
                             ctx_mesh%n_neigh_per_cell,ctx_err)
        call array_allocate(face_phi_phi_tau(i,j),ndof_vol,ndof_vol_l,  &
                             ctx_mesh%n_neigh_per_cell,ctx_err)
      end do
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do i_cell=1,ctx_mesh%n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      
      ctx_dg%hdg%Ainv(i_cell)%array = 0.d0
      ctx_dg%hdg%Q   (i_cell)%array = 0.d0
      ctx_dg%hdg%X   (i_cell)%array = 0.d0
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array = 0.d0
        ctx_dg%hdg%D(i_cell)%array = 0.d0
      endif

      coeff=0.0; hdgAinv=0.0; hdgB=0.0; hdgC=0.0; hdgL=0.0
      face_phii_xij               = 0.0
      sumface_phii_phij_tau = 0.0
      face_phii_xij_tau     = 0.0
      flag_info_sym               = .false.
      ! --------------------------------------------------------->
      !! initialization of edges informations and model parameters
      i_order_cell = ctx_dg%order_to_index      (ctx_mesh%order(i_cell))
      ndof_vol     = ctx_dg%n_dof_per_order     (i_order_cell)
      ndof_face    = ctx_dg%n_dof_face_per_order(i_order_cell)

      if(ctx_err%ierr .ne. 0) cycle

      !!  format of penalization ---------------------------------------
      penalization = 1.d0
      if(ctx_dg%penalization < 0) then
      !!tag_penalization = 0   !! using constant 1.0        
      !!penalization(:)  = 1.d0
        tag_penalization = int(-ctx_dg%penalization)
        penalization    =  1.d0
        if(tag_penalization < 0) tag_penalization = 0 !! default.
      else
        penalization    = ctx_dg%penalization
        !! avoid 0.0
        if(maxval(abs(penalization)) < tiny(1.0)) penalization = 1.d0 
        tag_penalization= 0
      end if
      ! ----------------------------------------------------------------
      call hdg_build_quadrature_int_3D(ctx_dg,ctx_mesh,model,i_cell,    &
                     i_order_cell,freq,tag_penalization,                &
                     !! volume matrices
                     vol_phi_phi(i_order_cell)%array,                   &
                     vol_phi_phi_q(i_order_cell)%array,                 &
                     vol_dphi_phi(i_order_cell)%array,                  &
                     !! face matrices
                     face_phi_phi,face_phi_phi_tau,                     &
                     ctx_err)
      if(ctx_err%ierr .ne. 0) cycle

      !! ---------------------------------------------------------------
      !! initialize face informations with neighbors
      !! ndf_edges_elem: for each face we store the incremental ndof
      !! ndf_edges_elem(5) = total number of dof on the edges of the element
      !! ---------------------------------------------------------------
      ndf_edges_elem(1)=0
      do iface=1,ctx_mesh%n_neigh_per_cell
        face                    = ctx_mesh%cell_face   (iface,i_cell)
        order                   = ctx_mesh%order_face  (face)
        i_order_neigh  (iface)  = ctx_dg%order_to_index(order)
        ndof_face_neigh(iface)  = ctx_dg%n_dof_face_per_order(i_order_neigh(iface))
        ndof_vol_neigh (iface)  = ctx_dg%n_dof_per_order     (i_order_neigh(iface))
        ndf_edges_elem (iface+1)=ndf_edges_elem(iface) + ndof_face_neigh(iface)
      end do
      ndof_face_tot = ndf_edges_elem(5)
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Computation of some utility matrices for the faces: we need
      !!
      !! (o) \sum_face penalization(face) (\phi_i, \phi_j)_face 
      !!                   => sumface_phii_phij(i,j), needed for A
      !! (o) (\phii, \xij) over the face for each face 
      !!                   => face_phii_xij(iface,i,j), needed for C
      !! (o) matrix L, see below, it is then adjusted if boundary cond.
      !! (o) matrix B, see below
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        !! local index of the face, for the pml coefficient ------------
        index_face  = ctx_mesh%cell_face(iface,i_cell)

        do i=1, ndof_face_neigh(iface)
          index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)

          !! L is block-diagonal with ndof_face x ndof_face blocks and
          !! is defined by - \tau (\Xi, \Xi) over the face
          !! -----------------------------------------------------------
          do j=1, ndof_face_neigh(iface)
            index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)

            !! using the one with penalization
            temp = dcmplx(face_phi_phi_tau(i_o_neigh,i_o_neigh)%  &
                          array(index_face_dofi,index_face_dofj,iface)* &
                          ctx_dg%det_jac_face(iface,i_cell))
            !! still multiplies in case of constant not set to 1.0
            hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j) =     &
                 !! only for constant penalization, otherwise everything
                 !! is already in the face_phi_phi_tau used above ......
                                                -penalization(iface)*temp
          enddo

          !! B is has one block per face, each block is of size 
          !! Ndof_face x Ndof_vol and the content is given by 
          !! (\Xi, \Phi) over the face
          !! -----------------------------------------------------------
          do j=1,ndof_vol
            !! careful that i is the face and j the vol ................
           
            !! using with penalization
            temp = dcmplx(face_phi_phi(i_order_cell,i_o_neigh)%         &
                          array(j,index_face_dofi,iface) *              &
                   ctx_dg%det_jac_face(iface,i_cell))
            
            face_phii_xij(iface,j,i) = real(temp) !! must be real here
            hdgB(ndf_edges_elem(iface)+i,           j)=temp*ctx_dg%normal(1,iface,i_cell)
            hdgB(ndf_edges_elem(iface)+i,  ndof_vol+j)=temp*ctx_dg%normal(2,iface,i_cell)
            hdgB(ndf_edges_elem(iface)+i,2*ndof_vol+j)=temp*ctx_dg%normal(3,iface,i_cell)
            
            !! using with penalization now.
            temp = dcmplx(face_phi_phi_tau(i_order_cell,i_o_neigh)% &
                          array(j,index_face_dofi,iface) *                &
                          ctx_dg%det_jac_face(iface,i_cell))
            !! it should be real valued.
            face_phii_xij_tau(iface,j,i) = temp
            !! still multiplies in case of constant not set to 1.0
            hdgB(ndf_edges_elem(iface)+i,3*ndof_vol+j) = temp*          &
                 !! only for constant penalization, otherwise everything
                 !! is already in the face_phi_phi_tau used above ......
                                                         penalization(iface)
          end do
        end do

        !! -------------------------------------------------------------
        !! Here we compute the term 
        !! \sum_face penalization(face) * (\phi_i, \phi_j)_face
        !! -------------------------------------------------------------
        !! we can use ndof_face because it is the discretization of 
        !! the volume quantity p, which is defined by the cell order
        do i = 1, ndof_face
          index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
          do j = 1, ndof_face
            
            index_face_dofj = ctx_dg%face_map(i_order_cell)%array(j,iface)
            !! using with penalization
            temp = ctx_dg%det_jac_face(iface,i_cell) *                  &
                   dcmplx(face_phi_phi_tau(i_order_cell,i_order_cell)%  &
                          array(index_face_dofi,index_face_dofj,iface))
            !! still multiplies in case of constant not set to 1.0
            sumface_phii_phij_tau(index_face_dofi,index_face_dofj) =    &
            sumface_phii_phij_tau(index_face_dofi,index_face_dofj)      &
                 + temp * &
                 !! only for constant penalization, otherwise everything
                 !! is already in the face_phi_phi_taueta used above ....
                                                      penalization(iface)
 
          end do
        end do

      end do

      ! ----------------------------------------------------------------
      !! adjust B in case we have an actual boundary
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        neigh     = ctx_mesh%cell_neigh(iface,i_cell)
        
        select case (neigh)
          !! absorbing boundary conditions given by
          !! \partial_n w = (-1/r + Z) w
          !! replacing \partial_n w with first-order equation gives,
          !!   freq  v.n = (-1/r + Z) w
          !!   v.n  - (-1/r + Z)/freq w = 0
          case(tag_absorbing)

            !! we need the wavenumber, and the spherical position, 
            !! approximated using the mid point between the two nodes. 
            !! 
            xnode1(:) = ctx_mesh%x_node(:,ctx_mesh%cell_node(           &
                      interface_3D_tetrahedron(1,iface),i_cell))
            xnode2(:) = ctx_mesh%x_node(:,ctx_mesh%cell_node(           &
                      interface_3D_tetrahedron(2,iface),i_cell))
            xnode3(:) = ctx_mesh%x_node(:,ctx_mesh%cell_node(           &
                      interface_3D_tetrahedron(3,iface),i_cell))
            rpos  = norm2( 1.d0/3.d0*(xnode2+xnode1+xnode3) )
            !! ---------------------------------------------------------
            if(rpos < tiny(1.0)) then
              ctx_err%msg   = "** ERROR: Robin Boundary Condition not"//&
                              " supported in r=0 [create_matrix]"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              cycle
            end if
            !! ---------------------------------------------------------
            !! compute the wavenumers, k and kinf
            !! evaluate model on face
            call hdg_model_on_midface_3D(ctx_dg,ctx_mesh,model,i_cell,  &
                                      iface,freq,k2,kinf2,alpha,ctx_err)

            !! only those that do not depend on l are possible .........
            select case(trim(adjustl(ctx_mesh%Zimpedance)))

              !! l-dependent are not possible...........................
              case('Z_DtN'     ,'ZDtN'     , 'ZS_HF_1b'  ,'ZSHF1b'   , &
                   'ZS_SAI_1'  ,'ZSSAI1'   , 'ZA_HF_1'   ,'ZAHF1'    , &
                   'Z_nonlocal','Znonlocal')
                ctx_err%msg   = "** ERROR: Cannot use a mode-dependant"//&
                                " boundary conditions with the "       //&
                                "conjugated problem in 2D [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
              !! list of BC can be found in the Research Report, or the
              !! ESAIM article.
              case('ZS_HF_0'   ,'ZSHF0'    )
                Zval = dcmplx(0.d0,1.d0)*sqrt(k2)
                !! adjust Solar RBC
                Zval = Zval - 1.d0/rpos
              case('ZS_HF_1a'  ,'ZSHF1a'   )
                Zval = dcmplx(0.d0,1.d0)*sqrt(k2) - dcmplx(0.d0,1.d0)/(2.d0*sqrt(k2)) & 
                     *(1./(rpos) * (alpha))
                !! adjust Solar RBC
                Zval = Zval - 1.d0/rpos
              case('ZS_SAI_0'  ,'ZSSAI0'   )
                Zval = dcmplx(0.d0,1.d0)*sqrt(k2) * sqrt(1.d0 - alpha/rpos*1.d0/(k2))
                !! adjust Solar RBC
                Zval = Zval - 1.d0/rpos
              case('ZA_HF_0'   ,'ZAHF0'    )
                Zval = dcmplx(0.d0,1.d0)*sqrt(kinf2)
                !! adjust Solar RBC
                Zval = Zval - 1.d0/rpos
              case('Z_naive'   ,'Znaive'   )
                Zval = 1.d0/rpos + alpha/2.  + dcmplx(0.d0,1.d0)*sqrt(kinf2)
                !! adjust Solar RBC
                Zval = Zval - 1.d0/rpos
              case('ZA_RBC_1'  ,'ZARBC1'   )
                Zval = 1.d0/rpos + dcmplx(0.d0,1.d0) * sqrt(k2)
                !! adjust Solar RBC
                Zval = Zval - 1.d0/rpos
              case default
                ctx_err%msg   = "** ERROR: Unrecognized Impedance "  // &
                                "condition "//                          &
                                trim(adjustl(ctx_mesh%Zimpedance))//    &
                                " for Robin Boundary Condition [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
            end select
            !! --------------------------------------------------------- 
            do i=1, ndof_face
              index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
              do j=1,ndof_face
                index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
                temp = dcmplx(face_phi_phi(i_order_cell,i_order_cell)%     &
                              array(index_face_dofi,index_face_dofj,iface)*&
                              ctx_dg%det_jac_face(iface,i_cell))
            
                !! update L with - Z (\xi,\xi)
                hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)=&
                hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)-&
                                                  temp*Zval
              end do
            end do

          !! other types
          case(tag_freesurface,tag_ghost_cell,tag_wallsurface,          &
               tag_neumann,tag_dirichlet)
            !! TAG_GHOST ==> nothing special

            !! TAG_WALL = TAG_NEUMANN
            !!    ==> \partial_\nu Pressure = 0, i.e., displacement = 0
            !!               nothing because Neuman BC is automatic

            !! TAG_FREE = TAG_DIRICHLET 
            !!           ==> !! -------------------------- <!
                             !! it will be done at the end <!
                             !! -------------------------- <!

          case(tag_pml,tag_planewave)
            ctx_err%msg   = "** ERROR: PML not thoroughly "//           &
                            "implemented for this equation "//          &
                            "[create_matrix_quadrature] "
            ctx_err%ierr  = -1
            cycle

          case default

        end select
      
      end do
      if(ctx_err%ierr .ne. 0) cycle !! non-shared error

      ! ----------------------------------------------------------------
      ! construct HDG matrices A and C 
      ! ----------------------------------------------------------------
      n = ndof_vol*dim_sol
      !! construction Ainv
      call hdg_build_Ainv_3D_tet_quadrature(hdgAinv,                    &
                     vol_phi_phi(i_order_cell)%array,                   &
                     vol_phi_phi_q(i_order_cell)%array,                 &
                     vol_dphi_phi(i_order_cell)%array,                  &
                     sumface_phii_phij_tau,                             &
                     ctx_dg%inv_jac(:,:,i_cell),                        &
                     ctx_dg%det_jac(i_cell),ndof_vol,ctx_err)
      if(ctx_err%ierr .ne. 0) cycle

      !! construction C
      call hdg_build_C_3D_tet(hdgC,face_phii_xij,face_phii_xij_tau,&
                              penalization,ndof_vol,                    &
                              ndof_face_neigh,ndf_edges_elem,           &
                              ctx_dg%normal(:,:,i_cell))

      !! update saved matrices
      ctx_dg%hdg%Ainv(i_cell)%array(1:n,1:n) = cmplx(hdgAinv(1:n,1:n),kind=RKIND_MAT)
      !! Q =-A^-1 C
      ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot) = cmplx(-matmul(  &
                   hdgAinv(1:n,1:n),hdgC(1:n,1:ndof_face_tot)),kind=RKIND_MAT)
      !! X =-B A^-1
      ctx_dg%hdg%X(i_cell)%array(1:ndof_face_tot,1:n) = cmplx(-matmul(  &
                  hdgB(1:ndof_face_tot,1:n),hdgAinv(1:n,1:n)),kind=RKIND_MAT)

      if(flag_inv) then
       ctx_dg%hdg%W(i_cell)%array(1:ndof_face_tot,1:n)=cmplx(           &
                     matmul(transpose(conjg(hdgC(1:n,1:ndof_face_tot))),&
                            transpose(conjg(hdgAinv(1:n,1:n)))), kind=RKIND_MAT)
        ctx_dg%hdg%D(i_cell)%array(1:n,1:ndof_face_tot)=cmplx(          &
            matmul(transpose(conjg(hdgAinv(1:n,1:n))),                  &
                   transpose(conjg(hdgB(1:ndof_face_tot,1:n)))), kind=RKIND_MAT)
      endif

      !! local matrix coeff = L - B*A^-1*C
      !!                    = L + B*Q
      coeff(1:ndof_face_tot,1:ndof_face_tot) =                &
                      hdgL(1:ndof_face_tot,1:ndof_face_tot) + &
                           matmul(hdgB(1:ndof_face_tot,1:n),  &
                           dcmplx(ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot)))

      !! -----------------------------------------------
      !! readjust the free surface case to identity
      !! it is similar to dirichlet and planewave
      !! because we impose the planewave with dirichlet.
      !! -----------------------------------------------
      do iface=1, ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        if(neigh .eq. tag_freesurface .or.&
           neigh .eq. tag_dirichlet) then
          !! create a Identity matrix coefficients to impose free
          !! surface in pressure, i.e. pressure(x_\Gamma) = 0
          coeff(  ndf_edges_elem(iface)+1: &
                  ndf_edges_elem(iface)+ndof_face,:)=0.0
          !! -----------------------------------------------------------
          !! keep lines. coeff(:,ndf_edges_elem(iface)+1: &
          !! keep lines.         ndf_edges_elem(iface)+ndof_face)=0.0
          !! save lines to symmetrize the matrix.
          flag_info_sym(iface) = .true. 
          !! we can use ndof_face above because the face on the boundary
          !! anyway has the order of the cell, but it should be 
          !! ndof_face_neigh(iface)
          ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) =                 &
            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) +               &
            !! only the other faces.
            (ndof_face_tot - ndof_face_neigh(iface))*ndof_face_neigh(iface)
          !! -----------------------------------------------------------
          !! diagonal
          do i=1, ndof_face
            coeff(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+i) = 1.0
          end do
        end if
      end do
      !! allocate arrays if non-zeros
      if(ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) .ne. 0) then
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_line(i_cell),   &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_col(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_val(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
         ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array = 0.d0
      end if 
      ind_sym = 0
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Update global coefficients
      !! we find the appropriate index for the face first
      do iface=1,ctx_mesh%n_neigh_per_cell
        if(ctx_err%ierr .ne. 0) cycle
        
        do ipos=1,ndf_edges_elem(iface+1)-ndf_edges_elem(iface)
          if(ctx_err%ierr .ne. 0) cycle
          
          !! line info -------------------------------------------------
          i        = ndf_edges_elem(iface)+ipos
          iline    = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(iface,i_cell))
          i_o_neigh= i_order_neigh(iface)
          i_orient = ctx_mesh%cell_face_orientation(iface,i_cell)
          iline=iline + ctx_dg%hdg%idof_face(i_orient,i_o_neigh)%array(ipos)

          !! col info --------------------------------------------------
          do jface=1,ctx_mesh%n_neigh_per_cell
            if(ctx_err%ierr .ne. 0) cycle
            
            do jpos=1,ndf_edges_elem(jface+1)-ndf_edges_elem(jface)
              if(ctx_err%ierr .ne. 0) cycle

              j        = ndf_edges_elem(jface)+jpos
              icol     = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(jface,i_cell))
              i_o_neigh= i_order_neigh(jface)
              i_orient = ctx_mesh%cell_face_orientation(jface,i_cell)
              icol=icol + ctx_dg%hdg%idof_face (i_orient,i_o_neigh)%array(jpos)

              !! save Dirichlet BC infos to make the matrix symmetric    
              !! by modifying the rhs.
              if( flag_info_sym(jface) .and. (.not.flag_info_sym(iface))) then
                ind_sym = ind_sym + 1
                ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array(ind_sym) = &
                  int(iline, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array(ind_sym) = &
                  int(icol, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array(ind_sym) = &
                                       cmplx(coeff(i,j),kind=RKIND_POL)
                !! this coeff can be set to zero now.
                coeff(i,j) = 0.d0
              end if

              !! _______________________________________________________
              !! The matrix is symmetric
              !! _______________________________________________________
              if(abs(coeff(i,j))>tol) then
                if(icol >= iline) then
                  !$OMP CRITICAL
                  nnz_exact       = nnz_exact + 1
                  Alin(nnz_exact) = iline
                  Acol(nnz_exact) = icol
                  Aval(nnz_exact) = cmplx(coeff(i,j),kind=RKIND_MAT)
                  !$OMP END CRITICAL

                  !! check too high values 
                  if(real (Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.aimag(Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.ieee_is_nan(real (Aval(nnz_exact))) .or.                &
                     ieee_is_nan(aimag(Aval(nnz_exact)))) then
                    write(ctx_err%msg,'(a,i0,a,es9.2,a)') "** ERROR: "//  &
                          "the matrix has entry infinity or NaN."//       &
                          " Current matrix precision kind is ", RKIND_MAT,&
                          " (8 at most), which amounts to a"     //       &
                          " maximal value of ",                           &
                          huge(real(1.0,kind=RKIND_MAT)), " [create_matrix]"
                    ctx_err%ierr  = -1
                    cycle
                  end if
                end if
              end if

            end do
          end do
        end do
      end do
      ! ----------------------------------------------------------------

    end do
    !$OMP END DO
    dim_nnz_loc = nnz_exact

    deallocate(hdgL)
    deallocate(coeff)
    deallocate(face_phii_xij)
    deallocate(face_phii_xij_tau)
    deallocate(sumface_phii_phij_tau)
    deallocate(hdgC)
    deallocate(hdgB)
    deallocate(hdgAinv)
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi_q  (i))
      call array_deallocate(vol_phi_phi    (i))
      call array_deallocate(vol_dphi_phi   (i))
      do j = 1, ctx_dg%n_different_order
        call array_deallocate(face_phi_phi(i,j))
        call array_deallocate(face_phi_phi_tau(i,j))
      end do
    end do
    deallocate(vol_phi_phi_q   )
    deallocate(vol_phi_phi     )
    deallocate(vol_dphi_phi    )
    deallocate(face_phi_phi    )
    deallocate(face_phi_phi_tau)
    !$OMP END PARALLEL
    
    return
  end subroutine create_matrix_quadrature_3D

end module m_create_matrix_quadrature
