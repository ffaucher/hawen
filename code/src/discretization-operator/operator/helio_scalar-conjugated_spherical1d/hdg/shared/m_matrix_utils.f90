!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_utils.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> ODE in HELIO  using the HDG discretization
!
!------------------------------------------------------------------------------
module m_matrix_utils

  !! module used -------------------------------------------------------
  use m_raise_error,               only : raise_error, t_error
  use m_define_precision,          only : IKIND_MESH,RKIND_POL
  use m_ctx_domain,                only : t_domain
  use m_ctx_discretization,        only : t_discretization
  use m_lapack_inverse,            only : lapack_matrix_inverse
  use m_mesh_simplex_area,         only : mesh_area_simplex
  use m_ctx_model,                 only : t_model
  use m_model_eval,                only : model_eval_piecewise_constant,  &
                                          model_eval_piecewise_polynomial,&
                                          model_eval_dof_refelem,         &
                                          model_eval_sep
  use m_ctx_model_representation,  only : tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                          tag_MODEL_PCONSTANT, tag_MODEL_DOF
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: hdg_build_Ainv_1D_line,hdg_build_C_1D_line
  public :: hdg_build_local_matrix_1D

  contains


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Create the local integral over the current cell to obtain 
  !> from [Pham et al. 2019] (i.e., the local element matrices)
  !>     T = \int r \phi_i \phi_j   +   r² \phi_i' \phi_j
  !>     Q = \int \phi_i F(r) \phi_j
  !>         with F(r) =  -kappa² r² + q r² + l*(l+1)
  !>         where
  !>                kappa = sigma / c 
  !>                    q = alpha²/4 + alpha'/2 + alpha/r
  !>     S = \int 2 \phi_i \phi_j + \int r \phi_i \phi_j'
  !>     M = \int r \phi_i \phi_j
  !
  !> @param[in]    ctx_dg          : DG context
  !> @param[in]    ctx_mesh        : mesh context
  !> @param[in]    i_cell          : current cell
  !> @param[in]    xdof            : position of the dof (i.e. r)
  !> @param[out]   mem             : memory required
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_local_matrix_1D(ctx_dg,ctx_mesh,model,           &
                                       i_cell,i_o_cell,freq,lmode,      &
                                       vol_matrix_Te,vol_matrix_Qe,     &
                                       vol_matrix_Se,vol_matrix_Me,     &
                                       mem,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=8)                    ,intent(in)    :: lmode
    integer                            ,intent(in)    :: i_o_cell
    !! these are the matrices that are computed here
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_matrix_Te(:,:,:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_matrix_Qe(:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_matrix_Se(:,:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_matrix_Me(:,:)
    integer(kind=8)                    ,intent(inout) :: mem
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local 
    integer                          :: k, i, j
    integer                          :: n_dof
    integer(kind=8)                  :: real_size_pol
    !! models
    real   (kind=8)                  :: v,a,da,g
    real   (kind=8)     ,allocatable :: all_v(:),all_a(:),all_da(:),all_g(:)
    complex(kind=8)                  :: omega,sigma,kinf
    real   (kind=8)     ,allocatable :: alpha(:)
    complex(kind=8)     ,allocatable :: wavenum(:)
    real   (kind=8)                  :: xpt(1)
    !! -----------------------------------------------------------------
    real(kind=8)                     :: jac1d(1,1),bval(1),pt_loc
    real(kind=8)                     :: node_coo(1,2),quad_pt(1)
    integer                          :: iquad
    real   (kind=RKIND_POL)          :: quad_phi_phi
    real   (kind=RKIND_POL)          :: quad_phi_phi_x
    real   (kind=RKIND_POL)          :: quad_phi_phi_x_alpha
    complex(kind=RKIND_POL)          :: quad_phi_phi_x2_wnum
    real   (kind=RKIND_POL)          :: quad_dphi_phi_x
    real   (kind=RKIND_POL)          :: quad_dphi_phi_x2
    
    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then 
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! evaluate the model parameters in all quadrature points,
    !! we need alpha and the wavenumber (that depends on vp, etc)

    !! We have already the frequency times i, that is 
    !!     i \sigma = i\omega -   \laplace
    !! but we want
    !!     \sigma =  \omega + i \laplace
    !! => we multiply by -i
    !! -----------------------------------------------------------------      
    omega = - dcmplx(0.,1.)*freq !! possibility for complex freq ?
    allocate(wavenum(ctx_dg%quadGL_npts))
    allocate(alpha  (ctx_dg%quadGL_npts))
    wavenum = 0.d0 ; alpha = 0.d0

    select case(trim(adjustl(model%format_disc)))
      case(tag_MODEL_PCONSTANT)
        !! evaluate the models at this cell
        call model_eval_piecewise_constant(model,i_cell,freq,v,a,da,g,ctx_err)

        !! sigma: complex frequency
        sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*g/omega) * omega)
        !! wave-number
        kinf  = dcmplx(sigma/v)
        !! q = alpha²/4 + alpha'/2 + alpha/r
        wavenum(:) = dcmplx(sqrt(sigma**2/(v**2) - a**2/4.d0 - da/2.))
        alpha  (:) = a

      case(tag_MODEL_PPOLY)   
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the reference 
          !! element coordinates directly. 
          !! ***********************************************************
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          call model_eval_piecewise_polynomial(model,i_cell,xpt,freq,   &
                                               v,a,da,g,ctx_err)
          !! sigma: complex frequency
          sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*g/omega) * omega)
          !! wave-number
          kinf  = dcmplx(sigma/v)
          !! q = alpha²/4 + alpha'/2 + alpha/r
          wavenum(iquad)= dcmplx(sqrt(sigma**2/(v**2) - a**2/4.d0 - da/2.))
          alpha  (iquad)= a

        end do

      case(tag_MODEL_SEP)
        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts
          !! ***********************************************************
          !! evaluate the models at this point, it uses the global
          !! position
          !! ***********************************************************
          !! find its global position
          !! we have jac2D * (x_ref,y_ref)^T + (x,y)_node1 = (x,y)al
          quad_pt(:) = dble  (ctx_dg%quadGL_pts(:,iquad))
          xpt    (:) = matmul(dble(ctx_dg%jac(:,:,i_cell)), quad_pt) +  &
                       dble(node_coo(:,1))
          call model_eval_sep(model,xpt,freq,v,a,da,g,ctx_err)

          !! sigma: complex frequency
          sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*g/omega) * omega)
          !! wave-number
          kinf  = dcmplx(sigma/v)
          !! q = alpha²/4 + alpha'/2 + alpha/r
          wavenum(iquad)= dcmplx(sqrt(sigma**2/(v**2) - a**2/4.d0 - da/2.))
          alpha  (iquad)= a
       end do

      case(tag_MODEL_DOF)   
        !! evaluate all models using the reference element
        allocate(all_v (ctx_dg%quadGL_npts))
        allocate(all_a (ctx_dg%quadGL_npts))
        allocate(all_da(ctx_dg%quadGL_npts))
        allocate(all_g (ctx_dg%quadGL_npts))
        all_v =0.d0
        all_a =0.d0
        all_da=0.d0
        all_g =0.d0
        call model_eval_dof_refelem(model,i_cell,freq,                  &
                                    dble(ctx_dg%quadGL_pts),            &
                                    ctx_dg%quadGL_npts,all_v,all_a,     &
                                    all_da,all_g,ctx_err)

        node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))
        do iquad=1,ctx_dg%quadGL_npts

          !! ***********************************************************
          !! evaluate the models at this point, it uses the reference 
          !! element coordinates directly. 
          !! ***********************************************************
          xpt = dble(ctx_dg%quadGL_pts(:,iquad))
          v  = all_v(iquad) 
          a  = all_a(iquad)
          da = all_da(iquad)
          g  = all_g(iquad)
          !! sigma: complex frequency
          sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*g/omega) * omega)
          !! wave-number
          kinf  = dcmplx(sigma/v)
          !! q = alpha²/4 + alpha'/2 + alpha/r
          wavenum(iquad)= dcmplx(sqrt(sigma**2/(v**2) - a**2/4.d0 - da/2.))
          alpha  (iquad)= a

        end do
        deallocate(all_v )
        deallocate(all_a )
        deallocate(all_da)
        deallocate(all_g )

      case default
        ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                        " [build_quadrature] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select

    if(ctx_err%ierr .ne. 0) return
    
    !! -----------------------------------------------------------------
    !!  There are two options: 
    !!   (1) We re-adjust the basis function onto the local element
    !!       and perform all computation with the new basis.
    !!   (2) We remain with the reference element, only the integral
    !!       evaluation is "translated" to the local element.
    !!
    !! We use option 2 because it is much more stable: the basis 
    !! functions onto the local elements have very high coefficients
    !! because of the very small elements...
    !! but it is only possible if we use the quadrature, with exact
    !! evaluation, we must use the addapted basis functions...
    !!
    !! REMARK: -------------------------------------------------------
    !!   for a polynomial given for x between [0 1],
    !!   it corresponds to, for some y between [a b]
    !!   to P((y-a)/(b-a))
    !!   such that x = (y-a)/(b-a) 
    !! -----------------------------------------------------------------

    !! only if not quadrature
    !if(.not. ctx_dg%flag_eval_int_quadrature) then
    !  ! --------------------------------------------------------->
    !  !! the basis function must be written between [xmin xmax]: i.e., 
    !  !! in the current element because we divide by x next.
    !  !! e.g., if we have 
    !  !!    P = a + b x  + c x²   between [0 1]
    !  !! we want Q between [xmin xmax], i.e. 
    !  !!    Q(xmin) = P(0) = a          = alpha + beta xmin + gamma xmin²
    !  !!    Q(xmax) = P(1) = a + b + c  = alpha + beta xmax + gamma xmax²
    !  !!    ...
    !  !! for each basis P, 
    !  !!    [ 1.  xmin  xmin² ] [alpha] = [a      ]
    !  !!    [ 1.  xmid  xmid² ] [beta ] = [P(xmid)]
    !  !!    [ 1.  xmax  xmax² ] [gamma] = [a + b + c]
    !  !! Here, the rhs is actually given by Kronecker symbol, i.e., for
    !  !! the first basis function, we have [1 0 0].
    !  !!
    !  !! The marix is always the same, only the rhs changes with basis
    !  !! ------------------------------------------------------------
    !! -----------------------------------------------------------------

    mem = 0
    !! initialize memory information for the dg context 
    real_size_pol = int8(RKIND_POL)

    !! nodes coordinates    
    node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))

    !! using quadrature rule >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! we need the current cell map from ref element to local one
    !! we have jac1D * (x_ref) + (x)_node1 = (x)_local
    jac1d(:,1) = (node_coo(:,2)-node_coo(:,1))
    bval(:)    =  node_coo(:,1)

    !! -----------------------------------------------------------------
    !! Create local matrices, the volumic matrices only need the 
    !! cell order, the faces matrix must have the combinations though
    !! -----------------------------------------------------------------
    k = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)

    mem = mem +                                                &
          (2*ctx_dg%dim_domain*n_dof*n_dof)*real_size_pol    + &  !! T
          (n_dof*n_dof)*real_size_pol*2                      + &  !! Cxmplx(Q)
          (2*ctx_dg%dim_domain*n_dof*n_dof)*real_size_pol    + &  !! S
          (n_dof*n_dof)*real_size_pol                             !! M
     
    do i=1, n_dof ; do j=1, n_dof
      quad_phi_phi          =0.0
      quad_phi_phi_x        =0.0
      quad_phi_phi_x_alpha  =0.0
      quad_phi_phi_x2_wnum  =0.0
      quad_dphi_phi_x       =0.0
      quad_dphi_phi_x2      =0.0
      do iquad=1,ctx_dg%quadGL_npts
        ! -----------------------------------------------------
        !! we evaluate the point of interest in the local element
        !! 
        ! -----------------------------------------------------
        !! we compute the evaluation of the function times the weight
        pt_loc  = jac1d(1,1) * dble(ctx_dg%quadGL_pts(1,iquad)) + bval(1)

        !! \integral   \phi \phi
        !! \integral x \phi \phi
        !! \integral x²\phi \phi
        !! the precomputation already has the weight.                            
        quad_phi_phi   = quad_phi_phi   +                               &
                         ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        quad_phi_phi_x = quad_phi_phi_x +  pt_loc*                      &
                         ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        quad_phi_phi_x_alpha = quad_phi_phi_x_alpha + pt_loc*alpha(iquad)* &
                               ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        quad_phi_phi_x2_wnum = quad_phi_phi_x2_wnum +                   &
                              (wavenum(iquad)*pt_loc)**2 *              &
                               ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)

        !! only 1 dimension so derivative is always 1
        quad_dphi_phi_x = quad_dphi_phi_x +  pt_loc*                    &
                          ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,1)
        quad_dphi_phi_x2= quad_dphi_phi_x2+ (pt_loc*pt_loc)*            &
                          ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,1)
      end do
      
      !! adjust with dimensions 
      quad_phi_phi         = quad_phi_phi         * ctx_dg%det_jac(i_cell)
      quad_phi_phi_x       = quad_phi_phi_x       * ctx_dg%det_jac(i_cell)
      quad_phi_phi_x_alpha = quad_phi_phi_x_alpha * ctx_dg%det_jac(i_cell)
      quad_phi_phi_x2_wnum = quad_phi_phi_x2_wnum * ctx_dg%det_jac(i_cell)
      ! ----------------------------------------------
      !! The derivative for phi' gives 1./(b-a) so nothing
      ! ----------------------------------------------
      !! quad_phi_phi_dx = quad_phi_phi_dx 
      !! quad_phi_phi_dx2= quad_phi_phi_dx2

      !! compute the local matrices ................................
      ! ---------------------------------------------------------
      !!     M = \int r \phi_i \phi_j
      vol_matrix_Me(i,j) = real(quad_phi_phi_x,kind=RKIND_POL)

      ! ---------------------------------------------------------
      !!     T = \int r \phi_i \phi_j   +   r² \phi_i' \phi_j
      !! first part only
      vol_matrix_Te(i,j,1) = vol_matrix_Te(i,j,1) + &
                             real(quad_phi_phi_x,kind=RKIND_POL)
      !! switch i and j 
      vol_matrix_Te(j,i,1) = vol_matrix_Te(j,i,1) + &
                             real(quad_dphi_phi_x2,kind=RKIND_POL)

      ! ---------------------------------------------------------
      !!     Q = \int \phi_i F(r) \phi_j
      !!         with F(r) = -k²r² + \alpha r + l(l+1)
      vol_matrix_Qe(i,j) = cmplx(quad_phi_phi_x_alpha,kind=RKIND_POL)
      vol_matrix_Qe(i,j) = vol_matrix_Qe(i,j) -                         &
                           cmplx(quad_phi_phi_x2_wnum,kind=RKIND_POL)
      vol_matrix_Qe(i,j) = vol_matrix_Qe(i,j) +                         &
                           cmplx((lmode*(lmode+1))*quad_phi_phi,kind=RKIND_POL)
      ! ---------------------------------------------------------
      !!     S = \int 2 \phi_i \phi_j + \int r \phi_i' \phi_j
      vol_matrix_Se(i,j,1) = real(quad_phi_phi*2.d0 + quad_dphi_phi_x,  &
                                  kind=RKIND_POL)


    enddo ; enddo

    return

  end subroutine hdg_build_local_matrix_1D

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in ODE in HELIO 
  !> in 1D based upon segment discretization 
  !
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    vol_matrix_    : current reference matrix 
  !> @param[in]    vol_matrix_    : current reference matrix 
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    rho,kappa      : models parameter
  !> @param[in]    r1,r2          : segment positions
  !> @param[in]    i_cell         : current cell
  !> @param[in]    i_order        : current order index
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_1D_line(ctx_dg,matrixTk,matrixQk,matrixSk,  &
                                    matrixMk,r1,r2,Ainv,ndof_vol,       &
                                    penalization,normal,ctx_err)
    implicit none
    type(t_discretization)           ,intent(in)   :: ctx_dg
    real   (kind=RKIND_POL)          ,intent(in)   :: matrixTk(:,:)
    complex(kind=RKIND_POL)          ,intent(in)   :: matrixQk(:,:)
    real   (kind=RKIND_POL)          ,intent(in)   :: matrixSk(:,:)
    real   (kind=RKIND_POL)          ,intent(in)   :: matrixMk(:,:)
    complex(kind=8), allocatable     ,intent(inout):: Ainv(:,:)
    real   (kind=8)                  ,intent(in)   :: r1,r2
    integer                          ,intent(in)   :: ndof_vol
    real   (kind=8)                  ,intent(in)   :: penalization(2)
    real   (kind=8)                  ,intent(in)   :: normal(1,2)


    type(t_error)                    ,intent(inout):: ctx_err
    !!   
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n
    
    ctx_err%ierr = 0
    dim_sol = ctx_dg%dim_sol
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    
    !! -----------------------------------------------------------------
    !! LOCAL element already, we have: [Pham et al., 2019]
    !! 
    !!        (-T_k      Q_k + \tau_k R_k )
    !!  A_k = (                           )
    !!        ( M_k       S_k              )
    !! -------------------------------------
    !! -----------------------------------------------------------------
    do i=1, ndof_vol
      do j=1, ndof_vol
        A(i         , j         ) = dcmplx(-matrixTk(i,j))
        A(i         , j+ndof_vol) = dcmplx( matrixQk(i,j))
        A(i+ndof_vol, j         ) = dcmplx( matrixMk(i,j))
        A(i+ndof_vol, j+ndof_vol) = dcmplx( matrixSk(i,j))
      enddo
    enddo

    !! adjust with R_k 
    A(       1,  ndof_vol+1) = A(       1,  ndof_vol+1) -               &
                               dcmplx(penalization(1)*r1*r1*normal(1,1))
    A(ndof_vol,2*ndof_vol  ) = A(ndof_vol,2*ndof_vol  ) -               &
                               dcmplx(penalization(2)*r2*r2*normal(1,2))

    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)

    return
  end subroutine hdg_build_Ainv_1D_line


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix C for HDG in ODE in HELIO 
  !> in 1D based upon line discretization 
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_C_1D_line(C,penalization,ndof_vol,normal,r1,r2)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: C(:,:)
    real   (kind=8)                  ,intent(in)   :: penalization(2)
    real   (kind=8)                  ,intent(in)   :: r1,r2
    integer                          ,intent(in)   :: ndof_vol
    real(kind=8)                     ,intent(in)   :: normal(1,2)
    !!

        
    !! -----------------------------------------------------------------
    !! LOCAL element already, we have: [Pham et al., 2019]
    !! 
    !!        (-\tau_k r1_k² e1     \tau_k r2_k² e_{m_k} )
    !!  C_k = (                                          )
    !!        (       r1 e1              -r2 e_{m_k}    )
    !!
    !! e indicates a vector of size n_dof, the index indicates the ONLY
    !! non-zero values, i.e., e1      = (1 0 0 ... 0)
    !!                        e_{m_k} = (0 0 0 ... 1)
    !!
    !! i.e., only the boundary dof are non-zeros here
    !! BECAUSE FACES ARE ORDER 1 ANYWAY
    !! -----------------------------------------------------------------
    C=0.0 !! because order 1 face 
    C(           1,1)= penalization(1)*(r1**2)*normal(1,1)
    C(  ndof_vol  ,2)= penalization(2)*(r2**2)*normal(1,2)
    C(  ndof_vol+1,1)=-r1*normal(1,1)
    C(2*ndof_vol  ,2)=-r2*normal(1,2)
    !! do i_face=1,2
    !!   do i=1,ndof_vol ; do j=1,ndof_face_neigh(i_face)
    !!     C(           i,ndf_edges_elem(i_face)+j)=F(i_face,i,j)*normal(1,i_face)
    !!     C(1*ndof_vol+i,ndf_edges_elem(i_face)+j)=penalization(i_face)*F(i_face,i,j)
    !!   enddo ; enddo
    !! enddo
    return
  end subroutine hdg_build_C_1D_line
end module m_matrix_utils
