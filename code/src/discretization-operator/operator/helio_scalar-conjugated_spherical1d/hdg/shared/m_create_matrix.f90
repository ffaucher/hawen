!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_matrix.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for ODE in HELIO 
!> using the HDG discretization. 
!> We must use quadrature rules for the integrals because we
!> have 1/x in the computations. 
!
!------------------------------------------------------------------------------
module m_create_matrix

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_distribute_error,         only: distribute_error
  use m_define_precision,         only: IKIND_MAT, RKIND_MAT
  use m_ctx_parallelism,          only: t_parallelism
  use m_allreduce_sum,            only: allreduce_sum
  use m_ctx_equation,             only: t_equation
  !> matrix and cartesian domain types
  use m_ctx_domain,               only: t_domain
  use m_ctx_discretization,       only: t_discretization,               &
                                        dg_symmat_reset_context,        &
                                        dg_reset_face_param
  use m_ctx_model,                only: t_model
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT
  use m_dg_lagrange_simplex_init_ctx, only: dg_lagrange_init_ref_matrix,&
                                      dg_lagrange_init_quadrature_vector
  use m_print_dg,                 only: print_info_dg_refmat
  use m_pml_work,                 only: initialize_pml_coeff     
  use m_create_matrix_refelem,    only: create_matrix_refelem_1D
  use m_create_matrix_quadrature, only: create_matrix_quadrature_1D  
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: create_matrix

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from ODE in HELIO sing HDG
  !
  !> @param[in]    ctx_paral         : parallel context
  !> @param[in]    ctx_equation      : equation context
  !> @param[in]    ctx_mesh          : mesh grid type
  !> @param[in]    ctx_model         : model parameters
  !> @param[in]    ctx_discretization: type DG for reference matrix
  !> @param[out]   dim_nnz_loc       : local nonzeros
  !> @param[out]   dim_nnz_gb        : global nonzeros
  !> @param[inout] ilin(:)           : matrix I
  !> @param[inout] icol(:)           : matrix J
  !> @param[inout] Aval(:)           : matrix values
  !> @param[in]    flag_inv          : indicates inversion
  !> @param[in]    verb              : indicates if verbose
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix(ctx_paral,ctx_equation,ctx_mesh,             &
                           ctx_discretization,ctx_model,dim_nnz_loc,    &
                           dim_nnz_gb,ilin,icol,Aval,flag_inv,verb,ctx_err)
    implicit none

    type(t_parallelism)              ,intent(in)      :: ctx_paral
    type(t_equation)                 ,intent(in)      :: ctx_equation
    type(t_domain)                   ,intent(in)      :: ctx_mesh
    type(t_discretization)           ,intent(inout)   :: ctx_discretization
    type(t_model)                    ,intent(in)      :: ctx_model
    integer(kind=8)                  ,intent(out)     :: dim_nnz_loc
    integer(kind=8)                  ,intent(out)     :: dim_nnz_gb
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: ilin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout) :: icol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout) :: Aval(:)
    logical                          ,intent(in)      :: flag_inv
    integer                          ,intent(in)      :: verb
    type(t_error)                    ,intent(inout)   :: ctx_err
    !! local
    complex(kind=8) :: angular_freq
    integer(kind=4) :: mode
    integer(kind=8) :: mem,mem_loc
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! get infos from equation context
    angular_freq = ctx_equation%current_angular_frequency
    mode         = ctx_equation%current_mode

    !! initialize PML informations >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! the size possibly depends on the wavelength so it must be 
    !! computed for each frequency. It is NOT compatible with 
    !! the 1D ODE equation for now.
    if(ctx_mesh%pml_flag) then
      ctx_err%msg   = "** ERROR: the use of PML is not implemented "//  &
                      "for the 1D ODE helio problem but we have "   //  &
                      "several options for radiation conditions ..."//  &
                      "[create_matrix] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
      !call initialize_pml_coeff(ctx_mesh,ctx_discretization,angular_freq,ctx_err)
    end if
    !! -----------------------------------------------------------------

    !! make sure the array is clean before we start
    call dg_symmat_reset_context(ctx_discretization%ctx_symmetrize_mat)
    call dg_reset_face_param    (ctx_discretization)

    !! depends if we use quadrature rules to approximate the integrals
    !! or the traditional reference element method .
    if(ctx_discretization%flag_eval_int_quadrature) then
      if(.not. ctx_discretization%flag_refmatrix) then
        mem = 0
        call dg_lagrange_init_quadrature_vector(ctx_paral,ctx_discretization,mem,ctx_err)
        ctx_discretization%memory_loc=ctx_discretization%memory_loc + mem
        if(verb > 0) then
          call print_info_dg_refmat(ctx_paral,mem,ctx_err)
        end if
        ctx_discretization%flag_refmatrix = .true.
      endif
      !! -----------------------------------------------------------
      !! create according to the dimension, 
      !! piecewise constant model representations
      !! -----------------------------------------------------------
      select case(ctx_mesh%dim_domain)
      
        case(1)
          call create_matrix_quadrature_1D(ctx_mesh,ctx_model,          &
                                  ctx_discretization,dim_nnz_loc,ilin,  &
                                  icol,Aval,angular_freq,mode,flag_inv, &
                                  mem_loc,ctx_err)
      
        case default
          ctx_err%msg   = "** ERROR: helio ODE equation for HDG needs"//&
                          " dimension 1 [create_matrix] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      
    else

     !! ------------------------------------------------------------- <!
     !!                                                               <!
     !!          We must have quadrature rules, it will raise an      <!
     !!                            ERROR                              <!
     !!                                                               <!
     !! ------------------------------------------------------------- <!

     !! only works if we have a picewise constant parameter ...........
      if(trim(adjustl(ctx_model%format_disc)) .ne. tag_MODEL_PCONSTANT) then
        ctx_err%msg   = "** ERROR: reference elements method require "//&
                        "piecewise-constant model [create_matrix] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      
      ! ----------------------------------------------------------------
      !! We first need to compute the reference matrices, they are same
      !! to all cells if the model is represented with a piecewise 
      !! constant, otherwize, it should be dealt with on the fly
      !! VOLUMIC INFOS: 
      !!      \phi_i  \phi_j ==> mass matrix
      !!     d\phi_i  \phi_j
      !!      \phi_i d\phi_j
      !!     d\phi_i d\phi_j
      !! INTERFACE INFOS:
      !!      \phi_i  \phi_j ==> face mass matrix
      ! ----------------------------------------------------------------
      if(.not. ctx_discretization%flag_refmatrix) then
        mem = 0
        call dg_lagrange_init_ref_matrix(ctx_paral,ctx_discretization,  &
                                         ctx_mesh,mem,ctx_err)
        ctx_discretization%memory_loc=ctx_discretization%memory_loc + mem
        if(verb > 0) then
          call print_info_dg_refmat(ctx_paral,mem,ctx_err)
        end if
        ctx_discretization%flag_refmatrix = .true.
      endif
        
      !! -----------------------------------------------------------
      !! create according to the dimension, it returns an error 
      !! as the reference element method does not work.
      !! -----------------------------------------------------------
      select case(ctx_mesh%dim_domain)
        case(1)
          call create_matrix_refelem_1D(ctx_err)
      
        case default
          ctx_err%msg   = "** ERROR: Helio ODE equation for HDG "   //  &
                          "needs dimension 1 [create_matrix] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    
    end if
    !! -----------------------------------------------------------------
    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! adjust global nnz count
    call allreduce_sum(dim_nnz_loc,dim_nnz_gb,1,ctx_paral%communicator,ctx_err)

    return

  end subroutine create_matrix

end module m_create_matrix
