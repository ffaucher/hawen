!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_matrix_refelem.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for 1D ODE helio
!> propagator using the HDG discretization with the reference 
!> element method. 
!> However, it is not possible because the discretization involves
!> 1/x coefficient inside the integrals: we have to use quadrature
!> rules.
!
!------------------------------------------------------------------------------
module m_create_matrix_refelem

  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: create_matrix_refelem_1D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> return an error, we cannot use reference element, we must have
  !> quadrature rules for this propagator.
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_refelem_1D(ctx_err)

    implicit none

    type(t_error)     ,intent(inout)   :: ctx_err

    ctx_err%msg   = "** ERROR: the reference element method " //        &
                    "cannot be used with the 1D ODE helio "   //        &
                    "problem, it must use quadrature rules. **"
    ctx_err%ierr  = -1
    return !! send upwards
  end subroutine create_matrix_refelem_1D

end module m_create_matrix_refelem
