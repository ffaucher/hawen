!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_matrix.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> ODE in HELIO using the HDG discretization
!
!------------------------------------------------------------------------------
module m_create_matrix_quadrature

  !! module used -------------------------------------------------------
  use omp_lib
  use, intrinsic                   :: ieee_arithmetic
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: IKIND_MAT, IKIND_MESH, RKIND_MAT, &
                                      RKIND_POL
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_sum,          only: allreduce_sum
  use m_ctx_equation,           only: t_equation
  !! matrix and cartesian domain types
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_tag_bc,                 only: tag_freesurface, tag_pml,         &
                                      tag_wallsurface, tag_absorbing,   &
                                      tag_ghost_cell,                   &
                                      tag_dirichlet,tag_neumann,tag_planewave
  use m_ctx_model,              only: t_model
  use m_model_eval,             only : model_eval_piecewise_constant,   &
                                       model_eval_piecewise_polynomial, &
                                       model_eval_dof_refelem,          &
                                       model_eval_sep
  use m_ctx_model_representation, only: tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_dg_lagrange_simplex_init_ctx, only: dg_lagrange_init_ref_matrix,&
                                      dg_lagrange_init_quadrature_vector
  use m_matrix_utils,           only: hdg_build_Ainv_1D_line,           &
                                      hdg_build_C_1D_line,              &
                                      hdg_build_local_matrix_1D

  !! reference matrices must be computed on the fly because
  !! the physical parameter cannot be taken out of the integral
  use m_array_types,            only: t_array2d_real, t_array3d_real,   &
                                      t_array4d_real, array_deallocate, &
                                      t_array2d_complex, array_allocate
  !! radiation boundary conditions
  use m_compute_whittaker_derivative,  only: compute_dwhittakerW_over_W
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: create_matrix_quadrature_1D

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from ODE in HELIO using HDG in 1D
  !
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[in]    model          : model parameters
  !> @param[inout] ctx_dg         : type DG
  !> @param[out]   dim_nnz_loc    : local nonzeros
  !> @param[inout] ilin(:)        : matrix I
  !> @param[inout] icol(:)        : matrix J
  !> @param[inout] Aval(:)        : matrix values
  !> @param[in]    frequency      : frequency
  !> @param[in]    frequency      : mode
  !> @param[in]    flag_inv       : indicates inversion
  !> @param[inout] ctx_err        : context error
  !
  !----------------------------------------------------------------------------
  subroutine create_matrix_quadrature_1D(ctx_mesh,model,ctx_dg,         &
                                         dim_nnz_loc,Alin,Acol,Aval,    &
                                         freq,mode,flag_inv,mem_locmat,ctx_err)
    implicit none
    type(t_domain)                     ,intent(in)      :: ctx_mesh
    type(t_model)                      ,intent(in)      :: model
    type(t_discretization)             ,intent(inout)   :: ctx_dg
    integer(kind=8)                    ,intent(out)     :: dim_nnz_loc
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Alin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Acol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout)   :: Aval(:)
    complex(kind=8)                    ,intent(in)      :: freq
    integer(kind=4)                    ,intent(in)      :: mode
    logical                            ,intent(in)      :: flag_inv
    integer(kind=8)                    ,intent(out)     :: mem_locmat
    type(t_error)                      ,intent(inout)   :: ctx_err
    !! local
    integer(kind=IKIND_MAT)             :: nnz_exact
    real   (kind=8)                     :: tol
    !! hdg
    complex(kind=8), allocatable :: coeff(:,:)
    complex(kind=8), allocatable :: hdgB(:,:)
    complex(kind=8), allocatable :: hdgL(:,:)
    complex(kind=8), allocatable :: hdgC(:,:)
    complex(kind=8), allocatable :: hdgAinv(:,:)
    complex(kind=8)              :: omega,k,sigma
    real   (kind=8)              :: penalization(2),r1,r2
    real   (kind=8)              :: vel,alpha,gamm,lmode,dalpha
    !! radiation condition
    real   (kind=8)              :: b,xpos,xpt(1),xposgb(1)
    complex(kind=8)              :: a,z,w1,Zval,kinf
    !! for "reference" matrix which have to be recomputed
    !! at every cells. The nomenclature follows [Pham et al., 2019],
    !! more detail can be found below.
    !! (3D if derivative of phi, 2D if no derivative)
    type(t_array3d_real)   ,allocatable :: vol_matrix_Te(:)
    type(t_array3d_real)   ,allocatable :: vol_matrix_Se(:)
    type(t_array2d_real)   ,allocatable :: vol_matrix_Me(:)
    !! complex because of k
    type(t_array2d_complex),allocatable :: vol_matrix_Qe(:)

    integer                             :: m,n_dof
    integer(kind=8)                     :: mem
    !! others
    integer(kind=IKIND_MESH)            :: i_cell,face,neigh
    integer(kind=IKIND_MAT)             :: iline, icol
    integer                             :: ndf_edges_elem(3),order,i_orient
    integer                             :: i_order_cell,ndof_vol,ndof_max_vol
    integer                             :: ndof_max_face,dim_sol,ndof_face_tot
    integer                             :: i,j,dim_var,ndof_face,n
    integer                             :: i_o_neigh,ipos,jpos,iface,jface
    integer                             :: i_order_neigh  (2) !! 1D segment, 2 faces
    integer                             :: ndof_face_neigh(2) !! 1D segment, 2 faces
    integer                             :: ndof_vol_neigh (2) !! 1D segment, 2 faces
    !! for symmetrization of the matrix even with Dirichlet boundary.
    integer        :: ind_sym
    logical        :: flag_info_sym(2)   !! 1D segment, 2 faces
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    mem_locmat    = 0

    lmode  = dble(mode) !! mode at which it is computed

    !! init informations on the matrix and hdg infos
    !! -----------------------------------------------------------------
    Alin = 0  ; Acol=0    ; Aval= 0.0

    nnz_exact = 0
    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif
    !! compute max order dof
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = 1 !! ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    dim_var       = ctx_dg%dim_var
    !! make sure it is zero at start as it is updated.
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared)                                      &
    !$OMP&         PRIVATE(coeff,hdgB,hdgC,hdgAinv,omega,k,sigma),      &
    !$OMP&         PRIVATE(penalization,r1,r2,vel,alpha,gamm),          &
    !$OMP&         PRIVATE(b,xpos,xposgb,a,z,w1,Zval,kinf),             &
    !$OMP&         PRIVATE(vol_matrix_Te,vol_matrix_Se,vol_matrix_Me),  &
    !$OMP&         PRIVATE(vol_matrix_Qe,mem),                          &
    !$OMP&         PRIVATE(i_cell,face,neigh,iline,icol,i_orient),      &
    !$OMP&         PRIVATE(ndf_edges_elem,order,i_order_cell,dalpha),   &
    !$OMP&         PRIVATE(ndof_vol,ndof_face_tot,ndof_face,n,i,j),     &
    !$OMP&         PRIVATE(jpos,jface,i_o_neigh,iface,i_order_neigh),   &
    !$OMP&         PRIVATE(flag_info_sym,ind_sym,m,xpt,n_dof),          &
    !$OMP&         PRIVATE(ipos,ndof_face_neigh,ndof_vol_neigh,hdgL)
    
    
    !! local array, the 2 is because of the 2 line faces
    allocate(coeff   (2*dim_var*ndof_max_face,2*dim_var*ndof_max_face))
    allocate(hdgL    (2*dim_var*ndof_max_face,2*dim_var*ndof_max_face) )
    allocate(hdgC    (dim_sol*ndof_max_vol ,2*dim_var*ndof_max_face))
    allocate(hdgB    (2*dim_var*ndof_max_face,dim_sol*ndof_max_vol) )
    allocate(hdgAinv (dim_sol*ndof_max_vol ,dim_sol*ndof_max_vol) )
    !! -----------------------------------------------------------------
    !! reference matrix are local in helio
    allocate(vol_matrix_Te(ctx_dg%n_different_order))
    allocate(vol_matrix_Qe(ctx_dg%n_different_order))
    allocate(vol_matrix_Se(ctx_dg%n_different_order))
    allocate(vol_matrix_Me(ctx_dg%n_different_order))
    !! allocation is done once and for all
    do m = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order (m)
      !! size depends if derivative or not
      call array_allocate(vol_matrix_Qe(m),n_dof,n_dof,ctx_err)
      call array_allocate(vol_matrix_Me(m),n_dof,n_dof,ctx_err)
      call array_allocate(vol_matrix_Te(m),n_dof,n_dof,ctx_dg%dim_domain,ctx_err)
      call array_allocate(vol_matrix_Se(m),n_dof,n_dof,ctx_dg%dim_domain,ctx_err)
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do i_cell=1,ctx_mesh%n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      
      ctx_dg%hdg%Ainv(i_cell)%array = 0.d0
      ctx_dg%hdg%Q   (i_cell)%array = 0.d0
      ctx_dg%hdg%X   (i_cell)%array = 0.d0
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array = 0.d0
        ctx_dg%hdg%D(i_cell)%array = 0.d0
      endif

      coeff=0.0 ; hdgAinv=0.0 ; hdgB=0.0 ; hdgC=0.0 ; hdgL=0.0
      flag_info_sym = .false.

      !! position of the segment
      r1 = dble(ctx_mesh%x_node(1,ctx_mesh%cell_node(1,i_cell)))
      r2 = dble(ctx_mesh%x_node(1,ctx_mesh%cell_node(2,i_cell)))

      ! --------------------------------------------------------->
      !! initialization of edges informations and model parameters
      i_order_cell = ctx_dg%order_to_index      (ctx_mesh%order(i_cell))
      ndof_vol     = ctx_dg%n_dof_per_order     (i_order_cell)

      !!  WARNING >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !! in one-dimension, the order of the face must be one ...
      ndof_face    = 1 !! ctx_dg%n_dof_face_per_order(1)
      ! ----------------------------------------------------------------
      
      !! Needed for the penalization here ==============================
      !! evaluated using the two positions left/right values
      
      !! both sides ====================================================
      do iface=1, ctx_mesh%n_neigh_per_cell
        select case(iface)
          case(1)
            xpos   = r1   ! global position
            xposgb = r1
            xpt    = 0.d0 ! on the reference element
          case(2)
            xpos   = r2   ! global position
            xposgb = r2
            xpt    = 1.d0 ! on the reference element
          case default
            ctx_err%msg   = "** ERROR:unrecognized face number " //     &
                            " [create_matrix_quadrature] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle !! non-shared error
        end select
        
        !! evaluate model in this position depending on the representation
        select case(trim(adjustl(model%format_disc)))
          case(tag_MODEL_PCONSTANT)
            !! evaluate the models at this cell
            call model_eval_piecewise_constant(model,i_cell,freq,   &
                                               vel,alpha,dalpha,    &
                                               gamm,ctx_err)
    
          case(tag_MODEL_PPOLY)
            !! using the reference element coordinate
            call model_eval_piecewise_polynomial(model,i_cell,xpt,  &
                                                 freq,vel,alpha,    &
                                                 dalpha,gamm,ctx_err)
    
          case(tag_MODEL_SEP)
            !! using the reference element coordinate
            call model_eval_sep(model,xposgb,freq,vel,alpha,dalpha,    &
                                gamm,ctx_err)
    
          case(tag_MODEL_DOF)
            !! using the reference element coordinate
            call model_eval_dof_refelem(model,i_cell,freq,xpt,vel,      &
                                        alpha,dalpha,gamm,ctx_err)
                  
          case default
            ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                            " [create_matrix_quadrature] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle !! non-shared error
        end select
        
        !! compute infos with those parameters
        omega = - dcmplx(0.,1.)*freq !! possibility for complex freq ?
        sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*gamm/omega) * omega)
        !! wave-number
        kinf  = dcmplx(sigma/vel)
        !! q = alpha²/4 + alpha'/2 + alpha/r
        k     = dcmplx(sqrt(sigma**2/(vel**2) - alpha**2/4.d0 - dalpha/2.))
      
      
        !! update the penalization
        !!  TO CLARIFY >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        if(ctx_dg%penalization < 0) then
          ! penalization = 1.d0
          penalization(iface) = abs(aimag(freq)**2/(vel**2)             &
                                    -alpha**2/4.d0-dalpha/2.d0)
        else
          penalization(iface) = ctx_dg%penalization
        end if
        ! ----------------------------------------------------------------
      
      end do !! end penalization on all faces.
      !! ---------------------------------------------------------------

      !! -----------------------------------------------------------------
      !! We first need to compute the local to element matrices, they are
      !! NOT common to all cells, so we need to recompute every time
      !! -----------------------------------------------------------------
      !! for this ODE we refer to the work of [Pham et al., 2019]
      !! for the computation of the local reference matrix, we
      !! have the following names
      !!
      !!     T = \int r \phi_i \phi_j   +   r² \phi_i' \phi_j
      !!     Q = \int \phi_i F(r) \phi_j
      !!         with F(r) =  -kappa² r² + q r² + l*(l+1)
      !!         where
      !!                kappa = sigma / c
      !!                    q = alpha²/4 + alpha'/2 + alpha/r
      !!     S = \int 2 \phi_i \phi_j + \int r \phi_i \phi_j'
      !!     M = \int r \phi_i \phi_j      !!
      !! ---------------------------------------------------------------
      !! therefore, the subroutine needs in input:
      !! r, i.e. the position of all the degrees of freedom.
      !! the Qcoeff, which we compute directly inside, from
      !! k=sqrt(kappa² -  alpha²/4 - alpha'/2), alpha, and l
      !! ---------------------------------------------------------------
      !! initialize to zero.
      vol_matrix_Te(i_order_cell)%array = 0.0
      vol_matrix_Qe(i_order_cell)%array = 0.0
      vol_matrix_Se(i_order_cell)%array = 0.0
      vol_matrix_Me(i_order_cell)%array = 0.0
      mem=0
      call hdg_build_local_matrix_1D(ctx_dg,ctx_mesh,model,i_cell,      &
                     i_order_cell,freq,lmode,                           &
                     vol_matrix_Te(i_order_cell)%array,                 &
                     vol_matrix_Qe(i_order_cell)%array,                 &
                     vol_matrix_Se(i_order_cell)%array,                 &
                     vol_matrix_Me(i_order_cell)%array,mem,ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
      mem_locmat = max(mem_locmat, mem)
      !! ---------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! initialize face informations with neighbors
      !! ndf_edges_elem: for each face we store the incremental ndof
      !! ndf_edges_elem(3) = total number of dof on the edges of the element
      !! ---------------------------------------------------------------
      ndf_edges_elem(1)=0
      do iface=1,ctx_mesh%n_neigh_per_cell
        face                    = ctx_mesh%cell_face   (iface,i_cell)
        order                   = ctx_mesh%order_face  (face)
        i_order_neigh  (iface)  = ctx_dg%order_to_index(order)
        ndof_face_neigh(iface)  = ctx_dg%n_dof_face_per_order(i_order_neigh(iface))
        ndof_vol_neigh (iface)  = ctx_dg%n_dof_per_order     (i_order_neigh(iface))
        ndf_edges_elem (iface+1)=ndf_edges_elem(iface) + ndof_face_neigh(iface)
      end do
      ndof_face_tot = ndf_edges_elem(3)
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Computation of the matrices L and B, which depends if it is a
      !! boundary node or not.
      ! ----------------------------------------------------------------
      !! interior representation:
      hdgL(1,1) = -penalization(1)*ctx_dg%normal(1,1,i_cell) !! face 1
      hdgL(2,2) = -penalization(2)*ctx_dg%normal(1,2,i_cell) !! face 2
      !!
      !! LOCAL element already, we have: [Pham et al., 2019]
      !!
      !!        (\nu e1        \tau_k \nu e1      )
      !!  B_k = (                                 )
      !!        (\nu e_{m_k}   \tau_k \nu e_{m_k} )
      !!
      !! e indicates a vector of size n_dof, the index indicates the ONLY
      !! non-zero values, i.e., e1      = (1 0 0 ... 0)
      !!                        e_{m_k} = (0 0 0 ... 1)
      !!
      !! i.e., only the boundary dof are non-zeros here
      !! BECAUSE FACES ARE ORDER 1 ANYWAY
      !! -----------------------------------------------------------------
      hdgB=0.0
      hdgB(1,           1)  = ctx_dg%normal(1,1,i_cell)
      hdgB(1,  ndof_vol+1)  = penalization(1)*ctx_dg%normal(1,1,i_cell)
      hdgB(2,  ndof_vol  )  = ctx_dg%normal(1,2,i_cell)
      hdgB(2,2*ndof_vol  )  = penalization(2)*ctx_dg%normal(1,2,i_cell)


      ! ----------------------------------------------------------------
      !! adjust in case we have an actual boundary
      do iface=1, ctx_mesh%n_neigh_per_cell
        if(ctx_err%ierr .ne. 0) cycle

        i_o_neigh = i_order_neigh(iface)

        !! in case of actual boundary
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        select case (neigh)
          !! absorbing boundary conditions
          case(tag_absorbing)
            !! first or last point
            if(iface == 1) then
              xpos  = r1
              xposgb= r1
              xpt   = 0.d0 !! on the reference element
            else
              xpos  = r2
              xposgb= r2
              xpt   = 1.d0 !! on the reference element
            end if

            !! we need to adjust with 1/r so it cannot be zero here.
            if(abs(xpos) < tiny(1.d0)) then
              ctx_err%msg   = "** ERROR: Boundary condition for "   //  &
                              "helio ODE does not work at the origin "//&
                              "(x=0) [create_matrix] **"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              cycle
            end if
            
            !! ---------------------------------------------------------
            !! evaluate the models at the position of the interface
            !! ---------------------------------------------------------
            select case(trim(adjustl(model%format_disc)))
              case(tag_MODEL_PCONSTANT)
                !! evaluate the models at this cell
                call model_eval_piecewise_constant(model,i_cell,freq,   &
                                                   vel,alpha,dalpha,    &
                                                   gamm,ctx_err)
        
              case(tag_MODEL_PPOLY)
                !! using the reference element coordinate
                call model_eval_piecewise_polynomial(model,i_cell,xpt,  &
                                                     freq,vel,alpha,    &
                                                     dalpha,gamm,ctx_err)
        
              case(tag_MODEL_SEP)
                !! using the reference element coordinate
                call model_eval_sep(model,xposgb,freq,vel,alpha,dalpha, &
                                    gamm,ctx_err)
        
              case(tag_MODEL_DOF)
                !! using the reference element coordinate
                call model_eval_dof_refelem(model,i_cell,freq,xpt,vel,  &
                                            alpha,dalpha,gamm,ctx_err)
                      
              case default
                ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                                " [create_matrix_quadrature] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle !! non-shared error
            end select
            !! compute infos with those parameters
            omega = - dcmplx(0.,1.)*freq !! possibility for complex freq ?
            sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*gamm/omega) * omega)
            !! wave-number
            kinf  = dcmplx(sigma/vel)
            !! q = alpha²/4 + alpha'/2 + alpha/r
            k     = dcmplx(sqrt(sigma**2/(vel**2) - alpha**2/4.d0 - dalpha/2.))

            !! in 1D the face is order 1 all the time >>>>>>>>>>>>>>>>>>
            !! It depends on the Impedance condition here
            select case(trim(adjustl(ctx_mesh%Zimpedance)))
              case('Z_DtN'     ,'ZDtN'     )

                a = dcmplx(0.,1)*alpha/(2*k)
                b = dble(lmode + 1./2.)
                z = -2.d0*dcmplx(0.,1.)*xpos*k   ;
                !! compute the ratio W'/W directly
                call compute_dwhittakerW_over_W(-a,b,z,w1,ctx_err)
                !! only problem is for NaN 
                if(abs(w1) > huge(1.d0) .or. ieee_is_nan(real (w1)) .or. &
                   ieee_is_nan(aimag(w1)) ) then
                  Zval   = 0.D0 
                  ctx_err%msg = "** WARNING Whittaker DtoN forced to 0"
                  ctx_err%critic=.false.
                  call raise_error(ctx_err)
                else
                  !! appropriate formula
                  Zval   = -2.d0 * dcmplx(0.,1.)*k*w1 !! that is W'/W 
                end if
                !! .............................................................
                !! .............................................................
                !! one can compute directly w' but it loose accuracy ...........
                !! .............................................................
                !! call compute_dwhittakerW(-a,b,z,w1,wprime,ctx_err)
                !! if(abs(w1) < tiny(0.d0) .and. abs(wprime) < tiny(0.d0)) then
                !!   Zval = -2.d0 * dcmplx(0.,1.)*k* (-1.d0/2.d0)
                !! else
                !!   Zval   = -2.d0 * dcmplx(0.,1.)*k*wprime/w1
                !! end if
                !! .............................................................

              case('Z_nonlocal','Znonlocal')
                Zval =  dcmplx(0.,1.)*k*sqrt(1 - alpha/(xpos*k*k)       &
                      - lmode*(lmode+1)/(xpos*xpos*k*k))
              case('ZS_HF_0'   ,'ZSHF0'    )
                Zval = dcmplx(0.,1.)*k
              case('ZS_HF_1a'  ,'ZSHF1a'   )
                Zval = dcmplx(0.,1.)*k - dcmplx(0.,1.)/(2.*k) *         &
                       1./(xpos) * (alpha)
              case('ZS_HF_1b'  ,'ZSHF1b'   )
                Zval = dcmplx(0.,1.)*k - dcmplx(0.,1.)/(2.*k) * 1./(xpos)*&
                      (lmode*(lmode+1)/xpos + alpha)
              case('ZS_SAI_0'  ,'ZSSAI0'   )
                Zval = dcmplx(0.,1.)*k*sqrt(1.0 - alpha/xpos * 1. / (k*k))
              case('ZS_SAI_1'  ,'ZSSAI1'   )
                Zval = dcmplx(0.,1.)*k*sqrt(1. - alpha/xpos * 1./(k*k))-&
                      dcmplx(0.,1.)/2.* (lmode*(lmode+1)/(xpos*xpos*k))/&
                      sqrt(1-alpha/(xpos*k*k))
              case('ZA_HF_0'   ,'ZAHF0'    )
                Zval   = dcmplx(0.,1.) * kinf
              case('ZA_HF_1'   ,'ZAHF1'    )
                Zval   = dcmplx(0.,1.)*kinf+1./(2.*dcmplx(0.,1.)*kinf)* &
                       (lmode*(lmode+1.)/(xpos*xpos) +                  &
                       alpha/xpos + alpha*alpha/4.)
              case('Z_naive'   ,'Znaive'   )
                Zval = 1./xpos + alpha/2.  + dcmplx(0.,1.)*kinf;
              case('ZA_RBC_1'  ,'ZARBC1'   )
                Zval = 1/xpos + dcmplx(0.,1.)*kinf*                     &
                       sqrt(1.-alpha*alpha/(4.*kinf*kinf))
              case default
                ctx_err%msg   = "** ERROR: Unrecognized Impedance "  // &
                                "condition "//                          &
                                trim(adjustl(ctx_mesh%Zimpedance))//    &
                                " for ABC [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
            end select


            !! we need to adjust with 1/r so it cannot be zero here.
            if(abs(xpos) < tiny(1.0)) then
              ctx_err%msg   = "** ERROR: Boundary condition for ODE " //&
                              "Helio does not work at the origin "    //&
                              "(x=0) [create_matrix] **"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              cycle
            end if
            hdgL(iface,iface) = (1./xpos-Zval-penalization(iface))*   &
                                ctx_dg%normal(1,iface,i_cell)

          !! other types
          case(tag_freesurface,tag_ghost_cell,tag_wallsurface,          &
               tag_neumann,tag_dirichlet)
            !! TAG_GHOST ==> nothing special

            !! TAG_WALL = TAG_NEUMANN ==> nothing because Neuman BC is default

            !! TAG_FREE = TAG_DIR = TAG_PLANEWAVE
            !!           ==> !! -------------------------- <!
                             !! it will be done at the end <!
                             !! -------------------------- <!

          case(tag_pml,tag_planewave)
           ctx_err%msg   = "** ERROR: PML not ready for radial ODE in helio "
           ctx_err%ierr  = -1
           cycle

          case default
            !! traditional L and B representation

        end select

      enddo !! end do for all face
      if(ctx_err%ierr .ne. 0) cycle !! non shared for openMP and mpi

      ! ----------------------------------------------------------------
      ! construct HDG matrices
      ! ----------------------------------------------------------------
      n = ndof_vol*dim_sol
      !! construction Ainv
      call hdg_build_Ainv_1D_line(ctx_dg,                               &
                             vol_matrix_Te(i_order_cell)%array(:,:,1),  &
                             vol_matrix_Qe(i_order_cell)%array(:,:)  ,  &
                             vol_matrix_Se(i_order_cell)%array(:,:,1),  &
                             vol_matrix_Me(i_order_cell)%array(:,:)  ,  &
                             r1,r2,hdgAinv,ndof_vol,penalization,       &
                             ctx_dg%normal(:,:,i_cell),ctx_err)
      if(ctx_err%ierr .ne. 0) cycle

      !! construction C
      call hdg_build_C_1D_line(hdgC,penalization,ndof_vol,              &
                               ctx_dg%normal(:,:,i_cell),r1,r2)

      !! update saved matrices
      ctx_dg%hdg%Ainv(i_cell)%array(1:n,1:n) = cmplx(hdgAinv(1:n,1:n),kind=RKIND_MAT)
      !! Q =-A^-1 C
      ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot) = cmplx(-matmul(  &
                   hdgAinv(1:n,1:n),hdgC(1:n,1:ndof_face_tot)),kind=RKIND_MAT)
      !! X =-B A^-1
      ctx_dg%hdg%X(i_cell)%array(1:ndof_face_tot,1:n) = cmplx(-matmul(  &
                  hdgB(1:ndof_face_tot,1:n),hdgAinv(1:n,1:n)),kind=RKIND_MAT)

      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array(1:ndof_face_tot,1:n)=cmplx(          &
                     matmul(transpose(conjg(hdgC(1:n,1:ndof_face_tot))),&
                            transpose(conjg(hdgAinv(1:n,1:n)))), kind=RKIND_MAT)
        ctx_dg%hdg%D(i_cell)%array(1:n,1:ndof_face_tot)=cmplx(          &
            matmul(transpose(conjg(hdgAinv(1:n,1:n))),                  &
                   transpose(conjg(hdgB(1:ndof_face_tot,1:n)))), kind=RKIND_MAT)
      endif

      !! local matrix coeff = L - B*A^-1*C
      !!                    = L + B*Q
      coeff(1:ndof_face_tot,1:ndof_face_tot) =                          &
                      hdgL(1:ndof_face_tot,1:ndof_face_tot) +           &
                           matmul(hdgB(1:ndof_face_tot,1:n),            &
                           dcmplx(ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot)))

      !! ------------------------------------------
      !! readjust the free surface case to identity
      !! ------------------------------------------
      do iface=1, ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        if(neigh .eq. tag_freesurface .or. neigh .eq. tag_dirichlet) then
          !! create a Identity matrix coefficients to impose free
          !! surface, i.e. pressure = 0
          coeff(  ndf_edges_elem(iface)+1: &
                  ndf_edges_elem(iface)+ndof_face_neigh(iface),:)=0.0
          ! -------------------------------------------------------
          !! -----------------------------------------------------------
          !! keep lines. coeff(:,ndf_edges_elem(iface)+1: &
          !! keep lines.         ndf_edges_elem(iface)+ndof_face)=0.0
          !! save lines to symmetrize the matrix.
          flag_info_sym(iface) = .true. 
          !! we can use ndof_face above because the face on the boundary
          !! anyway has the order of the cell, but it should be 
          !! ndof_face_neigh(iface)
          ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) =                 &
            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) +               &
            !! only the other faces.
            (ndof_face_tot - ndof_face_neigh(iface))*ndof_face_neigh(iface)
          !! -----------------------------------------------------------
          !! diagonal
          do i=1, ndof_face
            coeff(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+i) = 1.0
          end do
        end if
      end do
      !! allocate arrays if non-zeros
      if(ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) .ne. 0) then
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_line(i_cell),   &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_col(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_val(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
         ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array = 0.d0
      end if 
      ind_sym = 0
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Update global coefficients
      !! we find the appropriate index for the face first
      do iface=1,ctx_mesh%n_neigh_per_cell
        if(ctx_err%ierr .ne. 0) cycle

        do ipos=1,ndf_edges_elem(iface+1)-ndf_edges_elem(iface)
          if(ctx_err%ierr .ne. 0) cycle

          !! line info -------------------------------------------------
          i        = ndf_edges_elem(iface)+ipos
          iline    = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(iface,i_cell))
          i_o_neigh= i_order_neigh(iface)
          i_orient = ctx_mesh%cell_face_orientation(iface,i_cell)
          iline=iline + ctx_dg%hdg%idof_face(i_orient,i_o_neigh)%array(ipos)

          !! col info --------------------------------------------------
          do jface=1,ctx_mesh%n_neigh_per_cell
            if(ctx_err%ierr .ne. 0) cycle

            do jpos=1,ndf_edges_elem(jface+1)-ndf_edges_elem(jface)
              if(ctx_err%ierr .ne. 0) cycle

              j        = ndf_edges_elem(jface)+jpos
              icol     = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(jface,i_cell))
              i_o_neigh= i_order_neigh(jface)
              i_orient = ctx_mesh%cell_face_orientation(jface,i_cell)
              icol=icol + ctx_dg%hdg%idof_face (i_orient,i_o_neigh)%array(jpos)

              !! save Dirichlet BC infos to make the matrix symmetric    
              !! by modifying the rhs.
              if( flag_info_sym(jface) .and. (.not.flag_info_sym(iface))) then
                ind_sym = ind_sym + 1
                ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array(ind_sym) = &
                  int(iline, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array(ind_sym) = &
                  int(icol, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array(ind_sym) = &
                                       cmplx(coeff(i,j),kind=RKIND_POL)
                !! this coeff can be set to zero now.
                coeff(i,j) = 0.d0
              end if

              !! update is symmetric
              if(abs(coeff(i,j))>tol .and. icol >= iline) then
                !! -----------------------------------------------------

                !! the matrix may not be symmetric because of
                !! Dirichlet boundary Conditions... if(icol >= iline) then
                !$OMP CRITICAL
                nnz_exact       = nnz_exact + 1
                Alin(nnz_exact) = iline
                Acol(nnz_exact) = icol
                Aval(nnz_exact) = cmplx(coeff(i,j),kind=RKIND_MAT)

                !! check too high values
                if(real (Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)).or.&
                   aimag(Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)).or.&
                   ieee_is_nan(real (Aval(nnz_exact))) .or.                   &
                   ieee_is_nan(aimag(Aval(nnz_exact)))) then
                  write(ctx_err%msg,'(a,i0,a,es9.2,a)') "** ERROR: "//  &
                        "the matrix has entry infinity or NaN."//       &
                        " Current matrix precision kind is ", RKIND_MAT,&
                        " (8 at most), which amounts to a"     //       &
                        " maximal value of ",                           &
                        huge(real(1.0,kind=RKIND_MAT)), " [create_matrix]"
                  ctx_err%ierr  = -1
                  ctx_err%critic=.true.
                end if
                !$OMP END CRITICAL
                if(ctx_err%ierr .ne. 0) cycle

              end if

            end do
          end do
        end do
      end do
      ! ----------------------------------------------------------------

   end do
   !$OMP END DO

   dim_nnz_loc = nnz_exact

   deallocate(coeff)
   deallocate(hdgC )
   deallocate(hdgB )
   deallocate(hdgL )
   deallocate(hdgAinv)

   !! clean the ref matrices
   do i = 1, ctx_dg%n_different_order
     call array_deallocate(vol_matrix_Qe(i))
     call array_deallocate(vol_matrix_Te(i))
     call array_deallocate(vol_matrix_Me(i))
     call array_deallocate(vol_matrix_Se(i))
   end do
   deallocate(vol_matrix_Qe)
   deallocate(vol_matrix_Te)
   deallocate(vol_matrix_Me)
   deallocate(vol_matrix_Se)
   !$OMP END PARALLEL

   return
  end subroutine create_matrix_quadrature_1D

end module m_create_matrix_quadrature
