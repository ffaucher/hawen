!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_equation_infos.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module init of the information for the HDG discretization 
!> of the ODE problem in helioseismology
!>  \f$(-\frac{\partial^2}{\partial x^2} - k^2 + \alpha/x + l*(l+1)/x^2) u(x) = 0\f$.
!
!------------------------------------------------------------------------------
module m_equation_infos

  use m_raise_error,              only : raise_error, t_error
  use m_tag_namefield
  use m_tag_scale_rhs,            only : tag_RHS_SCALE_ID,              &
                                         tag_RHS_SCALE_R2

  implicit none

  private
  public  :: equation_infos
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init equation information for ODE in helio using HDG
  !
  !> @param[in]    dim_domain      : dimension of the domain
  !> @param[out]   freq_dep        : indicates if this equation has a 
  !>                                 frequency parameter
  !> @param[out]   mode_dep        : indicates if this equation has a mode 
  !> @param[out]   dim_solution    : number of solution
  !> @param[out]   dim_var         : number of matrix variable
  !> @param[out]   name_sol        : names of the solution
  !> @param[out]   name_var        : names of the variable (traces)
  !> @param[out]   symmetric_matrix: symmetry of the discretized matrix
  !> @param[out]   tag_scale_rhs   : indicates if rhs scaling
  !> @param[out]   n_media         : indicates maximal number of different media
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine equation_infos(dim_domain,                                 &
                            frequency_dependency,mode_dependency,eqname,&
                            medium,dim_solution,dim_var,name_sol,       &
                            name_var,formulation_order,symmetric_matrix,&
                            tag_scale_rhs,n_media,ctx_err)
    
    implicit none
    
    integer                      ,intent(in)   :: dim_domain
    integer                      ,intent(out)  :: dim_solution,dim_var
    integer                      ,intent(out)  :: formulation_order
    character(len=32)            ,intent(out)  :: eqname,medium
    character(len=3) ,allocatable,intent(inout):: name_sol(:),name_var(:)
    logical                      ,intent(out)  :: frequency_dependency
    logical                      ,intent(out)  :: mode_dependency
    logical                      ,intent(out)  :: symmetric_matrix
    integer                      ,intent(out)  :: tag_scale_rhs
    integer                      ,intent(out)  :: n_media
    type(t_error)                ,intent(out)  :: ctx_err
    !!

    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! ODE in helio using HDG
    !! -----------------------------------------------------------------
    eqname   ='helioseismology'
    medium   ='radial-ode'
    frequency_dependency=.true.
    mode_dependency     =.true.
    n_media  = 1 !! only one type of medium in this problem.

    !! -----------------------------------------------------------------
    !! because of the formulation, we must scale the RHS by the radius
    tag_scale_rhs = tag_RHS_SCALE_R2
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! In one-dimension, the HDG matrix is not symmetric because of the
    !! Dirichlet boundary conditions...
    !! symmetric_matrix    =.true.
    !! -----------------------------------------------------------------
    formulation_order= 1 !! order 2 formuation
    dim_var          = 1 !! only one matrix variable: the pressure
    allocate(name_var(dim_var))
    name_var(1) = tag_FIELD_W !! numerical trace for the matrix.

    !! the solution dimension depends on the domain dim
    select case(dim_domain)
      case(1)
        dim_solution     = 2 !! W and V in Helio
        allocate(name_sol(dim_solution))        
        name_sol(1) = tag_FIELD_V
        name_sol(2) = tag_FIELD_W
        symmetric_matrix = .true.
        
      case default

        ctx_err%msg   = "** ERROR: Equation dimension must be 1 "// &
                        "[equation_infos_ODE_helio_HDG] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
        
    end select    
    !! -----------------------------------------------------------------

    return
  end subroutine equation_infos
end module m_equation_infos
