!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_gradient.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the gradient of the cost function w.r.t
!> the physical coefficient
!> discretization:
!>
!> The reference parameters are
!>      1/vp²
!>      alpha
!>      alpha'
!
!------------------------------------------------------------------------------
module m_create_gradient

  !! module used -------------------------------------------------------
  use m_raise_error,               only : raise_error, t_error
  use m_distribute_error,          only : distribute_error
  use m_ctx_parallelism,           only : t_parallelism
  use m_ctx_domain,                only : t_domain
  use m_ctx_discretization,        only : t_discretization
  use m_ctx_model,                 only : t_model
  use m_ctx_model_representation,  only : tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                          tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_ctx_field,                 only: t_field
  use m_create_gradient_quadrature,only: create_gradient_quadrature_1D

  implicit none

  private
  public  :: create_gradient

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create gradient for OD HELIO SPHERICAL using HDG
  !> gradient of the misfit, with respect to:
  !> grad(:,:,1) = dA / d{1/vp²}
  !> grad(:,:,2) = dA / d\alpha
  !> grad(:,:,3) = dA / d\alpha'
  !> if viscosity
  !> grad(:,:,4) = dA / d\gamma
  !>
  !
  !> @param[in]    ctx_paral         : parallel context
  !> @param[in]    ctx_mesh          : mesh grid type
  !> @param[in]    ctx_model         : model parameters
  !> @param[in]    ctx_discretization: type DG for reference matrix
  !> @param[in]    ctx_field_fwd     : forward field 
  !> @param[in]    ctx_field_bwd     : backward field
  !> @param[in]    frequency         : frequency
  !> @param[inout] gradient          : output computed gradient
  !> @param[in]    tag_metric        : indicates the inner product metric
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient(ctx_paral,ctx_mesh,ctx_discretization,     &
                             ctx_model,ctx_field_fwd,ctx_field_bwd,     &
                             angular_freq,gradient,tag_metric,ctx_err)
    implicit none

    type(t_parallelism)              ,intent(in)      :: ctx_paral
    type(t_domain)                   ,intent(in)      :: ctx_mesh
    type(t_discretization)           ,intent(in)      :: ctx_discretization
    type(t_model)                    ,intent(in)      :: ctx_model
    type(t_field)                    ,intent(in)      :: ctx_field_fwd
    type(t_field)                    ,intent(in)      :: ctx_field_bwd
    complex(kind=8)                  ,intent(in)      :: angular_freq
    real   (kind=4)      ,allocatable,intent(inout)   :: gradient(:,:)
    integer                          ,intent(in)      :: tag_metric
    type(t_error)                    ,intent(inout)   :: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    gradient      = 0.0

    !! -----------------------------------------------------------------
    !! create the gradient depending on
    !!  - the dimension
    !!  - the model representation
    !!  - the method (quadrature / reference element)
    !! not every combination are compatible. 
    !! -----------------------------------------------------------------
    if(.not. ctx_discretization%flag_eval_int_quadrature) then
      ctx_err%msg   = "** ERROR: quadrature must be used [create_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    
    else

      !! only works if we have a picewise constant parameter ...........
      if(trim(adjustl(ctx_model%format_disc)) .ne. tag_MODEL_PCONSTANT) then
        ctx_err%msg   = "** ERROR: inversion requires "//&
                        "piecewise-constant model [create_gradient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      
      select case(ctx_mesh%dim_domain)
        case(1)
          call create_gradient_quadrature_1D(ctx_discretization,        &
                               ctx_mesh,ctx_model,ctx_field_fwd%field,  &
                               ctx_field_bwd%field,angular_freq,        &
                               ctx_field_fwd%n_src,gradient,            &
                               tag_metric,ctx_err)
    
        case default
          ctx_err%msg   = "** ERROR: gradient for helio spherical " //  &
                          "equation for HDG needs dimension 1 "     //  &
                          "[create_gradient] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if
    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 
  
    return

  end subroutine create_gradient

end module m_create_gradient
