!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_utils_derivative.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix derivative for
!> ODE in Helio spherical symmetry using the HDG 
!> discretization:
!> We derive with respect to the physical parameter: 1/vp², alpha,
!> alpha'. Note that in acoustic isotropic, only the matrix A 
!> contains the model parameters
!
!------------------------------------------------------------------------------
module m_matrix_utils_derivative

  !! module used -------------------------------------------------------
  use m_raise_error,               only : raise_error, t_error
  use m_define_precision,          only : IKIND_MESH,RKIND_POL
  use m_ctx_domain,                only : t_domain
  use m_ctx_discretization,        only : t_discretization
  use m_lapack_inverse,            only : lapack_matrix_inverse
  use m_mesh_simplex_area,         only : mesh_area_simplex
  use m_ctx_model,                 only : t_model
  use m_model_eval,                only : model_eval_piecewise_constant,  &
                                          model_eval_piecewise_polynomial,&
                                          model_eval_sep
  use m_ctx_model_representation,  only : tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                          tag_MODEL_PCONSTANT, tag_MODEL_DOF
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: hdg_build_dA_1D_line
  public :: hdg_build_local_dmatrix_1D

  contains


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Create the local integral over the current cell to obtain 
  !> from [Pham et al. 2019] (i.e., the local element matrices)
  !>     T = \int r \phi_i \phi_j   +   r² \phi_i' \phi_j
  !>     Q = \int \phi_i F(r) \phi_j
  !>         with F(r) =  -kappa² r² + q r² + l*(l+1)
  !>         where
  !>                kappa = sigma / c 
  !>                    q = alpha²/4 + alpha'/2 + alpha/r
  !>     S = \int 2 \phi_i \phi_j + \int r \phi_i \phi_j'
  !>     M = \int r \phi_i \phi_j
  !
  !> @param[in]    ctx_dg          : DG context
  !> @param[in]    ctx_mesh        : mesh context
  !> @param[in]    i_cell          : current cell
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_local_dmatrix_1D(ctx_dg,ctx_mesh,model,          &
                                       i_cell,i_o_cell,freq,            &
                                       vol_matrix_dQe,                  &
                                       vol_matrix_phi_phi,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    complex(kind=8)                    ,intent(in)    :: freq
    integer                            ,intent(in)    :: i_o_cell
    !! these are the matrices that are computed here, only the derivative
    !! with respect to the model parameters
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_matrix_dQe(:,:,:)
    !! only used later for gradient metric.
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_matrix_phi_phi(:,:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local 
    integer                          :: k, i, j
    integer                          :: n_dof
    !! models
    real   (kind=8)                  :: v,a,da,g
    complex(kind=8)                  :: omega,sigma
    complex(kind=8)     ,allocatable :: dQ(:,:)
    !! -----------------------------------------------------------------
    real(kind=8)                     :: jac1d(1,1),bval(1),pt_loc
    real(kind=8)                     :: node_coo(1,2)
    integer                          :: iquad
    complex(kind=RKIND_POL)          :: quad_phi_phi_x2_dQ(4)
    real   (kind=RKIND_POL)          :: quad_phi_phi
    
    ctx_err%ierr=0

    vol_matrix_dQe     = 0.d0
    vol_matrix_phi_phi = 0.d0
    
    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then 
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! evaluate the model parameters in all quadrature points,
    !! we need alpha and the wavenumber (that depends on vp, etc)

    !! We have already the frequency times i, that is 
    !!     i \sigma = i\omega -   \laplace
    !! but we want
    !!     \sigma =  \omega + i \laplace
    !! => we multiply by -i
    !! -----------------------------------------------------------------      
    omega = - dcmplx(0.,1.)*freq !! possibility for complex freq ?
    allocate(dQ(ctx_dg%quadGL_npts,4))
    dQ = 0.d0

    select case(trim(adjustl(model%format_disc)))
      case(tag_MODEL_PCONSTANT)
        !! evaluate the models at this cell
        call model_eval_piecewise_constant(model,i_cell,freq,v,a,da,g,ctx_err)

        !! sigma: complex frequency
        sigma = dcmplx(sqrt(1.d0 + 2.d0*cmplx(0.,1.)*g/omega) * omega)
        !! q = alpha²/4 + alpha'/2 + alpha/r
        !! -------------------------------------------------------------
        !! we then use k² so it must be taken as Q with
        !! 
        !!  Q/r² = -wavenum²  + alpha/r + l(l+1)/r²
        !!
        !!       = -sigma²/c² + alpha²/4 + alpha'/2 + alpha/r + l(l+1)/r²
        !! -------------------------------------------------------------
        !! THE MINUS IS NOT SET BELOW WHEN WE ASSEMBLE Qe MATRIX. 
        !! CONTRARY TO THE MODELING SUBROUTINE.
        dQ(:,1) =-sigma**2                              !! 1/vp² 
        dQ(:,2) = a/2.d0   !! add later the r part      !! alpha
        dQ(:,3) = 1.d0/2.d0                             !! alpha'
        dQ(:,4) =-2.d0*dcmplx(0.d0,1.d0)*omega          !! gamma
 
      case default
        ctx_err%msg   = "** ERROR: one must use the piecewise-constant"//&
                        " model representation for inversion "         //&
                        "[matrix_utils_derivative] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select

    if(ctx_err%ierr .ne. 0) return !! non-shared error
    
    !! nodes coordinates    
    node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))

    !! using quadrature rule >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! we need the current cell map from ref element to local one
    !! we have jac1D * (x_ref) + (x)_node1 = (x)_local
    jac1d(:,1) = (node_coo(:,2)-node_coo(:,1))
    bval(:)    =  node_coo(:,1)

    !! -----------------------------------------------------------------
    !! Create local matrices, the volumic matrices only need the 
    !! cell order, the faces matrix must have the combinations though
    !! -----------------------------------------------------------------
    k = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)

    do i=1, n_dof ; do j=1, n_dof
      quad_phi_phi_x2_dQ = 0.0
      quad_phi_phi       = 0.0

      do iquad=1,ctx_dg%quadGL_npts
        ! -----------------------------------------------------
        !! we evaluate the point of interest in the local element
        !! 
        ! -----------------------------------------------------
        !! we compute the evaluation of the function times the weight
        pt_loc  = jac1d(1,1) * dble(ctx_dg%quadGL_pts(1,iquad)) + bval(1)

        !! \integral   \phi \phi
        !! \integral x \phi \phi
        !! \integral x²\phi \phi
        !! the precomputation already has the weight.                            
        quad_phi_phi   = quad_phi_phi   +                               &
                         ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)

        !! the derivatives
        quad_phi_phi_x2_dQ(1) = quad_phi_phi_x2_dQ(1)   +               &
                                dQ(iquad,1)*(pt_loc**2) *               &
                                ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        
        !! alpha needs to add the 1/r part too.
        quad_phi_phi_x2_dQ(2) = quad_phi_phi_x2_dQ(2)   +               &
                                (dQ(iquad,2)*(pt_loc**2) + pt_loc) *    &
                                ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad) 

        quad_phi_phi_x2_dQ(3) = quad_phi_phi_x2_dQ(3)   +               &
                                dQ(iquad,3)*(pt_loc**2) *               &
                                ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        quad_phi_phi_x2_dQ(4) = quad_phi_phi_x2_dQ(4)   +               &
                                dQ(iquad,4)*(pt_loc**2) *               &
                                ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)

      end do
      
      !! adjust with dimensions 
      quad_phi_phi         = quad_phi_phi       * ctx_dg%det_jac(i_cell)
      quad_phi_phi_x2_dQ   = quad_phi_phi_x2_dQ * ctx_dg%det_jac(i_cell)


      ! Mass matrix
      vol_matrix_phi_phi(i,j) = vol_matrix_phi_phi(i,j) + &
                                real(quad_phi_phi,kind=RKIND_POL)

      ! ---------------------------------------------------------
      !!     Q = \int \phi_i F(r) \phi_j
      !!         with F(r) = -k²r² + \alpha r + l(l+1)
      vol_matrix_dQe(i,j,1) = cmplx(quad_phi_phi_x2_dQ(1),kind=RKIND_POL)
      vol_matrix_dQe(i,j,2) = cmplx(quad_phi_phi_x2_dQ(2),kind=RKIND_POL)
      vol_matrix_dQe(i,j,3) = cmplx(quad_phi_phi_x2_dQ(3),kind=RKIND_POL)
      vol_matrix_dQe(i,j,4) = cmplx(quad_phi_phi_x2_dQ(4),kind=RKIND_POL)

    enddo ; enddo

    deallocate(dQ)

    return

  end subroutine  hdg_build_local_dmatrix_1D

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix dA for HDG in ODE in HELIO 
  !> in 1D based upon segment discretization 
  !
  !> @param[inout] ctx_dg         : type DG
  !> @param[in]    vol_matrix_    : current reference matrix derivative
  !> @param[inout] dA             : A derivative
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[inout] ctx_err        : context error
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_dA_1D_line(matrixdQk,ndof_vol,dA,ctx_err)

    implicit none

    complex(kind=RKIND_POL)          ,intent(in)   :: matrixdQk(:,:,:)
    complex(kind=8), allocatable     ,intent(inout):: dA(:,:,:)
    integer                          ,intent(in)   :: ndof_vol
    type(t_error)                    ,intent(inout):: ctx_err
    !!   
    integer      :: i, j
    
    ctx_err%ierr=0
    dA=0.d0 
    
    !! -----------------------------------------------------------------
    !! LOCAL element already, we have: [Pham et al., 2019]
    !! 
    !!        (-T_k      Q_k + \tau_k R_k )
    !!  A_k = (                           )
    !!        ( M_k       S_k              )
    !! -------------------------------------
    !! there are four derivatives, for each of the parameter.
    !! 1/vp² ; alpha ; alpha' ; attenuation.
    !! -----------------------------------------------------------------
    do i=1, ndof_vol
      do j=1, ndof_vol
        dA(i, j+ndof_vol,1) = dcmplx( matrixdQk(i,j,1))
        dA(i, j+ndof_vol,2) = dcmplx( matrixdQk(i,j,2))
        dA(i, j+ndof_vol,3) = dcmplx( matrixdQk(i,j,3))
        dA(i, j+ndof_vol,4) = dcmplx( matrixdQk(i,j,4))
      end do
    end do

    return
  end subroutine hdg_build_dA_1D_line

end module m_matrix_utils_derivative
