!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_gradient_quadrature.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the gradient of the cost function w.r.t
!> the physical coefficient, assuming piecewise constant representation
!> (1 value per cell). 
!
!------------------------------------------------------------------------------
module m_create_gradient_quadrature

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: RKIND_MAT,RKIND_MESH,IKIND_MESH
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model, compute_dgamma0_powerlaw
  use m_model_eval,             only: model_eval_piecewise_constant
  use m_mesh_simplex_area,      only: mesh_area_simplex
  use m_grad_metric,            only: tag_GRADMETRIC_ID,                &
                                tag_GRADMETRIC_AREA,tag_GRADMETRIC_MASS,&
                                tag_GRADMETRIC_MASS_INV
  use m_model_list,             only: model_VP
  use m_matrix_utils_derivative,only: hdg_build_dA_1D_line,             &
                                      hdg_build_local_dmatrix_1D
  !! reference matrices must be computed on the fly because
  !! the physical parameter cannot be taken out of the integral
  use m_array_types,            only: t_array2d_real, t_array3d_complex,&
                                      array_deallocate, array_allocate
  implicit none

  private
  public  :: create_gradient_quadrature_1D

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the gradient of the misfit, with respect to the physical
  !> parameters for HDG in the context of the ODE fo helio under spherical
  !> symmetry, i.e., one-dimensional only.
  !> grad(:,:,1) = dA / d{1/vp²}
  !> grad(:,:,2) = dA / d{\alpha}
  !> grad(:,:,3) = dA / d{\alpha'}
  !> if viscosity
  !> grad(:,:,4) = dA / d{\gamma}
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_bwd         : backward wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] gradient          : gradient arrays
  !> @param[in]    tag_metric        : gradient metric
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_quadrature_1D(ctx_dg,ctx_mesh,ctx_model,      &
                                           field_fwd,field_bwd,freq,n_shot,&
                                           gradient,tag_metric,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd (:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_bwd (:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: gradient(:,:)
    integer                            ,intent(in)    :: tag_metric
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell,ic
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2
    integer                     :: iloc1,iloc2,n,idof,isol,icell_src
    integer                     :: n_param,m,n_dof
    complex(kind=8),allocatable :: sol_fwd_loc(:),sol_bwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8)             :: gradval(4),u
    real   (kind=8)             :: dgamm0
    !! complex because of k
    type(t_array3d_complex),allocatable :: vol_matrix_dQe(:)
    type(t_array2d_real)   ,allocatable :: vol_matrix_phi_phi(:)
    !! for metric only
    integer                     :: lwork,jdof,ierr
    integer        ,allocatable :: ipiv(:)
    real   (kind=8)             :: area
    real(kind=RKIND_MESH),allocatable :: coo_node(:,:)
    complex(kind=8),allocatable :: metric_weight(:,:),sol_bwd_weigh(:)
    complex(kind=8),allocatable :: work(:)
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! we compute size max so that we have only ONE allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    !! local array, the 4 is for the three physical parameters
    !!              the 3 is for the three triangle faces
    n_param = 4
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,n_param))
    allocate(dAU          (dim_sol*ndof_max_vol,n_param))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_weigh(dim_sol*ndof_max_vol))
    allocate(metric_weight(dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(work         (dim_sol*ndof_max_vol))
    allocate(ipiv         (dim_sol*ndof_max_vol))
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))

    !! -----------------------------------------------------------------
    !! using quadrature rules
    allocate(vol_matrix_phi_phi(ctx_dg%n_different_order))
    allocate(vol_matrix_dQe    (ctx_dg%n_different_order))
    do m = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order (m)
      !! size depends if derivative or not
      call array_allocate(vol_matrix_dQe    (m),n_dof,n_dof,4,ctx_err)
      call array_allocate(vol_matrix_phi_phi(m),n_dof,n_dof,ctx_err)
    end do
    
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,ic,i_order_cell),      &
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,n,idof),&
    !$OMP&         PRIVATE(isol,icell_src,m,n_dof,sol_fwd_loc),         &
    !$OMP&         PRIVATE(sol_bwd_loc,dA,dAU,gradval,u),               &
    !$OMP&         PRIVATE(vol_matrix_dQe,vol_matrix_phi_phi,lwork),    &
    !$OMP&         PRIVATE(jdof,ierr,ipiv,area,coo_node,metric_weight), &
    !$OMP&         PRIVATE(sol_bwd_weigh,work)
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      if(ctx_err%ierr .ne. 0) cycle

      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      n            = ndof_vol*dim_sol

      !! compute the matrix derivatives using the quadrature rule
      vol_matrix_phi_phi(i_order_cell)%array = 0.0
      vol_matrix_dQe    (i_order_cell)%array = 0.0
      call hdg_build_local_dmatrix_1D(ctx_dg,ctx_mesh,ctx_model,icell,  &
                     i_order_cell,freq,                                 &
                     vol_matrix_dQe(i_order_cell)%array,                &
                     vol_matrix_phi_phi(i_order_cell)%array,ctx_err)
      if(ctx_err%ierr .ne. 0) cycle

      ! ----------------------------------------------------------------
      !! Compute the appropriate weighting matrix in case we 
      !! do not have the identity metric for inner product
      ! ----------------------------------------------------------------
      !! THE MATRIX MUST BE SYMMETRIC POSITIVE DEFINITE
      ! ----------------------------------------------------------------
      metric_weight = 0.0
      select case(tag_metric)
        !! identity matrix ---------------------------------------------
        case(tag_GRADMETRIC_ID       )
          do idof=1,n
            metric_weight(idof,idof) = 1.0
          end do

        !! identity matrix with 1./area on the diagonal ----------------
        case(tag_GRADMETRIC_AREA     )
          coo_node(:,:)=ctx_mesh%x_node(:,ctx_mesh%cell_node(:,icell))
          call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)
          do idof=1,n
            metric_weight(idof,idof) = 1./area
          end do

        !! mass matrix -------------------------------------------------
        case(tag_GRADMETRIC_MASS,tag_GRADMETRIC_MASS_INV)
          do idof=1,ndof_vol ; do jdof=1,ndof_vol
            u = dcmplx(vol_matrix_phi_phi(i_order_cell)%array(idof,jdof) * &
                       ctx_dg%det_jac(icell))
            do isol=1,dim_sol
              metric_weight(idof+(isol-1)*ndof_vol, &
                            jdof+(isol-1)*ndof_vol) = u
            end do
          end do ; enddo
          if(tag_metric == tag_GRADMETRIC_MASS_INV) then
            !! inverse of M --------------------------------------------
            lwork=n
            call ZGETRF(n,n,metric_weight(1:n,1:n),n,ipiv,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg   = "** ERROR during ZGETRF in HDG create grad"
              cycle
            end if
            call ZGETRI(n,metric_weight(1:n,1:n),n,ipiv,work,lwork,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg  = "** ERROR during ZGETRI in HDG create grad"
              cycle
            end if
          end if
        !! stiffness matrix --------------------------------------------
        ! ---------------------------------------------------------
        !! does not provide a symmetric definitive positive
        !! matrix
        ! ---------------------------------------------------------
        !! error -------------------------------------------------------        
        case default
          ctx_err%msg   = "** ERROR: Unrecognized grad. metric [create_grad]**"
          ctx_err%ierr  = -1
      end select
      ! ----------------------------------------------------------------


      ! ----------------------------------------------------------------
      !! loop over all shots--------------------------------------------
      do i_src = 1, n_shot
        
        if(ctx_err%ierr .ne. 0) cycle
        
        !! multipler offset
        sol_fwd_loc  =0.0
        sol_bwd_loc  =0.0
        sol_bwd_weigh=0.0
        dA           =0.0
        dAU          =0.0

        ! ---------------------------------------------------------
        !! get the local solution
        igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
                    ctx_dg%offsetdof_sol_loc(icell)  + 1)
        igb2 = igb1 + ndof_vol - 1
        do isol=1,dim_sol
          iloc1 = (isol-1)*ndof_vol + 1
          iloc2 = iloc1  + ndof_vol - 1
          sol_fwd_loc(iloc1:iloc2) = dcmplx(field_fwd(igb1:igb2,isol))
          sol_bwd_loc(iloc1:iloc2) = dcmplx(field_bwd(igb1:igb2,isol))
        end do
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !! the gradient is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell
        call hdg_build_dA_1D_line(vol_matrix_dQe(i_order_cell)%array,   &
                                  ndof_vol,dA,ctx_err)
        !!> dC = 0 

        !! compute dA U, for all parameters: 1/vp² ; alpha ; alpha' ; gamma
        !! -------------------------------------------------------------
        dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))
        dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))
        dAU(1:n,3) = matmul(dA(1:n,1:n,3),sol_fwd_loc(1:n))
        dAU(1:n,4) = matmul(dA(1:n,1:n,4),sol_fwd_loc(1:n))

        !! apply metric onto the backward field 
        sol_bwd_weigh(1:n) = matmul(metric_weight(1:n,1:n),sol_bwd_loc(1:n))

        gradval   = 0.0
        do idof=1,n !! loop over all ndof * dim_solution
          !! gradient values
          gradval(1) = gradval(1) + conjg(dAU(idof,1))*sol_bwd_weigh(idof)
          gradval(2) = gradval(2) + conjg(dAU(idof,2))*sol_bwd_weigh(idof)
          gradval(3) = gradval(3) + conjg(dAU(idof,3))*sol_bwd_weigh(idof)
        end do
        !! average
        gradval    = dcmplx(gradval / dble(ndof_vol))
       
        !! in the case of viscosity
        if(ctx_model%flag_viscous) then

          ! ---------------------------------------------------------
          if(ctx_err%ierr .ne. 0) cycle
          
          select case(trim(adjustl(ctx_model%viscous_model)))

            case('gamma','gamma0_power-law')
              do idof=1,n !! loop over all ndof * dim_solution
                gradval(4) = gradval(4) +                               &
                             conjg(dAU(idof,4))*sol_bwd_weigh(idof)
              end do
              gradval(4)   = dcmplx(gradval(4) / dble(ndof_vol))
              
              if(ctx_err%ierr .ne. 0) cycle

              !! for power law, adjust with a constant 
              if(trim(adjustl(ctx_model%viscous_model)) .eq.            &
                 'gamma0_power-law') then
                !! here we have 
                !! \gamma = 2 pi \gamm0 \abs[ \omega / \omega0 ]^\beta
                !! so the same as above, but multiplied by a constant. 
                dgamm0     = compute_dgamma0_powerlaw(freq)
                gradval(4) = gradval(4) * dgamm0
              end if

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [grad]"
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
          end select

        end if
        ! ---------------------------------------------------------
        !! save value, we sum over the shots here
        gradient(icell,1) = gradient(icell,1)+real(gradval   (1),kind=4)
        gradient(icell,2) = gradient(icell,2)+real(gradval   (2),kind=4)
        gradient(icell,3) = gradient(icell,3)+real(gradval   (3),kind=4)
        if(ctx_model%flag_viscous) then
          gradient(icell,4) = gradient(icell,4)+real(gradval (4),kind=4)
        end if
        ! ---------------------------------------------------------
      end do !! end loop over shots
    end do !! end loop over cells
    !$OMP END DO
    !$OMP END PARALLEL

    !! quadrature arrays
    do m = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_matrix_dQe    (m))
      call array_deallocate(vol_matrix_phi_phi(m))
    end do
    deallocate(vol_matrix_dQe)
    deallocate(vol_matrix_phi_phi)


    deallocate(dA         )
    deallocate(sol_fwd_loc)
    deallocate(sol_bwd_loc)
    deallocate(sol_bwd_weigh)
    deallocate(dAU        )
    deallocate(coo_node   )
    deallocate(work       )
    deallocate(ipiv       )
    deallocate(metric_weight)

    return
  end subroutine create_gradient_quadrature_1D

end module m_create_gradient_quadrature
