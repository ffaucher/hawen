!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_gradient_refelem.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the gradient of the cost function w.r.t
!> the physical coefficient, assuming piecewise constant representation
!> (1 value per cell). It returns an error as the helio ODE in spherical
!> symmetry requires the quadrature rules.
!
!------------------------------------------------------------------------------
module m_create_gradient_refelem

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error

  implicit none

  private
  public  :: create_gradient_refelem

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> It returns an error as the helio ODE in spherical
  !> symmetry requires the quadrature rules.
  !>
  !
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_refelem(ctx_err)

    implicit none

    type(t_error)                      ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------

    ctx_err%msg   = "** ERROR: quadrature must be used to create the"// &
                    " gradient for helio ode with spherical symmetry"// &
                    " [create_gradient] **"
    ctx_err%ierr  = -1
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    
    return
  end subroutine create_gradient_refelem

end module m_create_gradient_refelem
