!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_pseudoh.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the Gauss-Newton pseudo-hessian: 
!>         J(m) = 1/2 || Ru - d ||²
!>   dmJ    = dm(u)* R* (Ru - d)
!>   d²m(J) = dm(u)* R* R dm(u)  + d²m(u)* R* (Ru - d).
!> Gauss-Newton assumes that d²m(u) = 0
!>   HGN  = dm(u)* R* R dm(u)
!>
!> we replace with dm(u) = - P^{-1} dm(P) u to obtain  
!>   HGN  = (P^{-1} dm(P) u)* R* R (P^{-1} dm(P) u)
!>   HGN  = u* dm(P)* P^{-*} R* R P^{-1} dm(P) u
!>
!> The pseudo-Hessian assumes that P^{-*} R* R P^{-1} = Identity:
!> PSEUDO-HESSIAN = u* dm(P)* dm(P) u
!>
!> Remark that it also depends on the choice of cost function, which
!>        we consider outside of this routine. 
!
!------------------------------------------------------------------------------
module m_create_pseudoh

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: RKIND_MAT,IKIND_MESH
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model
  use m_ctx_field,              only: t_field

  implicit none

  private
  public  :: create_pseudoh

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create pseudoh
  !>
  !
  !> @param[in]    ctx_paral         : parallel context
  !> @param[in]    ctx_mesh          : mesh grid type
  !> @param[in]    ctx_model         : model parameters
  !> @param[in]    ctx_discretization: type DG for reference matrix
  !> @param[in]    ctx_field_fwd     : forward field   
  !> @param[in]    field_fwd         : forward field depends on misfit
  !> @param[in]    field_scale       : scaling
  !> @param[in]    frequency         : frequency
  !> @param[inout] pseudoh           : output computed pseudoh
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_pseudoh(ctx_paral,ctx_mesh,ctx_discretization,      &
                             ctx_model,ctx_field_fwd,angular_freq,      &
                             pseudoh,ctx_err)
    implicit none

    type(t_parallelism)                ,intent(in)    :: ctx_paral
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_discretization)             ,intent(in)    :: ctx_discretization
    type(t_model)                      ,intent(in)    :: ctx_model
    type(t_field)                      ,intent(in)    :: ctx_field_fwd
    complex(kind=8)                    ,intent(in)    :: angular_freq
    real   (kind=4)        ,allocatable,intent(inout) :: pseudoh (:,:)
    type(t_error)                      ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    pseudoh      = 0.0

    !! only to avoid warning at compilation
    if(ctx_mesh%n_cell_gb < 0 .or. ctx_model%n_model < 0 .or.           &
       real(angular_freq) < 0 .or. allocated(ctx_field_fwd%field)) then
       !! avoid compilation warning
    end if

    !! -----------------------------------------------------------------
    !! create the pseudo-hessian depending on
    !!  - the dimension
    !!  - the model representation
    !!  - the method (quadrature / reference element)
    !! not every combination are compatible. 
    !! -----------------------------------------------------------------
    if(ctx_discretization%flag_eval_int_quadrature) then
      ctx_err%msg   = "** ERROR: the creation of the pseudo-hessian "// &
                      "is not ready yet [create_pseudoH] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    
    else
      ctx_err%msg   = "** ERROR: the helio ODE requires the use of " // &
                      "the quadrature method for integrals [create_pseudoH] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)

    end if
    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 

    return

  end subroutine create_pseudoh
end module m_create_pseudoh
