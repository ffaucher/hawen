!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_update.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the routine to update the model depending on 
!> the parametrization for ODE in HELIO SPHERICAL SYM.
!
!------------------------------------------------------------------------------
module m_model_update

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_get_piecewise_constant
  use m_model_list,             only: model_VP, model_GAMMA, model_GAMMA0, &
                                      model_ALPHA, model_DALPHA
  use m_ctx_model_representation,                                       &
                                only : tag_MODEL_PPOLY, tag_MODEL_SEP,  &
                                       tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_grad_model_function,    only: apply_model_function_inv,         &
                                      apply_model_function
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: model_update

  contains


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for SPHERICAL ODE in HELIO, at the end we give back the 
  !> (cp,alpha,dalpha).
  !> 
  !> we can work with 
  !>   vp, alpha, dalpha and the attenuation gamma.
  !> Here, we update accordingly the selected
  !> parameter, respecting given min and max values
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[in]    freq           : current frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_update(ctx_paral,ctx_model,search,alpha,param_current, &
                          tag_function,parametrization,freq,ctx_err)
    implicit none
   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case(trim(adjustl(ctx_model%format_disc)))
      case(tag_MODEL_PCONSTANT) 
        call model_update_pconstant(ctx_paral,ctx_model,search,alpha,   &
                                    param_current,tag_function,         &
                                    parametrization,freq,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: only piecewise-constant model " //&
                        " supported [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return

  end subroutine model_update


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for SPHERICAL ODE in HELIO, using piecewise-constant representation
  !> for (cp,alpha,dalpha) and the attenuation.
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[in]    freq           : current frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_update_pconstant(ctx_paral,ctx_model,search,alpha,   &
                                    param_current,tag_function,         &
                                    parametrization,freq,ctx_err)
    implicit none
   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ic, n_coeff_loc
    real(kind=4),allocatable  :: model(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0


    !! piecewise-constant representation *******************************
    n_coeff_loc = ctx_model%n_cell_loc
    !! *****************************************************************
    allocate(model(n_coeff_loc))
    model = 0.0
    !! -----------------------------------------------------------------
    !! 1) we get the parameter to be update
    !! PIECEWISE-CONSTANT **********************************************
    call model_get_piecewise_constant(ctx_model,model,                  & 
                                      trim(adjustl(param_current)),freq,ctx_err)
    call apply_model_function(ctx_paral,model,tag_function,ctx_err)

    !! 2) we update model function
    model(:) = model(:) - alpha * search(:)
    !! 3) we get the identity model back
    call apply_model_function_inv(ctx_paral,model,tag_function,ctx_err)

    !! -----------------------------------------------------------------
    !! 4) 
    !! -----------------------------------------------------------------
    !! depending on the parametrization we need to update
    !! the ctx_model which contains Vp, alpha and dalpha
    !!
    !! -----------------------------------------------------------------

                !***************************************!
                ! Everything is piecewise-constant here !
                !                                       !
                !***************************************!

    !! if we update the attenuation, we can leave now 
    if (trim(adjustl(param_current)) .eq. model_GAMMA .or.              &
        trim(adjustl(param_current)) .eq. model_GAMMA0) then
      ctx_model%gamm%pconstant(:) = model(:)
    
    !! otherwize it depends on the parametrization
    else
      ! ----------------------------------------------------------------
      !! parametrization Vp, Alpha, Alpha' 
      if(any(parametrization == model_VP)    .and.                      &
         any(parametrization == model_ALPHA) .and.                      &
         any(parametrization == model_DALPHA))  then
        select case(trim(adjustl(param_current)))
          case(model_VP)
            ctx_model%vp%pconstant(:)     = model
          case(model_ALPHA, model_DALPHA)
            ctx_err%msg   = "** ERROR: We are not able to reconstruct"//& 
                            " alpha and/or alpha' for now, due to "   //&
                            "inherent dependency [model_update] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)

          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! Error
      else
        ctx_err%msg   = "** ERROR: Unrecognized parametrization name [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    ! ----------------------------------------------------------------
    deallocate(model)

    !! in case of error ------------------------------------------------
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: Unrecognized parametrization [model_update] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 

    !! if everything is ok so far, we have to adjust min and max values
    !! -----------------------------------------------------------------
    do ic = 1, n_coeff_loc
      if(ctx_model%vp%pconstant(ic)   < ctx_model%vp_min)               &
         ctx_model%vp%pconstant(ic)   = ctx_model%vp_min
      if(ctx_model%vp%pconstant(ic)   > ctx_model%vp_max)               &
         ctx_model%vp%pconstant(ic)   = ctx_model%vp_max
      if(ctx_model%alpha%pconstant(ic)< ctx_model%alpha_min)            &
         ctx_model%alpha%pconstant(ic)= ctx_model%alpha_min
      if(ctx_model%alpha%pconstant(ic)> ctx_model%alpha_max)            &
         ctx_model%alpha%pconstant(ic)= ctx_model%alpha_max
      if(ctx_model%dalpha%pconstant(ic)< ctx_model%dalpha_min)          &
         ctx_model%dalpha%pconstant(ic)= ctx_model%dalpha_min
      if(ctx_model%dalpha%pconstant(ic)> ctx_model%dalpha_max)          &
         ctx_model%dalpha%pconstant(ic)= ctx_model%dalpha_max
    end do

    if(ctx_model%flag_viscous) then
      do ic = 1, n_coeff_loc
        if(ctx_model%gamm%pconstant(ic)< ctx_model%gamma_min) &
           ctx_model%gamm%pconstant(ic) =ctx_model%gamma_min
        if(ctx_model%gamm%pconstant(ic)> ctx_model%gamma_max) &
           ctx_model%gamm%pconstant(ic) =ctx_model%gamma_max
      end do
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine model_update_pconstant
end module m_model_update
