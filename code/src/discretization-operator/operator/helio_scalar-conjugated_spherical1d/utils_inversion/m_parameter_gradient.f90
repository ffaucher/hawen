!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_parameter_gradient.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the routine to convert the reference gradient
!> towards the gradient w.r.t the wanted physical parameters for the
!> ODE in HELIO SPHERICAL SYM.
!
!------------------------------------------------------------------------------
module m_parameter_gradient

  !! module used -------------------------------------------------------
  use m_define_precision,       only: IKIND_MESH
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_piecewise_constant
  use m_model_list,             only: model_VP, model_ALPHA,            &
                                      model_DALPHA, model_GAMMA, model_GAMMA0
  use m_ctx_model_representation,                                       &
                                only : tag_MODEL_PPOLY, tag_MODEL_SEP,  &
                                       tag_MODEL_PCONSTANT, tag_MODEL_DOF
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: parameter_gradient

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical parameter
  !> for the ODE in HELIO SPHERICAL SYM propagation
  !> The input gradient is given w.r.t:
  !>  a) vp
  !>  b) alpha
  !>  c) dalpha
  !>  d) atenuation
  !! -----------------------------
  !> for the derivative we have the
  !>  \f$ \partial_m1 \tilde{J} (m1, m2) =\partial_V J \partial_{m1} V 
  !>    + \partial_\alpha J \partial_{m1} \alpha
  !>    + \partial_{\alpha'} J \partial_{m1} \alpha'  \f$
  !>
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    freq           : angular frequency
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_pseudoh : pseudo-hessian need power squared
  !----------------------------------------------------------------------------
  subroutine parameter_gradient(ctx_model,grad_in,grad_out,str_model,   &
                                freq,ctx_err,o_flag_pseudoh)
    implicit none

    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    logical         ,optional          ,intent(in)   :: o_flag_pseudoh
    !! 
    integer    :: power
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(abs(freq) > tiny(1.0)) then
      !! frequency is not needed here, we just avoid warning at 
      !! compilation
    end if 

    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power = 2
    end if
    !! 
    select case (trim(adjustl(ctx_model%format_disc))) 
      case(tag_MODEL_PCONSTANT) 
        call parameter_gradient_pconstant(ctx_model,grad_in,grad_out,   &
                                          str_model,power,ctx_err)      
      case default
        ctx_err%msg   = "** ERROR: only piecewise-constant is "  //     &
                        "supported to parametrize the gradient " //     &
                        " [parameter_gradient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine parameter_gradient
    
    
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical parameter
  !> for the ODE in HELIO SPHERICAL SYM propagation
  !> The input gradient is given w.r.t:
  !>  a) vp
  !>  b) alpha
  !>  c) dalpha
  !>  d) atenuation
  !! -----------------------------
  !> for the derivative we have the
  !>  \f$ \partial_m1 \tilde{J} (m1, m2) =\partial_V J \partial_{m1} V 
  !>    + \partial_\alpha J \partial_{m1} \alpha
  !>    + \partial_{\alpha'} J \partial_{m1} \alpha'  \f$
  !>
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    power          : power = 2 if pseudo-hessian
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parameter_gradient_pconstant(ctx_model,grad_in,grad_out,   &
                                          str_model,power,ctx_err)
    implicit none

    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    integer                            ,intent(in)   :: power
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)              :: vp
    integer                   :: k,n_coeff_loc
    integer(kind=IKIND_MESH)  :: c
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(ctx_model%dim_domain .ne. 1) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helio spherical equation in acoustic medium "//  &
                      "[parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! *****************************************************************
    !! USING PIECEWISE-CONSTANT MODEL REPRESENTATION 
    !! *****************************************************************
    n_coeff_loc = ctx_model%n_cell_loc
    !! *****************************************************************

    !! -----------------------------------------------------------------
    !! if we want (vp,alpha,dalpha) then we can leave it is ok
    !! only convert 1/vp² which has been computed
    if(any(str_model == model_VP) .and. any(str_model == model_ALPHA)   &
      .and. any(str_model == model_DALPHA)) then
      
      !! we already have the gradient then
      do k=1,ctx_model%n_model
        if(ctx_err%ierr .ne. 0) cycle !! non-shared error
        
        select case(trim(adjustl(str_model(k))))
          case(model_VP)
            !! we only convert 1/vp² to vp
            do c=1,ctx_model%n_cell_loc !! piecewise-constant here 
              if(ctx_err%ierr .ne. 0) cycle !! non-shared error

              !! we use the RAW models 
              call model_eval_piecewise_constant(ctx_model,model_VP,c,  &
                                                 vp,ctx_err)
              if(ctx_err%ierr .ne. 0) cycle
              !! (1/vp) ' = - vp'  / vp²
              !! (1/vp²)' = -2 vp' / vp^3
              grad_out(c,k) =real(grad_in(c,1) * (-2.d0/(vp)**3)**power)
            end do

          case(model_ALPHA)
            grad_out(:,k) = grad_in(:,2)
          case(model_DALPHA)
            grad_out(:,k) = grad_in(:,3)
          case(model_GAMMA,model_GAMMA0)
            grad_out(:,k) = grad_in(:,4)

          case default
            ctx_err%msg   = "** ERROR: Unrecognized model name "    //  &
                            "[parameter_gradient] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end do

    !! -----------------------------------------------------------------
    !! else we do not know what to do.
    else
      ctx_err%msg   = "** ERROR: Unrecognized model parametrization"//  &
                      " [parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine parameter_gradient_pconstant

end module m_parameter_gradient
