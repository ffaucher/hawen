!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_eval.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to evaluate the model coefficients, in particular
!> to deal with piecewise-polynomials representation and with attenuation
!>
!
!------------------------------------------------------------------------------
module m_model_eval

  !! module used -------------------------------------------------------
  use m_raise_error,               only: raise_error, t_error
  use m_define_precision,          only: IKIND_MESH, RKIND_POL
  use m_ctx_domain,                only: t_domain
  use m_ctx_model,                 only: t_model, compute_gamma_powerlaw
  use m_ctx_model_representation,  only: model_representation_eval_sep
  use m_model_representation_dof,  only: model_representation_dof_eval_refelem
  use m_model_list
  use m_polynomial,                only: polynomial_eval, polynomial_eval_array
  use m_dg_lagrange_dof_coordinates,    only: discretization_dof_coo_ref_elem
  !! -------------------------------------------------------------------
  implicit none

  private

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_piecewise_constant
     module procedure model_eval_piecewise_constant_raw
     module procedure model_eval_piecewise_constant_all
  end interface
  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_piecewise_polynomial
     module procedure model_eval_piecewise_polynomial_raw
     module procedure model_eval_piecewise_polynomial_all
  end interface
  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_sep
     module procedure model_eval_sep_raw
     module procedure model_eval_sep_all
  end interface
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise constant representation
  interface model_get_piecewise_constant
     module procedure model_get_pconstant_real4
     module procedure model_get_pconstant_real8
  end interface
  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation for dof representation
  interface model_eval_dof_refelem
     module procedure model_eval_dof_raw_refelem_solo
     module procedure model_eval_dof_raw_refelem_multi
     module procedure model_eval_dof_all_refelem_solo
     module procedure model_eval_dof_all_refelem_multi
  end interface model_eval_dof_refelem
  interface model_eval_dof_index
     module procedure model_eval_dof_raw_solo_idof
  end interface model_eval_dof_index
  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model for dof representation
  interface model_get_pdof
     module procedure model_get_pdof_real4
     module procedure model_get_pdof_real8
  end interface
  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise polynomial representation
  interface model_get_piecewise_polynomial
     module procedure model_get_ppoly_real4
     module procedure model_get_ppoly_real8
  end interface
  !--------------------------------------------------------------------- 
  
  public  :: model_eval_piecewise_constant, model_eval_piecewise_polynomial
  public  :: model_get_piecewise_constant, model_get_piecewise_polynomial
  public  :: model_eval_dof_refelem, model_eval_dof_index
  public  :: model_get_pdof
  public  :: model_eval_sep
  
  contains
 
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-constant, here we just get
  !> the main, real models
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : which cell
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_constant_raw(ctx_model,strmodel,      &
                                               i_cell,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP    )
        out_val = dble(ctx_model%vp%pconstant(i_cell))
      case(model_ALPHA )
        out_val = dble(ctx_model%alpha%pconstant(i_cell))
      case(model_DALPHA)
        out_val = dble(ctx_model%dalpha%pconstant(i_cell))

      case(model_GAMMA,model_GAMMA0)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case('gamma','gamma0_power-law')
              out_val = ctx_model%gamm%pconstant(i_cell)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized visco. model [model_eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [model_eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if  

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! if(out_val < 0.d0) then !! note that some can be negative
    !   ctx_err%msg   = "** ERROR: value for the physical parameters " // &
    !                   "is <= 0 for the medium [model_eval] "
    !   ctx_err%ierr  = -1
    ! end if

    return

  end subroutine model_eval_piecewise_constant_raw

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-constant, here we get the  
  !> attenuation too
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    freq       : frequency might be needed for attenuation
  !> @param[out]   out_vp     : output value for vp
  !> @param[out]   out_alpha  : output value for alpha
  !> @param[out]   out_dalpha : output value for dalpha
  !> @param[out]   out_gamma  : output value for gamma
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_constant_all(ctx_model,i_cell,freq,   &
                                               out_vp,out_alpha,        &
                                               out_dalpha,out_gamma,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_alpha
    real   (kind=8)         ,intent(out)  :: out_dalpha
    real   (kind=8)         ,intent(out)  :: out_gamma 
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    out_vp    = dble(ctx_model%vp%pconstant    (i_cell))
    out_alpha = dble(ctx_model%alpha%pconstant (i_cell))
    out_dalpha= dble(ctx_model%dalpha%pconstant(i_cell))
    out_gamma = 0.d0

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        case('gamma') !! directly gamma
          out_gamma = ctx_model%gamm%pconstant(i_cell)
        case('gamma0_power-law') !! using the power law and the gamma0
          !! compute using the power law 
          out_gamma = compute_gamma_powerlaw(ctx_model%gamm%pconstant(i_cell),&
                                             freq)

        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model " //&
                          "[model_eval]"
          ctx_err%ierr  = -1
          return
      end select
    end if
      
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_vp .le. 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "vp is <= 0 for helio ODE medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_piecewise_constant_all


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a saved sep 
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_sep_raw(ctx_model,strmodel,xpt,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP    )
        call model_representation_eval_sep(ctx_model%vp%param_sep,      &
                                           ctx_model%dim_domain,xpt,    &
                                           out_val,ctx_err)
      case(model_ALPHA )
        call model_representation_eval_sep(ctx_model%alpha%param_sep,   &
                                           ctx_model%dim_domain,xpt,    &
                                           out_val,ctx_err)
      case(model_DALPHA)
        call model_representation_eval_sep(ctx_model%dalpha%param_sep,  &
                                           ctx_model%dim_domain,xpt,    &
                                           out_val,ctx_err)

      case(model_GAMMA,model_GAMMA0)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case('gamma','gamma0_power-law')
              call model_representation_eval_sep(ctx_model%gamm%param_sep, &
                                           ctx_model%dim_domain,xpt,       &
                                           out_val,ctx_err)

            case default
              ctx_err%msg   = "** ERROR: Unrecognized visco. model [model_eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [model_eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if  

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! if(out_val < 0.d0) then !! note that some can be negative
    !   ctx_err%msg   = "** ERROR: value for the physical parameters " // &
    !                   "is <= 0 for the medium [model_eval] "
    !   ctx_err%ierr  = -1
    ! end if

    return

  end subroutine model_eval_sep_raw

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation
  !
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_refelem_solo(ctx_model,i_cell,strmodel, &
                                               xpt,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
       call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,  &
                                                  xpt,out_val,ctx_err)
      case(model_ALPHA )
       call model_representation_dof_eval_refelem(ctx_model%alpha,      &
                                                  i_cell,xpt,out_val,   &
                                                  ctx_err)
      case(model_DALPHA)
       call model_representation_dof_eval_refelem(ctx_model%dalpha,     &
                                                  i_cell,xpt,out_val,   &
                                                  ctx_err)
      case(model_GAMMA,model_GAMMA0)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case('gamma','gamma0_power-law')
              call model_representation_dof_eval_refelem(               &
                                            ctx_model%gamm,i_cell,      &
                                            xpt,out_val,ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized visco. model [model_eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [model_eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if  

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
   
    !! no sanity check .................................................
    return

  end subroutine model_eval_dof_raw_refelem_solo
  
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation at a given index
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_solo_idof(ctx_model,i_cell,strmodel,    &
                                          i_dof,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    integer(kind=4)         ,intent(in)   :: i_dof
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
       out_val=ctx_model%vp%param_dof%mval(i_cell)%array(i_dof)
      case(model_ALPHA   )
       out_val=ctx_model%alpha%param_dof%mval(i_cell)%array(i_dof)
      case(model_DALPHA  )
       out_val=ctx_model%dalpha%param_dof%mval(i_cell)%array(i_dof)
      case(model_GAMMA,model_GAMMA0)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case('gamma','gamma0_power-law')
              out_val=ctx_model%gamm%param_dof%mval(i_cell)%array(i_dof)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized visco. model [model_eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [model_eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if  

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
   
    !! no sanity check .................................................
    return

  end subroutine model_eval_dof_raw_solo_idof


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation
  !
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_refelem_multi(ctx_model,i_cell,strmodel, &
                                              xpt,npt,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:,:)
    integer                 ,intent(in)   :: npt
    real   (kind=8)         ,intent(out)  :: out_val(:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    out_val = 0.d0 

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
       call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,  &
                                                  xpt,npt,out_val,ctx_err)
      case(model_ALPHA )
       call model_representation_dof_eval_refelem(ctx_model%alpha,      &
                                                  i_cell,xpt,npt,       &
                                                  out_val,ctx_err)
      case(model_DALPHA)
       call model_representation_dof_eval_refelem(ctx_model%dalpha,     &
                                                  i_cell,xpt,npt,       &
                                                  out_val,ctx_err)
      case(model_GAMMA,model_GAMMA0)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case('gamma','gamma0_power-law')
              call model_representation_dof_eval_refelem(               &
                                            ctx_model%gamm,i_cell,      &
                                            xpt,npt,out_val,ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized visco. model [model_eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [model_eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if  

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
   
    !! no sanity check .................................................
    return

  end subroutine model_eval_dof_raw_refelem_multi


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for the dof representation
  !
  !> @param[in]    ctx_mesh   : mesh context
  !> @param[in]    ctx_model  : model context
  !> @param[in]    xpt        : coordinates
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   out_vp     : output value
  !> @param[out]   out_rho    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_all_refelem_solo(ctx_model,i_cell,freq,xpt, &
                                             out_vp,out_alpha,          &
                                             out_dalpha,out_gamma,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    real   (kind=8)         ,intent(in)   :: xpt(:)
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_alpha
    real   (kind=8)         ,intent(out)  :: out_dalpha
    real   (kind=8)         ,intent(out)  :: out_gamma
    type(t_error)           ,intent(inout):: ctx_err
    !!
    real   (kind=8)                       :: gamma0
    !! -----------------------------------------------------------------

    call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,     &
                                               xpt,out_vp,ctx_err)
    call model_representation_dof_eval_refelem(ctx_model%alpha,i_cell,  &
                                               xpt,out_alpha,ctx_err)
    call model_representation_dof_eval_refelem(ctx_model%dalpha,i_cell, &
                                               xpt,out_dalpha,ctx_err)
    out_gamma = 0.d0

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        case('gamma') !! directly gamma
          call model_representation_dof_eval_refelem(ctx_model%gamm,    &
                                                     i_cell,xpt,        &
                                                     out_gamma,ctx_err)
      
        case('gamma0_power-law') !! using the power law and the gamma0
          call model_representation_dof_eval_refelem(ctx_model%gamm,    &
                                                     i_cell,xpt,        &
                                                     gamma0,ctx_err)
                                                     
          out_gamma = compute_gamma_powerlaw(gamma0,freq)
          
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model " //&
                          "[model_eval]"
          ctx_err%ierr  = -1
          return
      end select
    end if
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_vp .le. 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_dof_all_refelem_solo

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for the dof representation
  !
  !> @param[in]    ctx_mesh   : mesh context
  !> @param[in]    ctx_model  : model context
  !> @param[in]    xpt        : coordinates
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   out_vp     : output value
  !> @param[out]   out_rho    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_all_refelem_multi(ctx_model,i_cell,freq,    &
                                              xpt,npt,out_vp,out_alpha, &
                                              out_dalpha,out_gamma,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    real   (kind=8)         ,intent(in)   :: xpt(:,:)
    integer                 ,intent(in)   :: npt
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_vp(:)
    real   (kind=8)         ,intent(out)  :: out_alpha(:)
    real   (kind=8)         ,intent(out)  :: out_dalpha(:)
    real   (kind=8)         ,intent(out)  :: out_gamma(:)
    type(t_error)           ,intent(inout):: ctx_err
    !! 
    integer                               :: ipt
    real(kind=8)            ,allocatable  :: gamma0(:)
    !! -----------------------------------------------------------------

    call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,     &
                                               xpt,npt,out_vp,ctx_err)
    call model_representation_dof_eval_refelem(ctx_model%alpha,i_cell,  &
                                               xpt,npt,out_alpha,ctx_err)
    call model_representation_dof_eval_refelem(ctx_model%dalpha,i_cell, &
                                               xpt,npt,out_dalpha,ctx_err)
    out_gamma = 0.d0
    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        case('gamma') !! directly gamma
          call model_representation_dof_eval_refelem(ctx_model%gamm,    &
                                                     i_cell,xpt,npt,    &
                                                     out_gamma,ctx_err)
        case('gamma0_power-law') !! using the power law and the gamma0
          !! compute using the power law 
          allocate(gamma0(npt))
          call model_representation_dof_eval_refelem(ctx_model%gamm,    &
                                                     i_cell,xpt,npt,    &
                                                     gamma0,ctx_err)
          do ipt=1,npt
            out_gamma(ipt) = compute_gamma_powerlaw(gamma0(ipt),freq)
          end do
          deallocate(gamma0)

        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model " //&
                          "[model_eval]"
          ctx_err%ierr  = -1
          return
      end select
    end if

    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(minval(out_vp) .le. 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_dof_all_refelem_multi
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for sep saved
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    xpt        : pt
  !> @param[out]   out_vp     : output value
  !> @param[out]   out_rho    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_sep_all(ctx_model,xpt,freq,out_vp,out_alpha,    &
                                out_dalpha,out_gamma,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    real   (kind=8)         ,intent(in)   :: xpt(:)
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_alpha
    real   (kind=8)         ,intent(out)  :: out_dalpha
    real   (kind=8)         ,intent(out)  :: out_gamma 
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)    :: g

    call model_representation_eval_sep(ctx_model%vp%param_sep,          &
                                       ctx_model%dim_domain,xpt,out_vp,ctx_err)
    call model_representation_eval_sep(ctx_model%alpha%param_sep,       &
                                       ctx_model%dim_domain,xpt,        &
                                       out_alpha,ctx_err)
    call model_representation_eval_sep(ctx_model%dalpha%param_sep,      &
                                       ctx_model%dim_domain,xpt,        &
                                       out_dalpha,ctx_err)
    out_gamma = 0.d0

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        case('gamma') !! directly gamma
          call model_representation_eval_sep(ctx_model%gamm%param_sep,  &
                                             ctx_model%dim_domain,xpt,  &
                                             out_gamma,ctx_err)
        case('gamma0_power-law') !! using the power law and the gamma0
          !! compute using the power law 
          call model_representation_eval_sep(ctx_model%gamm%param_sep,  &
                                             ctx_model%dim_domain,xpt,  &
                                             g,ctx_err)
          out_gamma = compute_gamma_powerlaw(g,freq)

        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model " //&
                          "[model_eval]"
          ctx_err%ierr  = -1
          return
      end select
    end if
    return

  end subroutine model_eval_sep_all

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-polynomial, here we just get
  !> the main, real models
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : which cell
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_polynomial_raw(ctx_model,strmodel,i_cell, &
                                                 xpt,out_val,ctx_err)
    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:)
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !!
    real(kind=RKIND_POL)                :: v
    real(kind=RKIND_POL), allocatable   :: x(:)
    !! 
    !! -----------------------------------------------------------------

    allocate(x(ctx_model%dim_domain))
    x = real(xpt,kind=RKIND_POL)

    out_val = 0.d0

    select case(trim(adjustl(strmodel)))
      case(model_VP      )
        call polynomial_eval(ctx_model%vp%ppoly(i_cell),x,v,ctx_err)
        out_val = dble(v)
        if(out_val < ctx_model%vp_min) out_val = ctx_model%vp_min
        if(out_val > ctx_model%vp_max) out_val = ctx_model%vp_max
      case(model_ALPHA  )
        call polynomial_eval(ctx_model%alpha%ppoly(i_cell),x,v,ctx_err)
        out_val = dble(v)
        if(out_val < ctx_model%alpha_min) out_val = ctx_model%alpha_min
        if(out_val > ctx_model%alpha_max) out_val = ctx_model%alpha_max
      case(model_DALPHA  )
        call polynomial_eval(ctx_model%dalpha%ppoly(i_cell),x,v,ctx_err)
        out_val = dble(v)
        if(out_val < ctx_model%dalpha_min) out_val = ctx_model%dalpha_min
        if(out_val > ctx_model%dalpha_max) out_val = ctx_model%dalpha_max
        
      case(model_GAMMA,model_GAMMA0) !! Attenuation
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case('gamma','gamma0_power-law')
              call polynomial_eval(ctx_model%gamm%ppoly(i_cell),x,v,ctx_err)
              out_val = dble(v)
              if(out_val < ctx_model%gamma_min) out_val = ctx_model%gamma_min
              if(out_val > ctx_model%gamma_max) out_val = ctx_model%gamma_max
            
            case default
              ctx_err%msg   = "** ERROR: Unrecognized visco. model [model_eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no attenuation in this medium [model_eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if

      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! NO sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! alpha can be < 0 and dalpha too, or zero.
    
    return

  end subroutine model_eval_piecewise_polynomial_raw

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for piecewise-polynomial in a given point, 
  !> where we adjust for attenuation.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : cell in which the x belongs to
  !> @param[in]    x          : which position in this cell
  !> @param[in]    freq       : frequency for attenuation with power law
  !> @param[out]   out_vp     : output value for vp
  !> @param[out]   out_alpha  : output value for alpha
  !> @param[out]   out_dalpha : output value for dalpha
  !> @param[out]   out_gamma  : output value for gamma
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_piecewise_polynomial_all(ctx_model,i_cell,xpt,  &
                                                 freq,out_vp,out_alpha, &
                                                 out_dalpha,out_gamma,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt(:)
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_vp
    real   (kind=8)         ,intent(out)  :: out_alpha
    real   (kind=8)         ,intent(out)  :: out_dalpha
    real   (kind=8)         ,intent(out)  :: out_gamma
    type(t_error)           ,intent(inout):: ctx_err
    !!
    real(kind=RKIND_POL)                :: vp,alpha,dalpha,gamm
    real(kind=RKIND_POL), allocatable   :: x(:)
    !! 

    allocate(x(ctx_model%dim_domain))
    x = real(xpt,kind=RKIND_POL)
    
    call polynomial_eval(ctx_model%vp%ppoly    (i_cell),x,vp    ,ctx_err)
    call polynomial_eval(ctx_model%alpha%ppoly (i_cell),x,alpha ,ctx_err)
    call polynomial_eval(ctx_model%dalpha%ppoly(i_cell),x,dalpha,ctx_err)
    
    out_vp    = dble(vp)
    out_alpha = dble(alpha)
    out_dalpha= dble(dalpha)
    out_gamma = 0.d0

    if(out_vp    < ctx_model%vp_min)     out_vp    = ctx_model%vp_min
    if(out_vp    > ctx_model%vp_max)     out_vp    = ctx_model%vp_max
    if(out_alpha < ctx_model%alpha_min)  out_alpha = ctx_model%alpha_min
    if(out_alpha > ctx_model%alpha_max)  out_alpha = ctx_model%alpha_max
    if(out_dalpha< ctx_model%dalpha_min) out_dalpha= ctx_model%dalpha_min
    if(out_dalpha> ctx_model%dalpha_max) out_dalpha= ctx_model%dalpha_max

    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        case('gamma') !! directly gamma
          call polynomial_eval(ctx_model%gamm%ppoly(i_cell),x,gamm,ctx_err)
          out_gamma = dble(gamm)
        case('gamma0_power-law') !! using the power law and the gamma0
          !! compute using the power law 
          call polynomial_eval(ctx_model%gamm%ppoly(i_cell),x,gamm,ctx_err)
          out_gamma = compute_gamma_powerlaw(dble(gamm),freq)

        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model " //&
                          "[model_eval]"
          ctx_err%ierr  = -1
          return
      end select
    end if
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(out_vp .le. 0.d0) then
      ctx_err%msg   = "** ERROR: value for the physical parameters " // &
                      "is <= 0 for helio ODE medium [model_eval] "
      ctx_err%ierr  = -1
    end if

    return

  end subroutine model_eval_piecewise_polynomial_all

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pconstant_real4(ctx_model,model_out,strmodel,    &
                                       freq,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4), allocatable          ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(IKIND_MESH)  :: i_cell
    real(kind=8)         :: tempval
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! loop over all cells 
    do i_cell = 1, ctx_model%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle
      
      !! piecewise constant evaluation 
      call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,     &
                                         tempval,ctx_err)
      !! save in model
      model_out(i_cell) = real(tempval)
    end do
    
    return
  end subroutine model_get_pconstant_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pconstant_real8(ctx_model,model_out,strmodel,    &
                                       freq,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8), allocatable          ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(IKIND_MESH)  :: i_cell
    real(kind=8)         :: tempval
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! loop over all cells 
    do i_cell = 1, ctx_model%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle
      
      !! piecewise constant evaluation 
      call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,     &
                                         tempval,ctx_err)
      !! save in model
      model_out(i_cell) = tempval
    end do
    
    return
  end subroutine model_get_pconstant_real8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_ppoly_real4(ctx_domain,ctx_model,model_out,      &
                                   strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = max(ctx_model%vp%ppoly_order,ctx_model%alpha%ppoly_order)
    ndof_vol = max(ctx_model%vp%ppoly(1)%size_coeff,                    &
                   ctx_model%alpha%ppoly(1)%size_coeff)
    order    = max(order,ctx_model%dalpha%ppoly_order)    
    ndof_vol = max(ndof_vol,ctx_model%dalpha%ppoly(1)%size_coeff)
    if(ctx_model%flag_viscous) then
      order    = max(order,ctx_model%gamm%ppoly_order)    
      ndof_vol = max(ndof_vol,ctx_model%gamm%ppoly(1)%size_coeff)
    end if

    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: at least one of the piecewise-" //     &
                      "polynomial model must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0    
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell ............................................
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo,ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol      
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call model_eval_piecewise_polynomial(ctx_model,strmodel,i_cell, &
                                             dof_coo(:,k),tempval,ctx_err)
        model_out(i1 + k) = real(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    !! -----------------------------------------------------------------

    return
  end subroutine model_get_ppoly_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_ppoly_real8(ctx_domain,ctx_model,model_out,      &
                                   strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = max(ctx_model%vp%ppoly_order,ctx_model%alpha%ppoly_order)
    ndof_vol = max(ctx_model%vp%ppoly(1)%size_coeff,                    &
                   ctx_model%alpha%ppoly(1)%size_coeff)
    order    = max(order,ctx_model%dalpha%ppoly_order)    
    ndof_vol = max(ndof_vol,ctx_model%dalpha%ppoly(1)%size_coeff)
    if(ctx_model%flag_viscous) then
      order    = max(order,ctx_model%gamm%ppoly_order)    
      ndof_vol = max(ndof_vol,ctx_model%gamm%ppoly(1)%size_coeff)
    end if

    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: at least one of the piecewise-" //     &
                      "polynomial model must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0    
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell ............................................
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo,ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol      
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call model_eval_piecewise_polynomial(ctx_model,strmodel,i_cell, &
                                             dof_coo(:,k),tempval,ctx_err)
        model_out(i1 + k) = real(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    !! -----------------------------------------------------------------
    
    return
  end subroutine model_get_ppoly_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for a parameter 
  !> given per dof.
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pdof_real4(ctx_domain,ctx_model,model_out,       &
                                  strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = max(ctx_model%vp%param_dof%ndof ,ctx_model%alpha%param_dof%ndof)   
    order    = max(ctx_model%vp%param_dof%order,ctx_model%alpha%param_dof%order)

    ndof_vol = max(ndof_vol ,ctx_model%dalpha%param_dof%ndof)   
    order    = max(order    ,ctx_model%dalpha%param_dof%order)
    
    if(ctx_model%flag_viscous) then
      ndof_vol = max(ndof_vol ,ctx_model%gamm%param_dof%ndof)   
      order    = max(order    ,ctx_model%gamm%param_dof%order)
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    if(ndof_vol == 1) then
      do i_cell = 1, ctx_domain%n_cell_loc
        call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,   &
                                           tempval,ctx_err)
        model_out(i_cell) = real(tempval,kind=4)
      end do
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol, &
                                             ctx_domain%dim_domain,     &
                                             dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        call model_eval_dof_raw_refelem_multi(ctx_model,i_cell,         &
                                      strmodel,dof_coo,ndof_vol,out_val,&
                                      ctx_err)
        model_out(i_gb1:i_gb2) = real(out_val(:),kind=4)
      end do
      deallocate(dof_coo)
      deallocate(out_val)
    end if
    
    return
  end subroutine model_get_pdof_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for a parameter 
  !> given per dof.
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pdof_real8(ctx_domain,ctx_model,model_out,       &
                                  strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! for now, we do not have models that use the frequency (such as
    !! a Qfactor), therefore, we just add this line to avoid warning.
    if(real(freq) < 1.d0) then
    end if

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = max(ctx_model%vp%param_dof%ndof ,ctx_model%alpha%param_dof%ndof)   
    order    = max(ctx_model%vp%param_dof%order,ctx_model%alpha%param_dof%order)

    ndof_vol = max(ndof_vol ,ctx_model%dalpha%param_dof%ndof)   
    order    = max(order    ,ctx_model%dalpha%param_dof%order)
    
    if(ctx_model%flag_viscous) then
      ndof_vol = max(ndof_vol ,ctx_model%gamm%param_dof%ndof)   
      order    = max(order    ,ctx_model%gamm%param_dof%order)
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    if(ndof_vol == 1) then
      do i_cell = 1, ctx_domain%n_cell_loc
        call model_eval_piecewise_constant(ctx_model,strmodel,i_cell,   &
                                           tempval,ctx_err)
        model_out(i_cell) = dble(tempval)
      end do
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol, &
                                             ctx_domain%dim_domain,     &
                                             dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        call model_eval_dof_raw_refelem_multi(ctx_model,i_cell,         &
                                      strmodel,dof_coo,ndof_vol,out_val,&
                                      ctx_err)
        model_out(i_gb1:i_gb2) = dble(out_val(:))
      end do
      deallocate(dof_coo)
      deallocate(out_val)
    end if
    
    return
  end subroutine model_get_pdof_real8

end module m_model_eval
