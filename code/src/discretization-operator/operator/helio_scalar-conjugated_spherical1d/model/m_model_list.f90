!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_list.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines of the context for the model parameters for the
!> ODE problem ARISING IN HELIOSEISMOLOGY
!>
!>  \f$(-\frac{\partial^2}{\partial x^2} - k^2 + \alpha/x + l*(l+1)/x^2) u(x) = 0\f$.
!>
!
!------------------------------------------------------------------------------
module m_model_list

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  !> list of model names
  character(len= 2), parameter :: model_VP        = 'vp'
  character(len= 5), parameter :: model_GAMMA     = 'gamma'
  !> using a value of gamma0 and the viscosity will apply a power law 
  character(len= 6), parameter :: model_GAMMA0    = 'gamma0'
  character(len= 5), parameter :: model_ALPHA     = 'alpha'
  character(len= 6), parameter :: model_DALPHA    = 'dalpha'
  
  private
  public  :: model_VP, model_GAMMA, model_GAMMA0, model_ALPHA, model_DALPHA
  public  :: model_formalism

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Formalise models name with appropriate TAG for ODE Helio
  !> under spherical symmetry
  !
  !> @param[in]    dm             : dimension
  !> @param[in]    flag_viscous   : indicates if viscosity
  !> @param[in]    viscous_model  : viscosity model name
  !> @param[in]    n_model        : number of physical model parameter
  !> @param[in]    name_model     : name of physical model parameter
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_formalism(dm,flag_viscous,viscous_model,n_model,     &
                             name_model_in,name_model_out,ctx_err)
    implicit none

    integer                            ,intent(in)   :: dm
    logical                            ,intent(in)   :: flag_viscous
    character(len=32)                  ,intent(in)   :: viscous_model
    integer                            ,intent(in)   :: n_model
    character(len=32),allocatable      ,intent(in)   :: name_model_in (:)
    character(len=32),allocatable      ,intent(inout):: name_model_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer    :: k,l
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(dm .ne. 1) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helio ODE equation because spherical symmetry "//&
                      "[model_formalism] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    
    !! check count models
    if(flag_viscous) then
      if(n_model .ne. 4) then
        ctx_err%msg   = "** ERROR: INCORRECT NUMBER OF MODEL   " //  &
                        "[model_formalism] **"
        ctx_err%ierr  = -1
      end if
      if(trim(adjustl(viscous_model)) .ne.'gamma' .and.                 &
         trim(adjustl(viscous_model)) .ne.'gamma0_power-law' ) then
        ctx_err%msg   = "** ERROR: viscosity model for helio ODE "   // &
                        "equation in spherical symmetry is not "     // &
                        "recognized [model_formalism] **"
        ctx_err%ierr  = -1
      end if
    else
      if(n_model .ne. 3) then
        ctx_err%msg   = "** ERROR: INCORRECT NUMBER OF MODEL   " //  &
                        "[model_formalism] **"
        ctx_err%ierr  = -1
      end if
    endif
    
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if

    !! retrieve tag for all models
    do k=1,n_model
      select case(trim(adjustl(name_model_in(k))))
        case('velocity','vp','Velocity','Vp','VP','Cp','c','cp','VELOCITY','CP')
          name_model_out(k) = model_VP
        case('alpha','ALPHA','Alpha')
          name_model_out(k) = model_ALPHA
        case('DALPHA','dalpha','DAlpha','Dalpha')
          name_model_out(k) = model_DALPHA
        case('gamma','GAMMA','Gamma')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if          
          name_model_out(k) = model_GAMMA
        
        case('gamma0_power-law')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if          
          name_model_out(k) = model_GAMMA0

        case default 
          ctx_err%msg   = "** ERROR: model name in helio ODE not "   // &
                          "recognized [model_formalism] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      
      !! -----------------------------------------------------------------
      !! need to check that 
      !!   1) it is different from any other one 
      !!   2) in case of viscosity, one of the model must be the viscosity
      !! -----------------------------------------------------------------
      do l=1,k-1
        if(trim(adjustl(name_model_out(k))) == &
           trim(adjustl(name_model_out(l)))) then
          ctx_err%msg   = "** ERROR: helio model "         //           &
                          trim(adjustl(name_model_out(k))) //           &
                          " is repeated twice [model_formalism] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)        
        endif
      end do
    end do

    !! check that viscosity if present
    if(flag_viscous) then
      ctx_err%ierr=-1
      do k=1,n_model
       if(trim(adjustl(name_model_out(k))) .eq. model_GAMMA ) ctx_err%ierr=0 
       if(trim(adjustl(name_model_out(k))) .eq. model_GAMMA0) ctx_err%ierr=0 
      end do
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: attenuation requires one of the "// &
                        "model to be the viscosity [model_formalism] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if

    return
  end subroutine model_formalism

end module m_model_list
