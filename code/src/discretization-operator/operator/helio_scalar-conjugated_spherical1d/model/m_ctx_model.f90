!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_model.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines of the context for the model parameters for the
!> ODE problem ARISING IN HELIOSEISMOLOGY
!>
!>  \f$(-\frac{\partial^2}{\partial x^2} - k^2 + \alpha/x + l*(l+1)/x^2) u(x) = 0\f$.
!>
!
!------------------------------------------------------------------------------
module m_ctx_model

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_ctx_model_representation, only: t_model_param,                  &
                                        model_representation_clean,     &
                                        model_representation_copy
  use m_model_list
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: t_model, model_clean, model_infos
  public  :: model_allocate, model_copy
  public  :: compute_gamma_powerlaw, compute_dgamma0_powerlaw

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_model contains info on the models for the
  !> ODE problem in helio
  !
  !> utils:
  !> @param[integer]       dim_domain   : problem dimension,
  !> @param[string]        format_disc  : discretization format,
  !> @param[integer]       n_coeff      : ncoeff per model,
  !> @param[integer]       n_model      : number of models,
  !> @param[string]        name_model   : name of the models,
  !> @param[string]        flag_viscous : indicates if viscosity
  !> @param[string]        viscous_model: viscosity model
  !> @param[t_model_param] vp           : velocity,
  !> @param[t_model_param] alpha        : alpha,
  !> @param[t_model_param] dalpha       : alpha',
  !> @param[t_model_param] visco        : viscous factor(s),
  !----------------------------------------------------------------------------
  type t_model

    !! domain dimension (2 or 3D)
    integer                         :: dim_domain
    !! discretization format (e.g., piecewise-constant)
    character(len=32)               :: format_disc
    !! array size
    integer                         :: n_cell_loc
    integer                         :: n_cell_gb

    !! number of model
    integer                         :: n_model
    !! model names
    character(len=32), allocatable  :: name_model(:)

    !! viscosity information
    logical                         :: flag_viscous
    character(len=32)               :: viscous_model

    !! model representation 
    type(t_model_param)             :: vp
    type(t_model_param)             :: alpha
    type(t_model_param)             :: dalpha
    type(t_model_param)             :: gamm
    !! min/max values are only used in inversion
    real(kind=8)                    :: vp_min,vp_max
    real(kind=8)                    :: alpha_min,alpha_max
    real(kind=8)                    :: dalpha_min,dalpha_max
    real(kind=8)                    :: gamma_min,gamma_max

    !! memory infos
    integer(kind=8)                 :: memory_loc
    integer(kind=8)                 :: memory_gb
  end type t_model

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the corresponding attenuation with a power law, depending
  !> on the frequency
  !
  !> @param[in]    angular_freq         : angular frequency
  !> @param[out]   gamm                 : output gamma
  !----------------------------------------------------------------------------
  pure function compute_gamma_powerlaw(g0,angular_freq)    result(gamm)

    implicit none
    real   (kind=8)      ,intent(in)  :: g0
    complex(kind=8)      ,intent(in)  :: angular_freq
    real   (kind=8)                   :: gamm
    !! values extracted from ESAIM helio paper
    real   (kind=8), parameter        :: pi     = 4.d0*datan(1.d0)
    real   (kind=8), parameter        :: omega0 = 3e-3 * 2.d0*pi
    real   (kind=8), parameter        :: beta   = 5.77
    real   (kind=8)                   :: omega
    
    !! \gamma = \gamma_0 \vert \dfrac{\omega}{\omega_0} \vert^\beta
    
    !! need to adjust the angular frequency to have only the Fourier part
    omega = aimag(angular_freq)  !! = 2 * pi * f
    gamm  = (2.d0*pi*g0) * abs(omega / omega0)**(beta)
    
    return
  end function
  !----------------------------------------------------------------------------

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the corresponding derivative of the attenuation with a 
  !> power law, depending on the frequency
  !
  !> @param[in]    angular_freq         : angular frequency
  !> @param[out]   dgamm                : output gamma
  !----------------------------------------------------------------------------
  pure function compute_dgamma0_powerlaw(angular_freq) result(dgamm)

    implicit none

    complex(kind=8)      ,intent(in)  :: angular_freq
    real   (kind=8)                   :: dgamm
    !! values extracted from ESAIM helio paper
    real   (kind=8), parameter        :: pi     = 4.d0*datan(1.d0)
    real   (kind=8), parameter        :: omega0 = 3e-3 * 2.d0*pi
    real   (kind=8), parameter        :: beta   = 5.77
    real   (kind=8)                   :: omega
    
    !! need to adjust the angular frequency to have only the Fourier part
    omega  = aimag(angular_freq)  !! = 2 * pi * f
    dgamm  = 2.d0 * pi * abs(omega/omega0)**(beta)
    
    return
  end function
  !----------------------------------------------------------------------------
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models for Helio ODE
  !
  !> @param[in]    dm             : dimension
  !> @param[in]    flag_viscous   : indicates if viscosity
  !> @param[in]    viscous_model  : viscosity model name
  !> @param[in]    n_model        : number of physical model parameter
  !> @param[in]    name_model     : name of physical model parameter
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_infos(dm,flag_viscous,viscous_model,                 &
                         n_model,name_model,ctx_err)
    implicit none
    integer                            ,intent(in)   :: dm
    logical                            ,intent(in)   :: flag_viscous
    character(len=32)                  ,intent(inout):: viscous_model
    integer                            ,intent(out)  :: n_model
    character(len=32),allocatable      ,intent(inout):: name_model(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! we only support One-dimensional problem for now 
    !! -----------------------------------------------------------------
    if(dm .ne. 1) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helio radial ODE equation [model_infos] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! count models
    if(flag_viscous) then
      n_model = 4
    else
      n_model = 3
    endif
    allocate(name_model(n_model))
    name_model(1) = model_VP
    name_model(2) = model_ALPHA
    name_model(3) = model_DALPHA

    !! check consistency of viscous model
    if(flag_viscous) then
      !! viscosity models
      select case(trim(adjustl(viscous_model)))
        case('visco','viscosity','visco-helio','gamma','GAMMA','Gamma')
          viscous_model = trim(adjustl(model_GAMMA)) !! only gamma here
          name_model(4) = trim(adjustl(model_GAMMA)) !! only gamma here

        case('gamma0_power-law','Gamma0_power-law','GAMMA0_POWER-LAW',  &
             'gamma0','GAMMA0')
          viscous_model = 'gamma0_power-law'
          name_model(4) = trim(adjustl(model_GAMMA0))

        case default
          ctx_err%msg   = "** ERROR: viscosity model for helio "// &
                          "ODE equation not recognized "        // &
                          " [model_init] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)

      end select
    end if

    return
  end subroutine model_infos
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize model coefficients for the ODE in HELIO
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_in       : input model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    ndof           : number of dof in representation
  !> @param[in]    mmin           : allowed min val
  !> @param[in]    mmax           : allowed max val
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_allocate(ctx_model,ctx_model_param_in,strmodel,      &
                            mmin,mmax,ctx_err)
    implicit none
    type(t_model)                      ,intent(inout):: ctx_model
    type(t_model_param)                ,intent(in)   :: ctx_model_param_in
    character(len=*)                   ,intent(in)   :: strmodel
    real(kind=8)                       ,intent(in)   :: mmin, mmax
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    logical, parameter  :: flag_allocate = .true.
    !! -----------------------------------------------------------------

    select case(trim(adjustl(strmodel)))
      case(model_VP)         !! VP
        call model_representation_copy(ctx_model_param_in,ctx_model%vp, &
                                       flag_allocate,ctx_err)
        ctx_model%vp_min= mmin
        ctx_model%vp_max= mmax

      case(model_ALPHA)      !! ALPHA
        call model_representation_copy(ctx_model_param_in,              &
                                       ctx_model%alpha,flag_allocate,   &
                                       ctx_err)
        ctx_model%alpha_min= mmin
        ctx_model%alpha_max= mmax

      case(model_DALPHA)     !! dALPHA
        call model_representation_copy(ctx_model_param_in,              &
                                       ctx_model%dalpha,flag_allocate,  &
                                       ctx_err)
        ctx_model%dalpha_min= mmin
        ctx_model%dalpha_max= mmax

      case(model_GAMMA,model_GAMMA0) !! Attenuation

        select case(trim(adjustl(ctx_model%viscous_model)))
          case('gamma', 'gamma0_power-law')
            call model_representation_copy(ctx_model_param_in,          &
                                           ctx_model%gamm,flag_allocate,&
                                           ctx_err)
            ctx_model%gamma_min= mmin
            ctx_model%gamma_max= mmax
          
          case default
            ctx_err%msg   = "** ERROR: viscosity model for helio "// &
                            "ODE equation: " //                      &
                            trim(adjustl(ctx_model%viscous_model)) //&
                            "recognized [model_allocate] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select

      case default
        ctx_err%msg   = "** ERROR: unrecognized model for helio "    // &
                        "radial ODE equation  not recognized "       // &
                        "[model_allocate] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    return
  end subroutine model_allocate

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> copy a model for the ODE in HELIO
  !
  !> @param[in]    ctx_model_in   : type model
  !> @param[inout] ctx_model_out  : type model
  !> @param[in]    flag_allocate  : indicates if array have to be allocated
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_copy(ctx_model_in,ctx_model_out,flag_allocate,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model_in
    type(t_model)                      ,intent(inout):: ctx_model_out
    logical                            ,intent(in)   :: flag_allocate
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    ctx_model_out%dim_domain   = ctx_model_in%dim_domain
    ctx_model_out%format_disc  = ctx_model_in%format_disc
    ctx_model_out%n_cell_loc   = ctx_model_in%n_cell_loc
    ctx_model_out%n_cell_gb    = ctx_model_in%n_cell_gb
    ctx_model_out%n_model      = ctx_model_in%n_model
    ctx_model_out%flag_viscous = ctx_model_in%flag_viscous
    ctx_model_out%viscous_model= ctx_model_in%viscous_model
    ctx_model_out%memory_loc   = ctx_model_in%memory_loc
    ctx_model_out%memory_gb    = ctx_model_in%memory_gb
    ctx_model_out%vp_min       = ctx_model_in%vp_min
    ctx_model_out%vp_max       = ctx_model_in%vp_max
    ctx_model_out%alpha_min    = ctx_model_in%alpha_min
    ctx_model_out%alpha_max    = ctx_model_in%alpha_max
    ctx_model_out%dalpha_min   = ctx_model_in%dalpha_min
    ctx_model_out%dalpha_max   = ctx_model_in%dalpha_max

    !! copy representations 
    call model_representation_copy(ctx_model_in%vp ,ctx_model_out%vp ,  &
                                   flag_allocate,ctx_err)
    call model_representation_copy(ctx_model_in%alpha,                  &
                                   ctx_model_out%alpha,flag_allocate,   &
                                   ctx_err)
    call model_representation_copy(ctx_model_in%dalpha,                 &
                                   ctx_model_out%dalpha,flag_allocate,  &
                                   ctx_err)
    
    if(ctx_model_out%flag_viscous) then
      call model_representation_copy(ctx_model_in%gamm,                 &
                                     ctx_model_out%gamm,                &
                                     flag_allocate,ctx_err)
      ctx_model_out%gamma_min= ctx_model_in%gamma_min
      ctx_model_out%gamma_max= ctx_model_in%gamma_max
    end if

    if(flag_allocate) then
      allocate(ctx_model_out%name_model(ctx_model_out%n_model))
    end if
    ctx_model_out%name_model = ctx_model_in%name_model

    return
  end subroutine model_copy

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean model information
  !
  !> @param[inout] ctx_model    : model context
  !----------------------------------------------------------------------------
  subroutine model_clean(ctx_model)
    implicit none
    type(t_model)   ,intent(inout)  :: ctx_model

    ctx_model%dim_domain           =0
    ctx_model%format_disc          =''
    ctx_model%n_cell_loc           =0
    ctx_model%n_cell_gb            =0
    ctx_model%n_model              =0
    ctx_model%flag_viscous         = .false.
    ctx_model%viscous_model        =''
    ctx_model%memory_loc           =0
    ctx_model%memory_gb            =0
    if(allocated(ctx_model%name_model)) deallocate(ctx_model%name_model)
    !! clean representation 
    call model_representation_clean(ctx_model%vp)
    call model_representation_clean(ctx_model%alpha)
    call model_representation_clean(ctx_model%dalpha)
    call model_representation_clean(ctx_model%gamm)

    return
  end subroutine model_clean

end module m_ctx_model
