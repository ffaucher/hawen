!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_update.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the routine to update the model depending on 
!> the parametrization for HARMONIC ELASTIC ISO EQUATION.
!
!------------------------------------------------------------------------------
module m_model_update

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_define_precision,         only: IKIND_MESH
  use m_ctx_parallelism,          only: t_parallelism
  use m_ctx_model,                only: t_model
  use m_model_eval,               only: model_get_piecewise_constant,   &
                                        model_eval_dof_index
  use m_model_list
  use m_grad_model_function,      only: apply_model_function_inv,         &
                                        apply_model_function
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT, tag_MODEL_DOF
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: model_update
  private :: model_update_pconstant

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for HARMONIC ELASTIC ISOTROPIC, then we give back the (cp,cs,rho).
  !> 
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[in]    freq           : current frequency
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine model_update(ctx_paral,ctx_model,search,alpha,param_current, &
                          tag_function,parametrization,freq,ctx_err)
    implicit none
   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case(trim(adjustl(ctx_model%format_disc)))
      case(tag_MODEL_PCONSTANT) 
        call model_update_pconstant(ctx_paral,ctx_model,search,alpha,   &
                                    param_current,tag_function,         &
                                    parametrization,freq,ctx_err)

      case(tag_MODEL_DOF) 
        call model_update_dof(ctx_paral,ctx_model,search,alpha,         &
                              param_current,tag_function,               &
                              parametrization,freq,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: only piecewise-constant model " //&
                        " supported [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return

  end subroutine model_update

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for HARMONIC ELASTIC ISOTROPIC, then we give back the (cp,cs,rho).
  !> 
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[in]    freq           : current frequency
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine model_update_pconstant(ctx_paral,ctx_model,search,alpha,   &
                                    param_current,tag_function,         &
                                    parametrization,freq,ctx_err)
    implicit none

   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ic,n_coeff_loc,im,nvisco
    real(kind=4),allocatable  :: model(:),lambda(:),mu(:),temp,l,m
    real(kind=4),allocatable  :: impp(:),imps(:),bulk(:),pois(:)
    real(kind=4)              :: tol=tiny(1.0)
    real(kind=8)              :: mu_r,lambda_r,eta_l,eta_m,tau_s,tau_e 
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0


    !! piecewise-constant representation *******************************
    n_coeff_loc = ctx_model%n_cell_loc
    !! *****************************************************************
    allocate(model(n_coeff_loc))
    model = 0.0
    
    !! -----------------------------------------------------------------
    !! 1) we get the parameter to be update
    !! PIECEWISE-CONSTANT **********************************************
    call model_get_piecewise_constant(ctx_model,model,                  & 
                                      trim(adjustl(param_current)),freq,&
                                      ctx_err)
    call apply_model_function(ctx_paral,model,tag_function,ctx_err)

    !! 2) we update model function
    model(:) = model(:) - alpha * search(:)
    !! 3) we get the identity model back
    call apply_model_function_inv(ctx_paral,model,tag_function,ctx_err)
    !! 4) we get (Vp,Vs,Rho) from the current selected parametrization


    !! -----------------------------------------------------------------
    !! depending on the parametrization we need to update
    !! the ctx_model which contains Vp, Vs and Rho
    !!
    !! -----------------------------------------------------------------


                !***************************************!
                ! Everything is piecewise-constant here !
                !                                       !
                !***************************************!

    !! =================================================================
    !! we first update the attenuation parameters.
    !! =================================================================
    if (trim(adjustl(param_current))     .eq. model_ETA_LDA)   then
      ctx_model%visco(1)%pconstant(:) = model(:)

    elseif (trim(adjustl(param_current)) .eq. model_ETA_MU)    then
      ctx_model%visco(2)%pconstant(:) = model(:)

    elseif (trim(adjustl(param_current)) .eq. model_TAU_SIGMA .or. &
            trim(adjustl(param_current)) .eq. model_FPOWER)    then
      ctx_model%visco(3)%pconstant(:) = model(:)

    elseif (trim(adjustl(param_current)) .eq. model_TAU_EPS)   then
      ctx_model%visco(4)%pconstant(:) = model(:)
    
    elseif (trim(adjustl(param_current)) .eq. model_QFACTORP)  then
      !! the Qfactor only works if we have lambda & mu parametrization.
      if(any(parametrization == model_RHO    ) .and.                &
         any(parametrization == model_LAMBDA ) .and.                &
         any(parametrization == model_MU     )) then

        ctx_err%msg   = "** ERROR: the parametrization with quality"//&
                        " factors is not yet read [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
      else
        ctx_err%msg   = "** ERROR: the parametrization with quality"//&
                        " factors is not yet read [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    
    elseif (trim(adjustl(param_current)) .eq. model_QFACTORS)  then
      !! the Qfactor only works if we have lambda & mu parametrization.
      if(any(parametrization == model_RHO    ) .and.                &
         any(parametrization == model_LAMBDA ) .and.                &
         any(parametrization == model_MU     )) then
      
      else
        ctx_err%msg   = "** ERROR: the parametrization with quality"//&
                        " factors is only possible from "           //&
                        "(\lambda,\mu,\rho) parametrization "       //&
                        "[model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    
    !! =================================================================
    !! otherwize it depends on the parametrization
    else
      ! ----------------------------------------------------------------
      !! parametrization cp, cs, rho
      if(any(parametrization == model_RHO) .and.                        &
         any(parametrization == model_VP ) .and.                        &
         any(parametrization == model_VS )) then
        select case(trim(adjustl(param_current)))
          case(model_VP) ! ---------------------------------------------
            ctx_model%vp%pconstant(:)  = model(:)
          case(model_VS) ! ---------------------------------------------
            ctx_model%vs%pconstant(:)  = model(:)
          case(model_RHO)! ---------------------------------------------
            ctx_model%rho%pconstant(:)  = model(:)
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization lambda, mu, rho
      elseif(any(parametrization == model_RHO    ) .and.                &
             any(parametrization == model_LAMBDA ) .and.                &
             any(parametrization == model_MU     )) then
        allocate(lambda(n_coeff_loc)) ; lambda = 0.0
        allocate(mu    (n_coeff_loc)) ; mu     = 0.0       
        call model_get_piecewise_constant(ctx_model,lambda,model_LAMBDA,& 
                                          freq,ctx_err)
        call model_get_piecewise_constant(ctx_model,mu    ,model_MU    ,& 
                                          freq,ctx_err)

        select case(trim(adjustl(param_current)))
          case(model_LAMBDA) ! -----------------------------------------
            do ic = 1, n_coeff_loc
              temp = model(ic)+2.*mu(ic)
              if(temp > tol) then
                ctx_model%vp%pconstant(ic) = sqrt(temp/                 &
                                                  ctx_model%rho%pconstant(ic))
              else
                ctx_model%vp%pconstant(ic) = ctx_model%vp_min
              end if
            end do
          case(model_MU) ! ---------------------------------------------
            do ic = 1, n_coeff_loc
              temp = lambda(ic)+2*model(ic)
              if(temp > tol) then
                ctx_model%vp%pconstant(ic) = sqrt(temp/                 &
                                             ctx_model%rho%pconstant(ic))
              else
                ctx_model%vp%pconstant(ic) = ctx_model%vp_min
              end if
              if(model(ic) > tol) then
                ctx_model%vs%pconstant(ic) = sqrt(model(ic) /           &
                                                  ctx_model%rho%pconstant(ic))
              else
                ctx_model%vs%pconstant(ic) = ctx_model%vs_min
              end if
            end do
          case(model_RHO) ! --------------------------------------------
            ctx_model%rho%pconstant = model
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%vp%pconstant(ic) = sqrt((lambda(ic)+2*mu(ic))/&
                                                    model(ic))
                ctx_model%vs%pconstant(ic) = sqrt(mu(ic)/model(ic))
              else
                ctx_model%vp%pconstant(ic) = sqrt((lambda(ic)+2*mu(ic))/&
                                                   ctx_model%rho_min)
                ctx_model%vs%pconstant(ic) = sqrt(     mu(ic)          /&
                                                  ctx_model%rho_min)
              end if
            end do
          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! parametrization ip, is, rho
      elseif(any(parametrization == model_RHO    ) .and.                &
             any(parametrization == model_IP     ) .and.                &
             any(parametrization == model_IS     )) then
        allocate(impp(n_coeff_loc)) ; impp = 0.0
        allocate(imps(n_coeff_loc)) ; imps = 0.0

        call model_get_piecewise_constant(ctx_model,impp,model_IP,freq, &
                                          ctx_err)
        call model_get_piecewise_constant(ctx_model,imps,model_IS,freq, &
                                          ctx_err)
        
        select case(trim(adjustl(param_current)))
          case(model_IP) ! ---------------------------------------------
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%vp%pconstant(ic) = model(ic)/                 &
                                             ctx_model%rho%pconstant(ic)
              else
                ctx_model%vp%pconstant(ic) = ctx_model%vp_min
              end if
            end do
          case(model_IS) ! ---------------------------------------------
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%vs%pconstant(ic) = model(ic)/ctx_model%rho%pconstant(ic)
              else
                ctx_model%vs%pconstant(ic) = ctx_model%vs_min
              end if
            end do
          case(model_RHO) ! --------------------------------------------
            ctx_model%rho%pconstant = model
            do ic = 1, n_coeff_loc
              if(model(ic) > tol) then
                ctx_model%vp%pconstant(ic) = impp(ic)/model(ic)
                ctx_model%vs%pconstant(ic) = imps(ic)/model(ic)
              else
                ctx_model%vp%pconstant(ic) = impp(ic)/ctx_model%rho_min
                ctx_model%vs%pconstant(ic) = imps(ic)/ctx_model%rho_min
              end if
            end do         
          case default
            ctx_err%ierr = -1
        end select
      ! ----------------------------------------------------------------
      !! parametrization bulk, poisson, rho
      elseif(any(parametrization == model_RHO    ) .and.                &
             any(parametrization == model_BULK   ) .and.                &
             any(parametrization == model_POISSON)) then
        allocate(bulk(n_coeff_loc)) ; bulk = 0.0
        allocate(pois(n_coeff_loc)) ; pois = 0.0

        call model_get_piecewise_constant(ctx_model,bulk,model_BULK   , &
                                          freq,ctx_err)
        call model_get_piecewise_constant(ctx_model,pois,model_POISSON, &
                                          freq,ctx_err)
        
        select case(ctx_model%dim_domain)
          case(2)
            select case(trim(adjustl(param_current)))
              case(model_BULK) ! ---------------------------------------
                do ic = 1, n_coeff_loc
                  l = 2.*pois(ic)*model(ic)
                  m = (1. - 2.*pois(ic))*model(ic)
                  temp = l+2.*m
                  if(temp > tol) then
                    ctx_model%vp%pconstant(ic) = sqrt(temp/             &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vp%pconstant(ic) = ctx_model%vp_min
                  end if
                  if(m > tol) then
                    ctx_model%vs%pconstant(ic) = sqrt(m/                &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vs%pconstant(ic) = ctx_model%vs_min
                  end if
                end do
              case(model_POISSON) ! ------------------------------------
                do ic = 1, n_coeff_loc
                  l = 2.*model(ic)*bulk(ic)
                  m = (1. - 2.*model(ic))*bulk(ic)
                  temp = l+2.*m
                  if(temp > tol) then
                    ctx_model%vp%pconstant(ic) = sqrt(temp/             &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vp%pconstant(ic) = ctx_model%vp_min
                  end if
                  if(m > tol) then
                    ctx_model%vs%pconstant(ic) = sqrt(m/                &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vs%pconstant(ic) = ctx_model%vs_min
                  end if
                end do
              case(model_RHO) ! ----------------------------------------
                do ic = 1, n_coeff_loc
                  ctx_model%rho%pconstant = model
                  l = 2.*pois(ic)*bulk(ic)
                  m = (1. - 2.*pois(ic))*bulk(ic)
                  temp = l+2.*m
                  if(model(ic) > tol) then
                    ctx_model%vp%pconstant(ic) = sqrt(temp/model(ic))
                    ctx_model%vs%pconstant(ic) = sqrt(m   /model(ic))
                  else
                    ctx_model%vp%pconstant(ic) = sqrt(temp/ctx_model%rho_min)
                    ctx_model%vs%pconstant(ic) = sqrt(m   /ctx_model%rho_min)
                  end if
                end do
              case default
                ctx_err%ierr = -1
            end select

          case(3)
            select case(trim(adjustl(param_current)))
              case(model_BULK) ! ---------------------------------------
                do ic = 1, n_coeff_loc
                  l = (1. - (1.-2.*pois(ic))/(pois(ic)+1.))*model(ic)
                  m = 3./2.*(1.-2.*pois(ic))/(pois(ic)+1.) *model(ic)
                  temp = l+2.*m
                  if(temp > tol) then
                    ctx_model%vp%pconstant(ic) = sqrt(temp/             &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vp%pconstant(ic) = ctx_model%vp_min
                  end if
                  if(m > tol) then
                    ctx_model%vs%pconstant(ic) = sqrt(m/                &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vs%pconstant(ic) = ctx_model%vs_min
                  end if
                end do
              case(model_POISSON) ! ------------------------------------
                do ic = 1, n_coeff_loc
                  l = (1. - (1.-2.*model(ic))/(model(ic)+1.))*bulk(ic)
                  m = 3./2.*(1.-2.*model(ic))/(model(ic)+1.) *bulk(ic)
                  temp = l+2.*m
                  if(temp > tol) then
                    ctx_model%vp%pconstant(ic) = sqrt(temp/             &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vp%pconstant(ic) = ctx_model%vp_min
                  end if
                  if(m > tol) then
                    ctx_model%vs%pconstant(ic) = sqrt(m/                &
                                                 ctx_model%rho%pconstant(ic))
                  else
                    ctx_model%vs%pconstant(ic) = ctx_model%vs_min
                  end if
                end do
              case(model_RHO) ! ----------------------------------------
                do ic = 1, n_coeff_loc
                  ctx_model%rho%pconstant = model
                  l = (1. - (1.-2.*pois(ic))/(pois(ic)+1.))*bulk(ic)
                  m = 3./2.*(1.-2.*pois(ic))/(pois(ic)+1.) *bulk(ic)
                  temp = l+2.*m
                  if(model(ic) > tol) then
                    ctx_model%vp%pconstant(ic) = sqrt(temp/model(ic))
                    ctx_model%vs%pconstant(ic) = sqrt(m   /model(ic))
                  else
                    ctx_model%vp%pconstant(ic) = sqrt(temp/ctx_model%rho_min)
                    ctx_model%vs%pconstant(ic) = sqrt(m   /ctx_model%rho_min)
                  end if
                end do
              case default
                ctx_err%ierr = -1
            end select
        
          case default
            ctx_err%ierr = -1
        end select
      
      ! ----------------------------------------------------------------
      !! Error
      else
        ctx_err%msg   = "** ERROR: Unrecognized model name [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    ! ----------------------------------------------------------------
    deallocate(model)
    if(allocated(lambda)) deallocate(lambda)
    if(allocated(mu    )) deallocate(mu    )
    if(allocated(impp  )) deallocate(impp  )
    if(allocated(imps  )) deallocate(imps  )
    if(allocated(bulk  )) deallocate(bulk  )
    if(allocated(pois  )) deallocate(pois  )   

    !! in case of error ------------------------------------------------
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: Unrecognized parametrization [model_update] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 

    !! if everything is ok so far, we have to adjust min and max values
    !! -----------------------------------------------------------------
    do ic = 1, n_coeff_loc
      if(ctx_model%vp%pconstant(ic) < ctx_model%vp_min) &
         ctx_model%vp%pconstant(ic) = ctx_model%vp_min
      if(ctx_model%vp%pconstant(ic) > ctx_model%vp_max) &
         ctx_model%vp%pconstant(ic) = ctx_model%vp_max
      if(ctx_model%vs%pconstant(ic) < ctx_model%vs_min) &
         ctx_model%vs%pconstant(ic) = ctx_model%vs_min
      if(ctx_model%vs%pconstant(ic) > ctx_model%vs_max) &
         ctx_model%vs%pconstant(ic) = ctx_model%vs_max
      if(ctx_model%rho%pconstant(ic)< ctx_model%rho_min) &
         ctx_model%rho%pconstant(ic)= ctx_model%rho_min
      if(ctx_model%rho%pconstant(ic)> ctx_model%rho_max) &
         ctx_model%rho%pconstant(ic)= ctx_model%rho_max
    end do
    if(ctx_model%flag_viscous) then
      nvisco = size(ctx_model%visco)
      do im = 1, nvisco
        do ic = 1, n_coeff_loc
          if(ctx_model%visco(im)%pconstant(ic) < ctx_model%visco_min(im)) &
             ctx_model%visco(im)%pconstant(ic) = ctx_model%visco_min(im)
          if(ctx_model%visco(im)%pconstant(ic) > ctx_model%visco_max(im)) &
             ctx_model%visco(im)%pconstant(ic) = ctx_model%visco_max(im)
        end do
      end do
      
      !! if Zener model, we must ensure that the \tau are consistent
      !! we must respect \tau_eps * \eta_lambda > \tau_sugma \lambda
      if(trim(adjustl(ctx_model%viscous_model)) .eq. model_visco_Zener) then
        do ic = 1, n_coeff_loc
          !! get the parameters
          mu_r    = dble(ctx_model%rho%pconstant(ic) *                  &
                        (ctx_model%vs%pconstant(ic)**2))
          lambda_r= dble(ctx_model%rho%pconstant(ic) *                  &
                        (ctx_model%vp%pconstant(ic)**2) - 2.d0 * mu_r)
          eta_l =  dble(ctx_model%visco(1)%pconstant(ic))
          eta_m =  dble(ctx_model%visco(2)%pconstant(ic))
          tau_s =  dble(ctx_model%visco(3)%pconstant(ic))
          tau_e =  dble(ctx_model%visco(4)%pconstant(ic))         
          
          if(tau_e*eta_l < tau_s*lambda_r) then
            if(tau_e > tol) then
              !! arbitrarily, we only change the eta_\lambda
              ctx_model%visco(1)%pconstant(ic) = tau_s*lambda_r/tau_e
            else
              !! or increase the tau_eps
              if(eta_l > tol) then
                ctx_model%visco(4)%pconstant(ic) = tau_s*lambda_r/eta_l
              else
                !! last resort on tau_s
                ctx_model%visco(3)%pconstant(ic) = tau_e*eta_l/lambda_r
              end if
            end if
          endif
          if(tau_e*eta_m < tau_s*mu_r) then
            if(tau_e > tol) then
              !! arbitrarily, we only change the eta_\mu
              ctx_model%visco(2)%pconstant(ic) = tau_s*mu_r/tau_e
            else
              !! or increase the tau_eps
              if(eta_m > tol) then
                ctx_model%visco(4)%pconstant(ic) = tau_s*mu_r/eta_m
              else
                !! last resort on tau_s
                ctx_model%visco(3)%pconstant(ic) = tau_e*eta_m/mu_r
              end if
            end if
          endif
        
        end do
      end if

      !! if Fractional model, we must ensure that the power is between 0 and 1
      if(trim(adjustl(ctx_model%viscous_model)) .eq. 'Fractional') then
        do ic = 1, n_coeff_loc
          if(ctx_model%visco(3)%pconstant(ic) < 0)                      &
             ctx_model%visco(3)%pconstant(ic) = 0
          if(ctx_model%visco(3)%pconstant(ic) > 1)                      &
             ctx_model%visco(3)%pconstant(ic) = 1
        end do
      end if

    end if 

    !! -----------------------------------------------------------------
    return
  end subroutine model_update_pconstant



  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update a given model from a selected parametrization, 
  !> for model represented per dof, at the end we give back 
  !> the models (cp,cs,rho).
  !> 
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    search         : search direction
  !> @param[in]    alpha          : step length
  !> @param[in]    param_current  : current parameter to update
  !> @param[in]    tag_function   : function of the parameter
  !> @param[in]    parametrization: problem parametrization
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_update_dof(ctx_paral,ctx_model,search,alpha,         &
                              param_current,tag_function,               &
                              parametrization,freq,ctx_err)
    implicit none
   
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_model)                      ,intent(inout):: ctx_model
    real(kind=4)                       ,intent(in)   :: search(:)
    real(kind=4)                       ,intent(in)   :: alpha
    character(len=*)                   ,intent(in)   :: param_current
    integer                            ,intent(in)   :: tag_function
    character(len=*) ,allocatable      ,intent(in)   :: parametrization(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ic, n_coeff_loc,im,nvisco
    integer                   :: n1, n2, n3, n4
    real(kind=4),allocatable  :: model(:),lambda(:),imp_p(:)
    real(kind=4),allocatable  :: vp(:),vs(:),rho(:),mu(:),imp_s(:)
    real(kind=8)              :: average
    real(kind=8)              :: loc_rho,loc_vp,loc_vs
    real(kind=8)              :: lambda_r, mu_r, eta_l, eta_m, tau_e, tau_s
    integer(kind=IKIND_MESH)  :: igb1, igb2, i_cell, ind_gb
    integer(kind=4)           :: npt_per_cell
    real(kind=8)              :: mval
    integer(kind=4)           :: k_model
    real(kind=4)              :: tol=tiny(1.0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0


    !! dof model representation ****************************************
    !! it must be the same for all models
    n1 = size(search)
    n2 = ctx_model%vp%param_dof%ndof  *ctx_model%n_cell_loc
    n3 = ctx_model%vs%param_dof%ndof  *ctx_model%n_cell_loc
    n4 = ctx_model%rho%param_dof%ndof *ctx_model%n_cell_loc
    !! they must all be the same
    if(n1 .ne. n2 .or. n1 .ne. n3 .or. n2 .ne. n3 .or. n2 .ne. n4) then
      ctx_err%msg   = "** ERROR: Inconsistent number of parameters " // &
                      "for the model representation [model_update] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    n_coeff_loc = n1
    npt_per_cell= ctx_model%vp%param_dof%ndof
    allocate(vp   (n_coeff_loc))
    allocate(vs   (n_coeff_loc))
    allocate(rho  (n_coeff_loc))
    allocate(model(n_coeff_loc))
    model = 0.0
    !! *****************************************************************

    !! -----------------------------------------------------------------
    !! 1) we get the parameter to be update
    !! Using dof *******************************************************
    !! we manually get the vp and rho.
    do i_cell = 1, ctx_model%n_cell_loc
      igb1 = (i_cell-1)*npt_per_cell + 1
      igb2 =     igb1 + npt_per_cell - 1
      
      vp (igb1:igb2) = real(ctx_model%vp%param_dof%mval (i_cell)%array(:))
      vs (igb1:igb2) = real(ctx_model%vs%param_dof%mval (i_cell)%array(:))
      rho(igb1:igb2) = real(ctx_model%rho%param_dof%mval(i_cell)%array(:))
    end do
   
    select case(trim(adjustl(param_current)))
      case(model_VP)
        model = vp
      case(model_VS)
        model = vs
      case(model_RHO)
        model = rho
      case default
        !! get model.
        do i_cell = 1, ctx_model%n_cell_loc !! each cell has several dof
          ind_gb = (i_cell-1)*ctx_model%vp%param_dof%ndof
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error
    
          !! we use the RAW models
          do k_model=1,ctx_model%vp%param_dof%ndof
            call model_eval_dof_index(ctx_model,i_cell,                 &
                                      trim(adjustl(param_current)),     &
                                      k_model,freq,mval,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            model(ind_gb+k_model) = real(mval,kind=4)
          end do
        end do
    end select
    !! -----------------------------------------------------------------
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR during model_eval [model_update] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    call apply_model_function(ctx_paral,model,tag_function,ctx_err)

    !! 2) we update model function
    model(:) = model(:) - alpha * search(:)
    !! 3) we get the identity model back
    call apply_model_function_inv(ctx_paral,model,tag_function,ctx_err)

    !! 4) we get (Vp,Vs,Rho) from the current selected parametrization

    !! -----------------------------------------------------------------
    !! depending on the parametrization we need to update
    !! the ctx_model which contains Vp and Rho
    !!
    !! -----------------------------------------------------------------

    !! =================================================================
    !! 
    !! in the case of attenuation first
    !!
    !! =================================================================
    if (trim(adjustl(param_current)) == model_ETA_LDA   .or.              &
        trim(adjustl(param_current)) == model_ETA_MU    .or.              &
        trim(adjustl(param_current)) == model_TAU_EPS   .or.              &
        trim(adjustl(param_current)) == model_TAU_SIGMA) then

      if(ctx_model%flag_viscous) then
        !! only deal with the attenuation models here
        select case(trim(adjustl(param_current)))

          !! -----------------------------------------------------------
          case(model_ETA_LDA)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV,model_visco_Maxw,model_visco_Zener)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(1)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(1)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(1)%pconstant(i_cell) = average
                end do

              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          case(model_ETA_MU)
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV,model_visco_Maxw,model_visco_Zener)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(2)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(1)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(2)%pconstant(i_cell) = average
                end do

              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          case(model_TAU_SIGMA)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_Zener)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(3)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(1)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(3)%pconstant(i_cell) = average
                end do

              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
          case(model_TAU_EPS)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_Zener)
                do i_cell = 1, ctx_model%n_cell_loc
                  igb1 = (i_cell-1)*npt_per_cell + 1
                  igb2 =     igb1 + npt_per_cell - 1
                  ctx_model%visco(4)%param_dof%mval(i_cell)%array(:) =  &
                    model(igb1:igb2)
                  average = dble(1.d0/dble(npt_per_cell)                &
                          *sum(ctx_model%visco(1)%param_dof%mval(i_cell)%array))
                  ctx_model%visco(4)%pconstant(i_cell) = average
                end do

              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [model_update] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select
        end select !! end for the current model
      else
        ctx_err%msg   = "** ERROR: Inconsistent use of  elastic " //    &
                        "models for attenuation [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
      end if !! end if attenuation  
    !! =================================================================

    !! =================================================================    
    !! for the non-attenuated model, it depends on the parametrization
    else
      !! load models in case 
      allocate(lambda(n_coeff_loc))
      allocate(mu    (n_coeff_loc))
      allocate(imp_p (n_coeff_loc))
      allocate(imp_s (n_coeff_loc))
      mu     = rho*vs*vs
      lambda = rho*vp*vp - 2.0*mu
      imp_p   = rho*vp
      imp_s   = rho*vs

      ! ----------------------------------------------------------------
      !! parametrization Vp, Vs, Rho
      if(any(parametrization == model_RHO) .and.                        &
         any(parametrization == model_VP)  .and.                        &
         any(parametrization == model_VS)) then

        select case(trim(adjustl(param_current)))
          case(model_RHO)          
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%rho%param_dof%mval(i_cell)%array(:)=model(igb1:igb2)
            end do
          
          case(model_VP)
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%vp%param_dof%mval(i_cell)%array(:)=model(igb1:igb2)
            end do

          case(model_VS)
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%vs%param_dof%mval(i_cell)%array(:)=model(igb1:igb2)
            end do
          case default
            ctx_err%ierr = -1
        end select


      ! ----------------------------------------------------------------
      !! parametrization Lambda, Mu, Rho
      !! Rho = K / Vp²
      elseif(any(parametrization == model_RHO)     .and.                &
         any(parametrization == model_LAMBDA)  .and.                    &
         any(parametrization == model_MU))   then

        select case(trim(adjustl(param_current)))
          case(model_RHO)          
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%rho%param_dof%mval(i_cell)%array(:)=model(igb1:igb2)
            end do

          case(model_MU)           
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%vs%param_dof%mval(i_cell)%array(:)=             &
                sqrt(model(igb1:igb2)/ctx_model%rho%param_dof%mval(i_cell)%array(:))
              ctx_model%vp%param_dof%mval(i_cell)%array(:)=             &
                sqrt((lambda(igb1:igb2) + 2.d0*model(igb1:igb2))        &
                /ctx_model%rho%param_dof%mval(i_cell)%array(:))
            end do

          case(model_LAMBDA)
            do i_cell = 1, ctx_model%n_cell_loc
              igb1 = (i_cell-1)*npt_per_cell + 1
              igb2 =     igb1 + npt_per_cell - 1
              ctx_model%vp%param_dof%mval(i_cell)%array(:)=             &
                sqrt((model(igb1:igb2) + 2.d0*mu(igb1:igb2))            &
                /ctx_model%rho%param_dof%mval(i_cell)%array(:))
            end do

          case default
            ctx_err%ierr = -1
        end select

      ! ----------------------------------------------------------------
      !! Error
      else
        ctx_err%msg   = "** ERROR: Unrecognized parametrization [model_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      deallocate(lambda)
      deallocate(mu    )
      deallocate(imp_p )
      deallocate(imp_s )
    end if
    ! ----------------------------------------------------------------
    deallocate(model)
    deallocate(vp)
    deallocate(vs)
    deallocate(rho)

    !! in case of error ------------------------------------------------
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: Unrecognized parametrization [model_update] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 

    !! if everything is ok so far, we have to adjust min and max values
    !! as well as the piecewise-constant averaged value.
    !! -----------------------------------------------------------------
    do i_cell = 1, ctx_model%n_cell_loc
      do ic=1, npt_per_cell
        if(ctx_model%vp%param_dof%mval(i_cell)%array(ic) < ctx_model%vp_min) &
           ctx_model%vp%param_dof%mval(i_cell)%array(ic) = ctx_model%vp_min
        if(ctx_model%vp%param_dof%mval(i_cell)%array(ic) > ctx_model%vp_max) &
           ctx_model%vp%param_dof%mval(i_cell)%array(ic) = ctx_model%vp_max
        if(ctx_model%vs%param_dof%mval(i_cell)%array(ic) < ctx_model%vs_min) &
           ctx_model%vs%param_dof%mval(i_cell)%array(ic) = ctx_model%vs_min
        if(ctx_model%vs%param_dof%mval(i_cell)%array(ic) > ctx_model%vs_max) &
           ctx_model%vs%param_dof%mval(i_cell)%array(ic) = ctx_model%vs_max
        if(ctx_model%rho%param_dof%mval(i_cell)%array(ic)< ctx_model%rho_min)&
           ctx_model%rho%param_dof%mval(i_cell)%array(ic)= ctx_model%rho_min
        if(ctx_model%rho%param_dof%mval(i_cell)%array(ic)> ctx_model%rho_max)&
           ctx_model%rho%param_dof%mval(i_cell)%array(ic)= ctx_model%rho_max
      end do
      average = dble(                                                   &
                sum(ctx_model%rho%param_dof%mval(i_cell)%array)/npt_per_cell)
      ctx_model%rho%pconstant(i_cell) = average
      average = dble(                                                   &
                sum(ctx_model%vp%param_dof%mval(i_cell)%array)/npt_per_cell)
      ctx_model%vp%pconstant(i_cell) = average
      average = dble(                                                   &
                sum(ctx_model%vs%param_dof%mval(i_cell)%array)/npt_per_cell)
      ctx_model%vs%pconstant(i_cell) = average
    end do
    
    if(ctx_model%flag_viscous) then
      !! if Zener model, we must ensure that the \tau are consistent
      !! we must respect \tau_eps * \eta_lambda > \tau_sugma \lambda
      if(trim(adjustl(ctx_model%viscous_model)) .eq. model_visco_Zener) then
        do i_cell = 1, ctx_model%n_cell_loc
          do ic=1, npt_per_cell
            !! get the parameters
            loc_rho   = dble(ctx_model%rho%param_dof%mval(i_cell)%array(ic))
            loc_vp    = dble(ctx_model%vp%param_dof%mval(i_cell)%array(ic))
            loc_vs    = dble(ctx_model%vs%param_dof%mval(i_cell)%array(ic))
            mu_r      = (loc_vs**2) * loc_rho 
            lambda_r  = (loc_vp**2) * loc_rho - 2.d0*mu_r
            
            eta_l = dble(ctx_model%visco(1)%param_dof%mval(i_cell)%array(ic))
            eta_m = dble(ctx_model%visco(2)%param_dof%mval(i_cell)%array(ic))
            tau_s = dble(ctx_model%visco(3)%param_dof%mval(i_cell)%array(ic))
            tau_e = dble(ctx_model%visco(4)%param_dof%mval(i_cell)%array(ic))
            
            if(tau_e*eta_l < tau_s*lambda_r) then
              if(tau_e > tol) then
                !! arbitrarily, we only increase the eta_\lambda
                ctx_model%visco(1)%param_dof%mval(i_cell)%array(ic) =   &
                  tau_s*lambda_r/tau_e*1.02
              else
                !! or increase the tau_eps
                if(eta_l > tol) then
                  ctx_model%visco(4)%param_dof%mval(i_cell)%array(ic) = &
                    tau_s*lambda_r/eta_l*1.02
                else
                  !! last resort on tau_s
                  ctx_model%visco(3)%param_dof%mval(i_cell)%array(ic) = &
                    tau_e*eta_l/lambda_r*0.98
                end if
              end if
            endif
            if(tau_e*eta_m < tau_s*mu_r) then
              if(tau_e > tol) then
                !! arbitrarily, we only change the eta_\mu
                ctx_model%visco(2)%param_dof%mval(i_cell)%array(ic) =   &
                  tau_s*mu_r/tau_e*1.02
              else
                !! or increase the tau_eps
                if(eta_m > tol) then
                  ctx_model%visco(4)%param_dof%mval(i_cell)%array(ic) = &
                    tau_s*mu_r/eta_m*1.02
                else
                  !! last resort on tau_s
                  ctx_model%visco(3)%param_dof%mval(i_cell)%array(ic) = &
                    tau_e*eta_m/mu_r*0.98
                end if
              end if
            endif
          end do
        end do
      end if

      nvisco = size(ctx_model%visco)
      do im = 1, nvisco
        do i_cell = 1, ctx_model%n_cell_loc
          do ic=1, npt_per_cell
            if(ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) <   &
                                               ctx_model%visco_min(im)) &
               ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) =   &
                                               ctx_model%visco_min(im)
            if(ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) >   &
                                               ctx_model%visco_max(im)) &
               ctx_model%visco(im)%param_dof%mval(i_cell)%array(ic) =   &
                                               ctx_model%visco_max(im)
          end do
          average =dble(                                                &
                  sum(ctx_model%visco(im)%param_dof%mval(i_cell)%array) &
                  / npt_per_cell)
          ctx_model%visco(im)%pconstant(i_cell) = average          
        end do
      end do
    end if 
    !! -----------------------------------------------------------------
    return
  end subroutine model_update_dof

end module m_model_update
