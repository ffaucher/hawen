!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_grad_metric.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the possible metric for the inner product
!> during the gradient computation
!
!------------------------------------------------------------------------------
module m_grad_metric
  
  use m_raise_error,     only: raise_error, t_error

  implicit none

  !>
  !> classical inner product,          <u,v>  = u*v
  integer, parameter :: tag_GRADMETRIC_ID        = 1 
  !> scale using the cell area         <u,v>  = u*v / area(cell)
  integer, parameter :: tag_GRADMETRIC_AREA      = 2
  !> scale using the mass matrix       <u,v>  = u* M v 
  integer, parameter :: tag_GRADMETRIC_MASS      = 10
  !> scale using the inv mass matrix    <u,v> = u* M^{-1} v 
  integer, parameter :: tag_GRADMETRIC_MASS_INV  = 11
  !> !> scale using the stiff matrix       <u,v>  = u* K v 
  !> integer, parameter :: tag_GRADMETRIC_STIFF     = 20
  !> !> scale using the inv stiff matrix    <u,v> = u* K^{-1} v 
  !> integer, parameter :: tag_GRADMETRIC_STIFF_INV = 21

  private

  public :: tag_GRADMETRIC_ID,tag_GRADMETRIC_AREA,tag_GRADMETRIC_MASS
  public :: tag_GRADMETRIC_MASS_INV,grad_metric_formalism,grad_metric_naming

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> decide the metric from keyword
  !
  !> @param[in]   name_func_in  : input name
  !> @param[out]  tag_func_out  : output tag
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine grad_metric_formalism(name_func_in,tag_func_out,ctx_err)
    implicit none

    character(len=*)   ,intent(in)   :: name_func_in
    integer            ,intent(out)  :: tag_func_out
    type(t_error)      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! no need, inout ctx_err%ierr  = 0

    select case(trim(adjustl(name_func_in)))
      case('identity','id','ID','Id','Identity','IDENTITY','None','NONE','none')
        tag_func_out = tag_GRADMETRIC_ID
      case('area','AREA','Area','cell_area','Cell_area','CELL_AREA','Cell_Area')
        tag_func_out = tag_GRADMETRIC_AREA
      case('mass','MASS','Mass','mass_matrix','MASS_MATRIX')
        tag_func_out = tag_GRADMETRIC_MASS
      case('mass_inv','MASS_INV','Mass_Inv','mass_matrix_inverse',      &
           'MASS_MATRIX_INVERSE')
        tag_func_out = tag_GRADMETRIC_MASS_INV

      ! ----------------------------------------------------------------
      !! stiffness does not give a positive symmetric matrix here...
      ! ----------------------------------------------------------------
      !! case('stiff','STIFF','Stiff','stiff_matrix','stiffness',          &
      !!      'STIFF_MATRIX','stiffness_matrix')
      !!   tag_func_out = tag_GRADMETRIC_STIFF
      !! case('stiff_inv','STIFF_INV','Stiff_Inv','stiff_matrix_inverse',  &
      !!      'STIFF_MATRIX_INVERSE','stiffness_matrix_inverse')
      !!   tag_func_out = tag_GRADMETRIC_STIFF_INV
      case default
        ctx_err%msg   = "** ERROR: Unrecognized metric for gradient " //&
                        "[grad_metric_formalism]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! ------------------------------------------------------------------

    return

  end subroutine grad_metric_formalism


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> from the tag, we extract a word for printing
  !
  !> @param[in]   tag_in        : input tag
  !> @param[out]  str           : string out
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine grad_metric_naming(tag_in,str,ctx_err)
    implicit none

    integer                            ,intent(in)   :: tag_in
    character(len=128)                 ,intent(out)  :: str
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    str = ''
    select case(tag_in)
      case(tag_GRADMETRIC_ID       )
        str = 'identity'
      case(tag_GRADMETRIC_AREA     )
        str = 'cell area scaling'
      case(tag_GRADMETRIC_MASS     )
        str = 'mass matrix'
      case(tag_GRADMETRIC_MASS_INV )
        str = 'inverse mass matrix'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized tag for gradient "    //&
                        "metric [grad_metric_naming]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return

  end subroutine grad_metric_naming
  
end module m_grad_metric
