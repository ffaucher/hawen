!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_parameter_gradient.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the routine to convert the reference gradient
!> towards the gradient w.r.t the wanted physical parameters for the
!> HARMONIC ELASTIC ISO EQUATION.
!
!------------------------------------------------------------------------------
module m_parameter_gradient

  !! module used -------------------------------------------------------
  use m_define_precision,         only: IKIND_MESH
  use m_raise_error,              only: raise_error, t_error
  use m_ctx_model,                only: t_model
  use m_model_eval,               only: model_eval_raw
  use m_model_list
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,tag_MODEL_DOF
  !! -------------------------------------------------------------------
  implicit none

  private
  public   :: parameter_gradient
  private  :: parameter_gradient_pconstant

  contains
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical parameter
  !> for the HARMONIC ELASTIC ISOTROPIC propagation
  !> The input gradient is given w.r.t:
  !>  a) lambda
  !>  b) mu
  !>  c) rho
  !! -------------------------------------------------------------------
  !> we have the followin relation
  !>   cp    = \sqrt( (\lambda + 2\mu)/\rho )
  !>   cs    = \sqrt(  \mu/\rho )
  !>   ip    = 
  !>   is    = 
  !> bulk    = 
  !> poisson = 
  !! -----------------------------
  !> for the derivative we have the
  !> \partial_m1 \tilde{J) (m1, m2, m3)
  !>             =   \partial_\lambda J \partial_m1 \lambda
  !>               + \partial_\mu     J \partial_m1 \mu
  !>               + \partial_\rho    J \partial_m1 \rho
  !>
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    freq           : angular frequency
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_pseudoh : pseudo-hessian need power squared
  !----------------------------------------------------------------------------
  subroutine parameter_gradient(ctx_model,grad_in,grad_out,str_model,   &
                                freq,ctx_err,o_flag_pseudoh)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    logical         ,optional          ,intent(in)   :: o_flag_pseudoh
    !! 
    integer    :: power
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power = 2
    end if
    !! 
    !! 
    select case (trim(adjustl(ctx_model%format_disc))) 
      case(tag_MODEL_PCONSTANT) 
        call parameter_gradient_pconstant(ctx_model,grad_in,grad_out,   &
                                          str_model,freq,power,ctx_err)      
      case(tag_MODEL_DOF) 
        call parameter_gradient_dof(ctx_model,grad_in,grad_out,         &
                                    str_model,power,ctx_err)
      case default
        ctx_err%msg   = "** ERROR: only piecewise-constant or dof are"//&
                        " supported to parametrize the gradient " //    &
                        " [parameter_gradient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine parameter_gradient

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical parameter
  !> for the HARMONIC ELASTIC ISOTROPIC propagation
  !> The input gradient is given w.r.t:
  !>  a) lambda
  !>  b) mu
  !>  c) rho
  !! -------------------------------------------------------------------
  !> we have the followin relation
  !>   cp    = \sqrt( (\lambda + 2\mu)/\rho )
  !>   cs    = \sqrt(  \mu/\rho )
  !>   ip    = 
  !>   is    = 
  !> bulk    = 
  !> poisson = 
  !! -----------------------------
  !> for the derivative we have the
  !> \partial_m1 \tilde{J) (m1, m2, m3)
  !>             =   \partial_\lambda J \partial_m1 \lambda
  !>               + \partial_\mu     J \partial_m1 \mu
  !>               + \partial_\rho    J \partial_m1 \rho
  !>
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    freq           : angular frequency
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_pseudoh : pseudo-hessian need power squared
  !----------------------------------------------------------------------------
  subroutine parameter_gradient_pconstant(ctx_model,grad_in,grad_out,   &
                                          str_model,freq,power,ctx_err)
    implicit none

    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(in)   :: power
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=8)              :: d11,d12,d21,d22,d33,rho,vp,vs,Qp,Qs
    real(kind=8)              :: impp,imps,poiss,kappa,lambda0,mu0
    real(kind=8)              :: eta_l,eta_m
    real(kind=8),allocatable  :: gradl(:),gradm(:)
    real(kind=8),allocatable  :: gradcp(:),gradcs(:),gradr(:)
    real(kind=8),allocatable  :: gradip(:),gradis(:),gradpo(:),gradka(:)
    real(kind=8),allocatable  :: grad_etal(:),grad_etam(:),grad_fpower(:)
    real(kind=8),allocatable  :: grad_taus(:),grad_taue(:)
    real(kind=8),allocatable  :: grad_qs(:),grad_qp(:)
    real(kind=8),allocatable  :: xpt_refelem(:),xpt_global(:)
    integer                   :: k,n_coeff_loc,counter
    integer(kind=IKIND_MESH)  :: c
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    if(ctx_model%dim_domain.ne.2 .and. ctx_model%dim_domain.ne.3) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helmholtz equation in elastic-iso medium "   //  &
                      "[parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! *****************************************************************
    !! USING PIECEWISE-CONSTANT MODEL REPRESENTATION 
    !! *****************************************************************
    n_coeff_loc = ctx_model%n_cell_loc
    !! *****************************************************************

    !! =================================================================
    !!
    !! if we want (lambda,mu,rho) then we already have them 
    !!
    !! =================================================================
    if(any(str_model == model_LAMBDA) .and. any(str_model == model_MU)  &
      .and. any(str_model == model_RHO)) then
      
      !! we already have the gradient then
      allocate(gradl(n_coeff_loc)) ; gradl=grad_in(:,1)
      allocate(gradm(n_coeff_loc)) ; gradm=grad_in(:,2)
      allocate(gradr(n_coeff_loc)) ; gradr=grad_in(:,3)

    !! =================================================================
    !! PARAMETRIZATION: (VP,VS,RHO) ----------------------------------
    !! \mu     = cs² \rho
    !! \lambda = cp² \rho - 2\mu = cp² \rho - 2 cs² \rho
    !!     (o) \partial_cp \lambda  = 2*cp*\rho
    !!     (o) \partial_cp \mu      = 0
    !!     (o) \partial_cs \lambda  =-4*cs*\rho      
    !!     (o) \partial_cs \mu      = 2*cs*\rho
    !! =================================================================
    elseif(any(str_model == model_VP) .and. any(str_model == model_VS)  &
                                      .and. any(str_model == model_RHO)) then
      allocate(gradcp(n_coeff_loc)) ; gradcp=0.0
      allocate(gradcs(n_coeff_loc)) ; gradcs=0.0
      allocate(gradr (n_coeff_loc)) ; gradr =0.0
      allocate(xpt_refelem(ctx_model%dim_domain))
      allocate(xpt_global (ctx_model%dim_domain))
      !! using piecewise-constant, it does not matter
      xpt_refelem= 0.d0
      xpt_global = 0.d0
      d33  = 1.0
      do c=1,n_coeff_loc

        !! we use the RAW models defined from lambda_0 and mu_0 and 
        !! not the real part of the wave speeds which can be different
        !! depending on the viscosity models.
        call model_eval_raw(ctx_model,model_VP,c,xpt_refelem,xpt_global,&
                        freq,vp,ctx_err,o_force_format=tag_MODEL_PCONSTANT)
        if(ctx_err%ierr .ne. 0) return 
        
        call model_eval_raw(ctx_model,model_VS,c,xpt_refelem,xpt_global,&
                        freq,vs,ctx_err,o_force_format=tag_MODEL_PCONSTANT)
        if(ctx_err%ierr .ne. 0) return

        call model_eval_raw(ctx_model,model_RHO,c,xpt_refelem,xpt_global,&
                        freq,rho,ctx_err,o_force_format=tag_MODEL_PCONSTANT)
        if(ctx_err%ierr .ne. 0) return
        
        d11  = dble(( 2.*vp*rho)**power)
        d22  = dble(( 2.*vs*rho)**power)
        d21  = dble((-4.*vs*rho)**power)
        gradcp(c) = dble(d11*grad_in(c,1))
        gradcs(c) = dble(d21*grad_in(c,1)+d22*grad_in(c,2))
        gradr (c) = dble(                 d33*grad_in(c,3))
      end do
      deallocate(xpt_refelem)
      deallocate(xpt_global )

    !! =================================================================
    !! PARAMETRIZATION: (IP,IS,RHO) ----------------------------------
    !! \mu     = Is²/\rho
    !! \lambda = Ip²/\rho - 2\mu = Ip²/\rho - 2 Is²/\rho
    !!     (o) \partial_Ip \lambda  = 2*Ip*/\rho
    !!     (o) \partial_Ip \mu      = 0
    !!     (o) \partial_Is \lambda  =-4*Is/\rho
    !!     (o) \partial_Is \mu      = 2*Is/\rho
    !! =================================================================
    elseif(any(str_model == model_IP) .and. any(str_model == model_IS)  &
                                      .and. any(str_model == model_RHO)) then
      
      allocate(gradip(n_coeff_loc)) ; gradip=0.0
      allocate(gradis(n_coeff_loc)) ; gradis=0.0
      allocate(gradr (n_coeff_loc)) ; gradr =0.0
      allocate(xpt_refelem(ctx_model%dim_domain))
      allocate(xpt_global (ctx_model%dim_domain))
      !! using piecewise-constant, it does not matter
      xpt_refelem= 0.d0
      xpt_global = 0.d0
      d33  = 1.0
      do c=1,n_coeff_loc
        !! we use the RAW models defined from lambda_0 and mu_0 and 
        !! not the real part of the wave speeds which can be different
        !! depending on the viscosity models.
        call model_eval_raw(ctx_model,model_IP,c,xpt_refelem,xpt_global,&
                        freq,impp,ctx_err,o_force_format=tag_MODEL_PCONSTANT)
        if(ctx_err%ierr .ne. 0) return 
        
        call model_eval_raw(ctx_model,model_IS,c,xpt_refelem,xpt_global,&
                        freq,imps,ctx_err,o_force_format=tag_MODEL_PCONSTANT)
        if(ctx_err%ierr .ne. 0) return

        call model_eval_raw(ctx_model,model_RHO,c,xpt_refelem,xpt_global,&
                        freq,rho,ctx_err,o_force_format=tag_MODEL_PCONSTANT)
        if(ctx_err%ierr .ne. 0) return

        d11  = dble(( 2.*impp/rho)**power)
        d22  = dble(( 2.*imps/rho)**power)
        d21  = dble((-4.*imps/rho)**power)
        gradip(c) = dble(d11*grad_in(c,1)                 )
        gradis(c) = dble(d21*grad_in(c,1)+d22*grad_in(c,2))
        gradr (c) = dble(                 d33*grad_in(c,3))
      end do
      deallocate(xpt_refelem)
      deallocate(xpt_global )

    !! =================================================================
    !! PARAMETRIZATION: (BULK,POISSON,RHO) ---------------------------
    !! (2D)  lambda = 2 \nu \kappa
    !!       mu     = \kappa (1 - 2\nu)
    !! (3D)  lambda = [1 - (1 - 2\nu)/(\nu + 1)] \kappa
    !!       mu     = (3/2) * \kappa * (1 - 2\nu)/(\nu + 1)
    !!
    !! =================================================================
    elseif(any(str_model==model_BULK) .and. any(str_model==model_RHO)   &
                             .and. any(str_model == model_POISSON)) then
      allocate(gradpo(n_coeff_loc)) ; gradpo=0.0
      allocate(gradka(n_coeff_loc)) ; gradka=0.0
      allocate(gradr (n_coeff_loc)) ; gradr =0.0
      allocate(xpt_refelem(ctx_model%dim_domain))
      allocate(xpt_global (ctx_model%dim_domain))
      !! using piecewise-constant, it does not matter
      xpt_refelem= 0.d0
      xpt_global = 0.d0
      d33  = 1.0
      select case(ctx_model%dim_domain)
        case(2)
          do c=1,n_coeff_loc
            !! we use the RAW models      
            call model_eval_raw(ctx_model,model_BULK,c,xpt_refelem,     &
                                xpt_global,freq,kappa,ctx_err,          &
                                o_force_format=tag_MODEL_PCONSTANT)
            if(ctx_err%ierr .ne. 0) return

            call model_eval_raw(ctx_model,model_POISSON,c,xpt_refelem,  &
                                xpt_global,freq,poiss,ctx_err,          &
                                o_force_format=tag_MODEL_PCONSTANT)
            if(ctx_err%ierr .ne. 0) return

            d11  = dble((    2.*poiss)**power)
            d12  = dble((1 - 2.*poiss)**power)
            d21  = dble((    2.*kappa)**power)
            d22  = dble((   -2.*kappa)**power)
            gradpo(c) = dble(d11*grad_in(c,1)+d12*grad_in(c,2))
            gradka(c) = dble(d21*grad_in(c,1)+d22*grad_in(c,2))
            gradr (c) = dble(                 d33*grad_in(c,3))
          end do
        case(3)
          do c=1,n_coeff_loc
            !! we use the RAW models       
            call model_eval_raw(ctx_model,model_BULK,c,xpt_refelem,     &
                                xpt_global,freq,kappa,ctx_err,          &
                                o_force_format=tag_MODEL_PCONSTANT)
            if(ctx_err%ierr .ne. 0) return

            call model_eval_raw(ctx_model,model_POISSON,c,xpt_refelem,  &
                                xpt_global,freq,poiss,ctx_err,          &
                                o_force_format=tag_MODEL_PCONSTANT)
            if(ctx_err%ierr .ne. 0) return

            d11  = dble((1. -  (1.-2.*poiss)/(poiss + 1.))**power)
            d12  = dble((3./2.*(1.-2.*poiss)/(poiss + 1.))**power)
            d21  = dble((kappa*3./(poiss + 1.)**2)**power)
            d22  = dble((-3./2.*kappa*3./(poiss + 1.)**2)**power)
            gradpo(c) = dble(d11*grad_in(c,1)+d12*grad_in(c,2))
            gradka(c) = dble(d21*grad_in(c,1)+d22*grad_in(c,2))
            gradr (c) = dble(                 d33*grad_in(c,3))
          end do
        case default
          ctx_err%msg   = "** ERROR: incorrect dimension [param_grad] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      deallocate(xpt_refelem)
      deallocate(xpt_global )
      ! ----------------------------------------------------------------
      !! REMARK: 9 Parameters, would lead to 9*8*7 = 504 combinations, 
      !! because of symmetry we have 3 among 9 = nchoosek(9,3) = 84 
      ! ----------------------------------------------------------------
      !! PARAMETRIZATION: (LAMBDA,MU,POISSON) --------------------------
      !! PARAMETRIZATION: (LAMBDA,MU,BULK) -----------------------------
      !! PARAMETRIZATION: (LAMBDA,MU,VP) -------------------------------
      !! PARAMETRIZATION: (LAMBDA,MU,VS) -------------------------------
      !! PARAMETRIZATION: (LAMBDA,MU,IP) -------------------------------
      !! PARAMETRIZATION: (LAMBDA,MU,IS) -------------------------------

      !! PARAMETRIZATION: (IP,IS,POISSON) ------------------------------
      !! PARAMETRIZATION: (IP,IS,BULK) ---------------------------------
      !! PARAMETRIZATION: (IP,IS,VP) -----------------------------------
      !! PARAMETRIZATION: (IP,IS,VS) -----------------------------------
      !! PARAMETRIZATION: (IP,IS,LAMBDA) -------------------------------
      !! PARAMETRIZATION: (IP,IS,MU) -----------------------------------

      !! PARAMETRIZATION: (VP,VS,POISSON) ------------------------------
      !! PARAMETRIZATION: (VP,VS,BULK) ---------------------------------
      !! PARAMETRIZATION: (VP,VS,IP) -----------------------------------
      !! PARAMETRIZATION: (VP,VS,IS) -----------------------------------
      !! PARAMETRIZATION: (VP,VS,LAMBDA) -------------------------------
      !! PARAMETRIZATION: (VP,VS,MU) -----------------------------------

    !! =================================================================
    !! Unsupported version
    !! =================================================================
    else
      ctx_err%msg   = "** ERROR: Unrecognized set for parametrization"//&
                      " [parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! =================================================================
    !! 
    !! in the case of attenuation 
    !!
    !! =================================================================
    if(ctx_model%flag_viscous) then
      !! make sure that all viscosity models are acounted for
      counter=0
      allocate(xpt_refelem(ctx_model%dim_domain))
      allocate(xpt_global (ctx_model%dim_domain))
      !! using piecewise-constant, it does not matter
      xpt_refelem= 0.d0
      xpt_global = 0.d0
      
      !! there is the possibility to work with the Quality factors, 
      !! which depends on the model, otherwise, it can be the default
      !! attenuation models. 
      !! Here it only works if we have Qp AND Qs, we cannot mix with 
      !! other attenuations parameters.
      if(any(str_model==model_QFACTORP) .and.                           &
         any(str_model==model_QFACTORS)) then

        !! only ok for lambda, mu, rho parametrization
        if(any(str_model == model_LAMBDA) .and.                         &
           any(str_model == model_MU)     .and.                         &
           any(str_model == model_RHO)) then
        else
          ctx_err%msg   = "** ERROR: the parametrization with quality"//&
                          " factors is only possible from "           //&
                          "(\lambda,\mu,\rho) parametrization "       //&
                          "[parameter_gradient] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

        counter = counter + 2
        allocate(grad_qp(n_coeff_loc)) ; grad_qp = 0.d0
        allocate(grad_qs(n_coeff_loc)) ; grad_qs = 0.d0
        
        !! it depends on the model
        select case(trim(adjustl(ctx_model%viscous_model)))
          
          case(model_visco_KV)
            !! we have Qp = (\lambda0 + 2 \mu0) / (\omega \eta_\lambda + 2\eta_\mu)
            !! we have Qs = \mu0 / (\omega \eta_\mu)
            !! at this point we have the gradient with respect to lambda0, etc.
            do c=1,n_coeff_loc

              !! piecewise constant here.
              call model_eval_raw(ctx_model,model_VP      ,c,xpt_refelem,&
              xpt_global,freq,vp,ctx_err     ,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_VS      ,c,xpt_refelem,&
              xpt_global,freq,vs,ctx_err     ,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_RHO     ,c,xpt_refelem,&
              xpt_global,freq,rho,ctx_err    ,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_LAMBDA  ,c,xpt_refelem,&
              xpt_global,freq,lambda0,ctx_err,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_MU      ,c,xpt_refelem,&
              xpt_global,freq,mu0,ctx_err    ,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_ETA_LDA ,c,xpt_refelem,&
              xpt_global,freq,eta_l,ctx_err  ,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_ETA_MU  ,c,xpt_refelem,&
              xpt_global,freq,eta_m,ctx_err  ,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_QFACTORP,c,xpt_refelem,&
              xpt_global,freq,Qp,ctx_err     ,o_force_format=tag_MODEL_PCONSTANT)
              call model_eval_raw(ctx_model,model_QFACTORS,c,xpt_refelem,&
              xpt_global,freq,Qs,ctx_err     ,o_force_format=tag_MODEL_PCONSTANT)

              !! careful with complex frequencies
              !! *******************************************************
              !! this is parametrization 
              !!   (lambda, mu, rho, Qp, Qs)
              !! so Qp and Qs do not depend on lambda0 nor mu0
              !! *******************************************************
              ! grad_qs(c) = grad_in(c,2) * (aimag(freq)-real(freq))*eta_m&
              !            - grad_in(c,5) * mu0/(aimag(freq)-real(freq))  &
              !                           * 1.d0/(Qs*Qs)
              grad_qs(c) =-grad_in(c,5) * mu0/(aimag(freq)-real(freq))  &
                                        * 1.d0/(Qs*Qs)
              
              !! same with Qp 
              ! grad_qp(c) = (aimag(freq)-real(freq)) * (eta_l+2.d0*eta_m)&
              !            * (grad_in(c,1) + grad_in(c,2)/2.d0)           &
              !            - (lambda0+2.d0*mu0) / (aimag(freq)-real(freq))&
              !            / (Qp*Qp) * (grad_in(c,4) + grad_in(c,5)/2.d0)
              grad_qp(c) =-(lambda0+2.d0*mu0) / (aimag(freq)-real(freq))&
                          /(Qp*Qp) * (grad_in(c,4) + grad_in(c,5)/2.d0)
            end do

          ! case(model_visco_Frac)
          ! case(model_visco_Zener)
          ! case(model_visco_Maxw)
          case default
            ctx_err%msg   = "** ERROR: Unrecognized viscosity model " //&
                            " [parameter_gradient] **"
            ctx_err%ierr  = -1
            call raise_error(ctx_err)
        end select
      end if 
      
      !! ---------------------------------------------------------------
      !! then we do the other models, maybe everything is already taken
      !! care of, but it depends on the model, e.g., Zener also have the
      !! tau parameters.
      !! ---------------------------------------------------------------
      do k=1,ctx_model%n_model
        select case(trim(adjustl(str_model(k))))
          case(model_ETA_LDA)
            counter = counter + 1 
            allocate(grad_etal(n_coeff_loc)) ; grad_etal = grad_in(:,4)
            
          case(model_ETA_MU)
            counter = counter + 1 
            allocate(grad_etam(n_coeff_loc)) ; grad_etam = grad_in(:,5)
            
          case(model_TAU_SIGMA)
            counter = counter + 1 
            allocate(grad_taus(n_coeff_loc)) ; grad_taus = grad_in(:,6)

          case(model_FPOWER)
            counter = counter + 1 
            allocate(grad_fpower(n_coeff_loc)) ; grad_fpower = grad_in(:,6)
            
          case(model_TAU_EPS)
            counter = counter + 1 
            allocate(grad_taue(n_coeff_loc)) ; grad_taue = grad_in(:,7)

        end select
      end do
      
      if(counter .ne.  size(ctx_model%visco)) then
        ctx_err%msg   = "** ERROR: some parameters for attenuation " // &
                        "are missing [parameter_gradient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      
      deallocate(xpt_refelem)
      deallocate(xpt_global )
    end if
    !! =================================================================

    !! =================================================================
    !! 
    !! we now save the main gradient in the out array.
    !!
    !! =================================================================
    do k=1,ctx_model%n_model
      select case(trim(adjustl(str_model(k))))
        case(model_LAMBDA)
          grad_out(:,k) = real(gradl(:))
        case(model_MU)
          grad_out(:,k) = real(gradm(:))
        case(model_VP)
          grad_out(:,k) = real(gradcp(:))
        case(model_VS)
          grad_out(:,k) = real(gradcs(:))
        case(model_IP)
          grad_out(:,k) = real(gradip(:))
        case(model_IS)
          grad_out(:,k) = real(gradis(:))
        case(model_BULK)
          grad_out(:,k) = real(gradka(:))
        case(model_POISSON)
          grad_out(:,k) = real(gradpo(:))
        case(model_RHO)
          grad_out(:,k) = real(gradr(:))
        case(model_ETA_LDA)
          grad_out(:,k) = real(grad_etal(:))
        case(model_ETA_MU)
          grad_out(:,k) = real(grad_etam(:))
        case(model_TAU_SIGMA)
          grad_out(:,k) = real(grad_taus(:))
        case(model_FPOWER)
          grad_out(:,k) = real(grad_fpower(:)) 
        case(model_TAU_EPS)
          grad_out(:,k) = real(grad_taue(:))
        case(model_QFACTORP)
          grad_out(:,k) = real(grad_qp(:))
        case(model_QFACTORS)
          grad_out(:,k) = real(grad_qs(:))
        case default
          ctx_err%msg   = "** ERROR: Unrecognized model name "    //  &
                          "[parameter_gradient] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end do
    ! ----------------------------------------------------------------

    if(allocated(gradcp))       deallocate(gradcp)
    if(allocated(gradcs))       deallocate(gradcs)
    if(allocated(gradip))       deallocate(gradip)
    if(allocated(gradis))       deallocate(gradis)
    if(allocated(gradpo))       deallocate(gradpo)
    if(allocated(gradka))       deallocate(gradka)
    if(allocated(gradr))        deallocate(gradr)
    if(allocated(gradm))        deallocate(gradm)
    if(allocated(gradl))        deallocate(gradl)
    if(allocated(grad_etal))    deallocate(grad_etal)  
    if(allocated(grad_etam))    deallocate(grad_etam)  
    if(allocated(grad_taue))    deallocate(grad_taue)  
    if(allocated(grad_taus))    deallocate(grad_taus)  
    if(allocated(grad_fpower))  deallocate(grad_fpower)
    if(allocated(grad_qp))      deallocate(grad_qp)
    if(allocated(grad_qs))      deallocate(grad_qs)
    !! -----------------------------------------------------------------

    return
  end subroutine parameter_gradient_pconstant


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the gradient with respect to the appropriate physical 
  !> parameter for the HELMHOLTZ ELASTIC propagation
  !> The input gradient is given w.r.t:
  !>  a) lambda
  !>  b) mu
  !>  c) rho
  !! -------------------------------------------------------------------
  !
  !> @param[in]    ctx_model      : context_model
  !> @param[in]    grad_in        : gradient_in
  !> @param[in]    grad_out       : gradient_out
  !> @param[in]    power          : power = 2 if pseudo-hessian
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parameter_gradient_dof(ctx_model,grad_in,grad_out,         &
                                    str_model,power,ctx_err)
    implicit none

    type(t_model)                      ,intent(in)   :: ctx_model
    real            ,allocatable       ,intent(in)   :: grad_in (:,:)
    real            ,allocatable       ,intent(inout):: grad_out(:,:)
    character(len=*),allocatable       ,intent(in)   :: str_model(:)
    integer                            ,intent(in)   :: power
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: k, n_coeff_loc
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(ctx_model%dim_domain < 1 .and. ctx_model%dim_domain > 3) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "helmholtz equation in elastic medium "       //  &
                      "[parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    if(power .ne. 1) then
      ctx_err%msg   = "** ERROR: pseudo-hessian not yet validated " //  &
                      "for dof model representation in elastic "    //  &
                      "medium [parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    !! *****************************************************************
    !! USING DOF MODEL REPRESENTATION
    !! *****************************************************************
    n_coeff_loc = ctx_model%vp%param_dof%ndof*ctx_model%n_cell_loc
    !! *****************************************************************

    !! =================================================================
    !! 
    !! in the case of attenuation 
    !!
    !! =================================================================
    if(ctx_model%flag_viscous) then
      !! only deal with the attenuation models here
      do k=1,ctx_model%n_model
        select case(trim(adjustl(str_model(k))))

          !! -----------------------------------------------------------
          case(model_ETA_LDA)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV,model_visco_Maxw,model_visco_Zener)
                grad_out(:,k) =grad_in(:,4)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          case(model_ETA_MU )  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_KV,model_visco_Maxw,model_visco_Zener)
                grad_out(:,k) =grad_in(:,5)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          case(model_TAU_SIGMA)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_Zener)
                grad_out(:,k) =grad_in(:,6)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          case(model_TAU_EPS)  
            select case(trim(adjustl(ctx_model%viscous_model)))
              case(model_visco_Zener)
                grad_out(:,k) =grad_in(:,7)
              case default
                ctx_err%msg   = "** ERROR: Inconsistent elastic " //    &
                                "models for attenuation [parameter_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

        end select !! end for the current model
      end do
    end if
    !! =================================================================

    !! -----------------------------------------------------------------
    !! if we want (lambda,mu,rho) then we can leave it is ok
    if(any(str_model == model_LAMBDA) .and. any(str_model == model_MU)  &
      .and. any(str_model == model_RHO)) then
      
      !! we already have the gradient then
      do k=1,ctx_model%n_model
        if(ctx_err%ierr .ne. 0) cycle !! non-shared error
        
        select case(trim(adjustl(str_model(k))))
          case(model_LAMBDA)
            grad_out(:,k) = grad_in(:,1)
          case(model_MU)
            grad_out(:,k) = grad_in(:,2)
          case(model_RHO)
            grad_out(:,k) = grad_in(:,3)

          case(model_ETA_LDA,model_ETA_MU,model_TAU_SIGMA,model_TAU_EPS)
            !! attenuation has already been done at the beginning.

          case default
            ctx_err%msg   = "** ERROR: Unrecognized model name "    //  &
                            "[parameter_gradient] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
      end do

    !! -----------------------------------------------------------------
    !! else we need to compute the appropriate scalings
    else
      !! NOT SUPPORTED FOR NOW xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      !! xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      ctx_err%msg   = "** ERROR: Unrecognized physical " //             &
                      "parametrization for elastic inversion with " //  &
                      "dof model [parameter_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine parameter_gradient_dof

end module m_parameter_gradient
