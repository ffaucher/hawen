!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_gradient_refelem.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the gradient of the cost function w.r.t
!> the physical coefficient, assuming piecewise constant representation
!> (1 value per cell) for the HELMHOLTZ ELASTIC-ISO propagator using HDG
!> discretization:
!>
!> The reference parameters are
!>      lambda
!>          mu
!>         rho
!
!------------------------------------------------------------------------------
module m_create_gradient_refelem

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: RKIND_MAT,RKIND_MESH,IKIND_MESH
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model
  use m_mesh_simplex_area,      only: mesh_area_simplex
  use m_grad_metric,            only: tag_GRADMETRIC_ID,                &
                                tag_GRADMETRIC_AREA,tag_GRADMETRIC_MASS,&
                                tag_GRADMETRIC_MASS_INV
  use m_model_list
  use m_matrix_utils_derivative,only: hdg_build_dA_2D_tri,              &
                                      hdg_build_dA_3D_tet
  use m_array_types,            only: t_array2d_real,t_array5d_complex, &
                                      array_allocate,array_deallocate

  implicit none

  private
  public  :: create_gradient_refelem_2D
  public  :: create_gradient_refelem_3D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the gradient of the misfit for HDG in HELMHOLTZ ELASTIC 
  !> ISOTROPIC in 2D based upon triangle discretization
  !> grad(:,:,1) = dA / d\lambda
  !> grad(:,:,2) = dA / d\mu
  !> grad(:,:,3) = dA / d\rho
  !> if viscosity
  !> grad(:,:,4) = dA / d\viscosity1
  !> grad(:,:,5) = dA / d\viscosity2
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_bwd         : backward wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] gradient          : gradient arrays
  !> @param[in]    tag_metric        : gradient metric
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_refelem_2D(ctx_dg,ctx_mesh,ctx_model,      &
                                        field_fwd,field_bwd,freq,n_shot,&
                                        gradient,tag_metric,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd (:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_bwd (:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: gradient(:,:)
    integer                            ,intent(in)    :: tag_metric
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2,i
    integer                     :: iloc1,iloc2,n,idof,isol,nvisco
    complex(kind=8),allocatable :: sol_fwd_loc(:),sol_bwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8)             :: gradval(7)
    real   (kind=8)             :: eta_l,eta_m,fpower,lambda,mu,vp,vs
    real   (kind=8)             :: taueps,tausig,rho
    complex(kind=8)             :: u,v,uprime,vprime
    !! evaluation of the integrals
    type(t_array2d_real)   ,allocatable :: vol_phi_phi     (:)
    type(t_array5d_complex),allocatable :: vol_phi_phi_dSdm(:)
    !! for metric only
    integer                     :: lwork,jdof,ierr,i_model
    integer        ,allocatable :: ipiv(:)
    real   (kind=8)             :: area
    real(kind=RKIND_MESH),allocatable :: coo_node(:,:)
    complex(kind=8),allocatable :: metric_weight(:,:),sol_bwd_weigh(:)
    complex(kind=8),allocatable :: work(:)
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! we compute size max so that we have only one allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_order_cell,icell,n,idof),  &
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,isol),  &
    !$OMP&         PRIVATE(sol_fwd_loc,sol_bwd_loc,dA,dAU,gradval,u),   &
    !$OMP&         PRIVATE(rho,metric_weight,jdof,work,lwork,i_model),  &
    !$OMP&         PRIVATE(ipiv,ierr,sol_bwd_weigh,coo_node,area),      &
    !$OMP&         PRIVATE(nvisco,eta_l,eta_m,fpower,lambda,mu,vp,vs),  &
    !$OMP&         PRIVATE(taueps,tausig,vol_phi_phi,vol_phi_phi_dSdm), &
    !$OMP&         PRIVATE(i,v,uprime,vprime)
    !! local array, the 3 is for the two elastic parameters
    !!              the 3 is for the three triangle faces
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,3))
    allocate(dAU          (dim_sol*ndof_max_vol,3))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_weigh(dim_sol*ndof_max_vol))
    allocate(metric_weight(dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(work         (dim_sol*ndof_max_vol))
    allocate(ipiv         (dim_sol*ndof_max_vol))
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))
    allocate(vol_phi_phi     (ctx_dg%n_different_order))
    allocate(vol_phi_phi_dSdm(ctx_dg%n_different_order))
    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! size depends if derivative or not
      call array_allocate(vol_phi_phi     (i),ndof_vol,ndof_vol,ctx_err)
      !! 3x3 matrices for lambda and mu.
      call array_allocate(vol_phi_phi_dSdm(i),ndof_vol,ndof_vol,3,3,2,ctx_err)
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      if(ctx_err%ierr .ne. 0) cycle
      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      n            = ndof_vol*dim_sol

      
      !! volume integrals using reference element method
      vol_phi_phi(i_order_cell)%array = ctx_dg%vol_matrix_mass(i_order_cell)%array
      !! need to derive the analytic expression in elastic isotropy
      !!      ( (lambda + 2mu)/[ 2*mu)(2lambda + 2mu)]      -lambda/[ 2*mu)(2lambda + 2mu)]        0     )
      !!  S = (      -lambda/[ 2*mu)(2lambda + 2mu)]     (lambda + 2mu)/[ 2*mu)(2lambda + 2mu)]    0     )
      !!      (                      0                                      0                    1/(4*mu))
      vol_phi_phi_dSdm(i_order_cell)%array = 0.d0
      !! piecewise constant
      vp      = ctx_model%vp%pconstant(icell)
      vs      = ctx_model%vs%pconstant(icell)
      rho     = ctx_model%rho%pconstant(icell)
      mu      = rho*(vs**2)
      lambda  = rho*(vp**2) - 2.d0*mu
      
      !! derivatives for lambda ----------------------------------------
      v = 4.d0*mu*(lambda + mu)
      u = lambda + 2.d0*mu ; uprime = 1.d0
      vprime = 4.d0*mu
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,1) =                 &
          (uprime*v-u*vprime)/(v**2)*ctx_dg%vol_matrix_mass(i_order_cell)%array
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,1) =                 &
        vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,1)
      u     = -lambda; uprime= -1.d0
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,1) =                 &
          (uprime*v-u*vprime)/(v**2)*ctx_dg%vol_matrix_mass(i_order_cell)%array
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,1) =                 &
        vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,1)
      !! the (3,3) is 0.

      !! derivatives for mu ----------------------------------------
      !!= 4 mu lambda + 4 mu²
      v = 4.d0*mu*(lambda + mu) ; vprime = 4.d0*(lambda + 2.d0*mu)
      u = lambda + 2.d0*mu ; uprime = 2.d0
      
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,2) =                 &
          (uprime*v-u*vprime)/(v**2)*ctx_dg%vol_matrix_mass(i_order_cell)%array
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,2) =                 &
        vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,2)
      u     = -lambda; uprime= 0.d0
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,2) =                 &
          (uprime*v-u*vprime)/(v**2)*ctx_dg%vol_matrix_mass(i_order_cell)%array
      vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,2) =                 &
        vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,2)

      vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,3,2) =                 &
           -1.d0/(4.d0*(mu**2))*ctx_dg%vol_matrix_mass(i_order_cell)%array  

      !! ---------------------------------------------------------------
      
      ! ----------------------------------------------------------------
      !! Compute the appropriate weighting matrix in case we
      !! do not have the identity metric for inner product
      ! ----------------------------------------------------------------
      !! THE MATRIX MUST BE SYMMETRIC POSITIVE DEFINITE
      ! ----------------------------------------------------------------
      metric_weight = 0.0
      select case(tag_metric)
        !! identity matrix ---------------------------------------------
        case(tag_GRADMETRIC_ID       )
          do idof=1,n
            metric_weight(idof,idof) = 1.0
          end do

        !! identity matrix with 1./area on the diagonal ----------------
        case(tag_GRADMETRIC_AREA     )
          coo_node(:,:)=ctx_mesh%x_node(:,ctx_mesh%cell_node(:,icell))
          call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)
          do idof=1,n
            metric_weight(idof,idof) = 1./area
          end do

        !! mass matrix -------------------------------------------------
        case(tag_GRADMETRIC_MASS,tag_GRADMETRIC_MASS_INV)
          do idof=1,ndof_vol ; do jdof=1,ndof_vol
            u = dcmplx(ctx_dg%vol_matrix_mass(i_order_cell)%array(idof,jdof) * &
                       ctx_dg%det_jac(icell))
            do isol=1,dim_sol
              metric_weight(idof+(isol-1)*ndof_vol, &
                            jdof+(isol-1)*ndof_vol) = u
            end do
          end do ; enddo
          if(tag_metric == tag_GRADMETRIC_MASS_INV) then
            !! inverse of M --------------------------------------------
            lwork=n
            call ZGETRF(n,n,metric_weight(1:n,1:n),n,ipiv,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg   = "** ERROR during ZGETRF in HDG create grad"
              ctx_err%critic=.true.
              cycle
            end if
            call ZGETRI(n,metric_weight(1:n,1:n),n,ipiv,work,lwork,ierr)
            if(ierr .ne. 0) then
              ctx_err%ierr = ierr
              ctx_err%msg  = "** ERROR during ZGETRI in HDG create grad"
              ctx_err%critic=.true.
              cycle
            end if
          end if
        !! stiffness matrix --------------------------------------------
        ! ---------------------------------------------------------
        !! does not provide a symmetric definitive positive
        !! matrix
        ! ---------------------------------------------------------
        !! error -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized grad. metric [create_grad]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
      end select
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! loop over all shots--------------------------------------------
      do i_src = 1, n_shot

        if(ctx_err%ierr .ne. 0) cycle

        !! multipler offset
        sol_fwd_loc  =0.0
        sol_bwd_loc  =0.0
        sol_bwd_weigh=0.0
        dA           =0.0
        dAU          =0.0

        ! ---------------------------------------------------------
        !! get the local solution
        igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
                    ctx_dg%offsetdof_sol_loc(icell)  + 1)
        igb2 = igb1 + ndof_vol - 1
        do isol=1,dim_sol
          iloc1 = (isol-1)*ndof_vol + 1
          iloc2 = iloc1  + ndof_vol - 1
          sol_fwd_loc(iloc1:iloc2) = dcmplx(field_fwd(igb1:igb2,isol))
          sol_bwd_loc(iloc1:iloc2) = dcmplx(field_bwd(igb1:igb2,isol))
        end do
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !! the gradient is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell
        !! we now have dC = 0 with the compliance formulation
        call hdg_build_dA_2D_tri(dA,vol_phi_phi(i_order_cell)%array,    &
                                 vol_phi_phi_dSdm(i_order_cell)%array,  &
                                 ctx_dg%det_jac(icell),ndof_vol,freq)                            
        !! dC = 0 with compliance formulation

        !! compute dA U, for all parameters: 1/kappa and rho
        !! -------------------------------------------------------------
        dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))
        dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))
        dAU(1:n,3) = matmul(dA(1:n,1:n,3),sol_fwd_loc(1:n))
        !! apply metric onto the backward field
        sol_bwd_weigh(1:n) = matmul(metric_weight(1:n,1:n),sol_bwd_loc(1:n))

        gradval   = 0.0
        do idof=1,n !! loop over all ndof * dim_solution
          !! gradient values
          gradval(1) = gradval(1) + conjg(dAU(idof,1))*sol_bwd_weigh(idof)
          gradval(2) = gradval(2) + conjg(dAU(idof,2))*sol_bwd_weigh(idof)
          gradval(3) = gradval(3) + conjg(dAU(idof,3))*sol_bwd_weigh(idof)
        end do
        !! average
        gradval    = dcmplx(gradval   / dble(ndof_vol))


        !! ============================================================
        !! in the case of viscosity
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV)
              !! we have 
              !!  (o) \lambda = \lambda - freq Q_\lambda
              !!  (o) \mu     = \mu     - freq Q_\mu
              gradval(4) =-freq * gradval(1)
              gradval(5) =-freq * gradval(2)

            !! case(model_visco_Maxw)
              !! we have
              !!   -freq * eta / (-freq*eta/M0 + 1)
              !! = - freq * eta * M0 / (-freq*eta + M)
              !! 
              !! dJ/da =  dJ/d\lambda  d\lambda/ da.
              !! 
              !! ** it also changes the gradient of lambda and mu
              !!
              !!  
              !vp      = ctx_model%vp%pconstant(icell)
              !vs      = ctx_model%vs%pconstant(icell)
              !rho     = ctx_model%rho%pconstant(icell)
              !mu      = rho*(vs**2)
              !lambda  = rho*(vp**2) - 2.d0*mu
              !eta_l   = ctx_model%visco(1)%pconstant(icell)
              !eta_m   = ctx_model%visco(2)%pconstant(icell)
              !
              !gradval(4) = gradval(1) * (                               &
              !             (-freq*lambda)*(-freq*eta_l + lambda)        &
              !            -(-freq*eta_l*lambda)*(-freq)                 &
              !             ) / (-freq*eta_l + lambda)**2
              !gradval(5) = gradval(2) * (                               &
              !             (-freq*mu)*(-freq*eta_m + mu)                &
              !            -(-freq*eta_m*mu)*(-freq)                     &
              !             ) / (-freq*eta_m + mu)**2
              !!! also changes the lambda0 and mu0 gradient
              !!! dJ/da =  dJ/d\lambda  d\lambda/ da.
              !gradval(1) = gradval(1) * (                               &
              !             (-freq*eta_l)*(-freq*eta_l + lambda)         &
              !            -(-freq*eta_l*lambda)                         &
              !             ) / (-freq*eta_l + lambda)**2
              !gradval(2) = gradval(2) * (                               &
              !             (-freq*eta_m)*(-freq*eta_m + mu)             &
              !            -(-freq*eta_m*mu)                             &
              !             ) / (-freq*eta_m + mu)**2
              
              
            case(model_visco_Frac)
              !! we have 
              !!  (o) \lambda = \lambda0  + (- freq)^{\alpha} \eta_\lambda
              !!  (o) \mu     = \mu0      + (- freq)^{\alpha} \eta_\mu
              fpower = dble(ctx_model%visco(3)%pconstant(icell))
              gradval(4) =((-freq)**fpower) * gradval(1)
              gradval(5) =((-freq)**fpower) * gradval(2)
              !! we have a^b = exp(b log(a))
              gradval(6) = log(-freq) * ((-freq)**fpower) * gradval(1)  &
                         + log(-freq) * ((-freq)**fpower) * gradval(2)

            case(model_visco_Zener)
              vp      = ctx_model%vp%pconstant(icell)
              vs      = ctx_model%vs%pconstant(icell)
              rho     = ctx_model%rho%pconstant(icell)
              mu      = rho*(vs**2)
              lambda  = rho*(vp**2) - 2.d0*mu
              eta_l   = ctx_model%visco(1)%pconstant(icell)
              eta_m   = ctx_model%visco(2)%pconstant(icell)
              tausig  = ctx_model%visco(3)%pconstant(icell)
              taueps  = ctx_model%visco(4)%pconstant(icell)
              !!   \lambda=\dfrac{\lambda_0 - i\omega\taue\eta}
              !!                 {1-i\omega\taus}
              !!   and we derive w.r.t \lambda0 so we have to adjust
              !!   the gradient also.
              !! --------------------
              
              !! \eta_\lambda
              gradval(4) = (-freq * taueps) / (1.d0 - freq * tausig)*gradval(1)

              !! \eta_\mu
              gradval(5) = (-freq * taueps) / (1.d0 - freq * tausig)*gradval(2)
              
              !! \tau_\sigma
              gradval(6) = (-freq*eta_l)/(1.d0 - freq*tausig)*gradval(1) &
                         + (-freq*eta_m)/(1.d0 - freq*tausig)*gradval(2)
              !! \tau_\epsilon
              gradval(7) = (lambda - freq*taueps*eta_l) * freq /        &
                           ((1.d0 - freq*tausig)**2.) * gradval(1) +    &
                           (mu     - freq*taueps*eta_m) * freq /        &
                           ((1.d0 - freq*tausig)**2.) * gradval(2)
              
              !! we also need to adapt the gradient with respect to lambda
              !! and mu, because there is the multiplication
              gradval(1) = gradval(1) * 1.d0 / (1.d0 - freq * tausig)
              gradval(2) = gradval(2) * 1.d0 / (1.d0 - freq * tausig)

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [grad]"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              cycle !! possibility of non-shared error is dealt outside
          end select

        end if
        ! ---------------------------------------------------------
        !! save value, we sum over the shots here
        gradient(icell,1) = gradient(icell,1)+real(gradval   (1),kind=4)
        gradient(icell,2) = gradient(icell,2)+real(gradval   (2),kind=4)
        gradient(icell,3) = gradient(icell,3)+real(gradval   (3),kind=4)
        if(ctx_model%flag_viscous) then
          nvisco = size(ctx_model%visco)
          do i_model = 4, 3+nvisco
            gradient(icell,i_model) = gradient(icell,i_model)           &
                                    + real(gradval(i_model),kind=4)
          end do
        end if
        ! ---------------------------------------------------------
      end do !! end loop over shots
    end do !! end loop over cells
    !$OMP END DO

    deallocate(dA         )
    deallocate(sol_fwd_loc)
    deallocate(sol_bwd_loc)
    deallocate(sol_bwd_weigh)
    deallocate(dAU        )
    deallocate(coo_node   )
    deallocate(work       )
    deallocate(ipiv       )
    deallocate(metric_weight)
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi     (i))
      call array_deallocate(vol_phi_phi_dSdm(i))
    end do
    deallocate(vol_phi_phi     )
    deallocate(vol_phi_phi_dSdm)
    !$OMP END PARALLEL

    return
  end subroutine create_gradient_refelem_2D


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the gradient of the misfit for HDG in HELMHOLTZ ELASTIC 
  !> ISOTROPIC in 3D based upon tetrahedron discretization
  !> grad(:,:,1) = dA / d\lambda
  !> grad(:,:,2) = dA / d\mu
  !> grad(:,:,3) = dA / d\rho
  !> if viscosity
  !> grad(:,:,4) = dA / d\viscosity1
  !> grad(:,:,5) = dA / d\viscosity2
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_bwd         : backward wavefield
  !> @param[in]    mult_fwd          : forward Lagrange multiplier
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] gradient          : gradient arrays
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_refelem_3D(ctx_err)

    implicit none

    type(t_error)                      ,intent(inout) :: ctx_err

     
    ctx_err%msg   = "** ERROR: 3D is not ready yet [grad]"
    ctx_err%ierr  = -1
    ctx_err%critic=.true.

    return

  end subroutine create_gradient_refelem_3D

end module m_create_gradient_refelem
