!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_gradient_quadrature.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the gradient of the cost function w.r.t
!> the physical coefficient, assuming piecewise constant representation
!> (1 value per cell) for the HELMHOLTZ ELASTIC-ISO propagator using HDG
!> discretization:
!>
!> The reference parameters are
!>      lambda
!>          mu
!>         rho
!
!------------------------------------------------------------------------------
module m_create_gradient_quadrature

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_define_precision,       only: RKIND_MAT,RKIND_MESH,IKIND_MESH
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_raw,                   &
                                      model_eval_dof_index
  use m_mesh_simplex_area,      only: mesh_area_simplex
  use m_grad_metric,            only: tag_GRADMETRIC_ID,                &
                                tag_GRADMETRIC_AREA,tag_GRADMETRIC_MASS,&
                                tag_GRADMETRIC_MASS_INV
  use m_model_list
  use m_matrix_utils_derivative,only: hdg_build_dA_2D_tri,              &
                                      hdg_build_dA_3D_tet
  use m_array_types,            only: t_array2d_real,t_array5d_complex, &
                                      array_allocate,array_deallocate,  &
                                      t_array3d_real
  use m_lapack_inverse,         only : lapack_matrix_inverse

  implicit none

  private
  public  :: create_gradient_quadrature_pconstant
  public  :: create_gradient_quadrature_dof

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the gradient of the misfit, for HDG in elastic isotropy 
  !> in 2D/3D based upon simplex discretization, using piecewise-constant
  !> representation.
  !> grad(:,:,1) = dA / d{lambda}
  !> grad(:,:,2) = dA / d{mu}
  !> grad(:,:,3) = dA / d{rho}
  !> if viscosity it continues
  !> grad(:,:,4) = dA / d\viscosity1
  !> grad(:,:,5) = dA / d\viscosity2
  !> ...
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_bwd         : backward wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] gradient          : gradient arrays
  !> @param[in]    tag_metric        : gradient metric
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_quadrature_pconstant(ctx_dg,ctx_mesh,      & 
                                          ctx_model,field_fwd,field_bwd,&
                                          freq,n_shot,gradient,         &
                                          tag_metric,ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd (:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_bwd (:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: gradient(:,:)
    integer                            ,intent(in)    :: tag_metric
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2
    integer                     :: iloc1,iloc2,n,idof,isol,nvisco
    complex(kind=8),allocatable :: sol_fwd_loc(:),sol_bwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8)             :: gradval(7)
    real   (kind=8)             :: eta_l,eta_m,fpower,vp,vs
    real   (kind=8)             :: taueps,tausig,rho
    real   (kind=8)             :: muR,eta
    complex(kind=8)             :: u,v,uprime,vprime,mu,lambda
    !! evaluation of the integrals
    type(t_array5d_complex),allocatable :: vol_phi_phi_dSdm(:)
    !! for metric only
    integer                     :: lwork,jdof,i_model
    real   (kind=8)             :: area
    real(kind=RKIND_MESH),allocatable :: coo_node(:,:)
    complex(kind=8),allocatable :: metric_weight(:,:),sol_bwd_weigh(:)
    complex(kind=8),allocatable :: work(:,:)
    !! the quadrature mass matrix, one per order.
    integer                     :: k, i, j, iquad
    type(t_array2d_real),allocatable:: mass_matrix(:)
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! we only need the mass matrix, which amounts to the sum of 
    !! all quad points for piecewise-constant representation.
    !! number of different orders
    allocate(mass_matrix(ctx_dg%order_to_index(ctx_dg%order_max_gb)))
    do k = 1, ctx_dg%order_to_index(ctx_dg%order_max_gb)
      ndof_vol  = ctx_dg%n_dof_per_order(k)
      call array_allocate(mass_matrix(k),ndof_vol,ndof_vol,ctx_err)
      mass_matrix(k)%array = 0.d0
      do i=1, ndof_vol ; do j=1, ndof_vol
        do iquad=1,ctx_dg%quadGL_npts
          mass_matrix(k)%array(i,j) = mass_matrix(k)%array(i,j) +       &
                              ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        end do
      end do ; end do
    end do
    !! -----------------------------------------------------------------


    !! we compute size max so that we have only ONE allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_order_cell,icell,n,idof),  &
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,isol),  &
    !$OMP&         PRIVATE(sol_fwd_loc,sol_bwd_loc,dA,dAU,gradval,u),   &
    !$OMP&         PRIVATE(rho,metric_weight,jdof,work,lwork,i_model),  &
    !$OMP&         PRIVATE(sol_bwd_weigh,coo_node,area),                &
    !$OMP&         PRIVATE(nvisco,eta_l,eta_m,fpower,lambda,mu,vp,vs),  &
    !$OMP&         PRIVATE(taueps,tausig,vol_phi_phi_dSdm),             &
    !$OMP&         PRIVATE(k,i,j,iquad,v,uprime,vprime)
    !! local array, the 3 is for the two elastic parameters
    !!              the 3 is for the three triangle faces
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,3))
    allocate(dAU          (dim_sol*ndof_max_vol,3))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_weigh(dim_sol*ndof_max_vol))
    allocate(metric_weight(dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(work         (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))
    allocate(vol_phi_phi_dSdm(ctx_dg%n_different_order))
    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! 3x3 matrices for lambda and mu.
      select case(ctx_dg%dim_domain)
        case(2)
          call array_allocate(vol_phi_phi_dSdm(i),ndof_vol,ndof_vol,3,3,2,ctx_err)
        case(3)
          call array_allocate(vol_phi_phi_dSdm(i),ndof_vol,ndof_vol,6,6,2,ctx_err)
        case default
            ctx_err%msg   = "** ERROR: Unrecognized dimension [grad]"
            ctx_err%ierr  = -1
            cycle !! possibility of non-shared error is dealt outside
      end select
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      if(ctx_err%ierr .ne. 0) cycle
      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      n            = ndof_vol*dim_sol

      vol_phi_phi_dSdm(i_order_cell)%array = 0.d0
      !! piecewise constant
      vp      = ctx_model%vp%pconstant(icell)
      vs      = ctx_model%vs%pconstant(icell)
      rho     = ctx_model%rho%pconstant(icell)
      mu      = rho*(vs**2)
      lambda  = rho*(vp**2) - 2.d0*mu

      !! need complex-valued parameter
      mu      = rho*(vs**2) - freq*ctx_model%visco(2)%pconstant(icell)
      lambda  = rho*(vp**2) - 2.d0*rho*(vs**2) - freq*ctx_model%visco(1)%pconstant(icell)
      select case(ctx_dg%dim_domain)
        case(2)
          !! volume integrals
          !! need to derive the analytic expression in elastic isotropy
          !!      ( (lambda + 2mu)/[ 2*mu(2lambda + 2mu)]      -lambda/[ 2*mu)(2lambda + 2mu)]        0     )
          !!  S = (      -lambda/[ 2*mu)(2lambda + 2mu)]     (lambda + 2mu)/[ 2*mu)(2lambda + 2mu)]    0     )
          !!      (                      0                                      0                    1/(4*mu))
          
          !! derivatives for lambda ----------------------------------------
          v = 4.d0*mu*(lambda + mu)
          u = lambda + 2.d0*mu ; uprime = 1.d0
          vprime = 4.d0*mu
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,1) =             &
              (uprime*v-u*vprime)/(v**2)*mass_matrix(i_order_cell)%array
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,1) =             &
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,1)
          u     = -lambda; uprime= -1.d0
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,1) =             &
              (uprime*v-u*vprime)/(v**2)*mass_matrix(i_order_cell)%array
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,1) =             &
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,1)
          !! the (3,3) is 0.
          !! derivatives for mu ----------------------------------------
          !!= 4 mu lambda + 4 mu²
          v = 4.d0*mu*(lambda + mu) ; vprime = 4.d0*(lambda + 2.d0*mu)
          u = lambda + 2.d0*mu ; uprime = 2.d0
          
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,2) =             &
              (uprime*v-u*vprime)/(v**2)*mass_matrix(i_order_cell)%array
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,2) =             &
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,2)
          u     = -lambda; uprime= 0.d0
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,2) =             &
              (uprime*v-u*vprime)/(v**2)*mass_matrix(i_order_cell)%array
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,2) =             &
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,2)
          
          v = 4.d0*mu ; vprime = 4.d0
          u = 1.d0    ; uprime = 0.d0
          vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,3,2) =             &
              (uprime*v-u*vprime)/(v**2)*mass_matrix(i_order_cell)%array


          !! ---------------------------------------------------------------
        case default
            ctx_err%msg   = "** ERROR: Unrecognized dimension [grad]"
            ctx_err%ierr  = -1
            cycle !! possibility of non-shared error is dealt outside
      end select


      ! ----------------------------------------------------------------
      !! Compute the appropriate weighting matrix in case we
      !! do not have the identity metric for inner product
      ! ----------------------------------------------------------------
      !! THE MATRIX MUST BE SYMMETRIC POSITIVE DEFINITE
      ! ----------------------------------------------------------------
      metric_weight = 0.0
      select case(tag_metric)
        !! identity matrix ---------------------------------------------
        case(tag_GRADMETRIC_ID       )
          do idof=1,n
            metric_weight(idof,idof) = 1.0
          end do

        !! identity matrix with 1./area on the diagonal ----------------
        case(tag_GRADMETRIC_AREA     )
          coo_node(:,:)=ctx_mesh%x_node(:,ctx_mesh%cell_node(:,icell))
          call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)
          do idof=1,n
            metric_weight(idof,idof) = 1./area
          end do

        !! mass matrix -------------------------------------------------
        case(tag_GRADMETRIC_MASS,tag_GRADMETRIC_MASS_INV)
          do idof=1,ndof_vol ; do jdof=1,ndof_vol
            u = dcmplx(mass_matrix(i_order_cell)%array(idof,jdof) *     &
                       ctx_dg%det_jac(icell))
            do isol=1,dim_sol
              metric_weight(idof+(isol-1)*ndof_vol, &
                            jdof+(isol-1)*ndof_vol) = u
            end do
          end do ; enddo
          if(tag_metric == tag_GRADMETRIC_MASS_INV) then
            !! inverse of M --------------------------------------------
            work = 0.d0
            call lapack_matrix_inverse(metric_weight(1:n,1:n),          &
                                       work(1:n,1:n),n,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            metric_weight(1:n,1:n) = work(1:n,1:n)
          end if

        !! stiffness matrix --------------------------------------------
        ! ---------------------------------------------------------
        !! does not provide a symmetric definitive positive
        !! matrix
        ! ---------------------------------------------------------
        !! error -------------------------------------------------------        
        case default
          ctx_err%msg   = "** ERROR: Unrecognized grad. metric [create_grad]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          cycle
      end select
      ! ----------------------------------------------------------------


      ! ----------------------------------------------------------------
      !! loop over all shots--------------------------------------------
      do i_src = 1, n_shot

        if(ctx_err%ierr .ne. 0) cycle

        !! multipler offset
        sol_fwd_loc  =0.0
        sol_bwd_loc  =0.0
        sol_bwd_weigh=0.0
        dA           =0.0
        dAU          =0.0

        ! ---------------------------------------------------------
        !! get the local solution
        igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
                    ctx_dg%offsetdof_sol_loc(icell)  + 1)
        igb2 = igb1 + ndof_vol - 1
        do isol=1,dim_sol
          iloc1 = (isol-1)*ndof_vol + 1
          iloc2 = iloc1  + ndof_vol - 1
          sol_fwd_loc(iloc1:iloc2) = dcmplx(field_fwd(igb1:igb2,isol))
          sol_bwd_loc(iloc1:iloc2) = dcmplx(field_bwd(igb1:igb2,isol))
        end do
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !! the gradient is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell

        !! if we are constant per element, it simply is the sum of the 
        !! quadrature information
        select case(ctx_mesh%dim_domain)
          case(2)
            call hdg_build_dA_2D_tri(dA,mass_matrix(i_order_cell)%array,&
                                 vol_phi_phi_dSdm(i_order_cell)%array,  &
                                 ctx_dg%det_jac(icell),ndof_vol,freq)                            
          case(3)
            call hdg_build_dA_3D_tet(dA,mass_matrix(i_order_cell)%array,&
                                 vol_phi_phi_dSdm(i_order_cell)%array,  &
                                 ctx_dg%det_jac(icell),ndof_vol,freq)   
          case default
            ctx_err%msg   = "** ERROR: Unrecognized dimension [grad]"
            ctx_err%ierr  = -1
            cycle !! possibility of non-shared error is dealt outside
        end select
        if(ctx_err%ierr .ne. 0) cycle

        !!> dC = 0 

        !! compute dA U, for all parameters: 
        !! -------------------------------------------------------------
        dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))
        dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))
        dAU(1:n,3) = matmul(dA(1:n,1:n,3),sol_fwd_loc(1:n))
        !! apply metric onto the backward field
        sol_bwd_weigh(1:n) = matmul(metric_weight(1:n,1:n),sol_bwd_loc(1:n))

        gradval   = 0.0
        do idof=1,n !! loop over all ndof * dim_solution
          !! gradient values
          gradval(1) = gradval(1) + conjg(dAU(idof,1))*sol_bwd_weigh(idof)
          gradval(2) = gradval(2) + conjg(dAU(idof,2))*sol_bwd_weigh(idof)
          gradval(3) = gradval(3) + conjg(dAU(idof,3))*sol_bwd_weigh(idof)
        end do
        !! average
        gradval    = dcmplx(gradval   / dble(ndof_vol))


        !! ============================================================
        !! in the case of viscosity
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV)
              !! we have 
              !!  (o) \lambda = \lambda - freq Q_\lambda
              !!  (o) \mu     = \mu     - freq Q_\mu
              gradval(4) =-freq * gradval(1)
              gradval(5) =-freq * gradval(2)

            !! case(model_visco_Maxw)
              !! we have
              !!   -freq * eta / (-freq*eta/M0 + 1)
              !! = - freq * eta * M0 / (-freq*eta + M)
              !! 
              !! dJ/da =  dJ/d\lambda  d\lambda/ da.
              !! 
              !! ** it also changes the gradient of lambda and mu
              !!
              !!  
              !vp      = ctx_model%vp%pconstant(icell)
              !vs      = ctx_model%vs%pconstant(icell)
              !rho     = ctx_model%rho%pconstant(icell)
              !mu      = rho*(vs**2)
              !lambda  = rho*(vp**2) - 2.d0*mu
              !eta_l   = ctx_model%visco(1)%pconstant(icell)
              !eta_m   = ctx_model%visco(2)%pconstant(icell)
              !
              !gradval(4) = gradval(1) * (                               &
              !             (-freq*lambda)*(-freq*eta_l + lambda)        &
              !            -(-freq*eta_l*lambda)*(-freq)                 &
              !             ) / (-freq*eta_l + lambda)**2
              !gradval(5) = gradval(2) * (                               &
              !             (-freq*mu)*(-freq*eta_m + mu)                &
              !            -(-freq*eta_m*mu)*(-freq)                     &
              !             ) / (-freq*eta_m + mu)**2
              !!! also changes the lambda0 and mu0 gradient
              !!! dJ/da =  dJ/d\lambda  d\lambda/ da.
              !gradval(1) = gradval(1) * (                               &
              !             (-freq*eta_l)*(-freq*eta_l + lambda)         &
              !            -(-freq*eta_l*lambda)                         &
              !             ) / (-freq*eta_l + lambda)**2
              !gradval(2) = gradval(2) * (                               &
              !             (-freq*eta_m)*(-freq*eta_m + mu)             &
              !            -(-freq*eta_m*mu)                             &
              !             ) / (-freq*eta_m + mu)**2
              
              
            ! case('Fractional')
            !   !! we have 
            !   !!  (o) \lambda = \lambda0  + (- freq)^{\alpha} \eta_\lambda
            !   !!  (o) \mu     = \mu0      + (- freq)^{\alpha} \eta_\mu
            !   fpower = dble(ctx_model%visco(3)%pconstant(icell))
            !   gradval(4) =((-freq)**fpower) * gradval(1)
            !   gradval(5) =((-freq)**fpower) * gradval(2)
            !   !! we have a^b = exp(b log(a))
            !   gradval(6) = log(-freq) * ((-freq)**fpower) * gradval(1)  &
            !              + log(-freq) * ((-freq)**fpower) * gradval(2)

            case(model_visco_Zener)
              vp      = ctx_model%vp%pconstant(icell)
              vs      = ctx_model%vs%pconstant(icell)
              rho     = ctx_model%rho%pconstant(icell)
              mu      = rho*(vs**2)
              lambda  = rho*(vp**2) - 2.d0*mu
              eta_l   = ctx_model%visco(1)%pconstant(icell)
              eta_m   = ctx_model%visco(2)%pconstant(icell)
              tausig  = ctx_model%visco(3)%pconstant(icell)
              taueps  = ctx_model%visco(4)%pconstant(icell)
              !!   \lambda=\dfrac{\lambda_0 - i\omega\taue\eta}
              !!                 {1-i\omega\taus}
              !!   and we derive w.r.t \lambda0 so we have to adjust
              !!   the gradient also.
              !! --------------------
              
              !! \eta_\lambda
              gradval(4) = (-freq * taueps) / (1.d0 - freq * tausig)*gradval(1)

              !! \eta_\mu
              gradval(5) = (-freq * taueps) / (1.d0 - freq * tausig)*gradval(2)
              
              !! \tau_\sigma
              gradval(6) = (-freq*eta_l)/(1.d0 - freq*tausig)*gradval(1) &
                         + (-freq*eta_m)/(1.d0 - freq*tausig)*gradval(2)
              !! \tau_\epsilon
              gradval(7) = (lambda - freq*taueps*eta_l) * freq /        &
                           ((1.d0 - freq*tausig)**2.) * gradval(1) +    &
                           (mu     - freq*taueps*eta_m) * freq /        &
                           ((1.d0 - freq*tausig)**2.) * gradval(2)
              
              !! we also need to adapt the gradient with respect to lambda
              !! and mu, because there is the multiplication
              gradval(1) = gradval(1) * 1.d0 / (1.d0 - freq * tausig)
              gradval(2) = gradval(2) * 1.d0 / (1.d0 - freq * tausig)

            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [grad]"
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
          end select

        end if
        ! ---------------------------------------------------------
        !! save value, we sum over the shots here
        gradient(icell,1) = gradient(icell,1)+real(gradval(1),kind=4)
        gradient(icell,2) = gradient(icell,2)+real(gradval(2),kind=4)
        gradient(icell,3) = gradient(icell,3)+real(gradval(3),kind=4)
        if(ctx_model%flag_viscous) then
          nvisco = size(ctx_model%visco)
          do i_model = 4, 3+nvisco
            gradient(icell,i_model) = gradient(icell,i_model)           &
                                    + real(gradval(i_model),kind=4)
          end do
        end if
        ! ---------------------------------------------------------
      end do !! end loop over shots
    end do !! end loop over cells
    !$OMP END DO

    deallocate(dA         )
    deallocate(sol_fwd_loc)
    deallocate(sol_bwd_loc)
    deallocate(sol_bwd_weigh)
    deallocate(dAU        )
    deallocate(coo_node   )
    deallocate(work       )
    deallocate(metric_weight)
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi_dSdm(i))
    end do
    deallocate(vol_phi_phi_dSdm)
    !$OMP END PARALLEL

    do k = 1, ctx_dg%order_to_index(ctx_dg%order_max_gb)
      call array_deallocate(mass_matrix(k))
    end do
    deallocate(mass_matrix)

    return
  end subroutine create_gradient_quadrature_pconstant



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the gradient of the misfit, for HDG in elastic isotropy 
  !> in 2D/3D based upon simplex discretization, using piecewise-dof
  !> representation.
  !> grad(:,:,1) = dA / d{lambda}
  !> grad(:,:,2) = dA / d{mu}
  !> grad(:,:,3) = dA / d{rho}
  !> if viscosity it continues
  !> grad(:,:,4) = dA / d\viscosity1
  !> grad(:,:,5) = dA / d\viscosity2
  !> ...
  !>
  !
  !> @param[in]    ctx_dg            : context dg method
  !> @param[in]    ctx_mesh          : context mesh
  !> @param[in]    ctx_model         : context physical model
  !> @param[in]    field_fwd         : forward wavefield
  !> @param[in]    field_bwd         : backward wavefield
  !> @param[in]    freq              : angular frequency
  !> @param[in]    n_shot            : total number of shots
  !> @param[inout] gradient          : gradient arrays
  !> @param[in]    tag_metric        : gradient metric
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine create_gradient_quadrature_dof(ctx_dg,ctx_mesh,ctx_model,  &
                                            field_fwd,field_bwd,freq,   &
                                            n_shot,gradient,tag_metric, &
                                            ctx_err)

    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: ctx_model
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_fwd (:,:)
    complex(kind=RKIND_MAT),allocatable,intent(in)    :: field_bwd (:,:)
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=4)        ,allocatable,intent(inout) :: gradient(:,:)
    integer                            ,intent(in)    :: tag_metric
    integer                            ,intent(in)    :: n_shot
    type(t_error)                      ,intent(inout) :: ctx_err
    !!
    integer(kind=IKIND_MESH)    :: icell,ic
    integer                     :: i_order_cell,ndof_max_vol,ndof_max_face
    integer                     :: dim_sol,ndof_vol,i_src,igb1,igb2
    integer                     :: iloc1,iloc2,n,idof,isol,icell_src
    complex(kind=8),allocatable :: sol_fwd_loc(:),sol_bwd_loc(:)
    complex(kind=8),allocatable :: dA(:,:,:),dAU(:,:)
    complex(kind=8)             :: gradval(7),u,v,uprime,vprime
    complex(kind=8)             :: lambda,mu,vp,vs,dScoeff
    complex(kind=8)             :: dmucx_dmu,dmucx_detamu
    complex(kind=8)             :: dlambdacx_dmu,dlambdacx_dlambda
    complex(kind=8)             :: dlambdacx_detamu,dlambdacx_detal
    complex(kind=8)             :: omega,iomega
    real   (kind=8)             :: rho,lambdaR,muR
    real   (kind=8)             :: eta_l,eta_m,tausig,taueps
    !! evaluation of the integrals
    type(t_array5d_complex),allocatable :: vol_phi_phi_dSdm(:)
    !! for metric only
    integer                     :: lwork,jdof,n_visco,i_model
    real   (kind=8)             :: area
    real(kind=RKIND_MESH),allocatable :: coo_node(:,:)
    complex(kind=8),allocatable :: metric_weight(:,:),sol_bwd_weigh(:)
    complex(kind=8),allocatable :: work(:,:)
    !! the quadrature mass matrix, one per order.
    integer                     :: k, i, j
    type(t_array3d_real),allocatable:: mass_matrix_dm(:)
    type(t_array2d_real),allocatable:: mass_matrix_gb(:)
    !! as we have the model represented on Lagrange basis
    integer                     :: nmodel_per_cell, k_model, igbmod
    integer                     :: igbmod_start
    !!
    integer,         parameter  :: n_param_main = 3
    complex(kind=8), parameter  :: iunit = dcmplx(0.d0,1.d0)
    !! -----------------------------------------------------------------

    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! we only need the mass matrix, which amounts to the sum of 
    !! all quad points for piecewise-constant representation.
    !! number of different orders
    !! Here we have multiple poins per cell ==> the quad point are 
    !! separated in groups
    if(ctx_model%vp%param_dof%ndof .ne.ctx_model%rho%param_dof%ndof .or.&
       ctx_model%vs%param_dof%ndof .ne.ctx_model%rho%param_dof%ndof) then
      ctx_err%msg   = "** ERROR: need the same representation for all"//&
                      " models to avoid ambiguity for other "         //&
                      "parametrization such as bulk modulus "         //&
                      "[create_gradient] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if
    nmodel_per_cell = ctx_model%vp%param_dof%ndof
    
    !! create the mass matrices ========================================
    !! we only need its derivative w.r.t. each of the model parameter
    !! =================================================================
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    allocate(mass_matrix_dm(i_order_cell))
    allocate(mass_matrix_gb(i_order_cell))
    do k = 1, ctx_dg%order_to_index(ctx_dg%order_max_gb)
      ndof_vol  = ctx_dg%n_dof_per_order(k)
      
      call array_allocate(mass_matrix_gb(k),ndof_vol,ndof_vol,ctx_err)
      mass_matrix_gb(k)%array = 0.d0
      
      call array_allocate(mass_matrix_dm(k),ndof_vol,ndof_vol,          &
                          nmodel_per_cell,ctx_err)
      mass_matrix_dm(k)%array = 0.d0
      
      !! loop over the non-model basis functions
      do i=1, ndof_vol ; do j=1, ndof_vol
        !! loop over model basis functions
        do i_model = 1, nmodel_per_cell
          !! the sum of all weighted coefficients (sum over quad points)
          mass_matrix_dm(k)%array(i,j,i_model) =                        &
                 sum(ctx_dg%quadGL_phi_phi_phi_w(k)%array(i,j,i_model,:))
        end do
          
        !! the original mass matrix (sum over quad points)
        mass_matrix_gb(k)%array(i,j) =                                  &
          sum(ctx_dg%quadGL_phi_phi_w(k)%array(i,j,:))
      end do ; end do
    end do
    !! =================================================================

    !! we compute size max so that we have only ONE allocate
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol

    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_order_cell,icell,n,idof),  &
    !$OMP&         PRIVATE(ndof_vol,i_src,igb1,igb2,iloc1,iloc2,isol),  &
    !$OMP&         PRIVATE(sol_fwd_loc,sol_bwd_loc,dA,dAU,gradval,u,v), &
    !$OMP&         PRIVATE(uprime,vprime,icell_src,ic),                 &
    !$OMP&         PRIVATE(metric_weight,jdof,work,lwork,i_model),      &
    !$OMP&         PRIVATE(sol_bwd_weigh,coo_node,area),                &
    !$OMP&         PRIVATE(n_visco,k_model,igbmod,igbmod_start,i,j,k),  &
    !$OMP&         PRIVATE(vol_phi_phi_dSdm,vp,vs,rho,lambda,mu),       &
    !$OMP&         PRIVATE(eta_l,eta_m,tausig,taueps,muR,lambdaR),      &
    !$OMP&         PRIVATE(dmucx_dmu,dmucx_detamu,dlambdacx_dmu),       &
    !$OMP&         PRIVATE(dlambdacx_dlambda,dlambdacx_detamu),         &
    !$OMP&         PRIVATE(dlambdacx_detal,omega,iomega,dScoeff)
    
    !! local arrays
    allocate(dA           (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol,n_param_main))
    allocate(dAU          (dim_sol*ndof_max_vol,n_param_main))
    allocate(sol_fwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_loc  (dim_sol*ndof_max_vol))
    allocate(sol_bwd_weigh(dim_sol*ndof_max_vol))
    allocate(metric_weight(dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(work         (dim_sol*ndof_max_vol,dim_sol*ndof_max_vol))
    allocate(coo_node(ctx_mesh%dim_domain,ctx_mesh%n_neigh_per_cell))
    allocate(vol_phi_phi_dSdm(ctx_dg%n_different_order))
    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! 3x3 matrices for lambda and mu.
      select case(ctx_dg%dim_domain)
        case(2)
          call array_allocate(vol_phi_phi_dSdm(i),ndof_vol,ndof_vol,3,3,2,ctx_err)
        case(3)
          call array_allocate(vol_phi_phi_dSdm(i),ndof_vol,ndof_vol,6,6,2,ctx_err)
        case default
            ctx_err%msg   = "** ERROR: Unrecognized dimension [grad]"
            ctx_err%ierr  = -1
            cycle !! possibility of non-shared error is dealt outside
      end select
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do icell=1,ctx_mesh%n_cell_loc !! loop over all cell
      
      !! initial index for the position
      igbmod_start = (icell-1)*ctx_model%vp%param_dof%ndof

      if(ctx_err%ierr .ne. 0) cycle
      i_order_cell = ctx_dg%order_to_index (ctx_mesh%order(icell))
      ndof_vol     = ctx_dg%n_dof_per_order(i_order_cell)
      n            = ndof_vol*dim_sol

      ! ----------------------------------------------------------------
      !! Compute the appropriate weighting matrix in case we
      !! do not have the identity metric for inner product
      ! ----------------------------------------------------------------
      !! THE MATRIX MUST BE SYMMETRIC POSITIVE DEFINITE
      ! ----------------------------------------------------------------
      metric_weight = 0.0
      select case(tag_metric)
        !! identity matrix ---------------------------------------------
        case(tag_GRADMETRIC_ID       )
          do idof=1,n
            metric_weight(idof,idof) = 1.0
          end do

        !! identity matrix with 1./area on the diagonal ----------------
        case(tag_GRADMETRIC_AREA     )
          coo_node(:,:)=ctx_mesh%x_node(:,ctx_mesh%cell_node(:,icell))
          call mesh_area_simplex(ctx_mesh%dim_domain,coo_node,area,ctx_err)
          do idof=1,n
            metric_weight(idof,idof) = 1./area
          end do

        !! mass matrix -------------------------------------------------
        case(tag_GRADMETRIC_MASS,tag_GRADMETRIC_MASS_INV)
          do idof=1,ndof_vol ; do jdof=1,ndof_vol
            u = dcmplx(mass_matrix_gb(i_order_cell)%array(idof,jdof) *  &
                       ctx_dg%det_jac(icell))
            do isol=1,dim_sol
              metric_weight(idof+(isol-1)*ndof_vol, &
                            jdof+(isol-1)*ndof_vol) = u
            end do
          end do ; enddo
          if(tag_metric == tag_GRADMETRIC_MASS_INV) then
            !! inverse of M --------------------------------------------
            work = 0.d0
            call lapack_matrix_inverse(metric_weight(1:n,1:n),          &
                                       work(1:n,1:n),n,ctx_err)
            if(ctx_err%ierr .ne. 0) cycle
            metric_weight(1:n,1:n) = work(1:n,1:n)
          end if
        !! stiffness matrix --------------------------------------------
        ! ---------------------------------------------------------
        !! does not provide a symmetric definitive positive
        !! matrix
        ! ---------------------------------------------------------
        !! error -------------------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: Unrecognized grad. metric [create_grad]**"
          ctx_err%ierr  = -1
      end select
      ! ----------------------------------------------------------------
      
      ! ----------------------------------------------------------------
      !! loop over all points in model      
      do k_model=1, nmodel_per_cell
        if(ctx_err%ierr .ne. 0) cycle
        dA = 0.d0
        vol_phi_phi_dSdm(i_order_cell)%array = 0.d0
        
        !! using the dof model complete with complex values:
        call model_eval_dof_index(ctx_model,icell,k_model,freq,vp,vs,   &
                                  lambda,mu,rho,ctx_err)
        !! -------------------------------------------------------------
        !!
        !! compute the derivatives of dS / dm
        select case(ctx_dg%dim_domain)
          case(2)
            !! volume integrals
            !! need to derive the analytic expression in elastic isotropy
            !!      ( (lambda + 2mu)/[ 2*mu(2lambda + 2mu)]      -lambda/[ 2*mu)(2lambda + 2mu)]        0     )
            !!  S = (      -lambda/[ 2*mu)(2lambda + 2mu)]     (lambda + 2mu)/[ 2*mu)(2lambda + 2mu)]    0     )
            !!      (                      0                                      0                    1/(4*mu))
            !! derivatives for lambda ----------------------------------------

            !! .........................................................
            !! use only current index k of the model
            !! .........................................................
            v = 4.d0*mu*(lambda + mu)
            u = lambda + 2.d0*mu ; uprime = 1.d0
            vprime = 4.d0*mu
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,1) =             &
                (uprime*v-u*vprime)/(v**2)                                &
                *mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,1) =             &
              vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,1)
            u     = -lambda; uprime= -1.d0
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,1) =             &
                (uprime*v-u*vprime)/(v**2)                                &
                *mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,1) =             &
              vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,1)
            !! the (3,3) is 0.
            !! derivatives for mu ----------------------------------------
            !!= 4 mu lambda + 4 mu²
            v = 4.d0*mu*(lambda + mu) ; vprime = 4.d0*(lambda + 2.d0*mu)
            u = lambda + 2.d0*mu ; uprime = 2.d0

            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,2) =             &
                (uprime*v-u*vprime)/(v**2)                                &
                *mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,2) =             &
              vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,2)
            u     = -lambda; uprime= 0.d0
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,2) =             &
                (uprime*v-u*vprime)/(v**2)                                &
                *mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,2) =             &
              vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,2)
            
            v = 4.d0*mu ; vprime = 4.d0
            u = 1.d0    ; uprime = 0.d0
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,3,2) =             &
                (uprime*v-u*vprime)/(v**2)                                &
                *mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            !! ---------------------------------------------------------------
          
!! 3D ----------------------------------------------------------------------------------------------------------------------------------------------
!! [(lambda + mu)/(2*mu^2 + 3*lambda*mu),   -lambda/(2*(2*mu^2 + 3*lambda*mu)),   -lambda/(2*(2*mu^2 + 3*lambda*mu)),        0,        0,        0]
!! [  -lambda/(2*(2*mu^2 + 3*lambda*mu)), (lambda + mu)/(2*mu^2 + 3*lambda*mu),   -lambda/(2*(2*mu^2 + 3*lambda*mu)),        0,        0,        0]
!! [  -lambda/(2*(2*mu^2 + 3*lambda*mu)),   -lambda/(2*(2*mu^2 + 3*lambda*mu)), (lambda + mu)/(2*mu^2 + 3*lambda*mu),        0,        0,        0]
!! [                                   0,                                    0,                                    0, 1/(4*mu),        0,        0]
!! [                                   0,                                    0,                                    0,        0, 1/(4*mu),        0]
!! [                                   0,                                    0,                                    0,        0,        0, 1/(4*mu)]
!! -------------------------------------------------------------------------------------------------------------------------------------------------
          case (3)
            !! derivative with respect to lambda
            u = (lambda + mu)           ; uprime = 1.d0
            v = 2.*mu**2 + 3.*lambda*mu ; vprime = 3.d0*mu
            dScoeff = (uprime*v-u*vprime)/(v**2)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,3,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            u = -lambda                      ; uprime =-1.d0
            v = 2.*(2.*mu**2 + 3.*lambda*mu) ; vprime = 6.d0*mu
            dScoeff = (uprime*v-u*vprime)/(v**2)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,3,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,3,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,1,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,2,1) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)

            !! derivative with respect to mu
            u = (lambda + mu)           ; uprime = 1.d0
            v = 2.*mu**2 + 3.*lambda*mu ; vprime = 4.d0*mu + 3.d0*lambda
            dScoeff = (uprime*v-u*vprime)/(v**2)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,1,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,2,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,3,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            u = -lambda                      ; uprime = 0.d0
            v = 2.*(2.*mu**2 + 3.*lambda*mu) ; vprime = 8.d0*mu + 6.d0*lambda
            dScoeff = (uprime*v-u*vprime)/(v**2)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,2,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,1,3,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,1,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,2,3,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,1,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,3,2,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            ! u = 1.d0    ; uprime = 0.d0
            ! v = 4.d0*mu ; vprime = 4.d0
            dScoeff = -1.d0/(4.d0*mu*mu)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,4,4,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,5,5,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)
            vol_phi_phi_dSdm(i_order_cell)%array(:,:,6,6,2) = dScoeff* mass_matrix_dm(i_order_cell)%array(:,:,k_model)


          case default
              ctx_err%msg   = "** ERROR: Unrecognized dimension [grad]"
              ctx_err%ierr  = -1
              cycle !! possibility of non-shared error is dealt outside
        end select
        !!
        !! -------------------------------------------------------------
        
        ! --------------------------------------------------------------
        !! the gradient is given by
        !! < \partial_m Ak Uk + \partial_m Ck \projectionK(\Lambda), \gammak>
        !! were k indicates the cell
        !! Here we have dC = 0, so we just compute dA U, for all parameters
        !! -------------------------------------------------------------
        select case(ctx_mesh%dim_domain)
          case(2)
            call hdg_build_dA_2D_tri(dA,                                   &
                           mass_matrix_dm(i_order_cell)%array(:,:,k_model),&
                           vol_phi_phi_dSdm(i_order_cell)%array,           &
                           ctx_dg%det_jac(icell),ndof_vol,freq)
          case(3)
            call hdg_build_dA_3D_tet(dA,                                   &
                           mass_matrix_dm(i_order_cell)%array(:,:,k_model),&
                           vol_phi_phi_dSdm(i_order_cell)%array,           &
                           ctx_dg%det_jac(icell),ndof_vol,freq)

          case default
            ctx_err%msg   = "** ERROR: Unrecognized dimension [grad]"
            ctx_err%ierr  = -1
            cycle !! possibility of non-shared error is dealt outside
        end select
        !!  dC = 0 in acoustic  
        ! --------------------------------------------------------------

        !! loop over all shots--------------------------------------------
        do i_src = 1, n_shot
 
          if(ctx_err%ierr .ne. 0) cycle
    
          !! multipler offset
          sol_fwd_loc  =0.0
          sol_bwd_loc  =0.0
          sol_bwd_weigh=0.0
          dAU= 0.d0
        
          ! ---------------------------------------------------------
          !! get the local solution
          igb1 = int((i_src-1)*ctx_dg%n_dofloc_persol + &
                      ctx_dg%offsetdof_sol_loc(icell)  + 1)
          igb2 = igb1 + ndof_vol - 1
          do isol=1,dim_sol
            iloc1 = (isol-1)*ndof_vol + 1
            iloc2 = iloc1  + ndof_vol - 1
            sol_fwd_loc(iloc1:iloc2) = dcmplx(field_fwd(igb1:igb2,isol))
            sol_bwd_loc(iloc1:iloc2) = dcmplx(field_bwd(igb1:igb2,isol))
          end do
          ! ---------------------------------------------------------
    
          dAU(1:n,1) = matmul(dA(1:n,1:n,1),sol_fwd_loc(1:n))
          dAU(1:n,2) = matmul(dA(1:n,1:n,2),sol_fwd_loc(1:n))
          dAU(1:n,3) = matmul(dA(1:n,1:n,3),sol_fwd_loc(1:n))
          !! apply metric onto the backward field
          sol_bwd_weigh(1:n) = matmul(metric_weight(1:n,1:n),sol_bwd_loc(1:n))
  
          !! -----------------------------------------------------------
          !! we will loop over all ndof x dim_solution
          !! it depends if we have viscosity or not.
          !! -----------------------------------------------------------
          !! in the case of viscosity
          !!
          !! The viscosity model modifies the complex bulk modulus kappa.
          !! For the gradient we use the chain rul:
          !! dJ/dm  =  dJ/d\kappa   d\kappa/ dm
          !! xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          !! HOWEVER, because of complex-values, we have to insert the 
          !! derivative chain rule within the dAu operation, because 
          !! it feeds in the conjugate.
          !!
          !!  namely the gradient is 
          !!  
          !!  (dA.u, gamma) = conjg(dA.u).gamma
          !! which becomes
          !!  conjg(dA/dmu_cx dmu_cx/dmu_re . u) . gamma.
          !! -----------------------------------------------------------
          
          gradval   = 0.0
          if(.not. ctx_model%flag_viscous) then
            !! ---------------------------------------------------------
            !! case without any viscosity.
            !! ---------------------------------------------------------
            do idof=1,n !! loop over all ndof * dim_solution
              do k=1,n_param_main
                gradval(k) = gradval(k) + conjg(dAU(idof,k))*sol_bwd_weigh(idof)
              end do
            end do
          else
            !! ---------------------------------------------------------
            !! case with viscosity.
            !! ---------------------------------------------------------
            !! -> check consistency            
            do k=1,size(ctx_model%visco)
              if(ctx_model%vp%param_dof%ndof .ne.                       &
                 ctx_model%visco(k)%param_dof%ndof) then
                ctx_err%msg   = "** ERROR: need the same representation"//&
                                " for all viscosity models to avoid "  // &
                                "ambiguity for other parametrization " // &
                                "such as bulk modulus [create_gradient] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
              end if
            end do
            
            !! depends on the attenuation model ------------------------
            select case(trim(adjustl(ctx_model%viscous_model)))

              !! -------------------------------------------------------
              !!
              !! Kelvin--Voigt model
              !!
              !! -------------------------------------------------------
              case(model_visco_KV)
                !! we have 
                !!  (o) \lambda = \lambda - freq eta_\lambda
                !!  (o) \mu     = \mu     - freq eta_\mu
                do idof=1,n !! loop over all ndof * dim_solution
                  !! (lambda, mu, rho, eta-lambda, eta-mu).
                  gradval(1) = gradval(1)+conjg(dAU(idof,1))*sol_bwd_weigh(idof)
                  gradval(2) = gradval(2)+conjg(dAU(idof,2))*sol_bwd_weigh(idof)
                  gradval(3) = gradval(3)+conjg(dAU(idof,3))*sol_bwd_weigh(idof)
                  gradval(4) = gradval(4) + &
                               conjg(-freq*dAU(idof,1))*sol_bwd_weigh(idof)
                  gradval(5) = gradval(5) + &
                               conjg(-freq*dAU(idof,2))*sol_bwd_weigh(idof)
                end do

              !! -------------------------------------------------------
              !!
              !! Maxwell model
              !!
              !! -------------------------------------------------------
              case(model_visco_Maxw)
                !! we also need the raw (real) values here
                call model_eval_dof_index(ctx_model,icell,model_ETA_LDA,&
                                          k_model,freq,eta_l ,ctx_err)
                call model_eval_dof_index(ctx_model,icell,model_ETA_MU ,&
                                          k_model,freq,eta_m ,ctx_err)
                call model_eval_dof_index(ctx_model,icell,model_LAMBDA, &
                                          k_model,freq,lambdaR,ctx_err)
                call model_eval_dof_index(ctx_model,icell,model_MU,     &
                                          k_model,freq,muR    ,ctx_err)
                !! we have
                !! (o) complex mu =  - i omega mu eta_mu / (mu - i omega eta_mu) ; 
                u = -freq*muR*eta_m  ; v = muR-freq*eta_m
                !! derivate w.r.t. muR and eta
                uprime = -freq*eta_m ; vprime = 1.d0
                dmucx_dmu    = (uprime*v-u*vprime)/(v**2)
                uprime = -freq*muR   ; vprime = -freq
                dmucx_detamu = (uprime*v-u*vprime)/(v**2)
                !! (o) complex lambda =  
                !! -i omega*(lambda*omega*eta_m^2 + eta_l*lambda*omega*eta_m 
                !!           + eta_l*mu^2*1i + eta_l*lambda*mu*1i)
                !!    / (- eta_m^2*omega^2*1i + 2*eta_m*mu*omega - eta_l*eta_m*omega^2*1i
                !!       + lambda*eta_m*omega + mu^2*1i + eta_l*mu*omega + lambda*mu*1i)
                !! -----------------------------------------------------
                !! We have that
                !!                freq = (-s + i \omega)
                !! 
                !! so if we want omega we need,
                !!        - i freq) = i s + \omega
                !!                  = \hat{omega}
                !! VERIFICATION ------------------------------------
                !!           - \ii \hat{\omega}
                !!         = - \ii (\omega + \ii s)
                !!         = - \ii  \omega + s
                !!         = - freq.
                omega = - dcmplx(0.d0,1.d0) * freq 
                iomega= freq 

                u = -iomega*(lambdaR*omega*eta_m**2+eta_l*lambdaR*omega*eta_m & 
                    + eta_l*(muR**2)*iunit + eta_l*lambdaR*muR*iunit)
                v = (-eta_m**2 *(omega**2)*iunit   + 2.d0*eta_m*muR*omega &
                     -eta_l*eta_m*(omega**2)*iunit + lambdaR*eta_m*omega  &
                     + iunit*muR**2 + eta_l*muR*omega + lambdaR*muR*iunit)

                !! derivate w.r.t. muR; lambdaR; eta_mu; eta_lambda
                uprime = -iomega*(2.d0*eta_l*muR*iunit + eta_l*lambdaR*iunit)
                vprime = 2.d0*eta_m*omega + 2.d0*iunit*muR              &
                       + eta_l*omega + lambdaR*iunit
                dlambdacx_dmu    = (uprime*v-u*vprime)/(v**2)

                uprime = -iomega*(2.d0*lambdaR*omega*eta_m + eta_l*lambdaR*omega)
                vprime = - 2.d0*eta_m*(omega**2)*iunit + 2.d0*muR*omega &
                         - eta_l*(omega**2)*iunit + lambdaR*omega
                dlambdacx_detamu = (uprime*v-u*vprime)/(v**2)


                uprime = -iomega*(omega*eta_m**2+eta_l*omega*eta_m      &
                                  + eta_l*muR*iunit)
                vprime = (eta_m*omega + muR*iunit)                
                dlambdacx_dlambda= (uprime*v-u*vprime)/(v**2)

                uprime = -iomega*(lambdaR*omega*eta_m + (muR**2)*iunit  &
                         +lambdaR*muR*iunit)
                vprime = -eta_m*(omega**2)*iunit + muR*omega
                dlambdacx_detal  = (uprime*v-u*vprime)/(v**2)
                
                do idof=1,n !! loop over all ndof * dim_solution

                  !! (lambda, mu, rho, eta-lambda, eta-mu).
                  gradval(3) = gradval(3)+conjg(dAU(idof,3))*sol_bwd_weigh(idof)
                  
                  gradval(1) = gradval(1) + conjg(                      &
                                dAU(idof,1)*dlambdacx_dlambda)          &
                               *sol_bwd_weigh(idof)
                  gradval(2) = gradval(2) + conjg(                      &
                               (  dAU(idof,1)*dlambdacx_dmu             &
                                + dAU(idof,2)*dmucx_dmu     ))          &
                               *sol_bwd_weigh(idof)
                  !! eta-lambda
                  gradval(4) = gradval(4) + conjg(                      &
                                  dAU(idof,1)*dlambdacx_detal)          &
                               *sol_bwd_weigh(idof)
                  !! eta-mu
                  gradval(5) = gradval(5) + conjg(                      &
                               (  dAU(idof,1)*dlambdacx_detamu          &
                                + dAU(idof,2)*dmucx_detamu     ))       &
                               *sol_bwd_weigh(idof)
                end do
                
              !! -------------------------------------------------------
              !!
              !! Zener model
              !!
              !! -------------------------------------------------------
              case(model_visco_Zener)
                call model_eval_dof_index(ctx_model,icell,model_ETA_LDA  ,&
                                          k_model,freq,eta_l ,ctx_err)
                call model_eval_dof_index(ctx_model,icell,model_ETA_MU   ,&
                                          k_model,freq,eta_m ,ctx_err)
                call model_eval_dof_index(ctx_model,icell,model_TAU_SIGMA,&
                                          k_model,freq,tausig,ctx_err)
                call model_eval_dof_index(ctx_model,icell,model_TAU_EPS  ,&
                                          k_model,freq,taueps,ctx_err)
                !!   \lambda=\dfrac{\lambda_0 - i\omega\taue\eta}
                !!                 {1-i\omega\taus}
                !!   and we derive w.r.t \lambda0 so we have to adjust
                !!   the gradient also.
                !! we have
                !!
                !! lambda =  (lambda0 - i omega eta_lambda tau_eps) / (1 - i omega tau_sig)
                !! for instance
                !! dlambda / dlambda0 = 1/(1-iomega tau_sig)
                !! -----------------------------------------------------
                do idof=1,n !! loop over all ndof * dim_solution
                  !! (lambda, mu, rho, eta-lambda, eta-mu, tau_sigma, tau_epsilon).
                  gradval(1) = gradval(1) +                             &
                               conjg(dAU(idof,1)/(1.d0-freq*tausig))    &
                                          *sol_bwd_weigh(idof)
                  gradval(2) = gradval(2) +                             &
                               conjg(dAU(idof,2)/(1.d0-freq*tausig))    &
                                          *sol_bwd_weigh(idof)
                  gradval(3) = gradval(3)+conjg(dAU(idof,3))*sol_bwd_weigh(idof)
                  
                  gradval(4) = gradval(4) +                             &
                               conjg(dAU(idof,1)*(-freq*taueps)         &
                                                /(1.d0-freq*tausig))    &
                                                *sol_bwd_weigh(idof)
                  gradval(5) = gradval(5) +                             &
                               conjg(dAU(idof,2)*(-freq*taueps)         &
                                                /(1.d0-freq*tausig))    &
                                                *sol_bwd_weigh(idof)
            
                  gradval(6) = gradval(6) +                             &
                    conjg( (dAU(idof,1) + dAU(idof,2))                  &
                           *freq/(1.d0-freq*tausig)**2)                 &
                         *sol_bwd_weigh(idof)
                  gradval(7) = gradval(7) +                                  &
                    conjg( ( dAU(idof,1)*(-freq*eta_l)                       &
                            +dAU(idof,2)*(-freq*eta_m) )/(1.d0-freq*tausig)) &
                         *sol_bwd_weigh(idof)
                end do
                
            case default
                ctx_err%msg   = "** ERROR: Unrecognized viscosity model [grad]"
                ctx_err%ierr  = -1
                cycle !! possibility of non-shared error is dealt outside
            end select 
            
          end if !! end if viscosity 
          
          gradval = dcmplx(gradval / dble(ndof_vol)) !! average by ndof
                    
          ! ---------------------------------------------------------
          !! save value, we sum over the shots here, keep the i_model
          !! separated =================================================
          !! get global position on the array
          igbmod = igbmod_start + k_model
          do k=1,n_param_main
            gradient(igbmod,k) = gradient(igbmod,k)+real(gradval(k),kind=4)
          end do
            
          if(ctx_model%flag_viscous) then
            n_visco = size(ctx_model%visco)
            do i_model = n_param_main+1, n_param_main+n_visco
              gradient(igbmod,i_model) = gradient(igbmod,i_model)       &
                                      + real(gradval(i_model),kind=4)
            end do
          end if

        !! -------------------------------------------------------------
        end do !! end loop over shots
        !! -------------------------------------------------------------

      !! ---------------------------------------------------------------
      end do !! end loop over sub-model per cell
      !! ---------------------------------------------------------------
      
    end do !! end loop over cells
    !$OMP END DO

    deallocate(dA         )
    deallocate(sol_fwd_loc)
    deallocate(sol_bwd_loc)
    deallocate(sol_bwd_weigh)
    deallocate(dAU        )
    deallocate(coo_node   )
    deallocate(work       )
    deallocate(metric_weight)
    !$OMP END PARALLEL

    do k = 1, ctx_dg%order_to_index(ctx_dg%order_max_gb)
      call array_deallocate(mass_matrix_gb(k))
      call array_deallocate(mass_matrix_dm(k))
    end do
    deallocate(mass_matrix_dm)
    deallocate(mass_matrix_gb)
    return

  end subroutine create_gradient_quadrature_dof

end module m_create_gradient_quadrature
