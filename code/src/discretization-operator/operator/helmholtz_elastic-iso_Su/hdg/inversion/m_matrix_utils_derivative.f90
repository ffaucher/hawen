!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_utils_derivative.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix derivative for
!> HELMHOLTZ ELASTIC ISOTROPIC propagator using the HDG 
!> discretization:
!> We derive with respect to the physical parameter: \lambda, \mu, \rho
!
!------------------------------------------------------------------------------
module m_matrix_utils_derivative

  !! module used -------------------------------------------------------
  use m_define_precision,       only: IKIND_MESH, RKIND_POL
  use m_ctx_discretization,     only: t_discretization
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: hdg_build_dA_2D_tri
  public :: hdg_build_dA_3D_tet

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the matrix derivative dA, with respect to \lambda, \mu, \rho
  !> for HDG in HELMHOLTZ ELASTIC ISOTROPIC in 2D based upon 
  !> triangle discretization 
  !> dA(:,:,1) = dA / d{\lambda}
  !> dA(:,:,2) = dA / d{\mu }
  !> dA(:,:,3) = dA / d{\rho}
  !
  !> @param[inout] dA             : dA w.r.t the parameters
  !> @param[in]    vol_phiphi     : mass matrix needed for rho
  !> @param[in]    vol_phiphi_dSdm: mass matrix scaled with derivatives
  !>                              : w.r.t lambda/mu, size [3 x 3 x nparam]
  !>                              : => one tensor for each parameter derived.
  !> @param[in]    det_jac        : determinant of the cell jacobian
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !----------------------------------------------------------------------------
  subroutine hdg_build_dA_2D_tri(dA,vol_phiphi,vol_phiphi_dSdm,         &
                                 det_jac,ndof_vol,freq)
    
    implicit none
    
    complex(kind=8), allocatable     ,intent(inout):: dA(:,:,:)
    real   (kind=RKIND_POL)          ,intent(in)   :: vol_phiphi(:,:)
    complex(kind=RKIND_POL)          ,intent(in)   :: vol_phiphi_dSdm(:,:,:,:,:)
    real   (kind=8)                  ,intent(in)   :: det_jac
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: ndof_vol
    !!
    real   (kind=RKIND_POL) :: massterm
    complex(kind=RKIND_POL) :: masstens(3,3)
    integer                 :: i, j
    
    dA=0.0
    do i=1, ndof_vol ; do j=1, ndof_vol


      ! ----------------------------------------------------------------
      !! derivative w.r.t rho
      ! ----------------------------------------------------------------
      !! equation (1--2)
      massterm                       = vol_phiphi(i,j)*det_jac   
      dA(i         , j           ,3) = dcmplx(freq*freq*massterm)
      dA(i+ndof_vol, j+  ndof_vol,3) = dcmplx(freq*freq*massterm)
      ! ----------------------------------------------------------------
      !! derivative w.r.t lambda and mu, note that these are complex
      !! here, such that we also encode attenuation derivative that
      !! is treated later on.
      ! ----------------------------------------------------------------
      
      !! lambda --------------------------------------------------------
      masstens(:,:)= vol_phiphi_dSdm(i,j,:,:,1)*det_jac   
      ! equation (3)
      dA(i+2*ndof_vol, j+2*ndof_vol,1) =-dcmplx(masstens(1,1)) !! sxx
      dA(i+2*ndof_vol, j+3*ndof_vol,1) =-dcmplx(masstens(1,2)) !! szz
      dA(i+2*ndof_vol, j+4*ndof_vol,1) =-2.d0*dcmplx(masstens(1,3)) !! sxz
      ! equation (4)
      dA(i+3*ndof_vol, j+2*ndof_vol,1) =-dcmplx(masstens(2,1)) !! sxx
      dA(i+3*ndof_vol, j+3*ndof_vol,1) =-dcmplx(masstens(2,2)) !! szz
      dA(i+3*ndof_vol, j+4*ndof_vol,1) =-2.d0*dcmplx(masstens(2,3)) !! sxz
      ! equation (5)
      ! 2 from the Voigt2D matrix, and we further needs a 2 factor 
      ! because of the Voigt notation, that adds a 2 on the 
      ! off-diagonal tensor entries when put into vector.
      dA(i+4*ndof_vol, j+2*ndof_vol,1) =-1.d0*dcmplx(masstens(3,1)) !! sxx
      dA(i+4*ndof_vol, j+3*ndof_vol,1) =-1.d0*dcmplx(masstens(3,2)) !! szz
      dA(i+4*ndof_vol, j+4*ndof_vol,1) =-2.d0*dcmplx(masstens(3,3)) !! sxz
      
      !! mu ------------------------------------------------------------
      masstens(:,:)= vol_phiphi_dSdm(i,j,:,:,2)*det_jac
      ! equation (3)
      dA(i+2*ndof_vol, j+2*ndof_vol,2) =-dcmplx(masstens(1,1)) !! sxx
      dA(i+2*ndof_vol, j+3*ndof_vol,2) =-dcmplx(masstens(1,2)) !! szz
      dA(i+2*ndof_vol, j+4*ndof_vol,2) =-2.d0*dcmplx(masstens(1,3)) !! sxz
      ! equation (4)
      dA(i+3*ndof_vol, j+2*ndof_vol,2) =-dcmplx(masstens(2,1)) !! sxx
      dA(i+3*ndof_vol, j+3*ndof_vol,2) =-dcmplx(masstens(2,2)) !! szz
      dA(i+3*ndof_vol, j+4*ndof_vol,2) =-2.d0*dcmplx(masstens(2,3)) !! sxz
      ! equation (5)
      ! 2 from the Voigt2D matrix, and we further needs a 2 factor 
      ! because of the Voigt notation, that adds a 2 on the 
      ! off-diagonal tensor entries when put into vector.
      dA(i+4*ndof_vol, j+2*ndof_vol,2) =-1.d0*dcmplx(masstens(3,1)) !! sxx
      dA(i+4*ndof_vol, j+3*ndof_vol,2) =-1.d0*dcmplx(masstens(3,2)) !! szz
      dA(i+4*ndof_vol, j+4*ndof_vol,2) =-2.d0*dcmplx(masstens(3,3)) !! sxz
    enddo ; enddo

    return
  end subroutine hdg_build_dA_2D_tri

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create the matrix derivative dA, with respect to \lambda, \mu, \rho
  !> for HDG in HELMHOLTZ ELASTIC ISOTROPIC in 3 based upon 
  !> tetrahedral discretization 
  !> dA(:,:,1) = dA / d{\lambda}
  !> dA(:,:,2) = dA / d{\mu }
  !> dA(:,:,3) = dA / d{\rho}
  !
  !> @param[inout] dA             : dA w.r.t the parameters
  !> @param[in]    vol_phiphi     : mass matrix needed for rho
  !> @param[in]    vol_phiphi_dSdm: mass matrix scaled with derivatives
  !>                              : w.r.t lambda/mu, size [3 x 3 x nparam]
  !>                              : => one tensor for each parameter derived.
  !> @param[in]    det_jac        : determinant of the cell jacobian
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !----------------------------------------------------------------------------
  subroutine hdg_build_dA_3D_tet(dA,vol_phiphi,vol_phiphi_dSdm,         &
                                 det_jac,ndof_vol,freq)
    
    implicit none
    
    complex(kind=8), allocatable     ,intent(inout):: dA(:,:,:)
    real   (kind=RKIND_POL)          ,intent(in)   :: vol_phiphi(:,:)
    complex(kind=RKIND_POL)          ,intent(in)   :: vol_phiphi_dSdm(:,:,:,:,:)
    real   (kind=8)                  ,intent(in)   :: det_jac
    complex(kind=8)                  ,intent(in)   :: freq
    integer                          ,intent(in)   :: ndof_vol
    !!
    real   (kind=RKIND_POL) :: massterm
    complex(kind=RKIND_POL) :: masstens(6,6)
    integer                 :: i, j, p
    
    dA=0.0

    do i=1, ndof_vol ; do j=1, ndof_vol
      ! ----------------------------------------------------------------
      !! derivative w.r.t rho
      ! ----------------------------------------------------------------
      !! equation (1-2-3)
      massterm                       = vol_phiphi(i,j)*det_jac   
      dA(i           , j           ,3) = dcmplx(freq*freq*massterm)
      dA(i+  ndof_vol, j+  ndof_vol,3) = dcmplx(freq*freq*massterm)
      dA(i+2*ndof_vol, j+2*ndof_vol,3) = dcmplx(freq*freq*massterm)
      ! ----------------------------------------------------------------
      !! derivative w.r.t lambda and mu, note that these are complex
      !! here, such that we also encode attenuation derivative that
      !! is treated later on.
      ! ----------------------------------------------------------------

      !! loop over lambda and mu ---------------------------------------
      do p=1,2
        masstens(:,:)= vol_phiphi_dSdm(i,j,:,:,p)*det_jac   

        ! equation (4)
        dA(i+3*ndof_vol, j+3*ndof_vol,p) =-dcmplx(masstens(1,1)) ! sxx
        dA(i+3*ndof_vol, j+4*ndof_vol,p) =-dcmplx(masstens(1,2)) ! syy
        dA(i+3*ndof_vol, j+5*ndof_vol,p) =-dcmplx(masstens(1,3)) ! szz
        dA(i+3*ndof_vol, j+6*ndof_vol,p) =-2.d0*dcmplx(masstens(1,4)) ! sxy
        dA(i+3*ndof_vol, j+7*ndof_vol,p) =-2.d0*dcmplx(masstens(1,5)) ! sxz
        dA(i+3*ndof_vol, j+8*ndof_vol,p) =-2.d0*dcmplx(masstens(1,6)) ! syz
        ! equation (5)
        dA(i+4*ndof_vol, j+3*ndof_vol,p) =-dcmplx(masstens(2,1)) ! sxx
        dA(i+4*ndof_vol, j+4*ndof_vol,p) =-dcmplx(masstens(2,2)) ! syy
        dA(i+4*ndof_vol, j+5*ndof_vol,p) =-dcmplx(masstens(2,3)) ! szz
        dA(i+4*ndof_vol, j+6*ndof_vol,p) =-2.d0*dcmplx(masstens(2,4)) ! sxy
        dA(i+4*ndof_vol, j+7*ndof_vol,p) =-2.d0*dcmplx(masstens(2,5)) ! sxz
        dA(i+4*ndof_vol, j+8*ndof_vol,p) =-2.d0*dcmplx(masstens(2,6)) ! syz
        ! equation (6)
        dA(i+5*ndof_vol, j+3*ndof_vol,p) =-dcmplx(masstens(3,1)) ! sxx
        dA(i+5*ndof_vol, j+4*ndof_vol,p) =-dcmplx(masstens(3,2)) ! syy
        dA(i+5*ndof_vol, j+5*ndof_vol,p) =-dcmplx(masstens(3,3)) ! szz
        dA(i+5*ndof_vol, j+6*ndof_vol,p) =-2.d0*dcmplx(masstens(3,4)) ! sxy
        dA(i+5*ndof_vol, j+7*ndof_vol,p) =-2.d0*dcmplx(masstens(3,5)) ! sxz
        dA(i+5*ndof_vol, j+8*ndof_vol,p) =-2.d0*dcmplx(masstens(3,6)) ! syz
        
        ! equation (7)
        ! 2 from the Voigt3D matrix, and we further needs a 2 factor 
        ! because of the Voigt notation, that adds a 2 on the 
        ! off-diagonal tensor entries when put into vector.
        dA(i+6*ndof_vol, j+3*ndof_vol,p) =-1.d0*dcmplx(masstens(4,1)) ! sxx
        dA(i+6*ndof_vol, j+4*ndof_vol,p) =-1.d0*dcmplx(masstens(4,2)) ! syy
        dA(i+6*ndof_vol, j+5*ndof_vol,p) =-1.d0*dcmplx(masstens(4,3)) ! szz
        dA(i+6*ndof_vol, j+6*ndof_vol,p) =-2.d0*dcmplx(masstens(4,4)) ! sxy
        dA(i+6*ndof_vol, j+7*ndof_vol,p) =-2.d0*dcmplx(masstens(4,5)) ! sxz
        dA(i+6*ndof_vol, j+8*ndof_vol,p) =-2.d0*dcmplx(masstens(4,6)) ! syz
        
        ! equation (8)
        ! 2 from the Voigt3D matrix, and we further needs a 2 factor 
        ! because of the Voigt notation, that adds a 2 on the 
        ! off-diagonal tensor entries when put into vector.
        dA(i+7*ndof_vol, j+3*ndof_vol,p) =-1.d0*dcmplx(masstens(5,1)) ! sxx
        dA(i+7*ndof_vol, j+4*ndof_vol,p) =-1.d0*dcmplx(masstens(5,2)) ! syy
        dA(i+7*ndof_vol, j+5*ndof_vol,p) =-1.d0*dcmplx(masstens(5,3)) ! szz
        dA(i+7*ndof_vol, j+6*ndof_vol,p) =-2.d0*dcmplx(masstens(5,4)) ! sxy
        dA(i+7*ndof_vol, j+7*ndof_vol,p) =-2.d0*dcmplx(masstens(5,5)) ! sxz
        dA(i+7*ndof_vol, j+8*ndof_vol,p) =-2.d0*dcmplx(masstens(5,6)) ! syz
        
        ! equation (9)
        ! 2 from the Voigt3D matrix, and we further needs a 2 factor 
        ! because of the Voigt notation, that adds a 2 on the 
        ! off-diagonal tensor entries when put into vector.
        dA(i+8*ndof_vol, j+3*ndof_vol,p) =-1.d0*dcmplx(masstens(6,1)) ! sxx
        dA(i+8*ndof_vol, j+4*ndof_vol,p) =-1.d0*dcmplx(masstens(6,2)) ! syy
        dA(i+8*ndof_vol, j+5*ndof_vol,p) =-1.d0*dcmplx(masstens(6,3)) ! szz
        dA(i+8*ndof_vol, j+6*ndof_vol,p) =-2.d0*dcmplx(masstens(6,4)) ! sxy
        dA(i+8*ndof_vol, j+7*ndof_vol,p) =-2.d0*dcmplx(masstens(6,5)) ! sxz
        dA(i+8*ndof_vol, j+8*ndof_vol,p) =-2.d0*dcmplx(masstens(6,6)) ! syz
      end do
    enddo ; enddo

    return
  end subroutine hdg_build_dA_3D_tet

end module m_matrix_utils_derivative
