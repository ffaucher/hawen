!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_reciprocity_formula.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the reciprocity formula for 
!> elastic formulation: it depends on the problem 
!> dimension but basically, we need all V and Sigma
!> 
!
!------------------------------------------------------------------------------
module m_reciprocity_formula

  use m_raise_error,              only : raise_error, t_error
  use m_define_precision,         only : RKIND_MAT
  use m_tag_namefield

  implicit none

  private
  public  :: reciprocity_fields,reciprocity_formula
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Provides the fields that are required to compute the reciprocity.
  !> deending on the receivers normal directions, not everything has 
  !> to be known.
  !
  !> @param[in]    dim_domain      : dimension of the domain
  !> @param[out]   name_field_out  : names of the fields for reciprocity
  !> @param[in]    normals_rcvx    : maximal normal direction in x
  !> @param[in]    normals_rcvy    : maximal normal direction in y
  !> @param[in]    normals_rcvz    : maximal normal direction in z
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine reciprocity_fields(dim_domain,name_field_out,normals_rcvx, &
                                normals_rcvy,normals_rcvz,ctx_err)
    
    implicit none
    
    integer                      ,intent(in)   :: dim_domain
    character(len=*),allocatable ,intent(inout):: name_field_out(:)
    real(kind=8)                 ,intent(in)   :: normals_rcvx
    real(kind=8)                 ,intent(in)   :: normals_rcvy
    real(kind=8)                 ,intent(in)   :: normals_rcvz
    type(t_error)                ,intent(out)  :: ctx_err
    !!
    real(kind=8) :: tol

    ctx_err%ierr  = 0
    tol = 1E-12

    !! -----------------------------------------------------------------
    !! Reciprocity formula depends on the dimension
    !! -----------------------------------------------------------------  
    select case(dim_domain)
      case(2)
        !! We respect the numbering of the equations -------------------
        name_field_out(1) = tag_FIELD_UX
        name_field_out(2) = tag_FIELD_UZ
        name_field_out(3) = tag_FIELD_SXX
        name_field_out(4) = tag_FIELD_SZZ
        name_field_out(5) = tag_FIELD_SXZ

        !! the rest depends on the normal, 
        if(normals_rcvx < tol) name_field_out(3) = '' !> remove sxx
        if(normals_rcvz < tol) name_field_out(4) = '' !> remove szz
        !! consistancy 
        if(normals_rcvx < tol .and. normals_rcvz < tol) then
          ctx_err%msg   = "** ERROR: Normal receivers direction is below "// &
                          "tolerance [reciprocity_fields] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if 


      case(3)
        !! We respect the numbering of the equations -------------------
        name_field_out(1) = tag_FIELD_UX
        name_field_out(2) = tag_FIELD_UY
        name_field_out(3) = tag_FIELD_UZ
        name_field_out(4) = tag_FIELD_SXX
        name_field_out(5) = tag_FIELD_SYY
        name_field_out(6) = tag_FIELD_SZZ
        name_field_out(7) = tag_FIELD_SXY
        name_field_out(8) = tag_FIELD_SXZ
        name_field_out(9) = tag_FIELD_SYZ
        !! the rest depends on the normal, 
        if(normals_rcvx < tol) name_field_out(4) = '' !> remove sxx
        if(normals_rcvy < tol) name_field_out(5) = '' !> remove syy
        if(normals_rcvz < tol) name_field_out(6) = '' !> remove szz
        
        if(normals_rcvx < tol .and. normals_rcvy < tol) name_field_out(7)=''!sxy
        if(normals_rcvx < tol .and. normals_rcvz < tol) name_field_out(8)=''!sxz
        if(normals_rcvy < tol .and. normals_rcvz < tol) name_field_out(9)=''!syz
        
        if(normals_rcvx < tol .and. normals_rcvy < tol .and. &
           normals_rcvz < tol) then
          ctx_err%msg   = "** ERROR: Normal receivers direction is below "// &
                          "tolerance [reciprocity_fields] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

      case default
        ctx_err%msg   = "** ERROR: Equation dimension must be 2/3 "// &
                        "[reciprocity_fields] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select    
    !! -----------------------------------------------------------------

    return
  end subroutine reciprocity_fields

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> Compute the reciprocity misfit functional and appropriate 
  !> difference for rhs 
  !
  !> @param[in]    dim_domain      : dimension of the domain
  !> @param[in]    obs             : observation
  !> @param[in]    sim             : simulation
  !> @param[in]    n_val           : number of receivers
  !> @param[out]   cost            : output cost value
  !> @param[inout] rhs             : rhs array for backpropagation
  !> @param[in]    nx              : normal direction in x
  !> @param[in]    ny              : normal direction in y
  !> @param[in]    nz              : normal direction in z
  !> @param[in]    flag_log        : indicates if log functional
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine reciprocity_formula(dim_domain,obs,sim,n_val,cost,rhs,     &
                                 nx,ny,nz,flag_log,ctx_err)
    
    implicit none
    
    integer                             ,intent(in)   :: dim_domain
    complex(kind=RKIND_MAT),allocatable ,intent(in)   :: obs(:,:),sim(:,:)
    integer                             ,intent(in)   :: n_val
    complex(kind=8)        ,allocatable ,intent(inout):: rhs(:,:)
    real   (kind=8)                     ,intent(inout):: cost
    real   (kind=8)        ,allocatable ,intent(in)   :: nx(:)
    real   (kind=8)        ,allocatable ,intent(in)   :: ny(:)
    real   (kind=8)        ,allocatable ,intent(in)   :: nz(:)
    logical                             ,intent(in)   :: flag_log
    type(t_error)                       ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8) :: tol
    complex(kind=8) :: field1, field2, delta, v1, v2
    integer         :: k
    !! -----------------------------------------------------------------

    tol     = 1.E-20 !! for log norm only
    rhs     = 0.0
    cost    = 0.0
    field1  = 0.0
    field2  = 0.0

    select case(dim_domain)
      ! ----------------------------------------------------------------
      ! ----------------------------------------------------------------
      !! TWO DIMENSIONS
      ! ----------------------------------------------------------------
      ! ----------------------------------------------------------------
      case(2)
        !! standard formulation
        if(.not. flag_log) then
          do k=1,n_val
            !!   (UX_sim*SXX_obs + UZ_sim*SXZ_obs).NX 
            !! + (UX_sim*SXZ_obs + UZ_sim*SZZ_obs).NZ
            field1 = field1 + dcmplx(                                     &
                     sim(k,1)*obs(k,3)*nx(k) + sim(k,2)*obs(k,5)*nx(k)  + &
                     sim(k,1)*obs(k,5)*nz(k) + sim(k,2)*obs(k,4)*nz(k))
            field2 = field2 + dcmplx(                                     &
                     obs(k,1)*sim(k,3)*nx(k) + obs(k,2)*sim(k,5)*nx(k)  + &
                     obs(k,1)*sim(k,5)*nz(k) + obs(k,2)*sim(k,4)*nz(k))
            rhs(k,1) = conjg(obs(k,3)*nx(k) + obs(k,5)*nz(k)) !! UX
            rhs(k,2) = conjg(obs(k,5)*nx(k) + obs(k,4)*nz(k)) !! UZ
            rhs(k,3) =-conjg(obs(k,1)*nx(k))                  !! SXX
            rhs(k,4) =-conjg(obs(k,2)*nz(k))                  !! SZZ
            rhs(k,5) =-conjg(obs(k,2)*nx(k)+obs(k,1)*nz(k))   !! SXZ
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        !! LOG FORMULATION ---------------------------------------------
        else
          do k=1,n_val
            !!   (UX_sim*SXX_obs + UZ_sim*SXZ_obs).NX 
            !! + (UX_sim*SXZ_obs + UZ_sim*SZZ_obs).NZ
            v1 = dcmplx(                                                &
                     sim(k,1)*obs(k,3)*nx(k) + sim(k,2)*obs(k,5)*nx(k)+ &
                     sim(k,1)*obs(k,5)*nz(k) + sim(k,2)*obs(k,4)*nz(k))
            v2 = dcmplx(                                                &
                     obs(k,1)*sim(k,3)*nx(k) + obs(k,2)*sim(k,5)*nx(k)  + &
                     obs(k,1)*sim(k,5)*nz(k) + obs(k,2)*sim(k,4)*nz(k))
            !! only if both are ok 
            if(abs(v1) > tol .and. abs(v2) > tol) then
              field1 = field1 + log(v1)
              field2 = field2 + log(v2)
            
              !! the derivative makes the log desapear
              if(abs(sim(k,1)) > tol) rhs(k,1) = conjg(1./sim(k,1) *    &
                                      (obs(k,3)*nx(k) + obs(k,5)*nz(k))) !! UX
              if(abs(sim(k,2)) > tol) rhs(k,2) = conjg(1./sim(k,2) *    &
                                      (obs(k,5)*nx(k) + obs(k,4)*nz(k))) !! UZ
              if(abs(sim(k,3)) > tol) rhs(k,3) =-conjg(1./sim(k,3)*     &
                                                        obs(k,1)*nx(k))  !! SXX
              if(abs(sim(k,4)) > tol) rhs(k,4) =-conjg(1./sim(k,4) *    &
                                                        obs(k,2)*nz(k))  !! SZZ
              if(abs(sim(k,5)) > tol) rhs(k,5) =-conjg(1./sim(k,5) *    &
                                      (obs(k,2)*nx(k) + obs(k,1)*nz(k))) !! SXZ
            end if
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        endif
      case(3)
        ! ---------------------------------------------------------
        ! ---------------------------------------------------------
        !! THREE DIMENSIONS
        ! ---------------------------------------------------------
        ! ---------------------------------------------------------
        !! standard formulation
        if(.not. flag_log) then
          do k=1,n_val
            !!   (UX_sim*SXX_obs + UY_sim*SXY_obs + UZ_sim*SXZ_obs).NX 
            !! + (UX_sim*SXY_obs + UY_sim*SYY_obs + UZ_sim*SYZ_obs).NY 
            !! + (UX_sim*SXZ_obs + UY_sim*SYZ_obs + UZ_sim*SZZ_obs).NZ 
            field1 = field1 + dcmplx(                                     &
            sim(k,1)*obs(k,4)*nx(k)+sim(k,2)*obs(k,7)*nx(k)+sim(k,3)*obs(k,8)*nx(k) + &
            sim(k,1)*obs(k,7)*ny(k)+sim(k,2)*obs(k,5)*ny(k)+sim(k,3)*obs(k,9)*ny(k) + &
            sim(k,1)*obs(k,8)*nz(k)+sim(k,2)*obs(k,9)*nz(k)+sim(k,3)*obs(k,6)*nz(k))
          
            field2 = field2 + dcmplx(                                     &
            obs(k,1)*sim(k,4)*nx(k)+obs(k,2)*sim(k,7)*nx(k)+obs(k,3)*sim(k,8)*nx(k) + &
            obs(k,1)*sim(k,7)*ny(k)+obs(k,2)*sim(k,5)*ny(k)+obs(k,3)*sim(k,9)*ny(k) + &
            obs(k,1)*sim(k,8)*nz(k)+obs(k,2)*sim(k,9)*nz(k)+obs(k,3)*sim(k,6)*nz(k))
            
            rhs(k,1) = conjg(obs(k,4)*nx(k) + sim(k,7)*ny(k) + sim(k,8)*nz(k))!! UX
            rhs(k,2) = conjg(obs(k,7)*nx(k) + sim(k,5)*ny(k) + sim(k,9)*nz(k))!! UY
            rhs(k,3) = conjg(obs(k,8)*nx(k) + sim(k,9)*ny(k) + sim(k,6)*nz(k))!! UZ
            rhs(k,4) =-conjg(obs(k,1)*nx(k))                  !! SXX
            rhs(k,5) =-conjg(obs(k,2)*ny(k))                  !! SYY
            rhs(k,6) =-conjg(obs(k,3)*nz(k))                  !! SZZ
            rhs(k,7) =-conjg(obs(k,2)*nx(k) + obs(k,1)*ny(k)) !! SXY
            rhs(k,8) =-conjg(obs(k,3)*nx(k) + obs(k,1)*nz(k)) !! SXZ
            rhs(k,9) =-conjg(obs(k,3)*ny(k) + obs(k,2)*nz(k)) !! SYZ
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        !! LOG FORMULATION ---------------------------------------------
        else
          do k=1,n_val
            !!   (UX_sim*SXX_obs + UY_sim*SXY_obs + UZ_sim*SXZ_obs).NX 
            !! + (UX_sim*SXY_obs + UY_sim*SYY_obs + UZ_sim*SYZ_obs).NY 
            !! + (UX_sim*SXZ_obs + UY_sim*SYZ_obs + UZ_sim*SZZ_obs).NZ 
            v1 = dcmplx(                                                            &
            sim(k,1)*obs(k,4)*nx(k)+sim(k,2)*obs(k,7)*nx(k)+sim(k,3)*obs(k,8)*nx(k)+&
            sim(k,1)*obs(k,7)*ny(k)+sim(k,2)*obs(k,5)*ny(k)+sim(k,3)*obs(k,9)*ny(k)+&
            sim(k,1)*obs(k,8)*nz(k)+sim(k,2)*obs(k,9)*nz(k)+sim(k,3)*obs(k,6)*nz(k))
            v2 = dcmplx(                                                            &
            obs(k,1)*sim(k,4)*nx(k)+obs(k,2)*sim(k,7)*nx(k)+obs(k,3)*sim(k,8)*nx(k)+&
            obs(k,1)*sim(k,7)*ny(k)+obs(k,2)*sim(k,5)*ny(k)+obs(k,3)*sim(k,9)*ny(k)+&
            obs(k,1)*sim(k,8)*nz(k)+obs(k,2)*sim(k,9)*nz(k)+obs(k,3)*sim(k,6)*nz(k))

            !! only if both are ok for log
            if(abs(v1) > tol .and. abs(v2) > tol) then
              field1 = field1 + log(v1)
              field2 = field2 + log(v2)
              
              !! the derivative makes the log desapear
              if(abs(sim(k,1)) > tol) rhs(k,1) = conjg(1./sim(k,1) *    & 
                (obs(k,4)*nx(k) + sim(k,7)*ny(k) + sim(k,8)*nz(k)))     !! UX
              if(abs(sim(k,2)) > tol) rhs(k,2) = conjg(1./sim(k,2) *    & 
                (obs(k,7)*nx(k) + sim(k,5)*ny(k) + sim(k,9)*nz(k)))     !! UY
              if(abs(sim(k,3)) > tol) rhs(k,3) = conjg(1./sim(k,3) *    & 
                (obs(k,8)*nx(k) + sim(k,9)*ny(k) + sim(k,6)*nz(k)))     !! UZ
              if(abs(sim(k,4)) > tol) rhs(k,4) =-conjg(1./sim(k,4)*(obs(k,1)*nx(k))) !! SXX
              if(abs(sim(k,5)) > tol) rhs(k,5) =-conjg(1./sim(k,5)*(obs(k,2)*ny(k))) !! SYY
              if(abs(sim(k,6)) > tol) rhs(k,6) =-conjg(1./sim(k,6)*(obs(k,3)*nz(k))) !! SZZ
              if(abs(sim(k,7)) > tol) rhs(k,7) =-conjg(1./sim(k,7) *    & 
                (obs(k,2)*nx(k) + obs(k,1)*ny(k)))                      !! SXY
              if(abs(sim(k,8)) > tol) rhs(k,8) =-conjg(1./sim(k,8) *    & 
                (obs(k,3)*nx(k) + obs(k,1)*nz(k)))                      !! SXZ
              if(abs(sim(k,9)) > tol) rhs(k,9) =-conjg(1./sim(k,9) *    &
                (obs(k,3)*ny(k) + obs(k,2)*nz(k)))                      !! SYZ
            end if
          end do
          !! scaling
          delta = dcmplx(field1-field2)
          rhs = rhs * delta
          cost= dble(delta*conjg(delta))
        end if

      case default
        ctx_err%msg   = "** ERROR: Equation dimension must be 2/3 "// &
                        "[reciprocity_formula] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        !! non-shared error
    end select    
    !! -----------------------------------------------------------------

    return
  end subroutine reciprocity_formula



end module m_reciprocity_formula
