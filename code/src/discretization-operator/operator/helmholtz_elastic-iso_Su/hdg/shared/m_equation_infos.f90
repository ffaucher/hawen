!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_equation_infos.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module init of the information on the Helmholtz elastic
!> isotropic equation in the case of HDG discretization 
!>  i.e. we use order 1 formulation 
!
!------------------------------------------------------------------------------
module m_equation_infos

  use m_raise_error,              only : raise_error, t_error
  use m_tag_namefield
  use m_tag_scale_rhs,            only : tag_RHS_SCALE_ID
  implicit none

  private
  public  :: equation_infos
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init equation information for Helmholtz elastic isotropic using HDG
  !
  !> @param[in]    dim_domain      : dimension of the domain
  !> @param[out]   freq_dep        : indicates if this equation has a 
  !>                                 frequency parameter (yes for helmholtz!)
  !> @param[out]   mode_dep        : indicates if this equation has a mode 
  !> @param[out]   dim_solution    : number of solution
  !> @param[out]   dim_var         : number of matrix variable
  !> @param[out]   name_sol        : names of the solution
  !> @param[out]   name_var        : names of the variable (traces)
  !> @param[out]   symmetric_matrix: symmetry of the discretized matrix
  !> @param[out]   tag_scale_rhs   : indicates if rhs scaling
  !> @param[out]   n_media         : indicates maximal number of different media
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine equation_infos(dim_domain,                                 &
                            frequency_dependency,mode_dependency,eqname,&
                            medium,dim_solution,dim_var,name_sol,       &
                            name_var,formulation_order,symmetric_matrix,&
                            tag_scale_rhs,n_media,ctx_err)
    
    implicit none
    
    integer                      ,intent(in)   :: dim_domain
    integer                      ,intent(out)  :: dim_solution,dim_var
    integer                      ,intent(out)  :: formulation_order
    character(len=32)            ,intent(out)  :: eqname,medium
    character(len=3) ,allocatable,intent(inout):: name_sol(:),name_var(:)
    logical                      ,intent(out)  :: frequency_dependency
    logical                      ,intent(out)  :: mode_dependency
    logical                      ,intent(out)  :: symmetric_matrix
    integer                      ,intent(out)  :: tag_scale_rhs
    integer                      ,intent(out)  :: n_media
    type(t_error)                ,intent(out)  :: ctx_err
    !!

    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! no scale for the RHS
    tag_scale_rhs = tag_RHS_SCALE_ID
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! Helmholtz elastic equation using HDG
    !! order 1 formulation
    !! -----------------------------------------------------------------
    eqname   ='helmholtz'
    medium   ='elastic-isotropic'
    frequency_dependency=.true.
    mode_dependency     =.false.
    n_media  = 1 !! only one type of medium in this problem.
    
    formulation_order= 1          !! order 1 formuation
    dim_var          = dim_domain !! matrix variables (Lagrange multipliers)
    allocate(name_var(dim_var))
    symmetric_matrix = .true. !! always symmetric

    !! the solution dimension depends on the domain dim
    select case(dim_domain)
      case(2)
        dim_solution     = 5 !! UX, UZ, SXX, SZZ, SXZ
        allocate(name_sol(dim_solution))        
        name_sol(1) = tag_FIELD_UX
        name_sol(2) = tag_FIELD_UZ
        name_sol(3) = tag_FIELD_SXX
        name_sol(4) = tag_FIELD_SZZ
        name_sol(5) = tag_FIELD_SXZ
        
        name_var(1) = tag_FIELD_UX !! numerical traces
        name_var(2) = tag_FIELD_UZ !! numerical traces
      case(3)
        dim_solution     = 9 !! UX, UY, UZ, SXX, SYY, SZZ, SXY, SXZ, SYZ
        allocate(name_sol(dim_solution))
        name_sol(1) = tag_FIELD_UX
        name_sol(2) = tag_FIELD_UY
        name_sol(3) = tag_FIELD_UZ
        name_sol(4) = tag_FIELD_SXX
        name_sol(5) = tag_FIELD_SYY
        name_sol(6) = tag_FIELD_SZZ
        name_sol(7) = tag_FIELD_SXY
        name_sol(8) = tag_FIELD_SXZ
        name_sol(9) = tag_FIELD_SYZ

        name_var(1) = tag_FIELD_UX !! numerical traces
        name_var(2) = tag_FIELD_UY !! numerical traces
        name_var(3) = tag_FIELD_UZ !! numerical traces
      case default
        ctx_err%msg   = "** ERROR: Equation dimension must be 2/3 "// &
                        "[equation_infos_helmholtz_elastic_HDG] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)

    end select    
    !! -----------------------------------------------------------------

    return
  end subroutine equation_infos
end module m_equation_infos
