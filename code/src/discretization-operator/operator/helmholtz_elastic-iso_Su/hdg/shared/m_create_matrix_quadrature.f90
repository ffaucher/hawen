!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_matrix_quadrature.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> HELMHOLTZ ELASTIC ISOTROPIC propagator using the 
!> HDG discretization with quadrature rules to evaluate
!> the integrals
!
!------------------------------------------------------------------------------
module m_create_matrix_quadrature

  !! module used -------------------------------------------------------
  use omp_lib
  use, intrinsic                   :: ieee_arithmetic
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MAT, IKIND_MESH, RKIND_MAT, &
                                      RKIND_POL
  !> matrix and cartesian domain types
  use m_array_types,            only: t_array2d_real,t_array3d_real,    &
                                      t_array5d_complex,array_allocate, &
                                      array_deallocate,t_array4d_complex
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_tag_bc
  use m_ctx_model,              only: t_model
  use m_matrix_utils,           only: hdg_build_Ainv_2D_tri,            &
                                      hdg_build_C_2D_tri,               &
                                      hdg_build_Ainv_3D_tet,            &
                                      hdg_build_C_3D_tet,               &
                                      hdg_build_quadrature_int_2D,      &
                                      hdg_build_quadrature_int_3D,      &
                                      hdg_model_on_midface_2D,          &
                                      hdg_model_on_midface_3D
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: create_matrix_quadrature_2D
  public :: create_matrix_quadrature_3D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from HELMHOLTZ ELASTIC using HDG in 2D
  !
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[in]    model          : model parameters
  !> @param[inout] ctx_dg         : type DG
  !> @param[out]   dim_nnz_loc    : local nonzeros
  !> @param[inout] ilin(:)        : matrix I
  !> @param[inout] icol(:)        : matrix J
  !> @param[inout] Aval(:)        : matrix values
  !> @param[in]    frequency      : frequency
  !> @param[in]    flag_inv       : indicates inversion
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_quadrature_2D(ctx_mesh,model,ctx_dg,dim_nnz_loc,&
                                      Alin,Acol,Aval,freq,flag_inv,ctx_err)
    implicit none
    type(t_domain)                     ,intent(in)      :: ctx_mesh
    type(t_model)                      ,intent(in)      :: model
    type(t_discretization)             ,intent(inout)   :: ctx_dg
    integer(kind=8)                    ,intent(out)     :: dim_nnz_loc
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Alin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Acol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout)   :: Aval(:)
    complex(kind=8)                    ,intent(in)      :: freq
    logical                            ,intent(in)      :: flag_inv
    type(t_error)                      ,intent(inout)   :: ctx_err
    !! local
    integer(kind=IKIND_MAT)             :: nnz_exact
    real   (kind=8)                     :: tol
    !! hdg
    complex(kind=8), allocatable :: coeff(:,:)
    complex(kind=8), allocatable :: hdgB(:,:)
    complex(kind=8), allocatable :: hdgC(:,:)
    complex(kind=8), allocatable :: hdgAinv(:,:)
    complex(kind=8), allocatable :: hdgL(:,:)
    complex(kind=8), allocatable :: sumface_phiphi_tau(:,:,:,:)
    complex(kind=8), allocatable :: face_phii_xij     (:,:,:)    
    complex(kind=8), allocatable :: face_phii_xij_tau (:,:,:,:,:)    
    complex(kind=8)              :: vp,vs,temp,tempmat(2,2)
    complex(kind=8)              :: BCnormal(2,2),BCparam  (2,2)
    complex(kind=8)              :: BCmatrix(2,2),BCnormalT(2,2)
    complex(kind=8)              :: penalization(3) !! 2D triangle, 3 faces
    real   (kind=8)              :: rho
    integer                      :: tag_penalization
    !! evaluation of the integrals
    type(t_array2d_real)   ,allocatable :: vol_phi_phi    (:)
    type(t_array2d_real)   ,allocatable :: vol_phi_phi_rho(:)
    type(t_array4d_complex),allocatable :: vol_phi_phi_S  (:)
    type(t_array3d_real)   ,allocatable :: vol_dphi_phi   (:)
    type(t_array3d_real)   ,allocatable :: face_phi_phi    (:,:)
    type(t_array5d_complex),allocatable :: face_phi_phi_tau(:,:)

    integer(kind=IKIND_MESH) :: i_cell,face,neigh,index_face
    integer(kind=IKIND_MAT)  :: iline, icol
    integer        :: order,i_orient,dim_var,nval_i,n,ndof_vol_l
    integer        :: i_order_cell,ndof_vol,ndof_max_vol,dim_domain,i,j
    integer        :: ndof_max_face,dim_sol,ndof_face_tot,ndof_face
    integer        :: index_face_dofi,index_face_dofj,nval_j,ipos,jpos
    integer        :: i_o_neigh,iface,jface,ivar,jvar,ndof_face_cell
    integer        :: ndf_edges_elem (7) !! 2D triangles, 3 faces x 2 fields + 1 to sum
    integer        :: i_order_neigh  (3) !! 2D triangles, 3 faces
    integer        :: ndof_face_neigh(3) !! 2D triangles, 3 faces
    integer        :: ndof_vol_neigh (3) !! 2D triangles, 3 faces
    !! for symmetrization of the matrix even with Dirichlet boundary.
    integer        :: ind_sym
    logical        :: flag_info_sym(3)   !! 2D triangle, 3 faces
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! init informations on the matrix and hdg infos
    !! -----------------------------------------------------------------
    Alin = 0  ; Acol=0    ; Aval= 0.0

    nnz_exact = 0
    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif
    !! compute max order dof
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    dim_var       = ctx_dg%dim_var
    dim_domain    = ctx_dg%dim_domain
    !! make sure it is zero at start as it is updated.
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0

    !! ----------------------------------------------------------------- 
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(coeff,hdgB,hdgC,hdgAinv),      &
    !$OMP&         PRIVATE(hdgL,sumface_phiphi_tau,face_phii_xij),        &
    !$OMP&         PRIVATE(face_phii_xij_tau,vp,vs,temp,tempmat),         &
    !$OMP&         PRIVATE(BCnormal,BCparam,BCmatrix,BCnormalT),          &
    !$OMP&         PRIVATE(penalization,rho,tag_penalization),            &
    !$OMP&         PRIVATE(vol_phi_phi,vol_phi_phi_rho,vol_phi_phi_S),    &
    !$OMP&         PRIVATE(vol_dphi_phi,face_phi_phi,face_phi_phi_tau),   &
    !$OMP&         PRIVATE(i_cell,face,neigh,index_face,iline,icol),      &
    !$OMP&         PRIVATE(order,i_orient,nval_i,n,ndof_vol_l),           &
    !$OMP&         PRIVATE(i_order_cell,ndof_vol,i,j,ndof_face_tot),      &
    !$OMP&         PRIVATE(ndof_face,index_face_dofi,index_face_dofj),    &
    !$OMP&         PRIVATE(nval_j,ipos,jpos,i_o_neigh,iface,jface,ivar),  &
    !$OMP&         PRIVATE(jvar,ndof_face_cell,ndf_edges_elem),           &
    !$OMP&         PRIVATE(i_order_neigh,ndof_face_neigh,ndof_vol_neigh), &
    !$OMP&         PRIVATE(ind_sym,flag_info_sym)
    
    !! local array, the 3 is because of the 3 triangle faces
    allocate(coeff  (3*dim_var*ndof_max_face,3*dim_var*ndof_max_face))
    allocate(hdgL   (3*dim_var*ndof_max_face,3*dim_var*ndof_max_face))
    allocate(hdgB   (3*dim_var*ndof_max_face,  dim_sol*ndof_max_vol ))
    allocate(hdgC   (  dim_sol*ndof_max_vol ,3*dim_var*ndof_max_face))
    allocate(hdgAinv(  dim_sol*ndof_max_vol ,  dim_sol*ndof_max_vol ))
    !! for integrations of basis functions
    allocate(face_phii_xij    (3,ndof_max_vol,ndof_max_face))
    allocate(face_phii_xij_tau(3,ndof_max_vol,ndof_max_face,2,2))
    allocate(sumface_phiphi_tau( ndof_max_vol,ndof_max_vol ,2,2))
    !! for quadrature, once and for all
    allocate(vol_phi_phi     (ctx_dg%n_different_order))
    allocate(vol_phi_phi_rho (ctx_dg%n_different_order))
    allocate(vol_phi_phi_S   (ctx_dg%n_different_order))
    allocate(vol_dphi_phi    (ctx_dg%n_different_order))
    allocate(face_phi_phi    (ctx_dg%n_different_order,ctx_dg%n_different_order))
    allocate(face_phi_phi_tau(ctx_dg%n_different_order,ctx_dg%n_different_order))

    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! size depends if derivative or not
      call array_allocate(vol_phi_phi    (i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi_rho(i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi_S  (i),ndof_vol,ndof_vol,3,3,ctx_err)
      call array_allocate(vol_dphi_phi(i),ndof_vol,ndof_vol,            &
                          dim_domain,ctx_err)

      do j = 1, ctx_dg%n_different_order
        ndof_vol_l = ctx_dg%n_dof_per_order(j)
        call array_allocate(face_phi_phi(i,j),ndof_vol,ndof_vol_l,      &
                             ctx_mesh%n_neigh_per_cell,ctx_err)
        call array_allocate(face_phi_phi_tau(i,j),ndof_vol,ndof_vol_l,  &
                             ctx_mesh%n_neigh_per_cell,                 &
                             dim_domain,dim_domain,ctx_err)
      end do
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do i_cell=1,ctx_mesh%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle
      
      ctx_dg%hdg%Ainv(i_cell)%array = 0.d0
      ctx_dg%hdg%Q   (i_cell)%array = 0.d0
      ctx_dg%hdg%X   (i_cell)%array = 0.d0
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array = 0.d0
        ctx_dg%hdg%D(i_cell)%array = 0.d0
      endif

      coeff=0.0; hdgAinv=0.0; hdgB=0.0; hdgC=0.0; hdgL=0.0
      face_phii_xij         = 0.0
      face_phii_xij_tau     = 0.0
      sumface_phiphi_tau    = 0.0
      flag_info_sym         = .false.

      ! --------------------------------------------------------->
      !! initialization of edges informations and model parameters     
      i_order_cell = ctx_dg%order_to_index      (ctx_mesh%order(i_cell))
      ndof_vol     = ctx_dg%n_dof_per_order     (i_order_cell)
      ndof_face    = ctx_dg%n_dof_face_per_order(i_order_cell)

      !!  --------------------------------------------------------------
      if(ctx_dg%penalization <= 0) then
        !! some are known and given by an integer, otherwise it will
        !! raise an error when creating the matrices.
        tag_penalization = 23   !! Godunov.
        penalization     = 1.d0

      !! _________________________________________
      else
        penalization    = ctx_dg%penalization
        !! avoid 0.0
        if(maxval(abs(penalization)) < tiny(1.0)) penalization = 1.d0 
        tag_penalization= 0
      end if
      !! ---------------------------------------------------------------

      !! compute the matrices using quadrature rules
      call hdg_build_quadrature_int_2D(ctx_dg,ctx_mesh,model,i_cell,    &
                     i_order_cell,freq,tag_penalization,                &
                     !! volume matrices
                     vol_phi_phi    (i_order_cell)%array,               &
                     vol_phi_phi_rho(i_order_cell)%array,               &
                     vol_phi_phi_S  (i_order_cell)%array,               &
                     !! derivative needed
                     vol_dphi_phi(i_order_cell)%array,                  &
                     !! face matrices
                     face_phi_phi,                                      &
                     !! because of the jump condition
                     face_phi_phi_tau,                                  &
                     !! error context
                     ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
      ! ----------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! initialize face informations with neighbors
      !! ndf_edges_elem: for each face we store the incremental ndof
      !! multiply nface by two for the two variables.
      !! ndf_edges_elem(7) = total number of dof on the edges of the element
      !! ---------------------------------------------------------------
      ndf_edges_elem(1)=0
      do iface=1,ctx_mesh%n_neigh_per_cell
        face                    = ctx_mesh%cell_face   (iface,i_cell)
        order                   = ctx_mesh%order_face  (face)
        i_order_neigh  (iface)  = ctx_dg%order_to_index(order)
        ndof_face_neigh(iface)  = ctx_dg%n_dof_face_per_order(i_order_neigh(iface))
        ndof_vol_neigh (iface)  = ctx_dg%n_dof_per_order     (i_order_neigh(iface))
        !! VARIABLE 1 (x) 
        ndf_edges_elem (iface+1)=ndof_face_neigh(iface)
        !! VARIABLE 2 (z)
        ndf_edges_elem (iface+4)=ndof_face_neigh(iface)
      end do
      !! incremental array 
      do iface = 2,ctx_mesh%n_neigh_per_cell*dim_var
        ndf_edges_elem (iface+1) = ndf_edges_elem(iface+1)+ndf_edges_elem(iface)
      end do
      ndof_face_tot = ndf_edges_elem(7)
      ndof_face_cell= sum(ndof_face_neigh(:))
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Computation of some utility matrices for the faces: we need
      !!
      !! (o) \sum_face penalization(face) (\phi_i, \phi_j)_face 
      !!                   => sumface_phiphi_tau(i,j), needed for A
      !! (o) (\phii, \xij) over the face for each face 
      !!                   => face_phii_xij(iface,i,j), needed for C
      !! (o) matrix L, see below, it is then adjusted if boundary cond.
      !! (o) matrix B, see below
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        !! local index of the face, for the pml coefficient ------------
        index_face  = ctx_mesh%cell_face(iface,i_cell)

        do i=1, ndof_face_neigh(iface)
          index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)

          !! L is block-diagonal with ndof_face x ndof_face blocks and
          !! is defined by - \tau (\Xi, \Xi) over the face
          !! here we have 2 variables in the matrix (for Vx and Vz)
          !! -----------------------------------------------------------
          do j=1, ndof_face_neigh(iface)
            index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
            tempmat = dcmplx(face_phi_phi_tau(i_o_neigh,i_o_neigh)%         &
                          array(index_face_dofi,index_face_dofj,iface,:,:)* &
                          ctx_dg%det_jac_face(iface,i_cell))
            hdgL(ndf_edges_elem(iface)+i  ,ndf_edges_elem(iface  )+j) = &
                 penalization(iface)*tempmat(1,1)
            hdgL(ndf_edges_elem(iface)+i  ,ndf_edges_elem(iface+3)+j) = &
                 penalization(iface)*tempmat(1,2)
            hdgL(ndf_edges_elem(iface+3)+i,ndf_edges_elem(iface  )+j) = &
                 penalization(iface)*tempmat(2,1)
            hdgL(ndf_edges_elem(iface+3)+i,ndf_edges_elem(iface+3)+j) = &
                 penalization(iface)*tempmat(2,2)
          enddo

          !! B is has one block per face, each block is of size 
          !! Ndof_face x Ndof_vol and the content is given by 
          !! (\Xi, \Phi) over the face
          !! -----------------------------------------------------------
          do j=1,ndof_vol
            !! careful that i is the face and j the vol ................
            temp = dcmplx(face_phi_phi(i_order_cell,i_o_neigh)%         &
                          array(j,index_face_dofi,iface) *              &
                          ctx_dg%det_jac_face(iface,i_cell))
            face_phii_xij(iface,j,i) = temp
            
            tempmat = dcmplx(face_phi_phi_tau(i_order_cell,i_o_neigh)%  &
                             array(j,index_face_dofi,iface,:,:) *       &
                             ctx_dg%det_jac_face(iface,i_cell))
            face_phii_xij_tau(iface,j,i,:,:) = tempmat
            
            hdgB(ndf_edges_elem(iface  )+i,           j) =-penalization(iface)*tempmat(1,1)
            hdgB(ndf_edges_elem(iface  )+i,  ndof_vol+j) =-penalization(iface)*tempmat(1,2)
            hdgB(ndf_edges_elem(iface  )+i,2*ndof_vol+j) = temp*ctx_dg%normal(1,iface,i_cell)
            hdgB(ndf_edges_elem(iface  )+i,4*ndof_vol+j) = temp*ctx_dg%normal(2,iface,i_cell)

            hdgB(ndf_edges_elem(iface+3)+i,           j) =-penalization(iface)*tempmat(2,1)
            hdgB(ndf_edges_elem(iface+3)+i,  ndof_vol+j) =-penalization(iface)*tempmat(2,2)
            hdgB(ndf_edges_elem(iface+3)+i,3*ndof_vol+j) = temp*ctx_dg%normal(2,iface,i_cell)
            hdgB(ndf_edges_elem(iface+3)+i,4*ndof_vol+j) = temp*ctx_dg%normal(1,iface,i_cell)
          end do
        end do
        
        !! -------------------------------------------------------------
        !! Here we compute the term 
        !! \sum_face penalization(face) * (\phi_i, \phi_j)_face
        !! -------------------------------------------------------------
        do i = 1, ndof_face
          index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
          do j = 1, ndof_face
            index_face_dofj = ctx_dg%face_map(i_order_cell)%array(j,iface)
            tempmat = dcmplx(ctx_dg%det_jac_face(iface,i_cell) *        &
                      face_phi_phi_tau(i_order_cell,i_order_cell)%      &
                      array(index_face_dofi,index_face_dofj,iface,:,:))
            sumface_phiphi_tau(index_face_dofi,index_face_dofj,:,:) =   &
            sumface_phiphi_tau(index_face_dofi,index_face_dofj,:,:)     &
                 !! only for constant scaling, otherwise everything
                 !! is already in the face_phi_phi_tau used.
            + tempmat * penalization(iface)

          end do
        end do

      end do

      ! ----------------------------------------------------------------
      !! adjust B in case we have an actual boundary
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        neigh     = ctx_mesh%cell_neigh(iface,i_cell)
        
        select case (neigh)
          !! absorbing boundary conditions
          !! ----------------------------------------------------
          !!    \sigma n - \rho cp (v n) n - \rho cs (v t) t = 0
          !! ----------------------------------------------------
          case(tag_absorbing)

            !! ---------------------------------------------------------
            select case(trim(adjustl(ctx_mesh%Zimpedance)))

              case('Sommerfeld_0')
              
                !! compute locally needed matrices
                BCnormal = 0.0
                BCnormalT= 0.0
                BCparam  = 0.0
                BCmatrix = 0.0
                !! compute BC matrix with normal directions
                BCnormal(1,1) = ctx_dg%normal(1,iface,i_cell)
                BCnormal(1,2) =-ctx_dg%normal(2,iface,i_cell)
                BCnormal(2,1) = ctx_dg%normal(2,iface,i_cell)
                BCnormal(2,2) = ctx_dg%normal(1,iface,i_cell)
                !! compute BC transposed
                BCnormalT = transpose(BCnormal)
                
                !! compute the parameters vp vs and rho
                call hdg_model_on_midface_2D(ctx_dg,ctx_mesh,model,     &
                                             i_cell,iface,freq,rho,vp,  &
                                             vs,ctx_err)
                !! compute param BC matrix
                BCparam(1,1) = rho * vp*(-freq)
                BCparam(2,2) = rho * vs*(-freq)
                !! the BC matrix is BCnormal BCparam BCnormalT
                BCmatrix = matmul(BCnormal,BCparam  )
                BCmatrix = matmul(BCmatrix,BCnormalT)
                do i=1, ndof_face
                  index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
                  do j=1,ndof_face
                    index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
              
                    temp = dcmplx(face_phi_phi(i_order_cell,i_order_cell)%     &
                                  array(index_face_dofi,index_face_dofj,iface)*&
                                  ctx_dg%det_jac_face(iface,i_cell))

                   !! The two Lagrange multipliers (x) and (z):
                   hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j) =   &
                   hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)     &
                     + BCmatrix(1,1)*temp
                   hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface+3)+j) = &
                   hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface+3)+j)   &
                     + BCmatrix(1,2)*temp
                   hdgL(ndf_edges_elem(iface+3)+i,ndf_edges_elem(iface)+j) = &
                   hdgL(ndf_edges_elem(iface+3)+i,ndf_edges_elem(iface)+j)   &
                     + BCmatrix(2,1)*temp
                   hdgL(ndf_edges_elem(iface+3)+i,ndf_edges_elem(iface+3)+j)=&
                   hdgL(ndf_edges_elem(iface+3)+i,ndf_edges_elem(iface+3)+j) &
                     + BCmatrix(2,2)*temp

                  end do
                end do
              
              !! unknown -----------------------------------------------
              case default
                ctx_err%msg   = "** ERROR: Unrecognized Impedance "  // &
                                "condition "//                          &
                                trim(adjustl(ctx_mesh%Zimpedance))//    &
                                " for Robin Boundary Condition [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
            end select
            !! ---------------------------------------------------------

          !! other types
          case(tag_freesurface,tag_ghost_cell,tag_wallsurface,          &
               tag_neumann,tag_dirichlet,                               &
               tag_planewave_isoP,tag_planewave_isoS)

            !! TAG_GHOST ==> nothing special

            !! TAG_NEUMANN = TAG_FREESURFACE = TAG_PLLANEWAVE 
            !!               !! -------------------------- <!
                             !! it will be done at the end <!
                             !! -------------------------- <!
            !! TAG_WALLSURFACE = TAG_DIRICHLET
            !! ==> \partial_\nu Pressure = 0, i.e., displacement = 0
            !!               nothing because Neuman BC is automatic

          !! unsupported BC ............................................
          case(tag_pml_circular,tag_pml,tag_center_neumann,             &
               tag_aldirichlet_dlrobin,tag_deltapdirichlet_dldirichlet, &            
               tag_analytic_orthorhombic,tag_analytic_elasticiso,       &
               tag_deltapdirichlet,tag_alrbc_dlrobin,tag_planewave,     &
               tag_alrbc_dldirichlet)
            ctx_err%msg   = "** ERROR: Unsupported " //                 &
                            "boundary condition [create_matrix]"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle
            
          case default

        end select
      
      end do
      if(ctx_err%ierr .ne. 0) cycle !! non-shared error

      ! ----------------------------------------------------------------
      ! construct HDG matrices A and C 
      ! ----------------------------------------------------------------
      n = ndof_vol*dim_sol
      !! construction Ainv
      call hdg_build_Ainv_2D_tri(hdgAinv,                               &
                                 vol_phi_phi_rho(i_order_cell)%array,   &
                                 vol_phi_phi_S  (i_order_cell)%array,   &
                                 vol_dphi_phi   (i_order_cell)%array,   &
                                 sumface_phiphi_tau,                    &
                                 ctx_dg%inv_jac(:,:,i_cell),            &
                                 ctx_dg%det_jac(i_cell),ndof_vol,freq,  &
                                 ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
      !! construction C
      call hdg_build_C_2D_tri(hdgC,face_phii_xij,face_phii_xij_tau,     &
                              penalization,ndof_vol,ndof_face_neigh,    &
                              ndf_edges_elem,ctx_dg%normal(:,:,i_cell))

      !! update saved matrices
      ctx_dg%hdg%Ainv(i_cell)%array(1:n,1:n) = cmplx(hdgAinv(1:n,1:n),kind=RKIND_MAT)
      !! Q =-A^-1 C
      ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot) = cmplx(-matmul(  &
                   hdgAinv(1:n,1:n),hdgC(1:n,1:ndof_face_tot)),kind=RKIND_MAT)
      !! X =-B A^-1
      ctx_dg%hdg%X(i_cell)%array(1:ndof_face_tot,1:n) = cmplx(-matmul(           &
                  hdgB(1:ndof_face_tot,1:n),hdgAinv(1:n,1:n)),kind=RKIND_MAT)
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array(1:ndof_face_tot,1:n)=cmplx(          &
                     matmul(transpose(conjg(hdgC(1:n,1:ndof_face_tot))),&
                            transpose(conjg(hdgAinv(1:n,1:n)))), kind=RKIND_MAT)
        ctx_dg%hdg%D(i_cell)%array(1:n,1:ndof_face_tot)=cmplx(          &
            matmul(transpose(conjg(hdgAinv(1:n,1:n))),                  &
                   transpose(conjg(hdgB(1:ndof_face_tot,1:n)))), kind=RKIND_MAT)
      endif

      !! local matrix coeff = L - B*A^-1*C
      !!                    = L + B*Q
      coeff(1:ndof_face_tot,1:ndof_face_tot) =                &
                      hdgL(1:ndof_face_tot,1:ndof_face_tot) + &
                           matmul(hdgB(1:ndof_face_tot,1:n),  &
                           dcmplx(ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot)))

      !! ------------------------------------------
      !! readjust the wall surface to identity
      !! DIRICHLET condition
      !! ------------------------------------------
      do iface=1, ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        if(neigh .eq. tag_wallsurface    .or. &
           neigh .eq. tag_dirichlet      .or. &
           neigh .eq. tag_planewave_isoS .or. &
           neigh .eq. tag_planewave_isoP) then

          !! -------------------------------------------------------------- %
          !! The local block will be replaced by Identity to maintain the 
          !! symmetry of the matrix. However, using the weak form, we do
          !! not readily have the identity but 
          !!   \integral \Lambda \xi_j  = \integral g \xi_j   \forall j
          !! where g is my source function. That is, we have in matrix form:
          !!             hdgL \Lambda = S.
          !! 
          !! Here we save the matrix hdgL, and we will solve the local
          !! problem when we create the source. Hence we will readily
          !! have the values of Lambda, and we can use and identity block
          !! that allows us to maintain the symmetry of the matrix.
          !!
          !!  e.g., original local system would be 
          !!  (x x x)                  (x x x)                      (x x  0)
          !!  (x x x)\Lambda , and now (x x x)\Lambda, and we need  (x x  0) \Lambda
          !!  (x x x)                  (0 0 L)                      (0 0 Id)
          !!no dirichlet           dirichlet face 3               for symmetry
          !! -------------------------------------------------------------- %


          !! create a Identity matrix coefficients to impose 0 velocity
          !! All of the velocities must be zero, we cannot force 
          !! the normal velocity only because it would be ill-posed !
          !! Variable 1
          coeff(  ndf_edges_elem(iface)+1: &
                  ndf_edges_elem(iface)+ndof_face,:)=0.0
          !! Variable 2
          coeff(  ndf_edges_elem(iface+3)+1: &
                  ndf_edges_elem(iface+3)+ndof_face,:)=0.0

          !! -----------------------------------------------------------
          !! save lines to symmetrize the matrix.
          flag_info_sym(iface) = .true. 
          !! we can use ndof_face above because the face on the boundary
          !! anyway has the order of the cell, but it should be 
          !! ndof_face_neigh(iface)
          ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) =                 &
            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) +               &
            !! only the other faces times the number of variables
            dim_var*(ndof_face_tot - ndof_face_neigh(iface))*ndof_face_neigh(iface)
          !! -----------------------------------------------------------

          !! diagonal
          do i=1, ndof_face
            coeff(ndf_edges_elem(iface)  +i,ndf_edges_elem(iface)+i)  = 1.0
            coeff(ndf_edges_elem(iface+3)+i,ndf_edges_elem(iface+3)+i)= 1.0
          end do

          if(neigh .eq. tag_planewave_isoP .or.                         &
             neigh .eq. tag_planewave_isoS) then
            !! ---------------------------------------------------------
            !! we save the hdgL block for the weak form on the boundary.
            !! we also save the velocity on the face in case we have a 
            !! planewave. In elastic iso 2D there are two wavespeeds
            !! ---------------------------------------------------------
            face = ctx_mesh%cell_face(iface,i_cell)       
            !! compute the parameters vp vs and rho
            call hdg_model_on_midface_2D(ctx_dg,ctx_mesh,model,         &
                                         i_cell,iface,freq,rho,vp,      &
                                         vs,ctx_err)

            call array_allocate(ctx_dg%face_velocity(face),2,ctx_err)
            !! remark that we could save for all dof on the face ...
            ctx_dg%face_velocity(face)%array(1) = vp
            ctx_dg%face_velocity(face)%array(2) = vs

            do i=1,dim_var
              call array_allocate(ctx_dg%ctx_symmetrize_mat%f_matbc(face,i),&
                                  ndof_face_neigh(iface),                   &
                                  ndof_face_neigh(iface),ctx_err)
              ctx_dg%ctx_symmetrize_mat%f_matbc(face,i)%array = 0.d0
            end do
            !! because the matrix is symmetric without planewave.
            ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize = .true.
            i_o_neigh = i_order_neigh(iface)
            !! this is the block hdgL without the penalization: \int \xi_i \xi_j 
            do i=1, ndof_face_neigh(iface)
              index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)
              do j=1, ndof_face_neigh(iface)
                index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
                temp = dcmplx(face_phi_phi(i_o_neigh,i_o_neigh)%     &
                       array(index_face_dofi,index_face_dofj,iface)* &
                       ctx_dg%det_jac_face(iface,i_cell))
             
                 !! two variables here.
                 ctx_dg%ctx_symmetrize_mat%f_matbc(face,1)%array(i,j) = temp
                 ctx_dg%ctx_symmetrize_mat%f_matbc(face,2)%array(i,j) = temp
              end do
            end do

          end if !! end if planewave

        end if !! end if planewave or dirichlet or free-surface

      end do !! end loop over faces.
      
      !! allocate arrays if non-zeros
      if(ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) .ne. 0) then
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_line(i_cell),   &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_col(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_val(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
         ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array = 0.d0
      end if 
      ind_sym = 0
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Update global coefficients
      !! we first need to find the appropriate index for the face first
      !!
      !! loop over the face of the variable 1 == [ROW]
      do iface=1,ctx_mesh%n_neigh_per_cell ; do ivar=1,dim_var
        if(ctx_err%ierr .ne. 0) cycle
        
        nval_i = ndf_edges_elem((ivar-1)*3+iface+1) -                   &
                 ndf_edges_elem((ivar-1)*3+iface  )
        do ipos=1,nval_i
          if(ctx_err%ierr .ne. 0) cycle
          
          !! row info --------------------------------------------------
          i        = ndf_edges_elem(iface)+ ipos + (ivar-1)*ndof_face_cell
          iline    = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(iface,i_cell))
          i_o_neigh= i_order_neigh(iface)
          i_orient = ctx_mesh%cell_face_orientation(iface,i_cell)
          iline    = iline + (ivar-1)*ctx_dg%n_dofgb_pervar +           &
                     ctx_dg%hdg%idof_face(i_orient,i_o_neigh)%array(ipos)
          !! col info --------------------------------------------------
          !! loop over the face of the variable 2  == [COLUMN]
          do jface=1,ctx_mesh%n_neigh_per_cell ; do jvar=1,dim_var
            if(ctx_err%ierr .ne. 0) cycle

            nval_j = ndf_edges_elem((jvar-1)*3+jface+1) -               &
                     ndf_edges_elem((jvar-1)*3+jface  )

            do jpos=1,nval_j
              if(ctx_err%ierr .ne. 0) cycle

              j        = ndf_edges_elem(jface)+jpos + (jvar-1)*ndof_face_cell
              icol     = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(jface,i_cell))
              i_o_neigh= i_order_neigh(jface)
              i_orient = ctx_mesh%cell_face_orientation(jface,i_cell)
              icol     = icol + (jvar-1)*ctx_dg%n_dofgb_pervar +        &
                         ctx_dg%hdg%idof_face (i_orient,i_o_neigh)%array(jpos)

              !! save Dirichlet BC infos to make the matrix symmetric    
              !! by modifying the rhs.
              if( flag_info_sym(jface) .and. (.not.flag_info_sym(iface))) then
                ind_sym = ind_sym + 1
                ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array(ind_sym) = &
                  int(iline,kind=4)
                ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array(ind_sym) = &
                  int(icol, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array(ind_sym) = &
                                       cmplx(coeff(i,j),kind=RKIND_POL)
                !! this coeff can be set to zero now.
                coeff(i,j) = 0.d0
              end if

              !! update symmetric
              if(abs(coeff(i,j))>tol) then
                !! The BC do not remove the symmetry of the matrix
                if(icol >= iline) then
                  !$OMP CRITICAL
                  nnz_exact       = nnz_exact + 1
                  Alin(nnz_exact) = iline
                  Acol(nnz_exact) = icol
                  Aval(nnz_exact) = cmplx(coeff(i,j),kind=RKIND_MAT)
                  !$OMP END CRITICAL

                  !! check too high values 
                  if(real (Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.aimag(Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.ieee_is_nan(real (Aval(nnz_exact))) .or.                &
                     ieee_is_nan(aimag(Aval(nnz_exact)))) then
                    write(ctx_err%msg,'(a,i0,a,es9.2,a)') "** ERROR: "//  &
                          "the matrix has entry infinity or NaN."//       &
                          " Current matrix precision kind is ", RKIND_MAT,&
                          " (8 at most), which amounts to a"     //       &
                          " maximal value of ",                           &
                          huge(real(1.0,kind=RKIND_MAT)), " [create_matrix]"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    cycle
                  end if

                end if
              end if

            end do ; end do
          end do
        end do ; end do
      end do
      ! ----------------------------------------------------------------

   end do
   !$OMP END DO
   dim_nnz_loc = nnz_exact
   deallocate(coeff            )
   deallocate(hdgL             )
   deallocate(face_phii_xij    )
   deallocate(face_phii_xij_tau)
   deallocate(sumface_phiphi_tau)
   deallocate(hdgC )
   deallocate(hdgB )
   deallocate(hdgAinv)
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi    (i))
      call array_deallocate(vol_phi_phi_rho(i))
      call array_deallocate(vol_phi_phi_S  (i))
      call array_deallocate(vol_dphi_phi   (i))
      do j = 1, ctx_dg%n_different_order
        call array_deallocate(face_phi_phi(i,j))
        call array_deallocate(face_phi_phi_tau(i,j))
      end do
    end do
    deallocate(vol_phi_phi     )
    deallocate(vol_phi_phi_rho )
    deallocate(vol_phi_phi_S   )
    deallocate(vol_dphi_phi    )
    deallocate(face_phi_phi    )
    deallocate(face_phi_phi_tau)
   !$OMP END PARALLEL

    return
  end subroutine create_matrix_quadrature_2D

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from HELMHOLTZ ELASTIC using HDG in 3D
  !
  !> @param[in]    ctx_mesh       : cartesian grid type
  !> @param[in]    model          : model parameters
  !> @param[inout] ctx_dg         : type DG
  !> @param[out]   dim_nnz_loc    : local nonzeros
  !> @param[inout] ilin(:)        : matrix I
  !> @param[inout] icol(:)        : matrix J
  !> @param[inout] Aval(:)        : matrix values
  !> @param[in]    frequency      : frequency
  !> @param[in]    flag_inv       : indicates inversion
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_quadrature_3D(ctx_mesh,model,ctx_dg,dim_nnz_loc,&
                                      Alin,Acol,Aval,freq,flag_inv,ctx_err)
    implicit none
    type(t_domain)                     ,intent(in)      :: ctx_mesh
    type(t_model)                      ,intent(in)      :: model
    type(t_discretization)             ,intent(inout)   :: ctx_dg
    integer(kind=8)                    ,intent(out)     :: dim_nnz_loc
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Alin(:)
    integer(kind=IKIND_MAT),allocatable,intent(inout)   :: Acol(:)
    complex(kind=RKIND_MAT),allocatable,intent(inout)   :: Aval(:)
    complex(kind=8)                    ,intent(in)      :: freq
    logical                            ,intent(in)      :: flag_inv
    type(t_error)                      ,intent(inout)   :: ctx_err
    !! local
    integer(kind=IKIND_MAT)             :: nnz_exact
    real   (kind=8)                     :: tol
    !! hdg
    complex(kind=8), allocatable :: coeff(:,:)
    complex(kind=8), allocatable :: hdgB(:,:)
    complex(kind=8), allocatable :: hdgC(:,:)
    complex(kind=8), allocatable :: hdgAinv(:,:)
    complex(kind=8), allocatable :: hdgL(:,:)
    complex(kind=8), allocatable :: sumface_phiphi_tau(:,:,:,:)
    complex(kind=8), allocatable :: face_phii_xij     (:,:,:)    
    complex(kind=8), allocatable :: face_phii_xij_tau (:,:,:,:,:)    
    complex(kind=8)              :: vp,vs,temp,tempmat(3,3)
    complex(kind=8)              :: BCnormal(3,3),BCparam  (3,3)
    complex(kind=8)              :: BCmatrix(3,3),BCnormalT(3,3)
    complex(kind=8)              :: penalization(4) !! 3D tetra, 4 faces
    real   (kind=8)              :: rho
    integer                      :: tag_penalization
    !! evaluation of the integrals
    type(t_array2d_real)   ,allocatable :: vol_phi_phi    (:)
    type(t_array2d_real)   ,allocatable :: vol_phi_phi_rho(:)
    type(t_array4d_complex),allocatable :: vol_phi_phi_S  (:)
    type(t_array3d_real)   ,allocatable :: vol_dphi_phi   (:)
    type(t_array3d_real)   ,allocatable :: face_phi_phi    (:,:)
    type(t_array5d_complex),allocatable :: face_phi_phi_tau(:,:)

    integer(kind=IKIND_MESH) :: i_cell,face,neigh,index_face
    integer(kind=IKIND_MAT)  :: iline, icol
    integer        :: order,i_orient,dim_var,nval_i,n,ndof_vol_l
    integer        :: i_order_cell,ndof_vol,ndof_max_vol,dim_domain,i,j
    integer        :: ndof_max_face,dim_sol,ndof_face_tot,ndof_face
    integer        :: index_face_dofi,index_face_dofj,nval_j,ipos,jpos
    integer        :: i_o_neigh,iface,jface,ivar,jvar,ndof_face_cell
    integer        :: ndf_edges_elem(13) !! 3D tetra, 4 faces x 3 fields + 1 to sum
    integer        :: i_order_neigh  (4) !! 3D tetra, 4 faces
    integer        :: ndof_face_neigh(4) !! 3D tetra, 4 faces
    integer        :: ndof_vol_neigh (4) !! 3D tetra, 4 faces
    !! for symmetrization of the matrix even with Dirichlet boundary.
    integer        :: ind_sym
    logical        :: flag_info_sym(4)   !! 3D tetra, 4 faces
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! init informations on the matrix and hdg infos
    !! -----------------------------------------------------------------
    Alin = 0  ; Acol=0    ; Aval= 0.0

    nnz_exact = 0
    if(RKIND_MAT .eq. 8) then
      tol=tiny(1.d0)  ! could be 1.d-308
    else
      tol=tiny(1.0)   ! could be 1.e-38
    endif
    !! compute max order dof
    i_order_cell  = ctx_dg%order_to_index(ctx_dg%order_max_gb)
    ndof_max_vol  = ctx_dg%n_dof_per_order     (i_order_cell)
    ndof_max_face = ctx_dg%n_dof_face_per_order(i_order_cell)
    dim_sol       = ctx_dg%dim_sol
    dim_var       = ctx_dg%dim_var
    dim_domain    = ctx_dg%dim_domain
    !! make sure it is zero at start as it is updated.
    ctx_dg%ctx_symmetrize_mat%cell_nval = 0

    !! ----------------------------------------------------------------- 
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(coeff,hdgB,hdgC,hdgAinv),      &
    !$OMP&         PRIVATE(hdgL,sumface_phiphi_tau,face_phii_xij),        &
    !$OMP&         PRIVATE(face_phii_xij_tau,vp,vs,temp,tempmat),         &
    !$OMP&         PRIVATE(BCnormal,BCparam,BCmatrix,BCnormalT),          &
    !$OMP&         PRIVATE(penalization,rho,tag_penalization),            &
    !$OMP&         PRIVATE(vol_phi_phi,vol_phi_phi_rho,vol_phi_phi_S),    &
    !$OMP&         PRIVATE(vol_dphi_phi,face_phi_phi,face_phi_phi_tau),   &
    !$OMP&         PRIVATE(i_cell,face,neigh,index_face,iline,icol),      &
    !$OMP&         PRIVATE(order,i_orient,nval_i,n,ndof_vol_l),           &
    !$OMP&         PRIVATE(i_order_cell,ndof_vol,i,j,ndof_face_tot),      &
    !$OMP&         PRIVATE(ndof_face,index_face_dofi,index_face_dofj),    &
    !$OMP&         PRIVATE(nval_j,ipos,jpos,i_o_neigh,iface,jface,ivar),  &
    !$OMP&         PRIVATE(jvar,ndof_face_cell,ndf_edges_elem),           &
    !$OMP&         PRIVATE(i_order_neigh,ndof_face_neigh,ndof_vol_neigh), &
    !$OMP&         PRIVATE(ind_sym,flag_info_sym)
    
    !! local array, the 4 is because of the 4 tetrahedra faces
    allocate(coeff  (4*dim_var*ndof_max_face,4*dim_var*ndof_max_face))
    allocate(hdgL   (4*dim_var*ndof_max_face,4*dim_var*ndof_max_face))
    allocate(hdgB   (4*dim_var*ndof_max_face,  dim_sol*ndof_max_vol ))
    allocate(hdgC   (  dim_sol*ndof_max_vol ,4*dim_var*ndof_max_face))
    allocate(hdgAinv(  dim_sol*ndof_max_vol ,  dim_sol*ndof_max_vol ))
    !! for integrations of basis functions
    allocate(face_phii_xij    (4,ndof_max_vol,ndof_max_face))
    allocate(face_phii_xij_tau(4,ndof_max_vol,ndof_max_face,3,3))
    allocate(sumface_phiphi_tau( ndof_max_vol,ndof_max_vol ,3,3))
    !! for quadrature, once and for all
    allocate(vol_phi_phi     (ctx_dg%n_different_order))
    allocate(vol_phi_phi_rho (ctx_dg%n_different_order))
    allocate(vol_phi_phi_S   (ctx_dg%n_different_order))
    allocate(vol_dphi_phi    (ctx_dg%n_different_order))
    allocate(face_phi_phi    (ctx_dg%n_different_order,ctx_dg%n_different_order))
    allocate(face_phi_phi_tau(ctx_dg%n_different_order,ctx_dg%n_different_order))

    do i = 1, ctx_dg%n_different_order
      ndof_vol = ctx_dg%n_dof_per_order (i)
      !! size depends if derivative or not
      call array_allocate(vol_phi_phi    (i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi_rho(i),ndof_vol,ndof_vol,ctx_err)
      call array_allocate(vol_phi_phi_S  (i),ndof_vol,ndof_vol,6,6,ctx_err)
      call array_allocate(vol_dphi_phi(i),ndof_vol,ndof_vol,            &
                          dim_domain,ctx_err)

      do j = 1, ctx_dg%n_different_order
        ndof_vol_l = ctx_dg%n_dof_per_order(j)
        call array_allocate(face_phi_phi(i,j),ndof_vol,ndof_vol_l,      &
                             ctx_mesh%n_neigh_per_cell,ctx_err)
        call array_allocate(face_phi_phi_tau(i,j),ndof_vol,ndof_vol_l,  &
                             ctx_mesh%n_neigh_per_cell,                 &
                             dim_domain,dim_domain,ctx_err)
      end do
    end do

    !! -----------------------------------------------------------------
    !$OMP DO
    do i_cell=1,ctx_mesh%n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      
      ctx_dg%hdg%Ainv(i_cell)%array = 0.d0
      ctx_dg%hdg%Q   (i_cell)%array = 0.d0
      ctx_dg%hdg%X   (i_cell)%array = 0.d0
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array = 0.d0
        ctx_dg%hdg%D(i_cell)%array = 0.d0
      endif

      coeff=0.0; hdgAinv=0.0; hdgB=0.0; hdgC=0.0; hdgL=0.0
      face_phii_xij         = 0.0
      face_phii_xij_tau     = 0.0
      sumface_phiphi_tau    = 0.0
      flag_info_sym         = .false.

      ! --------------------------------------------------------->
      !! initialization of edges informations and model parameters     
      i_order_cell = ctx_dg%order_to_index      (ctx_mesh%order(i_cell))
      ndof_vol     = ctx_dg%n_dof_per_order     (i_order_cell)
      ndof_face    = ctx_dg%n_dof_face_per_order(i_order_cell)

      !!  --------------------------------------------------------------
      if(ctx_dg%penalization <= 0) then
        !! some are known and given by an integer, otherwise it will
        !! raise an error when creating the matrices.
        tag_penalization = 23   !! Godunov
        penalization    =  1.d0

      !! _________________________________________
      else
        penalization    = ctx_dg%penalization
        !! avoid 0.0
        if(maxval(abs(penalization)) < tiny(1.0)) penalization = 1.d0 
        tag_penalization= 0
      end if
      !! ---------------------------------------------------------------

      !! compute the matrices using quadrature rules
      call hdg_build_quadrature_int_3D(ctx_dg,ctx_mesh,model,i_cell,    &
                     i_order_cell,freq,tag_penalization,                &
                     !! volume matrices
                     vol_phi_phi    (i_order_cell)%array,               &
                     vol_phi_phi_rho(i_order_cell)%array,               &
                     vol_phi_phi_S  (i_order_cell)%array,               &
                     !! derivative needed
                     vol_dphi_phi(i_order_cell)%array,                  &
                     !! face matrices
                     face_phi_phi,                                      &
                     !! because of the jump condition
                     face_phi_phi_tau,                                  &
                     !! error context
                     ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
      ! ----------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! initialize face informations with neighbors
      !! ndf_edges_elem: for each face we store the incremental ndof
      !! multiply nface by three for the three variables.
      !! ndf_edges_elem(13) = total number of dof on the edges of the element
      !! ---------------------------------------------------------------
      ndf_edges_elem(1)=0
      do iface=1,ctx_mesh%n_neigh_per_cell
        face                    = ctx_mesh%cell_face   (iface,i_cell)
        order                   = ctx_mesh%order_face  (face)
        i_order_neigh  (iface)  = ctx_dg%order_to_index(order)
        ndof_face_neigh(iface)  = ctx_dg%n_dof_face_per_order(i_order_neigh(iface))
        ndof_vol_neigh (iface)  = ctx_dg%n_dof_per_order     (i_order_neigh(iface))
        !! VARIABLE 1 (x) 
        ndf_edges_elem (iface+1)=ndof_face_neigh(iface)
        !! VARIABLE 2 (z)
        ndf_edges_elem (iface+5)=ndof_face_neigh(iface)
        !! VARIABLE 3 (z)
        ndf_edges_elem (iface+9)=ndof_face_neigh(iface)
      end do
      !! incremental array 
      do iface = 2,ctx_mesh%n_neigh_per_cell*dim_var
        ndf_edges_elem (iface+1) = ndf_edges_elem(iface+1)+ndf_edges_elem(iface)
      end do
      ndof_face_tot = ndf_edges_elem(13)
      ndof_face_cell= sum(ndof_face_neigh(:))
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Computation of some utility matrices for the faces: we need
      !!
      !! (o) \sum_face penalization(face) (\phi_i, \phi_j)_face 
      !!                   => sumface_phiphi_tau(i,j), needed for A
      !! (o) (\phii, \xij) over the face for each face 
      !!                   => face_phii_xij(iface,i,j), needed for C
      !! (o) matrix L, see below, it is then adjusted if boundary cond.
      !! (o) matrix B, see below
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        !! local index of the face, for the pml coefficient ------------
        index_face  = ctx_mesh%cell_face(iface,i_cell)

        do i=1, ndof_face_neigh(iface)
          index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)

          !! L is block-diagonal with ndof_face x ndof_face blocks and
          !! is defined by - \tau (\Xi, \Xi) over the face
          !! here we have 3 variables in the matrix (for the
          !! numerical traces of Vx, Vy and Vz)
          !! -----------------------------------------------------------
          do j=1, ndof_face_neigh(iface)
            index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
            tempmat = dcmplx(face_phi_phi_tau(i_o_neigh,i_o_neigh)%         &
                          array(index_face_dofi,index_face_dofj,iface,:,:)* &
                          ctx_dg%det_jac_face(iface,i_cell))

            !! there are three equations as we consider 
            !! \sigma.n + n C N \tau (u - \lambda) (x) n 

            !! Equation (1): lambda_x, lambda_y, lambda_z
            hdgL(ndf_edges_elem(iface)+i  ,ndf_edges_elem(iface  )+j) = &
                                         penalization(iface)*tempmat(1,1)
            hdgL(ndf_edges_elem(iface)+i  ,ndf_edges_elem(iface+4)+j) = &
                                         penalization(iface)*tempmat(1,2)
            hdgL(ndf_edges_elem(iface)+i  ,ndf_edges_elem(iface+8)+j) = &
                                         penalization(iface)*tempmat(1,3)
            !! Equation (2): lambda_x, lambda_y, lambda_z
            hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface  )+j) = &
                                         penalization(iface)*tempmat(2,1)
            hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface+4)+j) = &
                                         penalization(iface)*tempmat(2,2)
            hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface+8)+j) = &
                                         penalization(iface)*tempmat(2,3)
            !! Equation (3): lambda_x, lambda_y, lambda_z
            hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface  )+j) = &
                                         penalization(iface)*tempmat(3,1)
            hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface+4)+j) = &
                                         penalization(iface)*tempmat(3,2)
            hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface+8)+j) = &
                                         penalization(iface)*tempmat(3,3)
          enddo

          !! B is has one block per face, each block is of size 
          !! Ndof_face x Ndof_vol and the content is given by 
          !! (\Xi, \Phi) over the face
          !! -----------------------------------------------------------
          do j=1,ndof_vol
            !! careful that i is the face and j the vol ................
            temp = dcmplx(face_phi_phi(i_order_cell,i_o_neigh)%         &
                          array(j,index_face_dofi,iface) *              &
                          ctx_dg%det_jac_face(iface,i_cell))
            face_phii_xij(iface,j,i) = temp
            
            tempmat = dcmplx(face_phi_phi_tau(i_order_cell,i_o_neigh)%  &
                             array(j,index_face_dofi,iface,:,:) *       &
                             ctx_dg%det_jac_face(iface,i_cell))
            face_phii_xij_tau(iface,j,i,:,:) = tempmat

            !! there are three equations as we consider 
            !! \sigma.n + \tau (u - \lambda) (x) n 
            !! Equation (1): ux, uy, uz, sxx, syy, szz, sxy, sxz, syz
            hdgB(ndf_edges_elem(iface  )+i,           j) =-penalization(iface)*tempmat(1,1)
            hdgB(ndf_edges_elem(iface  )+i,  ndof_vol+j) =-penalization(iface)*tempmat(1,2)
            hdgB(ndf_edges_elem(iface  )+i,2*ndof_vol+j) =-penalization(iface)*tempmat(1,3)
            hdgB(ndf_edges_elem(iface  )+i,3*ndof_vol+j) = temp*ctx_dg%normal(1,iface,i_cell) ! sxx
            hdgB(ndf_edges_elem(iface  )+i,6*ndof_vol+j) = temp*ctx_dg%normal(2,iface,i_cell) ! sxy
            hdgB(ndf_edges_elem(iface  )+i,7*ndof_vol+j) = temp*ctx_dg%normal(3,iface,i_cell) ! sxz
            !! Equation (2): ux, uy, uz, sxx, syy, szz, sxy, sxz, syz
            hdgB(ndf_edges_elem(iface+4)+i,           j) =-penalization(iface)*tempmat(2,1)
            hdgB(ndf_edges_elem(iface+4)+i,  ndof_vol+j) =-penalization(iface)*tempmat(2,2)
            hdgB(ndf_edges_elem(iface+4)+i,2*ndof_vol+j) =-penalization(iface)*tempmat(2,3)
            hdgB(ndf_edges_elem(iface+4)+i,4*ndof_vol+j) = temp*ctx_dg%normal(2,iface,i_cell) ! syy
            hdgB(ndf_edges_elem(iface+4)+i,6*ndof_vol+j) = temp*ctx_dg%normal(1,iface,i_cell) ! sxy
            hdgB(ndf_edges_elem(iface+4)+i,8*ndof_vol+j) = temp*ctx_dg%normal(3,iface,i_cell) ! syz
            !! Equation (3): ux, uy, uz, sxx, syy, szz, sxy, sxz, syz
            hdgB(ndf_edges_elem(iface+8)+i,           j) =-penalization(iface)*tempmat(3,1)
            hdgB(ndf_edges_elem(iface+8)+i,  ndof_vol+j) =-penalization(iface)*tempmat(3,2)
            hdgB(ndf_edges_elem(iface+8)+i,2*ndof_vol+j) =-penalization(iface)*tempmat(3,3)
            hdgB(ndf_edges_elem(iface+8)+i,5*ndof_vol+j) = temp*ctx_dg%normal(3,iface,i_cell) ! szz
            hdgB(ndf_edges_elem(iface+8)+i,7*ndof_vol+j) = temp*ctx_dg%normal(1,iface,i_cell) ! sxz
            hdgB(ndf_edges_elem(iface+8)+i,8*ndof_vol+j) = temp*ctx_dg%normal(2,iface,i_cell) ! syz

          end do
        end do
        
        !! -------------------------------------------------------------
        !! Here we compute the term 
        !! \sum_face penalization(face) * (\phi_i, \phi_j)_face
        !! -------------------------------------------------------------
        do i = 1, ndof_face
          index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
          do j = 1, ndof_face
            index_face_dofj = ctx_dg%face_map(i_order_cell)%array(j,iface)
            tempmat = dcmplx(ctx_dg%det_jac_face(iface,i_cell) *        &
                      face_phi_phi_tau(i_order_cell,i_order_cell)%      &
                      array(index_face_dofi,index_face_dofj,iface,:,:))
            sumface_phiphi_tau(index_face_dofi,index_face_dofj,:,:) =   &
            sumface_phiphi_tau(index_face_dofi,index_face_dofj,:,:)     &
                 !! only for constant scaling, otherwise everything
                 !! is already in the face_phi_phi_tau used.
            + tempmat * penalization(iface)

          end do
        end do

      end do

      ! ----------------------------------------------------------------
      !! adjust B in case we have an actual boundary
      ! ----------------------------------------------------------------
      do iface=1,ctx_mesh%n_neigh_per_cell
        i_o_neigh = i_order_neigh(iface)
        neigh     = ctx_mesh%cell_neigh(iface,i_cell)
        
        select case (neigh)
          !! absorbing boundary conditions
          !! ----------------------------------------------------
          !!    \sigma n - \rho cp (v n) n - \rho cs (v t) t = 0
          !! ----------------------------------------------------
          case(tag_absorbing)

            !! ---------------------------------------------------------
            select case(trim(adjustl(ctx_mesh%Zimpedance)))

              case('Sommerfeld_0')

                !! compute locally needed matrices
                BCnormal = 0.0
                BCnormalT= 0.0
                BCparam  = 0.0
                BCmatrix = 0.0
                !! compute BC matrix with normal directions
                if (abs(abs(ctx_dg%normal(3,iface,i_cell))-1.0) < 1.e-6) then
                  BCnormal(1,1)=0.0
                  BCnormal(1,2)=0.0
                  BCnormal(1,3)=-sin(ctx_dg%normal(3,iface,i_cell)*asin(1.0))
                  BCnormal(2,1)=0.0
                  BCnormal(2,2)=1.0
                  BCnormal(2,3)=0.0
                  BCnormal(3,1)=sin(ctx_dg%normal(3,iface,i_cell)*asin(1.0))
                  BCnormal(3,2)=0.0
                  BCnormal(3,3)=0.0
                else
                  BCnormal(1,1)=ctx_dg%normal(1,iface,i_cell)
                  BCnormal(1,2)=-ctx_dg%normal(2,iface,i_cell)/             &
                                 (sqrt(ctx_dg%normal(1,iface,i_cell)**2 +   &
                                       ctx_dg%normal(2,iface,i_cell)**2))
                  BCnormal(1,3)=-ctx_dg%normal(1,iface,i_cell)*             &
                                 ctx_dg%normal(3,iface,i_cell)/             &
                                 (sqrt(ctx_dg%normal(1,iface,i_cell)**2 +   &
                                       ctx_dg%normal(2,iface,i_cell)**2))
                  BCnormal(2,1)= ctx_dg%normal(2,iface,i_cell)
                  BCnormal(2,2)= ctx_dg%normal(1,iface,i_cell)/             &
                                 (sqrt(ctx_dg%normal(1,iface,i_cell)**2  +  &
                                       ctx_dg%normal(2,iface,i_cell)**2))
                  BCnormal(2,3)=-ctx_dg%normal(2,iface,i_cell)*             &
                                 ctx_dg%normal(3,iface,i_cell)/             &
                                 (sqrt(ctx_dg%normal(1,iface,i_cell)**2 +   &
                                       ctx_dg%normal(2,iface,i_cell)**2))
                  BCnormal(3,1)= ctx_dg%normal(3,iface,i_cell)
                  BCnormal(3,2)= 0.0
                  BCnormal(3,3)= sqrt(ctx_dg%normal(1,iface,i_cell)**2+     &
                                      ctx_dg%normal(2,iface,i_cell)**2)
                endif
                !! compute BC transposed
                BCnormalT = transpose(BCnormal)

                !! compute the parameters vp vs and rho
                call hdg_model_on_midface_3D(ctx_dg,ctx_mesh,model,     &
                                             i_cell,iface,freq,rho,vp,  &
                                             vs,ctx_err)
                !! compute param BC matrix
                BCparam(1,1) = rho * vp * (-freq)
                BCparam(2,2) = rho * vs * (-freq)
                BCparam(3,3) = rho * vs * (-freq)
                !! the BC matrix is BCnormal BCparam BCnormalT
                BCmatrix = matmul(BCnormal,BCparam)
                BCmatrix = matmul(BCmatrix,BCnormalT)
                
                do i=1, ndof_face
                  index_face_dofi = ctx_dg%face_map(i_order_cell)%array(i,iface)
                  do j=1,ndof_face
                    index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
                    
                    temp = dcmplx(face_phi_phi(i_order_cell,i_order_cell)%     &
                                  array(index_face_dofi,index_face_dofj,iface)*&
                                  ctx_dg%det_jac_face(iface,i_cell))
                    !! Variable 1 (x)
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)   = &
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface)+j)     &
                      + BCmatrix(1,1)*temp
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface+4)+j) = &
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface+4)+j)   &
                      + BCmatrix(1,2)*temp
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface+8)+j) = &
                    hdgL(ndf_edges_elem(iface)+i,ndf_edges_elem(iface+8)+j)   &
                      + BCmatrix(1,3)*temp
                    !! Variable 2 (y)
                    hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface)+j) = &
                    hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface)+j)   &
                      + BCmatrix(2,1)*temp
                    hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface+4)+j)=&
                    hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface+4)+j) &
                      + BCmatrix(2,2)*temp
                    hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface+8)+j)=&
                    hdgL(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface+8)+j) &
                      + BCmatrix(2,3)*temp
                    !! Variable 3 (z)
                    hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface)+j) = &
                    hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface)+j)   &
                      + BCmatrix(3,1)*temp
                    hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface+4)+j)=&
                    hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface+4)+j) &
                      + BCmatrix(3,2)*temp
                    hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface+8)+j)=&
                    hdgL(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface+8)+j) &
                      + BCmatrix(3,3)*temp
                  end do
                end do
              
              !! unknown -----------------------------------------------
              case default
                ctx_err%msg   = "** ERROR: Unrecognized Impedance "  // &
                                "condition "//                          &
                                trim(adjustl(ctx_mesh%Zimpedance))//    &
                                " for Robin Boundary Condition [create_matrix]"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                cycle
            end select
            !! ---------------------------------------------------------

          !! other types
          case(tag_freesurface,tag_ghost_cell,tag_wallsurface,          &
               tag_neumann,tag_dirichlet,tag_analytic_elasticiso,       &
               tag_planewave_isoP,tag_planewave_isoS)

            !! TAG_GHOST ==> nothing special

            !! TAG_NEUMANN = TAG_FREESURFACE = TAG_PLLANEWAVE 
            !!               !! -------------------------- <!
                             !! it will be done at the end <!
                             !! -------------------------- <!
            !! TAG_WALLSURFACE = TAG_DIRICHLET
            !! ==> \partial_\nu Pressure = 0, i.e., displacement = 0
            !!               nothing because Neuman BC is automatic

          !! unsupported BC ............................................
          case(tag_pml_circular,tag_pml,tag_center_neumann,             &
               tag_aldirichlet_dlrobin,tag_deltapdirichlet_dldirichlet, &            
               tag_analytic_orthorhombic,tag_planewave,                 &
               tag_deltapdirichlet,tag_alrbc_dlrobin,tag_alrbc_dldirichlet)
            ctx_err%msg   = "** ERROR: Unsupported " //                 &
                            "boundary condition [create_matrix]"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle

          case default

        end select
      
      end do
      if(ctx_err%ierr .ne. 0) cycle !! non-shared error

      ! ----------------------------------------------------------------
      ! construct HDG matrices A and C 
      ! ----------------------------------------------------------------
      n = ndof_vol*dim_sol
      !! construction Ainv
      call hdg_build_Ainv_3D_tet(hdgAinv,                               &
                                 vol_phi_phi_rho(i_order_cell)%array,   &
                                 vol_phi_phi_S  (i_order_cell)%array,   &
                                 vol_dphi_phi   (i_order_cell)%array,   &
                                 sumface_phiphi_tau,                    &
                                 ctx_dg%inv_jac(:,:,i_cell),            &
                                 ctx_dg%det_jac(i_cell),ndof_vol,freq,  &
                                 ctx_err)
      if(ctx_err%ierr .ne. 0) cycle
      !! construction C
      call hdg_build_C_3D_tet(hdgC,face_phii_xij,face_phii_xij_tau,     &
                              penalization,ndof_vol,ndof_face_neigh,    &
                              ndf_edges_elem,ctx_dg%normal(:,:,i_cell))

      !! update saved matrices
      ctx_dg%hdg%Ainv(i_cell)%array(1:n,1:n) = cmplx(hdgAinv(1:n,1:n),kind=RKIND_MAT)
      !! Q =-A^-1 C
      ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot) = cmplx(-matmul(  &
                   hdgAinv(1:n,1:n),hdgC(1:n,1:ndof_face_tot)),kind=RKIND_MAT)
      !! X =-B A^-1
      ctx_dg%hdg%X(i_cell)%array(1:ndof_face_tot,1:n) = cmplx(-matmul(  &
                  hdgB(1:ndof_face_tot,1:n),hdgAinv(1:n,1:n)),kind=RKIND_MAT)
      if(flag_inv) then
        ctx_dg%hdg%W(i_cell)%array(1:ndof_face_tot,1:n)=cmplx(          &
                     matmul(transpose(conjg(hdgC(1:n,1:ndof_face_tot))),&
                            transpose(conjg(hdgAinv(1:n,1:n)))), kind=RKIND_MAT)
        ctx_dg%hdg%D(i_cell)%array(1:n,1:ndof_face_tot)=cmplx(          &
            matmul(transpose(conjg(hdgAinv(1:n,1:n))),                  &
                   transpose(conjg(hdgB(1:ndof_face_tot,1:n)))), kind=RKIND_MAT)
      endif

      !! local matrix coeff = L - B*A^-1*C
      !!                    = L + B*Q
      coeff(1:ndof_face_tot,1:ndof_face_tot) =                &
                      hdgL(1:ndof_face_tot,1:ndof_face_tot) + &
                           matmul(hdgB(1:ndof_face_tot,1:n),  &
                           dcmplx(ctx_dg%hdg%Q(i_cell)%array(1:n,1:ndof_face_tot)))

      !! ------------------------------------------
      !! readjust the wall surface to identity
      !! DIRICHLET condition
      !! ------------------------------------------
      do iface=1, ctx_mesh%n_neigh_per_cell
        neigh = ctx_mesh%cell_neigh(iface,i_cell)
        if(neigh .eq. tag_wallsurface         .or.  &
           neigh .eq. tag_dirichlet           .or.  &
           neigh .eq. tag_analytic_elasticiso .or.  &
           neigh .eq. tag_planewave_isoS      .or.  &
           neigh .eq. tag_planewave_isoP) then

          !! -------------------------------------------------------------- %
          !! The local block will be replaced by Identity to maintain the 
          !! symmetry of the matrix. However, using the weak form, we do
          !! not readily have the identity but 
          !!   \integral \Lambda \xi_j  = \integral g \xi_j   \forall j
          !! where g is my source function. That is, we have in matrix form:
          !!             hdgL \Lambda = S.
          !! 
          !! Here we save the matrix hdgL, and we will solve the local
          !! problem when we create the source. Hence we will readily
          !! have the values of Lambda, and we can use and identity block
          !! that allows us to maintain the symmetry of the matrix.
          !!
          !!  e.g., original local system would be 
          !!  (x x x)                  (x x x)                      (x x  0)
          !!  (x x x)\Lambda , and now (x x x)\Lambda, and we need  (x x  0) \Lambda
          !!  (x x x)                  (0 0 L)                      (0 0 Id)
          !!no dirichlet           dirichlet face 3               for symmetry
          !! -------------------------------------------------------------- %



          !! create a Identity matrix coefficients to impose 0 velocity
          !! All of the velocities must be zero, we cannot force 
          !! the normal velocity only because it would be ill-posed !
          !! Variable 1
          coeff(  ndf_edges_elem(iface)+1: &
                  ndf_edges_elem(iface)+ndof_face,:)=0.0
          !! Variable 2
          coeff(  ndf_edges_elem(iface+4)+1: &
                  ndf_edges_elem(iface+4)+ndof_face,:)=0.0
          !! Variable 3
          coeff(  ndf_edges_elem(iface+8)+1: &
                  ndf_edges_elem(iface+8)+ndof_face,:)=0.0

          !! -----------------------------------------------------------
          !! save lines to symmetrize the matrix.
          flag_info_sym(iface) = .true. 
          !! we can use ndof_face above because the face on the boundary
          !! anyway has the order of the cell, but it should be 
          !! ndof_face_neigh(iface)
          ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) =                 &
            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) +               &
            !! only the other faces times the number of variables
            dim_var*(ndof_face_tot - ndof_face_neigh(iface))*ndof_face_neigh(iface)
          !! -----------------------------------------------------------

          !! diagonal for the 3 variables
          do i=1, ndof_face
            coeff(ndf_edges_elem(iface  )+i,ndf_edges_elem(iface  )+i) = 1.0
            coeff(ndf_edges_elem(iface+4)+i,ndf_edges_elem(iface+4)+i) = 1.0
            coeff(ndf_edges_elem(iface+8)+i,ndf_edges_elem(iface+8)+i) = 1.0
          end do

          if(neigh .eq. tag_planewave_isoP .or.                         &
             neigh .eq. tag_planewave_isoS .or.                         &
             neigh .eq. tag_analytic_elasticiso) then
            !! ---------------------------------------------------------
            !! we save the hdgL block for the weak form on the boundary.
            !! we also save the velocity on the face in case we have a 
            !! planewave. In elastic iso 3D there should be three wavespeeds?
            !! ---------------------------------------------------------
            face = ctx_mesh%cell_face(iface,i_cell)       
            !! compute the parameters vp vs and rho
            call hdg_model_on_midface_3D(ctx_dg,ctx_mesh,model,         &
                                         i_cell,iface,freq,rho,vp,      &
                                         vs,ctx_err)

            call array_allocate(ctx_dg%face_velocity(face),3,ctx_err)
            !! remark that we could save for all dof on the face ...
            ctx_dg%face_velocity(face)%array(1) = vp
            ctx_dg%face_velocity(face)%array(2) = vs
            ctx_dg%face_velocity(face)%array(3) = rho

            do i=1,dim_var
              call array_allocate(ctx_dg%ctx_symmetrize_mat%f_matbc(face,i),&
                                  ndof_face_neigh(iface),                   &
                                  ndof_face_neigh(iface),ctx_err)
              ctx_dg%ctx_symmetrize_mat%f_matbc(face,i)%array = 0.d0
            end do
            !! because the matrix is symmetric without planewave.
            ctx_dg%ctx_symmetrize_mat%flag_to_symmetrize = .true.
            i_o_neigh = i_order_neigh(iface)
            !! this is the block hdgL without the penalization: \int \xi_i \xi_j 
            do i=1, ndof_face_neigh(iface)
              index_face_dofi = ctx_dg%face_map(i_o_neigh)%array(i,iface)
              do j=1, ndof_face_neigh(iface)
                index_face_dofj = ctx_dg%face_map(i_o_neigh)%array(j,iface)
                temp = dcmplx(face_phi_phi(i_o_neigh,i_o_neigh)%     &
                       array(index_face_dofi,index_face_dofj,iface)* &
                       ctx_dg%det_jac_face(iface,i_cell))
             
                 !! three variables here.
                 ctx_dg%ctx_symmetrize_mat%f_matbc(face,1)%array(i,j) = temp
                 ctx_dg%ctx_symmetrize_mat%f_matbc(face,2)%array(i,j) = temp
                 ctx_dg%ctx_symmetrize_mat%f_matbc(face,3)%array(i,j) = temp
              end do
            end do

          end if !! end if planewave

        end if !! end if planewave or dirichlet or free-surface

      end do !! end loop over faces.

      !! allocate arrays if non-zeros
      if(ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell) .ne. 0) then
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_line(i_cell),   &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_col(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
        call array_allocate(ctx_dg%ctx_symmetrize_mat%c_val(i_cell),    &
                            ctx_dg%ctx_symmetrize_mat%cell_nval(i_cell),ctx_err)
         ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array = 0
         ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array = 0.d0
      end if 
      ind_sym = 0
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! Update global coefficients
      !! we first need to find the appropriate index for the face first
      !!
      !! loop over the face of the variable 1 == [ROW]
      do iface=1,ctx_mesh%n_neigh_per_cell ; do ivar=1,dim_var
        if(ctx_err%ierr .ne. 0) cycle
        
        nval_i = ndf_edges_elem((ivar-1)*4+iface+1) -                   &
                 ndf_edges_elem((ivar-1)*4+iface  )
        do ipos=1,nval_i
          if(ctx_err%ierr .ne. 0) cycle
          
          !! row info --------------------------------------------------
          i        = ndf_edges_elem(iface)+ipos + (ivar-1)*ndof_face_cell
          iline    = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(iface,i_cell))
          i_o_neigh= i_order_neigh(iface)
          i_orient = ctx_mesh%cell_face_orientation(iface,i_cell)
          iline    = iline + (ivar-1)*ctx_dg%n_dofgb_pervar +           &
                     ctx_dg%hdg%idof_face (i_orient,i_o_neigh)%array(ipos) 

          !! col info --------------------------------------------------
          do jface=1,ctx_mesh%n_neigh_per_cell ; do jvar=1,dim_var
            if(ctx_err%ierr .ne. 0) cycle
            
            nval_j = ndf_edges_elem((jvar-1)*4+jface+1) -               &
                     ndf_edges_elem((jvar-1)*4+jface)

            do jpos=1,nval_j
              if(ctx_err%ierr .ne. 0) cycle

              j        = ndf_edges_elem(jface)+jpos+ (jvar-1)*ndof_face_cell
              icol     = ctx_dg%offsetdof_mat_gb(ctx_mesh%cell_face(jface,i_cell))
              i_o_neigh= i_order_neigh(jface)
              i_orient = ctx_mesh%cell_face_orientation(jface,i_cell)
              icol     = icol + (jvar-1)*ctx_dg%n_dofgb_pervar +        &
                         ctx_dg%hdg%idof_face(i_orient,i_o_neigh)%array(jpos)

              !! save Dirichlet BC infos to make the matrix symmetric    
              !! by modifying the rhs.
              if( flag_info_sym(jface) .and. (.not.flag_info_sym(iface))) then
                ind_sym = ind_sym + 1
                ctx_dg%ctx_symmetrize_mat%c_line(i_cell)%array(ind_sym) = &
                  int(iline, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_col (i_cell)%array(ind_sym) = &
                  int(icol, kind=4)
                ctx_dg%ctx_symmetrize_mat%c_val (i_cell)%array(ind_sym) = &
                                       cmplx(coeff(i,j),kind=RKIND_POL)
                !! this coeff can be set to zero now.
                coeff(i,j) = 0.d0
              end if

              !! update symmetric
              if(abs(coeff(i,j))>tol) then
                !! The BC do not remove the symmetry of the matrix
                if(icol >= iline) then
                  !$OMP CRITICAL
                  nnz_exact       = nnz_exact + 1
                  Alin(nnz_exact) = iline
                  Acol(nnz_exact) = icol
                  Aval(nnz_exact) = cmplx(coeff(i,j),kind=RKIND_MAT)
                  !$OMP END CRITICAL

                  !! check too high values 
                  if(real (Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.aimag(Aval(nnz_exact)) > huge(real(1.0,kind=RKIND_MAT)) & 
                 .or.ieee_is_nan(real (Aval(nnz_exact))) .or.                &
                     ieee_is_nan(aimag(Aval(nnz_exact)))) then
                    write(ctx_err%msg,'(a,i0,a,es9.2,a)') "** ERROR: "//  &
                          "the matrix has entry infinity or NaN."//       &
                          " Current matrix precision kind is ", RKIND_MAT,&
                          " (8 at most), which amounts to a"     //       &
                          " maximal value of ",                           &
                          huge(real(1.0,kind=RKIND_MAT)), " [create_matrix]"
                    ctx_err%ierr  = -1
                    ctx_err%critic=.true.
                    cycle
                  end if

                end if
              end if

            end do ; end do
          end do
        end do ; end do
      end do
      ! ----------------------------------------------------------------

   end do
   !$OMP END DO
   dim_nnz_loc = nnz_exact
   deallocate(coeff            )
   deallocate(hdgL             )
   deallocate(face_phii_xij    )
   deallocate(face_phii_xij_tau)
   deallocate(sumface_phiphi_tau)
   deallocate(hdgC )
   deallocate(hdgB )
   deallocate(hdgAinv)
    !! quadrature arrays
    do i = 1, ctx_dg%n_different_order
      !! size depends if derivative or not
      call array_deallocate(vol_phi_phi    (i))
      call array_deallocate(vol_phi_phi_rho(i))
      call array_deallocate(vol_phi_phi_S  (i))
      call array_deallocate(vol_dphi_phi   (i))
      do j = 1, ctx_dg%n_different_order
        call array_deallocate(face_phi_phi(i,j))
        call array_deallocate(face_phi_phi_tau(i,j))
      end do
    end do
    deallocate(vol_phi_phi     )
    deallocate(vol_phi_phi_rho )
    deallocate(vol_phi_phi_S   )
    deallocate(vol_dphi_phi    )
    deallocate(face_phi_phi    )
    deallocate(face_phi_phi_tau)
   !$OMP END PARALLEL

    return
  end subroutine create_matrix_quadrature_3D

end module m_create_matrix_quadrature
