!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_utils.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> HELMHOLTZ ELASTIC propagator using the
!> HDG discretization
!
!------------------------------------------------------------------------------
module m_matrix_utils

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH, RKIND_POL
  use m_ctx_discretization,     only: t_discretization
  !> inverse matrix with Lapack
  use m_lapack_inverse,         only: lapack_matrix_inverse
  !> for the computation with quadrature rules 
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_array_types,            only: t_array3d_real, t_array5d_complex
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_eval_cx, model_eval_raw,    &
                                      model_eval_Cstiff,                &
                                      model_eval_Sstiff,                &
                                      compute_operator_A_2D,            &
                                      compute_operator_A_3D,            &
                                      compute_operator_DCD_2D,          &
                                      compute_operator_DCD_3D
  use m_model_list,             only: model_RHO
  use m_mesh_convert_face_position,                                     &
                                only: mesh_convert_face_position_2D,    &
                                      mesh_convert_face_position_3D
  !! -------------------------------------------------------------------


  implicit none

  !! ___________________________________________________________________
  !! careful that fortran is column-wise.
  !! real(kind=8), parameter :: VOIGT2D(3,3) = transpose(reshape(       &
  !!                         (/ 1.d0, 0.d0, 0.d0          &
  !!                           ,0.d0, 1.d0, 0.d0          &
  !!                           ,0.d0, 0.d0, 2.d0 /),     (/3,3/)))
  !! ___________________________________________________________________
  real   (kind=8),parameter   :: Id2x2(2,2) = transpose(reshape(        &
                                   (/ 1.d0, 0.d0, 0.d0, 1.d0/),(/2,2/)))
  real   (kind=8),parameter   :: Id3x3(3,3) = transpose(reshape(        &
      (/ 1.d0, 0.d0, 0.d0, 0.d0, 1.d0, 0.d0, 0.d0, 0.d0, 1.d0/),(/3,3/)))

  private
  public :: hdg_build_Ainv_2D_tri, hdg_build_C_2D_tri
  public :: hdg_build_Ainv_3D_tet, hdg_build_C_3D_tet
  public :: hdg_build_quadrature_int_2D
  public :: hdg_build_quadrature_int_3D
  public :: hdg_model_on_midface_2D, hdg_model_on_midface_3D


  contains



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ELASTIC 
  !> in 2D based upon triangle discretization 
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    rho,lambda,mu  : models parameter
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_2D_tri(Ainv,vol_phi_phi_rho,vol_phi_phi_S,  &
                                   vol_dphi_phi,sumface_phii_phij_nCn,  &
                                   inv_jac,det_jac,ndof_vol,freq,ctx_err)
    implicit none
    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_rho(:,:)
    complex(kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_S(:,:,:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij_nCn(:,:,:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: freq
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    
    !!
    real   (kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: masstens(3,3)
    real   (kind=RKIND_POL)     :: dphii_phij(2)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n

    dim_sol = 5                 !! Ux, Uz, Sxx, Szz, Sxz
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    !! -----------------------------------------------------------------

    
    do i=1, ndof_vol ; do j=1, ndof_vol

      massterm     =vol_phi_phi_rho(i,j)  *det_jac   
      dphii_phij(1)=vol_dphi_phi(j,i,1)*inv_jac(1,1) + &
                    vol_dphi_phi(j,i,2)*inv_jac(2,1)
      dphii_phij(2)=vol_dphi_phi(j,i,1)*inv_jac(1,2) + &
                    vol_dphi_phi(j,i,2)*inv_jac(2,2)
      dphii_phij   =dphii_phij*det_jac
      ! ----------------------------------------------------------------
      !! freq*freq = (-s + i \omega)² = s² - 2i s \omega - \omega² 
      ! ----------------------------------------------------------------
      !! equation (1--2)
      A(i         , j           ) = dcmplx(freq*freq*massterm + sumface_phii_phij_nCn(i,j,1,1))
      A(i         , j+  ndof_vol) = dcmplx(                     sumface_phii_phij_nCn(i,j,1,2))
      A(i         , j+2*ndof_vol) = dcmplx(-dphii_phij(1))
      A(i         , j+4*ndof_vol) = dcmplx(-dphii_phij(2))
      
      A(i+ndof_vol, j           ) = dcmplx(                     sumface_phii_phij_nCn(i,j,2,1))
      A(i+ndof_vol, j+  ndof_vol) = dcmplx(freq*freq*massterm + sumface_phii_phij_nCn(i,j,2,2))
      A(i+ndof_vol, j+3*ndof_vol) = dcmplx(-dphii_phij(2)              )
      A(i+ndof_vol, j+4*ndof_vol) = dcmplx(-dphii_phij(1)              )
      
      !! xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      !! equation (3--5)
      dphii_phij(1)= vol_dphi_phi(i,j,1)*inv_jac(1,1)  + &
                     vol_dphi_phi(i,j,2)*inv_jac(2,1)
      dphii_phij(2)= vol_dphi_phi(i,j,1)*inv_jac(1,2)  + &
                     vol_dphi_phi(i,j,2)*inv_jac(2,2)
      dphii_phij   = dphii_phij*det_jac
      masstens(:,:)= vol_phi_phi_S(i,j,:,:)*det_jac

      ! equation (3) ---------------------------------------------------
      A(i+2*ndof_vol, j)            =-dphii_phij(1)         !! ux
      A(i+2*ndof_vol, j+  ndof_vol) = 0.d0                  !! uz
      A(i+2*ndof_vol, j+2*ndof_vol) =     -dcmplx(masstens(1,1)) !! sxx
      A(i+2*ndof_vol, j+3*ndof_vol) =     -dcmplx(masstens(1,2)) !! szz
      A(i+2*ndof_vol, j+4*ndof_vol) =-2.d0*dcmplx(masstens(1,3)) !! sxz
      
      ! equation (4) ---------------------------------------------------
      A(i+3*ndof_vol, j           ) = 0.d0                  !! ux
      A(i+3*ndof_vol, j+  ndof_vol) =-dphii_phij(2)         !! uz
      A(i+3*ndof_vol, j+2*ndof_vol) =-dcmplx(masstens(2,1)) !! sxx
      A(i+3*ndof_vol, j+3*ndof_vol) =-dcmplx(masstens(2,2)) !! szz
      A(i+3*ndof_vol, j+4*ndof_vol) =-2.d0*dcmplx(masstens(2,3)) !! sxz

      ! equation (5) ---------------------------------------------------
      A(i+4*ndof_vol, j)            =-1./2.*dphii_phij(2)    !! ux
      A(i+4*ndof_vol, j+  ndof_vol) =-1./2.*dphii_phij(1)    !! uz
      A(i+4*ndof_vol, j+2*ndof_vol) =-1.d0*dcmplx(masstens(3,1)) !! sxx
      A(i+4*ndof_vol, j+3*ndof_vol) =-1.d0*dcmplx(masstens(3,2)) !! szz
      A(i+4*ndof_vol, j+4*ndof_vol) =-2.d0*dcmplx(masstens(3,3)) !! sxz


    enddo ; enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)
    return

    return
  end subroutine hdg_build_Ainv_2D_tri
 
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix C for HDG in HELMHOLTZ ELASTIC
  !> in 2D based upon triangle discretization 
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_C_2D_tri(C,F,Ftau,penalization,ndof_vol,         &
                                ndof_face_neigh,ndf_edges_elem,         &
                                normal)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: C(:,:)
    complex(kind=8), allocatable     ,intent(in)   :: F(:,:,:)
    complex(kind=8), allocatable     ,intent(in)   :: Ftau(:,:,:,:,:)
    complex(kind=8)                                :: penalization   (3)
    integer                          ,intent(in)   :: ndof_face_neigh(3)
    integer                          ,intent(in)   :: ndf_edges_elem (7)
    integer                          ,intent(in)   :: ndof_vol
    real(kind=8)                     ,intent(in)   :: normal(2,3)
    !!
    integer         :: i, j, i_face
    ! real(kind=8)    :: ExxN(2),EzzN(2),ExzN(2)

    C=0.0
    do i_face=1,3 !! triangle face
      
      do i=1,ndof_vol ; do j=1,ndof_face_neigh(i_face)
        !! equation (1--2) ---------------------------------------------
        C(           i,ndf_edges_elem(i_face)  +j)=-penalization(i_face)*Ftau(i_face,i,j,1,1)
        C(           i,ndf_edges_elem(i_face+3)+j)=-penalization(i_face)*Ftau(i_face,i,j,1,2)
        C(  ndof_vol+i,ndf_edges_elem(i_face)  +j)=-penalization(i_face)*Ftau(i_face,i,j,2,1)
        C(  ndof_vol+i,ndf_edges_elem(i_face+3)+j)=-penalization(i_face)*Ftau(i_face,i,j,2,2)

        !! equation (3--5) ---------------------------------------------
        C(2*ndof_vol+i,ndf_edges_elem(i_face  )+j)= normal(1,i_face)*F(i_face,i,j)

        C(3*ndof_vol+i,ndf_edges_elem(i_face+3)+j)= normal(2,i_face)*F(i_face,i,j)

        C(4*ndof_vol+i,ndf_edges_elem(i_face  )+j)=1./2.*normal(2,i_face)*F(i_face,i,j)
        C(4*ndof_vol+i,ndf_edges_elem(i_face+3)+j)=1./2.*normal(1,i_face)*F(i_face,i,j)
      enddo ; enddo
    enddo
    
    return
  end subroutine hdg_build_C_2D_tri


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix Ainv for HDG in HELMHOLTZ ELASTIC 
  !> in 3D based upon tetrahedral discretization 
  !
  !> @param[inout] Ainv           : A^(-1)
  !> @param[in]    vol_phi_phi    : volume integral \phi_i \phi_j
  !> @param[in]    sumface_phi_phi: sum of face integral \phi_i \phi_j
  !> @param[in]    inv_jac        : inverse of the jacobian
  !> @param[in]    det_jac        : determinant of the jacobian
  !> @param[in]    rho,lambda,mu  : models parameter
  !> @param[in]    ndof_vol       : volumic ndof
  !> @param[in]    frequency      : frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine hdg_build_Ainv_3D_tet(Ainv,vol_phi_phi_rho,vol_phi_phi_S,  &
                                   vol_dphi_phi,sumface_phii_phij_nCn,  &
                                   inv_jac,det_jac,ndof_vol,freq,ctx_err)
    implicit none
    complex(kind=8), allocatable  ,intent(inout):: Ainv(:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_rho(:,:)
    complex(kind=RKIND_POL)       ,intent(in)   :: vol_phi_phi_S(:,:,:,:)
    real   (kind=RKIND_POL)       ,intent(in)   :: vol_dphi_phi(:,:,:)
    complex(kind=8), allocatable  ,intent(in)   :: sumface_phii_phij_nCn(:,:,:,:)
    real   (kind=8)               ,intent(in)   :: inv_jac(:,:)
    real   (kind=8)               ,intent(in)   :: det_jac
    complex(kind=8)               ,intent(in)   :: freq
    integer                       ,intent(in)   :: ndof_vol
    type(t_error)                 ,intent(inout):: ctx_err
    !!
    real   (kind=RKIND_POL)     :: massterm
    complex(kind=RKIND_POL)     :: masstens(6,6)
    complex(kind=RKIND_POL)     :: dphii_phij(3)
    complex(kind=8),allocatable :: A(:,:)
    integer                     :: i, j, dim_sol, n
    
    dim_sol = 9                !! Ux, Uy, Uz, Sxx, Syy, Szz, Sxy, Sxz, Syz
    n       = ndof_vol*dim_sol
    allocate(A(n,n))
    A=0.0 ; Ainv =0.0
    ! ---------------

    do i=1, ndof_vol ; do j=1, ndof_vol
      massterm     = vol_phi_phi_rho(i,j) * det_jac
      dphii_phij(1)= vol_dphi_phi(j,i,1)* inv_jac(1,1) +                &
                     vol_dphi_phi(j,i,2)* inv_jac(2,1) +                &
                     vol_dphi_phi(j,i,3)* inv_jac(3,1)
      dphii_phij(2)= vol_dphi_phi(j,i,1)* inv_jac(1,2) +                &
                     vol_dphi_phi(j,i,2)* inv_jac(2,2) +                &
                     vol_dphi_phi(j,i,3)* inv_jac(3,2)
      dphii_phij(3)= vol_dphi_phi(j,i,1)* inv_jac(1,3) +                &
                     vol_dphi_phi(j,i,2)* inv_jac(2,3) +                &
                     vol_dphi_phi(j,i,3)* inv_jac(3,3)
      dphii_phij   = dphii_phij*det_jac


      !! equation (1,2 and 3) ==========================================
      A(i           , j           ) = dcmplx(freq*freq*massterm + sumface_phii_phij_nCn(i,j,1,1))
      A(i           , j+  ndof_vol) = dcmplx(                     sumface_phii_phij_nCn(i,j,1,2))
      A(i           , j+2*ndof_vol) = dcmplx(                     sumface_phii_phij_nCn(i,j,1,3))     
      A(i           , j+3*ndof_vol) = dcmplx(-dphii_phij(1)) ! sxx 
     !A(i           , j+4*ndof_vol) = 0.d0                   ! syy
     !A(i           , j+5*ndof_vol) = 0.d0                   ! szz 
      A(i           , j+6*ndof_vol) = dcmplx(-dphii_phij(2)) ! sxy
      A(i           , j+7*ndof_vol) = dcmplx(-dphii_phij(3)) ! sxz
     !A(i           , j+8*ndof_vol) = 0.d0                   ! syz
      
      A(i+  ndof_vol, j           ) = dcmplx( sumface_phii_phij_nCn(i,j,2,1))
      A(i+  ndof_vol, j+  ndof_vol) = dcmplx(freq*freq*massterm + sumface_phii_phij_nCn(i,j,2,2))
      A(i+  ndof_vol, j+2*ndof_vol) = dcmplx( sumface_phii_phij_nCn(i,j,2,3))
     !A(i+  ndof_vol, j+3*ndof_vol) = 0.d0                   ! sxx
      A(i+  ndof_vol, j+4*ndof_vol) = dcmplx(-dphii_phij(2)) ! syy
     !A(i+  ndof_vol, j+5*ndof_vol) = 0.d0                   ! szz
      A(i+  ndof_vol, j+6*ndof_vol) = dcmplx(-dphii_phij(1)) ! sxy
     !A(i+  ndof_vol, j+7*ndof_vol) = 0.d0                   ! sxz
      A(i+  ndof_vol, j+8*ndof_vol) = dcmplx(-dphii_phij(3)) ! syz

      A(i+2*ndof_vol, j           ) = dcmplx( sumface_phii_phij_nCn(i,j,3,1))
      A(i+2*ndof_vol, j+  ndof_vol) = dcmplx( sumface_phii_phij_nCn(i,j,3,2))
      A(i+2*ndof_vol, j+2*ndof_vol) = dcmplx(freq*freq*massterm + sumface_phii_phij_nCn(i,j,3,3))
     !A(i+2*ndof_vol, j+3*ndof_vol) = 0.d0                   ! sxx
     !A(i+2*ndof_vol, j+4*ndof_vol) = 0.d0                   ! syy
      A(i+2*ndof_vol, j+5*ndof_vol) = dcmplx(-dphii_phij(3)) ! szz
     !A(i+2*ndof_vol, j+6*ndof_vol) = 0.d0                   ! sxy
      A(i+2*ndof_vol, j+7*ndof_vol) = dcmplx(-dphii_phij(1)) ! sxz
      A(i+2*ndof_vol, j+8*ndof_vol) = dcmplx(-dphii_phij(2)) ! syz

      !! equation (4, 5, 6, 7, 8 and 9) ================================
      !! ---------------------------------------------------------------
      dphii_phij(1)= vol_dphi_phi(i,j,1)* inv_jac(1,1) +                &
                     vol_dphi_phi(i,j,2)* inv_jac(2,1) +                &
                     vol_dphi_phi(i,j,3)* inv_jac(3,1)
      dphii_phij(2)= vol_dphi_phi(i,j,1)* inv_jac(1,2) +                &
                     vol_dphi_phi(i,j,2)* inv_jac(2,2) +                &
                     vol_dphi_phi(i,j,3)* inv_jac(3,2)
      dphii_phij(3)= vol_dphi_phi(i,j,1)* inv_jac(1,3) +                &
                     vol_dphi_phi(i,j,2)* inv_jac(2,3) +                &
                     vol_dphi_phi(i,j,3)* inv_jac(3,3)
      dphii_phij   = dphii_phij*det_jac
      masstens(:,:) =  vol_phi_phi_S(i,j,:,:)*det_jac

      !! using operation D . chi_J, with chi_J = C.E
      !! careful that the convention is here
      !! (xx,yy,zz,yz,xz,xy)  ! warning --------------------------------
      !!     while I have     ! warning --------------------------------
      !! (xx,yy,zz,xy,xz,yz)  ! warning --------------------------------
      !! D = 0.d0
      !! D(1,1) = dphii_phij(1) ; D(1,5) = dphii_phij(3) ; D(1,6) = dphii_phij(2)
      !! D(2,2) = dphii_phij(2) ; D(2,4) = dphii_phij(3) ; D(2,6) = dphii_phij(1)
      !! D(3,3) = dphii_phij(3) ; D(3,4) = dphii_phij(2) ; D(3,5) = dphii_phij(1)

      ! equation (4)
      A(i+3*ndof_vol, j           ) =-dphii_phij(1)         ! ux
      A(i+3*ndof_vol, j+  ndof_vol) = 0.d0                  ! uy
      A(i+3*ndof_vol, j+2*ndof_vol) = 0.d0                  ! uz
      A(i+3*ndof_vol, j+3*ndof_vol) =    -dcmplx(masstens(1,1)) ! sxx
      A(i+3*ndof_vol, j+4*ndof_vol) =    -dcmplx(masstens(1,2)) ! syy
      A(i+3*ndof_vol, j+5*ndof_vol) =    -dcmplx(masstens(1,3)) ! szz
      A(i+3*ndof_vol, j+6*ndof_vol) =-2.0*dcmplx(masstens(1,4)) ! sxy
      A(i+3*ndof_vol, j+7*ndof_vol) =-2.0*dcmplx(masstens(1,5)) ! sxz
      A(i+3*ndof_vol, j+8*ndof_vol) =-2.0*dcmplx(masstens(1,6)) ! syz
      ! equation (5)
      A(i+4*ndof_vol, j           ) = 0.d0                  ! ux
      A(i+4*ndof_vol, j+  ndof_vol) =-dphii_phij(2)         ! uy
      A(i+4*ndof_vol, j+2*ndof_vol) = 0.d0                  ! uz
      A(i+4*ndof_vol, j+3*ndof_vol) =    -dcmplx(masstens(2,1)) ! sxx
      A(i+4*ndof_vol, j+4*ndof_vol) =    -dcmplx(masstens(2,2)) ! syy
      A(i+4*ndof_vol, j+5*ndof_vol) =    -dcmplx(masstens(2,3)) ! szz
      A(i+4*ndof_vol, j+6*ndof_vol) =-2.0*dcmplx(masstens(2,4)) ! sxy
      A(i+4*ndof_vol, j+7*ndof_vol) =-2.0*dcmplx(masstens(2,5)) ! sxz
      A(i+4*ndof_vol, j+8*ndof_vol) =-2.0*dcmplx(masstens(2,6)) ! syz
      ! equation (6)
      A(i+5*ndof_vol, j           ) = 0.d0                  ! ux
      A(i+5*ndof_vol, j+  ndof_vol) = 0.d0                  ! uy
      A(i+5*ndof_vol, j+2*ndof_vol) =-dphii_phij(3)         ! uz
      A(i+5*ndof_vol, j+3*ndof_vol) =    -dcmplx(masstens(3,1)) ! sxx
      A(i+5*ndof_vol, j+4*ndof_vol) =    -dcmplx(masstens(3,2)) ! syy
      A(i+5*ndof_vol, j+5*ndof_vol) =    -dcmplx(masstens(3,3)) ! szz
      A(i+5*ndof_vol, j+6*ndof_vol) =-2.0*dcmplx(masstens(3,4)) ! sxy
      A(i+5*ndof_vol, j+7*ndof_vol) =-2.0*dcmplx(masstens(3,5)) ! sxz
      A(i+5*ndof_vol, j+8*ndof_vol) =-2.0*dcmplx(masstens(3,6)) ! syz

      ! equation (7) ---------------------------------------------------
      A(i+6*ndof_vol, j           ) =-1d0/2d0*dphii_phij(2)         ! ux
      A(i+6*ndof_vol, j+  ndof_vol) =-1d0/2d0*dphii_phij(1)         ! uy
      A(i+6*ndof_vol, j+2*ndof_vol) = 0.d0                  ! uz
      ! 2 from the Voigt3D matrix, and we further needs a 2 factor 
      ! because of the Voigt notation, that adds a 2 on the 
      ! off-diagonal tensor entries when put into vector.
      A(i+6*ndof_vol, j+3*ndof_vol) =-1.d0*dcmplx(masstens(4,1)) ! sxx
      A(i+6*ndof_vol, j+4*ndof_vol) =-1.d0*dcmplx(masstens(4,2)) ! syy
      A(i+6*ndof_vol, j+5*ndof_vol) =-1.d0*dcmplx(masstens(4,3)) ! szz
      A(i+6*ndof_vol, j+6*ndof_vol) =-2.d0*dcmplx(masstens(4,4)) ! sxy
      A(i+6*ndof_vol, j+7*ndof_vol) =-2.d0*dcmplx(masstens(4,5)) ! sxz
      A(i+6*ndof_vol, j+8*ndof_vol) =-2.d0*dcmplx(masstens(4,6)) ! syz

      ! equation (8) ---------------------------------------------------
      A(i+7*ndof_vol, j           ) =-1d0/2d0*dphii_phij(3)         ! ux
      A(i+7*ndof_vol, j+  ndof_vol) = 0.d0                          ! uy
      A(i+7*ndof_vol, j+2*ndof_vol) =-1d0/2d0*dphii_phij(1)         ! uz
      ! 2 from the Voigt3D matrix, and we further needs a 2 factor 
      ! because of the Voigt notation, that adds a 2 on the 
      ! off-diagonal tensor entries when put into vector.
      A(i+7*ndof_vol, j+3*ndof_vol) =-1.d0*dcmplx(masstens(5,1)) ! sxx
      A(i+7*ndof_vol, j+4*ndof_vol) =-1.d0*dcmplx(masstens(5,2)) ! syy
      A(i+7*ndof_vol, j+5*ndof_vol) =-1.d0*dcmplx(masstens(5,3)) ! szz
      A(i+7*ndof_vol, j+6*ndof_vol) =-2.d0*dcmplx(masstens(5,4)) ! sxy
      A(i+7*ndof_vol, j+7*ndof_vol) =-2.d0*dcmplx(masstens(5,5)) ! sxz
      A(i+7*ndof_vol, j+8*ndof_vol) =-2.d0*dcmplx(masstens(5,6)) ! syz

      ! equation (9) ---------------------------------------------------
      A(i+8*ndof_vol, j           ) = 0.d0                          ! ux
      A(i+8*ndof_vol, j+  ndof_vol) =-1d0/2d0*dphii_phij(3)         ! uy
      A(i+8*ndof_vol, j+2*ndof_vol) =-1d0/2d0*dphii_phij(2)         ! uz
      ! 2 from the Voigt3D matrix, and we further needs a 2 factor 
      ! because of the Voigt notation, that adds a 2 on the 
      ! off-diagonal tensor entries when put into vector.
      A(i+8*ndof_vol, j+3*ndof_vol) =-1.d0*dcmplx(masstens(6,1)) ! sxx
      A(i+8*ndof_vol, j+4*ndof_vol) =-1.d0*dcmplx(masstens(6,2)) ! syy
      A(i+8*ndof_vol, j+5*ndof_vol) =-1.d0*dcmplx(masstens(6,3)) ! szz
      A(i+8*ndof_vol, j+6*ndof_vol) =-2.d0*dcmplx(masstens(6,4)) ! sxy
      A(i+8*ndof_vol, j+7*ndof_vol) =-2.d0*dcmplx(masstens(6,5)) ! sxz
      A(i+8*ndof_vol, j+8*ndof_vol) =-2.d0*dcmplx(masstens(6,6)) ! syz

    enddo ; enddo

    Ainv(1:n,1:n) = A(1:n,1:n)
    !! inverse of Ae -------------------------------------------------
    call lapack_matrix_inverse(A,Ainv,n,ctx_err)
    deallocate(A)

    return
  end subroutine hdg_build_Ainv_3D_tet

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix C for HDG in HELMHOLTZ ELASTIC
  !> in 3D based upon tetrahedral discretization 
  !
  !----------------------------------------------------------------------------
  subroutine hdg_build_C_3D_tet(C,F,Ftau,penalization,ndof_vol,         &
                                ndof_face_neigh,ndf_edges_elem,normal)
    implicit none
    complex(kind=8), allocatable     ,intent(inout):: C(:,:)
    complex(kind=8), allocatable     ,intent(in)   :: F(:,:,:)
    complex(kind=8), allocatable     ,intent(in)   :: Ftau(:,:,:,:,:)
    complex(kind=8)                                :: penalization   (4)
    integer                          ,intent(in)   :: ndof_face_neigh(4)
    integer                          ,intent(in)   :: ndf_edges_elem (13)
    integer                          ,intent(in)   :: ndof_vol
    real(kind=8)                     ,intent(in)   :: normal(3,4)
    !!
    integer         :: i, j, i_face
    ! real(kind=8)    :: ExxN(3),EyyN(3),EzzN(3),ExyN(3),ExzN(3),EyzN(3)
    

    do i_face=1,4
      !! ===============================================================
      !! equations are numbered as follow: 
      !!   from 1 to 3 => -\ii \omega v - \nabla \cdot \sigma = 0
      !!   from 4 to 9 => sigma = C : epsilon
      !! ===============================================================
      do i=1,ndof_vol ; do j=1,ndof_face_neigh(i_face)

        !! Equation (1 to 3), variables \lambda_x, \lambda_y, \lambda_z
        C(           i,ndf_edges_elem(i_face  )+j)=-penalization(i_face)*Ftau(i_face,i,j,1,1)
        C(           i,ndf_edges_elem(i_face+4)+j)=-penalization(i_face)*Ftau(i_face,i,j,1,2)
        C(           i,ndf_edges_elem(i_face+8)+j)=-penalization(i_face)*Ftau(i_face,i,j,1,3)
        
        C(  ndof_vol+i,ndf_edges_elem(i_face)+j)  =-penalization(i_face)*Ftau(i_face,i,j,2,1)
        C(  ndof_vol+i,ndf_edges_elem(i_face+4)+j)=-penalization(i_face)*Ftau(i_face,i,j,2,2)
        C(  ndof_vol+i,ndf_edges_elem(i_face+8)+j)=-penalization(i_face)*Ftau(i_face,i,j,2,3)
        
        C(2*ndof_vol+i,ndf_edges_elem(i_face  )+j)=-penalization(i_face)*Ftau(i_face,i,j,3,1)
        C(2*ndof_vol+i,ndf_edges_elem(i_face+4)+j)=-penalization(i_face)*Ftau(i_face,i,j,3,2)
        C(2*ndof_vol+i,ndf_edges_elem(i_face+8)+j)=-penalization(i_face)*Ftau(i_face,i,j,3,3)

        !! equation (4--9) ---------------------------------------------
        C(3*ndof_vol+i,ndf_edges_elem(i_face  )+j)= normal(1,i_face)*F(i_face,i,j)
        C(4*ndof_vol+i,ndf_edges_elem(i_face+4)+j)= normal(2,i_face)*F(i_face,i,j)
        C(5*ndof_vol+i,ndf_edges_elem(i_face+8)+j)= normal(3,i_face)*F(i_face,i,j)

        !! careful that now index 7 corresponds to _xy
        C(6*ndof_vol+i,ndf_edges_elem(i_face  )+j)= 1d0/2d0*normal(2,i_face)*F(i_face,i,j)
        C(6*ndof_vol+i,ndf_edges_elem(i_face+4)+j)= 1d0/2d0*normal(1,i_face)*F(i_face,i,j)
        C(6*ndof_vol+i,ndf_edges_elem(i_face+8)+j)= 0.d0

        !! careful that now index 8 corresponds to _xz
        C(7*ndof_vol+i,ndf_edges_elem(i_face  )+j)= 1d0/2d0*normal(3,i_face)*F(i_face,i,j)
        C(7*ndof_vol+i,ndf_edges_elem(i_face+4)+j)= 0.d0
        C(7*ndof_vol+i,ndf_edges_elem(i_face+8)+j)= 1d0/2d0*normal(1,i_face)*F(i_face,i,j)

        !! careful that now index 9 corresponds to _yz
        C(8*ndof_vol+i,ndf_edges_elem(i_face  )+j)= 0.d0
        C(8*ndof_vol+i,ndf_edges_elem(i_face+4)+j)= 1d0/2d0*normal(3,i_face)*F(i_face,i,j)
        C(8*ndof_vol+i,ndf_edges_elem(i_face+8)+j)= 1d0/2d0*normal(2,i_face)*F(i_face,i,j)
      enddo ; enddo
    enddo

    return
  end subroutine hdg_build_C_3D_tet


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Create the local integral over the current cell to obtain
  !> quantities needed on the local element matrices.
  !> 
  !>
  !   REMARK: **********************************************************
  !>  ********** Everything is still computed on the reference element
  !>  ********** we just incorporate a possibility for model variation
  !>  ********** that is, the multiplication with det_jac for the local
  !>  ********** element remains in the creation of matrix A and C.
  !   END REMARK: ******************************************************
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[in]    i_o_cell          : cell order for volume matrices
  !> @param[in]    freq              : frequency (for attenuation)
  !> @param[in]    mode              : mode for the axisymmetry
  !> @param[in]    tag_penalization  : format for penalization
  !> @param[out]   vol_phi_phi       : \f$ \int_{Ke } \phi_i(x) \phi_j(x)          \f$
  !> @param[out]   vol_dphi_phi      : \f$ \int_{Ke } d\phi_i(x) \phi_j(x)         \f$
  !> @param[out]   face_phi_xi       : \f$ \int_{DKe}  \phi_i(x) \xi_j(x)          \f$
  !> @param[out]   face_phi_xi_tau   : \f$ \int_{DKe}  \tau   \phi_i(x) \xi_j(x)   \f$
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_quadrature_int_2D(ctx_dg,ctx_mesh,model,i_cell,  &
                                  i_o_cell,freq,tag_penalization,       &
                                  !! volume matrices
                                  vol_phi_phi,                          &
                                  vol_phi_phi_rho,                      &
                                  vol_phi_phi_S,                        &
                                  !! derivative needed
                                  vol_dphi_phi,                         &
                                  !! face matrices
                                  face_phi_xi,                          &
                                  !! because of the jump condition
                                  face_phi_xi_nCntau,                   &
                                  !! error context
                                  ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer                            ,intent(in)    :: i_o_cell
    complex(kind=8)                    ,intent(in)    :: freq
    integer                            ,intent(in)    :: tag_penalization
    !! these are the matrices that are computed here
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi(:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_rho(:,:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_S(:,:,:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_dphi_phi   (:,:,:)
    !! face matrices
    type(t_array3d_real)   ,allocatable,intent(inout) :: face_phi_xi(:,:)
    type(t_array5d_complex),allocatable,intent(inout) :: face_phi_xi_nCntau(:,:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    integer                     :: k, i, j
    integer                     :: n_dof
    !! models
    complex(kind=8),allocatable :: quad_S  (:,:,:)
    real   (kind=8),allocatable :: quad_rho(:)
    !! integrals
    real   (kind=8)             :: w_phi_phi, w_dphi_phi(2)
    complex(kind=8)             :: intvol_mass(11)
    real   (kind=8)             :: intvol_dphi(2,1) !! one per dim
    integer                     :: iquad
    !! for face integral
    integer                     :: l, n_dof_l, iface
    !! geometry
    real(kind=8)                :: node_coo(2,3),xpt_gb(2),xpt_loc(2)
    !! array for the coefficients: separate Real and Imaginary
    !! size (3 faces, Nquad points, Narray)
    !! real   (kind=8),allocatable :: face_coeff_Re(:,:) 
    complex(kind=8),allocatable :: face_coeff_Cx(:,:,:,:)
    real   (kind=8)             :: intface_Re(3)     ! 3 faces, N array
    complex(kind=8)             :: intface_Cx(3,2,2) ! 3 faces, N array
    !! for continuity condition
    complex(kind=8),allocatable :: Cstiff_face(:,:,:)
    complex(kind=8),allocatable :: vp_face(:),vs_face(:)
    real   (kind=8),allocatable :: rho_face(:)
    real   (kind=8)             :: normals(2),An(2,3)
    complex(kind=8)             :: DCfaceD(3,3),loc(3,2),nCn(2,2)
    integer                     :: kdim,jdim

    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! -----------------------------------------------------------------
    !! evaluate the model parameters in all quadrature points
    !! the two coordinates of the axisymmetric are the followings:
    !!  \f$ z\f$: the height
    !!  \f$ \eta=\sqrt{x^2 + y^2} \f$.
    !! -----------------------------------------------------------------
    allocate(quad_S       (ctx_dg%quadGL_npts,3,3))
    allocate(quad_rho     (ctx_dg%quadGL_npts))
    !! -----------------------------------------------------------------
    quad_S       = 0.d0
    quad_rho     = 0.d0
    !! -----------------------------------------------------------------
    node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))

    !! -----------------------------------------------------------------
    !!
    !! read model parameters on the quadrature points 
    !! depending on the discretization.
    !! we need the global position of all of the quadrature points
    !! -----------------------------------------------------------------
    do iquad=1,ctx_dg%quadGL_npts
      xpt_loc= dble(ctx_dg%quadGL_pts(:,iquad))
      xpt_gb = matmul(dble(ctx_dg%jac(:,:,i_cell)),xpt_loc)             &
             + dble(node_coo(:,1))
      call model_eval_raw(model,model_RHO,i_cell,xpt_loc,xpt_gb,freq,   &
                      quad_rho(iquad),ctx_err)
      call model_eval_Sstiff(model,i_cell,xpt_loc,xpt_gb,freq,          &
                             quad_S(iquad,:,:),ctx_err)
    end do
    !! -----------------------------------------------------------------
    if(ctx_err%ierr .ne. 0) return

    !! -----------------------------------------------------------------
    !! Create local matrices, only the face matrix must adapted to
    !! the different order because of neighbors, otherwize, we
    !! actually only need the current cell order to deal with.
    !! -----------------------------------------------------------------
    k       = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)
    vol_phi_phi                     = 0.d0
    vol_phi_phi_rho                 = 0.d0
    vol_phi_phi_S                   = 0.d0
    vol_dphi_phi                    = 0.d0
    ! ----------------------------------------------------------------
    !!  VOLUME MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ----------------------------------------------------------------
    do i=1, n_dof ; do j=1, n_dof

      intvol_mass(:)   = 0.d0
      intvol_dphi(:,:) = 0.d0

      do iquad=1,ctx_dg%quadGL_npts
        !! REMARK: **********************************************************
        !! ********** Everything is still computed on the reference element
        !! ********** we just incorporate a possibility for model variation
        !! ********** that is, the multiplication with det_jac for the local
        !! ********** element remains in the creation of matrix A and C.
        !! END REMARK: ******************************************************

        !! for the volumic integrals
        !! the precomputation already has the weight.
        w_phi_phi     = ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        w_dphi_phi(1) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,1)
        w_dphi_phi(2) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,2)
        

        intvol_mass( 1) = intvol_mass( 1) + w_phi_phi
        intvol_mass( 2) = intvol_mass( 2) + w_phi_phi * quad_rho(iquad)
        intvol_mass( 3) = intvol_mass( 3) + w_phi_phi * quad_S(iquad,1,1)
        intvol_mass( 4) = intvol_mass( 4) + w_phi_phi * quad_S(iquad,1,2)
        intvol_mass( 5) = intvol_mass( 5) + w_phi_phi * quad_S(iquad,1,3)
        intvol_mass( 6) = intvol_mass( 6) + w_phi_phi * quad_S(iquad,2,1)
        intvol_mass( 7) = intvol_mass( 7) + w_phi_phi * quad_S(iquad,2,2)
        intvol_mass( 8) = intvol_mass( 8) + w_phi_phi * quad_S(iquad,2,3)
        intvol_mass( 9) = intvol_mass( 9) + w_phi_phi * quad_S(iquad,3,1)
        intvol_mass(10) = intvol_mass(10) + w_phi_phi * quad_S(iquad,3,2)
        intvol_mass(11) = intvol_mass(11) + w_phi_phi * quad_S(iquad,3,3)

        !!  derivatives
        intvol_dphi(:,1)  = intvol_dphi(:,1) + w_dphi_phi(:)
      end do
      
      !!     vol_matrix
      vol_phi_phi    (i,j)    =  real(intvol_mass( 1) ,kind=RKIND_POL)
      vol_phi_phi_rho(i,j)    =  real(intvol_mass( 2) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,1,1)= cmplx(intvol_mass( 3) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,1,2)= cmplx(intvol_mass( 4) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,1,3)= cmplx(intvol_mass( 5) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,2,1)= cmplx(intvol_mass( 6) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,2,2)= cmplx(intvol_mass( 7) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,2,3)= cmplx(intvol_mass( 8) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,3,1)= cmplx(intvol_mass( 9) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,3,2)= cmplx(intvol_mass(10) ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,3,3)= cmplx(intvol_mass(11) ,kind=RKIND_POL)

      vol_dphi_phi   (i,j,:) =  real(intvol_dphi(:,1),kind=RKIND_POL)
    end do ; end do

    !! -----------------------------------------------------------------
    !!  SURFACE MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !!  We only need to sum the pre-computed array and the
    !!  passage towards the local element is made, as usual, in the
    !!  creations of matrix A & B & L, etc.
    !! -----------------------------------------------------------------
    
    !! compute the penalization coeff on face once and for all.
    !! 3 triangle face, 2x2 matrix
    allocate(face_coeff_Cx(3,ctx_dg%quadGL_face_npts,2,2))
    face_coeff_Cx = 0.d0
    
    !! loop over all faces to evaluate the beta_sigma and A first
    allocate(Cstiff_face(3,3,ctx_dg%quadGL_face_npts))
    allocate(vp_face        (ctx_dg%quadGL_face_npts))
    allocate(vs_face        (ctx_dg%quadGL_face_npts))
    allocate(rho_face       (ctx_dg%quadGL_face_npts))
    do iface=1,ctx_mesh%n_neigh_per_cell !! three triangle faces
      Cstiff_face   =0.d0
      call hdg_model_on_face_2D_quad(ctx_dg,ctx_mesh,model,i_cell,      &
                                     iface,freq,Cstiff_face,rho_face,   &
                                     vp_face,vs_face,ctx_err)
      !! compute the weights depending on the penalization chosen.
      select case(tag_penalization)
        !! -------------------------------------------------------------
        !! Identity with 1.0.
        !! -------------------------------------------------------------
        case(0) 
          do iquad=1,ctx_dg%quadGL_face_npts
            !! diagonal only
            face_coeff_Cx(iface,iquad,1,1) = 1.d0 !! with tau = 1.0
            face_coeff_Cx(iface,iquad,2,2) = 1.d0 !! with tau = 1.0
          end do

        !! -------------------------------------------------------------
        !! using Godunov formulation with -i \omega scaling
        !! -------------------------------------------------------------
        case(23) 
          normals(:) = dble(ctx_dg%normal(:,iface,i_cell))
          call compute_operator_A_2D  (normals,An)
          do iquad=1,ctx_dg%quadGL_face_npts
            !! compute n . C . n
            call compute_operator_DCD_2D(Cstiff_face(:,:,iquad),DCfaceD)
            loc = matmul(DCfaceD,transpose(An))
            nCn = matmul(An,loc)
            !! adjust with param
            face_coeff_Cx(iface,iquad,:,:) = -dcmplx(0.0,1.0)*abs(aimag(freq)) * &
               rho_face(iquad)/(vp_face(iquad) + vs_face(iquad))        &
             *(vp_face(iquad)*vs_face(iquad)*Id2x2(:,:) + nCn/rho_face(iquad))
          end do

        case default !! unknown
          ctx_err%msg   = "** ERROR:unrecognized penalization format "//&
                          " [build_quadrature] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !! non-shared error
        end select
    end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! we need the second loop over the different orders ...............
    !! -----------------------------------------------------------------
    do k = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order(k)

      do l = 1, ctx_dg%n_different_order
        n_dof_l = ctx_dg%n_dof_per_order (l)

        face_phi_xi       (k,l)%array = 0.d0
        face_phi_xi_nCntau(k,l)%array = 0.d0
        do i=1, n_dof ; do j=1, n_dof_l
          intface_Re = 0.d0
          intface_Cx = 0.d0

          do iquad=1,ctx_dg%quadGL_face_npts
            !! weight is already taken into account, all faces at once here
            do iface=1,ctx_mesh%n_neigh_per_cell !! three triangle faces
              !! raw
              intface_Re(iface) = intface_Re(iface) +                   &
                   ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,iface)
              
              !! with coeffs complex
              do kdim=1,ctx_mesh%dim_domain ; do jdim=1,ctx_mesh%dim_domain
                intface_Cx(iface,kdim,jdim) = intface_Cx(iface,kdim,jdim)+ &
                               face_coeff_Cx(iface,iquad,kdim,jdim) *      &
                     ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,iface)
              enddo ; enddo
            end do
          end do
          !! update all faces.
          face_phi_xi(k,l)%array(i,j,:)=real(intface_Re(:),kind=RKIND_POL)
          do kdim=1,ctx_mesh%dim_domain ; do jdim=1,ctx_mesh%dim_domain
            face_phi_xi_nCntau(k,l)%array(i,j,:,kdim,jdim) =            &
                             cmplx(intface_Cx(:,kdim,jdim),kind=RKIND_POL)
          end do; end do
        end do ; end do

      end do !! end second loop on the different orders.
    end do !! end first loop on the different orders.

    deallocate(quad_rho     )
    deallocate(quad_S       ) 
    deallocate(face_coeff_Cx)
    deallocate(Cstiff_face  )
    deallocate(vp_face      )
    deallocate(vs_face      )
    deallocate(rho_face     )

    return
  end subroutine hdg_build_quadrature_int_2D

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Create the local integral over the current cell to obtain
  !> quantities needed on the local element matrices.
  !> 
  !>
  !   REMARK: **********************************************************
  !>  ********** Everything is still computed on the reference element
  !>  ********** we just incorporate a possibility for model variation
  !>  ********** that is, the multiplication with det_jac for the local
  !>  ********** element remains in the creation of matrix A and C.
  !   END REMARK: ******************************************************
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[in]    i_o_cell          : cell order for volume matrices
  !> @param[in]    freq              : frequency (for attenuation)
  !> @param[in]    mode              : mode for the axisymmetry
  !> @param[in]    tag_penalization  : format for penalization
  !> @param[out]   vol_phi_phi       : \f$ \int_{Ke } \phi_i(x) \phi_j(x)          \f$
  !> @param[out]   vol_dphi_phi      : \f$ \int_{Ke } d\phi_i(x) \phi_j(x)         \f$
  !> @param[out]   face_phi_xi       : \f$ \int_{DKe}  \phi_i(x) \xi_j(x)          \f$
  !> @param[out]   face_phi_xi_tau   : \f$ \int_{DKe}  \tau   \phi_i(x) \xi_j(x)   \f$
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_build_quadrature_int_3D(ctx_dg,ctx_mesh,model,i_cell,  &
                                  i_o_cell,freq,tag_penalization,       &
                                  !! volume matrices
                                  vol_phi_phi,                          &
                                  vol_phi_phi_rho,                      &
                                  vol_phi_phi_S,                        &
                                  !! derivative needed
                                  vol_dphi_phi,                         &
                                  !! face matrices
                                  face_phi_xi,                          &
                                  !! because of the jump condition
                                  face_phi_xi_nCntau,                   &
                                  !! error context
                                  ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer                            ,intent(in)    :: i_o_cell
    complex(kind=8)                    ,intent(in)    :: freq
    integer                            ,intent(in)    :: tag_penalization
    !! these are the matrices that are computed here
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi(:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_rho(:,:)
    complex(kind=RKIND_POL),allocatable,intent(inout) :: vol_phi_phi_S(:,:,:,:)
    real   (kind=RKIND_POL),allocatable,intent(inout) :: vol_dphi_phi   (:,:,:)
    !! face matrices
    type(t_array3d_real)   ,allocatable,intent(inout) :: face_phi_xi(:,:)
    type(t_array5d_complex),allocatable,intent(inout) :: face_phi_xi_nCntau(:,:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    integer                     :: k, i, j
    integer                     :: n_dof
    !! models
    complex(kind=8),allocatable :: quad_S  (:,:,:)
    real   (kind=8),allocatable :: quad_rho(:)
    !! integrals
    real   (kind=8)             :: w_phi_phi, w_dphi_phi(3)
    complex(kind=8)             :: intvol_mass  (2)
    complex(kind=8)             :: intvol_mass_S(6,6)
    real   (kind=8)             :: intvol_dphi(3,1) !! one per dim
    integer                     :: iquad
    !! for face integral
    integer                     :: l, n_dof_l, iface
    !! geometry
    real(kind=8)                :: node_coo(3,4),xpt_gb(3),xpt_loc(3)
    !! array for the coefficients: separate Real and Imaginary
    !! size (3 faces, Nquad points, Narray)
    !! real   (kind=8),allocatable :: face_coeff_Re(:,:) 
    complex(kind=8),allocatable :: face_coeff_Cx(:,:,:,:)
    real   (kind=8)             :: intface_Re(4)     ! 4 faces, N array
    complex(kind=8)             :: intface_Cx(4,3,3) ! 4 faces, N array
    !! for continuity condition
    complex(kind=8),allocatable :: Cstiff_face(:,:,:)
    complex(kind=8),allocatable :: vp_face(:),vs_face(:)
    real   (kind=8),allocatable :: rho_face(:)
    real   (kind=8)             :: normals(3),An(3,6)
    complex(kind=8)             :: DCfaceD(6,6),loc(6,3),nCn(3,3)
    integer                     :: kdim,jdim

    ctx_err%ierr=0

    !! -----------------------------------------------------------------
    !! sanity checks
    if(.not. ctx_dg%flag_eval_int_quadrature) then
      ctx_err%msg   = "** ERROR: inconsistant use of quadrature rule "//&
                      "[build_quadrature]"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    !! -----------------------------------------------------------------
    !! evaluate the model parameters in all quadrature points
    !! the two coordinates of the axisymmetric are the followings:
    !!  \f$ z\f$: the height
    !!  \f$ \eta=\sqrt{x^2 + y^2} \f$.
    !! -----------------------------------------------------------------
    allocate(quad_S       (ctx_dg%quadGL_npts,6,6))
    allocate(quad_rho     (ctx_dg%quadGL_npts))
    !! -----------------------------------------------------------------
    quad_S       = 0.d0
    quad_rho     = 0.d0
    !! -----------------------------------------------------------------
    node_coo(:,:)= dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell)))

    !! -----------------------------------------------------------------
    !!
    !! read model parameters on the quadrature points 
    !! depending on the discretization.
    !! we need the global position of all of the quadrature points
    !! -----------------------------------------------------------------
    do iquad=1,ctx_dg%quadGL_npts
      xpt_loc= dble(ctx_dg%quadGL_pts(:,iquad))
      xpt_gb = matmul(dble(ctx_dg%jac(:,:,i_cell)),xpt_loc)             &
             + dble(node_coo(:,1))
      call model_eval_raw(model,model_RHO,i_cell,xpt_loc,xpt_gb,freq,   &
                      quad_rho(iquad),ctx_err)
      call model_eval_Sstiff(model,i_cell,xpt_loc,xpt_gb,freq,          &
                             quad_S(iquad,:,:),ctx_err)
    end do
    !! -----------------------------------------------------------------
    if(ctx_err%ierr .ne. 0) return

    !! -----------------------------------------------------------------
    !! Create local matrices, only the face matrix must adapted to
    !! the different order because of neighbors, otherwize, we
    !! actually only need the current cell order to deal with.
    !! -----------------------------------------------------------------
    k       = i_o_cell
    n_dof   = ctx_dg%n_dof_per_order(k)
    vol_phi_phi                     = 0.d0
    vol_phi_phi_rho                 = 0.d0
    vol_phi_phi_S                   = 0.d0
    vol_dphi_phi                    = 0.d0
    ! ----------------------------------------------------------------
    !!  VOLUME MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ! ----------------------------------------------------------------
    do i=1, n_dof ; do j=1, n_dof

      intvol_mass  (:)   = 0.d0
      intvol_mass_S(:,:) = 0.d0
      intvol_dphi  (:,:) = 0.d0

      do iquad=1,ctx_dg%quadGL_npts
        !! REMARK: **********************************************************
        !! ********** Everything is still computed on the reference element
        !! ********** we just incorporate a possibility for model variation
        !! ********** that is, the multiplication with det_jac for the local
        !! ********** element remains in the creation of matrix A and C.
        !! END REMARK: ******************************************************

        !! for the volumic integrals
        !! the precomputation already has the weight.
        w_phi_phi     = ctx_dg%quadGL_phi_phi_w(k)%array(i,j,iquad)
        w_dphi_phi(1) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,1)
        w_dphi_phi(2) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,2)
        w_dphi_phi(3) = ctx_dg%quadGL_dphi_phi_w(k)%array(i,j,iquad,3)
        

        intvol_mass( 1) = intvol_mass( 1) + w_phi_phi
        intvol_mass( 2) = intvol_mass( 2) + w_phi_phi * quad_rho(iquad)
        !! using the symmetry.
        intvol_mass_S   = intvol_mass_S   + w_phi_phi * quad_S(iquad,:,:)
        
        !!  derivatives
        intvol_dphi(:,1)  = intvol_dphi(:,1) + w_dphi_phi(:)
      end do
      
      !!     vol_matrix
      vol_phi_phi    (i,j)    =  real(intvol_mass( 1)   ,kind=RKIND_POL)
      vol_phi_phi_rho(i,j)    =  real(intvol_mass( 2)   ,kind=RKIND_POL)
      vol_phi_phi_S  (i,j,:,:)= cmplx(intvol_mass_S(:,:),kind=RKIND_POL)
      vol_dphi_phi   (i,j,:) =  real(intvol_dphi(:,1)   ,kind=RKIND_POL)
    end do ; end do

    !! -----------------------------------------------------------------
    !!  SURFACE MATRICES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !!  We only need to sum the pre-computed array and the
    !!  passage towards the local element is made, as usual, in the
    !!  creations of matrix A & B & L, etc.
    !! -----------------------------------------------------------------
    
    !! compute the penalization coeff on face once and for all.
    !! 4 tetra faces, 3x3 matrix
    allocate(face_coeff_Cx(4,ctx_dg%quadGL_face_npts,3,3))
    face_coeff_Cx = 0.d0
    
    !! loop over all faces to evaluate the beta_sigma and A first
    allocate(Cstiff_face(6,6,ctx_dg%quadGL_face_npts))
    allocate(vp_face        (ctx_dg%quadGL_face_npts))
    allocate(vs_face        (ctx_dg%quadGL_face_npts))
    allocate(rho_face       (ctx_dg%quadGL_face_npts))
    do iface=1,ctx_mesh%n_neigh_per_cell !! four tetra faces
      Cstiff_face   =0.d0
      call hdg_model_on_face_3D_quad(ctx_dg,ctx_mesh,model,i_cell,      &
                                     iface,freq,Cstiff_face,rho_face,   &
                                     vp_face,vs_face,ctx_err)
      !! compute the weights depending on the penalization chosen.
      select case(tag_penalization)
        !! -------------------------------------------------------------
        !! Identity with 1.0.
        !! -------------------------------------------------------------
        case(0) 
          do iquad=1,ctx_dg%quadGL_face_npts
            !! diagonal only
            face_coeff_Cx(iface,iquad,1,1) = 1.d0 !! with tau = 1.0
            face_coeff_Cx(iface,iquad,2,2) = 1.d0 !! with tau = 1.0
            face_coeff_Cx(iface,iquad,3,3) = 1.d0 !! with tau = 1.0
          end do
        !! -------------------------------------------------------------
        !! using Godunov formulation with - i omega
        !! -------------------------------------------------------------
        case(23) 
          normals(:) = dble(ctx_dg%normal(:,iface,i_cell))
          call compute_operator_A_3D  (normals,An)
          do iquad=1,ctx_dg%quadGL_face_npts
            !! compute n . C . n
            call compute_operator_DCD_3D(Cstiff_face(:,:,iquad),DCfaceD)
            loc = matmul(DCfaceD,transpose(An))
            nCn = matmul(An,loc)
            !! adjust with param
            face_coeff_Cx(iface,iquad,:,:) =  aimag(freq) * dcmplx(0.d0,-1.d0) &
             * rho_face(iquad)/(vp_face(iquad) + vs_face(iquad))        &
             *(vp_face(iquad)*vs_face(iquad)*Id3x3(:,:) + nCn/rho_face(iquad))
          end do

        case default !! unknown
          ctx_err%msg   = "** ERROR:unrecognized penalization format "//&
                          " [build_quadrature] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !! non-shared error
        end select
    end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! we need the second loop over the different orders ...............
    !! -----------------------------------------------------------------
    do k = 1, ctx_dg%n_different_order
      n_dof   = ctx_dg%n_dof_per_order(k)

      do l = 1, ctx_dg%n_different_order
        n_dof_l = ctx_dg%n_dof_per_order (l)

        face_phi_xi       (k,l)%array = 0.d0
        face_phi_xi_nCntau(k,l)%array = 0.d0
        do i=1, n_dof ; do j=1, n_dof_l
          intface_Re = 0.d0
          intface_Cx = 0.d0

          do iquad=1,ctx_dg%quadGL_face_npts
            !! weight is already taken into account, all faces at once here
            do iface=1,ctx_mesh%n_neigh_per_cell !! four tetra faces
              !! raw
              intface_Re(iface) = intface_Re(iface) +                   &
                   ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,iface)
              
              !! with coeffs complex
              do kdim=1,ctx_mesh%dim_domain ; do jdim=1,ctx_mesh%dim_domain
                intface_Cx(iface,kdim,jdim) = intface_Cx(iface,kdim,jdim)+ &
                               face_coeff_Cx(iface,iquad,kdim,jdim) *      &
                     ctx_dg%quadGL_face_phi_phi_w(k,l)%array(i,j,iquad,iface)
              enddo ; enddo
            end do
          end do
          !! update all faces.
          face_phi_xi(k,l)%array(i,j,:)=real(intface_Re(:),kind=RKIND_POL)
          do kdim=1,ctx_mesh%dim_domain ; do jdim=1,ctx_mesh%dim_domain
            face_phi_xi_nCntau(k,l)%array(i,j,:,kdim,jdim) =            &
                             cmplx(intface_Cx(:,kdim,jdim),kind=RKIND_POL)
          end do; end do
        end do ; end do

      end do !! end second loop on the different orders.
    end do !! end first loop on the different orders.

    deallocate(quad_rho     )
    deallocate(quad_S       ) 
    deallocate(face_coeff_Cx)
    deallocate(Cstiff_face  )
    deallocate(vp_face      )
    deallocate(vs_face      )
    deallocate(rho_face     )

    return
  end subroutine hdg_build_quadrature_int_3D

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Estimates models on the element face, this is used for the
  !> boundary conditions.
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_model_on_face_2D_quad(ctx_dg,ctx_mesh,model,i_cell,    &
                                       iface,freq,Cstiff,rho,vp,vs,     &
                                       ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer(kind=4)                    ,intent(in)    :: iface
    complex(kind=8)                    ,intent(in)    :: freq
    complex(kind=8)                    ,intent(out)   :: Cstiff(:,:,:)
    real   (kind=8)                    ,intent(out)   :: rho(:)
    complex(kind=8)                    ,intent(out)   :: vp(:)
    complex(kind=8)                    ,intent(out)   :: vs(:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    real(kind=8), allocatable   :: quad_2dposition_gb (:,:)
    real(kind=8), allocatable   :: quad_2dposition_loc(:,:)
    integer                     :: iquad
    logical     , parameter     :: flag_lame = .false.

    ctx_err%ierr=0

    !! convert the face quadrature positions to the 2D global mesh
    allocate(quad_2dposition_gb (2,ctx_dg%quadGL_face_npts))
    allocate(quad_2dposition_loc(2,ctx_dg%quadGL_face_npts))
    quad_2dposition_gb =0.d0
    quad_2dposition_loc=0.d0
    call mesh_convert_face_position_2D(                         &
         !! coordinates of the nodes of the triangle
         dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
         !! face index and inverse jacobian
         iface,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
         !! all the positions to treat
         dble(ctx_dg%quadGL_face_pts(1,:)),                     &
         ctx_dg%quadGL_face_npts,                               &
         !! global and local positions.
         quad_2dposition_gb,quad_2dposition_loc,ctx_err)
             
    !! =================================================================
    !! loop over all quad face points
    !! =================================================================
    Cstiff  = 0.d0
    vp      = 0.d0
    vs      = 0.d0
    rho     = 0.d0
    do iquad=1,ctx_dg%quadGL_face_npts

      !! evaluate C
      call model_eval_Cstiff(model,i_cell,quad_2dposition_loc(:,iquad), &
                             quad_2dposition_gb(:,iquad),freq,          &
                             Cstiff(:,:,iquad),ctx_err)
      !! evaluate rho vp vs
      call model_eval_cx(model,i_cell,quad_2dposition_loc(:,iquad),     &
                         quad_2dposition_gb(:,iquad),freq,flag_lame,    &
                         vp(iquad),vs(iquad),rho(iquad),ctx_err)
    end do
    
    deallocate(quad_2dposition_loc)
    deallocate(quad_2dposition_gb )
    return

  end subroutine hdg_model_on_face_2D_quad

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Estimates models on the middle of the element face, 
  !> this is used for the boundary conditions.
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_model_on_midface_2D(ctx_dg,ctx_mesh,model,i_cell,iface,&
                                     freq,rho,vp,vs,ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer(kind=4)                    ,intent(in)    :: iface
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=8)                    ,intent(out)   :: rho
    complex(kind=8)                    ,intent(out)   :: vp
    complex(kind=8)                    ,intent(out)   :: vs
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    real(kind=8)                :: xpos_face
    real(kind=8)                :: quad_2dposition_gb (2)
    real(kind=8)                :: quad_2dposition_loc(2)
    logical     , parameter     :: flag_lame = .false.

    ctx_err%ierr=0

    !! =================================================================
    !! loop over all quad face points
    !! =================================================================
    vp      = 0.d0
    vs      = 0.d0
    rho     = 0.d0

    !! we take the value in the middle of the face
    !! it corresponds to 1D face position 0.50
    xpos_face = 0.5d0
    !! convert the face quadrature positions to the 2D global mesh
    quad_2dposition_gb =0.d0
    quad_2dposition_loc=0.d0
    call mesh_convert_face_position_2D(                         &
         !! coordinates of the nodes of the triangle
         dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
         !! face index and inverse jacobian
         iface,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
         !! position to treat
         xpos_face,                                             &
         !! global and local positions.
         quad_2dposition_gb,quad_2dposition_loc,ctx_err)

    
    !! evaluate rho vp vs
    call model_eval_cx(model,i_cell,quad_2dposition_loc,                &
                       quad_2dposition_gb,freq,flag_lame,vp,vs,         &
                       rho,ctx_err)
    return

  end subroutine hdg_model_on_midface_2D



  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Estimates models on the element face, this is used for the
  !> boundary conditions.
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_model_on_face_3D_quad(ctx_dg,ctx_mesh,model,i_cell,    &
                                       iface,freq,Cstiff,rho,vp,vs,     &
                                       ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer(kind=4)                    ,intent(in)    :: iface
    complex(kind=8)                    ,intent(in)    :: freq
    complex(kind=8)                    ,intent(out)   :: Cstiff(:,:,:)
    real   (kind=8)                    ,intent(out)   :: rho(:)
    complex(kind=8)                    ,intent(out)   :: vp(:)
    complex(kind=8)                    ,intent(out)   :: vs(:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    real(kind=8), allocatable   :: quad_3dposition_gb (:,:)
    real(kind=8), allocatable   :: quad_3dposition_loc(:,:)
    integer                     :: iquad
    logical     , parameter     :: flag_lame = .false.

    ctx_err%ierr=0

    !! =================================================================
    !! loop over all quad face points
    !! =================================================================
    Cstiff  = 0.d0
    vp      = 0.d0
    vs      = 0.d0
    rho     = 0.d0
    
    !! convert the face quadrature positions to the 2D global mesh
    allocate(quad_3dposition_gb (3,ctx_dg%quadGL_face_npts))
    allocate(quad_3dposition_loc(3,ctx_dg%quadGL_face_npts))
    quad_3dposition_gb =0.d0
    quad_3dposition_loc=0.d0
    call mesh_convert_face_position_3D(                                 &
                 !! coordinates of the nodes of the tetra
                 dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
                 !! face index and inverse jacobian
                 iface,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
                 !! all the positions to treat
                 dble(ctx_dg%quadGL_face_pts(:,:)),                     &
                 ctx_dg%quadGL_face_npts,                               &
                 !! global and local positions.
                 quad_3dposition_gb,quad_3dposition_loc,ctx_err)

    do iquad=1,ctx_dg%quadGL_face_npts
      !! evaluate C
      call model_eval_Cstiff(model,i_cell,quad_3dposition_loc(:,iquad), &
                             quad_3dposition_gb(:,iquad),freq,          &
                             Cstiff(:,:,iquad),ctx_err)
      !! evaluate rho vp vs
      call model_eval_cx(model,i_cell,quad_3dposition_loc(:,iquad),     &
                         quad_3dposition_gb(:,iquad),freq,flag_lame,    &
                         vp(iquad),vs(iquad),rho(iquad),ctx_err)
    end do
    deallocate(quad_3dposition_loc)
    deallocate(quad_3dposition_gb )
    return

  end subroutine hdg_model_on_face_3D_quad

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Estimates models on the middle of the element face, 
  !> this is used for the boundary conditions.
  !>
  !> @param[in]    ctx_dg            : DG context
  !> @param[in]    ctx_mesh          : mesh context
  !> @param[in]    ctx_model         : model parameters context
  !> @param[in]    i_cell            : current cell
  !> @param[out]   ctx_err           : error context
  !---------------------------------------------------------------------
  subroutine hdg_model_on_midface_3D(ctx_dg,ctx_mesh,model,i_cell,iface,&
                                     freq,rho,vp,vs,ctx_err)
    implicit none

    type(t_discretization)             ,intent(in)    :: ctx_dg
    type(t_domain)                     ,intent(in)    :: ctx_mesh
    type(t_model)                      ,intent(in)    :: model
    integer(kind=IKIND_MESH)           ,intent(in)    :: i_cell
    integer(kind=4)                    ,intent(in)    :: iface
    complex(kind=8)                    ,intent(in)    :: freq
    real   (kind=8)                    ,intent(out)   :: rho
    complex(kind=8)                    ,intent(out)   :: vp
    complex(kind=8)                    ,intent(out)   :: vs
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    real(kind=8)                :: xpos_face(2)
    real(kind=8)                :: quad_3dposition_gb (3)
    real(kind=8)                :: quad_3dposition_loc(3)
    logical     , parameter     :: flag_lame = .false.

    ctx_err%ierr=0

    !! =================================================================
    vp      = 0.d0
    vs      = 0.d0
    rho     = 0.d0

    !! it corresponds to barycenter of the 2D face
    xpos_face(1) = 1.d0/3.d0
    xpos_face(2) = 1.d0/3.d0
    quad_3dposition_gb =0.d0
    quad_3dposition_loc=0.d0
    call mesh_convert_face_position_3D(                                 &
                 !! coordinates of the nodes of the tetra
                 dble(ctx_mesh%x_node(:,ctx_mesh%cell_node(:,i_cell))), &
                 !! face index and inverse jacobian
                 iface,dble(ctx_dg%inv_jac(:,:,i_cell)),                &
                 !! position to treat
                 xpos_face,                                             &
                 !! global and local positions.
                 quad_3dposition_gb,quad_3dposition_loc,ctx_err)
                 
    !! evaluate rho vp vs
    call model_eval_cx(model,i_cell,quad_3dposition_loc,                &
                       quad_3dposition_gb,freq,flag_lame,vp,vs,         &
                       rho,ctx_err)
    return

  end subroutine hdg_model_on_midface_3D

end module m_matrix_utils
