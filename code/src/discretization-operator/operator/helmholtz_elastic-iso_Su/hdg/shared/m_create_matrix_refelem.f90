!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_create_matrix_refelem.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to create the matrix for
!> HELMHOLTZ ELASTIC ISOTROPIC propagator using the 
!> HDG discretization
!
!------------------------------------------------------------------------------
module m_create_matrix_refelem

  !! module used -------------------------------------------------------
  use omp_lib
  use, intrinsic                   :: ieee_arithmetic
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: create_matrix_refelem_2D

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> return an error, we cannot use reference element for now
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine create_matrix_refelem_2D(ctx_err)

    implicit none

    type(t_error)     ,intent(inout)   :: ctx_err

    ctx_err%msg   = "** ERROR: the reference element method " //        &
                    "cannot be used for now with the elastic " //       &
                    "problem, it must use quadrature rules."
    ctx_err%ierr  = -1
    return !! send upwards
  end subroutine create_matrix_refelem_2D

end module m_create_matrix_refelem
