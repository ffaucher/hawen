!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_eval.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to evaluate the model coefficients, in particular
!> to deal with piecewise-polynomials representation and with attenuation
!>
!
!------------------------------------------------------------------------------
module m_model_eval

  !! module used -------------------------------------------------------
  use m_raise_error,                 only: raise_error, t_error
  use m_define_precision,            only: IKIND_MESH, RKIND_POL
  use m_ctx_domain,                  only: t_domain
  use m_ctx_model,                   only: t_model
  use m_model_list
  use m_polynomial,                  only: polynomial_eval, polynomial_eval_array
  use m_dg_lagrange_dof_coordinates, only: discretization_dof_coo_ref_elem
  use m_ctx_model_representation,    only: tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                           tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_ctx_model_representation,    only: model_representation_eval_sep
  use m_model_representation_dof,    only: model_representation_dof_eval_refelem
  use m_lapack_inverse,              only: lapack_matrix_inverse
  !! -------------------------------------------------------------------

  implicit none

  !! the maximal number of models needed for viscosity 
  integer, parameter :: N_VISCO_MAX = 5 !! 5 at most with fractional Zener.

  !! transposed because fortran is column wise
  !! -------------------------------------------------------------------
  !! we have
  !!   Voigt3D = 1     0     0     0     0     0
  !!   Voigt3D = 0     1     0     0     0     0
  !!   Voigt3D = 0     0     1     0     0     0
  !!   Voigt3D = 0     0     0     2     0     0
  !!   Voigt3D = 0     0     0     0     2     0
  !!   Voigt3D = 0     0     0     0     0     2
  !! -------------------------------------------------------------------
  real   (kind=8), parameter            :: Voigt2D(3,3) =             &
                      transpose(reshape((/ 1.d0, 0.d0, 0.d0,          &
                                           0.d0, 1.d0, 0.d0,          &
                                           0.d0, 0.d0, 2.d0 /),       &
                                           (/3,3/)   ))
  real   (kind=8), parameter            :: Voigt3D(6,6) =               &
                      transpose(reshape((/ 1.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0, &
                                           0.d0, 1.d0, 0.d0, 0.d0, 0.d0, 0.d0, &
                                           0.d0, 0.d0, 1.d0, 0.d0, 0.d0, 0.d0, &
                                           0.d0, 0.d0, 0.d0, 2.d0, 0.d0, 0.d0, & 
                                           0.d0, 0.d0, 0.d0, 0.d0, 2.d0, 0.d0, & 
                                           0.d0, 0.d0, 0.d0, 0.d0, 0.d0, 2.d0/),&
                                           (/6,6/)   ))
  real   (kind=8), parameter            :: Voigt2Dinv(3,3) =          &
                      transpose(reshape((/ 1.d0, 0.d0, 0.d0,          &
                                           0.d0, 1.d0, 0.d0,          &
                                           0.d0, 0.d0, 0.50d0 /),     &
                                           (/3,3/)   ))
  real   (kind=8), parameter            :: Voigt3Dinv(6,6) =                   &
                      transpose(reshape((/ 1.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0, &
                                           0.d0, 1.d0, 0.d0, 0.d0, 0.d0, 0.d0, &
                                           0.d0, 0.d0, 1.d0, 0.d0, 0.d0, 0.d0, &
                                           0.d0, 0.d0, 0.d0, 0.5d0,0.d0, 0.d0, & 
                                           0.d0, 0.d0, 0.d0, 0.d0, 0.5d0,0.d0, & 
                                           0.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.5d0/),&
                                           (/6,6/)   ))

  private

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_cx
     !! evaluate the complex-valued parameters associated with lambda,
     !! mu, rho, or cp, cs and rho. It takes into account the visco-
     !! elastic model
     module procedure model_eval_all_multi
     module procedure model_eval_all_solo
  end interface model_eval_cx
  interface model_eval_raw
     !! evaluate the real-valued parameters lambda_0,
     !! mu_0, rho, or cp_0, cs_0 and rho_0. 
     !! It does not take into account the visco-elastic model !!
     module procedure model_eval_raw_solo
     module procedure model_eval_raw_multi
  end interface model_eval_raw
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation
  interface model_eval_Cstiff
     module procedure model_eval_C_solo
     module procedure model_eval_C_multi
  end interface model_eval_Cstiff
  interface model_eval_Sstiff
     module procedure model_eval_S_solo
     module procedure model_eval_S_multi
  end interface model_eval_Sstiff
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise constant representation
  interface model_get_piecewise_constant
     module procedure model_get_pconstant_real4
     module procedure model_get_pconstant_real8
  end interface model_get_piecewise_constant

  !--------------------------------------------------------------------- 
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model evaluation for dof representation
  interface model_eval_dof_index
     module procedure model_eval_dof_raw_solo_idof
     module procedure model_eval_dof_raw_solo_idof_cx
  end interface model_eval_dof_index
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the model for dof representation
  interface model_get_pdof
     module procedure model_get_pdof_real4
     module procedure model_get_pdof_real8
  end interface model_get_pdof
  !--------------------------------------------------------------------- 

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise polynomial representation
  interface model_get_piecewise_polynomial
     module procedure model_get_ppoly_real4
     module procedure model_get_ppoly_real8
  end interface model_get_piecewise_polynomial
  !--------------------------------------------------------------------- 

  public  :: model_eval_cx,model_eval_raw,model_get_piecewise_constant
  public  :: model_eval_dof_index,model_eval_Cstiff,model_eval_Sstiff
  public  :: model_get_pdof,model_get_piecewise_polynomial
  public  :: compute_operator_A_2D   , compute_operator_A_3D
  public  :: compute_operator_Adag_2D, compute_operator_Adag_3D
  public  :: compute_operator_DCD_2D, compute_operator_DCD_3D
  
  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the saved models, vp, vs rho depending on the model 
  !> representation: piecewise-constant, piecewise polynomial, etc. 
  !> Here we just get the main, real models and separate the viscous
  !> terms, that is, we do not build the complex-valued wave speeds.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, for viscous models.
  !> @param[out]   vp         : output vp
  !> @param[out]   vs         : output vs
  !> @param[out]   rho        : output rho
  !> @param[out]   visco      : output visco models
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_with_format(ctx_model,i_cell,xpt_refelem,       &
                                    xpt_gb,vp,vs,rho,visco,ctx_err,     &
                                    o_force_format)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:)
    real   (kind=8)         ,intent(in)   :: xpt_gb     (:)
    real   (kind=8)         ,intent(out)  :: vp
    real   (kind=8)         ,intent(out)  :: vs
    real   (kind=8)         ,intent(out)  :: rho
    real   (kind=8)         ,intent(out)  :: visco(N_VISCO_MAX)
    type(t_error)           ,intent(inout):: ctx_err
    character(len=*),optional,intent(in)  :: o_force_format
    !!
    real  (kind=RKIND_POL), allocatable   :: xloc(:)
    character(len=64)                     :: format_model
    !! -----------------------------------------------------------------
    
    vp   =0.d0
    vs   =0.d0
    rho  =0.d0
    visco=0.d0
    
    if(present(o_force_format)) then
      format_model = trim(adjustl(o_force_format))
    else 
      format_model = trim(adjustl(ctx_model%format_disc))
    end if

    !! start with Vp, Vs, Rho and check attenuation ====================
    select case(trim(adjustl(format_model)))
      !! ---------------------------------------------------------------
      case(tag_MODEL_PCONSTANT) ! piecewise-constant -------------------
        vp = dble(ctx_model%vp%pconstant (i_cell))
        vs = dble(ctx_model%vs%pconstant (i_cell))
        rho= dble(ctx_model%rho%pconstant(i_cell))
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw)
              visco(1) = dble(ctx_model%visco(1)%pconstant(i_cell)) 
              visco(2) = dble(ctx_model%visco(2)%pconstant(i_cell))
            case(model_visco_Zener)
              visco(1) = dble(ctx_model%visco(1)%pconstant(i_cell)) 
              visco(2) = dble(ctx_model%visco(2)%pconstant(i_cell))
              visco(3) = dble(ctx_model%visco(3)%pconstant(i_cell)) 
              visco(4) = dble(ctx_model%visco(4)%pconstant(i_cell)) 
            case(model_visco_ZenerFrac)
              visco(1) = dble(ctx_model%visco(1)%pconstant(i_cell)) 
              visco(2) = dble(ctx_model%visco(2)%pconstant(i_cell))
              visco(3) = dble(ctx_model%visco(3)%pconstant(i_cell)) 
              visco(4) = dble(ctx_model%visco(4)%pconstant(i_cell)) 
              visco(5) = dble(ctx_model%visco(5)%pconstant(i_cell)) 
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model"//&
                              " [model_eval_format] **"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        endif

      !! ---------------------------------------------------------------
      case(tag_MODEL_PPOLY)    ! piecewise-polynomial ------------------
        allocate(xloc(ctx_model%dim_domain))
        xloc = real(xpt_refelem,kind=RKIND_POL) !! ref elem position. 
        call polynomial_eval(ctx_model%vp%ppoly(i_cell) ,xloc,vp,ctx_err)
        call polynomial_eval(ctx_model%vs%ppoly(i_cell) ,xloc,vs,ctx_err)
        call polynomial_eval(ctx_model%rho%ppoly(i_cell),xloc,rho,ctx_err)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw)
              call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),    &
                                   xloc,visco(1),ctx_err)
              call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),    &
                                   xloc,visco(2),ctx_err)
            case(model_visco_Zener)
              call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),    &
                                   xloc,visco(1),ctx_err)
              call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),    &
                                   xloc,visco(2),ctx_err)
              call polynomial_eval(ctx_model%visco(3)%ppoly(i_cell),    &
                                   xloc,visco(3),ctx_err)
              call polynomial_eval(ctx_model%visco(4)%ppoly(i_cell),    &
                                   xloc,visco(4),ctx_err)
            case(model_visco_ZenerFrac)
              call polynomial_eval(ctx_model%visco(1)%ppoly(i_cell),    &
                                   xloc,visco(1),ctx_err)
              call polynomial_eval(ctx_model%visco(2)%ppoly(i_cell),    &
                                   xloc,visco(2),ctx_err)
              call polynomial_eval(ctx_model%visco(3)%ppoly(i_cell),    &
                                   xloc,visco(3),ctx_err)
              call polynomial_eval(ctx_model%visco(4)%ppoly(i_cell),    &
                                   xloc,visco(4),ctx_err)
              call polynomial_eval(ctx_model%visco(5)%ppoly(i_cell),    &
                                   xloc,visco(5),ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model"//&
                              " [model_eval_format] **"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        endif
        deallocate(xloc)

      !! ---------------------------------------------------------------
      case(tag_MODEL_SEP)      ! sep format ----------------------------
        !! using global position
        call model_representation_eval_sep(ctx_model%vp%param_sep,      &
                                           ctx_model%dim_domain,xpt_gb, &
                                           vp,ctx_err)
        call model_representation_eval_sep(ctx_model%vs%param_sep,      &
                                           ctx_model%dim_domain,xpt_gb, &
                                           vs, &
                                           ctx_err)
        call model_representation_eval_sep(ctx_model%rho%param_sep,     &
                                           ctx_model%dim_domain,xpt_gb, &
                                           rho,&
                                           ctx_err)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw)
              call model_representation_eval_sep(ctx_model%visco(1)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(1),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(2)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(2),  &
                                        ctx_err)
            case(model_visco_Zener)
              call model_representation_eval_sep(ctx_model%visco(1)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(1),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(2)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(2),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(3)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(3),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(4)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(4),  &
                                        ctx_err)

            case(model_visco_ZenerFrac)
              call model_representation_eval_sep(ctx_model%visco(1)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(1),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(2)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(2),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(3)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(3),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(4)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(4),  &
                                        ctx_err)
              call model_representation_eval_sep(ctx_model%visco(5)%param_sep, &
                                        ctx_model%dim_domain,xpt_gb,visco(5),  &
                                        ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model"//&
                              " [model_eval_format] **"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        endif
    
      !! ---------------------------------------------------------------
      case(tag_MODEL_DOF)      ! dof format ----------------------------
        call model_representation_dof_eval_refelem(ctx_model%vp,i_cell,  &
                                                   xpt_refelem,vp,ctx_err)
        call model_representation_dof_eval_refelem(ctx_model%vs,i_cell,  &
                                                   xpt_refelem,vs,ctx_err)
        call model_representation_dof_eval_refelem(ctx_model%rho,i_cell, &
                                                   xpt_refelem,rho,ctx_err)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw)
              call model_representation_dof_eval_refelem(ctx_model%visco(1),&
                                         i_cell,xpt_refelem,visco(1),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(2),&
                                         i_cell,xpt_refelem,visco(2),ctx_err)
            case(model_visco_Zener)
              call model_representation_dof_eval_refelem(ctx_model%visco(1),&
                                         i_cell,xpt_refelem,visco(1),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(2),&
                                         i_cell,xpt_refelem,visco(2),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(3),&
                                         i_cell,xpt_refelem,visco(3),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(4),&
                                         i_cell,xpt_refelem,visco(4),ctx_err)
            case(model_visco_ZenerFrac)
              call model_representation_dof_eval_refelem(ctx_model%visco(1),&
                                         i_cell,xpt_refelem,visco(1),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(2),&
                                         i_cell,xpt_refelem,visco(2),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(3),&
                                         i_cell,xpt_refelem,visco(3),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(4),&
                                         i_cell,xpt_refelem,visco(4),ctx_err)
              call model_representation_dof_eval_refelem(ctx_model%visco(5),&
                                         i_cell,xpt_refelem,visco(5),ctx_err)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model"//&
                              " [model_eval_format] **"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        endif
      
      case default
        ctx_err%msg   = "** ERROR:unrecognized model parameterization"//&
                        " [model_eval_format] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    return
    
  end subroutine model_eval_with_format

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the complex-valued models for viscoelasticiy
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    vp         : input real vp
  !> @param[in]    vs         : input real vs
  !> @param[in]    rho        : input rho
  !> @param[in]    visco      : input viscous models
  !> @param[in]    freq       : frequency, for viscous models.
  !> @param[out]   vp_cx      : complex vp
  !> @param[out]   vs_cx      : complex vs
  !> @param[out]   lambda_cx  : complex lambda
  !> @param[out]   mu_cx      : complex mu
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,    &
                                          freq,vp_cx,vs_cx,lambda_cx,   &
                                          mu_cx,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    real   (kind=8)         ,intent(in)   :: vp,vs,rho
    real   (kind=8)         ,intent(in)   :: visco(:)
    complex(kind=8)         ,intent(in)   :: freq
    complex(kind=8)         ,intent(out)  :: vp_cx
    complex(kind=8)         ,intent(out)  :: vs_cx
    complex(kind=8)         ,intent(out)  :: lambda_cx
    complex(kind=8)         ,intent(out)  :: mu_cx
    type(t_error)           ,intent(inout):: ctx_err
    !!
    integer          :: dim_domain
    real   (kind=8)  :: lambda_r,mu_r,eta_l,eta_m,tausig,taueps,fpower
    complex(kind=8)  :: alpha
    !! -----------------------------------------------------------------

    !! build real parts of lambda/mu from the input vp/vs
    mu_r    = rho*vs*vs
    lambda_r= rho*(vp*vp - 2.d0*vs*vs)

    !! initialize complex outputs
    vp_cx     = dcmplx(vp      , 0.d0)
    vs_cx     = dcmplx(vs      , 0.d0)
    lambda_cx = dcmplx(lambda_r, 0.d0)
    mu_cx     = dcmplx(mu_r    , 0.d0)

    !! adjust attenuation
    if(ctx_model%flag_viscous) then

      select case(trim(adjustl(ctx_model%viscous_model)))
      
        case(model_visco_KV)
          lambda_cx=dcmplx(lambda_r - freq*visco(1))
          mu_cx    =dcmplx(    mu_r - freq*visco(2))

        case(model_visco_Maxw)
          !! the attenuation is applied onto lambda & mu ...............
          dim_domain = ctx_model%dim_domain
          !! Maxwell model gives is a little technical as it involved
          !! the inverse of the tensors. we refer to the viscoelastic
          !! paper for the details
          eta_l =  visco(1)
          eta_m =  visco(2)
          mu_cx = dcmplx(- freq * mu_r * eta_m / (mu_r - freq * eta_m))
          alpha = dcmplx(- eta_l/(2.d0*eta_m*(dim_domain*eta_l + 2.d0*eta_m))  &
                  + freq*lambda_r/ (2.d0*mu_r*(dim_domain*lambda_r + 2.d0*mu_r)))
          lambda_cx = dcmplx(- 2.d0 * mu_cx / dim_domain                &
                      - 2.d0 * freq * mu_cx                             &
                      / (2.d0*(dim_domain**2)*alpha*mu_cx - freq*dim_domain))
        case(model_visco_Zener)
          !! the attenuation is applied onto lambda & mu ...............
          eta_l   =  visco(1)
          eta_m   =  visco(2)
          tausig  =  visco(3)
          taueps  =  visco(4)

          !! the Zener model writes as 
          !! (1 - i \omega \taus) \sigma = (C - i \omega \taue \eta) \epsilon.
          !! ==>
          !! C :=  \dfrac{C  - i \omega \taue \eta }{1 - i \omega \taus}
          if(taueps*eta_l < tausig*lambda_r) then
            ctx_err%msg   = "** ERROR: value for relaxation times of "//&
                            "epsilon for lambda must be higher than " //&
                            "or equal to those for sigma [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          if(taueps*eta_m < tausig*mu_r) then
            ctx_err%msg   = "** ERROR: value for relaxation times of "//&
                            "epsilon for mu must be higher than "     //&
                            "or equal to those for sigma [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          
          lambda_cx= dcmplx((lambda_r - freq*taueps*eta_l)/(1.d0-freq*tausig))
          mu_cx    = dcmplx((mu_r     - freq*taueps*eta_m)/(1.d0-freq*tausig))

        case(model_visco_ZenerFrac)
          !! the attenuation is applied onto lambda & mu ...............
          eta_l   =  visco(1)
          eta_m   =  visco(2)
          tausig  =  visco(3)
          taueps  =  visco(4)
          fpower  =  visco(5)

          !! the Zener model writes as 
          !! (1 - i \omega \taus) \sigma = (C - i \omega \taue \eta) \epsilon.
          !! ==>
          !! C :=  \dfrac{C  - i \omega \taue \eta }{1 - i \omega \taus}
          if(taueps*eta_l < tausig*lambda_r) then
            ctx_err%msg   = "** ERROR: value for relaxation times of "//&
                            "epsilon for lambda must be higher than " //&
                            "or equal to those for sigma [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          if(taueps*eta_m < tausig*mu_r) then
            ctx_err%msg   = "** ERROR: value for relaxation times of "//&
                            "epsilon for mu must be higher than "     //&
                            "or equal to those for sigma [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          if(fpower < 0 .or. fpower > 1) then
            ctx_err%msg   = "** ERROR: value for power argument "//&
                            "must be higher than or equal to 0  "//&
                            " [model_eval] "
            ctx_err%ierr  = -1
            return
          end if
          
          !! very close to the acoustic Cole-Cole model.................
          lambda_cx= dcmplx( (lambda_r + taueps*eta_l*(-freq)**fpower)  &
                         /   (1.d0 + (-freq)**fpower * tausig))
          mu_cx    = dcmplx((mu_r      + taueps*eta_m*(-freq)**fpower)   &
                         /   (1.d0 + (-freq)**fpower *tausig))
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error
        end select

        !! adjust complex wave-speeds ----------------------------------
        vp_cx = sqrt((lambda_cx + 2.d0*mu_cx)/rho)
        vs_cx = sqrt(mu_cx/rho)

    end if  
 
    !! sanity check ----------------------------------------------------
    if(real(vp_cx) <= 0.d0 .or. real(vs_cx) < 0.d0 .or. rho .le. 0.d0 .or. &
       real(lambda_cx) <= 0.d0 .or. real(mu_cx) < 0.d0) then
      ctx_err%msg   = "** ERROR: value for one of the physical "       //  &
                      "parameters (vp, vs, lambda or mu) is <= 0 "     //  &
                      "for elastic medium [model_eval] "
      ctx_err%ierr  = -1
    end if
    
    return

  end subroutine model_eval_viscoelastic_main

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the raw models, e.g., vp, vs, rho, etc.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, for viscous models.
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_raw_solo(ctx_model,strmodel,i_cell,xpt_refelem, &
                                 xpt_gb,freq,out_val,ctx_err,           &
                                 o_force_format)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:)
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    character(len=*),optional,intent(in)  :: o_force_format
    !!
    real   (kind=8)  :: vp, vs, rho, visco(N_VISCO_MAX) ! 4 at most (Zener)
    real   (kind=8)  :: lambda, mu
    complex(kind=8)  :: vp_cx, vs_cx, lambda_cx, mu_cx
    !! -----------------------------------------------------------------

    out_val = 0.d0 
    
    !! -----------------------------------------------------------------
    !! 1) evaluate the real vp, vs, rho depending on the parametrization
    !! -----------------------------------------------------------------
    if(present(o_force_format)) then
      call  model_eval_with_format(ctx_model,i_cell,xpt_refelem,xpt_gb, &
                                   vp,vs,rho,visco,ctx_err,             &
                                   o_force_format=trim(adjustl(o_force_format)))
    else
      call  model_eval_with_format(ctx_model,i_cell,xpt_refelem,xpt_gb, &
                                   vp,vs,rho,visco,ctx_err)
    end if
    mu     = rho*(vs**2)
    lambda = rho*(vp**2) - 2.d0*mu
    
    !! -----------------------------------------------------------------
    !! 2) We do not use the visco-elastic model here, we just get 
    !!    the "raw", parameters, not the real part of the complex-
    !!    valued ones.
    !! -----------------------------------------------------------------
    !! call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,freq,   &
    !!                                   vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)
    !! 
    !! -----------------------------------------------------------------
    !! 3) get output values
    select case(trim(adjustl(strmodel)))
      case(model_VP      )
        out_val = dble(vp)
      case(model_VS      )
        out_val = dble(vs)
      case(model_RHO     )
        out_val = rho

      case(model_QFACTORP)
        !! the quality factor is given by the real part divided by the
        !! imaginary part, for P-wave speed.
        if(.not. ctx_model%flag_viscous) then
          ctx_err%msg   = "** ERROR: The quality factor is only valid"//&
                          " for viscous propagation, it tends to "    //&
                          "infinity otherwise. [eval] **"
          ctx_err%ierr  = -1
          return !! non-shared error
        else  
          call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,  &
                                freq,vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)
          out_val =-real(vp_cx**2)/aimag(vp_cx**2)
        end if

      case(model_QFACTORS)
        !! the quality factor is given by the real part divided by the
        !! imaginary part, for P-wave speed.
        if(.not. ctx_model%flag_viscous) then
          ctx_err%msg   = "** ERROR: The quality factor is only valid"//&
                          " for viscous propagation, it tends to "    //&
                          "infinity otherwise. [eval] **"
          ctx_err%ierr  = -1
          return !! non-shared error
        else
          call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,  &
                                freq,vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)
          out_val =-real(vs_cx**2)/aimag(vs_cx**2)
        end if
      case(model_ETA_LDA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw,model_visco_Zener,     &
                 model_visco_ZenerFrac)
              out_val = visco(1)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if  

      case(model_ETA_MU   )
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw,model_visco_Zener,     &
                 model_visco_ZenerFrac)
              out_val = visco(2)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_TAU_SIGMA)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_Zener,model_visco_ZenerFrac)
              out_val = visco(3)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_Zener,model_visco_ZenerFrac)
              out_val = visco(4)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_FPOWER)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_ZenerFrac)
              out_val = visco(5)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_LAMBDA  )
        out_val = dble(lambda)

      case(model_MU      )
        out_val = dble(mu)
      
      case(model_BULK    )
        select case(ctx_model%dim_domain)
          case(2) !! cf. [Masson & Pride] paper
            out_val = dble(lambda + mu)
          case(3)
            out_val = dble(lambda + (2.d0/3.d0)*mu)
          case default
            ctx_err%msg   = "** ERROR: incorrect dimension [model_eval] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select

      case(model_POISSON )
        out_val = dble(lambda / (2*(lambda + mu)))
      
      case(model_IP      )
        out_val = dble(rho * vp)

      case(model_IS      )
        out_val = dble(rho * vs)
      
      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! attenuation can be <= 0.
    ! if(out_val < 0.d0) then
    !   ctx_err%msg   = "** ERROR: value for the physical parameters " // &
    !                   "is <= 0 for elastic medium [model_eval] "
    !   ctx_err%ierr  = -1
    ! end if

    return

  end subroutine model_eval_raw_solo
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the raw models, e.g., vp, vs, rho, etc.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, for viscous models.
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_raw_multi(ctx_model,strmodel,i_cell,xpt_refelem, &
                                  xpt_gb,npt,freq,out_val,ctx_err,       &
                                  o_force_format)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:,:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:,:)
    integer                 ,intent(in)   :: npt
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_val(:)
    type(t_error)           ,intent(inout):: ctx_err
    character(len=*),optional,intent(in)  :: o_force_format
    !!
    integer :: k
    !! -----------------------------------------------------------------

    out_val = 0.d0 
    if(present(o_force_format)) then
      do k=1,npt
        call model_eval_raw_solo(ctx_model,trim(adjustl(strmodel)),     &
                                 i_cell,xpt_refelem(:,k),xpt_gb(:,k),   &
                                 freq,out_val(k),ctx_err,o_force_format=&
                                 trim(adjustl(o_force_format)))
        if(ctx_err%ierr .ne. 0) return
      end do
    else
      do k=1,npt
        call model_eval_raw_solo(ctx_model,trim(adjustl(strmodel)),     &
                                 i_cell,xpt_refelem(:,k),xpt_gb(:,k),   &
                                 freq,out_val(k),ctx_err)
        if(ctx_err%ierr .ne. 0) return
      end do
    end if
    
    return

  end subroutine model_eval_raw_multi

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate all of the complex-valued models, with an option to either
  !> give the wave-speeds out, or the Lamé parameters.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   flag_lame  : flag for lamé (true) or speeds by default
  !> @param[out]   param1     : output value
  !> @param[out]   param2     : output value
  !> @param[out]   rho        : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_all_solo(ctx_model,i_cell,xpt_refelem,xpt_gb,   &
                                 freq,flag_lame,param1,param2,rho,      &
                                 ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:)
    logical                 ,intent(in)   :: flag_lame
    complex(kind=8)         ,intent(out)  :: param1
    complex(kind=8)         ,intent(out)  :: param2
    real   (kind=8)         ,intent(out)  :: rho
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    complex(kind=8)                       :: lambda_cx, mu_cx, vp_cx, vs_cx
    real   (kind=8)                       :: vp,vs
    real   (kind=8)                       :: visco(N_VISCO_MAX) ! Zener
    !! -----------------------------------------------------------------

    param1 = 0.d0
    param2 = 0.d0
    
    !! -----------------------------------------------------------------
    !! 1) evaluate the real vp, vs, rho depending on the parametrization
    !! -----------------------------------------------------------------
    call  model_eval_with_format(ctx_model,i_cell,xpt_refelem,xpt_gb,   &
                                 vp,vs,rho,visco,ctx_err)
    !! -----------------------------------------------------------------
    !! 2) evaluate the complex models
    !! -----------------------------------------------------------------
    call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,freq,   &
                                      vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)

    !! what do we want -------------------------------------------------
    if(flag_lame) then
      param1 = lambda_cx
      param2 = mu_cx
    else
      param1 = vp_cx
      param2 = vs_cx
    end if
    !! -----------------------------------------------------------------

    return

  end subroutine model_eval_all_solo

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate all of the complex-valued models, with an option to either
  !> give the wave-speeds out, or the Lamé parameters.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   flag_lame  : flag for lamé (true) or speeds by default
  !> @param[out]   param1     : output value
  !> @param[out]   param2     : output value
  !> @param[out]   rho        : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_all_multi(ctx_model,i_cell,xpt_refelem,xpt_gb,   &
                                  npt,freq,flag_lame,param1,param2,rho,  &
                                  ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:,:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:,:)
    integer(kind=4)         ,intent(in)   :: npt
    complex(kind=8)         ,intent(in)   :: freq
    logical                 ,intent(in)   :: flag_lame
    complex(kind=8)         ,intent(out)  :: param1(:)
    complex(kind=8)         ,intent(out)  :: param2(:)
    real   (kind=8)         ,intent(out)  :: rho(:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer           :: k
    complex(kind=8)   :: lambda_cx, mu_cx, vp_cx, vs_cx
    real   (kind=8)   :: vp,vs
    real   (kind=8)   :: visco(N_VISCO_MAX) ! Zener
    !! -----------------------------------------------------------------

    param1 = 0.d0
    param2 = 0.d0
    
    !! loop over all points
    do k=1,npt
      !! -----------------------------------------------------------------
      !! 1) evaluate the real vp, vs, rho depending on the parametrization
      !! -----------------------------------------------------------------
      call  model_eval_with_format(ctx_model,i_cell,xpt_refelem(:,k),   &
                                   xpt_gb(:,k),vp,vs,rho(k),visco,ctx_err)
      !! -----------------------------------------------------------------
      !! 2) evaluate the complex models
      !! -----------------------------------------------------------------
      call model_eval_viscoelastic_main(ctx_model,vp,vs,rho(k),visco,   &
                                        freq,vp_cx,vs_cx,lambda_cx,     &
                                        mu_cx,ctx_err)
      
      !! what do we want -------------------------------------------------
      if(flag_lame) then
        param1(k) = lambda_cx
        param2(k) = mu_cx
      else
        param1(k) = vp_cx
        param2(k) = vs_cx
      end if
      !! -----------------------------------------------------------------
    end do
    return
  end subroutine model_eval_all_multi

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the complex-valued Stiffness tensor C, depending on the 
  !! dimension.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   Cstiff     : output values
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_C_solo(ctx_model,i_cell,xpt_refelem,xpt_gb,freq,&
                               C,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:)
    complex(kind=8)         ,intent(inout):: C(:,:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    complex(kind=8)                       :: lambda_cx, mu_cx, vp_cx, vs_cx
    real   (kind=8)                       :: vp,vs,rho
    real   (kind=8)                       :: visco(N_VISCO_MAX) ! Zener
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 1) evaluate the real vp, vs, rho depending on the parametrization
    !! -----------------------------------------------------------------
    call  model_eval_with_format(ctx_model,i_cell,xpt_refelem,xpt_gb,   &
                                 vp,vs,rho,visco,ctx_err)
    !! -----------------------------------------------------------------
    !! 2) evaluate the complex models
    !! -----------------------------------------------------------------
    call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,freq,   &
                                      vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)

    !! compute the stiffness tensor ------------------------------------
    C(:,:) = 0.d0
    select case(ctx_model%dim_domain)
      case(2)
        C(1,1) = lambda_cx + 2*mu_cx ; C(1,2) = lambda_cx
        C(2,1) = lambda_cx ; C(2,2) = lambda_cx + 2*mu_cx
        C(3,3) = mu_cx
      case(3)
        C(1,1) = lambda_cx + 2*mu_cx ; C(1,2) = lambda_cx ; C(1,3) = lambda_cx
        C(2,1) = lambda_cx ; C(2,2) = lambda_cx + 2*mu_cx ; C(2,3) = lambda_cx
        C(3,1) = lambda_cx ; C(3,2) = lambda_cx ; C(3,3) = lambda_cx + 2*mu_cx
        C(4,4) = mu_cx
        C(5,5) = mu_cx
        C(6,6) = mu_cx
              
      case default
        ctx_err%msg   = "** ERROR: The stiffness tensor can only be "// &
                        "computed in 2 or 3D [model_eval_format] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return
    end select
    !! -----------------------------------------------------------------

    return

  end subroutine model_eval_C_solo

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the complex-valued Stiffness tensor C, depending on the 
  !! dimension.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   Cstiff     : output values
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_C_multi(ctx_model,i_cell,xpt_refelem,xpt_gb,npt,&
                                 freq,C,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:,:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:,:)
    integer(kind=4)         ,intent(in)   :: npt
    complex(kind=8)         ,intent(inout):: C(:,:,:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                               :: k
    !! -----------------------------------------------------------------

    !! loop over all points
    C(:,:,:) = 0.d0
    do k=1,npt
      call model_eval_C_solo(ctx_model,i_cell,xpt_refelem(:,k),         &
                             xpt_gb(:,k),freq,C(:,:,k),ctx_err)
      if(ctx_err%ierr .ne. 0) return
    end do
    
    return

  end subroutine model_eval_C_multi

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the complex-valued compliance tensor S, depending on the 
  !! dimension.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   Cstiff     : output values
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_S_solo(ctx_model,i_cell,xpt_refelem,xpt_gb,freq,&
                               S,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:)
    complex(kind=8)         ,intent(inout):: S(:,:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    complex(kind=8)         ,allocatable  :: C(:,:),Cinv(:,:)
    complex(kind=8)                       :: lambda_cx, mu_cx, vp_cx, vs_cx
    real   (kind=8)                       :: vp,vs,rho
    real   (kind=8)                       :: visco(N_VISCO_MAX) ! Zener


    S=0.d0
    select case(ctx_model%dim_domain)
      case(2)
        allocate(C   (3,3))
        allocate(Cinv(3,3))
      case(3)
        allocate(C   (6,6))
        allocate(Cinv(6,6))
      case default
        ctx_err%msg   = "** ERROR: Unrecognized dimension for the " //  &
                        "stiffness tensor evaluation [model_eval_S] **"
        ctx_err%ierr  = -1
        return
    end select
    
    !! -----------------------------------------------------------------
    !! 1) evaluate the real vp, vs, rho depending on the parametrization
    !! -----------------------------------------------------------------
    call  model_eval_with_format(ctx_model,i_cell,xpt_refelem,xpt_gb,   &
                                 vp,vs,rho,visco,ctx_err)
    !! -----------------------------------------------------------------
    !! 2) evaluate the complex models
    !! -----------------------------------------------------------------
    call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,freq,   &
                                      vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)

    !! compute the stiffness tensor ------------------------------------
    C(:,:) = 0.d0
    select case(ctx_model%dim_domain)
      case(2)
        C(1,1) = lambda_cx + 2*mu_cx ; C(1,2) = lambda_cx
        C(2,1) = lambda_cx ; C(2,2) = lambda_cx + 2*mu_cx
        C(3,3) = mu_cx
        !! from C to S
        call lapack_matrix_inverse(C,Cinv,3,ctx_err)
        S = matmul(Voigt2Dinv,Cinv)
        S = matmul(S,Voigt2Dinv)

      case(3)
        C(1,1) = lambda_cx + 2*mu_cx ; C(1,2) = lambda_cx ; C(1,3) = lambda_cx
        C(2,1) = lambda_cx ; C(2,2) = lambda_cx + 2*mu_cx ; C(2,3) = lambda_cx
        C(3,1) = lambda_cx ; C(3,2) = lambda_cx ; C(3,3) = lambda_cx + 2*mu_cx
        C(4,4) = mu_cx
        C(5,5) = mu_cx
        C(6,6) = mu_cx
        !! from C to S
        call lapack_matrix_inverse(C,Cinv,6,ctx_err)
        S = matmul(Voigt3Dinv,Cinv)
        S = matmul(S,Voigt3Dinv)
              
      case default
        ctx_err%msg   = "** ERROR: The stiffness tensor can only be "// &
                        "computed in 2 or 3D [model_eval_format] **"
        ctx_err%ierr  = -1
    end select
    deallocate(C)
    deallocate(Cinv)
    !! -----------------------------------------------------------------

    return

  end subroutine model_eval_S_solo

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the complex-valued compliance tensor S, depending on the 
  !! dimension.
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    i_cell     : which cell
  !> @param[in]    xpt_refelem: position on the reference element
  !> @param[in]    xpt_gb     : position on the global    element
  !> @param[in]    freq       : frequency, only for viscous models
  !> @param[out]   Cstiff     : output values
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_S_multi(ctx_model,i_cell,xpt_refelem,xpt_gb,npt,&
                                 freq,S,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:,:)
    real   (kind=8)         ,intent(in)   :: xpt_gb(:,:)
    integer(kind=4)         ,intent(in)   :: npt
    complex(kind=8)         ,intent(inout):: S(:,:,:)
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                               :: k
    !! -----------------------------------------------------------------

    !! loop over all points
    S(:,:,:) = 0.d0
    do k=1,npt
      call model_eval_S_solo(ctx_model,i_cell,xpt_refelem(:,k),         &
                             xpt_gb(:,k),freq,S(:,:,k),ctx_err)
      if(ctx_err%ierr .ne. 0) return
    end do
    
    return

  end subroutine model_eval_S_multi


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Given a 2D vector w, we compute A(w) with
  !> A(w) = sum w_k A_k,
  !> where A_k is a 2x4 matrix, so the result is a 2x4 matrix.
  !> note that A are mostly zeros so we only setup the non-zero entries.
  !
  !> @param[in]    w          : input vector with two coefficients
  !> @param[out]   Aw         : output weighted sum of matrix 
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine compute_operator_A_2D(w,Aw)

    implicit none

    !! -----------------------------------------------------------------
    real(kind=8)            ,intent(in)   :: w(2)
    real(kind=8)            ,intent(out)  :: Aw(2,3)
    !! -----------------------------------------------------------------
    
    Aw = 0.d0
    Aw(1,1) = w(1) 
    Aw(2,2) = w(2) 
    Aw(2,3) = 0.5d0*w(1) 
    Aw(1,3) = 0.5d0*w(2) 

    return
  end subroutine compute_operator_A_2D
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Given a 2D vector w, we compute A(w) with
  !> A(w) = sum w_k AT_k,
  !> where AT_k is a 2x4 matrix, so the result is a 2x4 matrix.
  !> note that A are mostly zeros so we only setup the non-zero entries.
  !
  !> @param[in]    w          : input vector with two coefficients
  !> @param[out]   Adagw      : output weighted sum of matrix 
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine compute_operator_Adag_2D(w,Adagw)

    implicit none

    !! -----------------------------------------------------------------
    real(kind=8)            ,intent(in)   :: w(2)
    real(kind=8)            ,intent(out)  :: Adagw(2,4)
    !! -----------------------------------------------------------------
    
    Adagw = 0.d0
    Adagw(1,1) = w(1)
    Adagw(2,2) = w(2) 
    Adagw(2,3) = w(1) 
    Adagw(1,3) = w(2) 

    return
  end subroutine compute_operator_Adag_2D

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Given a 3D vector w, we compute A(w) with
  !> A(w) = sum w_k A_k,
  !> where A_k is a 3x6 matrix, so the result is a 3x6 matrix.
  !> note that A are mostly zeros so we only setup the non-zero entries.
  !
  !> @param[in]    w          : input vector with two coefficients
  !> @param[out]   Adagw      : output weighted sum of matrix 
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine compute_operator_A_3D(w,Aw)

    implicit none

    !! -----------------------------------------------------------------
    real(kind=8)            ,intent(in)   :: w(3)
    real(kind=8)            ,intent(out)  :: Aw(3,6)
    !! -----------------------------------------------------------------
    
    ! we have
    !      (1 0 0 0  0    0 )        (0 0 0 0   0 1/2)          (0 0 0  0  1/2 0)
    ! Ax = (0 0 0 0  0   1/2)   Ay = (0 1 0 0   0  0 )     Az = (0 0 0 1/2  0  0)
    !      (0 0 0 0 1/2   0 )        (0 0 0 1/2 0  0 )          (0 0 1  0   0  0)
    Aw = 0.d0
    Aw(1,1) = w(1)
    Aw(2,2) = w(2) 
    Aw(3,3) = w(3) 

    Aw(2,6) = 0.5d0*w(1) 
    Aw(3,5) = 0.5d0*w(1) 
    Aw(1,6) = 0.5d0*w(2) 
    Aw(3,4) = 0.5d0*w(2) 
    Aw(1,5) = 0.5d0*w(3) 
    Aw(2,4) = 0.5d0*w(3) 
    return
  end subroutine compute_operator_A_3D

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Given a 3D vector w, we compute A(w) with
  !> A(w) = sum w_k A_k,
  !> where A_k is a 3x6 matrix, so the result is a 3x6 matrix.
  !> note that A are mostly zeros so we only setup the non-zero entries.
  !
  !> @param[in]    w          : input vector with two coefficients
  !> @param[out]   Aw         : output weighted sum of matrix 
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine compute_operator_Adag_3D(w,Adagw)

    implicit none

    !! -----------------------------------------------------------------
    real(kind=8)            ,intent(in)   :: w(3)
    real(kind=8)            ,intent(out)  :: Adagw(3,6)
    !! -----------------------------------------------------------------
    
    ! we have
    !      (1 0 0 0  0    0 )        (0 0 0 0   0 1/2)          (0 0 0  0  1/2 0)
    ! Ax = (0 0 0 0  0   1/2)   Ay = (0 1 0 0   0  0 )     Az = (0 0 0 1/2  0  0)
    !      (0 0 0 0 1/2   0 )        (0 0 0 1/2 0  0 )          (0 0 1  0   0  0)
    Adagw = 0.d0
    Adagw(1,1) = w(1)
    Adagw(2,2) = w(2) 
    Adagw(3,3) = w(3) 

    Adagw(2,6) = w(1) 
    Adagw(3,5) = w(1) 
    Adagw(1,6) = w(2) 
    Adagw(3,4) = w(2) 
    Adagw(1,5) = w(3) 
    Adagw(2,4) = w(3) 
    return
  end subroutine compute_operator_Adag_3D


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Given a 3x3 matrix C, we compute D(C)D = Voigt2D*C*Voigt2D
  !
  !> @param[in]    C          : input matrix
  !> @param[out]   DCD        : output solution
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine compute_operator_DCD_2D(C,DCD)

    implicit none

    !! -----------------------------------------------------------------
    complex(kind=8)    ,intent(in)   :: C  (3,3)
    complex(kind=8)    ,intent(out)  :: DCD(3,3)
    !! -----------------------------------------------------------------
    
    DCD = 0.d0 
    !! we provide the matrix directly rather than using matmul...
    !! DCD = matmul(Voigt2D,matmul(C,Voigt2D)) 
    DCD(1,1) =   C(1,1);DCD(1,2) =      C(1,2); DCD(1,3) = 2.d0*C(1,3);
    DCD(2,1) =   C(2,1);DCD(2,2) =      C(2,2); DCD(2,3) = 2.d0*C(2,3);
    DCD(3,1) = 2*C(3,1);DCD(3,2) = 2.d0*C(3,2); DCD(3,3) = 4.d0*C(3,3);

    return
  end subroutine compute_operator_DCD_2D

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Given a 6x6 matrix C, we compute D(C)D = Voigt3D*C*Voigt3D
  !
  !> @param[in]    C          : input matrix
  !> @param[out]   DCD        : output solution
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine compute_operator_DCD_3D(C,DCD)

    implicit none

    !! -----------------------------------------------------------------
    complex(kind=8)    ,intent(in)   :: C  (6,6)
    complex(kind=8)    ,intent(out)  :: DCD(6,6)
    !! -----------------------------------------------------------------

    DCD = 0.d0 
    !! we provide the matrix directly rather than using matmul...
    !! DCD = matmul(Voigt3D,matmul(C,Voigt3D)) 
    DCD(1,1)=  C(1,1);   DCD(1,2)=    C(1,2);  DCD(1,3)=   C(1,3) 
    DCD(2,1)=  C(2,1);   DCD(2,2)=    C(2,2);  DCD(2,3)=   C(2,3) 
    DCD(3,1)=  C(3,1);   DCD(3,2)=    C(3,2);  DCD(3,3)=   C(3,3) 
    DCD(4,1)=2.d0*C(4,1);DCD(4,2)= 2.d0*C(4,2);DCD(4,3)= 2.d0*C(4,3)
    DCD(5,1)=2.d0*C(5,1);DCD(5,2)= 2.d0*C(5,2);DCD(5,3)= 2.d0*C(5,3)
    DCD(6,1)=2.d0*C(6,1);DCD(6,2)= 2.d0*C(6,2);DCD(6,3)= 2.d0*C(6,3)

    DCD(1,4)= 2.d0*C(1,4);DCD(1,5)= 2.d0*C(1,5);DCD(1,6)= 2.d0*C(1,6)
    DCD(2,4)= 2.d0*C(2,4);DCD(2,5)= 2.d0*C(2,5);DCD(2,6)= 2.d0*C(2,6)
    DCD(3,4)= 2.d0*C(3,4);DCD(3,5)= 2.d0*C(3,5);DCD(3,6)= 2.d0*C(3,6)
    DCD(4,4)= 4.d0*C(4,4);DCD(4,5)= 4.d0*C(4,5);DCD(4,6)= 4.d0*C(4,6)
    DCD(5,4)= 4.d0*C(5,4);DCD(5,5)= 4.d0*C(5,5);DCD(5,6)= 4.d0*C(5,6)
    DCD(6,4)= 4.d0*C(6,4);DCD(6,5)= 4.d0*C(6,5);DCD(6,6)= 4.d0*C(6,6)

    return
  end subroutine compute_operator_DCD_3D
  
  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation at a given index
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_solo_idof(ctx_model,i_cell,strmodel,    &
                                          i_dof,freq,out_val,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    character(len=*)        ,intent(in)   :: strmodel
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    integer(kind=4)         ,intent(in)   :: i_dof
    complex(kind=8)         ,intent(in)   :: freq
    real   (kind=8)         ,intent(out)  :: out_val
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8)      :: vp,vs,rho,visco(N_VISCO_MAX)
    real   (kind=8)      :: lambda,mu
    complex(kind=8)      :: lambda_cx,mu_cx,vp_cx,vs_cx
    !! -----------------------------------------------------------------
    out_val = 0.d0 

    !! -----------------------------------------------------------------
    !! 1) models at this index
    !! -----------------------------------------------------------------
    vp     = ctx_model%vp %param_dof%mval(i_cell)%array(i_dof)
    vs     = ctx_model%vs %param_dof%mval(i_cell)%array(i_dof)
    rho    = ctx_model%rho%param_dof%mval(i_cell)%array(i_dof)
    mu     = rho*(vs**2)
    lambda = rho*(vp**2) - 2.d0*mu

    visco=0.d0
    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        case(model_visco_KV,model_visco_Maxw)
          visco(1) = ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof)
          visco(2) = ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof)
        case(model_visco_Zener)
          visco(1) = ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof)
          visco(2) = ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof)
          visco(3) = ctx_model%visco(3)%param_dof%mval(i_cell)%array(i_dof)
          visco(4) = ctx_model%visco(4)%param_dof%mval(i_cell)%array(i_dof)
        case(model_visco_ZenerFrac)
          visco(1) = ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof)
          visco(2) = ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof)
          visco(3) = ctx_model%visco(3)%param_dof%mval(i_cell)%array(i_dof)
          visco(4) = ctx_model%visco(4)%param_dof%mval(i_cell)%array(i_dof)
          visco(5) = ctx_model%visco(5)%param_dof%mval(i_cell)%array(i_dof)
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model"//&
                          " [model_eval_format] **"
          ctx_err%ierr  = -1
          return !! non-shared error
      end select
    endif

    !! -----------------------------------------------------------------
    !! 2) We do not use the visco-elastic model here, we just get 
    !!    the "raw", parameters, not the real part of the complex-
    !!    valued ones.
    !! -----------------------------------------------------------------
    !! call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,freq,   &
    !!                                   vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)
    !! 
    !! -----------------------------------------------------------------
    
    
    !! -----------------------------------------------------------------
    !! 3) get output values
    select case(trim(adjustl(strmodel)))
      case(model_VP      )
        out_val = dble(vp)
      case(model_VS      )
        out_val = dble(vs)
      case(model_RHO     )
        out_val = rho

      case(model_QFACTORP)
        !! the quality factor is given by the real part divided by the
        !! imaginary part, for P-wave speed.
        if(.not. ctx_model%flag_viscous) then
          ctx_err%msg   = "** ERROR: The quality factor is only valid"//&
                          " for viscous propagation, it tends to "    //&
                          "infinity otherwise. [eval] **"
          ctx_err%ierr  = -1
          return !! non-shared error
        else
          call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,  &
                                freq,vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)
          out_val =-real(vp_cx**2)/aimag(vp_cx**2)
        end if

      case(model_QFACTORS)
        !! the quality factor is given by the real part divided by the
        !! imaginary part, for P-wave speed.
        if(.not. ctx_model%flag_viscous) then
          ctx_err%msg   = "** ERROR: The quality factor is only valid"//&
                          " for viscous propagation, it tends to "    //&
                          "infinity otherwise. [eval] **"
          ctx_err%ierr  = -1
          return !! non-shared error
        else
          call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,  &
                                freq,vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)
          out_val =-real(vs_cx**2)/aimag(vs_cx**2)
        end if
      case(model_ETA_LDA)  
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw,model_visco_Zener,     &
                 model_visco_ZenerFrac)
              out_val = visco(1)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if  

      case(model_ETA_MU   )
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_KV,model_visco_Maxw,model_visco_Zener,     &
                 model_visco_ZenerFrac)
              out_val = visco(2)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_TAU_SIGMA)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_Zener,model_visco_ZenerFrac)
              out_val = visco(3)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_TAU_EPS)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_Zener,model_visco_ZenerFrac)
              out_val = visco(4)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_FPOWER)
        if(ctx_model%flag_viscous) then
          select case(trim(adjustl(ctx_model%viscous_model)))
            case(model_visco_ZenerFrac)
              out_val = visco(5)
            case default
              ctx_err%msg   = "** ERROR: Unrecognized viscosity model [eval]"
              ctx_err%ierr  = -1
              return !! non-shared error
          end select
        else
          ctx_err%msg   = "** ERROR: no viscosity [eval]"
          ctx_err%ierr  = -1
          return !! non-shared error        
        end if 

      case(model_LAMBDA  )
        out_val = dble(lambda)

      case(model_MU      )
        out_val = dble(mu)
      
      case(model_BULK    )
        select case(ctx_model%dim_domain)
          case(2) !! cf. [Masson & Pride] paper
            out_val = dble(lambda + mu)
          case(3)
            out_val = dble(lambda + (2.d0/3.d0)*mu)
          case default
            ctx_err%msg   = "** ERROR: incorrect dimension [model_eval] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select

      case(model_POISSON )
        out_val = dble(lambda / (2.d0*(lambda + mu )))
      
      case(model_IP      )
        out_val = dble(rho * vp)

      case(model_IS      )
        out_val = dble(rho * vs)
      
      case default
        ctx_err%msg   = "** ERROR: model name " // trim(adjustl(strmodel)) // &
                        " not recognized [model_eval] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error
    end select
    
    !! sanity check >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! attenuation can be 0.
    ! if(out_val <= 0.d0) then
    !   ctx_err%msg   = "** ERROR: value for the physical parameters " // &
    !                   "is <= 0 for elastic medium [model_eval] "
    !   ctx_err%ierr  = -1
    ! end if

    return

  end subroutine model_eval_dof_raw_solo_idof

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the model for a dof representation at a given index
  !
  !> @param[in]    ctx_model  : model context
  !> @param[in]    strmodel   : what model is expected
  !> @param[in]    xpt        : which pt
  !> @param[out]   out_val    : output value
  !> @param[inout] ctx_err    : context error
  !----------------------------------------------------------------------------
  subroutine model_eval_dof_raw_solo_idof_cx(ctx_model,i_cell,          &
                                             i_dof,freq,vp_cx,vs_cx,    &
                                             lambda_cx,mu_cx,rho,ctx_err)

    implicit none

    type(t_model)           ,intent(in)   :: ctx_model
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    integer(kind=4)         ,intent(in)   :: i_dof
    complex(kind=8)         ,intent(in)   :: freq
    complex(kind=8)         ,intent(out)  :: vp_cx
    complex(kind=8)         ,intent(out)  :: vs_cx
    complex(kind=8)         ,intent(out)  :: lambda_cx
    complex(kind=8)         ,intent(out)  :: mu_cx
    real   (kind=8)         ,intent(out)  :: rho
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8)      :: vp,vs,visco(N_VISCO_MAX)
    real   (kind=8)      :: lambda,mu
    !! -----------------------------------------------------------------
    vp_cx = 0.d0 
    vs_cx = 0.d0 
    mu_cx = 0.d0 
    lambda_cx = 0.d0 

    !! -----------------------------------------------------------------
    !! 1) models at this index
    !! -----------------------------------------------------------------
    vp     = ctx_model%vp %param_dof%mval(i_cell)%array(i_dof)
    vs     = ctx_model%vs %param_dof%mval(i_cell)%array(i_dof)
    rho    = ctx_model%rho%param_dof%mval(i_cell)%array(i_dof)
    mu     = rho*(vs**2)
    lambda = rho*(vp**2) - 2.d0*mu

    visco=0.d0
    if(ctx_model%flag_viscous) then
      select case(trim(adjustl(ctx_model%viscous_model)))
        case(model_visco_KV,model_visco_Maxw)
          visco(1) = ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof)
          visco(2) = ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof)
        case(model_visco_Zener)
          visco(1) = ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof)
          visco(2) = ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof)
          visco(3) = ctx_model%visco(3)%param_dof%mval(i_cell)%array(i_dof)
          visco(4) = ctx_model%visco(4)%param_dof%mval(i_cell)%array(i_dof)
        case(model_visco_ZenerFrac)
          visco(1) = ctx_model%visco(1)%param_dof%mval(i_cell)%array(i_dof)
          visco(2) = ctx_model%visco(2)%param_dof%mval(i_cell)%array(i_dof)
          visco(3) = ctx_model%visco(3)%param_dof%mval(i_cell)%array(i_dof)
          visco(4) = ctx_model%visco(4)%param_dof%mval(i_cell)%array(i_dof)
          visco(5) = ctx_model%visco(5)%param_dof%mval(i_cell)%array(i_dof)
        case default
          ctx_err%msg   = "** ERROR: Unrecognized viscosity model"//&
                          " [model_eval_format] **"
          ctx_err%ierr  = -1
          return !! non-shared error
      end select
    endif

    !! -----------------------------------------------------------------
    !! 2) We do not use the visco-elastic model here, we just get 
    !!    the "raw", parameters, not the real part of the complex-
    !!    valued ones.
    !! -----------------------------------------------------------------
    call model_eval_viscoelastic_main(ctx_model,vp,vs,rho,visco,freq,   &
                                      vp_cx,vs_cx,lambda_cx,mu_cx,ctx_err)
    
    !! -----------------------------------------------------------------
    
    return

  end subroutine model_eval_dof_raw_solo_idof_cx

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pconstant_real4(ctx_model,model_out,strmodel,    &
                                       freq,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4), allocatable          ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(IKIND_MESH)      :: i_cell
    real(kind=8)             :: tempval
    real(kind=8),allocatable :: xpt_refelem(:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! unused
    allocate(xpt_refelem(ctx_model%dim_domain))
    allocate(xpt_gb     (ctx_model%dim_domain))
    xpt_refelem = 0.d0
    xpt_gb      = 0.d0

    !! loop over all cells 
    do i_cell = 1, ctx_model%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle
      
      !! piecewise constant evaluation 
      call model_eval_raw_solo(ctx_model,strmodel,i_cell,xpt_refelem,   &
                               xpt_gb,freq,tempval,ctx_err,             &
                               o_force_format=tag_MODEL_PCONSTANT)
      !! save in model
      model_out(i_cell) = real(tempval)
    end do
    
    return
  end subroutine model_get_pconstant_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pconstant_real8(ctx_model,model_out,strmodel,    &
                                       freq,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8), allocatable          ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(IKIND_MESH)      :: i_cell
    real(kind=8)             :: tempval
    real(kind=8),allocatable :: xpt_refelem(:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! unused
    allocate(xpt_refelem(ctx_model%dim_domain))
    allocate(xpt_gb     (ctx_model%dim_domain))
    xpt_refelem = 0.d0
    xpt_gb      = 0.d0

    !! loop over all cells 
    do i_cell = 1, ctx_model%n_cell_loc
      if(ctx_err%ierr .ne. 0) cycle
      
      !! piecewise constant evaluation 
      call model_eval_raw_solo(ctx_model,strmodel,i_cell,xpt_refelem,   &
                               xpt_gb,freq,tempval,ctx_err,             &
                               o_force_format=tag_MODEL_PCONSTANT)
      !! save in model
      model_out(i_cell) = dble(tempval)
    end do
    
    return
  end subroutine model_get_pconstant_real8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_ppoly_real4(ctx_domain,ctx_model,model_out,      &
                                   strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! unused
    allocate(xpt_gb(ctx_domain%dim_domain))
    xpt_gb = 0.d0

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = max(ctx_model%vp%ppoly_order,ctx_model%vs%ppoly_order)
    ndof_vol = max(ctx_model%vp%ppoly(1)%size_coeff,                    &
                   ctx_model%vs%ppoly(1)%size_coeff)
    order    = max(order   ,ctx_model%rho%ppoly_order)    
    ndof_vol = max(ndof_vol,ctx_model%rho%ppoly(1)%size_coeff)
    if(ctx_model%flag_viscous) then
      do k=1,size(ctx_model%visco)
        order    = max(order   ,ctx_model%visco(k)%ppoly_order)    
        ndof_vol = max(ndof_vol,ctx_model%visco(k)%ppoly(1)%size_coeff)
      end do
    end if

    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: at least one of the piecewise-" //     &
                      "polynomial model must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell ............................................
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo, &
                                         ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol      
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call model_eval_raw_solo(ctx_model,strmodel,i_cell,dof_coo(:,k),&
                                 xpt_gb,freq,tempval,ctx_err,           &
                                 o_force_format=tag_MODEL_PPOLY)
        model_out(i1 + k) = real(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    deallocate(xpt_gb)
    !! -----------------------------------------------------------------  

    return
  end subroutine model_get_ppoly_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_ppoly_real8(ctx_domain,ctx_model,model_out,      &
                                   strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! unused
    allocate(xpt_gb(ctx_domain%dim_domain))
    xpt_gb = 0.d0

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = max(ctx_model%vp%ppoly_order,ctx_model%vs%ppoly_order)
    ndof_vol = max(ctx_model%vp%ppoly(1)%size_coeff,                    &
                   ctx_model%vs%ppoly(1)%size_coeff)
    order    = max(order   ,ctx_model%rho%ppoly_order)    
    ndof_vol = max(ndof_vol,ctx_model%rho%ppoly(1)%size_coeff)
    if(ctx_model%flag_viscous) then
      do k=1,size(ctx_model%visco)
        order    = max(order   ,ctx_model%visco(k)%ppoly_order)    
        ndof_vol = max(ndof_vol,ctx_model%visco(k)%ppoly(1)%size_coeff)
      end do
    end if

    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: at least one of the piecewise-" //     &
                      "polynomial model must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell ............................................
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo, &
                                         ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol      
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call model_eval_raw_solo(ctx_model,strmodel,i_cell,dof_coo(:,k),&
                                 xpt_gb,freq,tempval,ctx_err,           &
                                 o_force_format=tag_MODEL_PPOLY)
        model_out(i1 + k) = dble(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    deallocate(xpt_gb)
    !! -----------------------------------------------------------------   
    return
  end subroutine model_get_ppoly_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for a parameter 
  !> given per dof.
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pdof_real4(ctx_domain,ctx_model,model_out,       &
                                  strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    real(kind=8)        , allocatable :: xpt_refelem(:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = max(ctx_model%vp%param_dof%ndof ,ctx_model%vs%param_dof%ndof)   
    order    = max(ctx_model%vp%param_dof%order,ctx_model%vs%param_dof%order)
    ndof_vol = max(ndof_vol, ctx_model%rho%param_dof%ndof)   
    order    = max(order   , ctx_model%rho%param_dof%order)
    if(ctx_model%flag_viscous) then
      do k=1,size(ctx_model%visco)
        ndof_vol = max(ndof_vol,ctx_model%visco(k)%param_dof%ndof )   
        order    = max(order   ,ctx_model%visco(k)%param_dof%order)
      end do
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    !! unused we force p-constant
    allocate(xpt_gb     (ctx_domain%dim_domain))
    xpt_gb = 0.d0
    if(ndof_vol == 1) then
      allocate(xpt_refelem(ctx_domain%dim_domain))
      xpt_refelem = 0.d0
      do i_cell = 1, ctx_domain%n_cell_loc
        call model_eval_raw_solo(ctx_model,strmodel,i_cell,xpt_refelem, &
                                 xpt_gb,freq,tempval,ctx_err,           &
                                 o_force_format=tag_MODEL_PCONSTANT)
        model_out(i_cell) = real(tempval,kind=4)
      end do
      deallocate(xpt_refelem)
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol, &
                                             ctx_domain%dim_domain,     &
                                             dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        do k=1,ndof_vol
          call model_eval_raw_solo(ctx_model,strmodel,i_cell,           &
                                   dof_coo(:,k),xpt_gb,freq,tempval,    &
                                   ctx_err,o_force_format=tag_MODEL_DOF)
          out_val(k) = tempval
        end do
        
        model_out(i_gb1:i_gb2) = real(out_val(:),kind=4)
      end do
      deallocate(dof_coo)
      deallocate(out_val)
    end if
    
    return
  end subroutine model_get_pdof_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for a parameter 
  !> given per dof.
  !
  !> @param[in]    ctx_model      : type model
  !> @param[out]   model_out      : output model
  !> @param[in]    strmodel       : current model name
  !> @param[in]    freq           : frequency may be for viscous model
  !> @param[out]   order_app      : order approximated, max of all models
  !> @param[out]   ndof_gb        : global ndof, max of all models
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_get_pdof_real8(ctx_domain,ctx_model,model_out,       &
                                  strmodel,freq,order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model)                      ,intent(in)   :: ctx_model
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    character(len=*)                   ,intent(in)   :: strmodel
    complex(kind=8)                    ,intent(in)   :: freq
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    real(kind=8)        , allocatable :: xpt_refelem(:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = max(ctx_model%vp%param_dof%ndof ,ctx_model%vs%param_dof%ndof)   
    order    = max(ctx_model%vp%param_dof%order,ctx_model%vs%param_dof%order)
    ndof_vol = max(ndof_vol, ctx_model%rho%param_dof%ndof)   
    order    = max(order   , ctx_model%rho%param_dof%order)
    if(ctx_model%flag_viscous) then
      do k=1,size(ctx_model%visco)
        ndof_vol = max(ndof_vol,ctx_model%visco(k)%param_dof%ndof )   
        order    = max(order   ,ctx_model%visco(k)%param_dof%order)
      end do
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    !! unused we force p-constant
    allocate(xpt_gb     (ctx_domain%dim_domain))
    xpt_gb = 0.d0
    if(ndof_vol == 1) then
      allocate(xpt_refelem(ctx_domain%dim_domain))
      xpt_refelem = 0.d0
      do i_cell = 1, ctx_domain%n_cell_loc
        call model_eval_raw_solo(ctx_model,strmodel,i_cell,xpt_refelem, &
                                 xpt_gb,freq,tempval,ctx_err,           &
                                 o_force_format=tag_MODEL_PCONSTANT)
        model_out(i_cell) = real(tempval,kind=8)
      end do
      deallocate(xpt_refelem)
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol, &
                                             ctx_domain%dim_domain,     &
                                             dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        do k=1,ndof_vol
          call model_eval_raw_solo(ctx_model,strmodel,i_cell,           &
                                   dof_coo(:,k),xpt_gb,freq,tempval,    &
                                   ctx_err,o_force_format=tag_MODEL_DOF)
          out_val(k) = tempval
        end do
        
        model_out(i_gb1:i_gb2) = real(out_val(:),kind=8)
      end do
      deallocate(dof_coo)
      deallocate(out_val)
    end if
    
    return
  end subroutine model_get_pdof_real8

end module m_model_eval
