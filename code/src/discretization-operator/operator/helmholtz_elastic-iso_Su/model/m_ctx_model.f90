!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_model.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines of the context for the model parameters for the
!> HARMONIC ELASTIC ISOTROPIC EQUATION
!
!------------------------------------------------------------------------------
module m_ctx_model

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_ctx_model_representation, only: t_model_param,                  &
                                        model_representation_clean,     &
                                        model_representation_copy
  use m_model_list
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: t_model, model_clean, model_infos
  public  :: model_allocate, model_copy

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_model contains info on the models for the
  !> Harmonic elastic isotropic equation
  !
  !> utils:
  !> @param[integer] dim_domain   : problem dimension,
  !> @param[string]  format_disc  : discretization format,
  !> @param[integer] n_coeff      : ncoeff per model,
  !> @param[integer] n_model      : number of models,
  !> @param[string]  name_model   : name of the models,
  !> @param[string]  flag_viscous : indicates if viscosity
  !> @param[string]  viscous_model: viscosity model
  !> @param[t_model_param] vp      : vp,
  !> @param[t_model_param] vs      : vs,
  !> @param[t_model_param] rho     : density,
  !> @param[t_model_param] visco(:): viscous.
  !----------------------------------------------------------------------------
  type t_model

    !! domain dimension (2 or 3D)
    integer                         :: dim_domain
    !! discretization format (e.g., piecewise-constant)
    character(len=32)               :: format_disc
    !! array size
    integer                         :: n_cell_loc
    integer                         :: n_cell_gb

    !! number of model
    integer                         :: n_model
    !! model names
    character(len=32), allocatable  :: name_model(:)

    !! viscosity information
    logical                         :: flag_viscous
    character(len=32)               :: viscous_model

    !! model representation, can be piecewise constant or 
    !! piecewise polynomial for instance.
    type(t_model_param)             :: vp
    type(t_model_param)             :: vs
    type(t_model_param)             :: rho
    type(t_model_param), allocatable:: visco(:)
    !! min/max values allowed
    real(kind=8)                    :: vp_min,vp_max
    real(kind=8)                    :: vs_min,vs_max
    real(kind=8)                    :: rho_min,rho_max
    real(kind=8)     , allocatable  :: visco_min(:),visco_max(:)

    !! memory infos
    integer(kind=8)                 :: memory_loc
    integer(kind=8)                 :: memory_gb
  end type t_model

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models for HARMONIC ELASTIC ISOTROPIC
  !
  !> @param[in]    dm             : dimension
  !> @param[in]    flag_viscous   : indicates if viscosity
  !> @param[in]    viscous_model  : viscosity model name
  !> @param[in]    n_model        : number of physical model parameter
  !> @param[in]    name_model     : name of physical model parameter
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_infos(dm,flag_viscous,viscous_model,                 &
                         n_model,name_model,ctx_err)
    implicit none
    integer                            ,intent(in)   :: dm
    logical                            ,intent(in)   :: flag_viscous
    character(len=32)                  ,intent(inout):: viscous_model
    integer                            ,intent(out)  :: n_model
    character(len=32),allocatable      ,intent(inout):: name_model(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(dm.ne.2 .and. dm.ne.3) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "harmonic equation in elastic medium "        //  &
                      "[model_infos] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! count models depend on the type of viscosity
    !! check consistency of viscous model
    if(flag_viscous) then
      !! viscosity models
      select case(trim(adjustl(viscous_model)))
        case('kelvin-voigt','Kelvin-Voigt','KELVIN-VOIGT')
          viscous_model = model_visco_KV
          n_model = 5
          allocate(name_model(n_model))
          name_model(4) = model_ETA_LDA
          name_model(5) = model_ETA_MU

        case('Maxwell','maxwell','MAXWELL')
          viscous_model = model_visco_Maxw
          n_model = 5
          allocate(name_model(n_model))
          name_model(4) = model_ETA_LDA
          name_model(5) = model_ETA_MU

        case('Fractional','fractional','FRACTIONAL')
          viscous_model = model_visco_Frac
          n_model = 6
          allocate(name_model(n_model))
          name_model(4) = model_ETA_LDA
          name_model(5) = model_ETA_MU
          name_model(6) = model_FPOWER

        case('Zener-model_simple','Zener_simple','zener-model_simple')
          n_model = 7
          allocate(name_model(n_model))
          viscous_model = model_visco_Zener
          name_model(4) = model_ETA_LDA
          name_model(5) = model_ETA_MU 
          name_model(6) = model_TAU_SIGMA
          name_model(7) = model_TAU_EPS

        case('Zener-fractional','Zener_fractional','zener-fractional')
          n_model = 8
          allocate(name_model(n_model))
          viscous_model = model_visco_ZenerFrac
          name_model(4) = model_ETA_LDA
          name_model(5) = model_ETA_MU 
          name_model(6) = model_TAU_SIGMA
          name_model(7) = model_TAU_EPS
          name_model(8) = model_FPOWER

        case default
          ctx_err%msg   = "** ERROR: viscosity model for harmonic " // &
                          "equation in elastic medium not "         // &
                          "recognized [model_init] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)

      end select
    else
      !! no attenuation
      n_model = 3
      allocate(name_model(n_model))
    end if

    !! main models for all
    name_model(1) = model_VP
    name_model(2) = model_VS
    name_model(3) = model_RHO


    return
  end subroutine model_infos

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize model coefficients for HARMONIC ELASTIC ISOTROPIC
  !
  !> @param[in]    ctx_model      : type model
  !> @param[in]    model_in       : input representation from which we copy
  !> @param[in]    strmodel       : current model name
  !> @param[in]    mmin/mmax      : model min and max values
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_allocate(ctx_model,ctx_model_param_in,strmodel,      &
                            mmin,mmax,ctx_err)
    implicit none
    type(t_model)                      ,intent(inout):: ctx_model
    type(t_model_param)                ,intent(in)   :: ctx_model_param_in
    character(len=*)                   ,intent(in)   :: strmodel
    real(kind=8)                       ,intent(in)   :: mmin, mmax
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    logical, parameter  :: flag_allocate = .true.
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case(trim(adjustl(strmodel)))
      case(model_VP)      !! VP
        call model_representation_copy(ctx_model_param_in,ctx_model%vp, &
                                       flag_allocate,ctx_err)
        ctx_model%vp_min= mmin
        ctx_model%vp_max= mmax
      case(model_VS)      !! VS
        call model_representation_copy(ctx_model_param_in,ctx_model%vs, &
                                       flag_allocate,ctx_err)
        ctx_model%vs_min= mmin
        ctx_model%vs_max= mmax
      case(model_RHO)     !! Rho
        call model_representation_copy(ctx_model_param_in,ctx_model%rho,&
                                       flag_allocate,ctx_err)
        ctx_model%rho_min= mmin
        ctx_model%rho_max= mmax
      !! model of attenuation
      case(model_ETA_LDA,model_ETA_MU,model_FPOWER,                     &
           model_TAU_SIGMA,model_TAU_EPS) 
        
        select case(trim(adjustl(ctx_model%viscous_model)))
        
          !! Kelvin--Voigt and Maxwell models: eta ---------------------
          case(model_visco_KV,model_visco_Maxw)
            if (.not. allocated(ctx_model%visco)) then
              allocate(ctx_model%visco    (2))
              allocate(ctx_model%visco_min(2))
              allocate(ctx_model%visco_max(2))
            end if
            select case(trim(adjustl(strmodel)))
              case(model_ETA_LDA)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(1),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(1) = mmin
                ctx_model%visco_max(1) = mmax
              case(model_ETA_MU)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(2),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(2) = mmin
                ctx_model%visco_max(2) = mmax
              case default
                ctx_err%msg   = "** ERROR: viscosity model for harmonic " // &
                                "equation in elastic medium not "         // &
                                "recognized [model_allocate] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! Fractional model: eta & fraction --------------------------
          case(model_visco_Frac)
            if (.not. allocated(ctx_model%visco)) then
              allocate(ctx_model%visco    (3))
              allocate(ctx_model%visco_min(3))
              allocate(ctx_model%visco_max(3))
            end if
            select case(trim(adjustl(strmodel)))
              case(model_ETA_LDA)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(1),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(1) = mmin
                ctx_model%visco_max(1) = mmax
              case(model_ETA_MU)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(2),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(2) = mmin
                ctx_model%visco_max(2) = mmax
              case(model_FPOWER)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(3),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(3) = mmin
                ctx_model%visco_max(3) = mmax
              case default
                ctx_err%msg   = "** ERROR: viscosity model for harmonic " // &
                                "equation in elastic medium not "         // &
                                "recognized [model_allocate] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! Zener model -----------------------------------------------
          case(model_visco_Zener)
            if (.not. allocated(ctx_model%visco)) then
              allocate(ctx_model%visco    (4))
              allocate(ctx_model%visco_min(4))
              allocate(ctx_model%visco_max(4))
            end if
            select case(trim(adjustl(strmodel)))
              case(model_ETA_LDA)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(1),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(1) = mmin
                ctx_model%visco_max(1) = mmax
              case(model_ETA_MU)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(2),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(2) = mmin
                ctx_model%visco_max(2) = mmax
              case(model_TAU_SIGMA)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(3),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(3) = mmin
                ctx_model%visco_max(3) = mmax
              case(model_TAU_EPS)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(4),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(4) = mmin
                ctx_model%visco_max(4) = mmax
              case default
                ctx_err%msg   = "** ERROR: viscosity model for harmonic " // &
                                "equation in elastic medium not "         // &
                                "recognized [model_allocate] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          !! Zener fractional ------------------------------------------
          case(model_visco_ZenerFrac)
            if (.not. allocated(ctx_model%visco)) then
              allocate(ctx_model%visco    (5))
              allocate(ctx_model%visco_min(5))
              allocate(ctx_model%visco_max(5))
            end if
            select case(trim(adjustl(strmodel)))
              case(model_ETA_LDA)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(1),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(1) = mmin
                ctx_model%visco_max(1) = mmax
              case(model_ETA_MU)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(2),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(2) = mmin
                ctx_model%visco_max(2) = mmax
              case(model_TAU_SIGMA)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(3),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(3) = mmin
                ctx_model%visco_max(3) = mmax
              case(model_TAU_EPS)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(4),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(4) = mmin
                ctx_model%visco_max(4) = mmax
              case(model_FPOWER)
                call model_representation_copy(ctx_model_param_in,      &
                                          ctx_model%visco(5),           &
                                          flag_allocate,ctx_err)
                ctx_model%visco_min(5) = mmin
                ctx_model%visco_max(5) = mmax
              case default
                ctx_err%msg   = "** ERROR: viscosity model for harmonic " // &
                                "equation in elastic medium not "         // &
                                "recognized [model_allocate] **"
                ctx_err%ierr  = -1
                ctx_err%critic=.true.
                call raise_error(ctx_err)
            end select

          case default
            ctx_err%msg   = "** ERROR: viscosity model for harmonic " // &
                            "equation in elastic medium not "         // &
                            "recognized [model_allocate] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select

      case default
        ctx_err%msg   = "** ERROR: unrecognized model for harmonic " // &
                        "equation in elastic medium [model_allocate] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    return
  end subroutine model_allocate
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> copy a model for HARMONIC ELASTIC ISOTROPIC
  !
  !> @param[in]    ctx_model_in   : type model
  !> @param[inout] ctx_model_out  : type model
  !> @param[in]    flag_allocate  : indicates if array have to be allocated
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_copy(ctx_model_in,ctx_model_out,flag_allocate,ctx_err)
    implicit none
    type(t_model)                      ,intent(in)   :: ctx_model_in
    type(t_model)                      ,intent(inout):: ctx_model_out
    logical                            ,intent(in)   :: flag_allocate
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer :: k,n
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    ctx_model_out%dim_domain   = ctx_model_in%dim_domain
    ctx_model_out%format_disc  = ctx_model_in%format_disc
    ctx_model_out%n_cell_loc  = ctx_model_in%n_cell_loc
    ctx_model_out%n_cell_gb   = ctx_model_in%n_cell_gb
    ctx_model_out%n_model      = ctx_model_in%n_model
    ctx_model_out%flag_viscous = ctx_model_in%flag_viscous
    ctx_model_out%viscous_model= ctx_model_in%viscous_model
    ctx_model_out%memory_loc   = ctx_model_in%memory_loc
    ctx_model_out%memory_gb    = ctx_model_in%memory_gb
    ctx_model_out%vp_min       = ctx_model_in%vp_min
    ctx_model_out%vp_max       = ctx_model_in%vp_max
    ctx_model_out%vs_min       = ctx_model_in%vs_min
    ctx_model_out%vs_max       = ctx_model_in%vs_max
    ctx_model_out%rho_min      = ctx_model_in%rho_min
    ctx_model_out%rho_max      = ctx_model_in%rho_max

    !! copy representations 
    call model_representation_copy(ctx_model_in%vp ,ctx_model_out%vp ,  &
                                   flag_allocate,ctx_err)
    call model_representation_copy(ctx_model_in%vs ,ctx_model_out%vs ,  &
                                   flag_allocate,ctx_err)
    call model_representation_copy(ctx_model_in%rho,ctx_model_out%rho,  &
                                   flag_allocate,ctx_err)
    if(flag_allocate) then
      allocate(ctx_model_out%name_model(ctx_model_out%n_model))
      if(ctx_model_out%flag_viscous) then
        n = size(ctx_model_in%visco)
        allocate(ctx_model_out%visco     (n))
        allocate(ctx_model_out%visco_min (n))
        allocate(ctx_model_out%visco_max (n))
      end if
    end if
    ctx_model_out%name_model = ctx_model_in%name_model

    if(ctx_model_out%flag_viscous) then
      n = size(ctx_model_out%visco)
      do k=1, n
        call model_representation_copy(ctx_model_in%visco (k),          &
                                       ctx_model_out%visco(k),          &
                                       flag_allocate,ctx_err)
        ctx_model_out%visco_min(k)= ctx_model_in%visco_min(k)
        ctx_model_out%visco_max(k)= ctx_model_in%visco_max(k)
      end do
    end if

    return
  end subroutine model_copy

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean model information
  !
  !> @param[inout] ctx_model    : model context
  !----------------------------------------------------------------------------
  subroutine model_clean(ctx_model)
    implicit none
    type(t_model)   ,intent(inout)  :: ctx_model
    integer :: k

    ctx_model%dim_domain           =0
    ctx_model%format_disc          =''
    ctx_model%n_cell_loc           =0
    ctx_model%n_cell_gb            =0
    ctx_model%n_model              =0
    ctx_model%flag_viscous         = .false.
    ctx_model%viscous_model        =''
    ctx_model%memory_loc           =0
    ctx_model%memory_gb            =0
    if(allocated(ctx_model%name_model)) deallocate(ctx_model%name_model)
    !! representation 
    call model_representation_clean(ctx_model%vp)
    call model_representation_clean(ctx_model%vs)
    call model_representation_clean(ctx_model%rho)
    if(allocated(ctx_model%visco))  then
      do k=1,size(ctx_model%visco)
        call model_representation_clean(ctx_model%visco(k))
      end do
      deallocate(ctx_model%visco)
    end if

    return
  end subroutine model_clean

end module m_ctx_model
