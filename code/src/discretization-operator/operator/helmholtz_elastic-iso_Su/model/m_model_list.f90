!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_list.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines of the context for the model parameters for the
!> HELMHOLTZ ELASTIC ISOTROPIC EQUATION
!
!------------------------------------------------------------------------------
module m_model_list

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  !> list of model names
  character(len=2),  parameter :: model_VP      = 'vp'
  character(len=2),  parameter :: model_VS      = 'vs'
  character(len=3),  parameter :: model_RHO     = 'rho'
  !! Kelvin--Voigt, Maxwell, fractional and Zener model
  character(len=10), parameter :: model_ETA_LDA = 'eta-lambda'
  character(len=6),  parameter :: model_ETA_MU  = 'eta-mu'    
  !! fractional model only
  character(len=10), parameter :: model_FPOWER  = 'freq-power'    
  !! Zener model only
  character(len=9),  parameter :: model_TAU_SIGMA = 'tau-sigma' 
  character(len=11), parameter :: model_TAU_EPS   = 'tau-epsilon' 
  !! Quality factors
  character(len=8),  parameter :: model_QFACTORP = 'QfactorP' 
  character(len=8),  parameter :: model_QFACTORS = 'QfactorS' 
  !! other alternatives which can be used for conversion
  character(len=6),  parameter :: model_LAMBDA  = 'lambda'
  character(len=2),  parameter :: model_MU      = 'mu'
  character(len=5),  parameter :: model_BULK    = 'kappa'
  character(len=2),  parameter :: model_POISSON = 'nu'
  character(len=2),  parameter :: model_IP      = 'ip'
  character(len=2),  parameter :: model_IS      = 'is'

  !! list of viscosity name 
  character(len=12),  parameter :: model_visco_KV    = 'Kelvin-Voigt'
  character(len=18),  parameter :: model_visco_Zener = 'Zener-model_simple'
  character(len=7) ,  parameter :: model_visco_Maxw  = 'Maxwell'
  character(len=10),  parameter :: model_visco_Frac  = 'Fractional'
  character(len=16),  parameter :: model_visco_ZenerFrac = 'Zener-Fractional'
  
  private
  public  :: model_LAMBDA, model_MU, model_formalism
  public  :: model_VP, model_VS, model_RHO, model_ETA_LDA, model_ETA_MU   
  public  :: model_BULK, model_POISSON, model_IP, model_IS
  public  :: model_TAU_SIGMA, model_TAU_EPS,model_QFACTORP,model_QFACTORS
  public  :: model_FPOWER, model_visco_ZenerFrac
  public  :: model_visco_KV,model_visco_Maxw,model_visco_Zener,model_visco_Frac

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Formalise models name with appropriate TAG for HARMONIC ELASTIC ISO
  !
  !> @param[in]    dm             : dimension
  !> @param[in]    flag_viscous   : indicates if viscosity
  !> @param[in]    viscous_model  : viscosity model name
  !> @param[in]    n_model        : number of physical model parameter
  !> @param[in]    name_model     : name of physical model parameter
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_formalism(dm,flag_viscous,viscous_model,n_model,     &
                             name_model_in,name_model_out,ctx_err)
    implicit none

    integer                            ,intent(in)   :: dm
    logical                            ,intent(in)   :: flag_viscous
    character(len=32)                  ,intent(in)   :: viscous_model
    integer                            ,intent(in)   :: n_model
    character(len=32),allocatable      ,intent(in)   :: name_model_in (:)
    character(len=32),allocatable      ,intent(inout):: name_model_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer    :: k,l
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(dm.ne.2 .and. dm.ne.3) then
      ctx_err%msg   = "** ERROR: Unrecognized model dimension for " //  &
                      "harmonic equation in elastic medium "        //  &
                      "[model_formalism] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif

    !! check count models
    if(flag_viscous) then
      if(n_model < 5 .or. n_model > 7) then
        ctx_err%msg   = "** ERROR: INCORRECT NUMBER OF MODEL   " //  &
                        "[model_formalism] **"
        ctx_err%ierr  = -1
      end if
      if(trim(adjustl(viscous_model)) .ne. model_visco_KV    .and.      &
         trim(adjustl(viscous_model)) .ne. model_visco_Zener .and.      &
         trim(adjustl(viscous_model)) .ne. model_visco_Maxw  .and.      &
         trim(adjustl(viscous_model)) .ne. model_visco_ZenerFrac .and.  &
         trim(adjustl(viscous_model)) .ne. model_visco_Frac)  then
        ctx_err%msg   = "** ERROR: viscosity model for harmonic " // &
                        "equation in elastic medium not "         // &
                        "recognized [model_allocate] **"
        ctx_err%ierr  = -1
      end if
    else
      if(n_model .ne. 3) then
        ctx_err%msg   = "** ERROR: INCORRECT NUMBER OF MODEL   " //  &
                        "[model_formalism] **"
        ctx_err%ierr  = -1
      end if
    endif
    
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if

    !! retrieve tag for all models
    do k=1,n_model
      select case(trim(adjustl(name_model_in(k))))
        case('lame-lambda','lambda','Lambda','LAMBDA','LAME-LAMBDA')
          name_model_out(k) = model_LAMBDA
        case('lame-mu','mu','Mu','MU','LAME-MU')
          name_model_out(k) = model_MU
        case('velocity_p','vp','Velocity_P','Vp','VP','Cp','cp','CP')
          name_model_out(k) = model_VP
        case('velocity_s','vs','Velocity_S','Vs','VS','Cs','cs','CS')
          name_model_out(k) = model_VS
        case('rho','RHO','Rho','DENSITY','Density','density')
          name_model_out(k) = model_RHO
        case('bulk','BULK','Bulk','bulk-modulus','BULK-MODULUS','Bulk-Modulus')
          name_model_out(k) = model_BULK
        case('poisson','POISSON','Poisson','poisson-ratio',             &
             'POISSON-RATIO','Poisson-Ratio')
          name_model_out(k) = model_POISSON
        case('impedance_p','Impedance_P','IMPEDANCE_P')
          name_model_out(k) = model_IP
        case('impedance_s','Impedance_S','IMPEDANCE_S')
          name_model_out(k) = model_IS
        case('eta-lambda','Eta-Lambda','etalambda','ETA-LAMBDA','Eta-lambda')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if          
          name_model_out(k) = model_ETA_LDA 
        case('eta-mu','ETA-MU','EtaMu','Eta-mu')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if          
          name_model_out(k) = model_ETA_MU   
        case('tau-sigma','TAU-SIGMA','Tau-sigma')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if          
          name_model_out(k) = model_TAU_SIGMA
        case('tau-eps','TAU-EPS','Tau-eps','tau-epsilon','TAU-EPSILON', &
             'Tau-epsilon')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          name_model_out(k) = model_TAU_EPS
        case('frequency-power','freq-power','FREQ-POWER','Freq-Power',  &
             'attennuation-freq-power', 'attennuation-frequency-power')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          name_model_out(k) = model_FPOWER
        case('QfactorP','Qfactor_P','Qfactor_p','qfactorP','qfactor_P','qfactor_p')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          name_model_out(k) = model_QFACTORP
        case('QfactorS','Qfactor_S','Qfactor_s','qfactorS','qfactor_S','qfactor_s')
          if(.not. flag_viscous) then
            ctx_err%msg   = "** ERROR: given viscosity in non-viscous " //  &
                           "problem [model_formalism] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          name_model_out(k) = model_QFACTORS
          
        case default 
          ctx_err%msg   = "** ERROR: model name "         // &
                          trim(adjustl(name_model_in(k))) // &
                          " in elastic not "              // &
                          "recognized [model_formalism] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      
      !! -----------------------------------------------------------------
      !! need to check that 
      !!   1) it is different from any other one 
      !!   2) in case of viscosity, one of the model must be the viscosity
      !! -----------------------------------------------------------------
      do l=1,k-1
        if(trim(adjustl(name_model_out(k))) == &
           trim(adjustl(name_model_out(l)))) then
          ctx_err%msg   = "** ERROR: elastic model "       //           &
                          trim(adjustl(name_model_out(k))) //           &
                          " is repeated twice [model_formalism] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)        
        endif
      end do
    end do

    !! check that viscosity if present
    if(flag_viscous) then
      ctx_err%ierr=-1
      select case(trim(adjustl(viscous_model)))
        case(model_visco_Zener)
          if((any(name_model_out == model_ETA_LDA)        .and.         &
              any(name_model_out == model_ETA_MU)         .and.         & 
              any(name_model_out == model_TAU_SIGMA)      .and.         &
              any(name_model_out == model_TAU_EPS))       .or.          &
             (any(name_model_out == model_QFACTORP)       .and.         &
              any(name_model_out == model_QFACTORS)       .and.         & 
              any(name_model_out == model_TAU_SIGMA)      .and.         &
              any(name_model_out == model_TAU_EPS)))   ctx_err%ierr=0

        case(model_visco_ZenerFrac)
          if((any(name_model_out == model_ETA_LDA)        .and.         &
              any(name_model_out == model_ETA_MU)         .and.         & 
              any(name_model_out == model_TAU_SIGMA)      .and.         &
              any(name_model_out == model_TAU_EPS)        .and.         &
              any(name_model_out == model_FPOWER))        .or.          &
             (any(name_model_out == model_QFACTORP)       .and.         &
              any(name_model_out == model_QFACTORS)       .and.         & 
              any(name_model_out == model_TAU_SIGMA)      .and.         &
              any(name_model_out == model_TAU_EPS)        .and.         &
              any(name_model_out == model_FPOWER)))   ctx_err%ierr=0

        case(model_visco_KV,model_visco_Maxw)
          if((any(name_model_out == model_ETA_LDA ) .and.               &
              any(name_model_out == model_ETA_MU  )) .or.               &
             (any(name_model_out == model_QFACTORP) .and.               &
              any(name_model_out == model_QFACTORS))) ctx_err%ierr=0

        case(model_visco_Frac)
          if((any(name_model_out == model_ETA_LDA ) .and.               &
              any(name_model_out == model_ETA_MU  ) .and.               &
              any(name_model_out == model_FPOWER  )) .or.               &
             (any(name_model_out == model_QFACTORS) .and.               &
              any(name_model_out == model_QFACTORP) .and.               &
              any(name_model_out == model_FPOWER  ))) ctx_err%ierr=0

        case default

      end select
            
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: viscoelastic requires one of the "// &
                        "model to be the viscosity [model_formalism] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if

    return
  end subroutine model_formalism

end module m_model_list
