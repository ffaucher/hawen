!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_field.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for wavefields info
!
!------------------------------------------------------------------------------
module m_print_field

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  implicit none
  
  private
  public :: print_info_field_creation,print_info_field_save
    
  contains 

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> time info for the field distribution creation
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   time_creation : time spent
  !> @param[in]   flag_bwd      : backward flag
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_field_creation(ctx_paral,time_creation,flag_bwd,ctx_err)
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    real(kind=8)           ,intent(in)   :: time_creation
    logical                ,intent(in)   :: flag_bwd
    type(t_error)          ,intent(out)  :: ctx_err
    !!
    character(len=64)       :: str_time 
    
    ctx_err%ierr  = 0

    if(ctx_paral%master) then
      call cast_time(time_creation,str_time)
      if(flag_bwd) then
        write(6,'(2a)') "-    backward field distrib. : ",trim(adjustl(str_time))
      else
        write(6,'(2a)') "-    field distribution in   : ",trim(adjustl(str_time))
      end if
    endif
    return
  end subroutine print_info_field_creation


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> time info for the field save on disk
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   time_creation : time spent
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_field_save(ctx_paral,time_creation,ctx_err)
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    real(kind=8)           ,intent(in)   :: time_creation
    type(t_error)         ,intent(out)   :: ctx_err
    !!
    character(len=64)       :: str_time 
    
    ctx_err%ierr  = 0

    if(ctx_paral%master) then
      call cast_time(time_creation,str_time)
      write(6,'(2a)') "-    field saved in          : ",trim(adjustl(str_time))
      write(6,'(a)')    separator
    endif
    return
  end subroutine print_info_field_save

end module m_print_field
