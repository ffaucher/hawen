!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_field.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines of the context for the wavefield solution of the
!> linear system
!
!------------------------------------------------------------------------------
module m_ctx_field

  use m_raise_error,      only : raise_error, t_error
  use m_define_precision, only : RKIND_MAT

  implicit none
  private
  public :: t_field, field_clean
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_fields contains all solutions of the linear system
  !
  !> utils:
  !> @param[complex] field   : the wavefields of the linear system
  !----------------------------------------------------------------------------
  type t_field

    !! number of sources
    integer            :: n_src
    !! number of fields
    integer            :: n_field
    integer            :: n_mult
    !! local size 
    integer            :: n_dofsol_loc
    integer            :: n_dofvar_loc

    !! name of the fields
    character(len=3)       , allocatable :: name_field(:)
    !! list of concerned fields, size (ncoeff, nfield)
    complex(kind=RKIND_MAT), allocatable :: field     (:,:)
    complex(kind=RKIND_MAT), allocatable :: multiplier(:,:)
    
    !! time to create
    real(kind=8)       :: time_distribute=0.

  end type t_field
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean field information
  !
  !> @param[inout] ctx_field    : field context
  !----------------------------------------------------------------------------
  subroutine field_clean(ctx_field)
    type(t_field)       ,intent(inout)  :: ctx_field
    
    
    if(allocated(ctx_field%field))      deallocate(ctx_field%field)
    if(allocated(ctx_field%name_field)) deallocate(ctx_field%name_field)
    if(allocated(ctx_field%multiplier)) deallocate(ctx_field%multiplier)
    
    ctx_field%n_src       =0
    ctx_field%n_field     =0
    ctx_field%n_mult      =0
    ctx_field%n_dofsol_loc=0
    ctx_field%n_dofvar_loc=0
    ctx_field%time_distribute=0.

    return
  end subroutine field_clean

end module m_ctx_field
