!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_field_create.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an api to retreive the wavefields from
!> the global solution of the linear system depending on the type 
!> of methods (DG, FD).
!> In particular, HDG needs to post-process the Lagrange multiplier
!> which are the solutions to the system, this is done in the 
!> functions of the discretization-method/hdg for example.
!
!------------------------------------------------------------------------------
module m_field_create

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_define_precision,       only: RKIND_MAT
  use m_time,                   only: time
  !> 
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_field,              only: t_field
  use m_print_field,            only: print_info_field_creation
  !> depends on the discretization method
  use m_discretization_field,   only: discretization_field
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: field_create

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> retrieve wavefield infos from monoproc global solution
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    ctx_domain     : type domain
  !> @param[inout] ctx_dg         : type dg
  !> @param[inout] ctx_field      : type field
  !> @param[in]    tag_method     : tag method
  !> @param[in]    nrhs           : number of rhs
  !> @param[in]    nsol           : number of sol
  !> @param[in]    name_field     : name of the fields
  !> @param[inout] global_solution: global solution to distribute
  !> @param[in]    flag_backward  : indicates if forward or backward
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_verb    : for printing
  !----------------------------------------------------------------------------
  subroutine field_create(ctx_paral,ctx_domain,ctx_disc,ctx_field,nrhs, &
                          nsol,name_field,global_solution,flag_backward,&
                          ctx_err,o_flag_verb)
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_discretization)             ,intent(inout):: ctx_disc
    type(t_field)                      ,intent(inout):: ctx_field
    integer                            ,intent(in)   :: nrhs,nsol
    complex(kind=RKIND_MAT),allocatable,intent(inout):: global_solution(:)
    character(len=*),       allocatable,intent(in)   :: name_field(:)
    logical                            ,intent(in)   :: flag_backward
    type(t_error)                      ,intent(inout):: ctx_err
    logical                   ,optional,intent(in)   :: o_flag_verb
    !! -----------------------------------------------------------------
    integer           :: k
    real(kind=8)      :: time0,time1
    logical           :: flag_verb
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    call time(time0)

    flag_verb = .true.
    if(present(o_flag_verb)) flag_verb = o_flag_verb
    !! we initialize the arrays size infos and names 
    ctx_field%n_src   = nrhs
    ctx_field%n_field = nsol
    allocate(ctx_field%name_field(ctx_field%n_field))
    do k = 1, ctx_field%n_field
      ctx_field%name_field(k)  = name_field(k)
    enddo

    !! ---------------------------------------------
    call discretization_field(ctx_paral,ctx_disc,ctx_domain,            &
                              global_solution,ctx_field%n_src,          &
                              ctx_field%n_mult,ctx_field%n_field,       &
                              ctx_field%n_dofsol_loc,                   &
                              ctx_field%n_dofvar_loc,                   &
                              ctx_field%multiplier,ctx_field%field,     &
                              flag_backward,ctx_err)

    if(allocated(global_solution)) deallocate(global_solution)
    call time(time1)
    ctx_field%time_distribute = ctx_field%time_distribute + (time1-time0)
    if(flag_verb) then
      call print_info_field_creation(ctx_paral,time1-time0,             &
                                     flag_backward,ctx_err)
    endif
    return
  end subroutine field_create

end module m_field_create
