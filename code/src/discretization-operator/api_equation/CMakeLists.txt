add_library(api_equation
m_equation_init.f90
m_print_equation.f90
m_read_parameters_viscosity.f90
)
target_link_libraries(api_equation discretization_operator parameter_file raise_error printer parallelism)
install(TARGETS api_equation 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
