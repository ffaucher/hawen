!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_viscosity.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read parameters in the presence of viscosity 
!> in the propagation
!
!------------------------------------------------------------------------------
module m_read_parameters_viscosity
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_viscosity
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read all parameters that are required for viscosity
  !
  !> @param[in]   parameter_file : parameter file where infos are
  !> @param[out]  flag_viscous  : viscosity flag
  !> @param[out]  viscous_model : viscosity model name
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_viscosity(parameter_file,flag_viscous,     &
                                       viscous_model,ctx_err)
    character(len=*)   ,intent(in)   :: parameter_file
    character(len=32)  ,intent(out)  :: viscous_model
    logical            ,intent(out)  :: flag_viscous
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    integer            :: nval
    logical            :: buffer_logical(512)
    character(len=512) :: buffer_char
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! Reading the equation --------------------------------------------
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'viscosity',.false.,             &
                        buffer_logical,nval)
    flag_viscous=buffer_logical(1)
    if(flag_viscous) then
      call read_parameter(parameter_file,'viscosity_model','',          &
                          buffer_char,nval)
      if(nval > 32) then
        ctx_err%msg   = "** ERROR: viscosity model name "            // &
                        trim(adjustl(buffer_char)) // " too long in "// &
                        "[read_parameter_viscosity] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      endif 
      viscous_model=trim(adjustl(buffer_char))
    else
      viscous_model=''
    endif

    return
  end subroutine read_parameters_viscosity

end module m_read_parameters_viscosity
