!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_equation.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines of the context for the equation
!
!------------------------------------------------------------------------------
module m_ctx_equation

  implicit none

  private

  public :: t_equation, equation_clean

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_equation contains info on the equation
  !
  !> utils:
  !> @param[string]  eqname           : name of the harmonic equation,
  !> @param[string]  medium           : medium: acoustic, elastic, ...
  !> @param[integer] n_media_max      : number of different media in the problem
  !> @param[integer] dim_domain       : problem dimension,
  !> @param[integer] dim_var          : number of matrix variables,
  !> @param[integer] dim_solution     : solution dimension,
  !> @param[integer] formulation_order: order of the formulation (1 or 2)
  !> @param[integer] name_sol         : name of the fileds
  !> @param[logical] flag_viscous     : flag for viscous effects,
  !> @param[logical] flag_freq        : flag for frequency requirement,
  !> @param[logical] flag_mode        : flag for mode requirement,
  !> @param[logical] symmetric_matrix : flag for symmetri,
  !> @param[string]  domain_disc      : domain discretization string
  !> @param[complex] angular_freq     : current angular frequency
  !> @param[integer] tag_scale_rhs    : tag for scaling the RHS.
  !> @param[integer] mode             : current mode
  !----------------------------------------------------------------------------
  type t_equation

    !! equation name keyword, e.g. Helmholtz, Maxwell, conductivity, ...
    character(len=32)  :: eqname
    !! medium type, e.g. acoustic, elastic-isotropic, anisotropy, ...
    character(len=32)  :: medium
    !! indicates if one or multiple media type
    integer            :: n_media_max
    !! dimension of the problem
    integer            :: dim_domain
    !! dimension of the matrix variable proble (differs if HDG)
    integer            :: dim_var
    !! number of solution
    integer            :: dim_solution
    !! order of the equation formulation
    integer            :: formulation_order
    !! list of solution names (pressure, velocity, etc) of size dim_solution
    character(len=3),allocatable :: name_sol(:)
    !! list of variable names (pressure, velocity, etc) of size dim_var
    character(len=3),allocatable :: name_var(:)
    !! flag for viscous behaviour and model name
    logical            :: flag_viscous
    character(len=32)  :: viscous_model
    !! flag if frequency is needed  (not for conductivity)
    logical            :: flag_freq
    logical            :: flag_mode
    !! indicates if the discretized matrix is symmetric
    logical            :: symmetric_matrix
    !! indicates if the RHS needs some scaling or not.
    integer            :: tag_scale_rhs
    !! current frequency and modes 
    complex(kind=8)    :: current_angular_frequency
    integer(kind=4)    :: current_mode

  end type t_equation
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean equation information
  !
  !> @param[inout] ctx_equation    : equation context
  !----------------------------------------------------------------------------
  subroutine equation_clean(ctx_equation)
    implicit none
    type(t_equation)       ,intent(inout)  :: ctx_equation
    
    ctx_equation%eqname           = ''
    ctx_equation%medium           = ''
    ctx_equation%dim_domain       = 0
    ctx_equation%dim_var          = 0
    ctx_equation%dim_solution     = 0
    ctx_equation%formulation_order= 0
    ctx_equation%n_media_max      = 0
    ctx_equation%flag_freq        =.false.
    ctx_equation%flag_mode        =.false.
    ctx_equation%flag_viscous     =.false.
    ctx_equation%viscous_model    =''
    ctx_equation%symmetric_matrix =.false.
    ctx_equation%current_angular_frequency=0.d0
    ctx_equation%current_mode     =0
    deallocate(ctx_equation%name_sol)
    deallocate(ctx_equation%name_var)

    return
  end subroutine equation_clean

end module m_ctx_equation
