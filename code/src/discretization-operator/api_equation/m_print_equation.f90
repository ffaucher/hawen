!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_equation.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for the problem equation
!
!------------------------------------------------------------------------------
module m_print_equation

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_print_basics,     only : separator,indicator

  implicit none
  
  private
  public :: print_equation
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the problem
  !
  !> @param[in]   ctx_paral         : parallel context
  !> @param[in]   eqname            : equation name
  !> @param[in]   dim_domain        : dimension domaine
  !> @param[in]   dim_solution      : dimension solution
  !> @param[in]   dim_var           : dimension matrix variable
  !> @param[in]   medium            : medium type name
  !> @param[in]   flag_viscous      : viscosity flag
  !> @param[in]   viscous_model     : viscosity model
  !> @param[in]   formulation_order : order of the equation formulation
  !> @param[in]   name_sol          : fields name
  !> @param[in]   name_var          : traces name
  !> @param[out]  ctx_err           : error context
  !----------------------------------------------------------------------------
  subroutine print_equation(ctx_paral,eqname,dim_domain,dim_solution,dim_var, &
                            medium,flag_viscous,viscous_model,                &
                            formulation_order,name_sol,name_var,ctx_err)

    type(t_parallelism)           ,intent(in)  :: ctx_paral
    character(len=*)              ,intent(in)  :: eqname,medium,viscous_model
    integer                       ,intent(in)  :: dim_domain,formulation_order
    integer                       ,intent(in)  :: dim_solution,dim_var
    logical                       ,intent(in)  :: flag_viscous
    character(len=*),allocatable  ,intent(in)  :: name_sol(:)
    character(len=*),allocatable  ,intent(in)  :: name_var(:)
    type(t_error)                 ,intent(out) :: ctx_err
    !!
    character(len=128) :: str
    integer            :: k

    ctx_err%ierr  = 0

    if(ctx_paral%master) then
      write(6,'(a)')    separator
      str=trim(adjustl(medium))
      if(flag_viscous) then
        str=trim(adjustl(str))//" with viscosity model [" // &
            trim(adjustl(viscous_model)) // "]"
      else
        str=trim(adjustl(str))//" without viscosity"
      endif

      write(6,'(2a)')   indicator, " Problem information "
      write(6,'(2a)')     "- equation                   : ", trim(adjustl(eqname))     
      write(6,'(2a)')     "- medium                     : ", trim(adjustl(str))
      write(6,'(a,i2,a)') "- domain dimension           : ", dim_domain,"D"
      write(6,'(a,i2,a)') "- It uses the ORDER ",formulation_order," formulation:"
      
      str="s"
      if(dim_var==1) str=""
      str=" matrix variable"//trim(adjustl(str))//" (traces): "//       &
            trim(adjustl(name_var(1)))
      do k=2,dim_var
        str = trim(str) // ", " // trim(adjustl(name_var(k)))
      enddo
      write(6,'(a,i2,a)') "-     ",dim_var     ,trim(str)
      
      str="s"
      if(dim_solution==1) str=""
      str=" computed solution"//trim(adjustl(str))//": "// &
                                trim(adjustl(name_sol(1)))
      do k=2,dim_solution
        str = trim(str) // ", " // trim(adjustl(name_sol(k)))
      enddo
      
      write(6,'(a,i2,a)') "-     ",dim_solution,trim(str)
    
    endif

    return
  end subroutine print_equation

end module m_print_equation
