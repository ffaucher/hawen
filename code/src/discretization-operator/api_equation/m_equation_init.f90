!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_equation_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module init of the context for the equation
!
!------------------------------------------------------------------------------
module m_equation_init

  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_print_equation,           only : print_equation
  use m_read_parameters_viscosity,only : read_parameters_viscosity
  use m_equation_infos,           only : equation_infos
  use m_ctx_equation,             only : t_equation

  implicit none

  private
  public  :: equation_init
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init equation information
  !
  !> @param[inout] ctx_paral       : parallelism context
  !> @param[inout] ctx_equation    : equation context
  !> @param[in]    dm              : dimension of the domain
  !> @param[inout] parameter_file  : parameter file
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine equation_init(ctx_paral,ctx_equation,parameter_file,dm,ctx_err)
    
    implicit none
    
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_equation)   ,intent(inout):: ctx_equation
    character(len=*)   ,intent(in)   :: parameter_file
    integer            ,intent(in)   :: dm
    type(t_error)      ,intent(out)  :: ctx_err
    !!

    ctx_err%ierr  = 0

    ctx_equation%dim_domain = dm

    !! the PML can break the symmetry of the matrix.
    !! call read_parameter(parameter_file,'mesh_tag_pml',0,itemp,ntemp,    &
    !!                     o_flag_found=flag_pml)


    !! get infos for the current equation 
    call equation_infos(ctx_equation%dim_domain,                        &
                        ctx_equation%flag_freq,                         &
                        ctx_equation%flag_mode,                         &
                        ctx_equation%eqname,ctx_equation%medium,        &
                        ctx_equation%dim_solution,                      &
                        ctx_equation%dim_var,ctx_equation%name_sol,     &
                        ctx_equation%name_var,                          &
                        ctx_equation%formulation_order,                 &
                        ctx_equation%symmetric_matrix,                  &
                        ctx_equation%tag_scale_rhs,                     &
                        ctx_equation%n_media_max,                       &
                        ctx_err)

    !! get viscosity infos:
    call read_parameters_viscosity(parameter_file,                      &
                                   ctx_equation%flag_viscous,           &
                                   ctx_equation%viscous_model,ctx_err)
    !! print infos
    call print_equation(ctx_paral,ctx_equation%eqname,                  &
               ctx_equation%dim_domain,ctx_equation%dim_solution,       &
               ctx_equation%dim_var,ctx_equation%medium,                &
               ctx_equation%flag_viscous,ctx_equation%viscous_model,    &
               ctx_equation%formulation_order,ctx_equation%name_sol,    &
               ctx_equation%name_var,ctx_err)

    return
  end subroutine equation_init

end module m_equation_init
