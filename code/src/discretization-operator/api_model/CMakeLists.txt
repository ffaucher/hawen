add_library(api_model
m_model_init.f90
m_model_load.f90
m_model_read.f90
m_print_model.f90
m_read_parameters_model.f90
)
target_link_libraries(api_model discretization_operator raise_error parameter_file)
install(TARGETS api_model
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
