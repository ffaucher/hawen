!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_load.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an API to fill the discretized models
!> for the current propagators (elastic, acoustic, ...)
!
!------------------------------------------------------------------------------
module m_model_load

  !! module used -------------------------------------------------------
  use m_define_precision,         only : IKIND_MESH
  use m_raise_error,              only: raise_error, t_error
  use m_distribute_error,         only: distribute_error
  use m_ctx_parallelism,          only: t_parallelism
  use m_allreduce_sum,            only: allreduce_sum
  use m_allreduce_min,            only: allreduce_min
  use m_allreduce_max,            only: allreduce_max
  !! domain, equation and model types
  use m_ctx_domain,               only: t_domain
  use m_ctx_model,                only: t_model, model_allocate
  use m_print_model,              only: print_info_model_welcome,       &
                                        print_info_model_load,          &
                                        print_info_model_end
  use m_read_parameters_model,    only: read_parameters_model,          &
                                        read_parameters_model_delta
  use m_model_read,               only: model_read_record_sep,          &
                                        model_read_per_cell,            &
                                        model_read_to_polynomial
  use m_array_types,              only : array_deallocate
  !! model representation 
  use m_ctx_model_representation, only: t_model_param,                  &
                                        model_representation_clean,     &
                                        tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_basis_functions,          only: basis_clean
  use m_model_compression_init,   only: model_compression_init
  use m_representation_compress,  only: model_apply_compression_pconstant, &
                                        model_apply_compression_ppoly
  use m_model_representation_dof, only: model_representation_dof_init
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: model_load

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models according to propagator and discretization
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    ctx_model      : type model
  !> @param[in]    parameter_file : parameter file
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_load(ctx_paral,ctx_domain,ctx_model,parameter_file,ctx_err)

    implicit none

    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_domain)           ,intent(in)   :: ctx_domain
    type(t_model)            ,intent(inout):: ctx_model
    character(len=*)         ,intent(in)   :: parameter_file
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    type(t_model_param)        :: model_param_loc
    character(len=32)          :: mformat,strmodel
    character(len=512)         :: mpath
    integer                    :: imodel
    integer(kind=4)            :: ndof,x
    integer(kind=IKIND_MESH)   :: i_cell
    real(kind=8)               :: mcste,mscale,gbmin,gbmax,mmin,mmax
    real(kind=8),allocatable   :: model(:)
    logical                    :: flag_warning_min_max,flag_extend0
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! we load all the media of interest then
    !! -----------------------------------------------------------------
    !! print welcome on screen with compute current size
    ndof = ctx_model%n_cell_loc

    !! expected memory for picewise constant
    ctx_model%memory_loc = 8*(ndof*ctx_model%n_model) !! only double real
    call allreduce_sum(ctx_model%memory_loc,ctx_model%memory_gb,1,      &
                       ctx_paral%communicator,ctx_err)
    call print_info_model_welcome(ctx_paral,ctx_model%n_model,          &
                                  ctx_model%memory_loc,ctx_err)
    allocate(model(ndof)) !! working array
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! load model, association depends on the type of equation
    !! -----------------------------------------------------------------
    allocate(model_param_loc%pconstant(ndof)) !! in any case
    
    do imodel=1,ctx_model%n_model
      
      model_param_loc%pconstant = 0.d0
      strmodel = ctx_model%name_model(imodel)
      !!  read parameter and print infos
      call read_parameters_model(parameter_file,strmodel,mformat,mpath, &
                                 mcste,mscale,mmin,mmax,                &
                                 model_param_loc%compression_flag,      &
                                 model_param_loc%compression_tol,       &
                                 model_param_loc%compression_str,       &
                                 model_param_loc%compression_box_x,     &
                                 model_param_loc%compression_box_y,     &
                                 model_param_loc%compression_box_z,     &
                                 model_param_loc%ppoly_order,           &
                                 ctx_err)

      model    = 0.d0    
      !! we read the model as a piecewise constant first. it is then 
      !! given in model(ncells), with one value per cell.
      flag_extend0 = .false.
      call model_read_per_cell(ctx_domain,mformat,mpath,mcste,mscale,   &
                               model,flag_extend0,ctx_err)
      !! possibility for non-shared error
      call distribute_error(ctx_err,ctx_paral%communicator)    
      !! ---------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! initialize the representation: we create the decision array 
      !! for compression, which is set to the identity otherwize.
      !!
      !! ---------------------------------------------------------------
      !! COMPRESSION IS BASED ON THE PIECEWISE-CONSTANT MODEL EVERY
      !! TIME, WHATEVER REPRESENTATION YOU WANT ........................
      !! ---------------------------------------------------------------
      if(model_param_loc%compression_flag) then
        call model_compression_init(ctx_paral,ctx_domain,model_param_loc, &
                                   tag_MODEL_PCONSTANT,model,             &
                                    trim(adjustl(mformat)),ctx_err)
      else !! global amount
        model_param_loc%n_subdomain_gb = ctx_domain%n_cell_gb
      end if

      !! ---------------------------------------------------------------
      !! IF compression
      !!    - we can create the compressed representation, only if 
      !!      piecewise constant for now.
      !! ELSE
      !!    - we possibly create the piecewise polynomial representation.
      !!
      !! ---------------------------------------------------------------
      if(model_param_loc%compression_flag) then
        
        !! We already have computed the decision array from the 
        !! piecewise-constant representation above. 
        ! ---------------------------------------------------------
        select case (trim(adjustl(ctx_model%format_disc)))
          case(tag_MODEL_PCONSTANT)
            !! REMARK: the input model is updated here.
            call model_apply_compression_pconstant(ctx_paral,           &
                                      trim(adjustl(ctx_model%format_disc)), &
                                      model_param_loc%decision,model,   &
                                      model_param_loc%n_subdomain_gb,   &
                                      ctx_domain%n_cell_loc,ctx_err)
          case(tag_MODEL_PPOLY)
            if(imodel == 1) then
              allocate(model_param_loc%ppoly(ndof))
            end if
            call model_apply_compression_ppoly(ctx_paral,ctx_domain,    &
                                      ctx_model%format_disc,mformat,    &
                                      model_param_loc%decision,         &
                                      model_param_loc%n_subdomain_gb,   &
                                      model_param_loc%ppoly_order,      &
                                      model,model_param_loc%ppoly,      &
                                      mmin,mmax,ctx_err)
            !! possibility for non-shared error
            call distribute_error(ctx_err,ctx_paral%communicator)    

          case default
            ctx_err%ierr=-1
            ctx_err%msg ="** ERROR: unrecognized model representation '"&
                         //trim(adjustl(ctx_model%format_disc))//"' "// &
                         "for compression (only piecewise-constant " // &
                         " or pylonomials models are supported) [model_load]"
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end select
       else !! No compression ------------------------------------------
        select case (trim(adjustl(ctx_model%format_disc)))
          case(tag_MODEL_PCONSTANT)
            !! nothing to do.

          case(tag_MODEL_PPOLY)
            !! compute the piecewise polynomial representation: withouth 
            !! compression, we have one polynomial per cell. 
            !! we generate the polynomials and save them in the 
            !! representation context.
            if(imodel == 1) then
              allocate(model_param_loc%ppoly(ndof))
            end if
            call model_read_to_polynomial(ctx_domain,mformat,mpath,     &
                                          mscale,mmin,mmax,model,       &
                                          model_param_loc%ppoly_order,  &
                                          model_param_loc%ppoly,ctx_err)
            !! possibility for non-shared error
            call distribute_error(ctx_err,ctx_paral%communicator)    

          !! if we have dof: either we start as a constant, either we
          !! read from the sep file so we should keep it. Therefore, 
          !! let us keep the sep model in all cases.
          case(tag_MODEL_SEP, tag_MODEL_DOF)
            !! we save the complete sep info
            call model_read_record_sep(ctx_domain,mformat,mpath,mscale, &
                            mmin,mmax,model,                            &
                            model_param_loc%param_sep%str_sep_format,   &
                            model_param_loc%param_sep%ox,               &
                            model_param_loc%param_sep%oy,               &
                            model_param_loc%param_sep%oz,               &
                            model_param_loc%param_sep%dx,               &
                            model_param_loc%param_sep%dy,               &
                            model_param_loc%param_sep%dz,               &
                            model_param_loc%param_sep%nx,               &
                            model_param_loc%param_sep%ny,               &
                            model_param_loc%param_sep%nz,               &
                            model_param_loc%param_sep%sep_model,        &
                            model_param_loc%param_sep%sep_model_spherical,&
                            ctx_err)
          case default
            ctx_err%ierr=-1
            ctx_err%msg ="** ERROR: unrecognized model representation '"&
                         //trim(adjustl(ctx_model%format_disc))//"' " //&
                         "in [model_load]"
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end select
      end if
      !! ---------------------------------------------------------------
      !! in the case where we have the model per dof, we need to convert
      !! the sep to the dof values. 
      !! ---------------------------------------------------------------
      model_param_loc%param_dof%order = -1
      model_param_loc%param_dof%ndof  =  0
      if(trim(adjustl(ctx_model%format_disc)) == tag_MODEL_DOF) then
        if(imodel == 1) then
          allocate(model_param_loc%param_dof%mval (ctx_domain%n_cell_loc))
        else
          do i_cell=1,ctx_domain%n_cell_loc
            call array_deallocate(model_param_loc%param_dof%mval(i_cell))
          end do
          call basis_clean(model_param_loc%param_dof%basis)
        end if
        
        !! for now we assume same order for all.........................
        model_param_loc%param_dof%order = model_param_loc%ppoly_order
        call model_representation_dof_init(ctx_domain,model_param_loc,  &
                                           ctx_err)
        !! non-shared error
        call distribute_error(ctx_err,ctx_paral%communicator)
      end if
      !! ---------------------------------------------------------------
      
      !! ---------------------------------------------------------------
      !! get min and max val
      call allreduce_min(minval(model),gbmin,1,ctx_paral%communicator,ctx_err)
      call allreduce_max(maxval(model),gbmax,1,ctx_paral%communicator,ctx_err)
      !! verify we do not modify min/max
      flag_warning_min_max = .false.
      if(gbmin < mmin .or. gbmax > mmax) then
        do x=1,ndof
          if( model(x) < mmin) model(x) = mmin
          if( model(x) > mmax) model(x) = mmax
        end do
        call allreduce_min(minval(model),gbmin,1,ctx_paral%communicator,ctx_err)
        call allreduce_max(maxval(model),gbmax,1,ctx_paral%communicator,ctx_err)
        flag_warning_min_max=.true.
        if(trim(adjustl(mformat)) == 'constant') mcste = model(1)
      end if
      !! save the model in the picewise-constant representation 
      model_param_loc%pconstant(:) = model(:)
      
      call print_info_model_load(ctx_paral,ctx_model%format_disc,        &
                                 strmodel,mformat,mpath,mcste,           &
                                 mscale,gbmin,gbmax,flag_warning_min_max,&
                                 model_param_loc%compression_flag,       &
                                 model_param_loc%compression_str,        &
                                 model_param_loc%compression_tol,        &
                                 model_param_loc%compression_box_x,      &
                                 model_param_loc%compression_box_y,      &
                                 model_param_loc%compression_box_z,      &
                                 int8(model_param_loc%n_subdomain_gb),   &
                                 model_param_loc%ppoly_order,ctx_err)
      
      !! send model to appropriate ctx_model field, using the selected
      !! representation.
      call model_allocate(ctx_model,model_param_loc,                    &
                          trim(adjustl(strmodel)),mmin,mmax,ctx_err)
      call distribute_error(ctx_err,ctx_paral%communicator)

    end do
    !! -----------------------------------------------------------------
    call print_info_model_end(ctx_paral)

    return
  end subroutine model_load

end module m_model_load
