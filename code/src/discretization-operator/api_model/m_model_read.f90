!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_read.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the model file, depending
!> on the grid type and the format of the model
!
!------------------------------------------------------------------------------
module m_model_read

  !! module used -------------------------------------------------------
  use m_raise_error,             only: raise_error, t_error
  use m_define_precision,        only: IKIND_MESH
  !> domain type and projection to the domain 
  use m_ctx_domain,              only: t_domain
  use m_ctx_model_representation,only: tag_SEPFORMAT_CART,              &
                                       tag_SEPFORMAT_SPH1D,             &
                                       tag_SEPFORMAT_SPH3D
  use m_project_array,           only:                                     &
                             project_sep_global_to_cell_local,             &
                             project_ascii_global_to_cell_local,           &
                             project_sep_global_to_cell_local_extend0,     &
                             project_sep_global_spherical_to_cell_local,   &
                             project_sep_global_spherical3d_to_cell_local, &
                       project_sep_global_spherical_to_cell_local_extend0, &
                       project_sep_global_spherical3d_to_cell_local_extend0
                             
  !> sep file reader
  use m_read_parameters_sep,    only: read_parameters_sep
  !> for piecewise polynomial models 
  use m_polynomial,             only: t_polynomial,polynomial_construct,&
                                      polynomial_eval,polynomial_clean
  use m_lapack_solve,           only: lapack_solve_system
  use m_define_precision,       only: RKIND_POL
  use m_dg_lagrange_dof_coordinates,                                    &
                                only: discretization_dof_coo_loc_elem,  &
                                      discretization_dof_coo_ref_elem
  !! -------------------------------------------------------------------
  implicit none

  private

  public  :: model_read_per_cell
  public  :: model_read_to_polynomial
  public  :: model_read_record_sep

  private :: model_read_ascii
  private :: model_read_sep_spherical_with_projection
  private :: model_read_sep_spherical_with_projection_extend0
  private :: model_read_sep_full
  private :: model_read_sep_with_projection
  private :: model_read_sep_with_projection_extend0
  private :: model_read_sep_spherical_full
  private :: model_read_sep_spherical3d_with_projection_extend0
  private :: model_read_sep_spherical3d_with_projection

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read a physical parameter model from disk
  !
  !> @param[in]    mformat        : model format
  !> @param[in]    mpath          : path to load
  !> @param[in]    mcste          : in case of constant value
  !> @param[in]    mscale         : scale for values
  !> @param[inout] model          : model coefficients
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_per_cell(ctx_domain,mformat,mpath,mcste,        &
                                 mscale,model,flag_extend0,ctx_err)
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: mformat
    character(len=*)         ,intent(in)   :: mpath
    real(kind=8)             ,intent(in)   :: mcste
    real(kind=8)             ,intent(in)   :: mscale
    real(kind=8),allocatable ,intent(inout):: model(:)
    logical                  ,intent(in)   :: flag_extend0
    type(t_error)            ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    !! -----------------------------------------------------------------
    !! load file depending on the format
    !! -----------------------------------------------------------------
    select case(trim(adjustl(mformat)))
      case('sep')
        if(flag_extend0) then
          call model_read_sep_with_projection_extend0(ctx_domain,mpath, & 
                                                      model,ctx_err)
        else
          call model_read_sep_with_projection(ctx_domain,mpath,model,ctx_err)
        end if
        !! apply scale
        model = model*mscale

      case('sep-spherical1d')
        if(flag_extend0) then
          call model_read_sep_spherical_with_projection_extend0(        &
                                          ctx_domain,mpath,model,ctx_err)        
        else
          call model_read_sep_spherical_with_projection(ctx_domain,     &
                                                        mpath,model,ctx_err)
        endif
        !! apply scale
        model = model*mscale

      case('sep-spherical-coordinates')
        if(flag_extend0) then
          call model_read_sep_spherical3d_with_projection_extend0(      &
                                          ctx_domain,mpath,model,ctx_err)        
        else
          call model_read_sep_spherical3d_with_projection(ctx_domain,   &
                                          mpath,model,ctx_err)
        endif
        !! apply scale
        model = model*mscale

      case('ascii')
        call model_read_ascii(ctx_domain,mpath,model,ctx_err)
        !! apply scale
        model = model*mscale

      case('constant')
        model = mcste
      
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** Unrecognized model format: " // &
                     trim(adjustl(mformat)) // " [model_read] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine model_read_per_cell

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save the sep files
  !
  !> @param[in]    mformat        : model format
  !> @param[in]    mpath          : path to load the model
  !> @param[in]    model_percell  : model coefficients given per cell
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_record_sep(ctx_domain,mformat,mpath,mscale,     &
                                   mmin,mmax,model_percell,             &
                                   str_sep_format,xmin,ymin,zmin,dx,dy, &
                                   dz,nx,ny,nz,model_cart,model_sph,ctx_err)

    implicit none

    type(t_domain)                , intent(in)   :: ctx_domain
    character(len=*)              , intent(in)   :: mformat
    character(len=*)              , intent(in)   :: mpath
    real(kind=8)                  , intent(in)   :: mscale,mmin,mmax
    real(kind=8),allocatable      , intent(in)   :: model_percell(:)
    real(kind=8)                  , intent(out)  :: xmin,ymin,zmin
    character(len=*)              , intent(inout):: str_sep_format
    real(kind=8)                  , intent(out)  :: dx,dy,dz
    integer                       , intent(out)  :: nx,ny,nz
    real(kind=8),allocatable      , intent(inout):: model_cart(:,:,:)
    real(kind=8),allocatable      , intent(inout):: model_sph (:)
    type(t_error)                 , intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer      :: ix,iy,iz
    real(kind=8) :: center_point(3)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    str_sep_format = tag_SEPFORMAT_CART

    if(allocated(model_cart)) deallocate(model_cart)
    if(allocated(model_sph))  deallocate(model_sph)

    select case(trim(adjustl(mformat)))
      case('constant') ! use bounds - 1.d0 to be sure.
        xmin = dble(ctx_domain%bounds_xmin) - 1.d0
        ymin = dble(ctx_domain%bounds_ymin) - 1.d0
        zmin = dble(ctx_domain%bounds_zmin) - 1.d0
        nx = 1
        ny = 1
        nz = 1
        dx = dble(ctx_domain%bounds_xmax - xmin) + 1.d0
        dy = dble(ctx_domain%bounds_ymax - ymin) + 1.d0
        dz = dble(ctx_domain%bounds_zmax - zmin) + 1.d0
        allocate(model_cart(1,1,1))
        model_cart = model_percell(1)

      case('sep','sep-spherical-coordinates')
        
        if(trim(adjustl(mformat)) == 'sep-spherical-coordinates') then
          str_sep_format = tag_SEPFORMAT_SPH3D
        end if
        
        call model_read_sep_full(mpath,model_cart,nx,ny,nz,xmin,ymin,   &
                                 zmin,dx,dy,dz,ctx_err)
        !! scale and min/max
        model_cart = model_cart * mscale
        do ix=1,nx ; do iy=1,ny ; do iz=1,nz
          if(model_cart(ix,iy,iz) < mmin) model_cart(ix,iy,iz) = mmin
          if(model_cart(ix,iy,iz) > mmax) model_cart(ix,iy,iz) = mmax
        enddo ; enddo ; enddo

      case('sep-spherical1d')
        str_sep_format = tag_SEPFORMAT_SPH1D
        call model_read_sep_spherical_full(mpath,model_sph,nx,dx,       &
                                           center_point,ctx_err)
        xmin = center_point(1)
        ymin = center_point(2)
        zmin = center_point(3)
        model_sph = model_sph * mscale
        do ix=1,nx
          if(model_sph(ix) < mmin) model_sph(ix) = mmin
          if(model_sph(ix) > mmax) model_sph(ix) = mmax
        enddo

      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** Using a sep model as a basis needs sep " //    &
                      "models to read or a constant (no ascii) [model_read] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine model_read_record_sep

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read a physical parameter model from disk using piecewise-polynomials
  !> per cells. 
  !  REMARK: the polynomial is computed on the reference element because
  !>         it is more stable.
  !
  !> @param[in]    mformat        : model format
  !> @param[in]    mpath          : path to load the model
  !> @param[in]    model_percell  : model coefficients given per cell
  !> @param[in]    poly_order     : order of expected polynomials
  !> @param[in]    ppoly          : polynomials: one per cell.
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_to_polynomial(ctx_domain,mformat,mpath,mscale,  &
                                      mmin,mmax,model_percell,          &
                                      poly_order,ppoly,ctx_err)

    implicit none

    type(t_domain)                , intent(in)   :: ctx_domain
    character(len=*)              , intent(in)   :: mformat
    character(len=*)              , intent(in)   :: mpath
    real(kind=8)                  , intent(in)   :: mscale,mmin,mmax
    real(kind=8),allocatable      , intent(in)   :: model_percell(:)
    integer                       , intent(in)   :: poly_order
    type(t_polynomial),allocatable, intent(inout):: ppoly(:)
    type(t_error)                 , intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)  :: i_cell
    integer                   :: dim_domain,ndof_vol,inode_z
    integer                   :: nx,ny,nz,j,k,inode_x,inode_y
    real(kind=8)              :: xmin,ymin,zmin,dx,dy,dz
    real(kind=8),allocatable  :: model_sep(:,:,:)
    real(kind=8),allocatable  :: model_sph(:)
    real(kind=8),allocatable  :: matA(:,:),matB(:)
    type(t_polynomial)        :: poly_temp
    real(kind=8)              :: origin(3),xpos
    real(kind=8), allocatable :: dof_coo(:,:)
    real(kind=8), allocatable :: sol (:)
    real(kind=8), allocatable :: dist(:)
    real(kind=RKIND_POL), allocatable  :: xptref(:,:)
    real(kind=RKIND_POL)               :: myval
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    if(poly_order < 0) then
      ctx_err%ierr=-1
      ctx_err%msg   = "** ERROR: negative polynomial order in "     //  &
                      "[model_read_ppoly] **"
      ctx_err%critic=.true.
      return !! non-shared error 
    end if 

    dim_domain = ctx_domain%dim_domain

    !! -----------------------------------------------------------------
    !! load file depending on the format
    !! -----------------------------------------------------------------
    select case(trim(adjustl(mformat)))
      case('ascii','constant') !! copy the model per cell
        do i_cell = 1, ctx_domain%n_cell_loc
          call polynomial_construct(dim_domain,poly_order,ppoly(i_cell),ctx_err)
          !! zero except the first to the constant 
          ppoly(i_cell)%coeff_pol(:)=0.0
          ppoly(i_cell)%coeff_pol(1)=real(model_percell(i_cell),kind=RKIND_POL)
          !! adjust with min and max 
          if(ppoly(i_cell)%coeff_pol(1) < mmin) ppoly(i_cell)%coeff_pol(1) = mmin
          if(ppoly(i_cell)%coeff_pol(1) > mmax) ppoly(i_cell)%coeff_pol(1) = mmax
        end do

      case('sep-spherical-coordinates')
        ctx_err%ierr=-1
        ctx_err%msg   = "** ERROR: polynomial representation for sep "//&
                        "in spherical coordinates is not ready yet "  //&
                        "[model_read_ppoly] **"
        ctx_err%critic=.true.
        return !! non-shared error 
      case('sep')
      
        !! if we are at order zero, we simply use the piecewise constant
        !! model representation 
        if(poly_order == 0) then
          do i_cell = 1, ctx_domain%n_cell_loc
            call polynomial_construct(dim_domain,poly_order,ppoly(i_cell),ctx_err)
            !! zero except the first to the constant 
            ppoly(i_cell)%coeff_pol(:)=0.0
            ppoly(i_cell)%coeff_pol(1)=real(model_percell(i_cell),kind=RKIND_POL)
          end do
        
        elseif(poly_order > 0) then !! order > 0
          call model_read_sep_full(mpath,model_sep,nx,ny,nz,xmin,ymin,  &
                                   zmin,dx,dy,dz,ctx_err)
  
          !!  to build the linear system
          call polynomial_construct(dim_domain,poly_order,poly_temp,ctx_err)
          ndof_vol = poly_temp%size_coeff
          allocate(matA(ndof_vol,ndof_vol))
          allocate(matB(ndof_vol))
          allocate(sol (ndof_vol))
          allocate(xptref (dim_domain,ndof_vol))
          allocate(dof_coo(dim_domain,ndof_vol))
  
          !! -----------------------------------------------------------
          !! the matrix A is common to all cell as we work with the 
          !! reference element
          !!
          !! -----------------------------------------------------------
          matA = 0.d0
          !! we respect the dimension
          call discretization_dof_coo_ref_elem(ctx_domain,poly_order,     &
                                               ndof_vol,dim_domain,       &
                                               dof_coo,ctx_err)
          xptref = real(dof_coo,kind=RKIND_POL)
          do j = 1, ndof_vol
            poly_temp%coeff_pol(:) = 0.0 ; poly_temp%coeff_pol(j) = 1.0
            !! matrix A given from the polynomial inherent structures
            do k = 1, ndof_vol
  
              !! evaluate the polynomial, the dimension selection is 
              !! dealt with in the subroutine directly
              myval = 0.d0
              call polynomial_eval(poly_temp,xptref(:,k),myval,ctx_err)
              matA(k,j) = dble(myval)
            end do
          end do
          deallocate(xptref)
          call polynomial_clean(poly_temp)
          !! -----------------------------------------------------------
            
          !! loop over all cells to get the appropriate polynomial
          do i_cell = 1, ctx_domain%n_cell_loc
            matB = 0.d0
            call polynomial_construct(dim_domain,poly_order,ppoly(i_cell),ctx_err)
            
            !! *********************************************************
            !! position of the dof on the current element for the model
            !! values and before it was on the reference element for the matrix.
            !! *********************************************************
            call discretization_dof_coo_loc_elem(ctx_domain,i_cell,       &
                                                 poly_order,ndof_vol,     &
                                                 dof_coo,ctx_err)
  
            ! -----------------------------------------------------
            !! the rhs is given by the model values.
            !! ---------------------------------------------------------
            do j = 1, ndof_vol
 
              inode_x = nint((dof_coo(1,j) - xmin)/dx) + 1
              inode_y = 1
              inode_z = 1
              select case(dim_domain)
                case(2) 
                  inode_z = nint((dof_coo(2,j) - zmin)/dz) + 1
                case(3)
                  inode_y = nint((dof_coo(2,j) - ymin)/dy) + 1
                  inode_z = nint((dof_coo(3,j) - zmin)/dz) + 1
              end select
              !! consistency check 
              if(inode_x>nx .or. inode_x<1 .or. inode_y>ny .or.         &
                 inode_y<1 .or. inode_z>nz .or. inode_z<1) then
                ctx_err%ierr=-1
                ctx_err%msg   = "** ERROR: position is outside " //     &
                                " the sep file in [model_read_ppoly] **"
                ctx_err%critic=.true.
                return !! non-shared error 
              end if
              !! -------------------------------------------------------
              matB(j) = model_sep(inode_x,inode_y,inode_z) * mscale
              !! adjust with min and max 
              if(matB(j) < mmin) matB(j) = mmin
              if(matB(j) > mmax) matB(j) = mmax
              !! -------------------------------------------------------
            end do
  
            !! if it is constant, we can move to the next cell.
            if( (maxval(matB) - minval(matB)) < tiny(1.0) ) then
              !! zero except the first to the constant 
              ppoly(i_cell)%coeff_pol(:)=0.0
              !! using the scaled value
              ppoly(i_cell)%coeff_pol(1)=real(matB(1),kind=RKIND_POL)
              cycle
            end if 
            ! -----------------------------------------------------
  
            !! solve the dense linear system
            call lapack_solve_system(matA,matB,ndof_vol,ndof_vol,       &
                                    .false.,sol,ctx_err)
            !! adjust
            ppoly(i_cell)%coeff_pol(:) = real(sol,kind=RKIND_POL)
            ! -----------------------------------------------------
          end do
  
          deallocate(matA     )
          deallocate(matB     )
          deallocate(sol      )
          deallocate(dof_coo  )
          deallocate(model_sep)
        else !! order < 0 
          ctx_err%ierr=-1
          ctx_err%msg   = "** ERROR: polynomial order for the " //      &
                          "model must be > 0 [model_read_ppoly] **"
          ctx_err%critic=.true.
          return !! non-shared error 
        end if

      ! ----------------------------------------------------------------
      !! Spherical sep file
      ! ----------------------------------------------------------------
      case('sep-spherical1d')

        allocate(dist(dim_domain))
        
        !! if we are at order zero, we simply use the piecewise constant
        !! model representation 
        if(poly_order == 0) then
          do i_cell = 1, ctx_domain%n_cell_loc
            call polynomial_construct(dim_domain,poly_order,ppoly(i_cell),ctx_err)
            !! zero except the first to the constant 
            ppoly(i_cell)%coeff_pol(:)=0.0
            ppoly(i_cell)%coeff_pol(1)=real(model_percell(i_cell),kind=RKIND_POL)
          end do
          return
        
        elseif(poly_order > 0) then

          call model_read_sep_spherical_full(mpath,model_sph,nx,dx,     &
                                             origin,ctx_err)
          
          !!  to build the linear system
          call polynomial_construct(dim_domain,poly_order,poly_temp,ctx_err)
          ndof_vol = poly_temp%size_coeff
          allocate(matA(ndof_vol,ndof_vol))
          allocate(matB(ndof_vol))
          allocate(sol (ndof_vol))
          allocate(dof_coo  (dim_domain,ndof_vol))
          allocate(xptref (dim_domain,ndof_vol))
  
          !! -----------------------------------------------------------
          !! the matrix A is common to all cell as we work with the 
          !! reference element
          !!
          !! -----------------------------------------------------------
          call discretization_dof_coo_ref_elem(ctx_domain,poly_order,   &
                                               ndof_vol,dim_domain,     &
                                               dof_coo,ctx_err)
          xptref = real(dof_coo,kind=RKIND_POL)
          matA = 0.d0
          do j = 1, ndof_vol
            poly_temp%coeff_pol(:) = 0.0 ; poly_temp%coeff_pol(j) = 1.0
            !! matrix A given from the polynomial inherent structures
            do k = 1, ndof_vol
  
              !! evaluate the polynomial, the dimension selection is 
              !! dealt with in the subroutine directly
              myval = 0.d0
              call polynomial_eval(poly_temp,xptref(:,k),myval,ctx_err)
              matA(k,j) = dble(myval)
            end do
          end do
          deallocate(xptref)
          call polynomial_clean(poly_temp)
          !! -----------------------------------------------------------
  
          !! loop over all cells to get the appropriate polynomial
          do i_cell = 1, ctx_domain%n_cell_loc
            matB = 0.d0
            call polynomial_construct(dim_domain,poly_order,            & 
                                      ppoly(i_cell),ctx_err)
  
            !! *********************************************************
            !! position of the dof on the current element for the model
            !! values and before it was on the reference element for the matrix.
            !! *********************************************************
            call discretization_dof_coo_loc_elem(ctx_domain,i_cell,     &
                                                 poly_order,ndof_vol,   &
                                                 dof_coo,ctx_err)
  
            ! -----------------------------------------------------
            !! the rhs is given by the model values.
            !! ---------------------------------------------------------
            do j = 1, ndof_vol
  
              !! spherical position
              xpos = 0.d0
              select case(dim_domain)
                case(1)
                  xpos    = abs(dof_coo(1,j) - origin(1))
                case(2)
                  dist(1:2) = dble(dof_coo(1:2,j) - origin(1:2))
                  xpos      = norm2(dist)
                case(3)
                  dist(1:3) = dble(dof_coo(1:3,j) - origin(1:3))
                  xpos      = norm2(dist)
              end select
              inode_x = nint(xpos/dx) + 1
              
              !! consistency check 
              if(inode_x>nx .or. inode_x<1) then
                ctx_err%ierr=-1
                ctx_err%msg   = "** ERROR: position is outside " //     &
                                " the sep file in [model_read_ppoly] **"
                ctx_err%critic=.true.
                return !! non-shared error 
              end if
              !! -------------------------------------------------------
              matB(j) = model_sph(inode_x) * mscale
              !! adjust with min and max 
              if(matB(j) < mmin) matB(j) = mmin
              if(matB(j) > mmax) matB(j) = mmax
              !! -------------------------------------------------------
            end do
  
            !! if it is constant, we can move to the next cell.
            if( (maxval(matB) - minval(matB)) < tiny(1.0) ) then
              !! zero except the first to the constant 
              ppoly(i_cell)%coeff_pol(:) = 0.0
              !! using the scaled value
              ppoly(i_cell)%coeff_pol(1)=real(matB(1),kind=RKIND_POL)
              cycle
            end if 
            ! -----------------------------------------------------
           
            !! solve the dense linear system 
            call lapack_solve_system(matA,matB,ndof_vol,ndof_vol,.false., &
                                     sol,ctx_err)
            !! adjust
            ppoly(i_cell)%coeff_pol(:) = real(sol,kind=RKIND_POL)
            ! -----------------------------------------------------
          end do

          deallocate(matA     )
          deallocate(matB     )
          deallocate(sol      )
          deallocate(dof_coo  )
          deallocate(model_sph)
        else !! order < 0 
          ctx_err%ierr=-1
          ctx_err%msg   = "** ERROR: polynomial order for the " //      &
                          "model must be > 0 [model_read_ppoly] **"
          ctx_err%critic=.true.
          return !! non-shared error 
        end if
        
        deallocate(dist)
      
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** Unrecognized model format: " // &
                     trim(adjustl(mformat)) // " [model_read] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine model_read_to_polynomial


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Read a sep model.
  !
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_full(header_sep,model_sep,nx,ny,nz,xmin,    &
                                 ymin,zmin,dx,dy,dz,ctx_err)
    
    implicit none

    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model_sep(:,:,:)
    real(kind=8)             ,intent(out)  :: xmin,ymin,zmin,dx,dy,dz
    integer                  ,intent(out)  :: nx,ny,nz
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    real(kind=4),allocatable  :: model_sep_r4(:,:,:)
    integer                   :: rsize
    integer                   :: path_pos,k
    character(len=512)        :: binfile, binformat
    logical                   :: file_exists
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
        
    !! read header sep file
    call read_parameters_sep(trim(adjustl(header_sep)),nx,ny,nz,xmin,  &
                             ymin,zmin,dx,dy,dz,binfile,binformat,     &
                             ctx_err)
    !! add the path to the binary file 
    if(binfile(1:1) .ne. '/')then !! absolute path if it starts with /
      if(binfile(1:2) .eq. './') binfile(1:2) = ''
      path_pos = 0
      do k=1,len_trim(adjustl(header_sep))
       if(header_sep(k:k) == '/') then
         path_pos = k
       end if
      end do
      if(path_pos .ne. 0) then
        binfile=header_sep(1:path_pos) // trim(adjustl(binfile))
      end if
    end if

    !! read global model sep
    !! check file exists
    inquire(file=trim(adjustl(binfile)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Binary file associated to header (in=): " //    &
                   trim(adjustl(binfile)) // " does not exist "  //    &
                   "[model_read_sep] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    !! We probably want to read by layers for very large files 
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    allocate(model_sep(nx,ny,nz))
    select case(trim(adjustl(binformat)))
      case('native_float','float')
        rsize=4
        !! read file 
        allocate(model_sep_r4(nx,ny,nz))
        open(unit=1010, file=trim(adjustl(binfile)), access='direct',  &
             form='unformatted',recl=rsize*nx*ny*nz)
        read (1010,rec=1) model_sep_r4
        close(1010)
        !! large-scale sep.
        !! open(unit=1010, file=trim(adjustl(binfile)), form='unformatted',access='stream')
        !! read (1010) model_sep_r4
        !! close(1010)
        model_sep = dble(model_sep_r4)
        deallocate(model_sep_r4)

      case('double')
        rsize=8
        !! read file 
        open(unit=1010, file=trim(adjustl(binfile)), access='direct',  &
             form='unformatted',recl=rsize*nx*ny*nz)
        read (1010,rec=1) model_sep
        close(1010)

      case default
        ctx_err%msg   = "** ERROR: unrecognized binary format for SEP "// &
                        trim(adjustl(header_sep))// " [model_read_sep] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select


    return
  end subroutine model_read_sep_full
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a SEP file
  !> it includes a projection in case we have a mesh
  !
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_with_projection(ctx_domain,header_sep,model,ctx_err)
    
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                   :: nx,ny,nz
    real(kind=8)              :: xmin,ymin,zmin,dx,dy,dz
    real(kind=8),allocatable  :: model_sep(:,:,:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
        
    call model_read_sep_full(header_sep,model_sep,nx,ny,nz,xmin,ymin,   &
                             zmin,dx,dy,dz,ctx_err)

    !! -----------------------------------------------------------------
    !! convert to local shape, depending on the type of element
    !! -----------------------------------------------------------------
    call project_sep_global_to_cell_local(ctx_domain,xmin,ymin,zmin,nx, &
                                          ny,nz,dx,dy,dz,model_sep,     &
                                          model,ctx_err)
    deallocate(model_sep)
    !! -----------------------------------------------------------------
    return
  end subroutine model_read_sep_with_projection

  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a SEP file
  !> it includes a projection in case we have a mesh, 
  !> we extend to 0 if the cell mesh is not found in 
  !> the sep.
  !
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_with_projection_extend0(ctx_domain,         &
                                                    header_sep,model,ctx_err)
    
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                   :: nx,ny,nz
    real(kind=8)              :: xmin,ymin,zmin,dx,dy,dz
    real(kind=8),allocatable  :: model_sep(:,:,:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
        
    call model_read_sep_full(header_sep,model_sep,nx,ny,nz,xmin,ymin,   &
                             zmin,dx,dy,dz,ctx_err)

    !! -----------------------------------------------------------------
    !! convert to local shape, depending on the type of element
    !! -----------------------------------------------------------------
    call project_sep_global_to_cell_local_extend0(ctx_domain,xmin,ymin, &
                                          zmin,nx,ny,nz,dx,dy,dz,       &
                                          model_sep,model,ctx_err)
    deallocate(model_sep)
    !! -----------------------------------------------------------------
    return
  end subroutine model_read_sep_with_projection_extend0
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a ascii file
  !> it is an ascii file with the total number of values and the 
  !> list of values
  !
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    file_cellval   : path to ascii file
  !> @param[inout] model          : model coefficients
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_ascii(ctx_domain,file_cellval,model,ctx_err)
      
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: file_cellval
    real(kind=8),allocatable ,intent(inout):: model(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                           :: unit_file = 1011
    integer(kind=IKIND_MESH)          :: k,n_cell_expected
    real(kind=8),allocatable          :: model_mesh(:)
    !! -----------------------------------------------------------------
      
    ctx_err%ierr = 0
  
    !! open file 
    open (unit=unit_file,file=trim(trim(adjustl(file_cellval))))
    !! read number of values
    read(unit_file,*) n_cell_expected
    !! must be the same as what we read
    if(n_cell_expected .ne. ctx_domain%n_cell_gb) then
      ctx_err%msg   = "** ERROR: Inconsistant number of values in "//   &
                      trim(adjustl(file_cellval))//" [model_read_ascii] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! read files
    allocate(model_mesh(n_cell_expected)) 
    model_mesh=0.0
    do k=1,n_cell_expected
      read(unit_file,*) model_mesh(k)
    end do
    close(unit_file)
    
    !! send appropriate values
    call project_ascii_global_to_cell_local(ctx_domain,model_mesh,model,ctx_err)
    deallocate(model_mesh)
    
    return
  end subroutine model_read_ascii


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a SEP file with spherical 
  !> symmetry: 
  !> (a) we read a ONE-DIMENSIONAL sep model 
  !> (b) it is projected onto the 2 or 3dimensional grid
  !
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_spherical_full(header_sep,model_sep,n_ref,  &
                                           ref_d,center_point,ctx_err)
    
    implicit none
    
    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model_sep(:)
    integer                  ,intent(out)  :: n_ref
    real(kind=8)             ,intent(out)  :: ref_d
    real(kind=8)             ,intent(out)  :: center_point(3)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                   :: nx,ny,nz,rsize
    integer                   :: path_pos,k
    real(kind=8)              :: xmin,ymin,zmin,dx,dy,dz
    character(len=512)        :: binfile, binformat
    logical                   :: file_exists
    real(kind=4),allocatable  :: model_sep_r4(:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
        
    !! read header sep file
    call read_parameters_sep(trim(adjustl(header_sep)),nx,ny,nz,xmin,  &
                             ymin,zmin,dx,dy,dz,binfile,binformat,     &
                             ctx_err)
    !! add the path to the binary file 
    if(binfile(1:1) .ne. '/')then !! absolute path if it starts with /
      if(binfile(1:2) .eq. './') binfile(1:2) = ''
      path_pos = 0
      do k=1,len_trim(adjustl(header_sep))
       if(header_sep(k:k) == '/') then
         path_pos = k
       end if
      end do
      if(path_pos .ne. 0) then
        binfile=header_sep(1:path_pos) // trim(adjustl(binfile))
      end if
    end if

    !! read global model sep
    !! check file exists
    inquire(file=trim(adjustl(binfile)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Binary file associated to header (in=): " //    &
                   trim(adjustl(binfile)) // " does not exist "  //    &
                   "[model_read_sep] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! we are spherical: the model must be 1D
    if((nx .ne. 1 .and. ny .ne. 1) .or. (nx .ne. 1 .and. nz.ne. 1) .or. &
       (ny .ne. 1 .and. nz .ne. 1)) then
      ctx_err%ierr=-1
      ctx_err%msg ="** The spherical extension requires a " //          &
                   "one-dimenaional sep model [model_read_sep] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    !! We probably want to read by layers for very large files 
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    allocate(model_sep(nx*ny*nz)) !! one dimensional read
    
    select case(trim(adjustl(binformat)))
      case('native_float','float')
        rsize=4
        !! read file 
        allocate(model_sep_r4(nx*ny*nz))
        open(unit=1010, file=trim(adjustl(binfile)), access='direct',  &
             form='unformatted',recl=rsize*nx*ny*nz)
        read (1010,rec=1) model_sep_r4
        close(1010)
        model_sep = dble(model_sep_r4)
        deallocate(model_sep_r4)

      case('double')
        rsize=8
        !! read file 
        open(unit=1010, file=trim(adjustl(binfile)), access='direct',  &
             form='unformatted',recl=rsize*nx*ny*nz)
        read (1010,rec=1) model_sep
        close(1010)

      case default
        ctx_err%msg   = "** ERROR: unrecognized binary format for SEP "// &
                        trim(adjustl(header_sep))// " [model_read_sep] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    !! -----------------------------------------------------------------
    !! convert to local shape, depending on the type of element
    !! -----------------------------------------------------------------
    !! identify the leading dimension 
    if(nx .ne. 1) then
      n_ref   = nx
      ref_d   = dx
    elseif(ny .ne. 1) then
      n_ref   = ny
      ref_d   = dy
    elseif(nz .ne. 1) then
      n_ref   = nz
      ref_d   = dz
    end if

    center_point(1) = dble(xmin)
    center_point(2) = dble(ymin)
    center_point(3) = dble(zmin)
    !! -----------------------------------------------------------------
    return
  end subroutine model_read_sep_spherical_full

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a SEP file with spherical 
  !> symmetry: 
  !> (a) we read a ONE-DIMENSIONAL sep model 
  !> (b) it is projected onto the 2 or 3dimensional grid
  !
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_spherical_with_projection(ctx_domain,       &
                                                      header_sep,model, &
                                                      ctx_err)
    
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                   :: n_ref
    real(kind=8)              :: ref_d
    real(kind=8)              :: center_point(3)
    real(kind=8),allocatable  :: model_sep(:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
       
    !! read sep file to get the infos 
    call model_read_sep_spherical_full(header_sep,model_sep,n_ref,ref_d,&
                                       center_point,ctx_err)

    !! -----------------------------------------------------------------
    call project_sep_global_spherical_to_cell_local(ctx_domain,         &
                                              center_point,n_ref,ref_d, &
                                              model_sep,model,ctx_err)
    deallocate(model_sep)
    !! -----------------------------------------------------------------
    return
  end subroutine model_read_sep_spherical_with_projection

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a SEP file with spherical 
  !> symmetry: 
  !> (a) we read a ONE-DIMENSIONAL sep model 
  !> (b) it is projected onto the 2 or 3dimensional grid, 
  !> we extend to 0 if the cell mesh is not found in 
  !> the sep.
  !
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_spherical_with_projection_extend0(           &
                                           ctx_domain,header_sep,model,  &
                                           ctx_err)
    
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                   :: n_ref
    real(kind=8)              :: ref_d
    real(kind=8)              :: center_point(3)
    real(kind=8),allocatable  :: model_sep(:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
       
    !! read sep file to get the infos 
    call model_read_sep_spherical_full(header_sep,model_sep,n_ref,ref_d,&
                                       center_point,ctx_err)

    !! -----------------------------------------------------------------
    call project_sep_global_spherical_to_cell_local_extend0(ctx_domain, &
                                              center_point,n_ref,ref_d, &
                                              model_sep,model,ctx_err)
    deallocate(model_sep)
    !! -----------------------------------------------------------------
    return
  end subroutine model_read_sep_spherical_with_projection_extend0

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a SEP file, using a spherical
  !> coordinates system in (r phi theta).
  !
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_spherical3d_with_projection(ctx_domain,     &
                                                        header_sep,     &
                                                        model,ctx_err)
    
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                   :: nx,ny,nz
    real(kind=8)              :: xmin,ymin,zmin,dx,dy,dz
    real(kind=8),allocatable  :: model_sep(:,:,:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
        
    call model_read_sep_full(header_sep,model_sep,nx,ny,nz,xmin,ymin,   &
                             zmin,dx,dy,dz,ctx_err)
    !! -----------------------------------------------------------------
    !! convert to local shape, depending on the type of element
    !! -----------------------------------------------------------------
    call project_sep_global_spherical3d_to_cell_local(                  &
                                          ctx_domain,xmin,ymin,zmin,    &
                                          nx,ny,nz,dx,dy,dz,model_sep,  &
                                          model,ctx_err)
    deallocate(model_sep)
    !! -----------------------------------------------------------------
    return
  end subroutine model_read_sep_spherical3d_with_projection

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models in the case we read a SEP file, using a spherical
  !> coordinates system in (r phi theta).  
  !> we extend to 0 if the cell mesh is not found in 
  !> the sep.
  !
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    header_sep     : path to sep header
  !> @param[inout] model          : output model coefficients depending
  !>                                on the type of domain (mesh, cartesian)
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_read_sep_spherical3d_with_projection_extend0(        &
                                                        ctx_domain,     &
                                                        header_sep,     &
                                                        model,ctx_err)
    
    implicit none
    type(t_domain)           ,intent(in)   :: ctx_domain
    character(len=*)         ,intent(in)   :: header_sep
    real(kind=8),allocatable ,intent(inout):: model(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer                   :: nx,ny,nz
    real(kind=8)              :: xmin,ymin,zmin,dx,dy,dz
    real(kind=8),allocatable  :: model_sep(:,:,:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr = 0   
        
    call model_read_sep_full(header_sep,model_sep,nx,ny,nz,xmin,ymin,   &
                             zmin,dx,dy,dz,ctx_err)
    !! -----------------------------------------------------------------
    !! convert to local shape, depending on the type of element
    !! -----------------------------------------------------------------
    call project_sep_global_spherical3d_to_cell_local_extend0(          &
                                          ctx_domain,xmin,ymin,zmin,    &
                                          nx,ny,nz,dx,dy,dz,model_sep,  &
                                          model,ctx_err)
    deallocate(model_sep)
    !! -----------------------------------------------------------------
    return
  end subroutine model_read_sep_spherical3d_with_projection_extend0
  
end module m_model_read
