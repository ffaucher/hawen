!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an API to initialize the discretized models
!> for the current propagators (elastic, acoustic, ...)
!
!------------------------------------------------------------------------------
module m_model_init

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_define_precision,         only: IKIND_MESH
  !> init from propagators
  use m_ctx_model,                only: t_model, model_infos
  use m_read_parameter,           only: read_parameter
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,            &
                                        tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_DOF
  !! -------------------------------------------------------------------

  implicit none

  private
  
  public :: model_init, model_init_representation_format

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize models context according to propagator and discretization
  !
  !> @param[inout] ctx_model      : type model
  !> @param[in]    dm             : domain dimension
  !> @param[in]    n_grid_element : number of dof for the model discretization
  !> @param[in]    flag_viscous   : indicates viscosity
  !> @param[inout] viscous_model  : viscosity name
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_init(ctx_model,parameter_file,dm,n_grid_element_loc, &
                        n_grid_element_gb,flag_viscous,viscous_model,ctx_err)
    implicit none
    character(len=*)         ,intent(in)   :: parameter_file
    type(t_model)            ,intent(inout):: ctx_model
    integer                  ,intent(in)   :: dm
    integer(kind=IKIND_MESH) ,intent(in)   :: n_grid_element_loc
    integer(kind=IKIND_MESH) ,intent(in)   :: n_grid_element_gb
    logical                  ,intent(in)   :: flag_viscous
    character(len=32)        ,intent(inout):: viscous_model
    type(t_error)            ,intent(out)  :: ctx_err
    !! -----------------------------------------------------------------
    integer              :: nval
    character(len=512)   :: str
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    ctx_model%dim_domain = dm
    !! one possibility for now...
    call read_parameter(parameter_file,'model_representation',          &
                        'piecewise-constant',str,nval)
    call model_init_representation_format(trim(adjustl(str)),           &
                                          ctx_model%format_disc,ctx_err)
    !! -----------------------------------------------------------------

    !! force int4 here
    ctx_model%n_cell_loc  = int(n_grid_element_loc)
    ctx_model%n_cell_gb   = int(n_grid_element_gb )
    
    ctx_model%flag_viscous = flag_viscous
    ctx_model%viscous_model= trim(adjustl(viscous_model))
    call model_infos(ctx_model%dim_domain,ctx_model%flag_viscous,       &
                     ctx_model%viscous_model,ctx_model%n_model,         &
                     ctx_model%name_model,ctx_err)
    !! set common name
    viscous_model = trim(adjustl(ctx_model%viscous_model))
    return
  end subroutine model_init


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> intialize the model representation format
  !
  !> @param[in]    input name     : str_in
  !> @param[inout] output format  : format_disc
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine model_init_representation_format(str_in,format_disc,ctx_err)
    implicit none
    character(len=*)         ,intent(in)   :: str_in
    character(len=32)        ,intent(out)  :: format_disc
    type(t_error)            ,intent(out)  :: ctx_err
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0

    select case(trim(adjustl(str_in)))
      case('piecewise-constant','PIECEWISE-CONSTANT','constant','CONSTANT')
        format_disc  = tag_MODEL_PCONSTANT
      case('piecewise-polynomial','PIECEWISE-POLYNOMIAL','polynomial')
        format_disc  = tag_MODEL_PPOLY
      case('sep','SEP','sep-original','SEP-ORIGINAL')
        format_disc  = tag_MODEL_SEP
      case('degree-of-freedom','dof','DOF','Degree-of-freedom')
        format_disc  = tag_MODEL_DOF
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** ERROR: unrecognized model representation '"    &
                     //trim(adjustl(str_in))//"' [model_init] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! -----------------------------------------------------------------

    return
  end subroutine model_init_representation_format


end module m_model_init
