!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_model.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for model information
!
!------------------------------------------------------------------------------
module m_print_model

  use m_raise_error,      only : t_error,raise_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  use m_barrier,          only : barrier
  use m_allreduce_max,    only : allreduce_max
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,            &
                           tag_MODEL_PPOLY, tag_MODEL_SEP, tag_MODEL_DOF
  implicit none
  
  private
  public :: print_info_model_load, print_info_model_welcome, print_info_model_end
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print welcome information that we are processing the model
  !
  !> @param[in]    ctx_paral        : context parallelism
  !> @param[in]    nmodel           : number of model
  !> @param[in]    memory_loc       : local memory
  !> @param[inout] ctx_err          : context error
  !----------------------------------------------------------------------------
  subroutine print_info_model_welcome(ctx_paral,nmodel,memory_loc,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    integer                     ,intent(in)   :: nmodel
    integer(kind=8)             ,intent(in)   :: memory_loc
    type(t_error)               ,intent(inout):: ctx_err
    !!
    integer  (kind=8)  :: mem_max
    character(len=64)  :: str

    ctx_err%ierr = 0
    
    call allreduce_max(memory_loc,mem_max,1,ctx_paral%communicator,ctx_err)
    if(ctx_paral%master) then
      write(6,'(2a,i3,a)')   indicator, " loading",nmodel," physical models "
      call cast_memory(mem_max,str)
      if(ctx_paral%nproc.ne.1) then
        write(6,'(2a)')"- max memory on 1 core       : ",trim(adjustl(str))
      else
        write(6,'(2a)')"- models memory              : ",trim(adjustl(str))
      end if
    end if
    return
  end subroutine print_info_model_welcome

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print end information that we are processing the model
  !
  !> @param[in]    ctx_paral        : context parallelism
  !> @param[inout] ctx_err          : context error
  !----------------------------------------------------------------------------
  subroutine print_info_model_end(ctx_paral)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    
    if(ctx_paral%master) then
      write(6,'(a)')   separator
    end if
    
    return
  end subroutine print_info_model_end
                                   
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print information regarding the loaded model 
  !
  !> @param[in]    ctx_paral        : context parallelism
  !> @param[in]    strmodel         : model name (vp, vs, etc)
  !> @param[in]    mformat          : model format (mesh, sep or constant)
  !> @param[in]    mpath            : model file (for mesh and sep)
  !> @param[in]    mcste            : model constant value, if needed
  !> @param[in]    mscale           : model scale
  !> @param[in]    minmodel         : min model value 
  !> @param[in]    maxmodel         : max model value 
  !> @param[in]    flag_minmax      : indicates if min/max values modified
  !> @param[in]    mrepresent_flag  : indicates if special representation
  !> @param[in]    mrepresent_fmt   : indicates criterion representation
  !> @param[in]    mrepresent_tol   : indicates given reduction
  !> @param[in]    mrepresent_x     : x size for structured reduction
  !> @param[in]    mrepresent_y     : y size for structured reduction
  !> @param[in]    mrepresent_z     : z size for structured reduction
  !> @param[in]    mrepresent_nsub  : indicates actual n_subdomain
  !> @param[inout] ctx_err          : context error
  !----------------------------------------------------------------------------
  subroutine print_info_model_load(ctx_paral,format_disc,strmodel,      &
                                   mformat,mpath,mcste,mscale,minmodel, &
                                   maxmodel,flag_minmax,mrepresent_flag,&
                                   mrepresent_fmt,mrepresent_tol,       &
                                   mrepresent_x,mrepresent_y,           &
                                   mrepresent_z,mrepresent_nsub,        &
                                   poly_order,ctx_err)
  
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    character(len=*)            ,intent(in)   :: format_disc
    character(len=*)            ,intent(in)   :: strmodel
    character(len=*)            ,intent(in)   :: mformat
    character(len=*)            ,intent(in)   :: mpath
    character(len=*)            ,intent(in)   :: mrepresent_fmt
    real(kind=8)                ,intent(in)   :: mcste
    real                        ,intent(in)   :: mrepresent_tol
    real(kind=8)                ,intent(in)   :: mscale,minmodel,maxmodel
    real                        ,intent(in)   :: mrepresent_x
    real                        ,intent(in)   :: mrepresent_y
    real                        ,intent(in)   :: mrepresent_z
    logical                     ,intent(in)   :: flag_minmax,mrepresent_flag
    integer(kind=8)             ,intent(in)   :: mrepresent_nsub
    integer                     ,intent(in)   :: poly_order
    type(t_error)               ,intent(inout):: ctx_err

    !! local
    character(len=41)  :: str_intro
    ctx_err%ierr  = 0

    !! print infos depending on format
    str_intro=''

    
    if(flag_minmax .and. ctx_paral%master) then
      write(6,'(a)') ' '
      write(6,'(a)') ' '
      write(6,'(a)') ' ********************************************* '
      write(6,'(a)') ' ********************************************* '
      write(6,'(a)') ' *****               WARNING             ***** '//&
                     '                    WARNING '
      write(6,'(a)') ' ***** min/max values have been modified ***** '//&
                     '                    HERE '
      write(6,'(a)') ' ***** from parameter file infos         ***** '
      write(6,'(a)') ' ********************************************* '
      write(6,'(a)') ' ********************************************* '
      write(6,'(a)') ' '
      write(6,'(a)') ' '
    end if
    
    if(ctx_paral%master) then
      select case( trim(adjustl(format_disc)) )
        case(tag_MODEL_PCONSTANT)
          write(6,'(a)') "- " // trim(adjustl(strmodel))//              &
                         " model using a constant per cell"

        case(tag_MODEL_PPOLY)
          write(6,'(a,i0,a)') "- " // trim(adjustl(strmodel))//         &
                         " model using a polynomial of order ",         &
                         poly_order," per cell"

        case(tag_MODEL_SEP)
          write(6,'(a)') "- " // trim(adjustl(strmodel))//              &
                         " model using the original sep representation"

        case(tag_MODEL_DOF)
          write(6,'(a,i0)') "- " // trim(adjustl(strmodel))//           &
                         " model using Lagrange basis at order ",       &
                         poly_order

        case default
          write(6,'(2a)') "- " // trim(adjustl(strmodel))//             &
                          " model rerepsented with ",trim(adjustl(format_disc))      
      end select
    end if

    select case (trim(adjustl(mformat)))
      case('sep')
        if(ctx_paral%master) then
          str_intro="- sep file model for "//trim(adjustl(strmodel))
          str_intro(40:40)=":"
          write(6,'(2a)') str_intro, trim(adjustl(mpath))
          !! min/max values
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' min: ',minmodel
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' max: ',maxmodel
          if(mscale > 1+tiny(1.d0) .or. mscale < 1-tiny(1.d0)) then
            str_intro="- scale "//trim(adjustl(strmodel))//" model"
            str_intro(40:40)=":"
            write(6,'(a,es12.3)') str_intro, mscale
          end if
        end if
      
      case('sep-spherical1d')
        if(ctx_paral%master) then
          str_intro="- 1d spherical model for "//trim(adjustl(strmodel))
          str_intro(40:40)=":"
          write(6,'(2a)') str_intro, trim(adjustl(mpath))
          !! min/max values
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' min: ',minmodel
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' max: ',maxmodel
          if(mscale > 1+tiny(1.d0) .or. mscale < 1-tiny(1.d0)) then
            str_intro="- scale "//trim(adjustl(strmodel))//" model"
            str_intro(40:40)=":"
            write(6,'(a,es12.3)') str_intro, mscale
          end if
        endif

      case('sep-spherical-coordinates')
        if(ctx_paral%master) then
          str_intro="- spherical sep file model for "//trim(adjustl(strmodel))
          str_intro(40:40)=":"
          write(6,'(2a)') str_intro, trim(adjustl(mpath))
          !! min/max values
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' min: ',minmodel
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' max: ',maxmodel
          if(mscale > 1+tiny(1.d0) .or. mscale < 1-tiny(1.d0)) then
            str_intro="- scale "//trim(adjustl(strmodel))//" model"
            str_intro(40:40)=":"
            write(6,'(a,es12.3)') str_intro, mscale
          end if
        endif
      
      case('ascii')
        if(ctx_paral%master) then
          str_intro="- ascii file model for "//trim(adjustl(strmodel))
          str_intro(40:40)=":"
          write(6,'(2a)') str_intro, trim(adjustl(mpath))
          !! min/max values
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' min: ',minmodel
          write(6,'(a,es12.3)') '    - '//                              &
                          trim(adjustl(strmodel))//' max: ',maxmodel
          if(mscale > 1+tiny(1.d0) .or. mscale < 1-tiny(1.d0)) then
            str_intro="- scale "//trim(adjustl(strmodel))//" model"
            str_intro(40:40)=":"
            write(6,'(a,es12.3)') str_intro, mscale
          end if
        end if
      case('constant')
        if(ctx_paral%master) then
          str_intro="- constant model for "//trim(adjustl(strmodel))
          str_intro(40:40)=":"
          write(6,'(a,es12.3)') str_intro, mcste
        end if
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** Unrecognized model format: " // &
                     trim(adjustl(mformat)) // " [print_model_load] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    !! -----------------------------------------------------------------
    !! model representation 
    if(ctx_paral%master .and. mrepresent_flag) then  
      write(6,'(a)') "    - "//trim(adjustl(strmodel))//         &
                     " compress. criterion : "//trim(adjustl(mrepresent_fmt))
      select case(trim(adjustl(mrepresent_fmt)))
        case('neighbors')
          write(6,'(a,es12.3,a)') "    - "//trim(adjustl(strmodel))//   &
                                " compression/origin  : ",              &
                                mrepresent_tol*100," %"
        case('structured') 
          write(6,'(a,es12.3)')   "    - "//trim(adjustl(strmodel))//     &
                                " structured x size   : ", mrepresent_x
          if(mrepresent_y > 0) &
          write(6,'(a,es12.3)')   "    - "//trim(adjustl(strmodel))//     &
                                " structured y size   : ", mrepresent_y
          if(mrepresent_z > 0) &
          write(6,'(a,es12.3)')   "    - "//trim(adjustl(strmodel))//     &
                                " structured z size   : ", mrepresent_z
         
      end select
      write(6,'(a,i0)') "    - "//trim(adjustl(strmodel))//             &
                        " actual Nsubdomains  : ",mrepresent_nsub
    end if
    !! -----------------------------------------------------------------

    return
    
  end subroutine print_info_model_load

end module m_print_model
