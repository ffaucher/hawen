!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_model.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read parameter file for model infos
!
!------------------------------------------------------------------------------
module m_read_parameters_model
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: read_parameters_model
  public :: read_parameters_model_delta

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters that are required to define the model, we implement
  !> three possibilities: either sep file, cell ascii values or constant.
  !>  
  !> for a model {KEY} we are looking for the following string:
  !> model_sep_{KEY}          => link to SEP file
  !> model_ascii_{KEY}        => link to ASCII cell values
  !> model_constant_{KEY}     => constant model value
  !> model_scale_{KEY}        => scaling model value
  !> model_min_{KEY}          => model min value allowed
  !> model_max_{KEY}          => model max value allowed
  !
  !> @param[in]   parameter_file   : parameter file where infos are
  !> @param[in]   strmodel         : model name (vp, vs, etc)
  !> @param[out]  mformat          : model format (ascii, sep or constant)
  !> @param[out]  mpath            : model file (for ascii and sep)
  !> @param[out]  mcste            : model constant value, if needed
  !> @param[out]  mscale           : model scale
  !> @param[out]  mmin             : model allowed min
  !> @param[out]  mmax             : model allowed max
  !> @param[out]  mrepresent_flag  : indicates if compression is applied
  !> @param[out]  mrepresent_tol   : level of reduction [0, 1]
  !> @param[out]  mrepresent_fmt   : format of compression
  !> @param[out]  mrepresent_lim   : possibility for box size restriction
  !> @param[out]  ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_model(parameter_file,strmodel,mformat,     &
                                   mpath,mcste,mscale,mmin,mmax,        &
                                   mcompression_flag,                   &
                                   mcompression_tol,                    &
                                   mcompression_fmt,                    &
                                   mcompression_limx,                   &
                                   mcompression_limy,                   &
                                   mcompression_limz,                   &
                                   mrepresentation_polynom_order,ctx_err)

    character(len=*)              ,intent(in)   :: parameter_file
    character(len=*)              ,intent(in)   :: strmodel
    character(len=*)              ,intent(inout):: mformat
    character(len=*)              ,intent(inout):: mpath
    real(kind=8)                  ,intent(out)  :: mcste
    real(kind=8)                  ,intent(out)  :: mscale
    real(kind=8)                  ,intent(out)  :: mmin
    real(kind=8)                  ,intent(out)  :: mmax
    logical                       ,intent(out)  :: mcompression_flag
    real                          ,intent(out)  :: mcompression_tol
    character(len=*)              ,intent(inout):: mcompression_fmt
    real                          ,intent(out)  :: mcompression_limx
    real                          ,intent(out)  :: mcompression_limy
    real                          ,intent(out)  :: mcompression_limz
    integer                       ,intent(out)  :: mrepresentation_polynom_order
    type(t_error)                 ,intent(out)  :: ctx_err
    !! local
    character(len=64)  :: keysep, keymesh, keycste, keyscale, keysep1d
    character(len=64)  :: keymin, keymax, keyrepresent, keytol, keysepsph
    character(len=64)  :: keyreplimx, keyreplimy, keyreplimz,keyreporder
    character(len=64)  :: keyrepfmt
    character(len=512) :: str_read
    integer            :: nval
    integer            :: intlist(512)
    real(kind=8)       :: lreal(512)
    logical            :: file_exists,model_found,llist(512),flag_found

    ctx_err%ierr  = 0

    !! initialize output
    mformat = ''
    mpath   = ''
    mcste   = 0.0
    mscale  = 1.0
    model_found = .false.
    !! initialize strings to search
    keysep   ="model_sep_"                // trim(adjustl(strmodel))
    keysep1d ="model_sep-spherical1D_"    // trim(adjustl(strmodel))
    keysepsph="model_sep-spherical_"      // trim(adjustl(strmodel))
    keymesh  ="model_ascii_"              // trim(adjustl(strmodel))
    keycste  ="model_constant_"           // trim(adjustl(strmodel))
    keyscale ="model_scale_"              // trim(adjustl(strmodel))
    
    !! for the model representation/compression 
    keyrepresent="model_representation_compression_"       //trim(adjustl(strmodel))
    keytol      ="model_representation_compression_coeff_" //trim(adjustl(strmodel))
    keyrepfmt   ="model_representation_compression_format_"//trim(adjustl(strmodel))
    keyreplimx  ="model_representation_compression_boxx_"  //trim(adjustl(strmodel))
    keyreplimy  ="model_representation_compression_boxy_"  //trim(adjustl(strmodel))
    keyreplimz  ="model_representation_compression_boxz_"  //trim(adjustl(strmodel))
    keyreporder ="model_representation_polynomial_order_"  //trim(adjustl(strmodel))
    
    !! min and max models for FWI only
    keymin  ="model_min_"      // trim(adjustl(strmodel))
    keymax  ="model_max_"      // trim(adjustl(strmodel))

    !! -----------------------------------------------------------------
    !! priority to the sep format 
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,trim(adjustl(keysep)),'',mpath,nval)
    
    if(trim(adjustl(mpath)) .ne. '') then
      !! check file exists
      inquire(file=trim(adjustl(mpath)),exist=file_exists)
      if(.not. file_exists) then
        ctx_err%ierr=-1
        ctx_err%msg ="** SEP file "// trim(adjustl(mpath)) //"for model "// &
                     trim(adjustl(strmodel)) // " DOEST NOT EXIST "      // &
                     "[read_parameter_model] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      else 
        model_found=.true.
        mformat    = 'sep'
      endif
    end if

    !! -----------------------------------------------------------------
    !! if not, maybe spherical sep file 
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keysepsph)),'',mpath,nval)
      
      if(trim(adjustl(mpath)) .ne. '') then
        !! check file exists
        inquire(file=trim(adjustl(mpath)),exist=file_exists)
        if(.not. file_exists) then
          ctx_err%ierr=-1
          ctx_err%msg ="** SEP file "// trim(adjustl(mpath)) //"for model "// &
                       trim(adjustl(strmodel)) // " DOEST NOT EXIST "      // &
                       "[read_parameter_model] "
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        else 
          model_found=.true.
          mformat    = 'sep-spherical-coordinates'
        endif
      end if
    endif
    
    !! -----------------------------------------------------------------
    !! if not, maybe 1D spherical sep file 
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keysep1d)),'',mpath,nval)
      
      if(trim(adjustl(mpath)) .ne. '') then
        !! check file exists
        inquire(file=trim(adjustl(mpath)),exist=file_exists)
        if(.not. file_exists) then
          ctx_err%ierr=-1
          ctx_err%msg ="** SEP file "// trim(adjustl(mpath)) //"for model "// &
                       trim(adjustl(strmodel)) // " DOEST NOT EXIST "      // &
                       "[read_parameter_model] "
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        else 
          model_found=.true.
          mformat    = 'sep-spherical1d'
        endif
      end if
    endif

    !! -----------------------------------------------------------------
    !! if not sep, maybe mesh (i.e. a list of ascii values)
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keymesh)),'',mpath,nval)
      if(trim(adjustl(mpath)) .ne. '') then
        !! check file exists
        inquire(file=trim(adjustl(mpath)),exist=file_exists)
        if(.not. file_exists) then
          ctx_err%ierr=-1
          ctx_err%msg ="** ASCII file "// trim(adjustl(mpath)) //"for model "// &
                       trim(adjustl(strmodel)) // " DOEST NOT EXIST "        // &
                       "[read_parameter_model] "
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        else 
          model_found=.true.
          mformat    = 'ascii'
        endif
      end if
    end if
    
    !! -----------------------------------------------------------------
    !! constant is the last resort
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keycste)),0.d0,   &
                          lreal,nval,o_flag_found=flag_found)
      ! not necessarily > 0, in particular for helioseismic parameters
      if(flag_found) then
        mcste   = lreal(1)
        mformat = 'constant'
        model_found=.true.
      end if
    end if

    !! error if not found
    if (.not. model_found) then
      ctx_err%ierr=-1
      ctx_err%msg ="** No information found to load model " //  &
                   trim(adjustl(strmodel)) // " [read_parameter_model]"
      ctx_err%critic=.true.
      call raise_error(ctx_err)    
    end if


    !! -----------------------------------------------------------------
    !! We can control the model compression to reduce the number of 
    !! unknowns. 
    !! -----------------------------------------------------------------
    mcompression_flag = .false.
    mcompression_tol  = 1.0
    mcompression_fmt  = ''
    mcompression_limx = 0.
    mcompression_limy = 0.
    mcompression_limz = 0.
    mrepresentation_polynom_order = 0

    !! only used if piecewise-polynomial representation 
    call read_parameter(parameter_file,trim(adjustl(keyreporder)),0,    &
                        intlist,nval)
    mrepresentation_polynom_order  = intlist(1)
    
    
    call read_parameter(parameter_file,trim(adjustl(keyrepresent)),     &
                        .false.,llist,nval)
    mcompression_flag  = llist(1)
    !! should be between 0 and 1, 1 means no reduction...
    call read_parameter(parameter_file,trim(adjustl(keytol)),1.d0,lreal,nval)
    mcompression_tol =real(lreal(1))
    if(.not. mcompression_flag)  mcompression_tol =1.
    if(mcompression_tol  >= 1) then
      mcompression_tol = 1.
      mcompression_flag=.false.
    endif
    !! format can be neighbors or boxed/structured criterion
    call read_parameter(parameter_file,trim(adjustl(keyrepfmt)),'neigh',&
                        str_read,nval)
    mcompression_fmt = trim(adjustl(str_read))
    !! only used for box criterion .....................................
    call read_parameter(parameter_file,trim(adjustl(keyreplimx)),0.d0,lreal,nval)
    mcompression_limx =real(lreal(1))
    call read_parameter(parameter_file,trim(adjustl(keyreplimy)),0.d0,lreal,nval)
    mcompression_limy =real(lreal(1))
    call read_parameter(parameter_file,trim(adjustl(keyreplimz)),0.d0,lreal,nval)
    mcompression_limz =real(lreal(1))
    
    !! -----------------------------------------------------------------
    !! model scale
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,trim(adjustl(keyscale)),1.d0,lreal,nval)
    !! can be < 0 in particular for the helioseismic parameters.
    if(abs(lreal(1)) > tiny(1.d0)) then
      mscale = lreal(1)
    else
      ctx_err%ierr=-1
      ctx_err%msg ="** Cannot use scaling parameter set to 0.0 "    //  &
                   " in [read_parameter_model]"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! -----------------------------------------------------------------
    !! model min and max for FWI
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,trim(adjustl(keymin)),-huge(1.d0),lreal,nval)
    mmin = lreal(1)
    call read_parameter(parameter_file,trim(adjustl(keymax)),huge(1.d0),lreal,nval)
    mmax = lreal(1)
    if(mmin > mmax) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Cannot have min > max for model "            //  &
                   trim(adjustl(strmodel)) // " in [read_parameter_model]"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    return
  end subroutine read_parameters_model

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters that are required to define a perturbation of model, 
  !> this is optional so it may remain empty.
  !>  
  !> for a model {KEY} we are looking for the following string:
  !> model_sep_{KEY}          => link to SEP file
  !> model_ascii_{KEY}        => link to ASCII cell values
  !> model_constant_{KEY}     => constant model value
  !> model_scale_{KEY}        => scaling model value
  !
  !> @param[in]   parameter_file   : parameter file where infos are
  !> @param[in]   strmodel         : model name (vp, vs, etc)
  !> @param[out]  mformat          : model format (ascii, sep or constant)
  !> @param[out]  mpath            : model file (for ascii and sep)
  !> @param[out]  mcste            : model constant value, if needed
  !> @param[out]  mscale           : model scale
  !> @param[out]  ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_model_delta(parameter_file,strmodel,mformat,&
                                         mpath,mcste,mscale,mmin,mmax,ctx_err)

    character(len=*)              ,intent(in)   :: parameter_file
    character(len=*)              ,intent(in)   :: strmodel
    character(len=*)              ,intent(inout):: mformat
    character(len=*)              ,intent(inout):: mpath
    real(kind=8)                  ,intent(out)  :: mcste
    real(kind=8)                  ,intent(out)  :: mscale
    real(kind=8)                  ,intent(out)  :: mmin
    real(kind=8)                  ,intent(out)  :: mmax
    type(t_error)                 ,intent(out)  :: ctx_err
    !! local
    character(len=64)  :: keysep, keymesh, keycste, keysepsph
    character(len=64)  :: keymin, keymax, keyscale, keysep1d
    integer            :: nval
    real(kind=8)       :: lreal(512)
    logical            :: file_exists, model_found

    ctx_err%ierr  = 0
    
    !! initialize output
    mformat = ''
    mpath   = ''
    mcste   = 0.0
    mscale  = 1.0
    model_found = .false.
    
    !! initialize strings to search
    keysep   ="model_sep_"                // trim(adjustl(strmodel))
    keysep1d ="model_sep-spherical1D_"    // trim(adjustl(strmodel))
    keysepsph="model_sep-spherical_"      // trim(adjustl(strmodel))
    keymesh  ="model_ascii_"              // trim(adjustl(strmodel))
    keycste  ="model_constant_"           // trim(adjustl(strmodel))
    keyscale ="model_scale_"              // trim(adjustl(strmodel))
    !! min and max models for FWI only
    keymin  ="model_min_"      // trim(adjustl(strmodel))
    keymax  ="model_max_"      // trim(adjustl(strmodel))

    !! -----------------------------------------------------------------
    !! priority to the sep format 
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,trim(adjustl(keysep)),'',mpath,nval)
    
    if(trim(adjustl(mpath)) .ne. '') then
      !! check file exists
      inquire(file=trim(adjustl(mpath)),exist=file_exists)
      if(.not. file_exists) then
        ctx_err%ierr=-1
        ctx_err%msg ="** SEP file "// trim(adjustl(mpath)) //"for model "// &
                     trim(adjustl(strmodel)) // " DOEST NOT EXIST "      // &
                     "[read_parameter_model] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      else 
        model_found=.true.
        mformat    = 'sep'
      endif
    end if

    !! -----------------------------------------------------------------
    !! if not, maybe spherical sep file 
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keysepsph)),'',mpath,nval)
      
      if(trim(adjustl(mpath)) .ne. '') then
        !! check file exists
        inquire(file=trim(adjustl(mpath)),exist=file_exists)
        if(.not. file_exists) then
          ctx_err%ierr=-1
          ctx_err%msg ="** SEP file "// trim(adjustl(mpath)) //"for model "// &
                       trim(adjustl(strmodel)) // " DOEST NOT EXIST "      // &
                       "[read_parameter_model] "
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        else 
          model_found=.true.
          mformat    = 'sep-spherical-coordinates'
        endif
      end if
    endif

    !! -----------------------------------------------------------------
    !! if not, maybe 1D spherical sep file 
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keysep1d)),'',mpath,nval)
      
      if(trim(adjustl(mpath)) .ne. '') then
        !! check file exists
        inquire(file=trim(adjustl(mpath)),exist=file_exists)
        if(.not. file_exists) then
          ctx_err%ierr=-1
          ctx_err%msg ="** SEP file "// trim(adjustl(mpath)) //"for model "// &
                       trim(adjustl(strmodel)) // " DOEST NOT EXIST "      // &
                       "[read_parameter_model] "
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        else 
          model_found=.true.
          mformat    = 'sep-spherical1d'
        endif
      end if
    endif

    !! -----------------------------------------------------------------
    !! if not sep, maybe mesh (i.e. a list of ascii values)
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keymesh)),'',mpath,nval)
      if(trim(adjustl(mpath)) .ne. '') then
        !! check file exists
        inquire(file=trim(adjustl(mpath)),exist=file_exists)
        if(.not. file_exists) then
          ctx_err%ierr=-1
          ctx_err%msg ="** ASCII file "// trim(adjustl(mpath)) //"for model "// &
                       trim(adjustl(strmodel)) // " DOEST NOT EXIST "        // &
                       "[read_parameter_model] "
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        else 
          model_found=.true.
          mformat    = 'ascii'
        endif
      end if
    end if
    
    !! -----------------------------------------------------------------
    !! constant is the last resort
    !! -----------------------------------------------------------------
    if(.not. model_found) then
      call read_parameter(parameter_file,trim(adjustl(keycste)),-1d0,lreal,nval)
      if(lreal(1) >= 0.) then
        mcste   = lreal(1)
        mformat = 'constant'
        model_found=.true.
      end if
    end if

    !! If the model is not found, we force the perturbation delta to 0.
    if (.not. model_found) then
      ! ctx_err%ierr=-1
      ! ctx_err%msg ="** No information found to load model " //  &
      !              trim(adjustl(strmodel)) // " [read_parameter_model]"
      ! ctx_err%critic=.true.
      ! call raise_error(ctx_err)
      mcste   = 0.d0
      mformat = 'constant'
    end if

    !! -----------------------------------------------------------------
    !! model scale
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,trim(adjustl(keyscale)),1.d0,lreal,nval)
    if(lreal(1) > 0.) then
      mscale = lreal(1)
    else
      ctx_err%ierr=-1
      ctx_err%msg ="** Negative scale value for model "             //  &
                   trim(adjustl(strmodel)) // " in [read_parameter_model]"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! -----------------------------------------------------------------
    !! model min and max for FWI
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,trim(adjustl(keymin)),-huge(1.d0),lreal,nval)
    mmin = lreal(1)
    call read_parameter(parameter_file,trim(adjustl(keymax)),huge(1.d0),lreal,nval)
    mmax = lreal(1)
    if(mmin > mmax) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Cannot have min > max for model "            //  &
                   trim(adjustl(strmodel)) // " in [read_parameter_model]"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    return
  end subroutine read_parameters_model_delta

end module m_read_parameters_model
