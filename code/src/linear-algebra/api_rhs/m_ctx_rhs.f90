!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_rhs.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the type for rhs which can be strored either
!> in DENSE or CSR format
!
!------------------------------------------------------------------------------
module m_ctx_rhs

  use m_raise_error,      only : t_error, raise_error
  use m_define_precision, only : IKIND_MAT, RKIND_MAT
    
  implicit none
  private
  public :: t_rhs, rhs_clean, rhs_clean_allocation
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_rhs contains info on the rhs in sparse or dense format
  !
  !> utils:
  !> @param[string]  rhs_format        : format of rhs, sparse or dense
  !> @param[integer] n_rhs_in_group    : number of rhs per group
  !> @param[integer] i_group           : current group
  !> @param[integer] n_group           : number of rhs groups
  !> @param[integer] n_rhs_component   : number of component 
  !> @param[integer] i_component       : index of component
  !> @param[integer] i_component_bound : index of a boundary component
  !> @param[integer] csri              : array for the CSR Indexes
  !> @param[integer] icol              : array for the CSR col
  !> @param[complex] Rval              : array for the non-zeros coefficients.
  !> @param[real]    timer_init        : time to init rhs
  !> @param[real]    timer_create      : time to create rhs.
  !----------------------------------------------------------------------------
  type t_rhs

    !! format: dense or sparse
    character(len=16)                  :: rhs_format
    !! size of the groups
    integer                ,allocatable:: n_rhs_in_group(:)
    integer                            :: i_group
    !! number of group
    integer                            :: n_group
    !! number of component which have a rhs and their index
    !! (associated with solution components or variable for
    !! boundary rhs)
    integer                            :: n_rhs_component
    integer                ,allocatable:: i_component(:) 
    integer                ,allocatable:: i_component_boundary(:) 
    !! number of rhs in current subdomain 
    !! integer                         :: n_rhs_in_subdomain

    !! line and columns nnz indexes for sparse CSR format
    integer(kind=IKIND_MAT),allocatable:: csri(:)
    integer(kind=IKIND_MAT),allocatable:: icol(:)
    !! nnz values, which is the whole array if dense format
    complex(kind=RKIND_MAT),allocatable:: Rval(:)
    
    !! timers
    real(kind=8)                       :: timer_init
    real(kind=8)                       :: timer_create

  end type t_rhs
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean rhs information
  !
  !> @param[inout] ctx_rhs         : RHS context
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine rhs_clean(ctx_rhs,ctx_err)
    implicit none
    type(t_rhs)            ,intent(inout):: ctx_rhs
    type(t_error)          ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0
    
    ctx_rhs%rhs_format     =''
    ctx_rhs%i_group        = 0
    ctx_rhs%n_group        = 0
    ctx_rhs%n_rhs_component= 0
    ctx_rhs%timer_init     = 0.d0
    ctx_rhs%timer_create   = 0.d0
    
    if(allocated(ctx_rhs%n_rhs_in_group))  deallocate(ctx_rhs%n_rhs_in_group)    
    if(allocated(ctx_rhs%i_component   ))  deallocate(ctx_rhs%i_component   )
    if(allocated(ctx_rhs%csri          ))  deallocate(ctx_rhs%csri)    
    if(allocated(ctx_rhs%icol          ))  deallocate(ctx_rhs%icol)  
    if(allocated(ctx_rhs%Rval          ))  deallocate(ctx_rhs%Rval)    
    if(allocated(ctx_rhs%i_component_boundary))  &
      deallocate(ctx_rhs%i_component_boundary)

    return
  end subroutine rhs_clean
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean allocated rhs information for multi-group
  !
  !> @param[inout] ctx_rhs         : RHS context
  !----------------------------------------------------------------------------
  subroutine rhs_clean_allocation(ctx_rhs)
    implicit none 
    
    type(t_rhs)            ,intent(inout):: ctx_rhs
   
    if(allocated(ctx_rhs%csri))  deallocate(ctx_rhs%csri)    
    if(allocated(ctx_rhs%icol))  deallocate(ctx_rhs%icol)  
    if(allocated(ctx_rhs%Rval))  deallocate(ctx_rhs%Rval)

    return
  end subroutine rhs_clean_allocation

end module m_ctx_rhs
