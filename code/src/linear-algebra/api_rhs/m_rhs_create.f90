!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_rhs_create.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an api to create the RHS 
!> depending on the type of methods (DG, FD)
!
!------------------------------------------------------------------------------
module m_rhs_create

  !! module used -------------------------------------------------------
  use m_raise_error,                 only: raise_error, t_error
  use m_define_precision,            only: RKIND_MAT
  use m_ctx_parallelism,             only: t_parallelism
  use m_time,                        only: time
  use m_ctx_acquisition,             only: t_acquisition
  !> 
  use m_ctx_domain,                  only: t_domain
  use m_ctx_model,                   only: t_model
  use m_ctx_discretization,          only: t_discretization
  use m_ctx_rhs,                     only: t_rhs
  use m_print_rhs,                   only: print_info_rhs_time
  !> to initialize infos 
  use m_discretization_acquisition,  only: discretization_acquisition
  use m_discretization_rhs,          only: discretization_rhs_sparse,    &
                                           discretization_rhs_field_full,&
                                           discretization_rhs_backward_field_full
  use m_ctx_model_representation,    only: t_model_param
  !! -------------------------------------------------------------------

  implicit none

  private
  public :: rhs_create
  public :: rhs_create_field

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create RHS from acquisition
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    ctx_aq         : context acquisition
  !> @param[in]    ctx_domain     : type domain
  !> @param[inout] ctx_disc       : type discretization
  !> @param[inout] ctx_rhs        : type rhs
  !> @param[in]    freq           : frequency for plane-wave only
  !> @param[in]    tag_scale_rhs  : indicates if scaling the RHS
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine rhs_create(ctx_paral,ctx_aq,ctx_domain,ctx_disc,ctx_rhs,   &
                        freq,tag_scale_rhs,ctx_err)
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_acquisition)      ,intent(in)   :: ctx_aq
    type(t_domain)           ,intent(in)   :: ctx_domain
    type(t_discretization)   ,intent(inout):: ctx_disc
    type(t_rhs)              ,intent(inout):: ctx_rhs
    complex(kind=8)          ,intent(in)   :: freq
    integer(kind=4)          ,intent(in)   :: tag_scale_rhs
    type(t_error)            ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer         :: nrhs, i_src_first, i_src_last, k
    complex(kind=8) :: source_val
    real   (kind=8) :: time0,time1
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0
    ctx_rhs%timer_init   = 0.d0
    ctx_rhs%timer_create = 0.d0
    call time(time0) 
    
    !! -----------------------------------------------------------------
    !! 0) we identify the number of rhs 
    !! -----------------------------------------------------------------
    nrhs   = ctx_rhs%n_rhs_in_group(ctx_rhs%i_group)
    i_src_first = 1
    do k = 2, ctx_rhs%i_group
      i_src_first = i_src_first + ctx_rhs%n_rhs_in_group(k-1)
    end do
    i_src_last  = i_src_first + nrhs - 1
   
    !! -----------------------------------------------------------------
    !! 1) we initialize the discretization infos 
    !!    depending on the type of source, it will be a dense or a 
    !!    sparse format.
    !! -----------------------------------------------------------------
    ctx_rhs%rhs_format=''
    call discretization_acquisition(ctx_paral,ctx_aq,ctx_domain,        &
                                    ctx_disc,nrhs,i_src_first,          &
                                    i_src_last,tag_scale_rhs,           &
                                    ctx_rhs%rhs_format,freq,ctx_err)
    call time(time1)
    ctx_rhs%timer_init = time1 - time0
    
    call time(time0)
    !! -----------------------------------------------------------------
    !! 2) we create a sparse RHS only for now, using the specific
    !!    method (e.g., HDG). 
    !! -----------------------------------------------------------------
    source_val = ctx_aq%src_info%current_val
    select case(trim(adjustl(ctx_rhs%rhs_format)))
      case('sparse')
        call discretization_rhs_sparse(ctx_paral,ctx_disc,ctx_domain,   &
                                       nrhs,ctx_rhs%n_rhs_component,    &
                                       ctx_rhs%i_component,             &
                                       ctx_rhs%i_component_boundary,    &
                                       source_val,                      &
                                       ctx_rhs%csri,ctx_rhs%icol,       &
                                       ctx_rhs%Rval,ctx_err)
    
      case default
        ctx_err%msg   = "** ERROR: Unrecognized RHS format  [rhs_create] **"
        ctx_err%ierr  = -5
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    call time(time1)
    ctx_rhs%timer_create = time1 - time0
    
    !! print infos on screen
    call print_info_rhs_time(ctx_paral,ctx_rhs%timer_init,              &
                             ctx_rhs%timer_create,ctx_err)
    return
  end subroutine rhs_create

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create RHS from acquisition
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    ctx_aq         : context acquisition
  !> @param[in]    ctx_domain     : type domain
  !> @param[inout] ctx_disc       : type discretization
  !> @param[inout] ctx_rhs        : type rhs
  !> @param[in]    ctx_model_param: input model parameter used for rhs
  !> @param[in]    field          : input wavefield used for rhs 
  !> @param[in]    format_model   : format for the input model parameter
  !> @param[in]    flag_backward  : flag we compute for the conjugate
  !> @param[in]    freq           : frequency for plane-wave only
  !> @param[in]    tag_scale_rhs  : indicates if scaling the RHS
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine rhs_create_field(ctx_paral,ctx_domain,ctx_disc,ctx_rhs,    &
                              ctx_model_param_real,                     &
                              ctx_model_param_imag,                     &
                              field,format_model,flag_backward,         &
                              freq,tag_scale_rhs,ctx_err)
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_domain)           ,intent(in)   :: ctx_domain
    type(t_discretization)   ,intent(inout):: ctx_disc
    type(t_rhs)              ,intent(inout):: ctx_rhs
    type(t_model_param)      ,intent(in)   :: ctx_model_param_real
    type(t_model_param)      ,intent(in)   :: ctx_model_param_imag
    complex(kind=RKIND_MAT)  ,intent(in)   :: field(:,:)
    character(len=*)         ,intent(in)   :: format_model  
    logical                  ,intent(in)   :: flag_backward  
    complex(kind=8)          ,intent(in)   :: freq
    integer(kind=4)          ,intent(in)   :: tag_scale_rhs
    type(t_error)            ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer         :: nrhs, i_src_first, i_src_last, k
    complex(kind=8) :: source_val
    real   (kind=8) :: time0,time1
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0
    ctx_rhs%timer_init   = 0.d0
    ctx_rhs%timer_create = 0.d0
    call time(time0) 
    
    !! -----------------------------------------------------------------
    !! 0) we identify the number of rhs 
    !! -----------------------------------------------------------------
    nrhs   = ctx_rhs%n_rhs_in_group(ctx_rhs%i_group)
    i_src_first = 1
    do k = 2, ctx_rhs%i_group
      i_src_first = i_src_first + ctx_rhs%n_rhs_in_group(k-1)
    end do
    i_src_last  = i_src_first + nrhs - 1
   
    !! -----------------------------------------------------------------
    !! 1) we initialize the RHS in a dense formal
    !! -----------------------------------------------------------------
    ctx_rhs%rhs_format='dense'
    source_val = 1.d0 !! nothing to scale with.
    
    select case(trim(adjustl(ctx_rhs%rhs_format)))
      case('dense')
        if(.not. flag_backward) then !! forward problem
          call discretization_rhs_field_full(ctx_paral,ctx_disc,        &
                                             ctx_domain,                &
                                             ctx_model_param_real,      &
                                             ctx_model_param_imag,field,&
                                             ctx_rhs%n_rhs_component,   &
                                             ctx_rhs%i_component,       &
                                             format_model,              &
                                             i_src_first,i_src_last,    &
                                             tag_scale_rhs,nrhs,        &
                                             ctx_rhs%Rval,              &
                                             ctx_err)
        else !! backward problem
          call discretization_rhs_backward_field_full(ctx_paral,ctx_disc, &
                                             ctx_domain,                &
                                             ctx_model_param_real,      &
                                             ctx_model_param_imag,field,&
                                             ctx_rhs%n_rhs_component,   &
                                             ctx_rhs%i_component,       &
                                             format_model,              &
                                             i_src_first,i_src_last,    &
                                             tag_scale_rhs,nrhs,        &
                                             ctx_rhs%Rval,              &
                                             ctx_err)
        
        end if
      case default
        ctx_err%msg   = "** ERROR: Unrecognized RHS format [rhs_create_field] **"
        ctx_err%ierr  = -5
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    call time(time1)
    ctx_rhs%timer_create = time1 - time0
    
    !! print infos on screen
    call print_info_rhs_time(ctx_paral,ctx_rhs%timer_init,              &
                             ctx_rhs%timer_create,ctx_err)
    return
  end subroutine rhs_create_field

end module m_rhs_create
