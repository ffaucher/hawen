!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_rhs_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module intializes the type for rhs depending on the equation
!
!------------------------------------------------------------------------------
module m_rhs_init

  use m_raise_error,         only : t_error, raise_error
  use m_ctx_acquisition,     only : t_acquisition
  use m_read_parameter,      only : read_parameter
  use m_ctx_rhs,             only : t_rhs

  implicit none
  private
  public :: rhs_init,rhs_init_backward
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init rhs information, we basically get the index (in terms of 
  !> dimension of the solution) where to put the RHS. We need to 
  !> check that the acquisition source component (e.g., pressure, vx, ...)
  !> is consistent with the current equation (the input field 
  !> name_solution refers all possible solution names).
  !> We must have the same for the variable names (numerical traces
  !> in HDG), in case of a boundary source.
  !
  !> @param[in]    ctx_aq          : Acquisition context
  !> @param[inout] ctx_rhs         : RHS context
  !> @param[in]    parameter_file  : parameter file
  !> @param[in]    dim_solution    : dimension solution  (number of fields)
  !> @param[in]    dim_var         : dimension variables (for HDG)
  !> @param[in]    name_solution   : list of solution name (e.g.,p,vx,vz,...)
  !> @param[in]    name_var        : list of variables name (e.g.,p,vx,vz,...)
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine rhs_init(ctx_aq,ctx_rhs,parameter_file,dim_solution,       &
                      dim_var,name_solution,name_var,ctx_err)
    type(t_acquisition)         ,intent(in)   :: ctx_aq
    type(t_rhs)                 ,intent(inout):: ctx_rhs
    integer                     ,intent(in)   :: dim_solution
    integer                     ,intent(in)   :: dim_var
    character(len=*)            ,intent(in)   :: parameter_file
    character(len=*)            ,intent(in)   :: name_solution(:)
    character(len=*)            ,intent(in)   :: name_var(:)
    type(t_error)               ,intent(out)  :: ctx_err
    !! local
    integer            :: int_list(512),nrhs,nval,irhs,isol
    logical            :: flag_found

    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! get number of rhs per group
    call read_parameter(parameter_file,'parallel_rhs',1,int_list,nval)
    nrhs = min(int_list(1),ctx_aq%nsrc_loc)
    if(nrhs <= 0) then
      ctx_err%msg   = "** ERROR: need at least 1 rhs per solve [rhs_init] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if 
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! setup group count
    ctx_rhs%n_group = ceiling(real(ctx_aq%nsrc_loc)/real(nrhs))
    allocate(ctx_rhs%n_rhs_in_group(ctx_rhs%n_group))
    ctx_rhs%n_rhs_in_group = nrhs
    !! adjust last group
    ctx_rhs%n_rhs_in_group(ctx_rhs%n_group) = ctx_aq%nsrc_loc - &
                                             (ctx_rhs%n_group-1)*nrhs
    ctx_rhs%i_group = 0
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! number of component which have a rhs and their index (associated 
    !! with solution components)
    !! we need the same for the variables (traces) in case of 
    !! boundary source for first order formulation.
    ctx_rhs%n_rhs_component = ctx_aq%ncomponent_src
    allocate(ctx_rhs%i_component         (ctx_rhs%n_rhs_component))
    allocate(ctx_rhs%i_component_boundary(ctx_rhs%n_rhs_component))
    ctx_rhs%i_component_boundary = 0 !! not all exist
    !! -----------------------------------------------------------------
    !! we use the ctx_equation%name_sol which contains the actual
    !! name of the solution and the proper index
    !! for instance,
    !! 2nd order method means that the rhs can be
    !!   - pressure            in acoustic
    !!   - displacement{x,y,z} in elastic
    !! 1st order, it means that the rhs can be 
    !!   - p,vx,vy,vz            in acoustic
    !!   - vx,vy,vz,sxx,...      in elastic
    !! -----------------------------------------------------------------
    do irhs = 1, ctx_rhs%n_rhs_component
      !! it is ok not to find the traces, because we may not have 
      !! a boundary source.
      flag_found = .false.
      do isol =1, dim_var
        if(ctx_aq%src_component(irhs) .eq. name_var(isol) .and.         &
           (.not. flag_found)) then
          ctx_rhs%i_component_boundary(irhs) = isol
          flag_found                         =.true.
        end if
      end do

      flag_found = .false.
      do isol =1, dim_solution
        if(ctx_aq%src_component(irhs) .eq. name_solution(isol) .and.    &
           (.not. flag_found)) then
          ctx_rhs%i_component(irhs) = isol
          flag_found                =.true.
        end if
      end do
      
      if(.not. flag_found) then
        ctx_err%msg   = "** ERROR: Source component name "           // &
                        trim(adjustl(ctx_aq%src_component(irhs)))    // &
                        " cannot be found for current equation [rhs_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end do
    
    !! allocation will be done when creating the rhs in case it is CSR
    
    return
  end subroutine rhs_init

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init backward rhs information, it is done in two steps: 
  !>   1) copy from input rhs array (i.e. n of parallel rhs)
  !>   2) all component of the solution will have the values
  !>      (it can be zero, it only depends on the misfit)
  !
  !> @param[inout] ctx_rhs         : RHS context
  !> @param[in]    dim_solution    : dimension solution (number of fields)
  !> @param[in]    n_group         : n rhs group
  !> @param[in]    n_rhs_in_group  : n rhs in every group
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine rhs_init_backward(ctx_rhs,n_group,n_rhs_in_group,          &
                               dim_solution,ctx_err)
    type(t_rhs)                 ,intent(inout):: ctx_rhs
    integer                     ,intent(in)   :: n_group
    integer,allocatable         ,intent(in)   :: n_rhs_in_group(:)
    integer                     ,intent(in)   :: dim_solution
    type(t_error)               ,intent(out)  :: ctx_err
    !! local
    integer            :: irhs
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! copy number of rhs in group and number of group infos
    ctx_rhs%n_group           = n_group
    allocate(ctx_rhs%n_rhs_in_group(ctx_rhs%n_group))
    ctx_rhs%n_rhs_in_group(:) = n_rhs_in_group(:)
    ctx_rhs%i_group            = 0
    
    !! -----------------------------------------------------------------
    !! for the backward, all solution components are taken into account
    !! -----------------------------------------------------------------
    ctx_rhs%n_rhs_component = dim_solution
    allocate(ctx_rhs%i_component(ctx_rhs%n_rhs_component))

    !! -----------------------------------------------------------------
    !! we use the ctx_equation%name_sol which contains the actual
    !! name of the solution and the proper index
    !! for instance,
    !! 2nd order method means that the rhs can be
    !!   - pressure            in acoustic
    !!   - displacement{x,y,z} in elastic
    !! 1st order, it means that the rhs can be 
    !!   - p,vx,vy,vz            in acoustic
    !!   - vx,vy,vz,sxx,...      in elastic
    !! -----------------------------------------------------------------
    do irhs = 1, dim_solution
      ctx_rhs%i_component(irhs) = irhs
    end do
    
    return
  end subroutine rhs_init_backward

end module m_rhs_init
