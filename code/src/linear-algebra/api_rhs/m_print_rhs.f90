!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_rhs.f90
!
!> @author 
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for the right-hand sides
!
!------------------------------------------------------------------------------
module m_print_rhs

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  use m_barrier,          only : barrier

  implicit none
  
  private
  public :: print_info_rhs_time
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> time info to initialize and create the rhs.
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   time_init     : time to initialize
  !> @param[in]   time_create   : time to create
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_rhs_time(ctx_paral,time_init,time_create,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    real(kind=8)       ,intent(in)   :: time_init
    real(kind=8)       ,intent(in)   :: time_create
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    character(len=64) :: str_time
    
    ctx_err%ierr  = 0
    
    if(ctx_paral%master) then
      call cast_time(time_init  ,str_time)
      write(6,'(2a)') "- time to initialize the RHS : ",trim(adjustl(str_time))
      call cast_time(time_create,str_time)
      write(6,'(2a)') "- time to create     the RHS : ",trim(adjustl(str_time))
    endif
    
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_rhs_time

end module m_print_rhs
