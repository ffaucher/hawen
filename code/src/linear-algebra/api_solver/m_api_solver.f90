!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_solver.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to define the context for the solver, we allow:
!>  - MUMPS Complex simple precision
!>  - MUMPS Complex double precision
!
!------------------------------------------------------------------------------
module m_api_solver

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_time,                   only: time
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_matrix,             only: t_matrix
  use m_ctx_rhs,                only: t_rhs
  use m_solver_mumps_init,      only: mumps_init_options,               &
                                      mumps_init_default_options,       &
                                      mumps_update_lowrank_option,      &
                                      mumps_get_lowrank_option
  use m_solver_mumps_facto,     only: mumps_factorization
  use m_solver_mumps_clean,     only: mumps_clean
  use m_solver_mumps_solve,     only: mumps_solve
  use m_define_precision,       only: RKIND_MAT

  implicit none 
  !> mumps include: simple of double precision complex
  include 'cmumps_struc.h'
  include 'zmumps_struc.h'
  !! ------------------------------------------------------------------- 

  private
  public :: t_solver, solver_init_options, process_factorization,       &
            process_solve, solver_clean, solver_init_default_options,   &
            solver_get_lowrank, solver_update_lowrank

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_solver contains info on the choice of solver
  !
  !> utils:
  !> @param[string]  name_solver  : name of the solver,
  !> @param[mumps]   ctx_mumps_c  : context mumps simple precision complex,
  !> @param[mumps]   ctx_mumps_z  : context mumps double precision complex.
  !----------------------------------------------------------------------------
  type t_solver
    !! only mumps for now
    character(len=32)    :: name_solver = 'MUMPS'
    
    !! the monoproc solution after mumps
    complex(kind=RKIND_MAT), allocatable :: solution_global(:)
    
    !! list the possible solver: 
    !! - Mumps complex simple precision
    !! - Mumps complex double precision
    !! -----------------------------------------------------------------

    !! Mumps complex simple precision
    type(cmumps_struc)   :: ctx_mumps_c

    !! Mumps complex simple precision
    type(zmumps_struc)   :: ctx_mumps_z

    !! -----------------------------------------------------------------
    !! indicates if the analysis phase has been done or not (only once
    !! per matrix)
    !! -----------------------------------------------------------------
    logical       :: flag_analysis

    !! -----------------------------------------------------------------
    !! indicates if blocks are used
    !! -----------------------------------------------------------------
    logical       :: flag_block

    !! -----------------------------------------------------------------
    !! indicates if we repeat analysis phase for all call
    !! -----------------------------------------------------------------
    logical       :: flag_force_analysis

    !! timing & solver
    integer(kind=8)  :: mem_facto
    integer(kind=8)  :: mem_solve_monoproc
    real   (kind=8)  :: time_facto
    real   (kind=8)  :: time_solve
  end type t_solver
  
  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Update lowrank option values, only works with Mumps.
  !
  !> @param[inout] ctx_solver     : context solver
  !> @param[in]    lowrank        : low-rank option
  !> @param[in]    lowrank_tol    : low-rank tolerance
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine solver_update_lowrank(ctx_solver,lowrank,lowrank_tol,ctx_err)
  
    implicit none

    type(t_solver)           ,intent(inout) :: ctx_solver
    integer                  ,intent(in)    :: lowrank
    real                     ,intent(in)    :: lowrank_tol
    type(t_error)            ,intent(inout) :: ctx_err
    
    ctx_err%ierr  = 0

    select case (trim(adjustl(ctx_solver%name_solver)))
      case ('MUMPS')
        call  mumps_update_lowrank_option(ctx_solver%ctx_mumps_c,       &
                                          ctx_solver%ctx_mumps_z,       &
                                          lowrank,lowrank_tol,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Solver not recognized [update_lowrank] **"
        ctx_err%ierr  = -211
        ctx_err%critic=.true.
        call raise_error(ctx_err)      

    end select

    return

  end subroutine solver_update_lowrank

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Get lowrank option values, only works with Mumps.
  !
  !> @param[inout] ctx_solver     : context solver
  !> @param[out]   lowrank        : low-rank option
  !> @param[out]   lowrank_tol    : low-rank tolerance
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine solver_get_lowrank(ctx_solver,lowrank,lowrank_tol,ctx_err)
  
    implicit none

    type(t_solver)           ,intent(inout) :: ctx_solver
    integer                  ,intent(out)   :: lowrank
    real                     ,intent(out)   :: lowrank_tol
    type(t_error)            ,intent(inout) :: ctx_err
    
    ctx_err%ierr  = 0

    select case (trim(adjustl(ctx_solver%name_solver)))
      case ('MUMPS')
        call  mumps_get_lowrank_option(ctx_solver%ctx_mumps_c,          &
                                       ctx_solver%ctx_mumps_z,          &
                                       lowrank,lowrank_tol,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Solver not recognized [get_lowrank] **"
        ctx_err%ierr  = -211
        ctx_err%critic=.true.
        call raise_error(ctx_err)      

    end select

    return

  end subroutine solver_get_lowrank
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize solver main options
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    symmetry       : to indicate symmetry or not
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] parameter_file : parameter file where to read options
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine solver_init_options(ctx_paral,symmetry,ctx_solver,      &
                                 parameter_file,ctx_err)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_solver)           ,intent(inout):: ctx_solver
    logical                  ,intent(in)   :: symmetry
    character(len=*)         ,intent(in)   :: parameter_file
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    logical :: symmetric_matrix
    
    ctx_err%ierr  = 0
    ctx_solver%flag_block    = .false. 

    symmetric_matrix = symmetry
    ctx_solver%flag_analysis = .false. !> analysis not done so far.

    select case (trim(adjustl(ctx_solver%name_solver)))
      case ('MUMPS')
        call mumps_init_options(ctx_paral,ctx_solver%ctx_mumps_c,       &
                                ctx_solver%ctx_mumps_z,parameter_file,  &
                                symmetric_matrix,ctx_solver%flag_block, &
                                ctx_solver%flag_force_analysis,ctx_err)
        
      case default
        ctx_err%msg   = "** ERROR: Solver not recognized [init_options] **"
        ctx_err%ierr  = -211
        ctx_err%critic=.true.
        call raise_error(ctx_err)      
    end select
    return
  end subroutine solver_init_options

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize solver main options using the default values:
  !> we do not look into the parameter files
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    symmetry       : to indicate symmetry or not
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine solver_init_default_options(ctx_paral,symmetry,ctx_solver, &
                                         verb_level,ctx_err)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_solver)           ,intent(inout):: ctx_solver
    logical                  ,intent(in)   :: symmetry
    integer                  ,intent(in)   :: verb_level
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    logical :: symmetric_matrix
    
    ctx_err%ierr  = 0
    ctx_solver%flag_block = .false.
    
    symmetric_matrix = symmetry
    ctx_solver%flag_analysis = .false. !> analysis not done so far.

    select case (trim(adjustl(ctx_solver%name_solver)))
      case ('MUMPS')
        call mumps_init_default_options(ctx_paral,ctx_solver%ctx_mumps_c,&
                                        ctx_solver%ctx_mumps_z,          &
                                        symmetric_matrix,verb_level,     &
                                        ctx_solver%flag_block,           &
                                        ctx_solver%flag_force_analysis,ctx_err)
        
      case default
        ctx_err%msg   = "** ERROR: Solver not recognized [init_options] **"
        ctx_err%ierr  = -211
        ctx_err%critic=.true.
        call raise_error(ctx_err)      
    end select
    return
  end subroutine solver_init_default_options

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> api factorization
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_matrix     : context matrix for factorization
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_verb    : verbosity level
  !----------------------------------------------------------------------------
  subroutine process_factorization(ctx_paral,ctx_solver,ctx_matrix,     &
                                   ctx_err,o_flag_verb)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_solver)           ,intent(inout):: ctx_solver
    type(t_matrix)           ,intent(inout):: ctx_matrix
    type(t_error)            ,intent(inout):: ctx_err
    logical      ,optional   ,intent(in)   :: o_flag_verb
    !!
    real(kind=8)             :: time0, time1
    logical                  :: flag_verb

    ctx_err%ierr  = 0
    call time(time0)
    
    flag_verb = .true.
    if(present(o_flag_verb)) flag_verb = o_flag_verb
    !! reset timing 
    ctx_solver%time_facto = 0.0
    ctx_solver%time_solve = 0.0
    ctx_solver%mem_solve_monoproc = 0
    select case (trim(adjustl(ctx_solver%name_solver)))
      case ('MUMPS')
        !! adjust depending if we force analysis or not 
        if(ctx_solver%flag_force_analysis)  ctx_solver%flag_analysis = .false.
        call mumps_factorization(ctx_paral,ctx_solver%ctx_mumps_c,      &
                                 ctx_solver%ctx_mumps_z,ctx_matrix,     &
                                 ctx_solver%mem_facto,                  &
                                 ctx_solver%flag_analysis,ctx_err,      &
                                 o_flag_verb=flag_verb)

      case default
        ctx_err%msg   = "** ERROR: Solver not recognized [factorization] **"
        ctx_err%ierr  = -211
        ctx_err%critic=.true.
        call raise_error(ctx_err)      
    end select

    !! update time spent in solving
    call time(time1)
    ctx_solver%time_facto = (time1-time0)

    return
  end subroutine process_factorization

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> api solve
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_rhs        : context for rhs
  !> @param[inout] flag_adjoint   : flag for adjoint problem
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_verb    : verbosity level
  !----------------------------------------------------------------------------
  subroutine process_solve(ctx_paral,ctx_solver,ctx_rhs,flag_adjoint,   &
                           ctx_err,o_flag_verb)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_solver)           ,intent(inout):: ctx_solver
    type(t_rhs)              ,intent(inout):: ctx_rhs
    logical                  ,intent(in)   :: flag_adjoint
    type(t_error)            ,intent(inout):: ctx_err
    logical      ,optional   ,intent(in)   :: o_flag_verb
    !!
    real   (kind=8)          :: time0, time1
    integer(kind=8)          :: mem_solve
    logical                  :: flag_verb

    ctx_err%ierr  = 0
    call time(time0)

    mem_solve     = 0     
    flag_verb = .true.
    if(present(o_flag_verb)) flag_verb = o_flag_verb
    if(allocated(ctx_solver%solution_global)) &
       deallocate(ctx_solver%solution_global)

    select case (trim(adjustl(ctx_solver%name_solver)))
      case ('MUMPS')
        call mumps_solve(ctx_paral,ctx_solver%ctx_mumps_c,              &
                         ctx_solver%ctx_mumps_z,ctx_rhs,flag_adjoint,   &
                         ctx_solver%solution_global,                    &
                         ctx_solver%mem_solve_monoproc,ctx_err,         &
                         o_flag_verb=flag_verb)
      case default
        ctx_err%msg   = "** ERROR: Solver not recognized [factorization] **"
        ctx_err%ierr  = -211
        ctx_err%critic=.true.
        call raise_error(ctx_err)      
    end select
 
    ctx_solver%mem_solve_monoproc = max(mem_solve,ctx_solver%mem_solve_monoproc)
    call time(time1)
    ctx_solver%time_solve = ctx_solver%time_solve + (time1-time0)
 
    return
  end subroutine process_solve

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Clean solver infos
  !
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine solver_clean(ctx_solver,ctx_err)
  
    implicit none
    type(t_solver)           ,intent(inout):: ctx_solver
    type(t_error)            ,intent(inout):: ctx_err

    ctx_err%ierr  = 0
    
    if(allocated(ctx_solver%solution_global)) deallocate(ctx_solver%solution_global)
    ctx_solver%time_facto = 0.
    ctx_solver%time_solve = 0.
    ctx_solver%mem_facto  = 0
    ctx_solver%mem_solve_monoproc = 0
    ctx_solver%flag_block = .false.
    ctx_solver%flag_force_analysis = .false.

    select case (trim(adjustl(ctx_solver%name_solver)))
      case ('MUMPS')
        call mumps_clean(ctx_solver%ctx_mumps_c,ctx_solver%ctx_mumps_z,ctx_err)
        
      case default
        ctx_err%msg   = "** ERROR: Solver not recognized [solver_clean] **"
        ctx_err%ierr  = -211
        ctx_err%critic=.true.
        call raise_error(ctx_err)      
    end select
    return
  end subroutine solver_clean

end module m_api_solver
