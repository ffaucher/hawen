!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an API to initialize discretization infos for 
!> the matrix (e.g., the PML coefficient depending on the wavelength) 
!> and estimate the number of non-zeros in the matrix.
!>
!------------------------------------------------------------------------------
module m_matrix_init

  !! module used -------------------------------------------------------
  use m_raise_error,                only: raise_error, t_error
  use m_ctx_parallelism,            only: t_parallelism
  use m_ctx_equation,               only: t_equation
  use m_define_precision,           only: IKIND_MAT, RKIND_MAT
  use m_time,                       only: time
  use m_allreduce_min,              only: allreduce_min
  !> 
  use m_ctx_domain,                 only: t_domain
  use m_ctx_model,                  only: t_model
  use m_ctx_discretization,         only: t_discretization
  use m_ctx_matrix,                 only: t_matrix, matrix_allocate
  use m_print_matrix,               only: print_info_matrix_init
  use m_discretization_matrix_init, only: discretization_matrix_init
  use m_wavelength_utils,           only: compute_wavelength_dof_order
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: matrix_init

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> init NNZ matrix informations
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    ctx_eq         : type equation
  !> @param[in]    ctx_domain     : type domain
  !> @param[in]    tag_equation   : type equation
  !> @param[in]    tag_method     : discretization method
  !> @param[in]    dm_sol         : solution dimension
  !> @param[inout] ctx_matrix     : type matrix
  !> @param[in]    flag_block     : indicates if block are needed
  !> @param[inout] ctx_model      : type model parameters
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_verb_lvl     : verbose level
  !----------------------------------------------------------------------------
  subroutine matrix_init(ctx_paral,ctx_equation,ctx_domain,ctx_model,   &
                         ctx_discretization,ctx_matrix,flag_block,      &
                         ctx_err,o_verb_lvl)
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_domain)           ,intent(inout):: ctx_domain
    type(t_model)            ,intent(in)   :: ctx_model
    type(t_discretization)   ,intent(inout):: ctx_discretization
    type(t_matrix)           ,intent(inout):: ctx_matrix
    logical                  ,intent(in)   :: flag_block
    type(t_equation)         ,intent(in)   :: ctx_equation
    type(t_error)            ,intent(inout):: ctx_err
    integer,    optional     ,intent(in)   :: o_verb_lvl
    !! local
    real(kind=8)            :: wavelength_min,wavelength_max
    real(kind=8),allocatable:: ndof_min_per_order(:)
    integer                 :: verb_lvl,dim_solution
    integer(kind=8)         :: size_mat_loc,size_mat_gb,nnz_loc,nnz_gb
    logical                 :: flag_wl=.true.,flag_external_pml=.false.
    logical                 :: flag_symmetric_matrix
    integer                 :: size_pml_ix(2),size_pml_iy(2),size_pml_iz(2)
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0
    
    flag_symmetric_matrix = ctx_equation%symmetric_matrix
    dim_solution          = ctx_equation%dim_solution

    verb_lvl = 1
    if(present(o_verb_lvl)) verb_lvl=o_verb_lvl

    !! init timing for this matrix 
    ctx_matrix%time_create = 0.0
    !! -----------------------------------------------------------------
    !! count the number of nonzeros, we init the (potentially
    !! external) PML as well, depending on the wavelength
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! 1) print wavelength information depending on the equation
    allocate(ndof_min_per_order(ctx_domain%order_max))
    wavelength_min=0.0
    wavelength_max=0.0
    !! only if there is a Fourier frequency ?
    if( abs(aimag(ctx_equation%current_angular_frequency)) > tiny(1.0)) then
      flag_wl=.true.
      !! wavelength
      call compute_wavelength_dof_order(ctx_paral,ctx_model,ctx_domain, &
                                        ctx_discretization,ctx_equation,&
                                        ndof_min_per_order,             &
                                        wavelength_min,wavelength_max,ctx_err)
    else
      !! ---------------------------------------------------------------
      !! for Laplace domain, the wavelength is not very important
      !! as it will be naturally attenuated far from the source
      !! Hence, we take an arbitrary wavelength
      wavelength_min= 0.0 !! arbitrary set to zero 
      flag_wl       =.false.
    end if
    !! save in context
    ctx_discretization%wavelength_min = wavelength_min
    ctx_discretization%wavelength_max = wavelength_max
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 2) estimates number of nonzeros 
    !! Note that it potentially initializes the external PML as well as
    !! the blocks
    !! -----------------------------------------------------------------
    ctx_matrix%flag_block = flag_block
    call discretization_matrix_init(ctx_paral,ctx_discretization,       &
                                    ctx_domain,dim_solution,            &
                                    ctx_equation%symmetric_matrix,      &
                                    size_mat_loc,size_mat_gb,           &
                                    nnz_loc,nnz_gb,                     &
                                    ctx_matrix%flag_block,              &
                                    ctx_matrix%nblk,ctx_matrix%blkptr,  &
                                    flag_external_pml,                  &
                                    size_pml_ix,size_pml_iy,size_pml_iz,&
                                    ctx_err,o_wavelength=wavelength_min)
    if (verb_lvl .ne. 0) then
      call print_info_matrix_init(ctx_paral,size_mat_gb,size_mat_loc,   &
                                  nnz_gb,nnz_loc,flag_wl,               &
                                  wavelength_min,wavelength_max,        &
                                  ndof_min_per_order,                   &
                                  ctx_domain%order_max,ctx_err,         &
                                  o_external_pml=flag_external_pml,     &
                                  o_pml_ix=size_pml_ix(:),              &
                                  o_pml_iy=size_pml_iy(:),              &
                                  o_pml_iz=size_pml_iz(:),              &
                                  o_sym=flag_symmetric_matrix)
    end if
    deallocate(ndof_min_per_order)

    !! -----------------------------------------------------------------
    !! 3) allocate matrix context
    !! -----------------------------------------------------------------
    call matrix_allocate(ctx_matrix,size_mat_gb,size_mat_loc,nnz_gb,    &
                         nnz_loc,ctx_err)

    return
  end subroutine matrix_init

end module m_matrix_init
