!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_discretization.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an API to create the discretization matrix depending
!> on the type of methods (DG, FD) and the propagators (elastic, acoustic, ...)
!
!------------------------------------------------------------------------------
module m_matrix_discretization

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_time,                   only: time
  !> context
  use m_ctx_domain,             only: t_domain
  use m_ctx_equation,           only: t_equation
  use m_ctx_model,              only: t_model
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_matrix,             only: t_matrix
  !> matrix creation depends on the operator
  use m_create_matrix,          only: create_matrix
  !> printer
  use m_print_matrix,           only: print_info_matrix_trime_creation
  !! -------------------------------------------------------------------
  implicit none

  private
  public :: matrix_discretization

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create matrix from propagator infos
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    ctx_equation   : context equation
  !> @param[in]    ctx_domain     : type domain
  !> @param[inout] ctx_matrix     : type matrix
  !> @param[inout] ctx_model      : type model parameters
  !> @param[in]    frequency      : frequency
  !> @param[in]    flag_inv       : indicates inversion which 
  !>                                will generate some additional infos
  !>                                in the case of HDG
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_verb_lvl     : verbose level
  !----------------------------------------------------------------------------
  subroutine matrix_discretization(ctx_paral,ctx_equation,ctx_domain,   &
                                   ctx_model,ctx_discretization,        &
                                   ctx_matrix,flag_inv,ctx_err,o_verb_lvl)

    implicit none

    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_domain)           ,intent(in)   :: ctx_domain
    type(t_model)            ,intent(in)   :: ctx_model
    type(t_discretization)   ,intent(inout):: ctx_discretization
    type(t_matrix)           ,intent(inout):: ctx_matrix
    type(t_equation)         ,intent(in)   :: ctx_equation
    logical                  ,intent(in)   :: flag_inv
    type(t_error)            ,intent(inout):: ctx_err
    integer,    optional     ,intent(in)   :: o_verb_lvl
    !! -----------------------------------------------------------------
    integer              :: verb_lvl
    real(kind=8)         :: time1, time0
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    call time(time0)
    verb_lvl = 1
    if(present(o_verb_lvl)) verb_lvl=o_verb_lvl

    !! -----------------------------------------------------------------
    !! create the matrix 
    !! -----------------------------------------------------------------
    call create_matrix(ctx_paral,ctx_equation,ctx_domain,               &
                       ctx_discretization,ctx_model,                    &
                       ctx_matrix%dim_nnz_loc,ctx_matrix%dim_nnz_gb,    &
                       ctx_matrix%ilin,ctx_matrix%icol,ctx_matrix%Aval, &
                       flag_inv,verb_lvl,ctx_err)
    call time(time1)
    ctx_matrix%time_create = time1-time0

    !! print infos 
    if(verb_lvl .ne. 0) then
      call print_info_matrix_trime_creation(ctx_paral,time1-time0,      &
                                            ctx_matrix%dim_nnz_gb,      &
                                            ctx_matrix%dim_nnz_loc,ctx_err)
    end if
    
    return
  end subroutine matrix_discretization

end module m_matrix_discretization
