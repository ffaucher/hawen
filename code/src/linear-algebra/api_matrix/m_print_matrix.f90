!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_matrix.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for matrix
!
!------------------------------------------------------------------------------
module m_print_matrix

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_barrier        ,  only : barrier
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  use m_allreduce_max,    only : allreduce_max
  use m_allreduce_min,    only : allreduce_min
  !> matrix size precision
  use m_define_precision, only : IKIND_MAT, RKIND_MAT
  implicit none
  
  private
  public :: print_info_matrix_init,print_info_matrix_trime_creation
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print welcome info for the mesh
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   size_mat_gb   : global size of the matrix
  !> @param[in]   size_mat_loc  : local size of the sub-matrix
  !> @param[in]   nnz_gb        : global nnz estimates
  !> @param[in]   nnz_loc       : local nnz estimates
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_matrix_init(ctx_paral,size_mat_gb,size_mat_loc, &
                                    nnz_gb,nnz_loc,flag_wl,wl_min,      &
                                    wl_max,nptwl,omax,ctx_err,          &
                                    o_external_pml,o_pml_ix,o_pml_iy,   &
                                    o_pml_iz,o_sym)

    implicit none
    type(t_parallelism)        ,intent(in)   :: ctx_paral
    integer(kind=8)            ,intent(in)   :: size_mat_gb,size_mat_loc
    integer(kind=8)            ,intent(in)   :: nnz_gb,nnz_loc
    logical                    ,intent(in)   :: flag_wl
    real   (kind=8)            ,intent(in)   :: wl_min,wl_max
    real   (kind=8),allocatable,intent(in)   :: nptwl(:)
    integer                    ,intent(in)   :: omax
    type(t_error)              ,intent(out)  :: ctx_err
    logical, optional          ,intent(in)   :: o_external_pml,o_sym
    integer, optional          ,intent(in)   :: o_pml_ix(2),o_pml_iy(2)
    integer, optional          ,intent(in)   :: o_pml_iz(2)
    !! local
    integer  (kind=8)  :: nnz_locmax,size_mat_locmax,nnz_locmin,size_mat_locmin
    integer            :: pml_ix(2),pml_iy(2),pml_iz(2),k
    integer  (kind=8)  :: mem_loc,mem_gb,mem_locmax,mem_locmin
    character(len=7)   :: frmt
    character(len=64)  :: str_mem
    logical            :: external_pml, sym

    ctx_err%ierr  = 0
    
    !! optional infos
    external_pml = .false.
    pml_ix=0 ; pml_iy=0 ; pml_iz=0
    if(present(o_external_pml)) then
      external_pml = o_external_pml
      if(present(o_pml_ix)) pml_ix=o_pml_ix
      if(present(o_pml_iy)) pml_iy=o_pml_iy
      if(present(o_pml_iz)) pml_iz=o_pml_iz
    end if
    sym=.false.
    if(present(o_sym)) then
      sym = o_sym
    end if
    
    !! estimated memory
    mem_loc = (nnz_loc * IKIND_MAT  ) & ! line
            + (nnz_loc * IKIND_MAT  ) & ! col
            + (nnz_loc * 2*RKIND_MAT)   ! Cx values
    mem_gb  = (nnz_gb  * IKIND_MAT  ) & ! line
            + (nnz_gb  * IKIND_MAT  ) & ! col
            + (nnz_gb  * 2*RKIND_MAT)   ! Cx values
    
    frmt='(a,i10)'
    if(nnz_gb > int8(1d9)) then
      frmt='(a,i13)'
      if(nnz_gb > int8(1d12)) then
        frmt='(a,i20)'
      end if
    end if
    
    !! get maximal values and minimal ones
    call allreduce_max(mem_loc,mem_locmax,1,ctx_paral%communicator,ctx_err)    
    call allreduce_max(size_mat_loc,size_mat_locmax,1,                  &
                       ctx_paral%communicator,ctx_err)
    call allreduce_max(nnz_loc,nnz_locmax,1,ctx_paral%communicator,ctx_err)
    call allreduce_min(mem_loc,mem_locmin,1,ctx_paral%communicator,ctx_err)    
    call allreduce_min(size_mat_loc,size_mat_locmin,1,                  &
                       ctx_paral%communicator,ctx_err)
    call allreduce_min(nnz_loc,nnz_locmin,1,ctx_paral%communicator,ctx_err)
    
    !! print infos
    if(ctx_paral%master) then
      if(sym) then
        write(6,'(2a)')   indicator, " SYMMETRIC Matrix processing "
      else
        write(6,'(2a)')   indicator, " NON-SYMMETRIC Matrix processing "
      endif 
      !! additional nodes
      if(external_pml) then
        write(6,'(a)')     "- ADDITIONAL NODES FOR PML: "
        write(6,'(a,2i6)') "-    additional PML X nodes : ", pml_ix
        write(6,'(a,2i6)') "-    additional PML Y nodes : ", pml_iy
        write(6,'(a,2i6)') "-    additional PML Z nodes : ", pml_iz
      end if
      ! ----------------------------------------------------------------
      if(flag_wl) then
        !! wavelength information
        write(6,'(a,2es12.5)')  "- wavelength min/max (SI)    : ",wl_min,wl_max
        if(omax == 1) then
          !! finite differences
          write(6,'(a,es12.5)') "- min dof per wavelength     : ",nptwl(1)
        else
          !! hdg
          write(6,'(a)')        "- min dof per wavelength     : "
          do k=1,omax
            if(nptwl(k) > 0)                                            &
              write(6,'(a,i3,a,es12.5)')                                &
                           "-    order ",k," cells         : ", nptwl(k)
          end do
        end if
      end if        
      ! ----------------------------------------------------------------
      if(ctx_paral%nproc.eq.1) then
        write(6,frmt)    "- matrix global size         : ",size_mat_gb
        write(6,frmt)    "- matrix approx. global nnz  : ",nnz_gb
      else  
        write(6,frmt)    "- matrix global size         : ",size_mat_gb
        write(6,frmt)    "- matrix size max on 1core   : ",size_mat_locmax
        write(6,frmt)    "- matrix size min on 1core   : ",size_mat_locmin
        write(6,frmt)    "- matrix approx. global nnz  : ",nnz_gb
        write(6,frmt)    "- matrix max nnz on 1core    : ",nnz_locmax
        write(6,frmt)    "- matrix min nnz on 1core    : ",nnz_locmin
      endif
      
      call cast_memory(mem_gb,str_mem)
      write(6,'(2a)')  "- global memory for matrix   : ",trim(adjustl(str_mem))

      if(ctx_paral%nproc.ne.1) then
        call cast_memory(mem_locmax,str_mem)
        write(6,'(2a)')"- max memory on 1 core       : ",trim(adjustl(str_mem))
        call cast_memory(mem_locmin,str_mem)
        write(6,'(2a)')"- min memory on 1 core       : ",trim(adjustl(str_mem))

      endif
    endif    
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_matrix_init

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> time info for the matrix creation
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   time_creation : time spent
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_matrix_trime_creation(ctx_paral,time_creation,  &
                                              nnz_gb,nnz_loc,ctx_err)
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    real(kind=8)           ,intent(in)   :: time_creation
    integer(kind=8)        ,intent(in)   :: nnz_gb,nnz_loc
    type(t_error)          ,intent(out)  :: ctx_err
    !!
    character(len=64)       :: str_time 
    integer  (kind=8)       :: nnz_locmax
    character(len=7)        :: frmt
    
    ctx_err%ierr  = 0

    frmt='(a,i10)'
    if(nnz_gb > int8(1d9)) then
      frmt='(a,i13)'
      if(nnz_gb > int8(1d12)) then
        frmt='(a,i20)'
      end if
    end if
    
    call allreduce_max(nnz_loc,nnz_locmax,1,ctx_paral%communicator,ctx_err)
    
    if(ctx_paral%master) then
      call cast_time(time_creation,str_time)
      write(6,'(2a)') "- matrix creation time       : ",trim(adjustl(str_time))
      write(6,frmt)   "- matrix exact global nnz    : ", nnz_gb
      
      if(ctx_paral%nproc.ne.1) then
        write(6,frmt) "- matrix max nnz on 1core    : ", nnz_locmax
      endif
    endif
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_matrix_trime_creation

end module m_print_matrix
