!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_matrix.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the type for matrix
!
!------------------------------------------------------------------------------
module m_ctx_matrix

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_define_precision, only : IKIND_MAT, RKIND_MAT
    
  implicit none
  private
  public :: t_matrix, matrix_clean, matrix_allocate
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_matrix contains info on the matrix
  !
  !> utils:
  !> @param[integer] dim_matrix  : dimension of the matrix,
  !> @param[integer] dim_nnz     : number of non-zeros coefficients,
  !> @param[integer] ilin        : array for the non-zeros lines,
  !> @param[integer] icol        : array for the non-zeros columns,
  !> @param[complex] Aval        : array for the non-zeros coefficients.
  !> @param[logical] flag_block  : indicates if blocks are needed.
  !> @param[integer] nblk        : number of block.
  !> @param[integer] blkptr      : incremental counter of blocks.
  !----------------------------------------------------------------------------
  type t_matrix

    !! dimension of the matrix
    integer(kind=8)            :: dim_matrix_gb
    integer(kind=8)            :: dim_matrix_loc
    !! number of non-zeros coefficients
    integer(kind=8)            :: dim_nnz_gb
    integer(kind=8)            :: dim_nnz_loc
    !! line and columns nnz indexes
    integer(kind=IKIND_MAT),allocatable:: ilin(:)
    integer(kind=IKIND_MAT),allocatable:: icol(:)
    !! nnz values
    complex(kind=RKIND_MAT),allocatable:: Aval(:)
    
    !! in case we need the block structures for factorization
    logical                             :: flag_block
    integer(kind=IKIND_MAT)             :: nblk      !< number of blocks
    integer(kind=IKIND_MAT),allocatable :: blkptr(:) !< incremental array of block size

    !! time to create, factorize and solve linear system the matrix
    real(kind=8)  :: time_create
  end type t_matrix
  
  contains
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init matrix information
  !
  !> @param[inout] ctx_matrix      : matrix context
  !> @param[in]    size_mat        : dimension of the matrix
  !> @param[in]    nnz             : number of non-zeros coeff
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine matrix_allocate(ctx_matrix,size_mat_gb,size_mat_loc,       &
                             nnz_gb,nnz_loc,ctx_err)
    type(t_matrix)         ,intent(inout):: ctx_matrix
    integer(kind=8)        ,intent(in)   :: size_mat_gb,size_mat_loc
    integer(kind=8)        ,intent(in)   :: nnz_gb,nnz_loc
    type(t_error)          ,intent(out)  :: ctx_err
    
    ctx_err%ierr  = 0

    ctx_matrix%dim_matrix_gb  = size_mat_gb 
    ctx_matrix%dim_matrix_loc = size_mat_loc
    ctx_matrix%dim_nnz_gb     = nnz_gb 
    ctx_matrix%dim_nnz_loc    = nnz_loc

    !! allocate variables
    allocate(ctx_matrix%ilin(nnz_loc))
    allocate(ctx_matrix%icol(nnz_loc))
    allocate(ctx_matrix%Aval(nnz_loc))
    
    return
  end subroutine matrix_allocate

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean matrix information
  !
  !> @param[inout] ctx_matrix    : matrix context
  !----------------------------------------------------------------------------
  subroutine matrix_clean(ctx_matrix)
    type(t_matrix)       ,intent(inout)  :: ctx_matrix
    
    ctx_matrix%dim_matrix_gb  = 0
    ctx_matrix%dim_matrix_loc = 0
    ctx_matrix%dim_nnz_gb     = 0
    ctx_matrix%dim_nnz_loc    = 0
    ctx_matrix%nblk           = 0

    !! allocate variables
    if( allocated(ctx_matrix%ilin)  ) deallocate(ctx_matrix%ilin)
    if( allocated(ctx_matrix%icol)  ) deallocate(ctx_matrix%icol)
    if( allocated(ctx_matrix%Aval)  ) deallocate(ctx_matrix%Aval)
    if( allocated(ctx_matrix%blkptr)) deallocate(ctx_matrix%blkptr)

    return
  end subroutine matrix_clean

end module m_ctx_matrix
