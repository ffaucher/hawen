!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_lapack_eigenvalues.f90
!
!> @author
!> F. Faucher [Inria Makutu, University of Pau and Pays de l'Adour]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the eigenvalues of a matrix
!> with LAPACK. Here we rely onto [CDSZ]GEEV routines
!> Documentation: http://www.netlib.org/lapack/explore-html/
!
!------------------------------------------------------------------------------
module m_lapack_eigenvalues
  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error

  implicit none 
  !! ------------------------------------------------------------------- 
  
  interface lapack_matrix_eigenvalues
    module procedure lapack_matrix_ev_double_complex
    module procedure lapack_matrix_ev_simple_complex
    !! note the the real version also returns complex
    !! eigenvalues, thereofore we only provide the 
    !! complex version.
  end interface
   
  private
  public  :: lapack_matrix_eigenvalues
  
  contains 

  !! --------------------------------------------------------------------- %
  !! LAPACK DOCUMENTATION
  !! --------------------------------------------------------------------- %
  !! zgeev()
  !! subroutine zgeev  (character   JOBVL,
  !! character   JOBVR,
  !! integer   N,
  !! complex*16, dimension( lda, * )   A,
  !! integer   LDA,
  !! complex*16, dimension( * )   W,
  !! complex*16, dimension( ldvl, * )   VL,
  !! integer   LDVL,
  !! complex*16, dimension( ldvr, * )   VR,
  !! integer   LDVR,
  !! complex*16, dimension( * )   WORK,
  !! integer   LWORK,
  !! double precision, dimension( * )   RWORK,
  !! integer   INFO 
  !! )    
  !! ZGEEV computes the eigenvalues and, optionally, the left and/or right eigenvectors for GE matrices
  !! 
  !! Download ZGEEV + dependencies [TGZ] [ZIP] [TXT]
  !! 
  !! Purpose:
  !!  ZGEEV computes for an N-by-N complex nonsymmetric matrix A, the
  !!  eigenvalues and, optionally, the left and/or right eigenvectors.
  !! 
  !!  The right eigenvector v(j) of A satisfies
  !!                   A * v(j) = lambda(j) * v(j)
  !!  where lambda(j) is its eigenvalue.
  !!  The left eigenvector u(j) of A satisfies
  !!                u(j)**H * A = lambda(j) * u(j)**H
  !!  where u(j)**H denotes the conjugate transpose of u(j).
  !! 
  !!  The computed eigenvectors are normalized to have Euclidean norm
  !!  equal to 1 and largest component real.
  !! Parameters
  !! [in]  JOBVL  
  !!           JOBVL is CHARACTER*1
  !!           = 'N': left eigenvectors of A are not computed;
  !!           = 'V': left eigenvectors of are computed.
  !! [in]  JOBVR  
  !!           JOBVR is CHARACTER*1
  !!           = 'N': right eigenvectors of A are not computed;
  !!           = 'V': right eigenvectors of A are computed.
  !! [in]  N  
  !!           N is INTEGER
  !!           The order of the matrix A. N >= 0.
  !! [in,out]  A  
  !!           A is COMPLEX*16 array, dimension (LDA,N)
  !!           On entry, the N-by-N matrix A.
  !!           On exit, A has been overwritten.
  !! [in]  LDA  
  !!           LDA is INTEGER
  !!           The leading dimension of the array A.  LDA >= max(1,N).
  !! [out]  W  
  !!           W is COMPLEX*16 array, dimension (N)
  !!           W contains the computed eigenvalues.
  !! [out]  VL  
  !!           VL is COMPLEX*16 array, dimension (LDVL,N)
  !!           If JOBVL = 'V', the left eigenvectors u(j) are stored one
  !!           after another in the columns of VL, in the same order
  !!           as their eigenvalues.
  !!           If JOBVL = 'N', VL is not referenced.
  !!           u(j) = VL(:,j), the j-th column of VL.
  !! [in]  LDVL  
  !!           LDVL is INTEGER
  !!           The leading dimension of the array VL.  LDVL >= 1; if
  !!           JOBVL = 'V', LDVL >= N.
  !! [out]  VR  
  !!           VR is COMPLEX*16 array, dimension (LDVR,N)
  !!           If JOBVR = 'V', the right eigenvectors v(j) are stored one
  !!           after another in the columns of VR, in the same order
  !!           as their eigenvalues.
  !!           If JOBVR = 'N', VR is not referenced.
  !!           v(j) = VR(:,j), the j-th column of VR.
  !! [in]  LDVR  
  !!           LDVR is INTEGER
  !!           The leading dimension of the array VR.  LDVR >= 1; if
  !!           JOBVR = 'V', LDVR >= N.
  !! [out]  WORK  
  !!           WORK is COMPLEX*16 array, dimension (MAX(1,LWORK))
  !!           On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
  !! [in]  LWORK  
  !!           LWORK is INTEGER
  !!           The dimension of the array WORK.  LWORK >= max(1,2*N).
  !!           For good performance, LWORK must generally be larger.
  !! 
  !!           If LWORK = -1, then a workspace query is assumed; the routine
  !!           only calculates the optimal size of the WORK array, returns
  !!           this value as the first entry of the WORK array, and no error
  !!           message related to LWORK is issued by XERBLA.
  !! [out]  RWORK  
  !!           RWORK is DOUBLE PRECISION array, dimension (2*N)
  !! [out]  INFO  
  !!           INFO is INTEGER
  !!           = 0:  successful exit
  !!           < 0:  if INFO = -i, the i-th argument had an illegal value.
  !!           > 0:  if INFO = i, the QR algorithm failed to compute all the
  !!                 eigenvalues, and no eigenvectors have been computed;
  !!                 elements i+1:N of W contain eigenvalues which have
  !!                 converged.
  !!
  !!
  !!
  !! --------------------------------------------------------------------- %

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> eigenvalues using lapack, i.e. sequential. 
  !
  !> @param[in]    A              : matrix
  !> @param[in]    n              : size of the squared matrix
  !> @param[out]   eigenvalues    : eigenvalues
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine lapack_matrix_ev_double_complex(A,n,eigenvalues,ctx_err)
    implicit none

    complex(kind=8)                    ,intent(in)   :: A   (:,:)
    complex(kind=8)                    ,intent(inout):: eigenvalues(:)
    integer                            ,intent(in)   :: n
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer                     :: lwork,ierrloc
    complex(kind=8),allocatable :: Atemp(:,:)
    complex(kind=8),allocatable :: work(:)
    complex(kind=8)             :: VL(1,1) !! unused as we do not need the eigenvectors
    complex(kind=8)             :: VR(1,1) !! unused as we do not need the eigenvectors
    real   (kind=8),allocatable :: rwork(:)

    !! we use a local array to avoid temporary creation when calling
    !! Lapack routines, this can lead to troubles depending on the 
    !! compiler.
    allocate(Atemp(n,n))
    Atemp =  A(1:n,1:n)

    !! create the necessary and options --------------------------------
    lwork = 2*n
    allocate( work(lwork))
    allocate(rwork(2*n  ))
    call zgeev('N','N',n,Atemp,n,eigenvalues,VL,1,VR,1,work,lwork,rwork,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR during Lapack routine ZGEEV in [lapack_ev]"
      return
    end if

    deallocate( work)
    deallocate(rwork)
    deallocate(Atemp)

    return
  end subroutine lapack_matrix_ev_double_complex

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> eigenvalues using lapack, i.e. sequential. 
  !
  !> @param[in]    A              : matrix
  !> @param[in]    n              : size of the squared matrix
  !> @param[out]   eigenvalues    : eigenvalues
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine lapack_matrix_ev_simple_complex(A,n,eigenvalues,ctx_err)
    implicit none

    complex(kind=4)                    ,intent(in)   :: A   (:,:)
    complex(kind=4)                    ,intent(inout):: eigenvalues(:)
    integer                            ,intent(in)   :: n
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer                     :: lwork,ierrloc
    complex(kind=4),allocatable :: Atemp(:,:)
    complex(kind=4),allocatable :: work(:)
    complex(kind=4)             :: VL(1,1) !! unused as we do not need the eigenvectors
    complex(kind=4)             :: VR(1,1) !! unused as we do not need the eigenvectors
    real   (kind=4),allocatable :: rwork(:)

    !! we use a local array to avoid temporary creation when calling
    !! Lapack routines, this can lead to troubles depending on the 
    !! compiler.
    allocate(Atemp(n,n))
    Atemp =  A(1:n,1:n)

    !! create the necessary and options --------------------------------
    lwork = 2*n
    allocate( work(lwork))
    allocate(rwork(2*n  ))
    call cgeev('N','N',n,Atemp,n,eigenvalues,VL,1,VR,1,work,lwork,rwork,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR during Lapack routine ZGEEV in [lapack_ev]"
      return
    end if

    deallocate( work)
    deallocate(rwork)
    deallocate(Atemp)

    return
  end subroutine lapack_matrix_ev_simple_complex
  
end module m_lapack_eigenvalues
