!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_lapack_solve.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to solve linear system with LAPACK
!> Documentation: http://www.netlib.org/lapack/explore-html/
!
!------------------------------------------------------------------------------
module m_lapack_solve
  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error

  implicit none 
  !! ------------------------------------------------------------------- 
  
  interface lapack_solve_system
    module procedure lapack_solve_monorhs_double_complex
    module procedure lapack_solve_monorhs_simple_complex
    module procedure lapack_solve_monorhs_double_real
    module procedure lapack_solve_monorhs_simple_real
    module procedure lapack_solve_multirhs_double_complex
    module procedure lapack_solve_multirhs_simple_complex
    module procedure lapack_solve_multirhs_double_real
    module procedure lapack_solve_multirhs_simple_real
    !! for quadruple we need to force double-precision operations
    !! in lapack.
    module procedure lapack_solve_monorhs_quadruple_complex
  end interface
   
  private
  public  :: lapack_solve_system
  
  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_monorhs_double_complex(A,rhs,nline,ncol,      &
                                                 flag_adjoint,solution, &
                                                 ctx_err)
    implicit none

    complex(kind=8)                                  :: A(:,:)
    complex(kind=8)                                  :: rhs(:)
    integer                            ,intent(in)   :: nline,ncol
    logical                            ,intent(in)   :: flag_adjoint
    complex(kind=8)                    ,intent(inout):: solution(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    complex(kind=8),allocatable :: Aloc(:,:),work(:),rhsloc(:,:)
    integer,parameter           :: nrhs = 1

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call ZGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    call ZGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK ZGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK ZGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol) = rhsloc(1:ncol,1)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_monorhs_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_monorhs_quadruple_complex(A,rhs,nline,ncol,      &
                                                    flag_adjoint,solution, &
                                                    ctx_err)
    implicit none

    complex(kind=16)                                 :: A(:,:)
    complex(kind=16)                                 :: rhs(:)
    integer                            ,intent(in)   :: nline,ncol
    logical                            ,intent(in)   :: flag_adjoint
    complex(kind=16)                   ,intent(inout):: solution(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    complex(kind=8),allocatable :: Aloc(:,:),work(:),rhsloc(:,:)
    integer,parameter           :: nrhs = 1

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc(:,:)   = dcmplx(A  )
    rhsloc(:,1) = dcmplx(rhs)
    lwork=-1
    lda  = nline
    ldb  = nline
    call ZGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc(:,:)   = dcmplx(A  )
    rhsloc(:,1) = dcmplx(rhs)
    call ZGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK ZGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK ZGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol) = rhsloc(1:ncol,1)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_monorhs_quadruple_complex



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_monorhs_simple_complex(A,rhs,nline,ncol,      &
                                                 flag_adjoint,solution, &
                                                 ctx_err)
    implicit none

    complex(kind=4)                                  :: A(:,:)
    complex(kind=4)                                  :: rhs(:)
    integer                            ,intent(in)   :: nline,ncol
    logical                            ,intent(in)   :: flag_adjoint
    complex(kind=4)                    ,intent(inout):: solution(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    complex(kind=4),allocatable :: Aloc(:,:),work(:),rhsloc(:,:)
    integer,parameter           :: nrhs = 1

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call CGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    call CGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK CGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK CGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol) = rhsloc(1:ncol,1)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_monorhs_simple_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_monorhs_double_real(A,rhs,nline,ncol,         &
                                              flag_adjoint,solution,    &
                                              ctx_err)
    implicit none

    real(kind=8)                                     :: A(:,:)
    real(kind=8)                                     :: rhs(:)
    integer                            ,intent(in)   :: nline,ncol
    logical                            ,intent(in)   :: flag_adjoint
    real(kind=8)                       ,intent(inout):: solution(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    real(kind=8),allocatable    :: Aloc(:,:),work(:),rhsloc(:,:)
    integer,parameter           :: nrhs = 1

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call DGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    call DGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK DGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK DGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol) = rhsloc(1:ncol,1)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_monorhs_double_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_monorhs_simple_real(A,rhs,nline,ncol,         &
                                              flag_adjoint,solution,    &
                                              ctx_err)
    implicit none

    real(kind=4)                                     :: A(:,:)
    real(kind=4)                                     :: rhs(:)
    integer                            ,intent(in)   :: nline,ncol
    logical                            ,intent(in)   :: flag_adjoint
    real(kind=4)           ,allocatable,intent(inout):: solution(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    real(kind=4),allocatable    :: Aloc(:,:),work(:),rhsloc(:,:)
    integer,parameter           :: nrhs = 1

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call SGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc(:,:)   = A
    rhsloc(:,1) = rhs
    call SGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK SGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK SGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol) = rhsloc(1:ncol,1)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_monorhs_simple_real


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_multirhs_double_complex(A,rhs,nline,ncol,nrhs,&
                                                  flag_adjoint,solution,&
                                                  ctx_err)
    implicit none

    complex(kind=8)                                  :: A(:,:)
    complex(kind=8)                                  :: rhs(:,:)
    integer                            ,intent(in)   :: nline,ncol,nrhs
    logical                            ,intent(in)   :: flag_adjoint
    complex(kind=8)                    ,intent(inout):: solution(:,:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    complex(kind=8),allocatable :: Aloc(:,:),work(:),rhsloc(:,:)

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call ZGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    call ZGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK ZGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK ZGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol,1:nrhs) = rhsloc(1:ncol,1:nrhs)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_multirhs_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_multirhs_simple_complex(A,rhs,nline,ncol,nrhs,&
                                                  flag_adjoint,solution,&
                                                  ctx_err)
    implicit none

    complex(kind=4)                                  :: A(:,:)
    complex(kind=4)                                  :: rhs(:,:)
    integer                            ,intent(in)   :: nline,ncol,nrhs
    logical                            ,intent(in)   :: flag_adjoint
    complex(kind=4)                    ,intent(inout):: solution(:,:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    complex(kind=4),allocatable :: Aloc(:,:),work(:),rhsloc(:,:)

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call CGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    call CGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK CGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK CGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol,1:nrhs) = rhsloc(1:ncol,1:nrhs)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_multirhs_simple_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_multirhs_double_real(A,rhs,nline,ncol,nrhs,   &
                                               flag_adjoint,solution,   &
                                               ctx_err)
    implicit none

    real(kind=8)                                     :: A(:,:)
    real(kind=8)                                     :: rhs(:,:)
    integer                            ,intent(in)   :: nline,ncol,nrhs
    logical                            ,intent(in)   :: flag_adjoint
    real(kind=8)                       ,intent(inout):: solution(:,:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    real(kind=8),allocatable    :: Aloc(:,:),work(:),rhsloc(:,:)

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call DGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    call DGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK DGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK DGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol,1:nrhs) = rhsloc(1:ncol,1:nrhs)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_multirhs_double_real

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using lapack, i.e. sequential. It allows overdetermined
  !> systems too. 
  !> We solve A X = rhs
  !
  !> @param[in]    A              : matrix
  !> @param[in]    rhs            : rhs
  !> @param[in]    nline          : n matrix lines
  !> @param[in]    ncol           : n matrix col
  !> @param[in]    flag_adjoint   : flag for adjoint problem
  !> @param[inout] solution       : the output solution
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine lapack_solve_multirhs_simple_real(A,rhs,nline,ncol,nrhs,   &
                                               flag_adjoint,solution,   &
                                               ctx_err)
    implicit none

    real(kind=4)                                     :: A(:,:)
    real(kind=4)                                     :: rhs(:,:)
    integer                            ,intent(in)   :: nline,ncol,nrhs
    logical                            ,intent(in)   :: flag_adjoint
    real(kind=4)                       ,intent(inout):: solution(:,:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    character(len=1)            :: opt
    integer                     :: lwork,lda,ldb,ierrloc,n
    real(kind=4),allocatable    :: Aloc(:,:),work(:),rhsloc(:,:)

    !! character for normal or transposed
    if(flag_adjoint) then
      opt='C'
    else
      opt='N'
    end if
    solution = 0.0
    allocate(Aloc  (nline,ncol))
    allocate(rhsloc(nline,nrhs))
    n = max(nline,ncol)
    
    !! -----------------------------------------------------------------------
    !! 1. the first call uses lwork = -1  to obtain the optimal workspace size
    !! -----------------------------------------------------------------------
    allocate(work(n))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    lwork=-1
    lda  = nline
    ldb  = nline
    call SGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)

    !! optional memory
    !! memory_count = (8*2)*nline*ncol  + &  !! A
    !!                (8*2)*nline       + &  !! rhs
    !!                (8*2)*work(1))         !! work
    
    !! -----------------------------------------------------------------
    !! 2. solve with optimal size of work
    !! -----------------------------------------------------------------
    lwork=int(work(1))
    deallocate(work)
    allocate  (work(lwork))
    Aloc  (:,:) = A
    rhsloc(:,:) = rhs
    call SGELS(opt,nline,ncol,nrhs,Aloc,lda,rhsloc,ldb,work,lwork,ierrloc)
    
    !! error return ----------------------------------------------------
    if (ierrloc < 0) then
      ctx_err%ierr = ierrloc
      write(ctx_err%msg,'(a,i0,a)') "** ERROR in LAPACK SGELS: illegal"//&
           " values for input number ",ierrloc, "[lapack_solve] **"
      ctx_err%critic=.true.
      return
    else if(ierrloc > 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR in LAPACK SGELS: zero diagonal " //      &
                     "element [lapack_solve] **"
      ctx_err%critic=.true.
      return
    endif
    
    !! the solution is contained in rhsloc(:,irhs)
    solution(1:ncol,1:nrhs) = rhsloc(1:ncol,1:nrhs)
    
    !! clean
    deallocate(Aloc)
    deallocate(rhsloc)
    deallocate(work)

    return
  end subroutine lapack_solve_multirhs_simple_real

end module m_lapack_solve

