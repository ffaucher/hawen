!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_lapack_inverse.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to inverse a matrix with LAPACK
!> we use the LU decomposition 
!> and then [CDSZ]GETRI routines
!> Documentation: http://www.netlib.org/lapack/explore-html/
!
!------------------------------------------------------------------------------
module m_lapack_inverse
  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error

  implicit none 
  !! ------------------------------------------------------------------- 
  
  interface lapack_matrix_inverse
    module procedure lapack_matrix_inv_double_complex
    module procedure lapack_matrix_inv_simple_complex
    module procedure lapack_matrix_inv_double_real
    module procedure lapack_matrix_inv_simple_real
  end interface
   
  private
  public  :: lapack_matrix_inverse
  
  contains 

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse using lapack, i.e. sequential. 
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[in]    n              : size of the squared matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine lapack_matrix_inv_double_complex(A,Ainv,n,ctx_err)
    implicit none

    complex(kind=8)                    ,intent(in)   :: A   (:,:)
    complex(kind=8)                    ,intent(inout):: Ainv(:,:)
    integer                            ,intent(in)   :: n
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer                     :: lwork,ierrloc
    complex(kind=8),allocatable :: Atemp(:,:)
    complex(kind=8),allocatable :: work(:)
    integer        ,allocatable :: ipiv(:)

    !! we use a local array to avoid temporary creation when calling
    !! Lapack routines, this can lead to troubles depending on the 
    !! compiler.
    allocate(Atemp(n,n))
    Atemp =  A(1:n,1:n)

    !! compute the LU decomposition ------------------------------------
    lwork=n
    allocate(ipiv(lwork))
    call ZGETRF(n,n,Atemp,n,ipiv,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg   = "** ERROR during ZGETRF in [lapack_inverse]"
      return     
    end if
    
    !! compute the inverse using the LU decomposition ------------------
    allocate(work(n))
    call ZGETRI(n,  Atemp,n,ipiv,work,lwork,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR during ZGETRI in [lapack_inverse]"
      return
    end if

    Ainv(1:n,1:n) = Atemp
    deallocate(ipiv)
    deallocate(work)
    deallocate(Atemp)

    return
  end subroutine lapack_matrix_inv_double_complex
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse using lapack, i.e. sequential. 
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[in]    n              : size of the squared matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine lapack_matrix_inv_simple_complex(A,Ainv,n,ctx_err)
    implicit none

    complex(kind=4)                    ,intent(in)   :: A   (:,:)
    complex(kind=4)                    ,intent(inout):: Ainv(:,:)
    integer                            ,intent(in)   :: n
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer                     :: lwork,ierrloc
    complex(kind=4),allocatable :: Atemp(:,:)
    complex(kind=4),allocatable :: work(:)
    integer        ,allocatable :: ipiv(:)

    !! we use a local array to avoid temporary creation when calling
    !! Lapack routines, this can lead to troubles depending on the 
    !! compiler.
    allocate(Atemp(n,n))
    Atemp =  A(1:n,1:n)

    !! compute the LU decomposition ------------------------------------
    lwork=n
    allocate(ipiv(lwork))
    call CGETRF(n,n,Atemp,n,ipiv,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg   = "** ERROR during CGETRF in [lapack_inverse]"
      return     
    end if
    
    !! compute the inverse using the LU decomposition ------------------
    allocate(work(n))
    call CGETRI(n,Atemp,n,ipiv,work,lwork,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR during CGETRI in [lapack_inverse]"
      return
    end if

    Ainv(1:n,1:n) = Atemp
    deallocate(ipiv)
    deallocate(work)
    deallocate(Atemp)

    return
  end subroutine lapack_matrix_inv_simple_complex

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse using lapack, i.e. sequential. 
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[in]    n              : size of the squared matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine lapack_matrix_inv_double_real(A,Ainv,n,ctx_err)
    implicit none

    real(kind=8)                    ,intent(in)   :: A   (:,:)
    real(kind=8)                    ,intent(inout):: Ainv(:,:)
    integer                         ,intent(in)   :: n
    type(t_error)                   ,intent(inout):: ctx_err
    !! local
    integer                     :: lwork,ierrloc
    real(kind=8)   ,allocatable :: Atemp(:,:)
    real(kind=8)   ,allocatable :: work(:)
    integer        ,allocatable :: ipiv(:)

    !! we use a local array to avoid temporary creation when calling
    !! Lapack routines, this can lead to troubles depending on the 
    !! compiler.
    allocate(Atemp(n,n))
    Atemp =  A(1:n,1:n)

    !! compute the LU decomposition ------------------------------------
    lwork=n
    allocate(ipiv(lwork))
    call DGETRF(n,n,Atemp,n,ipiv,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg   = "** ERROR during DGETRF in [lapack_inverse]"
      return     
    end if
    
    !! compute the inverse using the LU decomposition ------------------
    allocate(work(n))
    call DGETRI(n, Atemp,n,ipiv,work,lwork,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR during DGETRI in [lapack_inverse]"
      return
    end if

    Ainv(1:n,1:n) = Atemp
    deallocate(ipiv)
    deallocate(work)
    deallocate(Atemp)

    return
  end subroutine lapack_matrix_inv_double_real
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse using lapack, i.e. sequential. 
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[in]    n              : size of the squared matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine lapack_matrix_inv_simple_real(A,Ainv,n,ctx_err)
    implicit none

    real(kind=4)                    ,intent(in)   :: A   (:,:)
    real(kind=4)                    ,intent(inout):: Ainv(:,:)
    integer                         ,intent(in)   :: n
    type(t_error)                   ,intent(inout):: ctx_err
    !! local
    integer                     :: lwork,ierrloc
    real(kind=4)   ,allocatable :: Atemp(:,:)
    real(kind=4)   ,allocatable :: work(:)
    integer        ,allocatable :: ipiv(:)

    !! we use a local array to avoid temporary creation when calling
    !! Lapack routines, this can lead to troubles depending on the 
    !! compiler.
    allocate(Atemp(n,n))
    Atemp =  A(1:n,1:n)    

    !! compute the LU decomposition ------------------------------------
    lwork=n
    allocate(ipiv(lwork))
    call SGETRF(n,n,Atemp,n,ipiv,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg   = "** ERROR during SGETRF in [lapack_inverse]"
      return     
    end if
    
    !! compute the inverse using the LU decomposition ------------------
    allocate(work(n))
    call SGETRI(n,Atemp,n,ipiv,work,lwork,ierrloc)
    if(ierrloc .ne. 0) then
      ctx_err%ierr = ierrloc
      ctx_err%msg  = "** ERROR during SGETRI in [lapack_inverse]"
      return
    end if

    Ainv(1:n,1:n) = Atemp
    deallocate(ipiv)
    deallocate(work)
    deallocate(Atemp)

    return
  end subroutine lapack_matrix_inv_simple_real

end module m_lapack_inverse
