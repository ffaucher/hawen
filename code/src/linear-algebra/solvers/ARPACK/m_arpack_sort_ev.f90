!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_arpack_sort_ev.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to sort the list of eigenvalues obtained from
!> ARPACK and PARPACK
!
!------------------------------------------------------------------------------
module m_arpack_sort_ev
  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error
  implicit none 
  !! ------------------------------------------------------------------- 
  
  !> to sort the eigenvalues
  interface arpack_sort_eigenvalues
    module procedure sort_eigenvalues_double_complex
    module procedure sort_eigenvalues_simple_complex
  end interface arpack_sort_eigenvalues
  
  private
  public  :: arpack_sort_eigenvalues

  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> find sorting indexes from low to high values
  !
  !> @param[in]    eigenvalues    : list of eigenvalues
  !> @param[in]    nev            : number of ev
  !> @param[in]    index_sort     : sorting index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine sort_eigenvalues_double_complex(eigenvalues,nev,index_sort,ctx_err)
    real(kind=8)                 ,intent(in)    :: eigenvalues(:)
    integer                      ,intent(in)    :: nev
    integer,allocatable          ,intent(inout) :: index_sort(:)
    type(t_error)                ,intent(inout) :: ctx_err
    !! local
    integer         :: k,l
    real(kind=8)    :: vtest,vref
    logical         :: flag_ok,flag
    
    index_sort   = -1
    index_sort(1)=  1

    if(ctx_err%ierr .ne. 0) return
    
    !! check if it is not already ok -----------------------------------
    flag_ok = .true.
    do k=2,nev
      if(eigenvalues(k) < eigenvalues(k-1)) then
        flag_ok=.false.
        exit
      end if
    end do
    if(flag_ok) then
      do k=1,nev
        index_sort(k) = k
      end do
      return
    end if
    
    !! check if it is not reverse --------------------------------------
    flag_ok = .true.
    do k=2,nev
      if(eigenvalues(k) > eigenvalues(k-1)) then
        flag_ok=.false.
        exit
      end if
    end do
    if(flag_ok) then
      do k=nev,1,-1
        index_sort(k) = nev-k+1
      end do
      return
    end if
    
    !! otherwize, we sort the array ------------------------------------
    !! very naive approach here.
    do k=2,nev
      vtest = eigenvalues(k)
      flag  = .false.
      l     = 1
      do while(l<k .and. .not. flag) 
        vref = eigenvalues(index_sort(l))
        if(vtest < vref) then
          flag = .true.
        else
          l=l+1  
        end if
      end do
     
      if(flag) then
        !! move
        index_sort(l+1:k) = index_sort(l:k-1)
        index_sort(l)     = k
      else
        !! new
        index_sort(k)     = k
      end if
    end do

    return
    
  end subroutine sort_eigenvalues_double_complex
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> find sorting indexes from low to high values
  !
  !> @param[in]    eigenvalues    : list of eigenvalues
  !> @param[in]    nev            : number of ev
  !> @param[in]    index_sort     : sorting index
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine sort_eigenvalues_simple_complex(eigenvalues,nev,index_sort,ctx_err)
    real(kind=4)                 ,intent(in)    :: eigenvalues(:)
    integer                      ,intent(in)    :: nev
    integer,allocatable          ,intent(inout) :: index_sort(:)
    type(t_error)                ,intent(inout) :: ctx_err
    !! local
    integer         :: k,l
    real(kind=4)    :: vtest,vref
    logical         :: flag_ok,flag
    
    index_sort   = -1
    index_sort(1)=  1

    if(ctx_err%ierr .ne. 0) return
    
    !! check if it is not already ok -----------------------------------
    flag_ok = .true.
    do k=2,nev
      if(eigenvalues(k) < eigenvalues(k-1)) then
        flag_ok=.false.
        exit
      end if
    end do
    if(flag_ok) then
      do k=1,nev
        index_sort(k) = k
      end do
      return
    end if
    
    !! check if it is not reverse --------------------------------------
    flag_ok = .true.
    do k=2,nev
      if(eigenvalues(k) > eigenvalues(k-1)) then
        flag_ok=.false.
        exit
      end if
    end do
    if(flag_ok) then
      do k=nev,1,-1
        index_sort(k) = nev-k+1
      end do
      return
    end if
    
    !! otherwize, we sort the array ------------------------------------
    !! very naive approach here.
    do k=2,nev
      vtest = eigenvalues(k)
      flag  = .false.
      l     = 1
      do while(l<k .and. .not. flag) 
        vref = eigenvalues(index_sort(l))
        if(vtest < vref) then
          flag = .true.
        else
          l=l+1  
        end if
      end do
     
      if(flag) then
        !! move
        index_sort(l+1:k) = index_sort(l:k-1)
        index_sort(l)     = k
      else
        !! new
        index_sort(k)     = k
      end if
    end do

    return
    
  end subroutine sort_eigenvalues_simple_complex


end module m_arpack_sort_ev

