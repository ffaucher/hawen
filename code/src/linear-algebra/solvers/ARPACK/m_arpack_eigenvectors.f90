!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_arpack_eigenvectors.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the eigenvectors and eigenvalues 
!> of a matrix using sequential library ARPACK that can be found 
!> at 
!> https://www.caam.rice.edu/software/ARPACK/
!> for details on subroutine, check the package installation folder,
!> or the websites 
!>
! https://people.sc.fsu.edu/~jburkardt%20/f_src/arpack/arpack.f90 
!  and
! http://trac.alcf.anl.gov/projects/libs/browser/ARPACK/SRC/ssaupd.f?rev=65&order=size
!
!! ---------------------------------------------------------------------
!> NOTE THAT Symmetric complex and symmetric double complex do not 
!>           exist in ARPACK; we rely on routines:
!> - [sdzc]naupd  :: symmetric
!> - [sd]saupd    :: non-symmetric
!
!------------------------------------------------------------------------------
module m_arpack_eigenvectors
  !! module used -------------------------------------------------------
  use m_define_precision,       only: RKIND_MAT,IKIND_MAT
  use m_parallelism_init,       only: init_parallelism_selfproc
  use m_raise_error,            only: t_error
  use m_arpack_print,           only: print_arpack_welcome,             &
                                      print_arpack_preprocessing,       &
                                      print_arpack_postprocessing,      &
                                      print_arpack_eigenvalues
  use m_time,                   only: time
  !> solver for linear problem
  use m_ctx_parallelism,        only: t_parallelism
  use m_api_solver,             only: t_solver, solver_clean,           &
                                      solver_init_default_options,      &
                                      process_factorization,            &
                                      process_solve
  use m_ctx_matrix,             only: t_matrix, matrix_clean
  use m_ctx_rhs,                only: t_rhs, rhs_clean
  use m_arpack_sort_ev,         only: arpack_sort_eigenvalues
  use, intrinsic                   :: ieee_arithmetic

  implicit none 
  !! ------------------------------------------------------------------- 

  
  !> to compute the eigenvectors and eigenvalues
  interface arpack_eigenvectors
    module procedure arpack_eigenvectors_double_complex
    !! module procedure arpack_eigenvectors_simple_complex
    !! note that for real matrices, a complete symmetric version exists
    !! Also the eigenvalues can still be complex, so careful...
    ! %-----------------------------------------------%
    ! | The real part of the eigenvalue is returned   |
    ! | in the first column of the two dimensional    |
    ! | array D, and the imaginary part is returned   |
    ! | in the second column of D.  The corresponding |
    ! | eigenvectors are returned in the first NEV    |
    ! | columns of the two dimensional array V if     |
    ! | requested.  Otherwise, an orthogonal basis    |
    ! | for the invariant subspace corresponding to   |
    ! | the eigenvalues in D is returned in V.        |
    ! %-----------------------------------------------%
    ! module procedure arpack_eigenvectors_double_real
    ! module procedure arpack_eigenvectors_simple_real
  end interface
   
  private
  public  :: arpack_eigenvectors


  !! -------------------------------------------------------------------
  !> we mostly rely on ARPACK function
  !> [sdcz]naupd => non-symmetric matrix pre-processing  eigenvectors
  !> [sd]  saupd =>     symmetric matrix pre-processing  eigenvectors
  !> [sdcz]neupd => non-symmetric matrix post-processing eigenvectors
  !> [sd]  seupd =>     symmetric matrix post-processing eigenvectors
  !
  !> Here is the list of common parameters and options, the infos
  !> come from ARPACK source files.
  !! -------------------------------------------------------------------
  !> BMAT
  !> RK: Matlab relies on Arpack and it seems to be using G
  ! ---------------------------------------------------------
  ! |______________________________________________________________|
  ! | MAT specifies the type of the matrix B that defines the      |
  ! | semi-inner product for the operator OP.                      |
  ! | B = 'I' -> standard eigenvalue problem    A*x = lambda*x     |
  ! | B = 'G' -> generalized eigenvalue problem A*x = lambda*B*x   |
  ! |______________________________________________________________|
  ! 
  !! -------------------------------------------------------------------
  !! -------------------------------------------------------------------
  !> WHICH   Character*2
  !> indicates if you want largest or lowest eigenvalues, and 
  !> depends if the matrix is symmetric or not
  !>
  !> We always want the largest EV, for the lowest, it simply is 
  !> the largest of A^{-1}, such that the matrix-vector product
  !> will be replaced by a linear system resolution.
  !> in which case we end up with 1/eigenvalues
  !>
  !> which  = 'LM'
  ! |______________________________________________________________|
  ! | 'LA' - compute the NEV largest (algebraic) eigenvalues.      |
  ! | 'SA' - compute the NEV smallest (algebraic) eigenvalues.     |
  ! | 'LM' - compute the NEV largest (in magnitude) eigenvalues.   |
  ! | 'SM' - compute the NEV smallest (in magnitude) eigenvalues.  |
  ! | 'BE' - compute NEV eigenvalues, half from each end of the    |
  ! |        spectrum.  When NEV is odd, compute one more from the |
  ! |        high end than from the low end.                       |
  ! | ****** NON-SYMMETRIC                                         |
  ! | 'LM' -> want the NEV eigenvalues of largest magnitude.       |
  ! | 'SM' -> want the NEV eigenvalues of smallest magnitude.      |
  ! | 'LR' -> want the NEV eigenvalues of largest real part.       |
  ! | 'SR' -> want the NEV eigenvalues of smallest real part.      |
  ! | 'LI' -> want the NEV eigenvalues of largest imaginary part.  |
  ! | 'SI' -> want the NEV eigenvalues of smallest imaginary part. |
  ! |______________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !> tolerance on computation
  !tol = dble(tol_accuracy)
  ! |______________________________________________________________|
  ! | Double precision  scalar.  (INPUT)                           |
  ! | Stopping criteria: the relative accuracy of the Ritz value   |
  ! | is considered acceptable if BOUNDS(I) .LE. TOL*ABS(RITZ(I))  |
  ! | where ABS(RITZ(I)) is the magnitude when RITZ(I) is complex. |
  ! | DEFAULT = dlamch('EPS')  (machine precision as computed      |
  ! |           by the LAPACK auxiliary subroutine dlamch).        |
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !ncv
  ! |_________________________________________________________________|
  ! | NCV is the largest number of basis vectors that will            |
  ! |     be used in the Implicitly Restarted Arnoldi                 |
  ! |     Process.  Work per major iteration is                       |
  ! |     proportional to N*NCV*NCV.                                  |
  ! | At present there is no a-priori analysis to guide the selection |
  ! | of NCV relative to NEV.                                         |
  ! | The only formal requrement is that NCV > NEV.                   |
  ! | However, it is recommended that NCV .ge. 2*NEV.                 |
  ! | If many problems of the same type are to be solved, one should  |
  ! | experiment with increasing NCV while keeping NEV fixed for a    |
  ! | given test problem.                                             |
  ! | This will usually decrease the required number of OP*x          |
  ! | operations but it also increases the work and storage required  |
  ! | to maintain the orthogonal basis vectors.                       |
  ! | The optimal "cross-over" with respect to CPU time is problem    |
  ! | dependent and must be determined empirically.                   |
  ! |_________________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !> LWORKL
  ! |______________________________________________________________|
  ! | cnaupd.f:  LWORKL must be at least 3*NCV**2 + 5*NCV          |
  ! | dnaupd.f:  LWORKL must be at least 3*NCV**2 + 6*NCV          |
  ! | snaupd.f:  LWORKL must be at least 3*NCV**2 + 6*NCV          |
  ! | znaupd.f:  LWORKL must be at least 3*NCV**2 + 5*NCV          |
  ! |                                                              |
  ! | dsaupd.f:  LWORKL must be at least   NCV**2 + 8*NCV          |
  ! | ssaupd.f:  LWORKL must be at least   NCV**2 + 8*NCV          |
  ! |______________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !info 
  ! |______________________________________________________________|
  ! | INFO    Integer.  (INPUT/OUTPUT)                             |
  ! | If INFO .EQ. 0, a randomly initial residual vector is used.  |
  ! | If INFO .NE. 0, RESID contains the initial residual vector,  |
  ! |                 possibly from a previous run.                |
  ! | Error flag on output.                                        |
  ! | =  0: Normal exit.                                           |
  ! | =  1: Maximum number of iterations taken.                    |
  ! |       All possible eigenvalues of OP has been found. IPARAM(5|  
  ! |       returns the number of wanted converged Ritz values.    |
  ! | =  2: No longer an informational error. Deprecated starting  |
  ! |       with release 2 of ARPACK.                              |
  ! | =  3: No shifts could be applied during a cycle of the       |
  ! |       Implicitly restarted Arnoldi iteration. One possibility|
  ! |       is to increase the size of NCV relative to NEV.        |
  ! |       See remark 4 below.                                    |
  ! | = -1: N must be positive.                                    |
  ! | = -2: NEV must be positive.                                  |
  ! | = -3: NCV-NEV >= 2 and less than or equal to N.              |
  ! | = -4: The maximum number of Arnoldi update iteration         |
  ! |       must be greater than zero.                             |
  ! | = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'|
  ! | = -6: BMAT must be one of 'I' or 'G'.                        |
  ! | = -7: Length of private work array is not sufficient.        |
  ! | = -8: Error return from LAPACK eigenvalue calculation;       |
  ! | = -9: Starting vector is zero.                               |
  ! | = -10: IPARAM(7) must be 1,2,3,4.                            |
  ! | = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatable.        |
  ! | = -12: IPARAM(1) must be equal to 0 or 1.                    |
  ! | = -9999: Could not build an Arnoldi factorization.           |
  ! |           IPARAM(5) returns the size of the current Arnoldi  |
  ! |           factorization.                                     |
  ! |______________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !ido
  ! |______________________________________________________________|
  ! | IDO     Integer.  (INPUT/OUTPUT)                             |
  ! | Reverse communication flag.  IDO must be zero on the first   |
  ! | call to dnaupd.  IDO will be set internally to               |
  ! | indicate the type of operation to be performed.  Control is  |
  ! | then given back to the calling routine which has the         |
  ! | responsibility to carry out the requested operation and call |
  ! | dnaupd with the result.  The operand is given in             |
  ! | WORKD(IPNTR(1)), the result must be put in WORKD(IPNTR(2)).  |
  ! | -------------------------------------------------------------|
  ! | IDO =  0: first call to the reverse communication interface  |
  ! | IDO = -1: compute  Y = OP * X  where                         |
  ! |           IPNTR(1) is the pointer into WORKD for X,          |
  ! |           IPNTR(2) is the pointer into WORKD for Y.          |
  ! |           This is for the initialization phase to force the  |
  ! |           starting vector into the range of OP.              |
  ! | IDO =  1: compute  Y = OP * X  where                         |
  ! |           IPNTR(1) is the pointer into WORKD for X,          |
  ! |           IPNTR(2) is the pointer into WORKD for Y.          |
  ! |           In mode 3 and 4, the vector B * X is already       |
  ! |           available in WORKD(ipntr(3)).  It does not         |
  ! |           need to be recomputed in forming OP * X.           |
  ! | IDO =  2: compute  Y = B * X  where                          |
  ! |           IPNTR(1) is the pointer into WORKD for X,          |
  ! |           IPNTR(2) is the pointer into WORKD for Y.          |
  ! | IDO =  3: compute the IPARAM(8) real and imaginary parts     |
  ! |           of the shifts where INPTR(14) is the pointer       |
  ! |           into WORKL for placing the shifts. See Remark      |
  ! |           5 below.                                           |
  ! | IDO = 99: done                                               |
  ! |______________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  ! iparam(1) = 1     !! starting
  ! iparam(3) = maxitr
  ! iparam(7) = 1     !! RK: it appears to be 3 in Matlab...
  ! |______________________________________________________________|
  ! | IPARAM  Integer array of length 11.  (INPUT/OUTPUT)          |
  ! | IPARAM(1) = ISHIFT: method for selecting the implicit shifts.|
  ! | The shifts selected at each iteration are used to restart    |
  ! | the Arnoldi iteration in an implicit fashion.                |
  ! | -------------------------------------------------------------|
  ! | ISHIFT = 0: the shifts are provided by the user via          |
  ! |             reverse communication.  The real and imaginary   |
  ! |             parts of the NCV eigenvalues of the Hessenberg   |
  ! |             matrix H are returned in the part of the WORKL   |
  ! |             array corresponding to RITZR and RITZI.          |
  ! | ISHIFT = 1: exact shifts with respect to the current         |
  ! |             Hessenberg matrix H.  This is equivalent to      |
  ! |             restarting the iteration with a starting vector  |
  ! |             that is a linear combination of approximate Schur|
  ! |             vectors associated with the "wanted" Ritz values.|
  ! | -------------------------------------------------------------|
  ! |                                                              |
  ! | IPARAM(2) = No longer referenced.                            |
  ! |                                                              |
  ! | IPARAM(3) = MXITER                                           |
  ! | On INPUT:  maximum number of Arnoldi update iterations allow |
  ! | On OUTPUT: actual number of Arnoldi update iterations taken. |
  ! |                                                              |
  ! | IPARAM(4) = NB: blocksize to be used in the recurrence.      |
  ! | The code currently works only for NB = 1.                    |
  ! |                                                              |
  ! | IPARAM(5) = NCONV: number of "converged" Ritz values.        |
  ! | This represents the number of Ritz values that satisfy       |
  ! | the convergence criterion.                                   |
  ! |                                                              |
  ! | IPARAM(6) = IUPD                                             |
  ! | No longer referenced. Implicit restarting is ALWAYS used.    |
  ! |                                                              |
  ! | IPARAM(7) = MODE                                             |
  ! | On INPUT determines what type of eigenpblem is being solved. |
  ! | Must be 1,2,3,4; See under \Description of dnaupd for the    |
  ! | four modes available.                                        |
  ! |                                                              |
  ! | IPARAM(8) = NP                                               |
  ! | When ido = 3 and the user provides shifts through reverse    |
  ! | communication (IPARAM(1)=0), dnaupd returns NP, the number   |
  ! | of shifts the user is to provide. 0 < NP <=NCV-NEV.          |
  ! |                                                              |
  ! | IPARAM(9) = NUMOP, IPARAM(10) = NUMOPB, IPARAM(11) = NUMREO, |
  ! | OUTPUT: NUMOP  = total number of OP*x operations,            |
  ! |         NUMOPB = total number of B*x operations if BMAT='G', |
  ! |         NUMREO = total number of steps of re-orthogonaliz.   |
  ! |______________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !resid
  ! |______________________________________________________________|
  ! | RESID   array of length N.  (INPUT/OUTPUT)                   |
  ! | On INPUT:                                                    |
  ! | If INFO .EQ. 0, a random initial residual vector is used.    |
  ! | If INFO .NE. 0, RESID contains the initial residual vector,  |
  ! |                 possibly from a previous run.                |
  ! | On OUTPUT:                                                   |
  ! | RESID contains the final residual vector.                    |
  ! |______________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !rwork
  ! |______________________________________________________________|
  ! | RWORK   Double precision  work array of length NCV           |
  ! | Private (replicated) array on each PE or array allocated on  |
  ! | the front end.                                               |
  ! |______________________________________________________________|
  !! -------------------------------------------------------------------

  !! -------------------------------------------------------------------
  !> post-processing {sdcz}{sn}eupd
  !> *******************************************************************
  ! |______________________________________________________________|
  ! | HOWMNY  Character*1  (INPUT)                                 |
  ! | Specifies the form of the basis for the invariant subspace   |
  ! | corresponding to the converged Ritz values that is           |
  ! | to be computed.                                              |
  ! |                                                              |
  ! | = 'A': Compute NEV Ritz vectors;                             |
  ! | = 'P': Compute NEV Schur vectors;                            |
  ! | = 'S': compute some of the Ritz vectors, specified           |
  ! |        by the logical array SELECT.                          |
  ! |______________________________________________________________|
  
  ! |______________________________________________________________|
  ! | SELECT  Logical array of dimension NCV.  (INPUT)             |
  ! | If HOWMNY = 'S', SELECT specifies the Ritz vectors to be     |
  ! | computed. To select the  Ritz vector corresponding to a      |
  ! | Ritz value D(j), SELECT(j) must be set to .TRUE..            |
  ! | If HOWMNY = 'A' or 'P', SELECT need not be initialized       |
  ! | but it is used as internal workspace.                        |
  ! |______________________________________________________________|




  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> returns error information for ARPACK {sdcz}{sn}aupd routines. 
  !> cf. the info return codes above.
  !
  !> @param[in]    info           : error code in Arpack
  !> @param[in]    ido            : ido code in Arpack
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine arpack_error_info_aupd(info,ido,maxitr,size_mat,n_ev,ncv,  &
                                    which,bmat,iparam,ctx_err)
    implicit none

    integer          ,intent(in)   :: info,ido,n_ev,ncv
    integer          ,intent(in)   :: maxitr,size_mat,iparam(11)
    character(len=1) ,intent(in)   :: bmat
    character(len=2) ,intent(in)   :: which
    type(t_error)    ,intent(inout):: ctx_err


    !! there is no error if ido == 99 with info == 0,
    !! otherwize, get information on the error.
    if(ido .ne. 99 .or. info .ne. 0) then
      ctx_err%ierr  =-1003
      ctx_err%critic=.true.

      select case(info)
        case(   1)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": maximum number of iteration (",maxitr,                     &
          ") reached [arpack_eigenvectors]"

        case(   3)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": no shifts during cycle of Arnoldi; Try to increase NCV "// &
          "[arpack_eigenvectors]"

        case(  -1)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": N must be positive and it is ", size_mat," [arpack_eigenvectors]"

        case(  -2)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": Nev must be positive and it is ", n_ev," [arpack_eigenvectors]"
        case(  -3)
          write(ctx_err%msg,'(a,i0,a,i0,a,i0,a,i0,a)')                  &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": (Ncv-Nev) must be >= 2 and <=N and we have Ncv=", ncv,     &
          " Nev=",n_ev," N=",size_mat," [arpack_eigenvectors]"
          
        case(  -4)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": maximum number of iteration",maxitr," must be > 0 "     // &
          "[arpack_eigenvectors]"
          
        case(  -5)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": parameter WHICH is '"//which//"' and is not recognized "// &
          "[arpack_eigenvectors]"

        case(  -6)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": parameter BMAT is '"//bmat//"' and is not recognized "//   &
          "(should be I or G) [arpack_eigenvectors]"

        case(  -7)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": insufficient length of work array [arpack_eigenvectors]"

        case(  -8)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": error return from LAPACK eigenvalue calculation "        //&
          "[arpack_eigenvectors]"

        case(  -9)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": starting vector is zero [arpack_eigenvectors]"
          
        case( -10)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": IPARAM(7) is ",iparam(7)," while is must be 1, 2, 3 or " //&
          "4 [arpack_eigenvectors]"
          
        case( -11)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": IPARAM(7)=1 with BMAT=G is incompatible [arpack_eigenvectors]"
          
        case( -12)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": IPARAM(1) is ",iparam(1)," while is must be 1 or 0 "     //&
          "[arpack_eigenvectors]"
          
        case(-9999)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          ": could not buil Arnoldi factorization, current size is ",   &
          iparam(5)," [arpack_eigenvectors]"

        case default      
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]aupd, error code ",info,        &
          "ido code is ",ido," [arpack_eigenvectors]"
      end select
    end if

    return
  end subroutine arpack_error_info_aupd

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> returns error information for ARPACK {sdcz}{sn}eupd routines. 
  !> cf. the info return codes above.
  !
  !> @param[in]    info           : error code in Arpack
  !> @param[in]    ido            : ido code in Arpack
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine arpack_error_info_eupd(info,ido,size_mat,n_ev,ncv,         &
                                    which,bmat,iparam,howmny,ctx_err)
    implicit none

    integer          ,intent(in)   :: info,ido,n_ev,ncv
    integer          ,intent(in)   :: size_mat,iparam(11)
    character(len=1) ,intent(in)   :: bmat,howmny
    character(len=2) ,intent(in)   :: which
    type(t_error)    ,intent(inout):: ctx_err


    !! there is no error if ido == 99 with info == 0,
    !! otherwize, get information on the error.
    if(ido .ne. 99 .or. info .ne. 0) then
      ctx_err%ierr  =-1003
      ctx_err%critic=.true.

      select case(info)
        case(   1)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": Schur from Lapack could not be reordered [arpack_eigenvectors]"

        case(  -1)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": N must be positive and it is ", size_mat," [arpack_eigenvectors]"

        case(  -2)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": Nev must be positive and it is ", n_ev," [arpack_eigenvectors]"

        case(  -3)
          write(ctx_err%msg,'(a,i0,a,i0,a,i0,a,i0,a)')                  &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": (Ncv-Nev) must be >= 1 and <= N and we have Ncv=", ncv,    &
          " Nev=",n_ev," N=",size_mat," [arpack_eigenvectors]"

        case(  -5)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": parameter WHICH is '"//which//"' and is not recognized "// &
          "[arpack_eigenvectors]"

        case(  -6)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": parameter BMAT is '"//bmat//"' and is not recognized "//   &
          "(should be I or G) [arpack_eigenvectors]"

        case(  -7)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": insufficient length of work array [arpack_eigenvectors]"

        case(  -8)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": error return from LAPACK eigenvalue calculation "        //&
          "[arpack_eigenvectors]"

        case(  -9)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": error in Lapack routine ztrevc [arpack_eigenvectors]"
          
        case( -10)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": IPARAM(7) is ",iparam(7)," while is must be 1, 2, 3 or " //&
          "4 [arpack_eigenvectors]"
          
        case( -11)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": IPARAM(7)=1 with BMAT=G is incompatible [arpack_eigenvectors]"

        case( -12,-13)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": howmny is'"//howmny//"', it should be A or P [arpack_eigenvectors]"

        case( -14)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": Znaupd did not find eigenvalues with accuracy [arpack_eigenvectors]"

        case( -15)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          ": {sdcz}{sn}upd dgot different count than {sdcz}{sn}aupd " //&
          "[arpack_eigenvectors]"

        case default      
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in ARPACK [sdcz][sn]eupd, error code ",info,        &
          "ido code is ",ido," [arpack_eigenvectors]"
      end select
    end if

    return
  end subroutine arpack_error_info_eupd

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute N eigenvectors and eigenvalues using ARPACK. 
  !> it can be: 
  !> - the lowest  eigenvalues,
  !> - the highest eigenvalues
  !
  !> @param[in]    A              : matrix in (line col val) format
  !> @param[in]    size_mat       : matrix size
  !> @param[in]    n_nz           : number of nonzeros
  !> @param[in]    flag_sym       : indicates if symmetric matrix or not
  !> @param[in]    n_ev           : number of eigenvectors/eigenvalues
  !> @param[in]    str_version    : lowest or highest eigenvalues
  !> @param[in]    str_method     : Ritz or Schur 
  !> @param[inout] eigenvalues    : the output eigenvalues, sorted 
  !> @param[inout] eigenvectors   : the output eigenvalues, sorted 
  !> @param[inout] tol_accuracy   : allowed accuracy tolerance (see below)
  !> @param[in]    str_product    : inner produc for <\lambda, v>
  !> @param[out]   mem            : memory 
  !> @param[in]    verblevel      : level of verbose
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine arpack_eigenvectors_double_complex(Alin,Acol,Aval,size_mat,&
                                 n_nz,Mlin,Mcol,Mval,Mnnz,flag_sym,n_ev,&
                                 str_version,str_method,eigenvalues,    &
                                 eigenvectors,tol_accuracy,str_product, &
                                 mem,verb_level,ctx_err)
    implicit none

    integer(kind=4)                    ,intent(in)   :: Alin(:)
    integer(kind=4)                    ,intent(in)   :: Acol(:)
    complex(kind=8)                    ,intent(in)   :: Aval(:)
    integer                            ,intent(in)   :: size_mat,n_nz
    logical                            ,intent(in)   :: flag_sym
    integer                            ,intent(in)   :: n_ev
    character(len=*)                   ,intent(in)   :: str_version
    character(len=*)                   ,intent(in)   :: str_method
    character(len=*)                   ,intent(in)   :: str_product
    real   (kind=4)                    ,intent(in)   :: tol_accuracy
    complex(kind=8)        ,allocatable,intent(inout):: eigenvectors(:,:)
    complex(kind=8)        ,allocatable,intent(inout):: eigenvalues (:)
    !! real   (kind=8)                    ,intent(in)   :: inner_prodmat(:)
    !! generalized matrix information           
    integer(kind=4)                    ,intent(in)   :: Mlin(:)
    integer(kind=4)                    ,intent(in)   :: Mcol(:)
    complex(kind=8)                    ,intent(in)   :: Mval(:)
    integer                                          :: Mnnz
    !! others
    integer(kind=4)                    ,intent(in)   :: verb_level
    integer(kind=8)                    ,intent(out)  :: mem
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer                     :: k,size_cx,size_int
    real(kind=8)                :: time0,time1
    integer       ,allocatable  :: index_sort(:)
    logical                     :: flag_lowest_ev,flag_ritz,flag_schur
    logical                     :: flag_generalized
    !! local solver mumps in case of lowest eigenvalues 
    ! ctx_solver
    type(t_parallelism)         :: ctx_paral_self
    type(t_solver)              :: ctx_solver
    type(t_solver)              :: ctx_solverM
    type(t_matrix)              :: ctx_matrix
    type(t_rhs)                 :: ctx_rhs
    !! arpack aupd
    character(len=2)            :: which
    character(len=1)            :: bmat
    integer                     :: maxitr,ncv,lworkl,ido,iiter
    integer                     :: iparam(11),n,info,ldv,ipntr(11)
    complex(kind=8),allocatable :: v(:,:),workd(:),workl(:),d(:,:)
    complex(kind=8),allocatable :: resid(:),rwork(:)
    real   (kind=8)             :: tol
    !! arpack eupd
    integer                     :: ldz
    character(len=1)            :: howmny
    logical                     :: flag_vectors
    complex(kind=8)             :: sigma
    complex(kind=8),allocatable :: workev(:),evalues_loc(:),evector_loc(:,:)
    logical        ,allocatable :: selection(:)

    call time(time0)
    ctx_err%ierr=0
    mem         =0
    eigenvalues =0.0
    eigenvectors=0.0
    !! for more infos you have the website with subroutine infos:
    !! https://people.sc.fsu.edu/~jburkardt%20/f_src/arpack/arpack.f90 
    !! trac.alcf.anl.gov/projects/libs/browser/ARPACK/SRC/ssaupd.f?rev=65&order=size

    size_cx = 8*2
    size_int= 4
    n       = size_mat

    !! -----------------------------------------------------------------
    !! REMARK 
    !! We always want the largest EV, for the lowest, it simply is 
    !! the largest of A^{-1}, such that the matrix-vector product
    !! will be replaced by a linear system resolution.
    !! in which case we end up with 1/eigenvalues
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! REMARK 
    !! Symmetric complex and symmetric double complex do not exist in 
    !! ARPACK
    !! -----------------------------------------------------------------

    !! basic errors 
    if(size_mat <= 0 .or. n_nz <=0) then
      ctx_err%ierr=-1003
      ctx_err%msg ="** ERROR: inconsitent matrix [arpack_eigenvectors]"
      ctx_err%critic=.true.
      return   
    end if
    if(size_mat < n_ev) then
      ctx_err%ierr=-1004
      write(ctx_err%msg,'(a,i0,a,i0,a)') "** ERROR: "                // &
           " asking for ", n_ev ," eigenvectors while the size of "  // &
           "the matrix is only ",size_mat," [arpack_eigenvectors]"
      ctx_err%critic=.true.
      return   
    end if
    !! -----------------------------------------------------------------
    !! to define the inner product for the eigenvectors
    iparam = 0
    flag_generalized = .false.
    select case(trim(adjustl(str_product)))
      case('identity')
        bmat   = 'I'
        ! ---------------------------------------------------------
        !! RK: Matlab relies on arpack and it seems to be using G
        ! ---------------------------------------------------------
        ! | B = 'I' -> standard eigenvalue problem    A*x = lambda*x     |
        ! | B = 'G' -> generalized eigenvalue problem A*x = lambda*B*x   |
        ! |______________________________________________________________|

        ! Mode is given by IPARAM(7) 
        ! %____________________________________________________________________%
        !  |   Mode 1:  A*x = lambda*x.                                        |
        !  |            ===> OP = A  and  B = I.                               |
        !  |                                                                   |
        !  |   Mode 2:  A*x = lambda*M*x, M hermitian positive definite        |
        !  |            ===> OP = inv[M]*A  and  B = M.                        |
        !  |            ===> (If M can be factored see remark 3 below)         |
        !  |                                                                   |
        !  |   Mode 3:  A*x = lambda*M*x, M hermitian semi-definite            |
        !  |            ===> OP =  inv[A - sigma*M]*M   and  B = M.            |
        !  |            ===> shift-and-invert mode                             |
        !  |            If OP*x = amu*x, then lambda = sigma + 1/amu.          |
        ! %____________________________________________________________________%
        iparam(7) = 1 
        flag_generalized = .false.

      !! -------------------------------------------------------------
      !! we have to use the generalized version to solve
      !!      K x = \lambda M x, 
      !!
      !! a) if one wants the lowest eigenvalues, we have 
      !!         K^(-1) K x     = \lambda K^(-1) M x
      !!   ==>   1/\lambda x = K^(-1) M x
      !!   so we have to compute the largest eigenvalues of 
      !!                     K^(-1) M
      !! b) if one wants the largest eigenvalues, we have
      !!         M^(-1) K x     = \lambda x
      !!   so we have to compute the largest eigenvalues of
      !!         M^(-1) K
      !! 
      !! CONCLUSION: even for a generalized eigenvalue problem,
      !!             we keep the IDENTITY option, and simply adjust
      !!             the matrix-vector multiplication opertions.
      !! ---------------------------------------------------------------
      case('hdg')
        bmat   = 'I'
        iparam(7) = 1 
        flag_generalized = .true.

      case default
        ctx_err%ierr=-1001
        ctx_err%msg ="** ERROR: unrecognize inner product for ARPACK "//&
                     "computation [arpack_eigenvectors]"
        ctx_err%critic=.true.
        return
    end select
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    flag_lowest_ev=.false.
    select case (trim(adjustl(str_version)))
      case('lowest','LOWEST','Lowest')  
        flag_lowest_ev=.true.

        !! need to initiliaze a self paralellism and solvers infos then.
        call init_parallelism_selfproc(ctx_paral_self,ctx_err)
        call solver_init_default_options(ctx_paral_self,flag_sym,       &
                                         ctx_solver,0,ctx_err)
        !! copy matrix in matrix context
        ctx_matrix%dim_matrix_gb  = size_mat
        ctx_matrix%dim_matrix_loc = size_mat
        ctx_matrix%dim_nnz_gb     = n_nz
        ctx_matrix%dim_nnz_loc    = n_nz
        ctx_matrix%flag_block     = .false.
        allocate(ctx_matrix%ilin(n_nz))
        allocate(ctx_matrix%icol(n_nz))
        allocate(ctx_matrix%Aval(n_nz))
        ctx_matrix%ilin = int(Alin  ,kind=IKIND_MAT)
        ctx_matrix%icol = int(Acol  ,kind=IKIND_MAT)
        ctx_matrix%Aval = cmplx(Aval,kind=RKIND_MAT)
        !! proceed with factorization
        call process_factorization(ctx_paral_self,ctx_solver,ctx_matrix,&
                                   ctx_err,o_flag_verb=.false.)
        call matrix_clean(ctx_matrix) !! clean matrix

        !! prepare dense rhs
        ctx_rhs%rhs_format='dense'
        allocate(ctx_rhs%n_rhs_in_group(1)) ; ctx_rhs%n_rhs_in_group=1
        ctx_rhs%i_group           = 1
        ctx_rhs%n_group           = 1
        ctx_rhs%n_rhs_component   = 1
        allocate(ctx_rhs%csri(2)) !! only one rhs
        ctx_rhs%csri(1) = 1 ; ctx_rhs%csri(2) = ctx_rhs%csri(1) + size_mat
        allocate(ctx_rhs%icol(size_mat))
        allocate(ctx_rhs%Rval(size_mat))
        !! column gives one value per position 
        do k=1,size_mat
          ctx_rhs%icol(k) = k
        end do

      case('highest','HIGHEST','Highest')
        flag_lowest_ev=.false.

        !! if we have a generalized matrix, we need the invert of M
        if(flag_generalized) then
          !! we need to compute M^{-1} if we have the largest eigenvalues.
          !! need to initiliaze a self paralellism and solvers infos then.
          call init_parallelism_selfproc(ctx_paral_self,ctx_err)
          call solver_init_default_options(ctx_paral_self,flag_sym,       &
                                           ctx_solverM,0,ctx_err)
          !! copy matrix in matrix context
          ctx_matrix%dim_matrix_gb  = size_mat
          ctx_matrix%dim_matrix_loc = size_mat
          ctx_matrix%dim_nnz_gb     = Mnnz
          ctx_matrix%dim_nnz_loc    = Mnnz
          ctx_matrix%flag_block     = .false.
          allocate(ctx_matrix%ilin(Mnnz))
          allocate(ctx_matrix%icol(Mnnz))
          allocate(ctx_matrix%Aval(Mnnz))
          ctx_matrix%ilin =   int(Mlin  ,kind=IKIND_MAT)
          ctx_matrix%icol =   int(Mcol  ,kind=IKIND_MAT)
          ctx_matrix%Aval = cmplx(Mval,kind=RKIND_MAT)
          !! proceed with factorization
          call process_factorization(ctx_paral_self,ctx_solverM,          &
                                     ctx_matrix,ctx_err,o_flag_verb=.false.)
          call matrix_clean(ctx_matrix) !! clean matrix
          !! prepare dense rhs 
          ctx_rhs%rhs_format='dense'
          allocate(ctx_rhs%n_rhs_in_group(1)) ; ctx_rhs%n_rhs_in_group=1
          ctx_rhs%i_group           = 1
          ctx_rhs%n_group           = 1
          ctx_rhs%n_rhs_component   = 1
          allocate(ctx_rhs%csri(2)) !! only one rhs
          ctx_rhs%csri(1) = 1 ; ctx_rhs%csri(2) = ctx_rhs%csri(1) + size_mat
          allocate(ctx_rhs%icol(size_mat))
          allocate(ctx_rhs%Rval(size_mat))
          !! column gives one value per position 
          do k=1,size_mat
            ctx_rhs%icol(k) = k
          end do
        end if

      case default
        ctx_err%ierr=-1002
        ctx_err%msg ="** ERROR: unrecognize eigenvectors version "//&
                     "for ARPACK, it should be 'lowest' or "      //&
                     "'highest' ones  [arpack_eigenvectors]"
        ctx_err%critic=.true.
        return
    end select
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    flag_ritz =.false.
    flag_schur=.false.
    select case (trim(adjustl(str_method)))
      case('RITZ','Ritz','ritz')  
        flag_ritz=.true.
      case('Schur','SCHUR','schur')
        !! =============================================================
        !! NOT RECOMMENDED
        flag_schur=.true.
        !! =============================================================
      case default
        ctx_err%ierr=-1006
        ctx_err%msg ="** ERROR: unrecognize eigenvectors method " //&
                     "for ARPACK, only RITZ is supported for now "//&
                     " [arpack_eigenvectors]"
        ctx_err%critic=.true.
        return
    end select
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! arbitrary maximal number of iterations
    maxitr = 500000
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------    
    which  = 'LM'
      ! |______________________________________________________________|
      ! | 'LA' - compute the NEV largest (algebraic) eigenvalues.      |
      ! | 'SA' - compute the NEV smallest (algebraic) eigenvalues.     |
      ! | 'LM' - compute the NEV largest (in magnitude) eigenvalues.   |
      ! | 'SM' - compute the NEV smallest (in magnitude) eigenvalues.  |
      ! | 'BE' - compute NEV eigenvalues, half from each end of the    |
      ! |        spectrum.  When NEV is odd, compute one more from the |
      ! |        high end than from the low end.                       |
      ! | ****** NON-SYMMETRIC                                         |
      ! | 'LM' -> want the NEV eigenvalues of largest magnitude.       |
      ! | 'SM' -> want the NEV eigenvalues of smallest magnitude.      |
      ! | 'LR' -> want the NEV eigenvalues of largest real part.       |
      ! | 'SR' -> want the NEV eigenvalues of smallest real part.      |
      ! | 'LI' -> want the NEV eigenvalues of largest imaginary part.  |
      ! | 'SI' -> want the NEV eigenvalues of smallest imaginary part. |
      ! |______________________________________________________________|
      !! ---------------------------------------------------------------   

    !! -----------------------------------------------------------------    
    !! tolerance on computation
    tol = dble(tol_accuracy)
    ncv=int(n_ev*2.5) 
    !! we must have (Ncv-Nev) must be >= 2 and <=N 
    !! check for small problem
    if(ncv >= size_mat) ncv=size_mat
    
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------    
    !! LWORKL
    !! Symmetric complex does not exist, so we take non-symmetric 
    !! if complex anyway
    !if(flag_sym) then
    !  lworkl =   ncv**2 + 8*ncv
    !else
    lworkl = 3*ncv**2 + 6*ncv 
    !endif
    !! we multiply to be sure
    lworkl = int(lworkl*1.20)
      ! |______________________________________________________________|
      ! | non-symmetric real at least 3*NCV**2 + 6*NCV                 |
      ! |     symmetric real at least   NCV**2 + 8*NCV                 |
      ! | non-symmetric cx   at least 3*NCV**2 + 5*NCV                 |
      ! |______________________________________________________________|
    !! -----------------------------------------------------------------    

    !! ----------------------------------------------------------------- 
    info = 0 !! starting, see detail above
    ido  = 0 !! starting, see detail above
    iparam(1) = 1 !! starting, see detail above
    iparam(3) = maxitr
    !! it depends on the inner product
    !! iparam(7) = 1 !! RK: it appears to be 3 in Matlab...
    !! -----------------------------------------------------------------

    !! estimate memory of allocatable array
    mem = size_cx*n_nz + size_int*n_nz*2         + & !! matrix A
          !! eigenvectors and eigenvalues, tiems 2 for local array
          (size_cx*n_ev + size_cx*n_ev*n)*2      + & 
          size_cx*n*ncv   + & !! v    
          size_cx*lworkl  + & !! workl
          size_cx*3*n     + & !! workd
          size_cx*ncv     + & !! rwork
          size_cx*ncv*2   + & !! d    
          size_cx*n       + & !! resid
          size_int*2*11   + & !! iparam and ipntr
          size_cx*3*ncv   + & !! workev
          ncv             + & !! selection
          size_int*n_ev       !! index sort
    !! add system for lowest ev or generalized highest system
    if(flag_lowest_ev .or. flag_generalized) then
      mem = mem + RKIND_MAT*2*n_nz + IKIND_MAT*n_nz*2 + & !! matrix
                  RKIND_MAT*2*size_mat                + & !! solution
                  !! rhs CSR
                  IKIND_MAT*2 + IKIND_MAT*size_mat + RKIND_MAT*2*size_mat
    end if
    if(flag_generalized) then
      !! another matrix
      mem = mem + RKIND_MAT*2*Mnnz + IKIND_MAT*Mnnz*2 
    end if 

    if(verb_level > 0) then
      call print_arpack_welcome(n,n_ev,flag_lowest_ev,flag_ritz,        &
                                flag_schur,trim(adjustl(str_product)),  &
                                mem,ctx_err)
    end if
    !! -----------------------------------------------------------------
    !!
    !! Pre-processing using {sdcz}{sn}aupd routines
    !! 
    !! -----------------------------------------------------------------
    ldv       = size_mat !! Leading dimension 
    !! allocation
    allocate(v    (n,ncv) ) !! [N,NCV], final set of Arnoldi basis vectors.
    allocate(workl(lworkl)) !! working array
    allocate(workd(3*n)   ) !! Reverse communication 
    allocate(rwork(ncv)   ) 
    allocate(d    (ncv,2) )
    allocate(resid(n)     ) !! If INFO .EQ. 0, a random init is used
    !! -----------------------------------------------------------------s

    iiter=1
    !! main loop
    do while(iiter <= maxitr)
      !! complex is always non-symmetric
      !if(flag_sym) then
      !  call zsaupd(ido,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,iparam,  &
      !              ipntr,workd,workl,lworkl,info)
      !else
      call znaupd(ido,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,iparam,     &
                  ipntr,workd,workl,lworkl,rwork,info)

      !endif
      iiter=iiter+1
      select case(ido)
        case(-1,1)

          !! %--------------------------------------%
          !! | Perform matrix vector multiplication |
          !! |              y <--- OP*x             |
          !! | workd(ipntr(1)) contains  X          |
          !! | workd(ipntr(2)) will have Y          |
          !! %--------------------------------------%
          ! Mode is given by IPARAM(7) 
          ! %--------------------------------------------------------------------%
          !  |   Mode 1:  A*x = lambda*x.                                        |
          !  |            ===> OP = A  and  B = I.                               |
          !  |                                                                   |
          !  |   Mode 2:  A*x = lambda*M*x, M hermitian positive definite        |
          !  |            ===> OP = inv[M]*A  and  B = M.                        |
          !  |            ===> (If M can be factored see remark 3 below)         |
          !  |                                                                   |
          !  |   Mode 3:  A*x = lambda*M*x, M hermitian semi-definite            |
          !  |            ===> OP =  inv[A - sigma*M]*M   and  B = M.            |
          !  |            ===> shift-and-invert mode                             |
          !  |            If OP*x = amu*x, then lambda = sigma + 1/amu.          |
          ! %--------------------------------------------------------------------%
          workd(ipntr(2):ipntr(2)+n-1)=0.0
          
          !! without generalized problem.
          if(.not. flag_generalized) then
            !! if we want the lowest one: we have to solve a linear problem
            !! otherwize we perform the matrix vector product
            ! -------------------------------------------------------
            if(flag_lowest_ev) then
              !! Solve y = A^{-1} x, 
              !! i.e. x is the RHS and we solve Ay = x
              !! 
              ctx_rhs%Rval    = 0.0
              ctx_rhs%Rval(:) = cmplx(workd(ipntr(1):ipntr(1)+n-1),kind=RKIND_MAT)
              call process_solve(ctx_paral_self,ctx_solver,ctx_rhs,       &
                                 .false.,ctx_err,o_flag_verb=.false.)
              workd(ipntr(2):ipntr(2)+n-1) = dcmplx(ctx_solver%solution_global)
            ! -------------------------------------------------------
            else
              !! matrix vector multiplication
              do k=1,n_nz !! loop over all 
                !! using line col val format for the matrix...
                workd(ipntr(2)+Alin(k)-1)=workd(ipntr(2)+Alin(k)-1) + & 
                                          workd(ipntr(1)+Acol(k)-1) * Aval(k)
                !! Symmetry
                if(flag_sym .and. Acol(k) > Alin(k)) then
                  workd(ipntr(2)+Acol(k)-1)=workd(ipntr(2)+Acol(k)-1) + & 
                                            workd(ipntr(1)+Alin(k)-1) * Aval(k)
                endif
              enddo
            end if

          !! -------------------------------------------------------------
          !! we have to use the generalized version to solve
          !!      K x = \lambda M x, 
          !!
          !! a) if one wants the lowest eigenvalues, we have 
          !!         K^(-1) K x     = \lambda K^(-1) M x
          !!   ==>   1/\lambda x = K^(-1) M x
          !!   so we have to compute the largest eigenvalues of 
          !!                     K^(-1) M
          !! b) if one wants the largest eigenvalues, we have
          !!         M^(-1) K x     = \lambda x
          !!   so we have to compute the largest eigenvalues of
          !!         M^(-1) K
          !! 
          !! CONCLUSION: even for a generalized eigenvalue problem,
          !!             we keep the IDENTITY option, and simply adjust
          !!             the matrix-vector multiplication opertions.
          !! ---------------------------------------------------------------
          else
            if(flag_lowest_ev) then
    
              !! Solve y = A^{-1} M x, 
              !! i.e. M x is the RHS and we solve Ay = Mx
              ctx_rhs%Rval    = 0.0
              !! matrix vector multiplication
              do k=1,Mnnz !! loop over all 
                !! using line col val format for the matrix...
                ctx_rhs%Rval(Mlin(k)) = ctx_rhs%Rval(Mlin(k)) +       & 
                                        workd(ipntr(1)+Mcol(k)-1) * Mval(k)
                !! Symmetry
                if(flag_sym .and. Mcol(k) > Mlin(k)) then
                  ctx_rhs%Rval(Mcol(k))= ctx_rhs%Rval(Mcol(k))     +  & 
                                         workd(ipntr(1)+Mlin(k)-1) * Mval(k)
                endif
              enddo
              !! solve A y = M x.
              call process_solve(ctx_paral_self,ctx_solver,ctx_rhs,       &
                                 .false.,ctx_err,o_flag_verb=.false.)
              workd(ipntr(2):ipntr(2)+n-1) = dcmplx(ctx_solver%solution_global)
              
            ! largest eigenvalues use M^(-1) K as the "main matrix"
            else
              
              !! a) matrix vector multiplication K x  for RHS
              do k=1,n_nz !! loop over all 
                !! using line col val format for the matrix...
                ctx_rhs%Rval(Alin(k)) = ctx_rhs%Rval(Alin(k)) +       & 
                                        workd(ipntr(1)+Acol(k)-1) * Aval(k)
                !! Symmetry
                if(flag_sym .and. Acol(k) > Alin(k)) then
                  ctx_rhs%Rval(Acol(k))= ctx_rhs%Rval(Acol(k))     +  & 
                                         workd(ipntr(1)+Alin(k)-1) * Aval(k)
                endif
              enddo

              !! solve M y = K x.
              call process_solve(ctx_paral_self,ctx_solverM,ctx_rhs,    &
                                 .false.,ctx_err,o_flag_verb=.false.)
              workd(ipntr(2):ipntr(2)+n-1) = dcmplx(ctx_solverM%solution_global)
            end if  
              
          !! -----------------------------------------------------------
          end if

        case(2)
          !! %--------------------------------------%
          !! | Perform matrix vector multiplication |
          !! |              y <--- M*x              |
          !! | where M is the matrix of the inner   |
          !! | product when we solve Ax = lambda M x|
          !! |                                      |
          !! | workd(ipntr(1)) contains  X          |
          !! | workd(ipntr(2)) will have Y          |
          !! %--------------------------------------%
          
          !! cannot happen as we should only have the identity version.
          ctx_err%ierr=-1001
          ctx_err%msg ="** ERROR: unrecognize inner product for ido=2"//&
                       " ARPACK computation [arpack_eigenvectors]"
          ctx_err%critic=.true.
          return
          
        case default
          exit !! we are done whatever it is an error or completion
      end select
      
    enddo

    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call arpack_error_info_aupd(info,ido,maxitr,size_mat,n_ev,ncv,      &
                                which,bmat,iparam,ctx_err)
    if(ctx_err%ierr .ne. 0) return

    !! clean solver infos and matrix
    if(flag_lowest_ev) then
      call solver_clean(ctx_solver,ctx_err)
      call rhs_clean(ctx_rhs,ctx_err)
    else
      if(flag_generalized) then
        call solver_clean(ctx_solverM,ctx_err)
        call rhs_clean(ctx_rhs,ctx_err)
      endif
    end if

    !! print preprocessing ok
    if(verb_level > 0) call print_arpack_preprocessing(iiter,ctx_err)


    !! -----------------------------------------------------------------
    !!
    !! Post-processing using {sdcz}{sn}eupd routines
    !! Eigenvectors may be also computed now if rvec = .true.
    !! 
    !! -----------------------------------------------------------------
    flag_vectors = .true. !> compute eigenvectors too, it RVEC.
    !! not symmetric for complex anyway 
    !! if(.not. flag_sym) allocate(workev(3*ncv))
    allocate(workev(3*ncv))
    allocate(selection(ncv))   
    
    if(flag_ritz) then 
      howmny = 'A'
    end if 
    if(flag_schur) then
      howmny    = 'P'
      selection = .true.
    end if
    sigma = dcmplx(0.)
    !! | because we have iparam(1) = 1, there is no shift
    !! |___________________________________________________|
    !! | SIGMA   Complex*16  (INPUT)                       |
    !! | If IPARAM(7) = 3 then SIGMA represents the shift. |
    !! | Not referenced if IPARAM(7) = 1 or 2.             |
    !! |___________________________________________________|

    !! local eigenvalues array, should be (n_ev+1) according to 
    !! documentation
    allocate(evalues_loc(n_ev  ))
    allocate(evector_loc(n,n_ev))
    evalues_loc = 0.
    evector_loc = 0.
    ldz = n !! leading dimension of evector
    !! others input must respect last call to znaupd

    !! complex cannot be symmetric anyway
    !if(flag_sym) then
    !call zseupd(flag_vectors,howmny,selection,evalues_loc,evector_loc, &
    !            ldz,sigma,workev,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,&
    !            iparam,ipntr,workd,workl,lworkl,rwork,info)
    !else
    call zneupd(flag_vectors,howmny,selection,evalues_loc,evector_loc,  &
                ldz,sigma,workev,bmat,n,which,n_ev,tol,resid,ncv,v,ldv, &
                iparam,ipntr,workd,workl,lworkl,rwork,info)

    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call arpack_error_info_eupd(info,ido,size_mat,n_ev,ncv,which,bmat,  &
                                iparam,howmny,ctx_err)
    if(ctx_err%ierr .ne. 0) return

    !! -----------------------------------------------------------------
    !! if lowest eigenvalues using A^{}-1, we have 1/eigenvalues
    if(flag_lowest_ev) then
      evalues_loc = 1./evalues_loc 
    end if
    !! -----------------------------------------------------------------

    !! check values are acceptable 
    do k=1,n_ev
      if(abs(evalues_loc(k)) > Huge(1.D0) .or.                          &
         ieee_is_nan(abs(evalues_loc(k)))) then
        ctx_err%ierr=-1004
        ctx_err%msg ="** ERROR: ARPACK eigenvalues is infinity or " //  &
                     "NaN [arpack_eigenvectors]"
        ctx_err%critic=.true.
        return
      end if
    end do

    !! sort array from low to high
    allocate(index_sort(n_ev))
    call arpack_sort_eigenvalues(abs(evalues_loc),n_ev,index_sort,ctx_err)

    !! output eigenvalues and eigenvectors, sorted
    do k=1,n_ev
      eigenvalues (k)     = evalues_loc(  index_sort(k)) !! cmplx(D(1:nev,1),D(1:nev,2))
      eigenvectors(:,k)   = evector_loc(:,index_sort(k))
    end do
    
    deallocate(index_sort)
    deallocate(v    )
    deallocate(workl)
    deallocate(workd)
    deallocate(rwork)
    deallocate(d    )
    deallocate(resid)
    deallocate(workev)
    deallocate(selection)
    deallocate(evalues_loc)
    deallocate(evector_loc)
    
    call time(time1)
    if(verb_level > 1) call print_arpack_eigenvalues   (eigenvalues,n_ev,ctx_err)
    if(verb_level > 0) call print_arpack_postprocessing(time1-time0,ctx_err)
    
    return
  end subroutine arpack_eigenvectors_double_complex


!!!!!  !----------------------------------------------------------------------------
!!!!!  ! DESCRIPTION:
!!!!!  !> @brief
!!!!!  !> compute N eigenvectors and eigenvalues using ARPACK. 
!!!!!  !> it can be: 
!!!!!  !> - the lowest  eigenvalues,
!!!!!  !> - the highest eigenvalues
!!!!!  !
!!!!!  !> @param[in]    A              : matrix in (line col val) format
!!!!!  !> @param[in]    size_mat       : matrix size
!!!!!  !> @param[in]    n_nz           : number of nonzeros
!!!!!  !> @param[in]    flag_sym       : indicates if symmetric matrix or not
!!!!!  !> @param[in]    n_ev           : number of eigenvectors/eigenvalues
!!!!!  !> @param[in]    str_version    : lowest or highest eigenvalues
!!!!!  !> @param[in]    str_method     : Ritz or Schur 
!!!!!  !> @param[inout] eigenvalues    : the output eigenvalues, sorted 
!!!!!  !> @param[inout] eigenvectors   : the output eigenvalues, sorted 
!!!!!  !> @param[inout] tol_accuracy   : allowed accuracy tolerance (see below)
!!!!!  !> @param[in]    str_product    : inner produc for <\lambda, v>
!!!!!  !> @param[out]   mem            : memory 
!!!!!  !> @param[in]    verblevel      : level of verbose
!!!!!  !> @param[inout] ctx_err        : context error
!!!!!  !----------------------------------------------------------------------------
!!!!!  subroutine arpack_eigenvectors_double_real(Alin,Acol,Aval,size_mat,   &
!!!!!                                 n_nz,flag_sym,n_ev,str_version,        &
!!!!!                                 str_method,eigenvalues,eigenvectors,   &
!!!!!                                 tol_accuracy,str_product,inner_prodmat,&
!!!!!                                 mem,verb_level,ctx_err)
!!!!!    implicit none
!!!!!
!!!!!    integer(kind=4)                    ,intent(in)   :: Alin(:)
!!!!!    integer(kind=4)                    ,intent(in)   :: Acol(:)
!!!!!    real   (kind=8)                    ,intent(in)   :: Aval(:)
!!!!!    integer                            ,intent(in)   :: size_mat,n_nz
!!!!!    logical                            ,intent(in)   :: flag_sym
!!!!!    integer                            ,intent(in)   :: n_ev
!!!!!    character(len=*)                   ,intent(in)   :: str_version
!!!!!    character(len=*)                   ,intent(in)   :: str_method
!!!!!    character(len=*)                   ,intent(in)   :: str_product
!!!!!    real   (kind=4)                    ,intent(in)   :: tol_accuracy
!!!!!    complex(kind=8)        ,allocatable,intent(inout):: eigenvectors(:,:)
!!!!!    complex(kind=8)        ,allocatable,intent(inout):: eigenvalues (:)
!!!!!    real   (kind=8)                    ,intent(in)   :: inner_prodmat(:)
!!!!!    integer(kind=4)                    ,intent(in)   :: verb_level
!!!!!    integer(kind=8)                    ,intent(out)  :: mem
!!!!!    type(t_error)                      ,intent(inout):: ctx_err
!!!!!    !! local
!!!!!    integer                     :: k,size_cx,size_int
!!!!!    real(kind=8)                :: time0,time1
!!!!!    integer       ,allocatable  :: index_sort(:)
!!!!!    logical                     :: flag_lowest_ev,flag_ritz,flag_schur
!!!!!    !! local solver mumps in case of lowest eigenvalues 
!!!!!    ! ctx_solver
!!!!!    type(t_parallelism)         :: ctx_paral_self
!!!!!    type(t_solver)              :: ctx_solver
!!!!!    type(t_matrix)              :: ctx_matrix
!!!!!    type(t_rhs)                 :: ctx_rhs
!!!!!    !! arpack aupd
!!!!!    character(len=2)            :: which
!!!!!    character(len=1)            :: bmat
!!!!!    integer                     :: maxitr,ncv,lworkl,ido,iiter
!!!!!    integer                     :: iparam(11),n,info,ldv,ipntr(11)
!!!!!    real   (kind=8),allocatable :: v(:,:),workd(:),workl(:),dR(:),dI(:)
!!!!!    real   (kind=8),allocatable :: resid(:)
!!!!!    real   (kind=8)             :: tol
!!!!!    !! arpack eupd
!!!!!    integer                     :: ldz
!!!!!    character(len=1)            :: howmny
!!!!!    logical                     :: flag_vectors
!!!!!    real(kind=8)                :: sigmaR,sigmaI
!!!!!    real(kind=8)   ,allocatable :: workev(:),evector_loc(:,:)
!!!!!    complex(kind=8),allocatable :: evalues_loc(:)
!!!!!    logical        ,allocatable :: selection(:)
!!!!!
!!!!!    call time(time0)
!!!!!    ctx_err%ierr=0
!!!!!    mem         =0
!!!!!    eigenvalues =0.0
!!!!!    eigenvectors=0.0
!!!!!    !! for more infos you have the website with subroutine infos:
!!!!!    !! https://people.sc.fsu.edu/~jburkardt%20/f_src/arpack/arpack.f90 
!!!!!    !! trac.alcf.anl.gov/projects/libs/browser/ARPACK/SRC/ssaupd.f?rev=65&order=size
!!!!!
!!!!!    size_cx = 8*2
!!!!!    size_int= 4
!!!!!    n       = size_mat
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! REMARK 
!!!!!    !! We always want the largest EV, for the lowest, it simply is 
!!!!!    !! the largest of A^{-1}, such that the matrix-vector product
!!!!!    !! will be replaced by a linear system resolution.
!!!!!    !! in which case we end up with 1/eigenvalues
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! REMARK 
!!!!!    !! Symmetric complex and symmetric double complex do not exist in 
!!!!!    !! ARPACK
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! basic errors 
!!!!!    if(size_mat <= 0 .or. n_nz <=0) then
!!!!!      ctx_err%ierr=-1003
!!!!!      ctx_err%msg ="** ERROR: inconsitent matrix [arpack_eigenvectors]"
!!!!!      ctx_err%critic=.true.
!!!!!      return   
!!!!!    end if
!!!!!    if(size_mat < n_ev) then
!!!!!      ctx_err%ierr=-1004
!!!!!      write(ctx_err%msg,'(a,i0,a,i0,a)') "** ERROR: "                // &
!!!!!           " asking for ", n_ev ," eigenvectors while the size of "  // &
!!!!!           "the matrix is only ",size_mat," [arpack_eigenvectors]"
!!!!!      ctx_err%critic=.true.
!!!!!      return   
!!!!!    end if
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! to define the inner product for the eigenvectors
!!!!!    iparam = 0
!!!!!    select case(trim(adjustl(str_product)))
!!!!!      case('identity')
!!!!!        bmat   = 'I'
!!!!!        ! ---------------------------------------------------------
!!!!!        !! RK: Matlab relies on arpack and it seems to be using G
!!!!!        ! ---------------------------------------------------------
!!!!!        ! | B = 'I' -> standard eigenvalue problem    A*x = lambda*x     |
!!!!!        ! | B = 'G' -> generalized eigenvalue problem A*x = lambda*B*x   |
!!!!!        ! |______________________________________________________________|
!!!!!        iparam(7) = 1 
!!!!!
!!!!!      case('bulk','vp')
!!!!!        !! for the diffusion equation it is \kappa \nabla \cdot (A \nabla) u
!!!!!        !! that is kappa is to be used, it is a diagonal matrix so quite ok.
!!!!!        !! we need the evaluation of kappa in each position of the dof.
!!!!!        bmat   = 'G'
!!!!!        iparam(7) = 2 !! symmetric positive definite B
!!!!!
!!!!!      case default
!!!!!        ctx_err%ierr=-1001
!!!!!        ctx_err%msg ="** ERROR: unrecognize inner product for ARPACK "//&
!!!!!                     "computation [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!    end select
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    flag_lowest_ev=.false.
!!!!!    select case (trim(adjustl(str_version)))
!!!!!      case('lowest','LOWEST','Lowest')  
!!!!!        flag_lowest_ev=.true.
!!!!!        !! need to initiliaze a self paralellism and solvers infos then.
!!!!!        call init_parallelism_selfproc(ctx_paral_self,ctx_err)
!!!!!        call solver_init_default_options(ctx_paral_self,flag_sym,       &
!!!!!                                         ctx_solver,0,ctx_err)
!!!!!        !! copy matrix in matrix context
!!!!!        ctx_matrix%dim_matrix_gb  = size_mat
!!!!!        ctx_matrix%dim_matrix_loc = size_mat
!!!!!        ctx_matrix%dim_nnz_gb     = n_nz
!!!!!        ctx_matrix%dim_nnz_loc    = n_nz
!!!!!        ctx_matrix%flag_block     = .false.
!!!!!        allocate(ctx_matrix%ilin(n_nz))
!!!!!        allocate(ctx_matrix%icol(n_nz))
!!!!!        allocate(ctx_matrix%Aval(n_nz))
!!!!!        ctx_matrix%ilin = int(Alin  ,kind=IKIND_MAT)
!!!!!        ctx_matrix%icol = int(Acol  ,kind=IKIND_MAT)
!!!!!        ctx_matrix%Aval = cmplx(Aval,kind=RKIND_MAT)
!!!!!        !! proceed with factorization
!!!!!        call process_factorization(ctx_paral_self,ctx_solver,ctx_matrix,&
!!!!!                                   ctx_err,o_flag_verb=.false.)
!!!!!        !! prepare dense rhs 
!!!!!        ctx_rhs%rhs_format='dense'
!!!!!        allocate(ctx_rhs%n_rhs_in_group(1)) ; ctx_rhs%n_rhs_in_group=1
!!!!!        ctx_rhs%i_group           = 1
!!!!!        ctx_rhs%n_group           = 1
!!!!!        ctx_rhs%n_rhs_component   = 1
!!!!!        allocate(ctx_rhs%csri(2)) !! only one rhs
!!!!!        ctx_rhs%csri(1) = 1 ; ctx_rhs%csri(2) = ctx_rhs%csri(1) + size_mat
!!!!!        allocate(ctx_rhs%icol(size_mat))
!!!!!        allocate(ctx_rhs%Rval(size_mat))
!!!!!        !! column gives one value per position 
!!!!!        do k=1,size_mat
!!!!!          ctx_rhs%icol(k) = k
!!!!!        end do
!!!!!
!!!!!      case('highest','HIGHEST','Highest')
!!!!!        flag_lowest_ev=.false.
!!!!!
!!!!!      case default
!!!!!        ctx_err%ierr=-1002
!!!!!        ctx_err%msg ="** ERROR: unrecognize eigenvectors version "//&
!!!!!                     "for ARPACK, it should be 'lowest' or "      //&
!!!!!                     "'highest' ones  [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!    end select
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    flag_ritz =.false.
!!!!!    flag_schur=.false.
!!!!!    select case (trim(adjustl(str_method)))
!!!!!      case('RITZ','Ritz','ritz')  
!!!!!        flag_ritz=.true.
!!!!!      case('Schur','SCHUR','schur')
!!!!!        flag_schur=.true.
!!!!!
!!!!!      case default
!!!!!        ctx_err%ierr=-1006
!!!!!        ctx_err%msg ="** ERROR: unrecognize eigenvectors method " //&
!!!!!                     "for ARPACK, only RITZ is supported for now "//&
!!!!!                     " [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!    end select
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! arbitrary maximal number of iterations
!!!!!    maxitr = 500000
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------    
!!!!!    which  = 'LM'
!!!!!      ! |______________________________________________________________|
!!!!!      ! | 'LA' - compute the NEV largest (algebraic) eigenvalues.      |
!!!!!      ! | 'SA' - compute the NEV smallest (algebraic) eigenvalues.     |
!!!!!      ! | 'LM' - compute the NEV largest (in magnitude) eigenvalues.   |
!!!!!      ! | 'SM' - compute the NEV smallest (in magnitude) eigenvalues.  |
!!!!!      ! | 'BE' - compute NEV eigenvalues, half from each end of the    |
!!!!!      ! |        spectrum.  When NEV is odd, compute one more from the |
!!!!!      ! |        high end than from the low end.                       |
!!!!!      ! | ****** NON-SYMMETRIC                                         |
!!!!!      ! | 'LM' -> want the NEV eigenvalues of largest magnitude.       |
!!!!!      ! | 'SM' -> want the NEV eigenvalues of smallest magnitude.      |
!!!!!      ! | 'LR' -> want the NEV eigenvalues of largest real part.       |
!!!!!      ! | 'SR' -> want the NEV eigenvalues of smallest real part.      |
!!!!!      ! | 'LI' -> want the NEV eigenvalues of largest imaginary part.  |
!!!!!      ! | 'SI' -> want the NEV eigenvalues of smallest imaginary part. |
!!!!!      ! |______________________________________________________________|
!!!!!      !! ---------------------------------------------------------------   
!!!!!
!!!!!    !! -----------------------------------------------------------------    
!!!!!    !! tolerance on computation
!!!!!    tol = dble(tol_accuracy)
!!!!!    ncv=int(n_ev*2.5) 
!!!!!    !! we must have (Ncv-Nev) must be >= 2 and <=N 
!!!!!    !! check for small problem
!!!!!    if(ncv >= size_mat) ncv=size_mat
!!!!!    
!!!!!    !! -----------------------------------------------------------------
!!!!!    
!!!!!    !! -----------------------------------------------------------------    
!!!!!    !! LWORKL
!!!!!    !! Symmetric complex does not exist, so we take non-symmetric 
!!!!!    !! if complex anyway
!!!!!    !if(flag_sym) then
!!!!!    !  lworkl =   ncv**2 + 8*ncv
!!!!!    !else
!!!!!    lworkl = 3*ncv**2 + 6*ncv 
!!!!!    !endif
!!!!!    !! we multiply to be sure
!!!!!    lworkl = int(lworkl*1.20)
!!!!!      ! |______________________________________________________________|
!!!!!      ! | non-symmetric real at least 3*NCV**2 + 6*NCV                 |
!!!!!      ! |     symmetric real at least   NCV**2 + 8*NCV                 |
!!!!!      ! | non-symmetric cx   at least 3*NCV**2 + 5*NCV                 |
!!!!!      ! |______________________________________________________________|
!!!!!    !! -----------------------------------------------------------------    
!!!!!
!!!!!    !! ----------------------------------------------------------------- 
!!!!!    info = 0 !! starting, see detail above
!!!!!    ido  = 0 !! starting, see detail above
!!!!!    iparam(1) = 1 !! starting, see detail above
!!!!!    iparam(3) = maxitr
!!!!!    !! it depends on the inner product
!!!!!    !! iparam(7) = 1 !! RK: it appears to be 3 in Matlab...
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! estimate memory of allocatable array
!!!!!    mem = size_cx*n_nz + size_int*n_nz*2         + & !! matrix A
!!!!!          !! eigenvectors and eigenvalues, tiems 2 for local array
!!!!!          (size_cx*n_ev + size_cx*n_ev*n)*2      + & 
!!!!!          size_cx*n*ncv   + & !! v    
!!!!!          size_cx*lworkl  + & !! workl
!!!!!          size_cx*3*n     + & !! workd
!!!!!          size_cx*ncv*2   + & !! d    
!!!!!          size_cx*n       + & !! resid
!!!!!          size_int*2*11   + & !! iparam and ipntr
!!!!!          size_cx*3*ncv   + & !! workev
!!!!!          ncv             + & !! selection
!!!!!          size_int*n_ev       !! index sort
!!!!!    !! add system for lowest ev
!!!!!    if(flag_lowest_ev) then
!!!!!      mem = mem + RKIND_MAT*2*n_nz + IKIND_MAT*n_nz*2 + & !! matrix
!!!!!                  RKIND_MAT*2*size_mat                + & !! solution
!!!!!                  !! rhs CSR
!!!!!                  IKIND_MAT*2 + IKIND_MAT*size_mat + RKIND_MAT*2*size_mat
!!!!!    end if
!!!!!
!!!!!    if(verb_level > 0) then
!!!!!      call print_arpack_welcome(n,n_ev,flag_lowest_ev,flag_ritz,        &
!!!!!                                flag_schur,trim(adjustl(str_product)),  &
!!!!!                                mem,ctx_err)
!!!!!    end if
!!!!!    !! -----------------------------------------------------------------
!!!!!    !!
!!!!!    !! Pre-processing using {sdcz}{sn}aupd routines
!!!!!    !! 
!!!!!    !! -----------------------------------------------------------------
!!!!!    ldv       = size_mat !! Leading dimension 
!!!!!    !! allocation
!!!!!    allocate(v    (n,ncv) ) !! [N,NCV], final set of Arnoldi basis vectors.
!!!!!    allocate(workl(lworkl)) !! working array
!!!!!    allocate(workd(3*n)   ) !! Reverse communication 
!!!!!    allocate(dR   (n_ev+1)) !! real      part
!!!!!    allocate(dI   (n_ev+1)) !! imaginary part
!!!!!    allocate(resid(n)     ) !! If INFO .EQ. 0, a random init is used
!!!!!    !! -----------------------------------------------------------------s
!!!!!
!!!!!    iiter=1
!!!!!    !! main loop
!!!!!    do while(iiter <= maxitr)
!!!!!      !! complex is alway non-symmetric
!!!!!      !if(flag_sym) then
!!!!!      !  call zsaupd(ido,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,iparam,  &
!!!!!      !              ipntr,workd,workl,lworkl,info)
!!!!!      !else
!!!!!      call dnaupd(ido,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,iparam,     &
!!!!!                  ipntr,workd,workl,lworkl,info)
!!!!!
!!!!!      !endif
!!!!!      iiter=iiter+1
!!!!!      select case(ido)
!!!!!        case(-1,1)
!!!!!          !! %--------------------------------------%
!!!!!          !! | Perform matrix vector multiplication |
!!!!!          !! |              y <--- OP*x             |
!!!!!          !! | workd(ipntr(1)) contains  X          |
!!!!!          !! | workd(ipntr(2)) will have Y          |
!!!!!          !! %--------------------------------------%
!!!!!          workd(ipntr(2):ipntr(2)+n-1)=0.0
!!!!! 
!!!!!          !! if we want the lowest one: we have to solve a linear problem
!!!!!          !! otherwize we perform the matrix vector product
!!!!!          ! -------------------------------------------------------
!!!!!          if(flag_lowest_ev) then
!!!!!            !! Solve y = A^{-1} x, 
!!!!!            !! i.e. x is the RHS and we solve Ay = x
!!!!!            !! 
!!!!!            ctx_rhs%Rval    = 0.0
!!!!!            ctx_rhs%Rval(:) = cmplx(workd(ipntr(1):ipntr(1)+n-1),kind=RKIND_MAT)
!!!!!            call process_solve(ctx_paral_self,ctx_solver,ctx_rhs,       &
!!!!!                               .false.,ctx_err,o_flag_verb=.false.)
!!!!!            workd(ipntr(2):ipntr(2)+n-1) = dble(ctx_solver%solution_global)
!!!!!
!!!!!          ! -------------------------------------------------------
!!!!!          else
!!!!!            !! matrix vector multiplication
!!!!!            do k=1,n_nz !! loop over all 
!!!!!              !! using line col val format for the matrix...
!!!!!              workd(ipntr(2)+Alin(k)-1)=workd(ipntr(2)+Alin(k)-1) + & 
!!!!!                                        workd(ipntr(1)+Acol(k)-1) * Aval(k)
!!!!!              !! Symmetry
!!!!!              if(flag_sym .and. Acol(k) > Alin(k)) then
!!!!!                workd(ipntr(2)+Acol(k)-1)=workd(ipntr(2)+Acol(k)-1) + & 
!!!!!                                          workd(ipntr(1)+Alin(k)-1) * Aval(k)
!!!!!              endif
!!!!!            enddo
!!!!!          end if
!!!!!        
!!!!!        case(2)
!!!!!          !! %--------------------------------------%
!!!!!          !! | Perform matrix vector multiplication |
!!!!!          !! |              y <--- M*x              |
!!!!!          !! | where M is the matrix of the inner   |
!!!!!          !! | product when we solve Ax = lambda M x|
!!!!!          !! |                                      |
!!!!!          !! | workd(ipntr(1)) contains  X          |
!!!!!          !! | workd(ipntr(2)) will have Y          |
!!!!!          !! %--------------------------------------%
!!!!!          
!!!!!          !! inner-product matrix vector multiplication
!!!!!          select case (trim(adjustl(str_product)))
!!!!!            case('bulk','vp')
!!!!!              !! diagonal multiplication only 
!!!!!              do k=1,n 
!!!!!                workd(ipntr(2)+k-1) = workd(ipntr(1)+k-1) * inner_prodmat(k)
!!!!!              end do
!!!!!
!!!!!            case default
!!!!!              ctx_err%ierr=-1001
!!!!!              ctx_err%msg ="** ERROR: unrecognize inner product for" // &
!!!!!                           " ARPACK computation [arpack_eigenvectors]"
!!!!!              ctx_err%critic=.true.
!!!!!              return
!!!!!          end select
!!!!!          
!!!!!        case default
!!!!!          exit !! we are done whatever it is an error or completion
!!!!!      end select
!!!!!      
!!!!!    enddo
!!!!!    
!!!!!    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!!!!!    call arpack_error_info_aupd(info,ido,maxitr,size_mat,n_ev,ncv,      &
!!!!!                                which,bmat,iparam,ctx_err)
!!!!!    if(ctx_err%ierr .ne. 0) return
!!!!!
!!!!!    !! clean solver infos and matrix
!!!!!    if(flag_lowest_ev) then
!!!!!      call matrix_clean(ctx_matrix)
!!!!!      call solver_clean(ctx_solver,ctx_err)
!!!!!      call rhs_clean(ctx_rhs,ctx_err)
!!!!!    end if
!!!!!    
!!!!!    !! print preprocessing ok
!!!!!    if(verb_level > 0) call print_arpack_preprocessing(iiter,ctx_err)
!!!!!
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !!
!!!!!    !! Post-processing using {sdcz}{sn}eupd routines
!!!!!    !! Eigenvectors may be also computed now if rvec = .true.
!!!!!    !! 
!!!!!    !! -----------------------------------------------------------------
!!!!!    flag_vectors = .true. !> compute eigenvectors too, it RVEC.
!!!!!    !! not symmetric for complex anyway 
!!!!!    !! if(.not. flag_sym) allocate(workev(3*ncv))
!!!!!    allocate(workev(3*ncv))
!!!!!    allocate(selection(ncv))   
!!!!!    
!!!!!    if(flag_ritz) then 
!!!!!      howmny = 'A'
!!!!!    end if 
!!!!!    if(flag_schur) then
!!!!!      howmny    = 'P'
!!!!!      selection = .true.
!!!!!    end if
!!!!!    sigmaR = 0.d0
!!!!!    sigmaI = 0.d0
!!!!!    !! | because we have iparam(1) = 1, there is no shift
!!!!!    !! |___________________________________________________|
!!!!!    !! | SIGMA   Complex*16  (INPUT)                       |
!!!!!    !! | If IPARAM(7) = 3 then SIGMA represents the shift. |
!!!!!    !! | Not referenced if IPARAM(7) = 1 or 2.             |
!!!!!    !! |___________________________________________________|
!!!!!
!!!!!    !! local eigenvalues array, should be (n_ev+1) according to 
!!!!!    !! documentation
!!!!!    allocate(evalues_loc(n_ev  ))
!!!!!    allocate(evector_loc(n,n_ev))
!!!!!    evalues_loc = 0.
!!!!!    evector_loc = 0.
!!!!!    ldz = n !! leading dimension of evector
!!!!!    !! others input must respect last call to znaupd
!!!!!
!!!!!    !! complex cannot be symmetric anyway
!!!!!    !if(flag_sym) then
!!!!!    !call zseupd(flag_vectors,howmny,selection,evalues_loc,evector_loc, &
!!!!!    !            ldz,sigma,workev,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,&
!!!!!    !            iparam,ipntr,workd,workl,lworkl,rwork,info)
!!!!!    !else
!!!!!    call dneupd(flag_vectors,howmny,selection,dR,dI,evector_loc,        &
!!!!!                ldz,sigmaR,sigmaI,workev,bmat,n,which,n_ev,tol,resid,   &
!!!!!                ncv,v,ldv,iparam,ipntr,workd,workl,lworkl,info)
!!!!!    
!!!!!    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!!!!!    call arpack_error_info_eupd(info,ido,size_mat,n_ev,ncv,which,bmat,  &
!!!!!                                iparam,howmny,ctx_err)
!!!!!    if(ctx_err%ierr .ne. 0) return
!!!!!
!!!!!    
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! if lowest eigenvalues using A^{}-1, we have 1/eigenvalues
!!!!!    evalues_loc = dcmplx(dR(1:n_ev),dI(1:n_ev))
!!!!!    if(flag_lowest_ev) then
!!!!!      evalues_loc = 1./evalues_loc 
!!!!!    end if
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! check values are acceptable 
!!!!!    do k=1,n_ev
!!!!!      if(abs(evalues_loc(k)) > Huge(1.D0) .or.                          &
!!!!!         ieee_is_nan(abs(evalues_loc(k)))) then
!!!!!        ctx_err%ierr=-1004
!!!!!        ctx_err%msg ="** ERROR: ARPACK eigenvalues is infinity or " //  &
!!!!!                     "NaN [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!      end if
!!!!!    end do
!!!!!
!!!!!    !! sort array from low to high
!!!!!    allocate(index_sort(n_ev))
!!!!!    call arpack_sort_eigenvalues(abs(evalues_loc),n_ev,index_sort,ctx_err)
!!!!!
!!!!!    !! output eigenvalues and eigenvectors, sorted
!!!!!    do k=1,n_ev
!!!!!      eigenvalues (k)     = evalues_loc(  index_sort(k)) !! cmplx(D(1:nev,1),D(1:nev,2))
!!!!!      eigenvectors(:,k)   = evector_loc(:,index_sort(k))
!!!!!    end do
!!!!!    
!!!!!    deallocate(index_sort)
!!!!!    deallocate(v    )
!!!!!    deallocate(workl)
!!!!!    deallocate(workd)
!!!!!    deallocate(dR   )
!!!!!    deallocate(dI   )
!!!!!    deallocate(resid)
!!!!!    deallocate(workev)
!!!!!    deallocate(selection)
!!!!!    deallocate(evalues_loc)
!!!!!    deallocate(evector_loc)
!!!!!    
!!!!!    call time(time1)
!!!!!    if(verb_level > 1) call print_arpack_eigenvalues   (eigenvalues,n_ev,ctx_err)
!!!!!    if(verb_level > 0) call print_arpack_postprocessing(time1-time0,ctx_err)
!!!!!    
!!!!!    return
!!!!!  end subroutine arpack_eigenvectors_double_real
!!!!!  
!!!!!  !----------------------------------------------------------------------------
!!!!!  ! DESCRIPTION:
!!!!!  !> @brief
!!!!!  !> compute N eigenvectors and eigenvalues using ARPACK. 
!!!!!  !> it can be: 
!!!!!  !> - the lowest  eigenvalues,
!!!!!  !> - the highest eigenvalues
!!!!!  !
!!!!!  !> @param[in]    A              : matrix in (line col val) format
!!!!!  !> @param[in]    size_mat       : matrix size
!!!!!  !> @param[in]    n_nz           : number of nonzeros
!!!!!  !> @param[in]    flag_sym       : indicates if symmetric matrix or not
!!!!!  !> @param[in]    n_ev           : number of eigenvectors/eigenvalues
!!!!!  !> @param[in]    str_version    : lowest or highest eigenvalues
!!!!!  !> @param[in]    str_method     : Ritz or Schur 
!!!!!  !> @param[inout] eigenvalues    : the output eigenvalues, sorted 
!!!!!  !> @param[inout] eigenvectors   : the output eigenvalues, sorted 
!!!!!  !> @param[inout] tol_accuracy   : allowed accuracy tolerance (see below)
!!!!!  !> @param[in]    str_product    : inner produc for <\lambda, v>
!!!!!  !> @param[out]   mem            : memory 
!!!!!  !> @param[in]    verblevel      : level of verbose
!!!!!  !> @param[inout] ctx_err        : context error
!!!!!  !----------------------------------------------------------------------------
!!!!!  subroutine arpack_eigenvectors_simple_complex(Alin,Acol,Aval,size_mat,&
!!!!!                                 n_nz,flag_sym,n_ev,str_version,        &
!!!!!                                 str_method,eigenvalues,eigenvectors,   &
!!!!!                                 tol_accuracy,str_product,inner_prodmat,&
!!!!!                                 mem,verb_level,ctx_err)
!!!!!    implicit none
!!!!!
!!!!!    integer(kind=4)                    ,intent(in)   :: Alin(:)
!!!!!    integer(kind=4)                    ,intent(in)   :: Acol(:)
!!!!!    complex(kind=4)                    ,intent(in)   :: Aval(:)
!!!!!    integer                            ,intent(in)   :: size_mat,n_nz
!!!!!    logical                            ,intent(in)   :: flag_sym
!!!!!    integer                            ,intent(in)   :: n_ev
!!!!!    character(len=*)                   ,intent(in)   :: str_version
!!!!!    character(len=*)                   ,intent(in)   :: str_method
!!!!!    character(len=*)                   ,intent(in)   :: str_product
!!!!!    real   (kind=4)                    ,intent(in)   :: tol_accuracy
!!!!!    complex(kind=4)        ,allocatable,intent(inout):: eigenvectors(:,:)
!!!!!    complex(kind=4)        ,allocatable,intent(inout):: eigenvalues (:)
!!!!!    real   (kind=8)                    ,intent(in)   :: inner_prodmat(:)
!!!!!    integer(kind=4)                    ,intent(in)   :: verb_level
!!!!!    integer(kind=8)                    ,intent(out)  :: mem
!!!!!    type(t_error)                      ,intent(inout):: ctx_err
!!!!!    !! local
!!!!!    integer                     :: k,size_cx,size_int
!!!!!    real(kind=8)                :: time0,time1
!!!!!    integer       ,allocatable  :: index_sort(:)
!!!!!    logical                     :: flag_lowest_ev,flag_ritz,flag_schur
!!!!!    !! local solver mumps in case of lowest eigenvalues 
!!!!!    ! ctx_solver
!!!!!    type(t_parallelism)         :: ctx_paral_self
!!!!!    type(t_solver)              :: ctx_solver
!!!!!    type(t_matrix)              :: ctx_matrix
!!!!!    type(t_rhs)                 :: ctx_rhs
!!!!!    !! arpack aupd
!!!!!    character(len=2)            :: which
!!!!!    character(len=1)            :: bmat
!!!!!    integer                     :: maxitr,ncv,lworkl,ido,iiter
!!!!!    integer                     :: iparam(11),n,info,ldv,ipntr(11)
!!!!!    complex(kind=4),allocatable :: v(:,:),workd(:),workl(:),d(:,:)
!!!!!    complex(kind=4),allocatable :: resid(:),rwork(:)
!!!!!    real   (kind=8)             :: tol
!!!!!    !! arpack eupd
!!!!!    integer                     :: ldz
!!!!!    character(len=1)            :: howmny
!!!!!    logical                     :: flag_vectors
!!!!!    complex(kind=4)             :: sigma
!!!!!    complex(kind=4),allocatable :: workev(:),evalues_loc(:),evector_loc(:,:)
!!!!!    logical        ,allocatable :: selection(:)
!!!!!
!!!!!    call time(time0)
!!!!!    ctx_err%ierr=0
!!!!!    mem         =0
!!!!!    eigenvalues =0.0
!!!!!    eigenvectors=0.0
!!!!!    !! for more infos you have the website with subroutine infos:
!!!!!    !! https://people.sc.fsu.edu/~jburkardt%20/f_src/arpack/arpack.f90 
!!!!!    !! trac.alcf.anl.gov/projects/libs/browser/ARPACK/SRC/ssaupd.f?rev=65&order=size
!!!!!
!!!!!    size_cx = 4*2
!!!!!    size_int= 4
!!!!!    n       = size_mat
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! REMARK 
!!!!!    !! We always want the largest EV, for the lowest, it simply is 
!!!!!    !! the largest of A^{-1}, such that the matrix-vector product
!!!!!    !! will be replaced by a linear system resolution.
!!!!!    !! in which case we end up with 1/eigenvalues
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! REMARK 
!!!!!    !! Symmetric complex and symmetric double complex do not exist in 
!!!!!    !! ARPACK
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! basic errors 
!!!!!    if(size_mat <= 0 .or. n_nz <=0) then
!!!!!      ctx_err%ierr=-1003
!!!!!      ctx_err%msg ="** ERROR: inconsitent matrix [arpack_eigenvectors]"
!!!!!      ctx_err%critic=.true.
!!!!!      return   
!!!!!    end if
!!!!!    if(size_mat < n_ev) then
!!!!!      ctx_err%ierr=-1004
!!!!!      write(ctx_err%msg,'(a,i0,a,i0,a)') "** ERROR: "                // &
!!!!!           " asking for ", n_ev ," eigenvectors while the size of "  // &
!!!!!           "the matrix is only ",size_mat," [arpack_eigenvectors]"
!!!!!      ctx_err%critic=.true.
!!!!!      return   
!!!!!    end if
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! to define the inner product for the eigenvectors
!!!!!    iparam = 0
!!!!!    select case(trim(adjustl(str_product)))
!!!!!      case('identity')
!!!!!        bmat   = 'I'
!!!!!        ! ---------------------------------------------------------
!!!!!        !! RK: Matlab relies on arpack and it seems to be using G
!!!!!        ! ---------------------------------------------------------
!!!!!        ! | B = 'I' -> standard eigenvalue problem    A*x = lambda*x     |
!!!!!        ! | B = 'G' -> generalized eigenvalue problem A*x = lambda*B*x   |
!!!!!        ! |______________________________________________________________|
!!!!!        iparam(7) = 1 
!!!!!
!!!!!      case('bulk','vp')
!!!!!        !! for the diffusion equation it is \kappa \nabla \cdot (A \nabla) u
!!!!!        !! that is kappa is to be used, it is a diagonal matrix so quite ok.
!!!!!        !! we need the evaluation of kappa in each position of the dof.
!!!!!        bmat   = 'G'
!!!!!        iparam(7) = 2 !! symmetric positive definite B
!!!!!
!!!!!      case default
!!!!!        ctx_err%ierr=-1001
!!!!!        ctx_err%msg ="** ERROR: unrecognize inner product for ARPACK "//&
!!!!!                     "computation [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!    end select
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    flag_lowest_ev=.false.
!!!!!    select case (trim(adjustl(str_version)))
!!!!!      case('lowest','LOWEST','Lowest')  
!!!!!        flag_lowest_ev=.true.
!!!!!        !! need to initiliaze a self paralellism and solvers infos then.
!!!!!        call init_parallelism_selfproc(ctx_paral_self,ctx_err)
!!!!!        call solver_init_default_options(ctx_paral_self,flag_sym,       &
!!!!!                                         ctx_solver,0,ctx_err)
!!!!!        !! copy matrix in matrix context
!!!!!        ctx_matrix%dim_matrix_gb  = size_mat
!!!!!        ctx_matrix%dim_matrix_loc = size_mat
!!!!!        ctx_matrix%dim_nnz_gb     = n_nz
!!!!!        ctx_matrix%dim_nnz_loc    = n_nz
!!!!!        ctx_matrix%flag_block     = .false.
!!!!!        allocate(ctx_matrix%ilin(n_nz))
!!!!!        allocate(ctx_matrix%icol(n_nz))
!!!!!        allocate(ctx_matrix%Aval(n_nz))
!!!!!        ctx_matrix%ilin = int(Alin  ,kind=IKIND_MAT)
!!!!!        ctx_matrix%icol = int(Acol  ,kind=IKIND_MAT)
!!!!!        ctx_matrix%Aval = cmplx(Aval,kind=RKIND_MAT)
!!!!!        !! proceed with factorization
!!!!!        call process_factorization(ctx_paral_self,ctx_solver,ctx_matrix,&
!!!!!                                   ctx_err,o_flag_verb=.false.)
!!!!!        !! prepare dense rhs 
!!!!!        ctx_rhs%rhs_format='dense'
!!!!!        allocate(ctx_rhs%n_rhs_in_group(1)) ; ctx_rhs%n_rhs_in_group=1
!!!!!        ctx_rhs%i_group           = 1
!!!!!        ctx_rhs%n_group           = 1
!!!!!        ctx_rhs%n_rhs_component   = 1
!!!!!        allocate(ctx_rhs%csri(2)) !! only one rhs
!!!!!        ctx_rhs%csri(1) = 1 ; ctx_rhs%csri(2) = ctx_rhs%csri(1) + size_mat
!!!!!        allocate(ctx_rhs%icol(size_mat))
!!!!!        allocate(ctx_rhs%Rval(size_mat))
!!!!!        !! column gives one value per position 
!!!!!        do k=1,size_mat
!!!!!          ctx_rhs%icol(k) = k
!!!!!        end do
!!!!!
!!!!!      case('highest','HIGHEST','Highest')
!!!!!        flag_lowest_ev=.false.
!!!!!
!!!!!      case default
!!!!!        ctx_err%ierr=-1002
!!!!!        ctx_err%msg ="** ERROR: unrecognize eigenvectors version "//&
!!!!!                     "for ARPACK, it should be 'lowest' or "      //&
!!!!!                     "'highest' ones  [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!    end select
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    flag_ritz =.false.
!!!!!    flag_schur=.false.
!!!!!    select case (trim(adjustl(str_method)))
!!!!!      case('RITZ','Ritz','ritz')  
!!!!!        flag_ritz=.true.
!!!!!      case('Schur','SCHUR','schur')
!!!!!        flag_schur=.true.
!!!!!
!!!!!      case default
!!!!!        ctx_err%ierr=-1006
!!!!!        ctx_err%msg ="** ERROR: unrecognize eigenvectors method " //&
!!!!!                     "for ARPACK, only RITZ is supported for now "//&
!!!!!                     " [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!    end select
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! arbitrary maximal number of iterations
!!!!!    maxitr = 500000
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! -----------------------------------------------------------------    
!!!!!    which  = 'LM'
!!!!!      ! |______________________________________________________________|
!!!!!      ! | 'LA' - compute the NEV largest (algebraic) eigenvalues.      |
!!!!!      ! | 'SA' - compute the NEV smallest (algebraic) eigenvalues.     |
!!!!!      ! | 'LM' - compute the NEV largest (in magnitude) eigenvalues.   |
!!!!!      ! | 'SM' - compute the NEV smallest (in magnitude) eigenvalues.  |
!!!!!      ! | 'BE' - compute NEV eigenvalues, half from each end of the    |
!!!!!      ! |        spectrum.  When NEV is odd, compute one more from the |
!!!!!      ! |        high end than from the low end.                       |
!!!!!      ! | ****** NON-SYMMETRIC                                         |
!!!!!      ! | 'LM' -> want the NEV eigenvalues of largest magnitude.       |
!!!!!      ! | 'SM' -> want the NEV eigenvalues of smallest magnitude.      |
!!!!!      ! | 'LR' -> want the NEV eigenvalues of largest real part.       |
!!!!!      ! | 'SR' -> want the NEV eigenvalues of smallest real part.      |
!!!!!      ! | 'LI' -> want the NEV eigenvalues of largest imaginary part.  |
!!!!!      ! | 'SI' -> want the NEV eigenvalues of smallest imaginary part. |
!!!!!      ! |______________________________________________________________|
!!!!!      !! ---------------------------------------------------------------   
!!!!!
!!!!!    !! -----------------------------------------------------------------    
!!!!!    !! tolerance on computation
!!!!!    tol = dble(tol_accuracy)
!!!!!    ncv=int(n_ev*2.5) 
!!!!!    !! we must have (Ncv-Nev) must be >= 2 and <=N 
!!!!!    !! check for small problem
!!!!!    if(ncv >= size_mat) ncv=size_mat
!!!!!    
!!!!!    !! -----------------------------------------------------------------
!!!!!    
!!!!!    !! -----------------------------------------------------------------    
!!!!!    !! LWORKL
!!!!!    !! Symmetric complex does not exist, so we take non-symmetric 
!!!!!    !! if complex anyway
!!!!!    !if(flag_sym) then
!!!!!    !  lworkl =   ncv**2 + 8*ncv
!!!!!    !else
!!!!!    lworkl = 3*ncv**2 + 6*ncv 
!!!!!    !endif
!!!!!    !! we multiply to be sure
!!!!!    lworkl = int(lworkl*1.25)
!!!!!      ! |______________________________________________________________|
!!!!!      ! | non-symmetric real at least 3*NCV**2 + 6*NCV                 |
!!!!!      ! |     symmetric real at least   NCV**2 + 8*NCV                 |
!!!!!      ! | non-symmetric cx   at least 3*NCV**2 + 5*NCV                 |
!!!!!      ! |______________________________________________________________|
!!!!!    !! -----------------------------------------------------------------    
!!!!!
!!!!!    !! ----------------------------------------------------------------- 
!!!!!    info = 0 !! starting, see detail above
!!!!!    ido  = 0 !! starting, see detail above
!!!!!    iparam(1) = 1 !! starting, see detail above
!!!!!    iparam(3) = maxitr
!!!!!    !! it depends on the inner product
!!!!!    !! iparam(7) = 1 !! RK: it appears to be 3 in Matlab...
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! estimate memory of allocatable array
!!!!!    mem = size_cx*n_nz + size_int*n_nz*2         + & !! matrix A
!!!!!          !! eigenvectors and eigenvalues, tiems 2 for local array
!!!!!          (size_cx*n_ev + size_cx*n_ev*n)*2      + & 
!!!!!          size_cx*n*ncv   + & !! v    
!!!!!          size_cx*lworkl  + & !! workl
!!!!!          size_cx*3*n     + & !! workd
!!!!!          size_cx*ncv     + & !! rwork
!!!!!          size_cx*ncv*2   + & !! d    
!!!!!          size_cx*n       + & !! resid
!!!!!          size_int*2*11   + & !! iparam and ipntr
!!!!!          size_cx*3*ncv   + & !! workev
!!!!!          ncv             + & !! selection
!!!!!          size_int*n_ev       !! index sort
!!!!!    !! add system for lowest ev
!!!!!    if(flag_lowest_ev) then
!!!!!      mem = mem + RKIND_MAT*2*n_nz + IKIND_MAT*n_nz*2 + & !! matrix
!!!!!                  RKIND_MAT*2*size_mat                + & !! solution
!!!!!                  !! rhs CSR
!!!!!                  IKIND_MAT*2 + IKIND_MAT*size_mat + RKIND_MAT*2*size_mat
!!!!!    end if
!!!!!
!!!!!    if(verb_level > 0) then
!!!!!      call print_arpack_welcome(n,n_ev,flag_lowest_ev,flag_ritz,        &
!!!!!                                flag_schur,trim(adjustl(str_product)),  &
!!!!!                                mem,ctx_err)
!!!!!    end if
!!!!!    !! -----------------------------------------------------------------
!!!!!    !!
!!!!!    !! Pre-processing using {sdcz}{sn}aupd routines
!!!!!    !! 
!!!!!    !! -----------------------------------------------------------------
!!!!!    ldv       = size_mat !! Leading dimension 
!!!!!    !! allocation
!!!!!    allocate(v    (n,ncv) ) !! [N,NCV], final set of Arnoldi basis vectors.
!!!!!    allocate(workl(lworkl)) !! working array
!!!!!    allocate(workd(3*n)   ) !! Reverse communication 
!!!!!    allocate(rwork(ncv)   ) 
!!!!!    allocate(d    (ncv,2) )
!!!!!    allocate(resid(n)     ) !! If INFO .EQ. 0, a random init is used
!!!!!    !! -----------------------------------------------------------------s
!!!!!
!!!!!    iiter=1
!!!!!    !! main loop
!!!!!    do while(iiter <= maxitr)
!!!!!      !! complex is alway non-symmetric
!!!!!      !if(flag_sym) then
!!!!!      !  call zsaupd(ido,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,iparam,  &
!!!!!      !              ipntr,workd,workl,lworkl,info)
!!!!!      !else
!!!!!      call cnaupd(ido,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,iparam,     &
!!!!!                  ipntr,workd,workl,lworkl,rwork,info)
!!!!!
!!!!!      !endif
!!!!!      iiter=iiter+1
!!!!!      select case(ido)
!!!!!        case(-1,1)
!!!!!          !! %--------------------------------------%
!!!!!          !! | Perform matrix vector multiplication |
!!!!!          !! |              y <--- OP*x             |
!!!!!          !! | workd(ipntr(1)) contains  X          |
!!!!!          !! | workd(ipntr(2)) will have Y          |
!!!!!          !! %--------------------------------------%
!!!!!          workd(ipntr(2):ipntr(2)+n-1)=0.0
!!!!!  
!!!!!          !! if we want the lowest one: we have to solve a linear problem
!!!!!          !! otherwize we perform the matrix vector product
!!!!!          ! -------------------------------------------------------
!!!!!          if(flag_lowest_ev) then
!!!!!            !! Solve y = A^{-1} x, 
!!!!!            !! i.e. x is the RHS and we solve Ay = x
!!!!!            !! 
!!!!!            ctx_rhs%Rval    = 0.0
!!!!!            ctx_rhs%Rval(:) = cmplx(workd(ipntr(1):ipntr(1)+n-1),kind=RKIND_MAT)
!!!!!            call process_solve(ctx_paral_self,ctx_solver,ctx_rhs,       &
!!!!!                               .false.,ctx_err,o_flag_verb=.false.)
!!!!!            workd(ipntr(2):ipntr(2)+n-1) = cmplx(ctx_solver%solution_global)
!!!!!
!!!!!          ! -------------------------------------------------------
!!!!!          else
!!!!!            !! matrix vector multiplication
!!!!!            do k=1,n_nz !! loop over all 
!!!!!              !! using line col val format for the matrix...
!!!!!              workd(ipntr(2)+Alin(k)-1)=workd(ipntr(2)+Alin(k)-1) + & 
!!!!!                                        workd(ipntr(1)+Acol(k)-1) * Aval(k)
!!!!!              !! Symmetry
!!!!!              if(flag_sym .and. Acol(k) > Alin(k)) then
!!!!!                workd(ipntr(2)+Acol(k)-1)=workd(ipntr(2)+Acol(k)-1) + & 
!!!!!                                          workd(ipntr(1)+Alin(k)-1) * Aval(k)
!!!!!              endif
!!!!!            enddo
!!!!!          end if
!!!!!        
!!!!!        case(2)
!!!!!          !! %--------------------------------------%
!!!!!          !! | Perform matrix vector multiplication |
!!!!!          !! |              y <--- M*x              |
!!!!!          !! | where M is the matrix of the inner   |
!!!!!          !! | product when we solve Ax = lambda M x|
!!!!!          !! |                                      |
!!!!!          !! | workd(ipntr(1)) contains  X          |
!!!!!          !! | workd(ipntr(2)) will have Y          |
!!!!!          !! %--------------------------------------%
!!!!!          
!!!!!          !! inner-product matrix vector multiplication
!!!!!          select case (trim(adjustl(str_product)))
!!!!!            case('bulk','vp')
!!!!!              !! diagonal multiplication only 
!!!!!              do k=1,n 
!!!!!                workd(ipntr(2)+k-1) = cmplx(workd(ipntr(1)+k-1)         &
!!!!!                                      *inner_prodmat(k),kind=4)
!!!!!              end do
!!!!!
!!!!!            case default
!!!!!              ctx_err%ierr=-1001
!!!!!              ctx_err%msg ="** ERROR: unrecognize inner product for" // &
!!!!!                           " ARPACK computation [arpack_eigenvectors]"
!!!!!              ctx_err%critic=.true.
!!!!!              return
!!!!!          end select
!!!!!          
!!!!!        case default
!!!!!          exit !! we are done whatever it is an error or completion
!!!!!      end select
!!!!!      
!!!!!    enddo
!!!!!    
!!!!!    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!!!!!    call arpack_error_info_aupd(info,ido,maxitr,size_mat,n_ev,ncv,      &
!!!!!                                which,bmat,iparam,ctx_err)
!!!!!    if(ctx_err%ierr .ne. 0) return
!!!!!
!!!!!    !! clean solver infos and matrix
!!!!!    if(flag_lowest_ev) then
!!!!!      call matrix_clean(ctx_matrix)
!!!!!      call solver_clean(ctx_solver,ctx_err)
!!!!!      call rhs_clean(ctx_rhs,ctx_err)
!!!!!    end if
!!!!!    
!!!!!    !! print preprocessing ok
!!!!!    if(verb_level > 0) call print_arpack_preprocessing(iiter,ctx_err)
!!!!!
!!!!!
!!!!!    !! -----------------------------------------------------------------
!!!!!    !!
!!!!!    !! Post-processing using {sdcz}{sn}eupd routines
!!!!!    !! Eigenvectors may be also computed now if rvec = .true.
!!!!!    !! 
!!!!!    !! -----------------------------------------------------------------
!!!!!    flag_vectors = .true. !> compute eigenvectors too, it RVEC.
!!!!!    !! not symmetric for complex anyway 
!!!!!    !! if(.not. flag_sym) allocate(workev(3*ncv))
!!!!!    allocate(workev(3*ncv))
!!!!!    allocate(selection(ncv))   
!!!!!    
!!!!!    if(flag_ritz) then 
!!!!!      howmny = 'A'
!!!!!    end if 
!!!!!    if(flag_schur) then
!!!!!      howmny    = 'P'
!!!!!      selection = .true.
!!!!!    end if
!!!!!    sigma = cmplx(0.)
!!!!!    !! | because we have iparam(1) = 1, there is no shift
!!!!!    !! |___________________________________________________|
!!!!!    !! | SIGMA   Complex*16  (INPUT)                       |
!!!!!    !! | If IPARAM(7) = 3 then SIGMA represents the shift. |
!!!!!    !! | Not referenced if IPARAM(7) = 1 or 2.             |
!!!!!    !! |___________________________________________________|
!!!!!
!!!!!    !! local eigenvalues array, should be (n_ev+1) according to 
!!!!!    !! documentation
!!!!!    allocate(evalues_loc(n_ev  ))
!!!!!    allocate(evector_loc(n,n_ev))
!!!!!    evalues_loc = 0.
!!!!!    evector_loc = 0.
!!!!!    ldz = n !! leading dimension of evector
!!!!!    !! others input must respect last call to znaupd
!!!!!
!!!!!    !! complex cannot be symmetric anyway
!!!!!    !if(flag_sym) then
!!!!!    !call zseupd(flag_vectors,howmny,selection,evalues_loc,evector_loc, &
!!!!!    !            ldz,sigma,workev,bmat,n,which,n_ev,tol,resid,ncv,v,ldv,&
!!!!!    !            iparam,ipntr,workd,workl,lworkl,rwork,info)
!!!!!    !else
!!!!!    call cneupd(flag_vectors,howmny,selection,evalues_loc,evector_loc,  &
!!!!!                ldz,sigma,workev,bmat,n,which,n_ev,tol,resid,ncv,v,ldv, &
!!!!!                iparam,ipntr,workd,workl,lworkl,rwork,info)
!!!!!
!!!!!    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!!!!!    call arpack_error_info_eupd(info,ido,size_mat,n_ev,ncv,which,bmat,  &
!!!!!                                iparam,howmny,ctx_err)
!!!!!    if(ctx_err%ierr .ne. 0) return
!!!!!
!!!!!    
!!!!!    !! -----------------------------------------------------------------
!!!!!    !! if lowest eigenvalues using A^{}-1, we have 1/eigenvalues
!!!!!    if(flag_lowest_ev) then
!!!!!      evalues_loc = 1./evalues_loc 
!!!!!    end if
!!!!!    !! -----------------------------------------------------------------
!!!!!
!!!!!    !! check values are acceptable 
!!!!!    do k=1,n_ev
!!!!!      if(abs(evalues_loc(k)) > Huge(1.D0) .or.                          &
!!!!!         ieee_is_nan(abs(evalues_loc(k)))) then
!!!!!        ctx_err%ierr=-1004
!!!!!        ctx_err%msg ="** ERROR: ARPACK eigenvalues is infinity or " //  &
!!!!!                     "NaN [arpack_eigenvectors]"
!!!!!        ctx_err%critic=.true.
!!!!!        return
!!!!!      end if
!!!!!    end do
!!!!!
!!!!!    !! sort array from low to high
!!!!!    allocate(index_sort(n_ev))
!!!!!    call arpack_sort_eigenvalues(abs(evalues_loc),n_ev,index_sort,ctx_err)
!!!!!
!!!!!    !! output eigenvalues and eigenvectors, sorted
!!!!!    do k=1,n_ev
!!!!!      eigenvalues (k)     = evalues_loc(  index_sort(k)) !! cmplx(D(1:nev,1),D(1:nev,2))
!!!!!      eigenvectors(:,k)   = evector_loc(:,index_sort(k))
!!!!!    end do
!!!!!    
!!!!!    deallocate(index_sort)
!!!!!    deallocate(v    )
!!!!!    deallocate(workl)
!!!!!    deallocate(workd)
!!!!!    deallocate(rwork)
!!!!!    deallocate(d    )
!!!!!    deallocate(resid)
!!!!!    deallocate(workev)
!!!!!    deallocate(selection)
!!!!!    deallocate(evalues_loc)
!!!!!    deallocate(evector_loc)
!!!!!    
!!!!!    call time(time1)
!!!!!    if(verb_level > 1) call print_arpack_eigenvalues   (eigenvalues,n_ev,ctx_err)
!!!!!    if(verb_level > 0) call print_arpack_postprocessing(time1-time0,ctx_err)
!!!!!    
!!!!!    return
!!!!!  end subroutine arpack_eigenvectors_simple_complex
  
end module m_arpack_eigenvectors

