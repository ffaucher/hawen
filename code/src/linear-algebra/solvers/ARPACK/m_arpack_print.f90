!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_arpack_print.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for ARPACK solver
!
!------------------------------------------------------------------------------
module m_arpack_print

  use m_raise_error,      only : t_error
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  implicit none
  
  private
  public :: print_arpack_welcome,print_arpack_preprocessing
  public :: print_arpack_postprocessing,print_arpack_eigenvalues
  public :: print_parpack_postprocessing,print_parpack_preprocessing
  public :: print_parpack_welcome

  interface print_arpack_eigenvalues
    module procedure print_arpack_eigenvalues_simple_complex
    module procedure print_arpack_eigenvalues_double_complex
    module procedure print_arpack_eigenvalues_double_real
  end interface
  
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> welcome arpack main infos
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_arpack_welcome(n,n_ev,flag_low,flag_ritz,flag_schur, &
                                  str_product,mem,ctx_err)
  
    implicit none
    integer                  ,intent(in)   :: n,n_ev
    logical                  ,intent(in)   :: flag_low,flag_ritz,flag_schur
    character(len=*)         ,intent(in)   :: str_product
    integer(kind=8)          ,intent(in)   :: mem
    type(t_error)            ,intent(inout):: ctx_err
    !!
    character(len=64)   :: str_mem,str

    if(ctx_err%ierr .ne. 0) return
    !! print infos
    !! if(ctx_paral%master) then
    write(6,'(a)')    separator
    
    if(flag_low) then
      str = "lowest"
    else
      str = "highest"
    end if
    
    if(flag_ritz)  str= trim(adjustl(str)) // " Ritz eigenvectors"
    if(flag_schur) str= trim(adjustl(str)) // " Schur eigenvectors"
    
    write(6,'(2a)')   indicator, " ARPACK library for " //trim(adjustl(str))
    write(6,'(a,i0,a,i0)') "- computation of ",n_ev,                    &
                           " eigenvectors, matrix of size ",n
    write(6,'(a)') "- using inner-product: "//str_product
    call cast_memory(mem,str_mem)
    write(6,'(2a)') "- total memory (incl. matrix) : ",trim(adjustl(str_mem))
    if(flag_schur) then
      write(6,'(a)') "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx "
      write(6,'(a)') "xx WARNING xx "
      write(6,'(a)') "xx   Shur eigenvectors are not recommended in general xx "
      write(6,'(a)') "xx   it is better to use the Ritz one.                xx "
      write(6,'(a)') "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx "
    end if
    
    return
    
  end subroutine print_arpack_welcome

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> welcome parpack main infos
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_parpack_welcome(n,n_ev,flag_low,flag_ritz,flag_schur,&
                                   str_product,mem,ctx_err)
  
    implicit none
    integer                  ,intent(in)   :: n,n_ev
    logical                  ,intent(in)   :: flag_low,flag_ritz,flag_schur
    character(len=*)         ,intent(in)   :: str_product
    integer(kind=8)          ,intent(in)   :: mem
    type(t_error)            ,intent(inout):: ctx_err
    !!
    character(len=64)   :: str_mem,str

    if(ctx_err%ierr .ne. 0) return
    !! print infos
    !! if(ctx_paral%master) then
    write(6,'(a)')    separator
    
    if(flag_low) then
      str = "lowest"
    else
      str = "highest"
    end if
    
    if(flag_ritz)  str= trim(adjustl(str)) // " Ritz eigenvectors"
    if(flag_schur) str= trim(adjustl(str)) // " Schur eigenvectors"
    
    write(6,'(2a)')   indicator, " PARPACK library for " //trim(adjustl(str))
    write(6,'(a,i0,a,i0)') "- computation of ",n_ev,                    &
                           " eigenvectors, matrix of size ",n
    write(6,'(a)') "- using inner-product: "//str_product
    call cast_memory(mem,str_mem)
    write(6,'(2a)') "- max memory proc (w. matrix) : ",trim(adjustl(str_mem))
    if(flag_schur) then
      write(6,'(a)') "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx "
      write(6,'(a)') "xx WARNING xx "
      write(6,'(a)') "xx   Shur eigenvectors are not recommended in general xx "
      write(6,'(a)') "xx   it is better to use the Ritz one.                xx "
      write(6,'(a)') "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx "
    end if

    return
    
  end subroutine print_parpack_welcome

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> arpack print eigenvalues
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_arpack_eigenvalues_double_real(eigenvalues,n_ev,ctx_err)
    implicit none
    integer                    ,intent(in)   :: n_ev
    real(kind=8) ,allocatable  ,intent(in)   :: eigenvalues(:)
    type(t_error)              ,intent(inout):: ctx_err
    !!
    integer           :: i
    character(len=1)  :: intlen
    character(len=64) :: formt
    
    if(ctx_err%ierr .ne. 0) return
  
    intlen = '0'
    if(n_ev <1000000) intlen='6'
    if(n_ev < 100000) intlen='5'
    if(n_ev <  10000) intlen='4'
    if(n_ev <   1000) intlen='3'
    if(n_ev <    100) intlen='2'
    if(n_ev <     10) intlen='1'
    formt = '(a,i'//intlen//',a,es14.7)'
    
    write(6,'(a,i0,a)') "- output list of ",n_ev," eigenvalues computed"
    do i=1,n_ev
      write(6,trim(adjustl(formt))) "- eigenvalue n°",i,": ",eigenvalues(i)
    end do

    return
  end subroutine print_arpack_eigenvalues_double_real
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> arpack print eigenvalues
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_arpack_eigenvalues_double_complex(eigenvalues,n_ev,ctx_err)
    implicit none
    integer                    ,intent(in)   :: n_ev
    complex(kind=8),allocatable,intent(in)   :: eigenvalues(:)
    type(t_error)              ,intent(inout):: ctx_err
    !!
    integer           :: i
    character(len=1)  :: intlen,sgn
    character(len=64) :: formt
    
    if(ctx_err%ierr .ne. 0) return
  
    intlen = '0'
    if(n_ev <1000000) intlen='6'
    if(n_ev < 100000) intlen='5'
    if(n_ev <  10000) intlen='4'
    if(n_ev <   1000) intlen='3'
    if(n_ev <    100) intlen='2'
    if(n_ev <     10) intlen='1'
    formt = '(a,i'//intlen//',a,es14.7,a,es14.7,a,es14.7)'
    
    write(6,'(a,i0,a)') "- output list of ",n_ev," eigenvalues computed"
    do i=1,n_ev
      if(aimag(eigenvalues(i)) < 0) then
        sgn='-'
      else
        sgn='+'
      end if
      write(6,trim(adjustl(formt))) "- eigenvalue n°",i,": (",          &
                                real(eigenvalues(i)), " "//sgn//" i",   &
                            abs(aimag(eigenvalues(i))),"). abs=",       &
                                abs(eigenvalues(i))
    end do

    return
  end subroutine print_arpack_eigenvalues_double_complex
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> arpack print eigenvalues
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_arpack_eigenvalues_simple_complex(eigenvalues,n_ev,ctx_err)
    implicit none
    integer                    ,intent(in)   :: n_ev
    complex(kind=4),allocatable,intent(in)   :: eigenvalues(:)
    type(t_error)              ,intent(inout):: ctx_err
    !!
    integer           :: i
    character(len=1)  :: intlen,sgn
    character(len=64) :: formt
    
    if(ctx_err%ierr .ne. 0) return
  
    intlen = '0'
    if(n_ev <1000000) intlen='6'
    if(n_ev < 100000) intlen='5'
    if(n_ev <  10000) intlen='4'
    if(n_ev <   1000) intlen='3'
    if(n_ev <    100) intlen='2'
    if(n_ev <     10) intlen='1'
    formt = '(a,i'//intlen//',a,es14.7,a,es14.7,a,es14.7)'
    
    write(6,'(a,i0,a)') "- output list of ",n_ev," eigenvalues computed"
    do i=1,n_ev
      if(aimag(eigenvalues(i)) < 0) then
        sgn='-'
      else
        sgn='+'
      end if
      write(6,trim(adjustl(formt))) "- eigenvalue n°",i,": (",          &
                                real(eigenvalues(i)), " "//sgn//" i",   &
                            abs(aimag(eigenvalues(i))),"). abs=",       &
                                abs(eigenvalues(i))
    end do


    return
  end subroutine print_arpack_eigenvalues_simple_complex
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> arpack preprocessing ok
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_arpack_preprocessing(iiter,ctx_err)
  
    implicit none
    integer                  ,intent(in)   :: iiter
    type(t_error)            ,intent(inout):: ctx_err
    !!
    
    if(ctx_err%ierr .ne. 0) return
    !! print infos
    !! if(ctx_paral%master) then
    write(6,'(a,i0,a)') "- Arpack pre-processing ok in ",iiter," iterations"
    return
    
  end subroutine print_arpack_preprocessing
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> parpack preprocessing ok
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_parpack_preprocessing(iiter,ctx_err)
  
    implicit none
    integer                  ,intent(in)   :: iiter
    type(t_error)            ,intent(inout):: ctx_err
    !!
    
    if(ctx_err%ierr .ne. 0) return
    !! print infos
    !! if(ctx_paral%master) then
    write(6,'(a,i0,a)') "- Parpack pre-processing ok in ",iiter," iterations"
    return
    
  end subroutine print_parpack_preprocessing

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> arpack postprocessing ok
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_arpack_postprocessing(time_gb,ctx_err)
  
    implicit none
    real(kind=8)             ,intent(in)   :: time_gb
    type(t_error)            ,intent(inout):: ctx_err
    !!
    character(len=64) :: str_time
    
    if(ctx_err%ierr .ne. 0) return
    
    call cast_time(time_gb,str_time)
    write(6,'(a)')  "- Arpack post-processing ok"
    write(6,'(2a)') "- Arpack total time          : ",trim(adjustl(str_time))
    write(6,'(a)')    separator
    return
    
  end subroutine print_arpack_postprocessing

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> arpack postprocessing ok
  !
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_parpack_postprocessing(time_gb,ctx_err)
  
    implicit none
    real(kind=8)             ,intent(in)   :: time_gb
    type(t_error)            ,intent(inout):: ctx_err
    !!
    character(len=64) :: str_time
    
    if(ctx_err%ierr .ne. 0) return
    
    call cast_time(time_gb,str_time)
    write(6,'(a)')  "- Parpack post-processing ok"
    write(6,'(2a)') "- Parpack total time         : ",trim(adjustl(str_time))
    write(6,'(a)')    separator
    return
    
  end subroutine print_parpack_postprocessing

end module m_arpack_print
