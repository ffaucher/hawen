!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_parpack_eigenvectors.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the eigenvectors and eigenvalues 
!> of a matrix using parallel library PARPACK 
!
!! ---------------------------------------------------------------------
!> NOTE THAT Symmetric complex and symmetric double complex do not 
!>           exist in PARPACK; we rely on routines:
!> - [sdzc]naupd  :: symmetric
!> - [sd]saupd    :: non-symmetric
!
!------------------------------------------------------------------------------
module m_parpack_eigenvectors
  !! module used -------------------------------------------------------
  use m_define_precision,       only: RKIND_MAT,IKIND_MAT
  use m_raise_error,            only: t_error
  use m_distribute_error,       only: distribute_error
  use m_arpack_print,           only: print_parpack_welcome,            &
                                      print_parpack_preprocessing,      &
                                      print_parpack_postprocessing,     &
                                      print_arpack_eigenvalues
  use m_time,                   only: time
  !> solver for linear problem
  use m_ctx_parallelism,        only: t_parallelism
  use m_barrier,                only: barrier
  use m_broadcast,              only: broadcast
  use m_allreduce_sum,          only: allreduce_sum
  use m_api_solver,             only: t_solver, solver_clean,           &
                                      solver_init_default_options,      &
                                      process_factorization,            &
                                      process_solve
  use m_ctx_matrix,             only: t_matrix, matrix_clean
  use m_ctx_rhs,                only: t_rhs, rhs_clean
  use m_arpack_sort_ev,         only: arpack_sort_eigenvalues
  use, intrinsic                   :: ieee_arithmetic

  implicit none 
  !! ------------------------------------------------------------------- 

  !> to compute the eigenvectors and eigenvalues
  interface parpack_eigenvectors
    module procedure parpack_eigenvectors_double_complex
    module procedure parpack_eigenvectors_simple_complex
    !! note that for real matrices, a complete symmetric version exists
    !! Also the eigenvalues can still be complex, so careful...
    ! %-----------------------------------------------%
    ! | The real part of the eigenvalue is returned   |
    ! | in the first column of the two dimensional    |
    ! | array D, and the imaginary part is returned   |
    ! | in the second column of D.  The corresponding |
    ! | eigenvectors are returned in the first NEV    |
    ! | columns of the two dimensional array V if     |
    ! | requested.  Otherwise, an orthogonal basis    |
    ! | for the invariant subspace corresponding to   |
    ! | the eigenvalues in D is returned in V.        |
    ! %-----------------------------------------------%
    !module procedure parpack_eigenvectors_double_real
    !module procedure parpack_eigenvectors_simple_real
  end interface parpack_eigenvectors

  private
  public  :: parpack_eigenvectors


  !! -------------------------------------------------------------------
  !> we mostly rely on PARPACK function
  !> p[sdcz]naupd => non-symmetric matrix pre-processing  eigenvectors
  !> p[sd]  saupd =>     symmetric matrix pre-processing  eigenvectors
  !> p[sdcz]neupd => non-symmetric matrix post-processing eigenvectors
  !> p[sd]  seupd =>     symmetric matrix post-processing eigenvectors
  !
  !> very similar to ARPACK, cf. dedicated subroutines
  !! -------------------------------------------------------------------

  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> returns error information for PARPACK p{sdcz}{sn}aupd routines. 
  !> cf. the info return codes above.
  !
  !> @param[in]    info           : error code in PARPACK
  !> @param[in]    ido            : ido code in PARPACK
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parpack_error_info_aupd(info,ido,maxitr,size_mat,n_ev,ncv, &
                                     which,bmat,iparam,ctx_err)
    implicit none

    integer          ,intent(in)   :: info,ido,n_ev,ncv
    integer          ,intent(in)   :: maxitr,size_mat,iparam(11)
    character(len=1) ,intent(in)   :: bmat
    character(len=2) ,intent(in)   :: which
    type(t_error)    ,intent(inout):: ctx_err


    !! there is no error if ido == 99 with info == 0,
    !! otherwize, get information on the error.
    if(ido .ne. 99 .or. info .ne. 0) then
      ctx_err%ierr  =-1003
      ctx_err%critic=.true.

      select case(info)
        case(   1)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": maximum number of iteration (",maxitr,                     &
          ") reached [PARPACK_eigenvectors]"

        case(   3)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": no shifts during cycle of Arnoldi; Try to increase NCV "// &
          "[PARPACK_eigenvectors]"

        case(  -1)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": N must be positive and it is ", size_mat," [PARPACK_eigenvectors]"

        case(  -2)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": Nev must be positive and it is ", n_ev," [PARPACK_eigenvectors]"
        case(  -3)
          write(ctx_err%msg,'(a,i0,a,i0,a,i0,a,i0,a)')                  &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": (Ncv-Nev) must be >= 2 and <=N and we have Ncv=", ncv,     &
          " Nev=",n_ev," N=",size_mat," [PARPACK_eigenvectors]"
          
        case(  -4)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": maximum number of iteration",maxitr," must be > 0 "     // &
          "[PARPACK_eigenvectors]"
          
        case(  -5)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": parameter WHICH is '"//which//"' and is not recognized "// &
          "[PARPACK_eigenvectors]"

        case(  -6)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": parameter BMAT is '"//bmat//"' and is not recognized "//   &
          "(should be I or G) [PARPACK_eigenvectors]"

        case(  -7)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": insufficient length of work array [PARPACK_eigenvectors]"

        case(  -8)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": error return from LAPACK eigenvalue calculation "        //&
          "[PARPACK_eigenvectors]"

        case(  -9)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": starting vector is zero [PARPACK_eigenvectors]"
          
        case( -10)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": IPARAM(7) is ",iparam(7)," while is must be 1, 2, 3 or " //&
          "4 [PARPACK_eigenvectors]"
          
        case( -11)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": IPARAM(7)=1 with BMAT=G is incompatible [PARPACK_eigenvectors]"
          
        case( -12)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": IPARAM(1) is ",iparam(1)," while is must be 1 or 0 "     //&
          "[PARPACK_eigenvectors]"
          
        case(-9999)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          ": could not buil Arnoldi factorization, current size is ",   &
          iparam(5)," [PARPACK_eigenvectors]"

        case default      
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]aupd, error code ",info,        &
          "ido code is ",ido," [PARPACK_eigenvectors]"
      end select
    end if

    return
  end subroutine parpack_error_info_aupd

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> returns error information for PARPACK p{sdcz}{sn}eupd routines. 
  !> cf. the info return codes above.
  !
  !> @param[in]    info           : error code in PARPACK
  !> @param[in]    ido            : ido code in PARPACK
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parpack_error_info_eupd(info,ido,size_mat,n_ev,ncv,        &
                                     which,bmat,iparam,howmny,ctx_err)
    implicit none

    integer          ,intent(in)   :: info,ido,n_ev,ncv
    integer          ,intent(in)   :: size_mat,iparam(11)
    character(len=1) ,intent(in)   :: bmat,howmny
    character(len=2) ,intent(in)   :: which
    type(t_error)    ,intent(inout):: ctx_err


    !! there is no error if ido == 99 with info == 0,
    !! otherwize, get information on the error.
    if(ido .ne. 99 .or. info .ne. 0) then
      ctx_err%ierr  =-1003
      ctx_err%critic=.true.

      select case(info)
        case(   1)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": Schur from Lapack could not be reordered [PARPACK_eigenvectors]"

        case(  -1)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": N must be positive and it is ", size_mat," [PARPACK_eigenvectors]"

        case(  -2)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": Nev must be positive and it is ", n_ev," [PARPACK_eigenvectors]"

        case(  -3)
          write(ctx_err%msg,'(a,i0,a,i0,a,i0,a,i0,a)')                  &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": (Ncv-Nev) must be >= 2 and <= N and we have Ncv=", ncv,    &
          " Nev=",n_ev," N=",size_mat," [PARPACK_eigenvectors]"

        case(  -5)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": parameter WHICH is '"//which//"' and is not recognized "// &
          "[PARPACK_eigenvectors]"

        case(  -6)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": parameter BMAT is '"//bmat//"' and is not recognized "//   &
          "(should be I or G) [PARPACK_eigenvectors]"

        case(  -7)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": insufficient length of work array [PARPACK_eigenvectors]"

        case(  -8)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": error return from LAPACK eigenvalue calculation "        //&
          "[PARPACK_eigenvectors]"

        case(  -9)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": error in Lapack routine ztrevc [PARPACK_eigenvectors]"
          
        case( -10)
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": IPARAM(7) is ",iparam(7)," while is must be 1, 2, 3 or " //&
          "4 [PARPACK_eigenvectors]"
          
        case( -11)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": IPARAM(7)=1 with BMAT=G is incompatible [PARPACK_eigenvectors]"

        case( -12,-13)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": howmny is'"//howmny//"', it should be A or P [PARPACK_eigenvectors]"

        case( -14)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": Znaupd did not find eigenvalues with accuracy [PARPACK_eigenvectors]"

        case( -15)
          write(ctx_err%msg,'(a,i0,a)')                                 &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          ": {sdcz}{sn}upd dgot different count than {sdcz}{sn}aupd " //&
          "[PARPACK_eigenvectors]"

        case default      
          write(ctx_err%msg,'(a,i0,a,i0,a)')                            &
          "** ERROR in PARPACK [sdcz][sn]eupd, error code ",info,        &
          "ido code is ",ido," [PARPACK_eigenvectors]"
      end select
    end if

    return
  end subroutine parpack_error_info_eupd

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute N eigenvectors and eigenvalues using PARPACK. 
  !> it can be: 
  !> - the lowest  eigenvalues,
  !> - the highest eigenvalues
  !
  !> @param[in]    A              : matrix in (line col val) format
  !> @param[in]    size_mat       : matrix size
  !> @param[in]    n_nz           : number of nonzeros
  !> @param[in]    flag_sym       : indicates if symmetric matrix or not
  !> @param[in]    n_ev           : number of eigenvectors/eigenvalues
  !> @param[in]    str_version    : lowest or highest eigenvalues
  !> @param[in]    str_method     : Ritz or Schur 
  !> @param[inout] eigenvalues    : the output eigenvalues, sorted 
  !> @param[inout] eigenvectors   : the output eigenvalues, sorted 
  !> @param[inout] tol_accuracy   : allowed accuracy tolerance (see below)
  !> @param[in]    str_product    : inner produc for <\lambda, v>
  !> @param[out]   mem            : memory 
  !> @param[in]    verblevel      : level of verbose
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parpack_eigenvectors_double_complex(ctx_paral,Alin,Acol,   &
                                  Aval,size_mat_gb,size_mat_loc,n_nz_gb,&
                                  n_nz_loc,flag_sym,n_ev,str_version,   &
                                  str_method,eigenvalues,eigenvectors,  &
                                  tol_accuracy,str_product,             &
                                  inner_prodmat,mem,verb_level,ctx_err)

    implicit none

    type(t_parallelism)                ,intent(in)   :: ctx_paral
    integer(kind=4)        ,allocatable,intent(in)   :: Alin(:)
    integer(kind=4)        ,allocatable,intent(in)   :: Acol(:)
    complex(kind=8)        ,allocatable,intent(in)   :: Aval(:)
    integer                            ,intent(in)   :: size_mat_gb
    integer                            ,intent(in)   :: size_mat_loc
    integer                            ,intent(in)   :: n_nz_gb,n_nz_loc
    logical                            ,intent(in)   :: flag_sym
    integer                            ,intent(in)   :: n_ev
    character(len=*)                   ,intent(in)   :: str_version
    character(len=*)                   ,intent(in)   :: str_method
    character(len=*)                   ,intent(in)   :: str_product
    real   (kind=4)                    ,intent(in)   :: tol_accuracy
    complex(kind=8)        ,allocatable,intent(inout):: eigenvectors(:,:)
    complex(kind=8)        ,allocatable,intent(inout):: eigenvalues (:)
    real   (kind=8)                    ,intent(in)   :: inner_prodmat(:)
    integer(kind=4)                    ,intent(in)   :: verb_level
    integer(kind=8)                    ,intent(out)  :: mem
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer                     :: k,size_cx,size_int
    real(kind=8)                :: time0,time1
    integer       ,allocatable  :: index_sort(:)
    logical                     :: flag_lowest_ev,flag_ritz,flag_schur
    !! local solver mumps in case of lowest eigenvalues 
    ! ctx_solver
    type(t_solver)              :: ctx_solver
    type(t_matrix)              :: ctx_matrix
    type(t_rhs)                 :: ctx_rhs
    complex(kind=8),allocatable :: sol_loc(:),sol_gb(:),vect(:),vect_gb(:)
    complex(kind=8),allocatable :: evect_loc(:,:),evect_gb(:,:)
    !! arpack aupd
    character(len=2)            :: which
    character(len=1)            :: bmat
    integer                     :: maxitr,ncv,lworkl,ido,iiter
    integer                     :: iparam(11),n,info,ldv,ipntr(14)
    complex(kind=8),allocatable :: v(:,:),workd(:),workl(:),d(:,:)
    complex(kind=8),allocatable :: resid(:),rwork(:)
    real   (kind=8)             :: tol
    !! arpack eupd
    integer                     :: ldz
    character(len=1)            :: howmny
    logical                     :: flag_vectors
    complex(kind=8)             :: sigma
    complex(kind=8),allocatable :: workev(:),evalues_loc(:)
    complex(kind=8),allocatable :: evector_loc(:,:)
    logical        ,allocatable :: selection(:)

    call time(time0)
    ctx_err%ierr=0
    mem         =0
    eigenvalues =0.0
    eigenvectors=0.0

    size_cx = 8*2
    size_int= 4
    n       = size_mat_loc !! n is the local dimension problem <!

    !! -----------------------------------------------------------------
    !! REMARK 
    !! We always want the largest EV, for the lowest, it simply is 
    !! the largest of A^{-1}, such that the matrix-vector product
    !! will be replaced by a linear system resolution.
    !! in which case we end up with 1/eigenvalues
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! REMARK 
    !! Symmetric complex and symmetric double complex do not exist in 
    !! PARPACK
    !! -----------------------------------------------------------------

    !! basic errors 
    if(size_mat_loc <= 0 .or. n_nz_loc <=0 .or.                         &
       size_mat_gb  <= 0 .or. n_nz_gb  <=0) then
      ctx_err%ierr=-1003
      ctx_err%msg ="** ERROR: inconsitent matrix [parpack_eigenvectors]"
      ctx_err%critic=.true.
    end if
    if(size_mat_gb < n_ev) then
      ctx_err%ierr=-1004
      write(ctx_err%msg,'(a,i0,a,i0,a)') "** ERROR: "                // &
           " asking for ", n_ev ," eigenvectors while the size of "  // &
           "the matrix is only ",size_mat_gb," [parpack_eigenvectors]"
      ctx_err%critic=.true.
    end if
    !! -----------------------------------------------------------------
    !! to define the inner product for the eigenvectors
    iparam = 0
    select case(trim(adjustl(str_product)))
      case('identity')
        bmat   = 'I'
        ! ---------------------------------------------------------
        !! RK: Matlab relies on arpack and it seems to be using G
        ! ---------------------------------------------------------
        ! | B = 'I' -> standard eigenvalue problem    A*x = lambda*x     |
        ! | B = 'G' -> generalized eigenvalue problem A*x = lambda*B*x   |
        ! |______________________________________________________________|
        iparam(7) = 1 

      case('bulk','vp')
        !! for the diffusion equation it is \kappa \nabla \cdot (A \nabla) u
        !! that is kappa is to be used, it is a diagonal matrix so quite ok.
        !! we need the evaluation of kappa in each position of the dof.
        bmat   = 'G'
        iparam(7) = 2 !! symmetric positive definite B

      case default
        ctx_err%ierr=-1001
        ctx_err%msg ="** ERROR: unrecognize inner product for PARPACK "//&
                     "computation [parpack_eigenvectors]"
        ctx_err%critic=.true.
    end select
    !! -----------------------------------------------------------------
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! -----------------------------------------------------------------
    flag_lowest_ev=.false.
    select case (trim(adjustl(str_version)))
      case('lowest','LOWEST','Lowest')  
        flag_lowest_ev=.true.
        !! parallelism already given in input, contrary to ARPACK, 
        !! everyone is working here. 
        !! call init_parallelism_selfproc(ctx_paral_self,ctx_err)
        call solver_init_default_options(ctx_paral,flag_sym,            &
                                         ctx_solver,0,ctx_err)
        !! copy matrix in matrix context
        ctx_matrix%dim_matrix_gb  = size_mat_gb
        ctx_matrix%dim_matrix_loc = size_mat_loc
        ctx_matrix%dim_nnz_gb     = n_nz_gb
        ctx_matrix%dim_nnz_loc    = n_nz_loc
        ctx_matrix%flag_block     = .false.
        allocate(ctx_matrix%ilin(n_nz_loc))
        allocate(ctx_matrix%icol(n_nz_loc))
        allocate(ctx_matrix%Aval(n_nz_loc))
        ctx_matrix%ilin = int(Alin  ,kind=IKIND_MAT)
        ctx_matrix%icol = int(Acol  ,kind=IKIND_MAT)
        ctx_matrix%Aval = cmplx(Aval,kind=RKIND_MAT)
        !! proceed with factorization
        call process_factorization(ctx_paral,ctx_solver,ctx_matrix,     &
                                   ctx_err,o_flag_verb=.false.)
        !! prepare dense rhs 
        ctx_rhs%rhs_format='dense'
        allocate(ctx_rhs%n_rhs_in_group(1)) ; ctx_rhs%n_rhs_in_group=1
        ctx_rhs%i_group           = 1
        ctx_rhs%n_group           = 1
        ctx_rhs%n_rhs_component   = 1
        !! only master
        if(ctx_paral%master) then
          allocate(ctx_rhs%csri(2)) !! only one rhs
          ctx_rhs%csri(1) = 1 ; ctx_rhs%csri(2) = ctx_rhs%csri(1) + size_mat_gb
          allocate(ctx_rhs%icol(size_mat_gb))
          allocate(ctx_rhs%Rval(size_mat_gb))
          !! column gives one value per position 
          do k=1,size_mat_gb
            ctx_rhs%icol(k) = k
          end do
        end if

      case('highest','HIGHEST','Highest')
        flag_lowest_ev=.false.

      case default
        ctx_err%ierr=-1002
        ctx_err%msg ="** ERROR: unrecognize eigenvectors version "//&
                     "for PARPACK, it should be 'lowest' or "      //&
                     "'highest' ones  [parpack_eigenvectors]"
        ctx_err%critic=.true.
    end select
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    flag_ritz =.false.
    flag_schur=.false.
    select case (trim(adjustl(str_method)))
      case('RITZ','Ritz','ritz')  
        flag_ritz=.true.
      case('Schur','SCHUR','schur')
        flag_schur=.true.

      case default
        ctx_err%ierr=-1006
        ctx_err%msg ="** ERROR: unrecognize eigenvectors method "  //&
                     "for PARPACK, only RITZ is supported for now "//&
                     " [parpack_eigenvectors]"
        ctx_err%critic=.true.
    end select
    !! -----------------------------------------------------------------
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! -----------------------------------------------------------------
    !! arbitrary maximal number of iterations
    maxitr = 500000
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------    
    which  = 'LM'
      ! |______________________________________________________________|
      ! | 'LA' - compute the NEV largest (algebraic) eigenvalues.      |
      ! | 'SA' - compute the NEV smallest (algebraic) eigenvalues.     |
      ! | 'LM' - compute the NEV largest (in magnitude) eigenvalues.   |
      ! | 'SM' - compute the NEV smallest (in magnitude) eigenvalues.  |
      ! | 'BE' - compute NEV eigenvalues, half from each end of the    |
      ! |        spectrum.  When NEV is odd, compute one more from the |
      ! |        high end than from the low end.                       |
      ! | ****** NON-SYMMETRIC                                         |
      ! | 'LM' -> want the NEV eigenvalues of largest magnitude.       |
      ! | 'SM' -> want the NEV eigenvalues of smallest magnitude.      |
      ! | 'LR' -> want the NEV eigenvalues of largest real part.       |
      ! | 'SR' -> want the NEV eigenvalues of smallest real part.      |
      ! | 'LI' -> want the NEV eigenvalues of largest imaginary part.  |
      ! | 'SI' -> want the NEV eigenvalues of smallest imaginary part. |
      ! |______________________________________________________________|
      !! ---------------------------------------------------------------   

    !! -----------------------------------------------------------------    
    !! tolerance on computation
    tol = dble(tol_accuracy)
    ncv=int(n_ev*3) 
    !! we must have (Ncv-Nev) must be >= 2 and <=N 
    !! check for small problem
    if(ncv >= size_mat_gb) ncv=size_mat_gb
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------    
    !! LWORKL
    !! Symmetric complex does not exist, so we take non-symmetric 
    !! if complex anyway
    !if(flag_sym) then
    !  lworkl =   ncv**2 + 8*ncv
    !else
    lworkl = 3*ncv**2 + 6*ncv 
    !endif
    !! we multiply to be sure
    lworkl = int(lworkl*1.10)
      ! |______________________________________________________________|
      ! | non-symmetric real at least 3*NCV**2 + 6*NCV                 |
      ! |     symmetric real at least   NCV**2 + 8*NCV                 |
      ! | non-symmetric cx   at least 3*NCV**2 + 5*NCV                 |
      ! |______________________________________________________________|
    !! -----------------------------------------------------------------    

    !! ----------------------------------------------------------------- 
    info = 0 !! starting, see detail above
    ido  = 0 !! starting, see detail above
    iparam(1) = 1 !! starting, see detail above
    iparam(3) = maxitr
    !! it depends on the inner product
    !! iparam(7) = 1 !! RK: it appears to be 3 in Matlab...
    !! -----------------------------------------------------------------

    !! estimate memory of allocatable array
    mem = size_cx*n_nz_loc + size_int*n_nz_loc*2 + & !! matrix Aloc
          !! eigenvectors and eigenvalues, tiems 2 for local array
          (size_cx*n_ev + size_cx*n_ev*n)*2      + & 
          size_cx*n*ncv   + & !! v    
          size_cx*lworkl  + & !! workl
          size_cx*3*n     + & !! workd
          size_cx*ncv     + & !! rwork
          size_cx*ncv*2   + & !! d    
          size_cx*n       + & !! resid
          size_int*11 + size_int*14 + & !! iparam and ipntr
          size_cx*3*ncv   + & !! workev
          ncv             + & !! selection
          size_int*n_ev   + & !! index sort
          size_cx*size_mat_gb*4 !! global array for assemble
    !! for each processors <!
    
    !! add system for lowest ev
    if(flag_lowest_ev) then
      mem = mem + RKIND_MAT*2*n_nz_loc + IKIND_MAT*n_nz_loc*2 + & !! matrix
                  RKIND_MAT*2*size_mat_gb                     + & !! solution
                  !! rhs CSR
                  IKIND_MAT*2 + IKIND_MAT*size_mat_gb +                 &
                  RKIND_MAT*2*size_mat_gb
    end if

    if(verb_level > 0 .and. ctx_paral%master) then
      call print_parpack_welcome(n,n_ev,flag_lowest_ev,flag_ritz,       &
                                flag_schur,trim(adjustl(str_product)),  &
                                mem,ctx_err)
    end if
    !! -----------------------------------------------------------------
    !!
    !! Pre-processing using {sdcz}{sn}aupd routines
    !! 
    !! -----------------------------------------------------------------
    ldv       = size_mat_loc !! Leading dimension 
    !! allocation
    allocate(v    (n,ncv) ) !! [N,NCV], final set of Arnoldi basis vectors.
    allocate(workl(lworkl)) !! working array
    allocate(workd(3*n)   ) !! Reverse communication 
    allocate(rwork(ncv)   ) 
    allocate(d    (ncv,2) )
    allocate(resid(n)     ) !! If INFO .EQ. 0, a random init is used
    !! global vector to assemble before we can solve the matrix vector
    !! multiplication...
    allocate(vect   (size_mat_gb)) 
    allocate(vect_gb(size_mat_gb)) 
    allocate(sol_loc(size_mat_gb)) 
    allocate(sol_gb (size_mat_gb)) 
    !! -----------------------------------------------------------------

    iiter=1
    !! main loop
    do while(iiter <= maxitr)
      !! complex is alway non-symmetric
      !if(flag_sym) then
      !else
      call pznaupd(ctx_paral%communicator,ido,bmat,size_mat_loc,which,  &
                   n_ev,tol,resid,ncv,v,ldv,iparam,ipntr,workd,workl,   &
                   lworkl,rwork,info)

      iiter=iiter+1
      select case(ido)
        case(-1,1)
          !! %--------------------------------------%
          !! | Perform matrix vector multiplication |
          !! |              y <--- OP*x             |
          !! | workd(ipntr(1)) contains  X          |
          !! | workd(ipntr(2)) will have Y          |
          !! %--------------------------------------%
          !! need to assemble the local vector to the global one before
          !! we solve the matrix vector product
          vect    = 0.d0
          vect_gb = 0.d0
          sol_loc = 0.d0
          sol_gb  = 0.d0
          !! every proc must have a complete line        
          !! *************************************************************
          !! Warning *****************************************************
          !! We assume consecutive FULL lines
          !! *************************************************************
          !! *************************************************************
          vect(Alin(1):Alin(1)+size_mat_loc-1) =                        &
                                   workd(ipntr(1):ipntr(1)+size_mat_loc-1)
          !! *************************************************************
          !! assemble 
          call allreduce_sum(vect,vect_gb,size_mat_gb,                  &
                             ctx_paral%communicator,ctx_err)
      
          !! if we want the lowest one: we have to solve a linear problem
          !! using the global vector as rhs, otherwize we perform the 
          !! matrix vector product
          ! -------------------------------------------------------
          if(flag_lowest_ev) then
            !! Solve y = A^{-1} x, 
            !! i.e. x is the RHS and we solve Ay = x
            !! 
            if(ctx_paral%master) then
              ctx_rhs%Rval    = 0.0
              ctx_rhs%Rval(:) = cmplx(vect_gb,kind=RKIND_MAT)
            end if 
            call process_solve(ctx_paral,ctx_solver,ctx_rhs,.false.,    &
                               ctx_err,o_flag_verb=.false.)
            if(ctx_paral%master) sol_gb = dcmplx(ctx_solver%solution_global)
            !! send to everyone 
            call broadcast(sol_gb,size_mat_gb,0,ctx_paral%communicator,ctx_err)
          ! -------------------------------------------------------
          else
            !! local Matrix vector product must be global size in case
            !! of symmetry
            do k=1,n_nz_loc !! local loop
              !! using line col val format for the matrix...
              sol_loc(Alin(k)) = sol_loc(Alin(k)) + vect_gb(Acol(k)) * Aval(k)
              if(flag_sym .and. Acol(k) > Alin(k)) then
                sol_loc(Acol(k)) = sol_loc(Acol(k)) + vect_gb(Alin(k))*Aval(k)
              end if
            enddo
            !! assemble
            call allreduce_sum(sol_loc,sol_gb,size_mat_gb,              &
                               ctx_paral%communicator,ctx_err)
          end if
          ! -------------------------------------------------------
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=0.0
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=                      &
                                     sol_gb(Alin(1):Alin(1)+size_mat_loc-1)

        case(2)
          !! %--------------------------------------%
          !! | Perform matrix vector multiplication |
          !! |              y <--- M*x              |
          !! | where M is the matrix of the inner   |
          !! | product when we solve Ax = lambda M x|
          !! |                                      |
          !! | workd(ipntr(1)) contains  X          |
          !! | workd(ipntr(2)) will have Y          |
          !! %--------------------------------------%
          !! need to assemble the local vector to the global one
          vect    = 0.d0
          vect_gb = 0.d0
          sol_loc = 0.d0
          sol_gb  = 0.d0
          !! every proc must have a complete line        
          !! *************************************************************
          !! Warning *****************************************************
          !! We assume consecutive FULL lines
          !! *************************************************************
          !! *************************************************************
          vect(Alin(1):Alin(1)+size_mat_loc-1) =                        &
                                   workd(ipntr(1):ipntr(1)+size_mat_loc-1)
          !! *************************************************************
          !! assemble 
          call allreduce_sum(vect,vect_gb,size_mat_gb,                  &
                             ctx_paral%communicator,ctx_err)
          
          !! perform the multiplication and keep what processor info.
          !! we assume it is a diagonal matrix for the inner product, 
          !! so only a multiplication
          select case (trim(adjustl(str_product)))
            case('bulk','vp')
              sol_gb = dcmplx(vect_gb * inner_prodmat)

            case default
              ctx_err%ierr=-1001
              ctx_err%msg ="** ERROR: unrecognize inner product for" // &
                           " PARPACK computation [parpack_eigenvectors]"
              ctx_err%critic=.true.
              call distribute_error(ctx_err,ctx_paral%communicator) 
          end select
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=0.0
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=                      &
                                     sol_gb(Alin(1):Alin(1)+size_mat_loc-1)
        case default
          exit !! we are done whatever it is an error or completion
      end select
      
    enddo
    !! clean
    deallocate(vect   ) 
    deallocate(vect_gb) 
    deallocate(sol_loc) 
    deallocate(sol_gb ) 

    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call parpack_error_info_aupd(info,ido,maxitr,size_mat_gb,n_ev,ncv,  &
                                 which,bmat,iparam,ctx_err)
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! clean solver infos and matrix
    if(flag_lowest_ev) then
      call matrix_clean(ctx_matrix)
      call solver_clean(ctx_solver,ctx_err)
      call rhs_clean(ctx_rhs,ctx_err)
    end if
    
    !! print preprocessing ok
    if(verb_level > 0 .and. ctx_paral%master) then
      call print_parpack_preprocessing(iiter,ctx_err)
    end if

    !! -----------------------------------------------------------------
    !!
    !! Post-processing using {sdcz}{sn}eupd routines
    !! Eigenvectors may be also computed now if rvec = .true.
    !! 
    !! -----------------------------------------------------------------
    flag_vectors = .true. !> compute eigenvectors too, it RVEC.
    !! not symmetric for complex anyway 
    !! if(.not. flag_sym) allocate(workev(3*ncv))
    allocate(workev(3*ncv))
    allocate(selection(ncv))   
    
    if(flag_ritz) then 
      howmny = 'A'
    end if 
    if(flag_schur) then
      howmny    = 'P'
      selection = .true.
    end if
    sigma = dcmplx(0.)
    !! | because we have iparam(1) = 1, there is no shift
    !! |___________________________________________________|
    !! | SIGMA   Complex*16  (INPUT)                       |
    !! | If IPARAM(7) = 3 then SIGMA represents the shift. |
    !! | Not referenced if IPARAM(7) = 1 or 2.             |
    !! |___________________________________________________|

    !! local eigenvalues array, should be (n_ev+1) according to 
    !! documentation
    allocate(evalues_loc(n_ev  ))
    allocate(evector_loc(n,n_ev))
    evalues_loc = 0.
    evector_loc = 0.
    ldz = n !! leading dimension of evector
    !! others input must respect last call to znaupd

    !! complex cannot be symmetric anyway
    !if(flag_sym) then
    !else
    call pzneupd(ctx_paral%communicator,flag_vectors,howmny,selection,  &
                 evalues_loc,evector_loc,ldz,sigma,workev,bmat,         &
                 size_mat_loc,which,n_ev,tol,resid,ncv,v,ldv,iparam,    &
                 ipntr,workd,workl,lworkl,rwork,info)

    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call parpack_error_info_eupd(info,ido,size_mat_gb,n_ev,ncv,which,   &
                                 bmat,iparam,howmny,ctx_err)
    call distribute_error(ctx_err,ctx_paral%communicator) 

    
    !! -----------------------------------------------------------------
    !! if lowest eigenvalues using A^{}-1, we have 1/eigenvalues
    if(flag_lowest_ev) then
      evalues_loc = 1./evalues_loc 
    end if
    !! -----------------------------------------------------------------

    !! check values are acceptable 
    do k=1,n_ev
      if(abs(evalues_loc(k)) > Huge(1.D0) .or.                          &
         ieee_is_nan(abs(evalues_loc(k)))) then
        ctx_err%ierr=-1004
        ctx_err%msg ="** ERROR: PARPACK eigenvalues is infinity or " // &
                     "NaN [parpack_eigenvectors]"
        ctx_err%critic=.true.
        return
      end if
    end do

    !! assemble the eigenvectors
    allocate(evect_loc(size_mat_gb,n_ev))
    allocate(evect_gb (size_mat_gb,n_ev))
    evect_loc = 0.0
    evect_gb  = 0.0
    do k=1,n_ev
      evect_loc(Alin(1):Alin(1)+size_mat_loc-1,k) = evector_loc(:,k)
    end do
    call allreduce_sum(evect_loc,evect_gb,size_mat_gb*n_ev,             &
                       ctx_paral%communicator,ctx_err)
    deallocate(evect_loc)
    deallocate(evector_loc)
    !! -----------------------------------------------------------------

    !! sort array from low to high >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    allocate(index_sort(n_ev))
    call arpack_sort_eigenvalues(abs(evalues_loc),n_ev,index_sort,ctx_err)

    !! output eigenvalues and eigenvectors, sorted
    do k=1,n_ev
      eigenvalues (k)     = evalues_loc(  index_sort(k)) !! cmplx(D(1:nev,1),D(1:nev,2))
      eigenvectors(:,k)   = evect_gb   (:,index_sort(k))
    end do
    
    deallocate(index_sort)
    deallocate(v    )
    deallocate(workl)
    deallocate(workd)
    deallocate(rwork)
    deallocate(d    )
    deallocate(resid)
    deallocate(workev)
    deallocate(selection)
    deallocate(evalues_loc)
    deallocate(evect_gb)
    
    call time(time1)
    if(verb_level > 1 .and. ctx_paral%master) & 
      call print_arpack_eigenvalues   (eigenvalues,n_ev,ctx_err)
    if(verb_level > 0 .and. ctx_paral%master) & 
      call print_parpack_postprocessing(time1-time0,ctx_err)
    
    return
  end subroutine parpack_eigenvectors_double_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute N eigenvectors and eigenvalues using PARPACK. 
  !> it can be: 
  !> - the lowest  eigenvalues,
  !> - the highest eigenvalues
  !
  !> @param[in]    A              : matrix in (line col val) format
  !> @param[in]    size_mat       : matrix size
  !> @param[in]    n_nz           : number of nonzeros
  !> @param[in]    flag_sym       : indicates if symmetric matrix or not
  !> @param[in]    n_ev           : number of eigenvectors/eigenvalues
  !> @param[in]    str_version    : lowest or highest eigenvalues
  !> @param[in]    str_method     : Ritz or Schur 
  !> @param[inout] eigenvalues    : the output eigenvalues, sorted 
  !> @param[inout] eigenvectors   : the output eigenvalues, sorted 
  !> @param[inout] tol_accuracy   : allowed accuracy tolerance (see below)
  !> @param[in]    str_product    : inner produc for <\lambda, v>
  !> @param[out]   mem            : memory 
  !> @param[in]    verblevel      : level of verbose
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parpack_eigenvectors_simple_complex(ctx_paral,Alin,Acol,   &
                                  Aval,size_mat_gb,size_mat_loc,n_nz_gb,&
                                  n_nz_loc,flag_sym,n_ev,str_version,   &
                                  str_method,eigenvalues,eigenvectors,  &
                                  tol_accuracy,str_product,             &
                                  inner_prodmat,mem,verb_level,ctx_err)

    implicit none

    type(t_parallelism)                ,intent(in)   :: ctx_paral
    integer(kind=4)        ,allocatable,intent(in)   :: Alin(:)
    integer(kind=4)        ,allocatable,intent(in)   :: Acol(:)
    complex(kind=4)        ,allocatable,intent(in)   :: Aval(:)
    integer                            ,intent(in)   :: size_mat_gb
    integer                            ,intent(in)   :: size_mat_loc
    integer                            ,intent(in)   :: n_nz_gb,n_nz_loc
    logical                            ,intent(in)   :: flag_sym
    integer                            ,intent(in)   :: n_ev
    character(len=*)                   ,intent(in)   :: str_version
    character(len=*)                   ,intent(in)   :: str_method
    character(len=*)                   ,intent(in)   :: str_product
    real   (kind=4)                    ,intent(in)   :: tol_accuracy
    complex(kind=4)        ,allocatable,intent(inout):: eigenvectors(:,:)
    complex(kind=4)        ,allocatable,intent(inout):: eigenvalues (:)
    real   (kind=8)                    ,intent(in)   :: inner_prodmat(:)
    integer(kind=4)                    ,intent(in)   :: verb_level
    integer(kind=8)                    ,intent(out)  :: mem
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer                     :: k,size_cx,size_int
    real(kind=8)                :: time0,time1
    integer       ,allocatable  :: index_sort(:)
    logical                     :: flag_lowest_ev,flag_ritz,flag_schur
    !! local solver mumps in case of lowest eigenvalues 
    ! ctx_solver
    type(t_solver)              :: ctx_solver
    type(t_matrix)              :: ctx_matrix
    type(t_rhs)                 :: ctx_rhs
    complex(kind=4),allocatable :: sol_loc(:),sol_gb(:),vect(:),vect_gb(:)
    complex(kind=4),allocatable :: evect_loc(:,:),evect_gb(:,:)
    !! arpack aupd
    character(len=2)            :: which
    character(len=1)            :: bmat
    integer                     :: maxitr,ncv,lworkl,ido,iiter
    integer                     :: iparam(11),n,info,ldv,ipntr(14)
    complex(kind=4),allocatable :: v(:,:),workd(:),workl(:),d(:,:)
    complex(kind=4),allocatable :: resid(:),rwork(:)
    real   (kind=8)             :: tol
    !! arpack eupd
    integer                     :: ldz
    character(len=1)            :: howmny
    logical                     :: flag_vectors
    complex(kind=4)             :: sigma
    complex(kind=4),allocatable :: workev(:),evalues_loc(:)
    complex(kind=4),allocatable :: evector_loc(:,:)
    logical        ,allocatable :: selection(:)

    call time(time0)
    ctx_err%ierr=0
    mem         =0
    eigenvalues =0.0
    eigenvectors=0.0

    size_cx = 4*2
    size_int= 4
    n       = size_mat_loc !! n is the local dimension problem <!

    !! -----------------------------------------------------------------
    !! REMARK 
    !! We always want the largest EV, for the lowest, it simply is 
    !! the largest of A^{-1}, such that the matrix-vector product
    !! will be replaced by a linear system resolution.
    !! in which case we end up with 1/eigenvalues
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! REMARK 
    !! Symmetric complex and symmetric double complex do not exist in 
    !! PARPACK
    !! -----------------------------------------------------------------

    !! basic errors 
    if(size_mat_loc <= 0 .or. n_nz_loc <=0 .or.                         &
       size_mat_gb  <= 0 .or. n_nz_gb  <=0) then
      ctx_err%ierr=-1003
      ctx_err%msg ="** ERROR: inconsitent matrix [parpack_eigenvectors]"
      ctx_err%critic=.true.
    end if
    if(size_mat_gb < n_ev) then
      ctx_err%ierr=-1004
      write(ctx_err%msg,'(a,i0,a,i0,a)') "** ERROR: "                // &
           " asking for ", n_ev ," eigenvectors while the size of "  // &
           "the matrix is only ",size_mat_gb," [parpack_eigenvectors]"
      ctx_err%critic=.true.
    end if
    !! -----------------------------------------------------------------
    !! to define the inner product for the eigenvectors
    iparam = 0
    select case(trim(adjustl(str_product)))
      case('identity')
        bmat   = 'I'
        ! ---------------------------------------------------------
        !! RK: Matlab relies on arpack and it seems to be using G
        ! ---------------------------------------------------------
        ! | B = 'I' -> standard eigenvalue problem    A*x = lambda*x     |
        ! | B = 'G' -> generalized eigenvalue problem A*x = lambda*B*x   |
        ! |______________________________________________________________|
        iparam(7) = 1 

      case('bulk','vp')
        !! for the diffusion equation it is \kappa \nabla \cdot (A \nabla) u
        !! that is kappa is to be used, it is a diagonal matrix so quite ok.
        !! we need the evaluation of kappa in each position of the dof.
        bmat   = 'G'
        iparam(7) = 2 !! symmetric positive definite B

      case default
        ctx_err%ierr=-1001
        ctx_err%msg ="** ERROR: unrecognize inner product for PARPACK "//&
                     "computation [parpack_eigenvectors]"
        ctx_err%critic=.true.
    end select
    !! -----------------------------------------------------------------
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! -----------------------------------------------------------------
    flag_lowest_ev=.false.
    select case (trim(adjustl(str_version)))
      case('lowest','LOWEST','Lowest')  
        flag_lowest_ev=.true.
        !! parallelism already given in input, contrary to ARPACK, 
        !! everyone is working here. 
        !! call init_parallelism_selfproc(ctx_paral_self,ctx_err)
        call solver_init_default_options(ctx_paral,flag_sym,            &
                                         ctx_solver,0,ctx_err)
        !! copy matrix in matrix context
        ctx_matrix%dim_matrix_gb  = size_mat_gb
        ctx_matrix%dim_matrix_loc = size_mat_loc
        ctx_matrix%dim_nnz_gb     = n_nz_gb
        ctx_matrix%dim_nnz_loc    = n_nz_loc
        ctx_matrix%flag_block     = .false.
        allocate(ctx_matrix%ilin(n_nz_loc))
        allocate(ctx_matrix%icol(n_nz_loc))
        allocate(ctx_matrix%Aval(n_nz_loc))
        ctx_matrix%ilin = int(Alin  ,kind=IKIND_MAT)
        ctx_matrix%icol = int(Acol  ,kind=IKIND_MAT)
        ctx_matrix%Aval = cmplx(Aval,kind=RKIND_MAT)
        !! proceed with factorization
        call process_factorization(ctx_paral,ctx_solver,ctx_matrix,     &
                                   ctx_err,o_flag_verb=.false.)
        !! prepare dense rhs 
        ctx_rhs%rhs_format='dense'
        allocate(ctx_rhs%n_rhs_in_group(1)) ; ctx_rhs%n_rhs_in_group=1
        ctx_rhs%i_group           = 1
        ctx_rhs%n_group           = 1
        ctx_rhs%n_rhs_component   = 1
        !! only master
        if(ctx_paral%master) then
          allocate(ctx_rhs%csri(2)) !! only one rhs
          ctx_rhs%csri(1) = 1 ; ctx_rhs%csri(2) = ctx_rhs%csri(1) + size_mat_gb
          allocate(ctx_rhs%icol(size_mat_gb))
          allocate(ctx_rhs%Rval(size_mat_gb))
          !! column gives one value per position 
          do k=1,size_mat_gb
            ctx_rhs%icol(k) = k
          end do
        end if

      case('highest','HIGHEST','Highest')
        flag_lowest_ev=.false.

      case default
        ctx_err%ierr=-1002
        ctx_err%msg ="** ERROR: unrecognize eigenvectors version "//&
                     "for PARPACK, it should be 'lowest' or "      //&
                     "'highest' ones  [parpack_eigenvectors]"
        ctx_err%critic=.true.
    end select
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    flag_ritz =.false.
    flag_schur=.false.
    select case (trim(adjustl(str_method)))
      case('RITZ','Ritz','ritz')  
        flag_ritz=.true.
      case('Schur','SCHUR','schur')
        flag_schur=.true.

      case default
        ctx_err%ierr=-1006
        ctx_err%msg ="** ERROR: unrecognize eigenvectors method "  //&
                     "for PARPACK, only RITZ is supported for now "//&
                     " [parpack_eigenvectors]"
        ctx_err%critic=.true.
    end select
    !! -----------------------------------------------------------------
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! -----------------------------------------------------------------
    !! arbitrary maximal number of iterations
    maxitr = 500000
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------    
    which  = 'LM'
      ! |______________________________________________________________|
      ! | 'LA' - compute the NEV largest (algebraic) eigenvalues.      |
      ! | 'SA' - compute the NEV smallest (algebraic) eigenvalues.     |
      ! | 'LM' - compute the NEV largest (in magnitude) eigenvalues.   |
      ! | 'SM' - compute the NEV smallest (in magnitude) eigenvalues.  |
      ! | 'BE' - compute NEV eigenvalues, half from each end of the    |
      ! |        spectrum.  When NEV is odd, compute one more from the |
      ! |        high end than from the low end.                       |
      ! | ****** NON-SYMMETRIC                                         |
      ! | 'LM' -> want the NEV eigenvalues of largest magnitude.       |
      ! | 'SM' -> want the NEV eigenvalues of smallest magnitude.      |
      ! | 'LR' -> want the NEV eigenvalues of largest real part.       |
      ! | 'SR' -> want the NEV eigenvalues of smallest real part.      |
      ! | 'LI' -> want the NEV eigenvalues of largest imaginary part.  |
      ! | 'SI' -> want the NEV eigenvalues of smallest imaginary part. |
      ! |______________________________________________________________|
      !! ---------------------------------------------------------------   

    !! -----------------------------------------------------------------    
    !! tolerance on computation
    tol = dble(tol_accuracy)
    ncv=int(n_ev*3) 
    !! we must have (Ncv-Nev) must be >= 2 and <=N 
    !! check for small problem
    if(ncv >= size_mat_gb) ncv=size_mat_gb
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------    
    !! LWORKL
    !! Symmetric complex does not exist, so we take non-symmetric 
    !! if complex anyway
    !if(flag_sym) then
    !  lworkl =   ncv**2 + 8*ncv
    !else
    lworkl = 3*ncv**2 + 6*ncv 
    !endif
    !! we multiply to be sure
    lworkl = int(lworkl*1.10)
      ! |______________________________________________________________|
      ! | non-symmetric real at least 3*NCV**2 + 6*NCV                 |
      ! |     symmetric real at least   NCV**2 + 8*NCV                 |
      ! | non-symmetric cx   at least 3*NCV**2 + 5*NCV                 |
      ! |______________________________________________________________|
    !! -----------------------------------------------------------------    

    !! ----------------------------------------------------------------- 
    info = 0 !! starting, see detail above
    ido  = 0 !! starting, see detail above
    iparam(1) = 1 !! starting, see detail above
    iparam(3) = maxitr
    !! it depends on the inner product
    !! iparam(7) = 1 !! RK: it appears to be 3 in Matlab...
    !! -----------------------------------------------------------------

    !! estimate memory of allocatable array
    mem = size_cx*n_nz_loc + size_int*n_nz_loc*2 + & !! matrix Aloc
          !! eigenvectors and eigenvalues, tiems 2 for local array
          (size_cx*n_ev + size_cx*n_ev*n)*2      + & 
          size_cx*n*ncv   + & !! v    
          size_cx*lworkl  + & !! workl
          size_cx*3*n     + & !! workd
          size_cx*ncv     + & !! rwork
          size_cx*ncv*2   + & !! d    
          size_cx*n       + & !! resid
          size_int*11 + size_int*14 + & !! iparam and ipntr
          size_cx*3*ncv   + & !! workev
          ncv             + & !! selection
          size_int*n_ev   + & !! index sort
          size_cx*size_mat_gb*4 !! global array for assemble
    !! for each processors <!
    
    !! add system for lowest ev
    if(flag_lowest_ev) then
      mem = mem + RKIND_MAT*2*n_nz_loc + IKIND_MAT*n_nz_loc*2 + & !! matrix
                  RKIND_MAT*2*size_mat_gb                     + & !! solution
                  !! rhs CSR
                  IKIND_MAT*2 + IKIND_MAT*size_mat_gb +                 &
                  RKIND_MAT*2*size_mat_gb
    end if

    if(verb_level > 0 .and. ctx_paral%master) then
      call print_parpack_welcome(n,n_ev,flag_lowest_ev,flag_ritz,       &
                                flag_schur,trim(adjustl(str_product)),  &
                                mem,ctx_err)
    end if
    !! -----------------------------------------------------------------
    !!
    !! Pre-processing using {sdcz}{sn}aupd routines
    !! 
    !! -----------------------------------------------------------------
    ldv       = size_mat_loc !! Leading dimension 
    !! allocation
    allocate(v    (n,ncv) ) !! [N,NCV], final set of Arnoldi basis vectors.
    allocate(workl(lworkl)) !! working array
    allocate(workd(3*n)   ) !! Reverse communication 
    allocate(rwork(ncv)   ) 
    allocate(d    (ncv,2) )
    allocate(resid(n)     ) !! If INFO .EQ. 0, a random init is used
    !! global vector to assemble before we can solve the matrix vector
    !! multiplication...
    allocate(vect   (size_mat_gb)) 
    allocate(vect_gb(size_mat_gb)) 
    allocate(sol_loc(size_mat_gb)) 
    allocate(sol_gb (size_mat_gb)) 
    !! -----------------------------------------------------------------

    iiter=1
    !! main loop
    do while(iiter <= maxitr)
      !! complex is alway non-symmetric
      !if(flag_sym) then
      !else
      call pcnaupd(ctx_paral%communicator,ido,bmat,size_mat_loc,which,  &
                   n_ev,tol,resid,ncv,v,ldv,iparam,ipntr,workd,workl,   &
                   lworkl,rwork,info)

      iiter=iiter+1
      select case(ido)
        case(-1,1)
          !! %--------------------------------------%
          !! | Perform matrix vector multiplication |
          !! |              y <--- OP*x             |
          !! | workd(ipntr(1)) contains  X          |
          !! | workd(ipntr(2)) will have Y          |
          !! %--------------------------------------%
          !! need to assemble the local vector to the global one before
          !! we solve the matrix vector product
          vect    = 0.d0
          vect_gb = 0.d0
          sol_loc = 0.d0
          sol_gb  = 0.d0
          !! every proc must have a complete line        
          !! *************************************************************
          !! Warning *****************************************************
          !! We assume consecutive FULL lines
          !! *************************************************************
          !! *************************************************************
          vect(Alin(1):Alin(1)+size_mat_loc-1) =                        &
                                   workd(ipntr(1):ipntr(1)+size_mat_loc-1)
          !! *************************************************************
          !! assemble 
          call allreduce_sum(vect,vect_gb,size_mat_gb,                  &
                             ctx_paral%communicator,ctx_err)
      
          !! if we want the lowest one: we have to solve a linear problem
          !! using the global vector as rhs, otherwize we perform the 
          !! matrix vector product
          ! -------------------------------------------------------
          if(flag_lowest_ev) then
            !! Solve y = A^{-1} x, 
            !! i.e. x is the RHS and we solve Ay = x
            !! 
            if(ctx_paral%master) then
              ctx_rhs%Rval    = 0.0
              ctx_rhs%Rval(:) = cmplx(vect_gb,kind=RKIND_MAT)
            end if 
            call process_solve(ctx_paral,ctx_solver,ctx_rhs,.false.,    &
                               ctx_err,o_flag_verb=.false.)
            if(ctx_paral%master) sol_gb = cmplx(ctx_solver%solution_global,kind=4)
            !! send to everyone 
            call broadcast(sol_gb,size_mat_gb,0,ctx_paral%communicator,ctx_err)
          ! -------------------------------------------------------
          else
            !! local Matrix vector product must be global size in case
            !! of symmetry
            do k=1,n_nz_loc !! local loop
              !! using line col val format for the matrix...
              sol_loc(Alin(k)) = sol_loc(Alin(k)) + vect_gb(Acol(k)) * Aval(k)
              if(flag_sym .and. Acol(k) > Alin(k)) then
                sol_loc(Acol(k)) = sol_loc(Acol(k)) + vect_gb(Alin(k))*Aval(k)
              end if
            enddo
            !! assemble
            call allreduce_sum(sol_loc,sol_gb,size_mat_gb,              &
                               ctx_paral%communicator,ctx_err)
          end if
          ! -------------------------------------------------------
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=0.0
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=                      &
                                     sol_gb(Alin(1):Alin(1)+size_mat_loc-1)

        case(2)
          !! %--------------------------------------%
          !! | Perform matrix vector multiplication |
          !! |              y <--- M*x              |
          !! | where M is the matrix of the inner   |
          !! | product when we solve Ax = lambda M x|
          !! |                                      |
          !! | workd(ipntr(1)) contains  X          |
          !! | workd(ipntr(2)) will have Y          |
          !! %--------------------------------------%
          !! need to assemble the local vector to the global one
          vect    = 0.d0
          vect_gb = 0.d0
          sol_loc = 0.d0
          sol_gb  = 0.d0
          !! every proc must have a complete line        
          !! *************************************************************
          !! Warning *****************************************************
          !! We assume consecutive FULL lines
          !! *************************************************************
          !! *************************************************************
          vect(Alin(1):Alin(1)+size_mat_loc-1) =                        &
                                   workd(ipntr(1):ipntr(1)+size_mat_loc-1)
          !! *************************************************************
          !! assemble 
          call allreduce_sum(vect,vect_gb,size_mat_gb,                  &
                             ctx_paral%communicator,ctx_err)
          
          !! perform the multiplication and keep what processor info.
          !! we assume it is a diagonal matrix for the inner product, 
          !! so only a multiplication
          select case (trim(adjustl(str_product)))
            case('bulk','vp')
              sol_gb = cmplx(vect_gb * inner_prodmat,kind=4)
            case default
              ctx_err%ierr=-1001
              ctx_err%msg ="** ERROR: unrecognize inner product for" // &
                           " PARPACK computation [parpack_eigenvectors]"
              ctx_err%critic=.true.
              call distribute_error(ctx_err,ctx_paral%communicator) 
          end select
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=0.0
          workd(ipntr(2):ipntr(2)+size_mat_loc-1)=                      &
                                     sol_gb(Alin(1):Alin(1)+size_mat_loc-1)
        case default
          exit !! we are done whatever it is an error or completion
      end select
      
    enddo
    !! clean
    deallocate(vect   ) 
    deallocate(vect_gb) 
    deallocate(sol_loc) 
    deallocate(sol_gb ) 

    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call parpack_error_info_aupd(info,ido,maxitr,size_mat_gb,n_ev,ncv,  &
                                 which,bmat,iparam,ctx_err)
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! clean solver infos and matrix
    if(flag_lowest_ev) then
      call matrix_clean(ctx_matrix)
      call solver_clean(ctx_solver,ctx_err)
      call rhs_clean(ctx_rhs,ctx_err)
    end if
    
    !! print preprocessing ok
    if(verb_level > 0 .and. ctx_paral%master) then
      call print_parpack_preprocessing(iiter,ctx_err)
    end if

    !! -----------------------------------------------------------------
    !!
    !! Post-processing using {sdcz}{sn}eupd routines
    !! Eigenvectors may be also computed now if rvec = .true.
    !! 
    !! -----------------------------------------------------------------
    flag_vectors = .true. !> compute eigenvectors too, it RVEC.
    !! not symmetric for complex anyway 
    !! if(.not. flag_sym) allocate(workev(3*ncv))
    allocate(workev(3*ncv))
    allocate(selection(ncv))   
    
    if(flag_ritz) then 
      howmny = 'A'
    end if 
    if(flag_schur) then
      howmny    = 'P'
      selection = .true.
    end if
    sigma = dcmplx(0.)
    !! | because we have iparam(1) = 1, there is no shift
    !! |___________________________________________________|
    !! | SIGMA   Complex*16  (INPUT)                       |
    !! | If IPARAM(7) = 3 then SIGMA represents the shift. |
    !! | Not referenced if IPARAM(7) = 1 or 2.             |
    !! |___________________________________________________|

    !! local eigenvalues array, should be (n_ev+1) according to 
    !! documentation
    allocate(evalues_loc(n_ev  ))
    allocate(evector_loc(n,n_ev))
    evalues_loc = 0.
    evector_loc = 0.
    ldz = n !! leading dimension of evector
    !! others input must respect last call to znaupd

    !! complex cannot be symmetric anyway
    !if(flag_sym) then
    !else
    call pcneupd(ctx_paral%communicator,flag_vectors,howmny,selection,  &
                 evalues_loc,evector_loc,ldz,sigma,workev,bmat,         &
                 size_mat_loc,which,n_ev,tol,resid,ncv,v,ldv,iparam,    &
                 ipntr,workd,workl,lworkl,rwork,info)

    !! info error code are indicated above >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call parpack_error_info_eupd(info,ido,size_mat_gb,n_ev,ncv,which,   &
                                 bmat,iparam,howmny,ctx_err)
    call distribute_error(ctx_err,ctx_paral%communicator) 

    
    !! -----------------------------------------------------------------
    !! if lowest eigenvalues using A^{}-1, we have 1/eigenvalues
    if(flag_lowest_ev) then
      evalues_loc = 1./evalues_loc 
    end if
    !! -----------------------------------------------------------------

    !! check values are acceptable 
    do k=1,n_ev
      if(abs(evalues_loc(k)) > Huge(1.D0) .or.                          &
         ieee_is_nan(abs(evalues_loc(k)))) then
        ctx_err%ierr=-1004
        ctx_err%msg ="** ERROR: PARPACK eigenvalues is infinity or " // &
                     "NaN [parpack_eigenvectors]"
        ctx_err%critic=.true.
        return
      end if
    end do

    !! assemble the eigenvectors
    allocate(evect_loc(size_mat_gb,n_ev))
    allocate(evect_gb (size_mat_gb,n_ev))
    evect_loc = 0.0
    evect_gb  = 0.0
    do k=1,n_ev
      evect_loc(Alin(1):Alin(1)+size_mat_loc-1,k) = evector_loc(:,k)
    end do
    call allreduce_sum(evect_loc,evect_gb,size_mat_gb*n_ev,             &
                       ctx_paral%communicator,ctx_err)
    deallocate(evect_loc)
    deallocate(evector_loc)
    !! -----------------------------------------------------------------

    !! sort array from low to high >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    allocate(index_sort(n_ev))
    call arpack_sort_eigenvalues(abs(evalues_loc),n_ev,index_sort,ctx_err)

    !! output eigenvalues and eigenvectors, sorted
    do k=1,n_ev
      eigenvalues (k)     = evalues_loc(  index_sort(k)) !! cmplx(D(1:nev,1),D(1:nev,2))
      eigenvectors(:,k)   = evect_gb   (:,index_sort(k))
    end do
    
    deallocate(index_sort)
    deallocate(v    )
    deallocate(workl)
    deallocate(workd)
    deallocate(rwork)
    deallocate(d    )
    deallocate(resid)
    deallocate(workev)
    deallocate(selection)
    deallocate(evalues_loc)
    deallocate(evect_gb)
    
    call time(time1)
    if(verb_level > 1 .and. ctx_paral%master) & 
      call print_arpack_eigenvalues   (eigenvalues,n_ev,ctx_err)
    if(verb_level > 0 .and. ctx_paral%master) & 
      call print_parpack_postprocessing(time1-time0,ctx_err)
    
    return
  end subroutine parpack_eigenvectors_simple_complex

end module m_parpack_eigenvectors

