!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_parpack_missing_library.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> PARPACK is not a mandatory library for the code, therefore, we can 
!> compile everything without the dependency to PARPACK library. 
!> In this case, the specific subroutines are replaced with error
!> message, in case one still wants to use it...
!
!------------------------------------------------------------------------------
module m_parpack_eigenvectors
  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error
  use m_ctx_parallelism,        only: t_parallelism

  implicit none 
  !! ------------------------------------------------------------------- 

  interface parpack_eigenvectors
    module procedure parpack_eigenvectors_double_complex
    module procedure parpack_eigenvectors_simple_complex
  end interface
  
  private
  public  :: parpack_eigenvectors
  
  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> return an error if one wants to use PARPACK while libraries are not 
  !> linked
  !>
  !----------------------------------------------------------------------------
  !> @param[in]    A              : matrix in (line col val) format
  !> @param[in]    size_mat       : matrix size
  !> @param[in]    n_nz           : number of nonzeros
  !> @param[in]    flag_sym       : indicates if symmetric matrix or not
  !> @param[in]    n_ev           : number of eigenvectors/eigenvalues
  !> @param[in]    str_version    : lowest or highest eigenvalues
  !> @param[in]    str_method     : Ritz or Schur 
  !> @param[inout] eigenvalues    : the output eigenvalues, sorted 
  !> @param[inout] eigenvectors   : the output eigenvalues, sorted 
  !> @param[inout] tol_accuracy   : allowed accuracy tolerance (see below)
  !> @param[in]    str_product    : inner produc for <\lambda, v>
  !> @param[out]   mem            : memory 
  !> @param[in]    verblevel      : level of verbose
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine parpack_eigenvectors_double_complex(ctx_paral,Alin,Acol,   &
                                  Aval,size_mat_gb,size_mat_loc,n_nz_gb,&
                                  n_nz_loc,flag_sym,n_ev,str_version,   &
                                  str_method,eigenvalues,eigenvectors,  &
                                  tol_accuracy,str_product,mem,         &
                                  verb_level,ctx_err)
                                 
    implicit none
    
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    integer(kind=4)        ,allocatable,intent(in)   :: Alin(:)
    integer(kind=4)        ,allocatable,intent(in)   :: Acol(:)
    complex(kind=8)        ,allocatable,intent(in)   :: Aval(:)
    integer                            ,intent(in)   :: size_mat_gb
    integer                            ,intent(in)   :: size_mat_loc
    integer                            ,intent(in)   :: n_nz_gb,n_nz_loc
    logical                            ,intent(in)   :: flag_sym
    integer                            ,intent(in)   :: n_ev
    character(len=*)                   ,intent(in)   :: str_version
    character(len=*)                   ,intent(in)   :: str_method
    character(len=*)                   ,intent(in)   :: str_product
    real   (kind=4)                    ,intent(in)   :: tol_accuracy
    complex(kind=8)        ,allocatable,intent(inout):: eigenvectors(:,:)
    complex(kind=8)        ,allocatable,intent(inout):: eigenvalues (:)
    integer(kind=4)                    ,intent(in)   :: verb_level
    integer(kind=8)                    ,intent(out)  :: mem
    type(t_error)                      ,intent(inout):: ctx_err
        
    !! prevent warning compilation
    eigenvectors=0.0
    eigenvalues =0.0
    mem         =0  
    if(allocated(Alin) .or. allocated(Acol) .or. allocated(Aval) .or.   &
       size_mat_gb>0 .or. n_nz_gb>0 .or. flag_sym .or. n_ev > 0  .or.   &
       str_version.eq.'' .or. str_method==''  .or. str_product=='' .or. &
       tol_accuracy > 0 .or. verb_level > 0 .or. ctx_paral%master  .or. &
       size_mat_loc > 0 .or. n_nz_loc > 0) then
       !! only to prevent compilation warning.
    end if    

    ctx_err%ierr=-1000
    ctx_err%msg ="** ERROR: trying to use PARPACK specific routine " //  &
                 "it has not been linked at compilation, you have to"// &
                 " change the make.config with DEPENDENCY_PARPACK=1 "//  &
                 "and give PARPACKDIR [parpack_eigenvectors]"
    ctx_err%critic=.true.
    return
  end subroutine parpack_eigenvectors_double_complex


  subroutine parpack_eigenvectors_simple_complex(ctx_paral,Alin,Acol,   &
                                  Aval,size_mat_gb,size_mat_loc,n_nz_gb,&
                                  n_nz_loc,flag_sym,n_ev,str_version,   &
                                  str_method,eigenvalues,eigenvectors,  &
                                  tol_accuracy,str_product,mem,         &
                                  verb_level,ctx_err)
                                 
    implicit none
    
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    integer(kind=4)        ,allocatable,intent(in)   :: Alin(:)
    integer(kind=4)        ,allocatable,intent(in)   :: Acol(:)
    complex(kind=4)        ,allocatable,intent(in)   :: Aval(:)
    integer                            ,intent(in)   :: size_mat_gb
    integer                            ,intent(in)   :: size_mat_loc
    integer                            ,intent(in)   :: n_nz_gb,n_nz_loc
    logical                            ,intent(in)   :: flag_sym
    integer                            ,intent(in)   :: n_ev
    character(len=*)                   ,intent(in)   :: str_version
    character(len=*)                   ,intent(in)   :: str_method
    character(len=*)                   ,intent(in)   :: str_product
    real   (kind=4)                    ,intent(in)   :: tol_accuracy
    complex(kind=4)        ,allocatable,intent(inout):: eigenvectors(:,:)
    complex(kind=4)        ,allocatable,intent(inout):: eigenvalues (:)
    integer(kind=4)                    ,intent(in)   :: verb_level
    integer(kind=8)                    ,intent(out)  :: mem
    type(t_error)                      ,intent(inout):: ctx_err
        
    !! prevent warning compilation
    eigenvectors=0.0
    eigenvalues =0.0
    mem         =0  
    if(allocated(Alin) .or. allocated(Acol) .or. allocated(Aval) .or.   &
       size_mat_gb>0 .or. n_nz_gb>0 .or. flag_sym .or. n_ev > 0  .or.   &
       str_version.eq.'' .or. str_method==''  .or. str_product=='' .or. &
       tol_accuracy > 0 .or. verb_level > 0 .or. ctx_paral%master  .or. &
       size_mat_loc > 0 .or. n_nz_loc > 0) then
       !! only to prevent compilation warning.
    end if    

    ctx_err%ierr=-1000
    ctx_err%msg ="** ERROR: trying to use PARPACK specific routine " //  &
                 "it has not been linked at compilation, you have to"// &
                 " change the make.config with DEPENDENCY_PARPACK=1 "//  &
                 "and give PARPACKDIR [parpack_eigenvectors]"
    ctx_err%critic=.true.
    return
  end subroutine parpack_eigenvectors_simple_complex
  
end module m_parpack_eigenvectors

