!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_solver_mumps_clean.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to clean MUMPS solver with choices
!>  - MUMPS Complex simple precision
!>  - MUMPS Complex double precision
!
!------------------------------------------------------------------------------
module m_solver_mumps_clean
  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_define_precision,       only: RKIND_MAT
  implicit none 
  !> mumps include: simple of double precision complex
  include 'cmumps_struc.h'
  include 'zmumps_struc.h'
  !! ------------------------------------------------------------------- 

  private
  public  :: mumps_clean
  
  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Clean Mumps solver
  !
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_clean(ctx_mumps_c,ctx_mumps_z,ctx_err)
  
    implicit none
    type(cmumps_struc)       ,intent(inout):: ctx_mumps_c
    type(zmumps_struc)       ,intent(inout):: ctx_mumps_z
    type(t_error)            ,intent(inout):: ctx_err
   
    ctx_err%ierr  = 0

    !! depends on the precision
    select case(RKIND_MAT)

      !! simple precision
      case(4)
        ctx_mumps_c%JOB = -2
        call cmumps(ctx_mumps_c)
        ctx_err%ierr = ctx_mumps_c%INFOG(1)
        ctx_mumps_c%N      = 0
        ctx_mumps_c%NZ_loc = 0

        !! these are pointer -------------------------------------------
        if(associated(ctx_mumps_c%IRN_loc))  then
          deallocate(ctx_mumps_c%IRN_loc,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(ctx_mumps_c%IRN_loc)
        end if 
        if(associated(ctx_mumps_c%JCN_loc)) then
          deallocate(ctx_mumps_c%JCN_loc,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(ctx_mumps_c%JCN_loc)
        end if
        if(associated(ctx_mumps_c%A_loc  )) then
          deallocate(ctx_mumps_c%A_loc  ,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(ctx_mumps_c%A_loc)
        end if
        !! we do not really care for error in deallocation
        ctx_err%ierr = 0
        !! -------------------------------------------------------------

      !! double precision
      case(8)
        ctx_mumps_z%JOB = -2
        call zmumps(ctx_mumps_z)
        ctx_err%ierr = ctx_mumps_z%INFOG(1)
        ctx_mumps_z%N      = 0
        ctx_mumps_z%NZ_loc = 0

        !! these are pointer -------------------------------------------
        if(associated(ctx_mumps_z%IRN_loc))  then
          deallocate(ctx_mumps_z%IRN_loc,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(ctx_mumps_z%IRN_loc)
        end if 
        if(associated(ctx_mumps_z%JCN_loc)) then
          deallocate(ctx_mumps_z%JCN_loc,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(ctx_mumps_z%JCN_loc)
        end if
        if(associated(ctx_mumps_z%A_loc  )) then
          deallocate(ctx_mumps_z%A_loc  ,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(ctx_mumps_z%A_loc)
        end if
        !! we do not really care for error in deallocation
        ctx_err%ierr = 0
        !! -------------------------------------------------------------

      case default
        ctx_err%msg   = "** ERROR: Precision for matrix operations not " // &
                        " recognized (must be 4 or 8) [mumps_clean] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR during MUMPS cleaning [mumps_clean] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)    
    endif
    
    return
  end subroutine mumps_clean

end module m_solver_mumps_clean
