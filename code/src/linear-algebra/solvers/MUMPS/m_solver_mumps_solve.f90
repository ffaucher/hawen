!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_solver_mumps_solve.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to solve linear system with MUMPS with choices:
!>  - MUMPS Complex simple precision
!>  - MUMPS Complex double precision
!
!------------------------------------------------------------------------------
module m_solver_mumps_solve
  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_sum,          only: allreduce_sum
  use m_define_precision,       only: RKIND_MAT
  use m_ctx_rhs,                only: t_rhs
  use m_print_mumps,            only: print_info_mumps_solve
  use m_time,                   only: time
  implicit none 
  !> mumps include: simple of double precision complex
  include 'cmumps_struc.h'
  include 'zmumps_struc.h'
  !! ------------------------------------------------------------------- 
  
  interface mumps_solve_precision
     module procedure mumps_solve_double_complex
     module procedure mumps_solve_simple_complex
  end interface
   
  private
  public  :: mumps_solve
  private :: mumps_solve_precision  
  
  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> solve using mumps
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_mumps_c    : context solver simple
  !> @param[inout] ctx_mumps_z    : context solver double
  !> @param[inout] ctx_rhs        : context R.H.S
  !> @param[inout] solution       : the monoproc global solution
  !> @param[inout] flag_adjoint   : flag for adjoint problem
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_verb    : verbosity level
  !----------------------------------------------------------------------------
  subroutine mumps_solve(ctx_paral,ctx_mumps_c,ctx_mumps_z,ctx_rhs,     &
                         flag_adjoint,solution,mem_monoproc_array,      &
                         ctx_err,o_flag_verb)
  
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(cmumps_struc)                 ,intent(inout):: ctx_mumps_c
    type(zmumps_struc)                 ,intent(inout):: ctx_mumps_z
    type(t_rhs)                        ,intent(in)   :: ctx_rhs
    complex(kind=RKIND_MAT),allocatable,intent(inout):: solution(:)
    logical                            ,intent(in)   :: flag_adjoint
    integer(kind=8)                    ,intent(out)  :: mem_monoproc_array
    type(t_error)                      ,intent(inout):: ctx_err
    logical                ,optional   ,intent(in)   :: o_flag_verb
    !! local
    integer         :: nrhs
    integer(kind=8) :: mem_solve
    real   (kind=8) :: time0,time1
    logical         :: flag_verb
    
    ctx_err%ierr  = 0
    
    call time(time0)
    
    flag_verb = .true.
    if(present(o_flag_verb)) flag_verb = o_flag_verb
    nrhs = ctx_rhs%n_rhs_in_group(ctx_rhs%i_group)
    !! depends on the precision
    select case(RKIND_MAT)
      !! simple precision
      case(4)
        call mumps_solve_simple_complex(ctx_paral,ctx_mumps_c,ctx_rhs, &
                                        flag_adjoint,mem_solve,        &
                                        mem_monoproc_array,solution,ctx_err)
      !! double precision
      case(8)
        call mumps_solve_double_complex(ctx_paral,ctx_mumps_z,ctx_rhs, &
                                        flag_adjoint,mem_solve,        &
                                        mem_monoproc_array,solution,ctx_err)
      
      case default
        ctx_err%msg   = "** ERROR: Precision for matrix operations not " // &
                        " recognized (must be 4 or 8) [mumps_solve] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    !! error return, possibility for non-shared error 
    call distribute_error(ctx_err,ctx_paral%communicator)

    !! print informations on screen
    call time(time1)
    if(flag_verb) then
      call print_info_mumps_solve(ctx_paral,mem_solve,time1-time0,      &
                                  flag_adjoint,nrhs,mem_monoproc_array, &
                                  ctx_err)
    end if

    return
  end subroutine mumps_solve
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options for simple complex
  !
  !> @param[inout] ctx_mumps      : context solver
  !> @param[inout] ctx_rhs        : context rhs
  !> @param[inout] flag_adjoint   : flag adjoint problem
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_solve_simple_complex(ctx_paral,ctx_mumps,ctx_rhs,   &
                                        flag_adjoint,mem_solve,        &
                                        mem_monoproc_array,solution,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(cmumps_struc)                 ,intent(inout):: ctx_mumps
    type(t_rhs)                        ,intent(in)   :: ctx_rhs
    logical                            ,intent(in)   :: flag_adjoint
    integer(kind=8)                    ,intent(out)  :: mem_solve
    integer(kind=8)                    ,intent(out)  :: mem_monoproc_array
    complex(kind=RKIND_MAT),allocatable,intent(inout):: solution(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer(kind=8)  :: n_unknown
    integer          :: nrhs,istat

    ctx_err%ierr  = 0

    nrhs = ctx_rhs%n_rhs_in_group(ctx_rhs%i_group)
    !! Transfer to Mumps architecture
    ctx_mumps%NRHS     = int(nrhs        ,kind=4)
    ctx_mumps%LRHS     = int(ctx_mumps%N ,kind=4)
    !! 'blocking' size to full NRHS size
    ctx_mumps%ICNTL(27) = nrhs

    !! the solution is dense and contained in %RHS
    ctx_mumps%ICNTL(21) = 0
    n_unknown = int8(ctx_mumps%N * nrhs)
    !! simple complex memory
    mem_monoproc_array = int8(n_unknown*2*4)
    
    if(ctx_paral%master) then
      allocate(ctx_mumps%RHS(n_unknown))
    end if
 
    !! -----------------------------------------------------------------
    !! the rhs may be sparse or dense 
    !! in any case, only the master contains the information on the RHS
    !! -----------------------------------------------------------------
    select case (trim(adjustl(ctx_rhs%rhs_format)))
      !! sparse format rhs ---------------------------------------------
      case('sparse')
        !! we use CSR format for RHS (yes = 1, no = 0)
        ctx_mumps%ICNTL(20) = 1
        if(ctx_paral%master) then
          ctx_mumps%NZ_RHS = int(ctx_rhs%csri(nrhs+1)-1,kind=4)
          allocate(ctx_mumps%IRHS_PTR   (nrhs+1))
          allocate(ctx_mumps%IRHS_SPARSE(ctx_mumps%NZ_RHS))
          allocate(ctx_mumps%RHS_SPARSE (ctx_mumps%NZ_RHS))
          ctx_mumps%IRHS_PTR    = int  (ctx_rhs%csri,kind=4)
          ctx_mumps%IRHS_SPARSE = int  (ctx_rhs%icol,kind=4)
          ctx_mumps%RHS_SPARSE  = cmplx(ctx_rhs%Rval,kind=4)
          if(flag_adjoint) then
            ctx_mumps%RHS_SPARSE = conjg(ctx_mumps%RHS_SPARSE)
          end if
        end if
        !! -------------------------------------------------------------

      !! dense format rhs ----------------------------------------------
      case('dense')
        !! we use Dense format for RHS (no = 1, yes = 0)
        ctx_mumps%ICNTL(20) = 0
        if(ctx_paral%master) then
          ctx_mumps%RHS  = cmplx(ctx_rhs%Rval,kind=4)
          if(flag_adjoint) then
            ctx_mumps%RHS= cmplx(conjg(ctx_rhs%Rval),kind=4)
          end if
        end if
        !! -------------------------------------------------------------

      case default
        ctx_err%msg   = "** ERROR: RHS format unrecognized for MUMPS " // &
                        " (should be sparse or dense) [mumps_solve] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    if(flag_adjoint) then !! Consider A (=1) or A^T (!=1)
      ctx_mumps%ICNTL(9)  = 10
    else
      ctx_mumps%ICNTL(9)  = 1
    endif
      
    !! Set MUMPS step to 'solve'
    ctx_mumps%JOB = 3
    call cmumps(ctx_mumps)

    if(flag_adjoint) then
      if(ctx_paral%master) ctx_mumps%RHS = conjg(ctx_mumps%RHS)
    endif

    ctx_err%ierr=ctx_mumps%INFOG(1)
    mem_solve   =int8(ctx_mumps%INFOG(31)*1D6)

    !! save solution
    if(ctx_paral%master) then
      allocate(solution(n_unknown))
      solution = cmplx(ctx_mumps%RHS,kind=RKIND_MAT)
      deallocate(ctx_mumps%RHS)
    endif

    !! clear RHS mumps information -------------------------------------
    if(associated(ctx_mumps%RHS        )) then
      deallocate(ctx_mumps%RHS        ,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%RHS)
    end if
    if(associated(ctx_mumps%RHS_SPARSE )) then 
      deallocate(ctx_mumps%RHS_SPARSE ,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%RHS_SPARSE)
    end if
    if(associated(ctx_mumps%IRHS_PTR   )) then 
      deallocate(ctx_mumps%IRHS_PTR   ,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%IRHS_PTR)
    end if
    if(associated(ctx_mumps%IRHS_SPARSE)) then 
      deallocate(ctx_mumps%IRHS_SPARSE,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%IRHS_SPARSE)
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine mumps_solve_simple_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options for simple complex
  !
  !> @param[inout] ctx_mumps      : context solver
  !> @param[inout] ctx_rhs        : context rhs
  !> @param[inout] flag_adjoint   : flag adjoint problem
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_solve_double_complex(ctx_paral,ctx_mumps,ctx_rhs,   &
                                        flag_adjoint,mem_solve,        &
                                        mem_monoproc_array,solution,ctx_err)
    implicit none
    type(t_parallelism)                ,intent(in)   :: ctx_paral
    type(zmumps_struc)                 ,intent(inout):: ctx_mumps
    type(t_rhs)                        ,intent(in)   :: ctx_rhs
    logical                            ,intent(in)   :: flag_adjoint
    integer(kind=8)                    ,intent(out)  :: mem_solve
    integer(kind=8)                    ,intent(out)  :: mem_monoproc_array
    complex(kind=RKIND_MAT),allocatable,intent(inout):: solution(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! local
    integer(kind=8)  :: n_unknown
    integer          :: nrhs,istat

    ctx_err%ierr  = 0

    nrhs = ctx_rhs%n_rhs_in_group(ctx_rhs%i_group)
    !! Transfer to Mumps architecture
    ctx_mumps%NRHS     = int(nrhs       ,kind=4)
    ctx_mumps%LRHS     = int(ctx_mumps%N,kind=4)
    !! 'blocking' size to full NRHS size
    ctx_mumps%ICNTL(27) = nrhs

    !! the solution is dense and contained in %RHS
    ctx_mumps%ICNTL(21) = 0
    n_unknown = int8(ctx_mumps%N * nrhs)
    !! double complex memory
    mem_monoproc_array = int8(n_unknown*2*8)

    if(ctx_paral%master) then
      allocate(ctx_mumps%RHS(n_unknown))
    end if
 
    !! -----------------------------------------------------------------
    !! the rhs may be sparse or dense 
    !! in any case, only the master contains the information on the RHS
    !! -----------------------------------------------------------------
    select case (trim(adjustl(ctx_rhs%rhs_format)))
      !! sparse format rhs ---------------------------------------------
      case('sparse')
        !! we use CSR format for RHS (yes = 1, no = 0)
        ctx_mumps%ICNTL(20) = 1
        if(ctx_paral%master) then
          ctx_mumps%NZ_RHS = int(ctx_rhs%csri(nrhs+1)-1,kind=4)
          allocate(ctx_mumps%IRHS_PTR (nrhs+1))
          allocate(ctx_mumps%IRHS_SPARSE(ctx_mumps%NZ_RHS))
          allocate(ctx_mumps%RHS_SPARSE (ctx_mumps%NZ_RHS))
          ctx_mumps%IRHS_PTR    = int  (ctx_rhs%csri,kind=4)
          ctx_mumps%IRHS_SPARSE = int  (ctx_rhs%icol,kind=4)
          ctx_mumps%RHS_SPARSE  = cmplx(ctx_rhs%Rval,kind=8)
          if(flag_adjoint) then
            ctx_mumps%RHS_SPARSE = conjg(ctx_mumps%RHS_SPARSE)
          end if
        end if
        !! -------------------------------------------------------------
        
      !! dense format rhs ----------------------------------------------
      case('dense')
        !! we use Dense format for RHS (yes = 1, no = 0)
        ctx_mumps%ICNTL(20) = 0
        if(ctx_paral%master) then
          ctx_mumps%RHS   = dcmplx(ctx_rhs%Rval)
          if(flag_adjoint) then
            ctx_mumps%RHS = dcmplx(conjg(ctx_rhs%Rval))
          end if
        end if
        !! -------------------------------------------------------------

      case default
        ctx_err%msg   = "** ERROR: RHS format unrecognized for MUMPS " // &
                        " (should be sparse or dense) [mumps_solve] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    if(flag_adjoint) then !! Consider A (=1) or A^T (!=1)
      ctx_mumps%ICNTL(9)  = 10
    else
      ctx_mumps%ICNTL(9)  = 1
    endif
      
    !! Set MUMPS step to 'solve'
    ctx_mumps%JOB = 3
    call zmumps(ctx_mumps)

    if(flag_adjoint) then
      if(ctx_paral%master) ctx_mumps%RHS = dcmplx(conjg(ctx_mumps%RHS))
    endif

    ctx_err%ierr=ctx_mumps%INFOG(1)
    mem_solve   =int8(ctx_mumps%INFOG(31)*1D6)
    
    !! save solution
    if(ctx_paral%master) then
      allocate(solution(n_unknown))
      solution = cmplx(ctx_mumps%RHS,kind=RKIND_MAT)
    endif

    !! clear RHS mumps information -------------------------------------
    if(associated(ctx_mumps%RHS        )) then
      deallocate(ctx_mumps%RHS        ,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%RHS)
    end if
    if(associated(ctx_mumps%RHS_SPARSE )) then 
      deallocate(ctx_mumps%RHS_SPARSE ,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%RHS_SPARSE)
    end if
    if(associated(ctx_mumps%IRHS_PTR   )) then 
      deallocate(ctx_mumps%IRHS_PTR   ,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%IRHS_PTR)
    end if
    if(associated(ctx_mumps%IRHS_SPARSE)) then 
      deallocate(ctx_mumps%IRHS_SPARSE,stat=istat)
      if(istat .ne. 0) nullify(ctx_mumps%IRHS_SPARSE)
    end if
    !! -----------------------------------------------------------------


    return
  end subroutine mumps_solve_double_complex

end module m_solver_mumps_solve

