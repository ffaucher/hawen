!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_solver_mumps_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to perform operation for solver MUMPS with choices:
!>  - MUMPS Complex simple precision
!>  - MUMPS Complex double precision
!
!------------------------------------------------------------------------------
module m_solver_mumps_init
  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_define_precision,       only: RKIND_MAT
  use m_read_parameter,         only: read_parameter
  use m_print_mumps,            only: print_info_mumps_options
  implicit none 
  !> mumps include: simple of double precision complex
  include 'cmumps_struc.h'
  include 'zmumps_struc.h'
  !! ------------------------------------------------------------------- 
  
  interface mumps_init_options_precision
     module procedure mumps_init_options_double_complex
     module procedure mumps_init_options_simple_complex
  end interface
   
  private
  public  :: mumps_init_options, mumps_init_default_options
  public  :: mumps_get_lowrank_option, mumps_update_lowrank_option
  private :: mumps_init_options_precision  
  
  contains 


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Get Mumps low rank option, from values
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_get_lowrank_option(ctx_mumps_c,ctx_mumps_z,lowrank,  &
                                      lowrank_tol,ctx_err)
  
    implicit none
    
    type(cmumps_struc)       ,intent(inout):: ctx_mumps_c
    type(zmumps_struc)       ,intent(inout):: ctx_mumps_z
    integer                  ,intent(out)  :: lowrank
    real                     ,intent(out)  :: lowrank_tol
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    
    ctx_err%ierr  = 0

    !! depends on the precision
    select case(RKIND_MAT)
      !! simple precision
      case(4)
        lowrank     = ctx_mumps_c%ICNTL(35)
        lowrank_tol = ctx_mumps_c%CNTL(7)  

      !! double precision
      case(8)
        lowrank     = ctx_mumps_z%ICNTL(35)
        lowrank_tol = real(ctx_mumps_z%CNTL(7))
    
      case default
        ctx_err%msg   = "** ERROR: Precision for matrix operations not " // &
                        " recognized (must be 4 or 8) [mumps_lowrank] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine mumps_get_lowrank_option

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps low rank option, from values
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_update_lowrank_option(ctx_mumps_c,ctx_mumps_z,       &
                                         lowrank,lowrank_tol,ctx_err)
  
    implicit none
    
    type(cmumps_struc)       ,intent(inout):: ctx_mumps_c
    type(zmumps_struc)       ,intent(inout):: ctx_mumps_z
    integer                  ,intent(in)   :: lowrank
    real                     ,intent(in)   :: lowrank_tol
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    
    ctx_err%ierr  = 0

    !! depends on the precision
    select case(RKIND_MAT)
      !! simple precision
      case(4)
        ctx_mumps_c%ICNTL(35) = lowrank
        ctx_mumps_c%CNTL(7)   = lowrank_tol

      !! double precision
      case(8)
        ctx_mumps_z%ICNTL(35) = lowrank
        ctx_mumps_z%CNTL(7)   = dble(lowrank_tol)
    
      case default
        ctx_err%msg   = "** ERROR: Precision for matrix operations not " // &
                        " recognized (must be 4 or 8) [mumps_lowrank] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine mumps_update_lowrank_option
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] parameter_file : parameter file where to read options
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_init_options(ctx_paral,ctx_mumps_c,ctx_mumps_z,      &
                                parameter_file,symmetric_matrix,        &
                                flag_block,flag_force_analysis,ctx_err)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(cmumps_struc)       ,intent(inout):: ctx_mumps_c
    type(zmumps_struc)       ,intent(inout):: ctx_mumps_z
    character(len=*)         ,intent(in)   :: parameter_file
    logical                  ,intent(in)   :: symmetric_matrix
    logical                  ,intent(out)  :: flag_block
    logical                  ,intent(out)  :: flag_force_analysis
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer            :: itemp(512),nval,analysis,part_par,part_seq
    integer            :: lowrank,lowrank_var,advanced,blk_structure
    integer            :: mem_increase_pct,verbose,multithread_tree
    integer            :: root_parallel,cb_compress,pivot_relax
    integer            :: mumps_mpi_komp,mumps_mixed_prec
    integer            :: mumps_ngpu_per_mpi,mumps_gpu_mpi_tuning
    integer(kind=8)    :: mem_limit
    real               :: lowrank_tol,tol_pivot
    real               :: rtemp(512)
    character(len=512) :: mumps_version
    
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! get possible mumps options from parameter files, they will 
    !! be sent to the appropriat context (c or z) afterward
    !! -----------------------------------------------------------------
    !! Ordering 0=AUTO, 1=SEQUENTIAL, 2=PARALLEL
    call read_parameter(parameter_file,'mumps_analysis'       ,0,itemp,nval)
    analysis=itemp(1)
    !! 0=AUTO, 1=PT_SCOTCH,  2=PARMETIS
    call read_parameter(parameter_file,'mumps_partitioner_par',0,itemp,nval)
    part_par=itemp(1)
    !! 0=AMD, 1=PIVOT, 2=AMF, 3=SCOTCH, 4=PORD, 5=METIS, 6=MINDEG, 7=AUTO
    call read_parameter(parameter_file,'mumps_partitioner_seq',4,itemp,nval)
    part_seq=itemp(1)
    !! verbose mumps
    call read_parameter(parameter_file,'mumps_verbose'       , 0,itemp,nval)
    verbose=itemp(1)

    !! force analysis step for each case, it can be necessary for problem
    !! with modes as it can change the structure of the matrix. 
    flag_force_analysis=.false.
    call read_parameter(parameter_file,'mumps_force_analysis', 0,itemp,nval)
    if(itemp(1) > 0) flag_force_analysis=.true.


    !! -----------------------------------------------------------------
    !! Block low rank approximation for MUMPS version >= 5.1
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'mumps_lowrank'    ,0,itemp,nval)
    call read_parameter(parameter_file,'mumps_lowrank_tol',0.,rtemp,nval)
    lowrank    =itemp(1)
    lowrank_tol=rtemp(1)
    call read_parameter(parameter_file,'mumps_lowrank_variant',0,itemp,nval)
    lowrank_var=itemp(1)
    !! increase estimate memory
    call read_parameter(parameter_file,'mumps_memory_increase_percent',30, &
                        itemp,nval)
    mem_increase_pct=itemp(1)

    !! -----------------------------------------------------------------
    !! Option to deactivate ScaLAPACK, actually it can be more efficient
    !! in particular when we have the block low rank.
    !!   0: parallel factorization of the root node (default), using ScaLAPACK
    !!   1: sequential, avoiding ScaLAPACK
    !!  -1: force splitting of the root node even if sequential.
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'mumps_root_parallel',0,itemp,nval)
    root_parallel=itemp(1)
    if(root_parallel > 1 .or. root_parallel < -1) root_parallel = 0

    !! -----------------------------------------------------------------
    !! for MUMPS version >= 5.2, consortium
    !! -----------------------------------------------------------------
    !! Memory limit it is given in MegaBytes
    call read_parameter(parameter_file,'mumps_memory_limit',-1.,rtemp,nval)
    mem_limit=int8(rtemp(1))
    !! ** KEEP(371) : advanced experimental optimizations
    call read_parameter(parameter_file,'mumps_advanced_experimental',0,itemp,nval)
    advanced=itemp(1)
    !! if advanced=1, KEEP(370) should be forced to 1 as well
    
    !! -----------------------------------------------------------------
    !! for MUMPS version >= 5.3, consortium
    !! activation of the multithread at tree level
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'mumps_multithread_tree',0,itemp,nval)
    multithread_tree=itemp(1)
    if(multithread_tree > 1 .or. multithread_tree < 0) multithread_tree = 0
    !! can only be with multithread
    if(ctx_paral%nthread <= 1) multithread_tree = 0
    
    !! -----------------------------------------------------------------
    !! giving the block structure using NBLK, BLKPTR,, BLKVAR
    !! NBLK   = number of blocks = number of faces in HDG
    !! BLKPTR = CSR with dof per face
    !! BLKVAR = identity, so we do not have to give it.
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'mumps_block_structure',0,itemp,nval)
    blk_structure=itemp(1)
    if(blk_structure > 1 .or. blk_structure < 0) blk_structure = 0
    if(blk_structure == 1) then
      flag_block = .true.
    else
      flag_block = .false.
    end if
    
    !! -----------------------------------------------------------------
    !!
    !! IT IS UNCLEAR IF THE FOLLOWINGS ARE ONLY USED FOR STATISTICS
    !! OR IF THEY WOULD REALLY AFFECT THE BLOCK LOW RANK FACTORIZATION ?
    !!
    !! -----------------------------------------------------------------
    !! ** ICNTL(37) is the contribution blocks are compressed (1) or not (0)
    !!   if activated, it should save memory at the cost of more operations
    call read_parameter(parameter_file,'mumps_cb_compress',0,itemp,nval)
    cb_compress=itemp(1)
    if(cb_compress > 1 .or. cb_compress < 0) cb_compress = 0
    !! ** ICNTL(38) estimated compression rate for the LU factors.
    !! for Mumps, the value is between 0 (full) and 1000 (no compression)
    !! here we use percentage and multiply by 10.
    !! from Mumps user guide:
    !! "ICNTL(38)/10 is a percentage representing the typical compression 
    !!  of the compressed factors factor matrices in BLR fronts with
    !!  default value: 333."
    ! call read_parameter(parameter_file,                                 &
    !                    'mumps_factor_lu_compress_percent_rate',33,itemp,nval)
    ! lu_compress_rate=itemp(1)*10 !! scale percent to 1000
    !! ** ICNTL(39) estimated compression rate of Contribution Blocks.
    !! for Mumps, the value is between 0 (full) and 1000 (no compression)
    !! here we use percentage and multiply by 10.
    ! call read_parameter(parameter_file,                                 &
    !                    'mumps_factor_cb_compress_percent_rate',20,itemp,nval)
    ! cb_compress_rate=itemp(1)*10 !! scale percent to 1000
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! cntl(1) = 0.0 helps for the low-rank compression, to reduce
    !! the memory and to save time. 
    !! HOWEVER, it should only be used where there is NO delayed pivots
    !! Therefore, one should first check that there is no delayed pivots
    !! before using this option.
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'mumps_crit_pivoting',-1.0,rtemp,nval)
    tol_pivot = rtemp(1)
    !! -----------------------------------------------------------------
    !! Relaxed pivoting (KEEP(268)):
    !! to enable more BLAS-3 operations on matrices for which partial threshold
    !! pivoting has been requested (SYM=0,2 and CNTL(1)6 = 0.0).
    !!     -2 : relaxed pivoting is applied only on large enough matrices
    !!      0 : feature not activated
    !!      1 : relaxed pivoting is applied to all frontal matrices
    !!     77 : automatic setting, feature is not activated except when GPUs
    call read_parameter(parameter_file,'mumps_relaxed_pivoting',0,itemp,nval)
    pivot_relax = itemp(1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! MPI to k OpenMP:
    !! This feature enables MUMPS to use some MPI processes, by block of 
    !! size ICNTL(17), as threads. 
    !! WARNING: It is limited by the number of core on one Node !!!
    !! ≤ 1 : feature not activated
    !! ≥ 2 : value of the MPI to k OMP reuse factor. 
    !!       The number of MPI processes will be divided by this
    !!       factor.
    !! Default value: 0 (functionality not used)
    call read_parameter(parameter_file,'mumps_mpi_to_komp',0,itemp,nval)
    mumps_mpi_komp = itemp(1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! BLR mixed precision: 
    !! ICNTL(40) activates mixed precision Block Low-Rank feature
    !!
    !!  0 : no mixed precision BLR
    !!  1 : mixed precision BLR
    !! Default value: 0 (no mixed precision BLR)
    !!
    call read_parameter(parameter_file,'mumps_mixed_precision',0,itemp,nval)
    mumps_mixed_prec = itemp(1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! GPU activations
    !! KEEP(417) number of GPUs per MPI process
    !!
    !! 0     : do not use GPU acceleration
    !! k > 0 : use k GPUs per MPI process
    !! Default value: 0
    !!
    call read_parameter(parameter_file,'mumps_ngpu_per_mpi',0,itemp,nval)
    mumps_ngpu_per_mpi = itemp(1)
    if(mumps_ngpu_per_mpi < 0) mumps_ngpu_per_mpi = 0
    !! -----------------------------------------------------------------
    
    !! KEEP(416) Integer defining the set of GPU devices to be used
    !! should be set to the default value, 0 ?
    !!
    !! KEEP(418) GPU arithmetic intensity threshold
    !!
    !! KEEP(422) Memory pinning strategy
    !!
    !! KEEP(66)  Tuning for multi-MPI processes
    !! 0 : no tuning
    !! 1 : force internal settings to increase granularity of tasks 
    !!     involved when processing a frontal matrix distributed on 
    !!     MPI processes and to adapt scheduling. 
    !!     This setting will also increase buffer sizes.
    call read_parameter(parameter_file,'mumps_gpu_mpi_tuning',0,itemp,nval)
    mumps_gpu_mpi_tuning = itemp(1)
    if(mumps_gpu_mpi_tuning > 1 .or. mumps_gpu_mpi_tuning < 0)          &
      mumps_gpu_mpi_tuning = 0
    !! -----------------------------------------------------------------

    !! depends on the precision
    select case(RKIND_MAT)
      !! simple precision
      case(4)
        call mumps_init_options_precision(ctx_paral,ctx_mumps_c,analysis, &
                                part_par,part_seq,lowrank,lowrank_tol,    &
                                lowrank_var,advanced,mem_increase_pct,    &
                                mem_limit,verbose,symmetric_matrix,       &
                                multithread_tree,blk_structure,           &
                                root_parallel,cb_compress,tol_pivot,      &
                                pivot_relax,mumps_mpi_komp,               &
                                mumps_mixed_prec,mumps_ngpu_per_mpi,      &
                                mumps_gpu_mpi_tuning,ctx_err)
        mumps_version = ctx_mumps_c%version_number

      !! double precision
      case(8)
        call mumps_init_options_precision(ctx_paral,ctx_mumps_z,analysis, &
                                part_par,part_seq,lowrank,lowrank_tol,    &
                                lowrank_var,advanced,mem_increase_pct,    &
                                mem_limit,verbose,symmetric_matrix,       &
                                multithread_tree,blk_structure,           &
                                root_parallel,cb_compress,tol_pivot,      &
                                pivot_relax,mumps_mpi_komp,               &
                                mumps_mixed_prec,mumps_ngpu_per_mpi,      &
                                mumps_gpu_mpi_tuning,ctx_err)      
        mumps_version = ctx_mumps_z%version_number

      case default
        ctx_err%msg   = "** ERROR: Precision for matrix operations not " // &
                        " recognized (must be 4 or 8) [mumps_init_options] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    !! ctx_mumps_c%ICNTL(1) = 6  ! print diagnostic infos.
    !! ctx_mumps_z%ICNTL(1) = 6
    !! ctx_mumps_c%write_problem = "matrix_dump.txt"
    !! ctx_mumps_z%write_problem = "matrix_dump.txt"
    
    !!  ----------------------------------------------------------------
    !!  error analysis is NOT compatible with multiple-RHS
    !!
    !!  it works now.   Nor with Distributed Matrix. Therefore we first need 
    !!  it works now.   to force ctx_mumps%ICNTL(18) = 0
    !!  it works now.   and use, in the matrix arrays [ASSUMING WE USE 1 MPI !!]
    !!  it works now.   ctx_mumps%NNZ=ctx_mumps%NZ_loc
    !!  it works now.   ctx_mumps%IRN=ctx_mumps%IRN_LOC
    !!  it works now.   ctx_mumps%JCN=ctx_mumps%JCN_LOC
    !!  it works now.   ctx_mumps%A  =ctx_mumps%A_LOC
    !! xxxxxxxxxx
    !!  To make it work with distributed matrix: 
    !!     need to comment the deallocation of the matrix that 
    !!     is done once the factorization is performed: this is
    !!     in file m_solver_mumps_facto_vX.X.f90
    !!
    !! xxxxxxxxxx
    !! ------------------------------
    !! 0: none; 1: most complete and expensive; 2: main ones only
    !! ctx_mumps_c%ICNTL(11) = 1 !! need one rhs too.
    !! ctx_mumps_z%ICNTL(11) = 1 !! need one rhs too.
    !! iterative refinement: 
    !! ctx_mumps_c%ICNTL(10) =-1000  !! minus force iterations
    !! ctx_mumps_c%CNTL(2)   = 1e-30 !! tolerance for refinement
    !! -----------------------------------------------------------------

    !! print informations on screen
    call print_info_mumps_options(ctx_paral,analysis,part_par,part_seq, &
                                  lowrank,lowrank_tol,lowrank_var,      &
                                  advanced,mem_increase_pct,mem_limit,  &
                                  symmetric_matrix,mumps_version,       &
                                  multithread_tree,blk_structure,       &
                                  root_parallel,cb_compress,tol_pivot,  &
                                  pivot_relax,mumps_mpi_komp,           &
                                  mumps_mixed_prec,mumps_ngpu_per_mpi,  &
                                  mumps_gpu_mpi_tuning,ctx_err)
    
    return
  end subroutine mumps_init_options
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options using default values instead
  !> of looking into the parameter files 
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_init_default_options(ctx_paral,ctx_mumps_c,          &
                                        ctx_mumps_z,symmetric_matrix,   &
                                        verb_level,flag_block,          &
                                        flag_force_analysis,ctx_err)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(cmumps_struc)       ,intent(inout):: ctx_mumps_c
    type(zmumps_struc)       ,intent(inout):: ctx_mumps_z
    logical                  ,intent(in)   :: symmetric_matrix
    integer                  ,intent(in)   :: verb_level
    logical                  ,intent(out)  :: flag_block
    logical                  ,intent(out)  :: flag_force_analysis
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer            :: analysis,part_par,part_seq,multithread_tree
    integer            :: lowrank,lowrank_var,advanced,root_parallel
    integer            :: mem_increase_pct,verbose,blk_structure
    integer            :: cb_compress,pivot_relax,mumps_mpi_komp
    integer            :: mumps_mixed_prec,mumps_ngpu_per_mpi
    integer            :: mumps_gpu_mpi_tuning
    integer(kind=8)    :: mem_limit
    real               :: lowrank_tol,tol_pivot
    character(len=512) :: mumps_version
    
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! get possible mumps options from parameter files, they will 
    !! be sent to the appropriat context (c or z) afterward
    !! -----------------------------------------------------------------
    
    !! DEFAULT values here, we do not look for anything
    !! Ordering 0=AUTO, 1=SEQUENTIAL, 2=PARALLEL
    analysis=1
    !! 0=AUTO, 1=PT_SCOTCH,  2=PARMETIS
    part_par=0
    !! 0=AMD, 1=PIVOT, 2=AMF, 3=SCOTCH, 4=PORD, 5=METIS, 6=MINDEG, 7=AUTO
    part_seq=4
    !! verbose mumps
    verbose =0
    !! default for the root node factorization
    root_parallel = 0

    !! by default we do not repeat analysis phase every time.
    flag_force_analysis = .false.

    !! Block low rank is not activated by default
    lowrank    =0
    lowrank_tol=0
    lowrank_var=0
    cb_compress=0
    mem_increase_pct=30
    !! multithread not activated at tree level by default
    multithread_tree=0
    !! not activated by default
    blk_structure   =0
    flag_block = .false.
    tol_pivot  = -1.0

    !! for MUMPS version >= 5.2
    mem_limit=-1
    advanced =0
    pivot_relax = 0
    !! not activated by default
    mumps_mpi_komp = 0
    !! not activated by default
    mumps_mixed_prec = 0
    
    !! GPU are not activated
    mumps_ngpu_per_mpi   = 0
    mumps_gpu_mpi_tuning = 0
    
    !! depends on the precision
    select case(RKIND_MAT)
      !! simple precision
      case(4)
        call mumps_init_options_precision(ctx_paral,ctx_mumps_c,analysis, &
                                part_par,part_seq,lowrank,lowrank_tol,    &
                                lowrank_var,advanced,mem_increase_pct,    &
                                mem_limit,verbose,symmetric_matrix,       &
                                multithread_tree,blk_structure,           &
                                root_parallel,cb_compress,tol_pivot,      &
                                pivot_relax,mumps_mpi_komp,               &
                                mumps_mixed_prec,mumps_ngpu_per_mpi,      &
                                mumps_gpu_mpi_tuning,ctx_err)
        mumps_version = ctx_mumps_c%version_number

      !! double precision
      case(8)
        call mumps_init_options_precision(ctx_paral,ctx_mumps_z,analysis, &
                                part_par,part_seq,lowrank,lowrank_tol,    &
                                lowrank_var,advanced,mem_increase_pct,    &
                                mem_limit,verbose,symmetric_matrix,       &
                                multithread_tree,blk_structure,           &
                                root_parallel,cb_compress,tol_pivot,      &
                                pivot_relax,mumps_mpi_komp,               &
                                mumps_mixed_prec,mumps_ngpu_per_mpi,      &
                                mumps_gpu_mpi_tuning,ctx_err)  
        mumps_version = ctx_mumps_z%version_number

      case default
        ctx_err%msg   = "** ERROR: Precision for matrix operations not " // &
                        " recognized (must be 4 or 8) [mumps_init_options] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    !! ctx_mumps_c%write_problem = "matrix_dump.txt"
    !! ctx_mumps_z%write_problem = "matrix_dump.txt"
    
    !!  ----------------------------------------------------------------
    !!  error analysis is NOT compatible with multiple-RHS
    !!  Nor with Distributed Matrix. Therefore we first need 
    !!  to force ctx_mumps%ICNTL(18) = 0
    !!  and use, in the matrix arrays [ASSUMING WE USE 1 MPI !!]
    !!  ctx_mumps%NNZ=ctx_mumps%NZ_loc
    !!  ctx_mumps%IRN=ctx_mumps%IRN_LOC
    !!  ctx_mumps%JCN=ctx_mumps%JCN_LOC
    !!  ctx_mumps%A  =ctx_mumps%A_LOC
    !! ------------------------------
    !! 0: none; 1: most complete and expensive; 2: main ones only
    !! ctx_mumps_c%ICNTL(11) = 1 !! need one rhs too.
    !! ctx_mumps_z%ICNTL(11) = 1
    !! iterative refinement: 
    !! ctx_mumps_c%ICNTL(10) =-1000  !! minus force iterations
    !! ctx_mumps_c%CNTL(2)   = 1e-30 !! tolerance for refinement
    !! -----------------------------------------------------------------

    !! optional print informations on screen
    if(verb_level > 0) then
      call print_info_mumps_options(ctx_paral,analysis,part_par,part_seq, &
                                    lowrank,lowrank_tol,lowrank_var,      &
                                    advanced,mem_increase_pct,mem_limit,  &
                                    symmetric_matrix,mumps_version,       &
                                    multithread_tree,blk_structure,       &
                                    root_parallel,cb_compress,tol_pivot,  &
                                    pivot_relax,mumps_mpi_komp,           &
                                    mumps_mixed_prec,mumps_ngpu_per_mpi,  &
                                    mumps_gpu_mpi_tuning,ctx_err)
    end if

    return
  end subroutine mumps_init_default_options


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options for simple complex
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_mumps      : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_init_options_simple_complex(ctx_paral,ctx_mumps,     &
                                analysis,part_par,part_seq,lowrank,     &
                                lowrank_tol,lowrank_var,advanced,       &
                                mem_increase_pct,mem_limit,verbose,     &
                                symmetric_matrix,multithread_tree,      &
                                blk_structure,root_parallel,cb_compress,&
                                tol_pivot,pivot_relax,mumps_mpi_komp,   &
                                mumps_mixed_prec,mumps_ngpu_per_mpi,    &
                                mumps_gpu_mpi_tuning,ctx_err)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(cmumps_struc)       ,intent(inout):: ctx_mumps
    integer                  ,intent(in)   :: analysis,part_par,part_seq
    integer                  ,intent(in)   :: lowrank,lowrank_var,verbose
    integer                  ,intent(in)   :: advanced
    integer                  ,intent(in)   :: mem_increase_pct
    integer                  ,intent(in)   :: multithread_tree
    integer                  ,intent(in)   :: blk_structure
    integer                  ,intent(in)   :: root_parallel
    integer                  ,intent(in)   :: cb_compress
    integer(kind=8)          ,intent(in)   :: mem_limit
    real                     ,intent(in)   :: lowrank_tol
    logical                  ,intent(in)   :: symmetric_matrix
    real                     ,intent(in)   :: tol_pivot
    integer                  ,intent(in)   :: pivot_relax
    integer                  ,intent(in)   :: mumps_mpi_komp
    integer                  ,intent(in)   :: mumps_mixed_prec
    integer                  ,intent(in)   :: mumps_ngpu_per_mpi
    integer                  ,intent(in)   :: mumps_gpu_mpi_tuning
    type(t_error)            ,intent(inout):: ctx_err
    ctx_err%ierr  = 0

    ctx_mumps%COMM= ctx_paral%communicator
    !! Symmetrical matrices (no = 0, yes = 2, SPD (only if real) = 1)
    if(symmetric_matrix) then
      ctx_mumps%SYM = 2
    else
      ctx_mumps%SYM = 0
    endif
    !! Use rank 0 in facto and solve (=1) or just in calls and results (=0)
    ctx_mumps%PAR = 1
    !! initialize context
    ctx_mumps%JOB = -1
    call cmumps(ctx_mumps)
    ctx_err%ierr = ctx_mumps%INFOG(1)
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: INIT CMUMPS JOB=-1 [mumps_init_options] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    if(verbose .eq. 0) then
      ctx_mumps%ICNTL(2)  = 0
      ctx_mumps%ICNTL(3)  = 0
    end if
    !! Parallel distributed matrix
    ctx_mumps%ICNTL(18) = 3

    !! options from parameter file
    ctx_mumps%ICNTL(28) = analysis
    ctx_mumps%ICNTL(29) = part_par
    ctx_mumps%ICNTL(7)  = part_seq
    ctx_mumps%ICNTL(13) = root_parallel
    ctx_mumps%ICNTL(35) = lowrank
    ctx_mumps%CNTL(7)   = lowrank_tol
    !! sanity check
    if(ctx_mumps%ICNTL(35).eq.1 .and. &
       ctx_mumps%CNTL(7) <= 0.) then
      ctx_mumps%ICNTL(35) = 0
    endif
    ctx_mumps%ICNTL(36) = lowrank_var
    ctx_mumps%ICNTL(14) = mem_increase_pct
    !! compression of the contribution blocks
    ctx_mumps%ICNTL(37) = cb_compress
    
    !! -----------------------------------------------------------------
    !! Memory limit for MUMPS version >= 5.2
    !! it is given in MegaBytes
    !! -----------------------------------------------------------------
    if(mem_limit > 0) ctx_mumps%ICNTL(23) = int(mem_limit)
    !! ** KEEP(371) : advanced experimental optimizations
    !! if advanced=1, KEEP(370) should be forced to 1 as well
    ctx_mumps%keep(371) = advanced
    if(ctx_mumps%keep(371) == 1) ctx_mumps%keep(370) = 1

    !! Multithreading at tree level L0 
    !! for Mumps > 5.3 consortium
    !! ctx_mumps%keep(401) = multithread_tree
    ctx_mumps%ICNTL(48) = multithread_tree !! change in 5.4

    !! BLK Structure
    ctx_mumps%ICNTL(15) = blk_structure

    !! pivoting threshold
    if(tol_pivot >= 0.0) then
      ctx_mumps%CNTL(1) = tol_pivot
    end if
    
    !! Relaxed pivoting
    !!     -2 : relaxed pivoting is applied only on large enough matrices
    !!      0 : feature not activated
    !!      1 : relaxed pivoting is applied to all frontal matrices
    !!     77 : automatic setting, feature is not activated except when GPUs
    select case(pivot_relax)
      case(-2,1,0,77)
        ctx_mumps%KEEP(268) = pivot_relax
      case default 
        ctx_mumps%KEEP(268) = 0 ! deactivated
    end select
    
    !! MPI to k OpenMP:
    !! This feature enables MUMPS to use some MPI processes, by block of 
    !! size ICNTL(17), as threads. 
    !! WARNING: It is limited by the number of core on one Node !!!
    !! ≤ 1 : feature not activated
    !! ≥ 2 : value of the MPI to k OMP reuse factor. 
    !!       The number of MPI processes will be divided by this
    !!       factor.
    !! Default value: 0 (functionality not used)
    if(mumps_mpi_komp > 1) then
      ctx_mumps%ICNTL(17) = mumps_mpi_komp
    end if

    !! BLR mixed precision: 
    !! ICNTL(40) activates mixed precision Block Low-Rank feature
    !!
    !!  0 : no mixed precision BLR
    !!  1 : mixed precision BLR
    !! Default value: 0 (no mixed precision BLR)
    !!
    if(mumps_mixed_prec > 0) then
      ctx_mumps%ICNTL(40) = mumps_mixed_prec
    end if

    !! -----------------------------------------------------------------
    !! GPU activations
    !! KEEP(417) number of GPUs per MPI process
    ctx_mumps%keep(417) = mumps_ngpu_per_mpi
    !! KEEP(66)  Tuning for multi-MPI processes
    ctx_mumps%keep(66)  = mumps_gpu_mpi_tuning

    return
  end subroutine mumps_init_options_simple_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options for double complex
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_init_options_double_complex(ctx_paral,ctx_mumps,     &
                                analysis,part_par,part_seq,lowrank,     &
                                lowrank_tol,lowrank_var,advanced,       &
                                mem_increase_pct,mem_limit,verbose,     &
                                symmetric_matrix,multithread_tree,      &
                                blk_structure,root_parallel,cb_compress,&
                                tol_pivot,pivot_relax,mumps_mpi_komp,   &
                                mumps_mixed_prec,mumps_ngpu_per_mpi,    &
                                mumps_gpu_mpi_tuning,ctx_err)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(zmumps_struc)       ,intent(inout):: ctx_mumps
    integer                  ,intent(in)   :: analysis,part_par,part_seq
    integer                  ,intent(in)   :: lowrank,lowrank_var,verbose
    integer                  ,intent(in)   :: advanced
    integer                  ,intent(in)   :: mem_increase_pct
    integer                  ,intent(in)   :: multithread_tree
    integer                  ,intent(in)   :: blk_structure
    integer                  ,intent(in)   :: root_parallel
    integer                  ,intent(in)   :: cb_compress
    integer(kind=8)          ,intent(in)   :: mem_limit
    real                     ,intent(in)   :: lowrank_tol
    logical                  ,intent(in)   :: symmetric_matrix
    real                     ,intent(in)   :: tol_pivot
    integer                  ,intent(in)   :: pivot_relax
    integer                  ,intent(in)   :: mumps_mpi_komp
    integer                  ,intent(in)   :: mumps_mixed_prec
    integer                  ,intent(in)   :: mumps_ngpu_per_mpi
    integer                  ,intent(in)   :: mumps_gpu_mpi_tuning
    type(t_error)            ,intent(inout):: ctx_err
    ctx_err%ierr  = 0


    ctx_mumps%COMM= ctx_paral%communicator
    !! Symmetrical matrices (no = 0, yes = 2, SPD (only if real) = 1)
    if(symmetric_matrix) then
      ctx_mumps%SYM = 2
    else
      ctx_mumps%SYM = 0
    endif
    !! Use rank 0 in facto and solve (=1) or just in calls and results (=0)
    ctx_mumps%PAR = 1
    !! initialize context
    ctx_mumps%JOB = -1
    call zmumps(ctx_mumps)
    ctx_err%ierr = ctx_mumps%INFOG(1)
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: INIT CMUMPS JOB=-1 [mumps_init_options] **"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    if(verbose .eq. 0) then
      ctx_mumps%ICNTL(2)  = 0
      ctx_mumps%ICNTL(3)  = 0
    end if
    !! Parallel distributed matrix
    ctx_mumps%ICNTL(18) = 3

    !! options from parameter file
    ctx_mumps%ICNTL(28) = analysis
    ctx_mumps%ICNTL(29) = part_par
    ctx_mumps%ICNTL(7)  = part_seq
    ctx_mumps%ICNTL(13) = root_parallel
    ctx_mumps%ICNTL(35) = lowrank
    ctx_mumps%CNTL(7)   = lowrank_tol
    !! sanity check
    if(ctx_mumps%ICNTL(35).eq.1 .and. &
       ctx_mumps%CNTL(7) <= 0.) then
      ctx_mumps%ICNTL(35) = 0
    endif
    ctx_mumps%ICNTL(36) = lowrank_var
    ctx_mumps%ICNTL(14) = mem_increase_pct
    !! compression of the contribution blocks
    ctx_mumps%ICNTL(37) = cb_compress

    !! -----------------------------------------------------------------
    !! Memory limit for MUMPS version >= 5.2
    !! it is given in MegaBytes
    !! -----------------------------------------------------------------
    if(mem_limit > 0) ctx_mumps%ICNTL(23) = int(mem_limit)
    !! ** KEEP(371) : advanced experimental optimizations
    !! if advanced=1, KEEP(370) should be forced to 1 as well
    ctx_mumps%keep(371) = advanced
    if(ctx_mumps%keep(371) == 1) ctx_mumps%keep(370) = 1

    !! Multithreading at tree level L0 
    !! for Mumps > 5.3 consortium
    !! ctx_mumps%keep(401) = multithread_tree
    ctx_mumps%ICNTL(48) = multithread_tree !! change in 5.4

    !! BLK Structure
    ctx_mumps%ICNTL(15) = blk_structure
    
    !! pivoting threshold
    if(tol_pivot >= 0.0) then
      ctx_mumps%CNTL(1) = tol_pivot
    end if

    !! Relaxed pivoting
    !!     -2 : relaxed pivoting is applied only on large enough matrices
    !!      0 : feature not activated
    !!      1 : relaxed pivoting is applied to all frontal matrices
    !!     77 : automatic setting, feature is not activated except when GPUs
    select case(pivot_relax)
      case(-2,1,0,77)
        ctx_mumps%KEEP(268) = pivot_relax
      case default 
        ctx_mumps%KEEP(268) = 0 ! deactivated
    end select

    !! MPI to k OpenMP:
    !! This feature enables MUMPS to use some MPI processes, by block of 
    !! size ICNTL(17), as threads. 
    !! WARNING: It is limited by the number of core on one Node !!!
    !! ≤ 1 : feature not activated
    !! ≥ 2 : value of the MPI to k OMP reuse factor. 
    !!       The number of MPI processes will be divided by this
    !!       factor.
    !! Default value: 0 (functionality not used)
    if(mumps_mpi_komp > 1) then
      ctx_mumps%ICNTL(17) = mumps_mpi_komp
    end if

    !! BLR mixed precision: 
    !! ICNTL(40) activates mixed precision Block Low-Rank feature
    !!
    !!  0 : no mixed precision BLR
    !!  1 : mixed precision BLR
    !! Default value: 0 (no mixed precision BLR)
    !!
    if(mumps_mixed_prec > 0) then
      ctx_mumps%ICNTL(40) = mumps_mixed_prec
    end if
    
    !! -----------------------------------------------------------------
    !! GPU activations
    !! KEEP(417) number of GPUs per MPI process
    ctx_mumps%keep(417) = mumps_ngpu_per_mpi
    !! KEEP(66)  Tuning for multi-MPI processes
    ctx_mumps%keep(66)  = mumps_gpu_mpi_tuning
    
    return
  end subroutine mumps_init_options_double_complex


end module m_solver_mumps_init

