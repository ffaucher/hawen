!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_mumps.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for mumps solver
!
!------------------------------------------------------------------------------
module m_print_mumps

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  use m_barrier,          only : barrier
  !> matrix size precision
  use m_define_precision, only : IKIND_MAT, RKIND_MAT
  implicit none
  
  private
  public :: print_info_mumps_options, print_info_mumps_factorization,  &
            print_info_mumps_solve
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options for double complex
  !
  !> @param[inout] ctx_paral      : context parallelism
  !> @param[inout] ctx_solver     : context solver
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine print_info_mumps_options(ctx_paral,analysis,part_par,part_seq,   &
                                      lowrank,lowrank_tol,lowrank_var,        &
                                      advanced,mem_increase_pct,mem_limit,    &
                                      symmetric_matrix,mumps_ver,             &
                                      multithread_tree,blk_structure,         &
                                      root_parallel,cb_compress,tol_pivot,    &
                                      pivot_relax,mumps_mpi_komp,             &
                                      mumps_mixed_prec,mumps_ngpu_per_mpi,    &
                                      mumps_gpu_mpi_tuning,ctx_err)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    integer                  ,intent(in)   :: analysis,part_par,part_seq
    integer                  ,intent(in)   :: lowrank,lowrank_var
    integer                  ,intent(in)   :: advanced
    integer                  ,intent(in)   :: mem_increase_pct
    integer                  ,intent(in)   :: multithread_tree
    integer                  ,intent(in)   :: blk_structure
    integer                  ,intent(in)   :: root_parallel
    integer                  ,intent(in)   :: cb_compress
    integer(kind=8)          ,intent(in)   :: mem_limit
    real                     ,intent(in)   :: lowrank_tol
    logical                  ,intent(in)   :: symmetric_matrix
    character(len=*)         ,intent(in)   :: mumps_ver
    real                     ,intent(in)   :: tol_pivot
    integer                  ,intent(in)   :: pivot_relax
    integer                  ,intent(in)   :: mumps_mpi_komp
    integer                  ,intent(in)   :: mumps_mixed_prec
    integer                  ,intent(in)   :: mumps_ngpu_per_mpi
    integer                  ,intent(in)   :: mumps_gpu_mpi_tuning
    type(t_error)            ,intent(inout):: ctx_err
    !!
    character(len=64)                      :: str_mem

    ctx_err%ierr  = 0
    
    !! print infos
    if(ctx_paral%master) then
      write(6,'(2a)') indicator, " solver MUMPS "//                     &
                      trim(adjustl(mumps_ver))//" options "
      select case (RKIND_MAT)
        case(4)
          if(symmetric_matrix) then
            write(6,'(a)')    "    [SYMMETRIC SIMPLE PRECISION COMPLEX MATRIX] "
          else
            write(6,'(a)')    "    [NON-SYMMETRIC SIMPLE PRECISION COMPLEX "// &
                              "MATRIX] "
          endif
        case(8)
          if(symmetric_matrix) then
            write(6,'(a)')    "    [SYMMETRIC DOUBLE PRECISION COMPLEX MATRIX] "
          else
            write(6,'(a)')    "    [NON-SYMMETRIC DOUBLE PRECISION COMPLEX "// &
                              "MATRIX] "
          endif
      end select
      
      !! Mumps analysis
      select case(analysis)
        case(1)
          write(6,'(a)') "- SEQUENTIAL analysis"
        case(2)
          write(6,'(a)') "- PARALLEL analysis"
        case default
          write(6,'(a)') "- AUTOMATIC (SEQ/PAR) analysis"
      end select
      !! parallel partitioner 
      if(analysis .eq. 2) then
        select case(part_par)
          case(1)
            write(6,'(a)') "-    PARALLEL partitioner PT_SCOTCH"
          case(2)
            write(6,'(a)') "-    PARALLEL partitioner PARMETIS"
          case default
            write(6,'(a)') "-    PARALLEL partitioner AUTOMATIC CHOICE"
        end select
      endif
      !! sequential partitioner
      if(analysis .eq. 1) then
        select case(part_seq)
          case(0)
            write(6,'(a)') "-    SEQUENTIAL partitioner AMD"
          case(1)
            write(6,'(a)') "-    SEQUENTIAL partitioner PIVOT"
          case(2)
            write(6,'(a)') "-    SEQUENTIAL partitioner AMF"
          case(3)
            write(6,'(a)') "-    SEQUENTIAL partitioner SCOTCH"
          case(4)
            write(6,'(a)') "-    SEQUENTIAL partitioner PORD"
          case(5)
            write(6,'(a)') "-    SEQUENTIAL partitioner METIS"
          case(6)
            write(6,'(a)') "-    SEQUENTIAL partitioner MINDEG"
          case default
            write(6,'(a)') "-    SEQUENTIAL partitioner AUTOMATIC CHOICE"
        end select
      endif      
      !! memory increase info 
      write(6,'(a,i4)') "- Working space memory increase (%)", mem_increase_pct
      if(mem_limit > 0) then
        !! given in MegaBytes
        call cast_memory(int8(1024*1024*mem_limit),str_mem)
        write(6,'(2a)') "- Working space maximal memory: ",trim(adjustl(str_mem))
      end if
      
      !! root node factorization
      select case(root_parallel)
        case(0)
          write(6,'(a)') "- parallel factorization of the root node"
        case(1)
          write(6,'(a)') "- sequential factorization of the root node"
        case(-1)
          write(6,'(a)') "- force parallel factorization of the root node"
      end select
      
      !! block low rank infos 
      if(lowrank .eq. 1) then
        if(lowrank_var .eq. 1) then  
          write(6,'(a)') "- Block low rank VARIANT USED for facto.: "
        else
          write(6,'(a)') "- Block low rank USED for facto.: "
        endif
        write(6,'(a,ES12.4)')  "-    tolerance  ", lowrank_tol
      else if(lowrank .eq. 2) then
        if(lowrank_var .eq. 1) then  
          write(6,'(a)') "- Block low rank VARIANT USED for facto. and solve: "
        else
          write(6,'(a)') "- Block low rank USED for facto. and solve: "
        endif
        write(6,'(a,ES12.4)')  "-    tolerance  ", lowrank_tol
      else
        write(6,'(a)') "- Block low rank NOT USED "
      endif

      !! compression of the contribution blocks
      if(lowrank > 0) then
        if(cb_compress == 1) then
          write(6,'(a)') "- Compression of the contribution blocks activated"
        end if      
      end if
      
      !! advanced
      if(advanced .eq. 1) then
        write(6,'(a)') "- EXPERIMENTAL advanced optimization used "
      endif
      
      !! multithreading l0 tree
      if(multithread_tree .eq. 1) then
        write(6,'(a)') "- Multithreading L0 at tree level is activated "
      endif
      
      !! Using BLK Structure
      if(blk_structure .eq. 1) then
        write(6,'(a)') "- Using the Block Structure Option "
      end if

      !! tolerance for the pivoting
      if(tol_pivot >= 0.0) then
        write(6,'(a,ES12.4)') "- Tolerance for pivoting: ", tol_pivot
      end if

    
      !! Relaxed pivoting
      !!     -2 : relaxed pivoting is applied only on large enough matrices
      !!      0 : feature not activated
      !!      1 : relaxed pivoting is applied to all frontal matrices
      !!     77 : automatic setting, feature is not activated except when GPUs
      select case(pivot_relax)
        case(-2)
          write(6,'(a)') "- Relaxed pivoting on large matrices "
        case( 1)
          write(6,'(a)') "- Relaxed pivoting on all matrices "
        case( 0)
          write(6,'(a)') "- Relaxed pivoting deactivated "
        case(77)
          write(6,'(a)') "- Automatic selection of relaxed pivoting "
        case default
          write(6,'(a)') "- Unrecognized selection of relaxed pivoting "
      end select

      !! MPI to k OpenMP:
      !! This feature enables MUMPS to use some MPI processes, by block of 
      !! size ICNTL(17), as threads. 
      !! WARNING: It is limited by the number of core on one Node !!!
      !! ≤ 1 : feature not activated
      !! ≥ 2 : value of the MPI to k OMP reuse factor. 
      !!       The number of MPI processes will be divided by this
      !!       factor.
      !! Default value: 0 (functionality not used)
      if(mumps_mpi_komp > 1) then
        write(6,'(a,i0,a)') "- Remaping ",mumps_mpi_komp,"mpi to threads "
        write(6,'(a)')      "   |- must not exceed the ncores per node   "
      end if
    
      if(mumps_mixed_prec == 1) then
        if(lowrank > 0) then
          write(6,'(a)') "- Using mixed precision with block low rank "
        else
          write(6,'(a)') "- Mixed precision cannot be used without blr "
        end if
      end if
      
      !! GPU Information
      if(mumps_ngpu_per_mpi > 0) then
        write(6,'(a,a)')  "- GPU activation for solver operations "
        write(6,'(a,i0)') "-     number of GPU per MPI:        ",       &
                                                      mumps_ngpu_per_mpi
        write(6,'(a,i0)') "-     tuning for multi-MPI process: ",       &
                                                    mumps_gpu_mpi_tuning
      end if

      write(6,'(a)')    separator
    end if
    return
    
  end subroutine print_info_mumps_options    

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> time info for mumps factorization
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   time_creation : time spent
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_mumps_factorization(ctx_paral,mem_gb,mem_eff,   &
                                            time,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    integer(kind=8)    ,intent(in)   :: mem_gb
    integer(kind=8)    ,intent(in)   :: mem_eff
    real   (kind=8)    ,intent(in)   :: time
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    character(len=64) :: str_time,str_mem
    
    ctx_err%ierr  = 0
    
    if(ctx_paral%master) then
      write(6,'(2a)') indicator, " Solver operations "
      call cast_memory(mem_gb,str_mem)
      write(6,'(2a)') "- matrix facto. mem global   : ",trim(adjustl(str_mem))
      call cast_memory(mem_eff,str_mem)
      write(6,'(2a)') "- matrix facto. mem effective: ",trim(adjustl(str_mem))
      
      call cast_time(time,str_time)
      write(6,'(2a)') "- matrix factorization time  : ",trim(adjustl(str_time))
      !! write(6,'(a)')    separator
    endif
    
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_mumps_factorization

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> time info for mumps solve
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   mem           : memory
  !> @param[in]   time          : time spent
  !> @param[in]   flag_adjoint  : adjoint problem
  !> @param[in]   n_rhs         : number of parallel rhs  
  !> @param[in]   mem_monoproc_array: memory for monoprocessor array 
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_mumps_solve(ctx_paral,mem,time,flag_adjoint,   &
                                    n_rhs,mem_monoproc_array,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    integer(kind=8)    ,intent(in)   :: mem
    integer(kind=8)    ,intent(in)   :: mem_monoproc_array
    real(kind=8)       ,intent(in)   :: time
    logical            ,intent(in)   :: flag_adjoint
    integer            ,intent(in)   :: n_rhs
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    character(len=64) :: str_time,str_mem
    
    ctx_err%ierr  = 0
    
    if(ctx_paral%master) then
      if(flag_adjoint) then
        write(6,'(a)') "- matrix solve for ADJOINT problem "
      else
        write(6,'(a)') "- matrix solve for DIRECT problem "
      endif
      
      write(6,'(a,i8)')"-    nb of parallel rhs      : ",n_rhs
      call cast_memory(mem_monoproc_array,str_mem)
      write(6,'(2a)')  "-    monoproc solution memory: ",trim(adjustl(str_mem))      
      call cast_memory(mem,str_mem)
      !! It is not accurate if one has provided the 
      !! memory limit to Mumps ...
      write(6,'(2a)')  "-    solve memory (ko if lim): ",trim(adjustl(str_mem))

      
      call cast_time(time,str_time)
      write(6,'(2a)')  "-    solve time              : ",trim(adjustl(str_time))
      !write(6,'(a)')    separator
    endif
    
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_mumps_solve

end module m_print_mumps
