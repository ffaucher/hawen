!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_solver_mumps_facto.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to perform factorization for solver MUMPS with choices:
!>  - MUMPS Complex simple precision
!>  - MUMPS Complex double precision
!
!------------------------------------------------------------------------------
module m_solver_mumps_facto
  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_sum,          only: allreduce_sum
  use m_define_precision,       only: RKIND_MAT
  use m_ctx_matrix,             only: t_matrix
  use m_print_mumps,            only: print_info_mumps_factorization
  use m_time,                   only: time
  implicit none 
  !> mumps include: simple of double precision complex
  include 'cmumps_struc.h'
  include 'zmumps_struc.h'
  !! ------------------------------------------------------------------- 
  
  interface mumps_factorization_precision
     module procedure mumps_factorization_double_complex
     module procedure mumps_factorization_simple_complex
  end interface
   
  private
  public  :: mumps_factorization
  private :: mumps_factorization_precision  
  
  contains 

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> factorization using mumps
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_mumps_c    : context solver simple
  !> @param[inout] ctx_mumps_z    : context solver double
  !> @param[inout] ctx_matrix     : context matrix
  !> @param[out]   mem_facto      : memory for facto
  !> @param[inout] flag_analysis  : indicates if we should do the analysis
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_flag_verb    : verbosity level
  !----------------------------------------------------------------------------
  subroutine mumps_factorization(ctx_paral,ctx_mumps_c,ctx_mumps_z,     &
                                 ctx_matrix,mem_facto,flag_analysis,    &
                                 ctx_err,o_flag_verb)
  
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(cmumps_struc)       ,intent(inout):: ctx_mumps_c
    type(zmumps_struc)       ,intent(inout):: ctx_mumps_z
    type(t_matrix)           ,intent(inout):: ctx_matrix
    integer(kind=8)          ,intent(out)  :: mem_facto
    logical                  ,intent(inout):: flag_analysis
    type(t_error)            ,intent(inout):: ctx_err
    logical      ,optional   ,intent(in)   :: o_flag_verb
    !! local
    integer         :: ierr,ierr_ref
    integer(kind=8) :: mem_gb, mem_eff
    real   (kind=8) :: time0,time1
    logical         :: flag_verb
    
    ctx_err%ierr  = 0
    
    call time(time0)
    flag_verb = .true.
    if(present(o_flag_verb)) flag_verb = o_flag_verb
    
    !! depends on the precision
    select case(RKIND_MAT)
      !! simple precision
      case(4)
        call mumps_factorization_simple_complex(ctx_paral,ctx_mumps_c,  &
                                 ctx_matrix,mem_gb,mem_eff,             &
                                 flag_analysis,ctx_err)
      !! double precision
      case(8)
        call mumps_factorization_double_complex(ctx_paral,ctx_mumps_z,  &
                                 ctx_matrix,mem_gb,mem_eff,             &
                                 flag_analysis,ctx_err)
      
      case default
        ctx_err%msg   = "** ERROR: Precision for matrix operations not " // &
                        " recognized (must be 4 or 8) [mumps_factorization] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    !! error return
    ierr    =0
    ierr_ref=ctx_err%ierr
    call allreduce_sum(ctx_err%ierr,ierr,1,ctx_paral%communicator,ctx_err)
    if(ierr.ne.0) then
      ctx_err%ierr  = ierr_ref
      
      !! some known error ----------------------------------------------
      select case(ctx_err%ierr)
        case(-9)
          ctx_err%msg="**ERROR MUMPS: Workarray is too small (you can"//&
                      " increase ICNTL(14) with keyword "             //&
                      "mumps_memory_increase_percent or maybe the "   //&
                      "memory limit with mumps_memory_limit)"
        case(-10)
          ctx_err%msg="**ERROR MUMPS: The matrix is singular "
        case(-19)
          ctx_err%msg="**ERROR MUMPS: Not Enough Memory (you can try " // &
                      "to increase ICNTL(23) with keyword mumps_memory_limit)"
        case(-20)
          ctx_err%msg="**ERROR MUMPS: Reception buffer is too small " //&
                      "(you can increase ICNTL(14) with keyword "     //&
                      "mumps_memory_increase_percent)"
        case default
          ctx_err%msg="**ERROR during MUMPS Matrix factorization"
      end select
      !! ---------------------------------------------------------------
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    
    !! print informations on screen
    call time(time1)
    if(flag_verb) then
      call print_info_mumps_factorization(ctx_paral,mem_gb,mem_eff,     &
                                          time1-time0,ctx_err)
    end if
    !! the output memory is the global one
    mem_facto = mem_gb

    return
  end subroutine mumps_factorization
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options for simple complex
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_mumps      : context solver
  !> @param[inout] ctx_matrix     : context matrix
  !> @param[inout] mem_facto      : memory for factorization
  !> @param[inout] flag_analysis  : indicates if we should do the analysis
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_factorization_simple_complex(ctx_paral,ctx_mumps,    &
                                 ctx_matrix,mem_facto_global,           &
                                 mem_facto_effective,flag_analysis,ctx_err)
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(cmumps_struc)       ,intent(inout):: ctx_mumps
    type(t_matrix)           ,intent(inout):: ctx_matrix
    integer(kind=8)          ,intent(out)  :: mem_facto_global
    integer(kind=8)          ,intent(out)  :: mem_facto_effective
    logical                  ,intent(inout):: flag_analysis
    type(t_error)            ,intent(inout):: ctx_err
    ctx_err%ierr  = 0

    !! Transfer to Mumps architecture
    ctx_mumps%N      = int(ctx_matrix%dim_matrix_gb,kind=4)
    ctx_mumps%NZ_loc = int(ctx_matrix%dim_nnz_loc  ,kind=4)

    allocate(ctx_mumps%IRN_loc(ctx_mumps%NZ_loc))
    allocate(ctx_mumps%JCN_loc(ctx_mumps%NZ_loc))
    allocate(ctx_mumps%A_loc  (ctx_mumps%NZ_loc))

    ctx_mumps%IRN_loc(1:ctx_mumps%NZ_loc) = &
                         int(ctx_matrix%ilin(1:ctx_mumps%NZ_loc),kind=4)
    ctx_mumps%JCN_loc(1:ctx_mumps%NZ_loc) = &
                         int(ctx_matrix%icol(1:ctx_mumps%NZ_loc),kind=4)
    ctx_mumps%A_loc  (1:ctx_mumps%NZ_loc) = &
                       cmplx(ctx_matrix%Aval(1:ctx_mumps%NZ_loc),kind=4)

    !! clean ctx_matrix
    deallocate(ctx_matrix%ilin)
    deallocate(ctx_matrix%icol)
    deallocate(ctx_matrix%Aval)

    !! for 1 mpi with statistics, we need to use a non-distributed array.===========
    !write(*,*) "mumps statistics with 1mpi ======================================= "
    !ctx_mumps%ICNTL(11) = 1 ! full statistics 
    !ctx_mumps%ICNTL(18) = 0 ! non-distributed matrix.
    !allocate(ctx_mumps%IRN(ctx_mumps%NZ_loc))
    !allocate(ctx_mumps%JCN(ctx_mumps%NZ_loc))
    !allocate(ctx_mumps%A  (ctx_mumps%NZ_loc))
    !ctx_mumps%NNZ=ctx_mumps%NZ_loc
    !ctx_mumps%IRN=ctx_mumps%IRN_LOC
    !ctx_mumps%JCN=ctx_mumps%JCN_LOC
    !ctx_mumps%A  =ctx_mumps%A_LOC
    !! =============================================================================

    !! in case we use the blocks
    if(ctx_matrix%flag_block) then
      !! always simple precision
      ctx_mumps%NBLK = int(ctx_matrix%nblk,kind=4)
      if(ctx_paral%master) then
        allocate(ctx_mumps%BLKPTR(ctx_mumps%NBLK+1))
        !! always simple precision
        ctx_mumps%BLKPTR(:) = int(ctx_matrix%blkptr(:),kind=4)
      end if
    end if

    !! factorization: the analysis phase is only done once -------------
    if(.not. flag_analysis) then
      ctx_mumps%JOB=4
      flag_analysis=.true.
    else
      ctx_mumps%JOB=2 !! factorization only
    end if
    call cmumps(ctx_mumps)

    !! after the factorization, we do not need I,J,A anymore, 
    !! these are pointer
    !! -----------------------------------------------------------------
    if(associated(ctx_mumps%IRN_loc))  then
      deallocate(ctx_mumps%IRN_loc,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%IRN_loc)
    end if
    if(associated(ctx_mumps%JCN_loc))  then
      deallocate(ctx_mumps%JCN_loc,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%JCN_loc)
    end if
    if(associated(ctx_mumps%A_loc  ))  then
      deallocate(ctx_mumps%A_loc  ,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%A_loc)
    end if
    if(associated(ctx_mumps%BLKPTR  ))  then
      deallocate(ctx_mumps%BLKPTR  ,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%BLKPTR)
    end if

    !! get factorization memory 
    !! INFOG(19): global allocation (ko when mem limit given)
    !! INFOG(22): effectively used
    mem_facto_global    = int8(ctx_mumps%INFOG(19)*1D6)
    mem_facto_effective = int8(ctx_mumps%INFOG(22)*1D6)

    !! we do not care for deallocation error because we nullify anyway   
    ctx_err%ierr=ctx_mumps%INFOG(1)

    return
  end subroutine mumps_factorization_simple_complex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Initialize Mumps solver main options for simple complex
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_mumps      : context solver
  !> @param[inout] ctx_matrix     : context matrix
  !> @param[inout] mem_facto      : memory for factorization
  !> @param[inout] flag_analysis  : indicates if we should do the analysis
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine mumps_factorization_double_complex(ctx_paral,ctx_mumps,    &
                                 ctx_matrix,mem_facto_global,           &
                                 mem_facto_effective,flag_analysis,ctx_err)
    implicit none
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(zmumps_struc)       ,intent(inout):: ctx_mumps
    type(t_matrix)           ,intent(inout):: ctx_matrix
    integer(kind=8)          ,intent(out)  :: mem_facto_global
    integer(kind=8)          ,intent(out)  :: mem_facto_effective
    logical                  ,intent(inout):: flag_analysis
    type(t_error)            ,intent(inout):: ctx_err
    ctx_err%ierr  = 0

    !! Transfer to Mumps architecture
    ctx_mumps%N      = int(ctx_matrix%dim_matrix_gb, kind=4)
    ctx_mumps%NZ_loc = int(ctx_matrix%dim_nnz_loc  , kind=4)

    allocate(ctx_mumps%IRN_loc(ctx_mumps%NZ_loc))
    allocate(ctx_mumps%JCN_loc(ctx_mumps%NZ_loc))
    allocate(ctx_mumps%A_loc  (ctx_mumps%NZ_loc))

    ctx_mumps%IRN_loc(1:ctx_mumps%NZ_loc) = &
                         int(ctx_matrix%ilin(1:ctx_mumps%NZ_loc),kind=4)
    ctx_mumps%JCN_loc(1:ctx_mumps%NZ_loc) = &
                         int(ctx_matrix%icol(1:ctx_mumps%NZ_loc),kind=4)
    ctx_mumps%A_loc  (1:ctx_mumps%NZ_loc) = &
                       cmplx(ctx_matrix%Aval(1:ctx_mumps%NZ_loc),kind=8)

    !! clean ctx_matrix    
    deallocate(ctx_matrix%ilin)
    deallocate(ctx_matrix%icol)
    deallocate(ctx_matrix%Aval)

    !! for 1 mpi with statistics, we need to use a non-distributed array.===========
    !write(*,*) "mumps statistics with 1mpi ======================================= "
    !ctx_mumps%ICNTL(11) = 1 ! full statistics 
    !ctx_mumps%ICNTL(18) = 0 ! non-distributed matrix.
    !allocate(ctx_mumps%IRN(ctx_mumps%NZ_loc))
    !allocate(ctx_mumps%JCN(ctx_mumps%NZ_loc))
    !allocate(ctx_mumps%A  (ctx_mumps%NZ_loc))
    !ctx_mumps%NNZ=ctx_mumps%NZ_loc
    !ctx_mumps%IRN=ctx_mumps%IRN_LOC
    !ctx_mumps%JCN=ctx_mumps%JCN_LOC
    !ctx_mumps%A  =ctx_mumps%A_LOC
    !! =============================================================================

    !! in case we use the blocks
    if(ctx_matrix%flag_block) then
      !! always simple precision
      ctx_mumps%NBLK = int(ctx_matrix%nblk,kind=4)
      if(ctx_paral%master) then
        allocate(ctx_mumps%BLKPTR(ctx_mumps%NBLK+1))
        !! always simple precision
        ctx_mumps%BLKPTR(:) = int(ctx_matrix%blkptr(:),kind=4)
      end if
    end if
    
    !! factorization: the analysis phase is only done once -------------
    if(.not. flag_analysis) then
      ctx_mumps%JOB=4
      flag_analysis=.true.
    else
      ctx_mumps%JOB=2 !! factorization only
    end if
    call zmumps(ctx_mumps)

    !! after the factorization, we do not need I,J,A anymore, 
    !! these are pointer
    !! -----------------------------------------------------------------
    if(associated(ctx_mumps%IRN_loc))  then
      deallocate(ctx_mumps%IRN_loc,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%IRN_loc)
    end if
    if(associated(ctx_mumps%JCN_loc))  then
      deallocate(ctx_mumps%JCN_loc,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%JCN_loc)
    end if
    if(associated(ctx_mumps%A_loc  ))  then
      deallocate(ctx_mumps%A_loc  ,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%A_loc)
    end if
    if(associated(ctx_mumps%BLKPTR  ))  then
      deallocate(ctx_mumps%BLKPTR  ,stat=ctx_err%ierr)
      if(ctx_err%ierr .ne. 0) nullify(ctx_mumps%BLKPTR)
    end if
    ctx_err%ierr=ctx_mumps%INFOG(1)

    !! get factorization memory 
    !! INFOG(19): global allocation (ko when mem limit given)
    !! INFOG(22): effectively used
    mem_facto_global    = int8(ctx_mumps%INFOG(19)*1D6)
    mem_facto_effective = int8(ctx_mumps%INFOG(22)*1D6)

    return
  end subroutine mumps_factorization_double_complex

end module m_solver_mumps_facto

