!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_square_root.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the square root 
!> of a small matrix: only for 2x2 matrices where
!> we have explicit formulas.
!
!------------------------------------------------------------------------------
module m_matrix_square_root

  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error

  implicit none 
  !! ------------------------------------------------------------------- 
  
  interface matrix_sqroot_2x2
    module procedure matrix_sqroot_2x2_double_complex
    module procedure matrix_sqroot_2x2_simple_complex
    !! unclear for real matrices as the square 
    !! root may be complex.
    !! module procedure matrix_sqroot_2x2_double_real
    !! module procedure matrix_sqroot_2x2_simple_real
  end interface matrix_sqroot_2x2
   
  private
  public  :: matrix_sqroot_2x2
  
  contains 

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> square root of a 2x2 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Asqroot        : square root of the matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_sqroot_2x2_double_complex(A,Asqroot,ctx_err)
    implicit none

    complex(kind=8)    ,intent(in)     :: A      (2,2)
    complex(kind=8)    ,intent(inout)  :: Asqroot(2,2)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    complex(kind=8)  :: det,tr,s,t

    Asqroot = 0.d0

    !! compute determinant
    det = A(1,1)*A(2,2) - A(1,2)*A(2,1)
    !! compute the trace
    tr  = A(1,1) + A(2,2)
    !! compute auxiliary quantities s and t: we use the + convention.
    s = sqrt(det)
    t = sqrt(tr + 2.d0*s)

    if(abs(t) < tiny(1.d0)) then
      !! identity matrix maybe 
      if(abs(A(1,2)) < tiny(1.0) .and. abs(A(2,1)) < tiny(1.0)) then
        !! our convention is to keep the sign out to avoid imaginary unit
        if(real(A(1,1)) < 0.d0 .and. real(A(2,2)) < 0.d0) then
          Asqroot(1,1) =-sqrt(-A(1,1))
          Asqroot(2,2) =-sqrt(-A(2,2))
        else
          Asqroot(1,1) = sqrt(A(1,1))
          Asqroot(2,2) = sqrt(A(2,2))        
        end if
      else
        ctx_err%ierr = -76
        ctx_err%msg  = "** ERROR: the 2x2 matrix square root has its"// &
                       " auxiliary quantity set to zero. [matrix_sqroot_2x2]"
        ctx_err%critic=.true.
        return
      end if
    else   
      !! start with s*identity
      Asqroot(1,1) = s ; Asqroot(2,2) = s 
      Asqroot = 1.d0/t * (A + Asqroot)
    end if

    return
  end subroutine matrix_sqroot_2x2_double_complex
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> square root of a 2x2 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Asqroot        : square root of the matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_sqroot_2x2_simple_complex(A,Asqroot,ctx_err)
    implicit none

    complex(kind=4)    ,intent(in)     :: A      (2,2)
    complex(kind=4)    ,intent(inout)  :: Asqroot(2,2)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    complex(kind=8)  :: det,tr,s,t
    complex(kind=8)  :: identity(2,2)

    Asqroot = 0.d0

    !! compute determinant
    det = dcmplx(A(1,1)*A(2,2)) - dcmplx(A(1,2)*A(2,1))
    !! compute the trace
    tr  = dcmplx(A(1,1)) + dcmplx(A(2,2))
    !! compute auxiliary quantities s and t: we use the + convention.
    s = sqrt(det)
    t = sqrt(tr + 2.d0*s)
    
    if(abs(t) < tiny(1.d0)) then
      !! identity matrix maybe 
      if(abs(A(1,2)) < tiny(1.0) .and. abs(A(2,1)) < tiny(1.0)) then
        !! our convention is to keep the sign out to avoid imaginary unit
        if(real(A(1,1)) < 0.d0 .and. real(A(2,2)) < 0.d0) then
          Asqroot(1,1) =-sqrt(-A(1,1))
          Asqroot(2,2) =-sqrt(-A(2,2))
        else
          Asqroot(1,1) = sqrt(A(1,1))
          Asqroot(2,2) = sqrt(A(2,2))        
        end if
      else
        ctx_err%ierr = -76
        ctx_err%msg  = "** ERROR: the 2x2 matrix square root has its"// &
                       " auxiliary quantity set to zero. [matrix_sqroot_2x2]"
        ctx_err%critic=.true.
        return
      end if
    else
      !! start with s*identity
      identity = 0.d0
      identity(1,1) = s ; identity(2,2) = s
      Asqroot = cmplx(1.d0/t * (A + identity))
    end if
    return
  end subroutine matrix_sqroot_2x2_simple_complex

end module m_matrix_square_root
