!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_matrix_inverse.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to inverse a small matrix: only for 2x2
!> matrices or 3x3, where we have explicit formulas.
!
!------------------------------------------------------------------------------
module m_matrix_inverse
  !! module used -------------------------------------------------------
  use m_raise_error,            only: t_error

  implicit none 
  !! ------------------------------------------------------------------- 
  
  interface matrix_inverse_2x2
    module procedure matrix_inverse_2x2_double_complex
    module procedure matrix_inverse_2x2_simple_complex
    module procedure matrix_inverse_2x2_double_real
    module procedure matrix_inverse_2x2_simple_real
  end interface matrix_inverse_2x2

  interface matrix_inverse_3x3
    module procedure matrix_inverse_3x3_double_complex
    module procedure matrix_inverse_3x3_simple_complex
    module procedure matrix_inverse_3x3_double_real
    module procedure matrix_inverse_3x3_simple_real
  end interface matrix_inverse_3x3
   
  private
  public  :: matrix_inverse_2x2, matrix_inverse_3x3
  
  contains 

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 2x2 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_2x2_double_complex(A,Ainv,ctx_err)
    implicit none

    complex(kind=8)    ,intent(in)     :: A   (:,:)
    complex(kind=8)    ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    complex(kind=8)  :: det

    Ainv = 0.d0

    !! compute determinant
    det = A(1,1)*A(2,2) - A(1,2)*A(2,1)
    if(abs(det) < tiny(1.d0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 2x2 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_2x2]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) = A(2,2)
    Ainv(1,2) =-A(1,2)
    Ainv(2,1) =-A(2,1)
    Ainv(2,2) = A(1,1)
    Ainv      = 1.d0/det * Ainv

    return
  end subroutine matrix_inverse_2x2_double_complex
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 2x2 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_2x2_double_real(A,Ainv,ctx_err)
    implicit none

    real(kind=8)       ,intent(in)     :: A   (:,:)
    real(kind=8)       ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    real(kind=8) :: det

    Ainv = 0.d0

    !! compute determinant
    det = A(1,1)*A(2,2) - A(1,2)*A(2,1)
    if(abs(det) < tiny(1.d0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 2x2 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_2x2]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) = A(2,2)
    Ainv(1,2) =-A(1,2)
    Ainv(2,1) =-A(2,1)
    Ainv(2,2) = A(1,1)
    Ainv      = 1.d0/det * Ainv

    return
  end subroutine matrix_inverse_2x2_double_real

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 2x2 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_2x2_simple_complex(A,Ainv,ctx_err)
    implicit none

    complex(kind=4)    ,intent(in)     :: A   (:,:)
    complex(kind=4)    ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    complex(kind=8)  :: det

    Ainv = 0.d0

    !! compute determinant
    det = dcmplx(A(1,1)*A(2,2) - A(1,2)*A(2,1))
    if(abs(det) < tiny(1.0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 2x2 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_2x2]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) = A(2,2)
    Ainv(1,2) =-A(1,2)
    Ainv(2,1) =-A(2,1)
    Ainv(2,2) = A(1,1)
    Ainv      =cmplx(1.d0/det * Ainv, kind=4)

    return
  end subroutine matrix_inverse_2x2_simple_complex
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 2x2 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_2x2_simple_real(A,Ainv,ctx_err)
    implicit none

    real(kind=4)       ,intent(in)     :: A   (:,:)
    real(kind=4)       ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    real(kind=8) :: det

    Ainv = 0.d0

    !! compute determinant
    det = dble(A(1,1)*A(2,2) - A(1,2)*A(2,1))
    if(abs(det) < tiny(1.0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 2x2 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_2x2]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) = A(2,2)
    Ainv(1,2) =-A(1,2)
    Ainv(2,1) =-A(2,1)
    Ainv(2,2) = A(1,1)
    Ainv      =real(1.d0/det * Ainv,kind=4)

    return
  end subroutine matrix_inverse_2x2_simple_real

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 3x3 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_3x3_double_complex(A,Ainv,ctx_err)
    implicit none

    complex(kind=8)    ,intent(in)     :: A   (:,:)
    complex(kind=8)    ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    complex(kind=8) :: det

    Ainv = 0.d0
    
    !! compute determinant
    det = A(1,1)*A(2,2)*A(3,3) + A(1,2)*A(2,3)*A(3,1) + A(1,3)*A(2,1)*A(3,2) &
        - A(1,3)*A(2,2)*A(3,1) - A(1,2)*A(2,1)*A(3,3) - A(1,1)*A(2,3)*A(3,2)
    if(abs(det) < tiny(1.d0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 3x3 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_3x3]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) = 1.d0/det * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    Ainv(1,2) =-1.d0/det * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    Ainv(1,3) = 1.d0/det * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    Ainv(2,1) =-1.d0/det * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    Ainv(2,2) = 1.d0/det * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    Ainv(2,3) =-1.d0/det * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    Ainv(3,1) = 1.d0/det * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    Ainv(3,2) =-1.d0/det * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    Ainv(3,3) = 1.d0/det * (A(1,1)*A(2,2) - A(1,2)*A(2,1))

    return
  end subroutine matrix_inverse_3x3_double_complex
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 3x3 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_3x3_simple_complex(A,Ainv,ctx_err)
    implicit none

    complex(kind=4)    ,intent(in)     :: A   (:,:)
    complex(kind=4)    ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    complex(kind=8) :: det

    Ainv = 0.d0
    
    !! compute determinant
    det = A(1,1)*A(2,2)*A(3,3) + A(1,2)*A(2,3)*A(3,1) + A(1,3)*A(2,1)*A(3,2) &
        - A(1,3)*A(2,2)*A(3,1) - A(1,2)*A(2,1)*A(3,3) - A(1,1)*A(2,3)*A(3,2)
    if(abs(det) < tiny(1.0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 3x3 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_3x3]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) =cmplx( 1.d0/det * (A(2,2)*A(3,3) - A(2,3)*A(3,2)),kind=4)
    Ainv(1,2) =cmplx(-1.d0/det * (A(2,1)*A(3,3) - A(2,3)*A(3,1)),kind=4)
    Ainv(1,3) =cmplx( 1.d0/det * (A(2,1)*A(3,2) - A(2,2)*A(3,1)),kind=4)
    Ainv(2,1) =cmplx(-1.d0/det * (A(1,2)*A(3,3) - A(1,3)*A(3,2)),kind=4)
    Ainv(2,2) =cmplx( 1.d0/det * (A(1,1)*A(3,3) - A(1,3)*A(3,1)),kind=4)
    Ainv(2,3) =cmplx(-1.d0/det * (A(1,1)*A(3,2) - A(1,2)*A(3,1)),kind=4)
    Ainv(3,1) =cmplx( 1.d0/det * (A(1,2)*A(2,3) - A(1,3)*A(2,2)),kind=4)
    Ainv(3,2) =cmplx(-1.d0/det * (A(1,1)*A(2,3) - A(1,3)*A(2,1)),kind=4)
    Ainv(3,3) =cmplx( 1.d0/det * (A(1,1)*A(2,2) - A(1,2)*A(2,1)),kind=4)

    return
  end subroutine matrix_inverse_3x3_simple_complex

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 3x3 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_3x3_double_real(A,Ainv,ctx_err)
    implicit none

    real(kind=8)       ,intent(in)     :: A   (:,:)
    real(kind=8)       ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    real(kind=8) :: det

    Ainv = 0.d0
    
    !! compute determinant
    det = A(1,1)*A(2,2)*A(3,3) + A(1,2)*A(2,3)*A(3,1) + A(1,3)*A(2,1)*A(3,2) &
        - A(1,3)*A(2,2)*A(3,1) - A(1,2)*A(2,1)*A(3,3) - A(1,1)*A(2,3)*A(3,2)
    if(abs(det) < tiny(1.d0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 3x3 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_3x3]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) = 1.d0/det * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    Ainv(1,2) =-1.d0/det * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    Ainv(1,3) = 1.d0/det * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    Ainv(2,1) =-1.d0/det * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    Ainv(2,2) = 1.d0/det * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    Ainv(2,3) =-1.d0/det * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    Ainv(3,1) = 1.d0/det * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    Ainv(3,2) =-1.d0/det * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    Ainv(3,3) = 1.d0/det * (A(1,1)*A(2,2) - A(1,2)*A(2,1))

    return
  end subroutine matrix_inverse_3x3_double_real
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> inverse a 3x3 matrix
  !
  !> @param[in]    A              : matrix
  !> @param[in]    Ainv           : inverse matrix
  !> @param[inout] ctx_err        : context error
  !---------------------------------------------------------------------
  subroutine matrix_inverse_3x3_simple_real(A,Ainv,ctx_err)
    implicit none

    real(kind=4)       ,intent(in)     :: A   (:,:)
    real(kind=4)       ,intent(inout)  :: Ainv(:,:)
    type(t_error)      ,intent(inout)  :: ctx_err
    !! local
    real(kind=8) :: det

    Ainv = 0.d0
    
    !! compute determinant
    det = A(1,1)*A(2,2)*A(3,3) + A(1,2)*A(2,3)*A(3,1) + A(1,3)*A(2,1)*A(3,2) &
        - A(1,3)*A(2,2)*A(3,1) - A(1,2)*A(2,1)*A(3,3) - A(1,1)*A(2,3)*A(3,2)
    if(abs(det) < tiny(1.0)) then
      ctx_err%ierr = -75
      ctx_err%msg  = "** ERROR: the 3x3 matrix is not invertible, its"//&
                     " determinant is zero. [matrix_inverse_3x3]"
      ctx_err%critic=.true.
      return
    end if
    
    !! compute Ainv
    Ainv(1,1) =real( 1.d0/det * (A(2,2)*A(3,3) - A(2,3)*A(3,2)),kind=4)
    Ainv(1,2) =real(-1.d0/det * (A(2,1)*A(3,3) - A(2,3)*A(3,1)),kind=4)
    Ainv(1,3) =real( 1.d0/det * (A(2,1)*A(3,2) - A(2,2)*A(3,1)),kind=4)
    Ainv(2,1) =real(-1.d0/det * (A(1,2)*A(3,3) - A(1,3)*A(3,2)),kind=4)
    Ainv(2,2) =real( 1.d0/det * (A(1,1)*A(3,3) - A(1,3)*A(3,1)),kind=4)
    Ainv(2,3) =real(-1.d0/det * (A(1,1)*A(3,2) - A(1,2)*A(3,1)),kind=4)
    Ainv(3,1) =real( 1.d0/det * (A(1,2)*A(2,3) - A(1,3)*A(2,2)),kind=4)
    Ainv(3,2) =real(-1.d0/det * (A(1,1)*A(2,3) - A(1,3)*A(2,1)),kind=4)
    Ainv(3,3) =real( 1.d0/det * (A(1,1)*A(2,2) - A(1,2)*A(2,1)),kind=4)

    return
  end subroutine matrix_inverse_3x3_simple_real
  
end module m_matrix_inverse
