!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_regularization.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for the regularization infos
!
!------------------------------------------------------------------------------
module m_print_regularization

  use m_ctx_parallelism,      only : t_parallelism
  use m_allreduce_max,        only : allreduce_max
  use m_raise_error,          only : t_error, raise_error
  use m_tag_regularization,   only : regularization_tag_naming
  use m_print_basics,         only : separator,indicator,cast_memory
  implicit none
  
  private
  public :: print_regularization_info
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the regularization
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   n_model         : number of models inverted
  !> @param[in]   param_name      : name of all parameters
  !> @param[in]   flag_regul      : regularization or not 
  !> @param[in]   tag_method      : regularization method
  !> @param[in]   str_weight      : regularization weighting method
  !> @param[in]   weight          : regularization weights
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine print_regularization_info(ctx_paral,flag_regul,n_regul,    &
                                       param_name,tag_method,str_weight,&
                                       weight,mem_peak,ctx_err)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: n_regul
    character(len=*),allocatable  ,intent(in)   :: param_name(:)
    character(len=*),allocatable  ,intent(in)   :: str_weight(:)
    logical                       ,intent(in)   :: flag_regul
    integer         ,allocatable  ,intent(in)   :: tag_method(:)
    real(kind=8)    ,allocatable  ,intent(in)   :: weight(:)
    integer(kind=8)               ,intent(in)   :: mem_peak
    type(t_error)                 ,intent(out)  :: ctx_err
    !!
    character(len=128) :: str
    character(len=64)  :: str_mem
    integer(kind=8)    :: mem_max
    integer            :: k

    ctx_err%ierr=0

    call allreduce_max(mem_peak,mem_max,1,ctx_paral%communicator,ctx_err)
    !! for all model, we indicate the function
    if(.not. flag_regul) then
      if(ctx_paral%master) write(6,'(a)') "- No Regularization applied "
    else
      
      call cast_memory(mem_max,str_mem)
      do k=1,n_regul
        call regularization_tag_naming(tag_method(k),param_name(k),str,ctx_err)
        if(ctx_paral%master) then 
          write(6,'(2a)')   indicator, " Regularization information  "
          write(6,'(a)') "- " // trim(adjustl(str))
          write(6,'(a,es10.3)') "- " // trim(adjustl(str_weight(k))) // &
                                " weight method, coeff=",weight(k)
          write(6,'(2a)') "- max memory per core        : ",trim(adjustl(str_mem))
        end if
      end do
    end if
    
    return
  end subroutine print_regularization_info

end module m_print_regularization
