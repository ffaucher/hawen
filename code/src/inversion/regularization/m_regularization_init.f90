!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_regularization_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize the regularization information
!
!------------------------------------------------------------------------------
module m_regularization_init
  
  use m_raise_error,                only : raise_error, t_error
  use m_ctx_parallelism,            only : t_parallelism
  use m_read_parameters_regularization, only : read_parameters_regularization
  use m_ctx_gradient,               only : t_gradient
  use m_ctx_regularization,         only : t_regularization
  use m_print_regularization,       only : print_regularization_info
  use m_tag_regularization,         only : regularization_tag_formalism

  implicit none

  private  
  public :: regularization_init
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init the regularization context information
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[inout] ctx_regul        : regularization context
  !> @param[in]   ctx_grad         : gradient context
  !> @param[inout] parameter_file   : parameter file
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine regularization_init(ctx_paral,ctx_regul,ctx_grad,          &
                                 parameter_file,ctx_err)
  
    implicit none

    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_regularization) ,intent(inout):: ctx_regul
    type(t_gradient)       ,intent(in)   :: ctx_grad
    character(len=*)       ,intent(in)   :: parameter_file
    type(t_error)          ,intent(out)  :: ctx_err
    !! -----------------------------------------------------------------
    character(len=32)  ,allocatable:: str(:)
    character(len=32)  ,allocatable:: str_weight(:)
    real(kind=8)       ,allocatable:: coeff_weight(:)
    integer                        :: k
    integer            ,parameter  :: rkind=4
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0

    !! get some infos from gradient
    ctx_regul%n_coeff_loc     = ctx_grad%n_coeff_loc
    ctx_regul%n_coeff_gb      = ctx_grad%n_coeff_gb 
    ctx_regul%n_param         = ctx_grad%n_grad
    
    !! read if regularization is applied and the method(s)
    allocate(str         (ctx_regul%n_param))
    allocate(str_weight  (ctx_regul%n_param))
    allocate(coeff_weight(ctx_regul%n_param))
    call read_parameters_regularization(parameter_file,ctx_regul%n_param, &
                                        ctx_regul%flag,str,str_weight,    &
                                        coeff_weight,ctx_err)
                                        
    !! if not applied, reset param >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if( .not. ctx_regul%flag) then
      ctx_regul%n_coeff_loc     = 0
      ctx_regul%n_coeff_gb      = 0
      ctx_regul%n_param         = 0
    else !! allocate
      ctx_regul%part_misfit = 0.d0
      allocate(ctx_regul%part_grad (ctx_regul%n_coeff_loc,ctx_regul%n_param))
      allocate(ctx_regul%weight    (ctx_regul%n_coeff_loc,ctx_regul%n_param))
      allocate(ctx_regul%param_name(ctx_regul%n_param))
      allocate(ctx_regul%tag       (ctx_regul%n_param))
      ctx_regul%part_grad = 0.d0    
      ctx_regul%weight    = 0.d0    
      ctx_regul%param_name= ctx_grad%param_name(1:ctx_regul%n_param)

      !! we convert the input names in TAG, depending on the method
      call regularization_tag_formalism(ctx_regul%n_param,str,          &
                                        ctx_regul%tag,ctx_err)
      
      !! initialize weight >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      do k=1,ctx_regul%n_param
        select case(trim(adjustl(str_weight(k))))
          case('constant','CONSTANT','Constant')
            if(coeff_weight(k) > tiny(1.0)) then
              ctx_regul%weight(:,k) = real(coeff_weight(k))
            else
            ctx_err%msg   = "** ERROR: weight for regularization "  //  &
                            " must be > 0 [regularization_init]**"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        
            end if
          case default
            ctx_err%msg   = "** ERROR: Unrecognized regularization "  //&
                            " weighting method: "//                     &
                            trim(adjustl(str_weight(k)))//              &
                            " [regularization_init]**"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        
        end select
      end do
      
      !! estimation of the memory peak in the compute subroutine 
      ctx_regul%mem_peak =                                       &
                 ctx_regul%n_coeff_loc*ctx_regul%n_param*rkind + & !! part_grad
                 ctx_regul%n_coeff_loc*ctx_regul%n_param*rkind + & !! weight
                 ctx_regul%n_coeff_loc                  *rkind + & !! m_temp
                 ctx_regul%n_coeff_gb                   *rkind + & !! mgb_loc
                 ctx_regul%n_coeff_gb                   *rkind + & !! mgb_all
                 ctx_regul%n_coeff_loc                  *rkind + & !! m_loc
                 !! assuming 3D at most
                 ctx_regul%n_coeff_loc*3                *rkind + & !! m_deriv
                 ctx_regul%n_coeff_loc                  *rkind + & !! m_work
                 ctx_regul%n_coeff_loc*3                *rkind + & !! eta_grad
                 ctx_regul%n_coeff_loc*3                *rkind     !! eta_grad_work

    end if

    !! print infos
    call print_regularization_info(ctx_paral,ctx_regul%flag,            &
                                   ctx_regul%n_param,                   &
                                   ctx_regul%param_name,ctx_regul%tag,  &
                                   str_weight,coeff_weight,             &
                                   ctx_regul%mem_peak,ctx_err)
    deallocate(str         )
    deallocate(str_weight  )
    deallocate(coeff_weight)

    return
  end subroutine regularization_init

end module m_regularization_init
