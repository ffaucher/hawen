!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_regularization.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the context for the regularization parameter.
!> it contains the tag for the possibilities (e.g., TV and Tikhonov)
!
!------------------------------------------------------------------------------
module m_ctx_regularization

  implicit none

  private
  public :: t_regularization, regularization_clean
  public :: regularization_copy_ctx, regularization_reset

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_regularization contains all information for the regularization
  !
  !> @param[logical] flag         : is there regularization or not
  !> @param[integer] n_param      : how many parameter to regularize
  !> @param[integer] tag          : method for regularization
  !> @param[integer] n_coeff      : number of coefficients per weights
  !> @param[real]    weight       : regularization parameter
  !> @param[real]    misfit       : misfit part for the regularization
  !> @param[real]    grad         : gradient part for the regularization
  !
  !----------------------------------------------------------------------------
  type t_regularization

    !! regularization tag and flag, one for each parameter
    logical                       :: flag
    integer                       :: n_param
    integer, allocatable          :: tag(:)
    character(len=32), allocatable:: param_name(:)
    integer(kind=8)               :: mem_peak

    !! regularization weight: size(n_coeff_loc, n_param)
    integer                       :: n_coeff_loc
    integer                       :: n_coeff_gb
    real   , allocatable          :: weight(:,:)
    !! corresponding information for the regularization:
    !! increment of misfit function and local gradient part
    real(kind=8)                  :: part_misfit
    real   , allocatable          :: part_grad(:,:)

  end type t_regularization
  !----------------------------------------------------------------------------

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> copy regularization context
  !
  !> @param[inout] ctx_regularization  : regularization context
  !----------------------------------------------------------------------------
  subroutine regularization_copy_ctx(ctx_regul_in,ctx_regul_out)

    implicit none

    type(t_regularization)    ,intent(in)     :: ctx_regul_in
    type(t_regularization)    ,intent(inout)  :: ctx_regul_out

    ctx_regul_out%flag       = ctx_regul_in%flag
    ctx_regul_out%n_param    = ctx_regul_in%n_param
    ctx_regul_out%n_coeff_loc= ctx_regul_in%n_coeff_loc
    ctx_regul_out%n_coeff_gb = ctx_regul_in%n_coeff_gb
    ctx_regul_out%part_misfit= ctx_regul_in%part_misfit
    ctx_regul_out%mem_peak   = ctx_regul_in%mem_peak

    if(allocated(ctx_regul_in%tag))       then
      allocate(ctx_regul_out%tag(ctx_regul_out%n_param))
      ctx_regul_out%tag(:) = ctx_regul_in%tag(:)
    end if
    if(allocated(ctx_regul_in%weight))    then
      allocate(ctx_regul_out%weight(ctx_regul_in%n_coeff_loc,ctx_regul_out%n_param))
      ctx_regul_out%weight(:,:) = ctx_regul_in%weight(:,:)
    end if
    if(allocated(ctx_regul_in%part_grad)) then
      allocate(ctx_regul_out%part_grad(ctx_regul_in%n_coeff_loc,ctx_regul_out%n_param))
      ctx_regul_out%part_grad(:,:) = ctx_regul_in%part_grad(:,:)
    end if
    if(allocated(ctx_regul_in%param_name)) then
      allocate(ctx_regul_out%param_name(ctx_regul_out%n_param))
      ctx_regul_out%param_name(:) = ctx_regul_in%param_name(:)
    end if

    return
  end subroutine regularization_copy_ctx

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> reset regularization information, i.e. set to 0
  !
  !> @param[inout] ctx_regul      : regul context
  !----------------------------------------------------------------------------
  subroutine regularization_reset(ctx_regul)

    implicit none

    type(t_regularization)    ,intent(inout)  :: ctx_regul

    ctx_regul%part_misfit = 0.d0
    if(allocated(ctx_regul%part_grad)) ctx_regul%part_grad=0.0

    return
  end subroutine regularization_reset

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean regularization information and allocation
  !
  !> @param[inout] ctx_regul      : regul context
  !----------------------------------------------------------------------------
  subroutine regularization_clean(ctx_regul)

    implicit none

    type(t_regularization)    ,intent(inout)  :: ctx_regul

    ctx_regul%flag       = .false.
    ctx_regul%n_param    = 0
    ctx_regul%n_coeff_loc= 0
    ctx_regul%n_coeff_gb = 0
    ctx_regul%part_misfit= 0.d0
    ctx_regul%mem_peak   = 0

    if(allocated(ctx_regul%tag))       deallocate(ctx_regul%tag)
    if(allocated(ctx_regul%weight))    deallocate(ctx_regul%weight)
    if(allocated(ctx_regul%part_grad)) deallocate(ctx_regul%part_grad)
    if(allocated(ctx_regul%param_name))deallocate(ctx_regul%param_name)

    return
  end subroutine regularization_clean

end module m_ctx_regularization
