!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_regularization_compute.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the selected regularization to enrich
!> the gradient of the cost function
!
!------------------------------------------------------------------------------
module m_regularization_compute

  !! module used -------------------------------------------------------
  use omp_lib
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: RKIND_MESH,IKIND_MESH
  use m_distribute_error,       only: distribute_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_sum,          only: allreduce_sum
  use m_allreduce_max,          only: allreduce_max
  use m_ctx_domain,             only: t_domain
  use m_mesh_simplex_area,      only: mesh_area_simplex
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_get_piecewise_constant
  use m_project_array,          only: project_array_local_cell_to_global, &
                                      project_array_global_cell_to_local
  use m_ctx_regularization,     only: t_regularization
  use m_tag_regularization
  use m_domain_derivative_model,only: model_domain_divergence,model_domain_gradient
  use m_ctx_model_representation,                                       &
                                only: tag_MODEL_PCONSTANT

  implicit none

  private

  public   :: regularization_compute

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the regularization, and consequently update the gradient
  !>    Total-Variation:            \int_\Omega     |\grad(m)|  d\Omega
  !>    Tikhonov       :            \int_\Omega  0.5|\grad(m)|² d\Omega
  !>
  !> the gradient is given by
  !>  TV:   \div( |\grad(m)|^{-1}  \grad( m ) )
  !>  TK:   \div(                  \grad( m ) )
  !>
  !! -------------------------------------------------------------------
  !
  !> @param[in]    ctx_paral     : ctx_paral
  !> @param[in]    ctx_domain    : domain context
  !> @param[in]    ctx_model     : model context
  !> @param[in]    ctx_reful     : regularization context
  !> @param[in]    freq          : frequency
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine regularization_compute(ctx_paral,ctx_domain,ctx_model,     &
                                    ctx_regul,freq,ctx_err)

    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    type(t_domain)                ,intent(in)   :: ctx_domain
    type(t_model)                 ,intent(in)   :: ctx_model
    type(t_regularization)        ,intent(inout):: ctx_regul
    complex(kind=8)               ,intent(in)   :: freq
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! in case regularization is not applied but we still end up here...
    if(.not. ctx_regul%flag) return

    select case(trim(adjustl(ctx_model%format_disc)))
      case(tag_MODEL_PCONSTANT) 
        call regularization_compute_pconstant(ctx_paral,ctx_domain,     &
                                              ctx_model,ctx_regul,      &
                                              freq,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: only piecewise-constant model " //&
                        " supported [regularization_compute] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
  
  end subroutine regularization_compute



  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the regularization for piecewise-constant models
  !> it also updates the gradient here.
  !>    Total-Variation:            \int_\Omega     |\grad(m)|  d\Omega
  !>    Tikhonov       :            \int_\Omega  0.5|\grad(m)|² d\Omega
  !>
  !> the gradient is given by
  !>  TV:   \div( |\grad(m)|^{-1}  \grad( m ) )
  !>  TK:   \div(                  \grad( m ) )
  !>
  !! -------------------------------------------------------------------
  !
  !> @param[in]    ctx_paral     : ctx_paral
  !> @param[in]    ctx_domain    : domain context
  !> @param[in]    ctx_model     : model context
  !> @param[in]    ctx_reful     : regularization context
  !> @param[in]    freq          : frequency
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine regularization_compute_pconstant(ctx_paral,ctx_domain,     &
                                              ctx_model,ctx_regul,freq, &
                                              ctx_err)

    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    type(t_domain)                ,intent(in)   :: ctx_domain
    type(t_model)                 ,intent(in)   :: ctx_model
    type(t_regularization)        ,intent(inout):: ctx_regul
    complex(kind=8)               ,intent(in)   :: freq
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=4),allocatable     :: m_temp(:),m_gb_loc(:),m_work(:)
    real(kind=4),allocatable     :: m_loc(:),m_deriv(:,:),m_gb_all(:)
    real(kind=4),allocatable     :: eta_grad(:,:),eta_grad_work(:,:)
    integer                      :: i_param,i_dim
    integer(kind=IKIND_MESH)     :: i_cell
    real(kind=8)                 :: tol !! for division, depends on max
    real(kind=8)                 :: misfit_param,max_loc,max_gb,area    
    real(kind=RKIND_MESH),allocatable  :: coo_node(:,:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! in case regularization is not applied but we still end up here...
    if(.not. ctx_regul%flag) return

    !! we need the current model and its directional derivative
    allocate(m_temp  (ctx_domain%n_cell_loc))
    allocate(m_gb_loc(ctx_domain%n_cell_gb))
    allocate(m_gb_all(ctx_domain%n_cell_gb))
    allocate(m_loc   (ctx_domain%n_cell_loc+ctx_domain%n_ghost_loc))
    allocate(m_deriv (ctx_domain%n_cell_loc,ctx_domain%dim_domain))
    allocate(m_work  (ctx_domain%n_cell_loc))
    allocate(eta_grad(ctx_domain%n_cell_loc,ctx_domain%dim_domain))
    allocate(eta_grad_work(ctx_domain%n_cell_loc + &
                           ctx_domain%n_ghost_loc,ctx_domain%dim_domain))
    allocate(coo_node(ctx_domain%dim_domain,ctx_domain%n_neigh_per_cell))
    ctx_regul%part_misfit = 0.d0
    ctx_regul%part_grad   = 0.d0
    misfit_param          = 0.d0

    !! loop over all parameters to invert
    do i_param = 1, ctx_regul%n_param

      m_temp         = 0.d0
      m_gb_loc       = 0.d0
      m_gb_all       = 0.d0
      m_loc          = 0.d0
      m_deriv        = 0.d0
      m_work         = 0.d0
      eta_grad       = 0.d0
      eta_grad_work  = 0.d0


      ! ----------------------------------------------------------------
      !! 0) initialize local array depending on the method.
      !!    Total-Variation:            \int_\Omega     |\grad(m)|  d\Omega
      !!    Tikhonov       :            \int_\Omega  0.5|\grad(m)|² d\Omega
      !!
      !! the gradient is given by
      !!  TV:   \div( |\grad(m)|^{-1}  \grad( m ) )
      !!  TK:   \div(                  \grad( m ) )
      !!
      ! ----------------------------------------------------------------
      select case(ctx_regul%tag(i_param))
        case(tag_REGULARIZATION_TV,tag_REGULARIZATION_TV_x,              &
             tag_REGULARIZATION_TV_z,tag_REGULARIZATION_TIKHONOV,        &
             tag_REGULARIZATION_TIKHONOV_x,tag_REGULARIZATION_TIKHONOV_y,&
             tag_REGULARIZATION_TIKHONOV_z,tag_REGULARIZATION_TV_y)

          call model_get_piecewise_constant(ctx_model,m_temp,           &
                         ctx_regul%param_name(i_param),freq,ctx_err)
          !! extend model with ghost infos
          call project_array_local_cell_to_global(ctx_domain,m_temp,    &
                                                  m_gb_loc,ctx_err)

          !! all reduce
          call allreduce_sum(m_gb_loc,m_gb_all,                         &
                             int(ctx_domain%n_cell_gb),                 &
                             ctx_paral%communicator,ctx_err)

          call project_array_global_cell_to_local(ctx_domain,m_gb_all,  &
                                                  m_loc,ctx_err,        &
                                                  o_flag_ghost=.true.)

          !! compute its directional derivative
          call model_domain_gradient(ctx_domain,ctx_model%format_disc,  &
                                     m_loc,m_deriv,ctx_err)

        case (tag_REGULARIZATION_IGNORE)
          cycle

        case default
          !! Remark that tag_SEARCH_IGNORE should not appear here.
          ctx_err%msg   = "** ERROR: Unrecognized regularization " //   &
                          "method [regularization_compute]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

      ! ----------------------------------------------------------------
      !! 1) update the misfit function and compute the norm model grad.
      !!    the misfit incorporates and integral so we must divide
      !!    by the area of the cells
      ! ----------------------------------------------------------------
      !! sum the derivative, multiply by the weight and compute the norm
      select case(ctx_regul%tag(i_param))
        !! -------------------------------------------------------------
        case(tag_REGULARIZATION_TV)             !! |GRAD(m)|
          do i_cell = 1, ctx_domain%n_cell_loc
            do i_dim = 1, ctx_domain%dim_domain
              m_work(i_cell) = m_work(i_cell) + m_deriv(i_cell,i_dim)**2
            end do
            m_work(i_cell) = sqrt(m_work(i_cell))
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                           ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        case(tag_REGULARIZATION_TV_x)
          i_dim=1
          !! set to zero other derivatives
          if(ctx_domain%dim_domain .ne. 1) &
            m_deriv(:,2:ctx_domain%dim_domain) = 0.d0

          do i_cell = 1, ctx_domain%n_cell_loc
            m_work(i_cell) = m_work(i_cell) + m_deriv(i_cell,i_dim)**2
            m_work(i_cell) = sqrt(m_work(i_cell))
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                             ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        case(tag_REGULARIZATION_TV_y)
          if(ctx_domain%dim_domain < 3) then
            ctx_err%msg   = "** ERROR: Cannot apply Y-derivative if not"//&
                            " three dimension [regularization_compute]**"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          i_dim=2
          !! set to zero other derivatives
          m_deriv(:,1) = 0.d0
          m_deriv(:,3) = 0.d0


          do i_cell = 1, ctx_domain%n_cell_loc
            m_work(i_cell) = m_work(i_cell) + m_deriv(i_cell,i_dim)**2
            m_work(i_cell) = sqrt(m_work(i_cell))
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                          ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        case(tag_REGULARIZATION_TV_z)
          if(ctx_domain%dim_domain < 2) then
            ctx_err%msg   = "** ERROR: Cannot apply Z-derivative if not"//&
                            " two/three dimensions [regularization_compute]**"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          if(ctx_domain%dim_domain == 3) then
            i_dim = 3
            !! set to zero other derivatives
            m_deriv(:,1:2) = 0.d0
          else
            i_dim=2
            !! set to zero other derivatives
            m_deriv(:,1) = 0.d0
          end if


          do i_cell = 1, ctx_domain%n_cell_loc
            m_work(i_cell) = m_work(i_cell) + m_deriv(i_cell,i_dim)**2
            m_work(i_cell) = sqrt(m_work(i_cell))
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                          ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        !! -------------------------------------------------------------
        case(tag_REGULARIZATION_TIKHONOV)       !! 1./2. |GRAD(m)|²

          do i_cell = 1, ctx_domain%n_cell_loc
            do i_dim = 1, ctx_domain%dim_domain
              m_work(i_cell) = m_work(i_cell) + 1./2.*m_deriv(i_cell,i_dim)**2
            end do
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                          ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        case(tag_REGULARIZATION_TIKHONOV_x)
          i_dim=1
          !! set to zero other derivatives
          if(ctx_domain%dim_domain .ne. 1) &
            m_deriv(:,2:ctx_domain%dim_domain) = 0.d0

          do i_cell = 1, ctx_domain%n_cell_loc
            m_work(i_cell) = m_work(i_cell) + 1./2.*m_deriv(i_cell,i_dim)**2
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                          ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        case(tag_REGULARIZATION_TIKHONOV_y)
          if(ctx_domain%dim_domain < 3) then
            ctx_err%msg   = "** ERROR: Cannot apply Y-derivative if not"//&
                            " three dimension [regularization_compute]**"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          i_dim=2
          !! set to zero other derivatives
          m_deriv(:,1) = 0.d0
          m_deriv(:,3) = 0.d0

          do i_cell = 1, ctx_domain%n_cell_loc
            m_work(i_cell) = m_work(i_cell) + 1./2.*m_deriv(i_cell,i_dim)**2
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                          ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        case(tag_REGULARIZATION_TIKHONOV_z)
          if(ctx_domain%dim_domain < 2) then
            ctx_err%msg   = "** ERROR: Cannot apply Z-derivative if not"//&
                            " three dimension [regularization_compute]**"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          if(ctx_domain%dim_domain == 3) then
            i_dim = 3
            !! set to zero other derivatives
            m_deriv(:,1:2) = 0.d0
          else
            i_dim=2
            !! set to zero other derivatives
            m_deriv(:,1) = 0.d0
          end if

          do i_cell = 1, ctx_domain%n_cell_loc
            m_work(i_cell) = m_work(i_cell) + 1./2.*m_deriv(i_cell,i_dim)**2
            coo_node=ctx_domain%x_node(:,ctx_domain%cell_node(:,i_cell))
            call mesh_area_simplex(ctx_domain%dim_domain,coo_node,      &
                                   area,ctx_err)
            misfit_param = misfit_param +  1./area *                    &
                          ctx_regul%weight(i_cell,i_param)*m_work(i_cell)
          end do

        case default
          !! Remark that tag_SEARCH_IGNORE should not appear here.
          ctx_err%msg   = "** ERROR: Unrecognized regularization " //   &
                          "method [regularization_compute]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

      ! ----------------------------------------------------------------
      !! 2) update the gradient.
      !! see above for their definition
      ! ----------------------------------------------------------------
      !! the tolerance for the diffusion depends on the maximal value
      !! of the norm
      max_loc = maxval(abs(m_work))
      call allreduce_max(max_loc,max_gb,1,ctx_paral%communicator,ctx_err)
      tol = max_gb * 1e-6

      !! 2a) multiply the model derivative by \eta
      !! ---------------------------------------------------------------
      select case(ctx_regul%tag(i_param))
        !! -------------------------------------------------------------
        case(tag_REGULARIZATION_TV  , tag_REGULARIZATION_TV_x,          &
             tag_REGULARIZATION_TV_z, tag_REGULARIZATION_TV_y)

          do i_cell = 1, ctx_domain%n_cell_loc
            !! must not be below some arbitrary criterion
            if(m_work(i_cell) > tol) then !! too small ?
              eta_grad(i_cell,:) = 1.0 / m_work(i_cell) * m_deriv(i_cell,:)
            end if
          end do
        case(tag_REGULARIZATION_TIKHONOV  , tag_REGULARIZATION_TIKHONOV_x,&
             tag_REGULARIZATION_TIKHONOV_z, tag_REGULARIZATION_TIKHONOV_y)
          eta_grad(:,:) = m_deriv(:,:)

        case default
          !! Remark that tag_SEARCH_IGNORE should not appear here.
          ctx_err%msg   = "** ERROR: Unrecognized regularization " //   &
                          "method [regularization_compute]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

      ! ----------------------------------------------------------------
      !! 2b) extract the divergence of the array eta_grad
      !! ---------------------------------------------------------------
      m_temp  = 0.d0
      m_gb_loc= 0.d0
      m_gb_all= 0.d0
      m_loc   = 0.d0
      !! loop over dimensions
      do i_dim = 1,ctx_domain%dim_domain
        !! extract array + ghost for each dimension
        m_temp(:) = eta_grad(:,i_dim)
        call project_array_local_cell_to_global(ctx_domain,m_temp,      &
                                                m_gb_loc,ctx_err)
        !! all reduce
        call allreduce_sum(m_gb_loc,m_gb_all,int(ctx_domain%n_cell_gb), &
                           ctx_paral%communicator,ctx_err)

        call project_array_global_cell_to_local(ctx_domain,m_gb_all,    &
                                                m_loc,ctx_err,          &
                                                o_flag_ghost=.true.)
        eta_grad_work(:,i_dim) = m_loc(:)
      end do
      !! compute the divergence
      call model_domain_divergence(ctx_domain,ctx_model%format_disc,    &
                                   eta_grad_work,                       &
                                   ctx_regul%part_grad(:,i_param),ctx_err)
      !! multiply by the weight
      do i_cell = 1, ctx_domain%n_cell_loc
        ctx_regul%part_grad(i_cell,i_param) =                           &
        ctx_regul%part_grad(i_cell,i_param) * ctx_regul%weight(i_cell,i_param)
      end do


    end do
    !! -----------------------------------------------------------------

    !! sum all contributions for the misfits
    ctx_regul%part_misfit = 0.d0
    call allreduce_sum(misfit_param,ctx_regul%part_misfit,1,            &
                       ctx_paral%communicator,ctx_err)
    deallocate(m_temp       )
    deallocate(m_gb_loc     )
    deallocate(m_gb_all     )
    deallocate(m_loc        )
    deallocate(m_deriv      )
    deallocate(m_work       )
    deallocate(eta_grad     )
    deallocate(eta_grad_work)

    return
  end subroutine regularization_compute_pconstant

end module m_regularization_compute
