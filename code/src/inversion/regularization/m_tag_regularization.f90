!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_tag_regularization.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the function of the Regularization
!
!------------------------------------------------------------------------------
module m_tag_regularization

  use m_raise_error,              only : raise_error, t_error

  implicit none

  !>
  integer, parameter :: tag_REGULARIZATION_IGNORE = 0 !!   nothing

  integer, parameter :: tag_REGULARIZATION_TV        = 10 !! Total-Variation
  integer, parameter :: tag_REGULARIZATION_TV_x      = 11 !! Total-Variation
  integer, parameter :: tag_REGULARIZATION_TV_y      = 12 !! Total-Variation
  integer, parameter :: tag_REGULARIZATION_TV_z      = 13 !! Total-Variation
  integer, parameter :: tag_REGULARIZATION_TIKHONOV  = 20 !! Tikhonov
  integer, parameter :: tag_REGULARIZATION_TIKHONOV_x= 21 !! Tikhonov
  integer, parameter :: tag_REGULARIZATION_TIKHONOV_y= 22 !! Tikhonov
  integer, parameter :: tag_REGULARIZATION_TIKHONOV_z= 23 !! Tikhonov
  private
  
  public :: regularization_tag_formalism, regularization_tag_naming
  public :: tag_REGULARIZATION_TV  , tag_REGULARIZATION_TIKHONOV
  public :: tag_REGULARIZATION_TV_x, tag_REGULARIZATION_TIKHONOV_x
  public :: tag_REGULARIZATION_TV_y, tag_REGULARIZATION_TIKHONOV_y
  public :: tag_REGULARIZATION_TV_z, tag_REGULARIZATION_TIKHONOV_z
  public :: tag_REGULARIZATION_IGNORE

  contains 
    
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> decide the regularization tag from keyword
  !
  !> @param[in]   n_model       : number of model function
  !> @param[in]   name_func_in  : input name
  !> @param[out]  tag_func_out  : output tag
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine regularization_tag_formalism(n_model,name_func_in,tag_func_out,ctx_err)
    implicit none

    integer                            ,intent(in)   :: n_model
    character(len=*) ,allocatable      ,intent(in)   :: name_func_in(:)
    integer          ,allocatable      ,intent(inout):: tag_func_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer    :: k
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    do k=1,n_model
      select case(trim(adjustl(name_func_in(k))))
        case('total-variation','TOTAL-VARIATION','Total-Variation',     &
             'Total-variation','TV','tv','Tv')
          tag_func_out(k) = tag_REGULARIZATION_TV
        case('total-variation_x','TOTAL-VARIATION_x','Total-Variation_x',&
             'Total-variation_x','TV_x','tv_x','Tv_x')
          tag_func_out(k) = tag_REGULARIZATION_TV_x
        case('total-variation_z','TOTAL-VARIATION_z','Total-Variation_z',&
             'Total-variation_z','TV_z','tv_z','Tv_z')
          tag_func_out(k) = tag_REGULARIZATION_TV_z
        case('total-variation_y','TOTAL-VARIATION_y','Total-Variation_y',&
             'Total-variation_y','TV_y','tv_y','Tv_y')
          tag_func_out(k) = tag_REGULARIZATION_TV_y
        case('tikhonov','Tikhonov','TIKHONOV')
          tag_func_out(k) = tag_REGULARIZATION_TIKHONOV
        case('tikhonov_x','Tikhonov_x','TIKHONOV_x')
          tag_func_out(k) = tag_REGULARIZATION_TIKHONOV_x
        case('tikhonov_y','Tikhonov_y','TIKHONOV_y')
          tag_func_out(k) = tag_REGULARIZATION_TIKHONOV_y
        case('tikhonov_z','Tikhonov_z','TIKHONOV_z')
          tag_func_out(k) = tag_REGULARIZATION_TIKHONOV_z
        case default
          ctx_err%msg   = "** ERROR: Unrecognized regularization  " // &
                          "[regularization_tag_formalism]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end do
        
    return
    
  end subroutine regularization_tag_formalism

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> from the tag, we extract a word for printing
  !
  !> @param[in]   tag_in        : input tag
  !> @param[out]  model_name    : current model name
  !> @param[out]  str           : string out
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine regularization_tag_naming(tag_in,model_name,str,ctx_err)
    implicit none

    integer                            ,intent(in)   :: tag_in
    character(len=*)                   ,intent(in)   :: model_name
    character(len=128)                 ,intent(inout):: str
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    str = ''
    str = 'update '// trim(adjustl(model_name)) 
    str = 'Regularize '// trim(adjustl(model_name)) 
    select case(tag_in)
      case(tag_REGULARIZATION_IGNORE)
        str = trim(adjustl(model_name)) // ' not considered'
      case(tag_REGULARIZATION_TV)
        str = str(1:24) // ' with Total Variation'
      case(tag_REGULARIZATION_TV_x)
        str = str(1:24) // ' with Total Variation direction X only'
      case(tag_REGULARIZATION_TV_y)
        str = str(1:24) // ' with Total Variation direction Y only'
      case(tag_REGULARIZATION_TV_z)
        str = str(1:24) // ' with Total Variation direction Z only'
      case(tag_REGULARIZATION_TIKHONOV)
        str = str(1:24) // ' with Tikhonov'
      case(tag_REGULARIZATION_TIKHONOV_x)
        str = str(1:24) // ' with Tikhonov direction X only'
      case(tag_REGULARIZATION_TIKHONOV_y)
        str = str(1:24) // ' with Tikhonov direction Y only'
      case(tag_REGULARIZATION_TIKHONOV_z)
        str = str(1:24) // ' with Tikhonov direction Z only'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized Regularization "      //&
                        "[regularization_tag_naming]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
    
  end subroutine regularization_tag_naming

end module m_tag_regularization
