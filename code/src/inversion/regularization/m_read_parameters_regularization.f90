!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_regularization.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the regularization input parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_regularization
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_regularization
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters for the gradient
  !
  !> @param[in]   parameter_file  : parameter file where infos are
  !> @param[in]   n_model         : number of model to look for 
  !> @param[out]  regul_flag      : regularization or not 
  !> @param[out]  str_regul       : string to indicate the regularization
  !> @param[out]  str_weight      : string to indicate the weight method
  !> @param[out]  weight          : weight coefficient
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_regularization(parameter_file,n_model,     &
                             regularization_flag,str_regularization,    &
                             str_weight,weight_val,ctx_err)
    implicit none 
    
    character(len=*)             ,intent(in)   :: parameter_file
    integer                      ,intent(in)   :: n_model
    logical                      ,intent(out)  :: regularization_flag
    character(len=32),allocatable,intent(inout):: str_regularization(:)
    character(len=32),allocatable,intent(inout):: str_weight        (:)
    real(kind=8)     ,allocatable,intent(inout):: weight_val        (:)
    type(t_error)                ,intent(out)  :: ctx_err

    character(len=512)   :: str(512)
    character(len=512)   :: strtemp
    integer              :: k,nval
    real(kind=8)         :: rval(512)
    logical              :: llist(512)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    call read_parameter(parameter_file,'fwi_regularization',.false.,llist,nval)
    regularization_flag=llist(1)

    if(regularization_flag) then
      !! there are n_model to be found at most, if less are indicated,
      !! we repeat the last method
      call read_parameter(parameter_file,'fwi_regularization_method','',str,nval)
      if(nval < 1) then
        ctx_err%msg   = "** ERROR: missing regularization method [read_param]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      do k=1,n_model
        if(k<=nval) then
          strtemp = str(k)
        end if
        str_regularization(k) = trim(adjustl(strtemp(1:32)))
      end do

      !! Weighting method
      call read_parameter(parameter_file,'fwi_regularization_weight_method',&
                          '',str,nval)
      if(nval < 1) then
        ctx_err%msg   = "** ERROR: missing regularization weight method [read_param]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      do k=1,n_model
        if(k<=nval) then
          strtemp = str(k)
        end if
        str_weight(k) = trim(adjustl(strtemp(1:32)))
      end do
      
      !! Weighting coefficient
      call read_parameter(parameter_file,'fwi_regularization_weight',0.d0,rval,nval)
      if(nval < 1) then
        ctx_err%msg   = "** ERROR: missing regularization weight [read_param]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      do k=1,n_model
        if(k<=nval) then
          weight_val(k) = rval(k)
        else 
          weight_val(k) = weight_val(k-1)
        end if
      end do

    end if

    return
  end subroutine read_parameters_regularization

end module m_read_parameters_regularization
