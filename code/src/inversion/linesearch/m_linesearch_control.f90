!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_linesearch_goldstein.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to control the linesearch step, e.g. sufficient
!> decrease, Armijo and Goldstein condition
!>
!
!------------------------------------------------------------------------------
module m_linesearch_control
  
  use m_raise_error,                only : raise_error, t_error
  use m_ctx_parallelism,            only : t_parallelism
  use m_allreduce_sum,              only : allreduce_sum

  implicit none

  private  
  public :: linesearch_goldstein, linesearch_armijo
    
  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> compute the goldstein condition for a given tolerance K
  !> [p36, Nocedal & Wright].
  !> J(m) + (1-K) \alpha (s^T.grad(J)) 
  !>                 <= J(m +alpha s) <= J(m) + K \alpha (s^T.grad(J))
  !> 
  !
  !> @param[in]   ctx_paral     : parallel   context
  !> @param[inout] ctx_ls        : linesearch context
  !> @param[in]   parameter_file : parameter file
  !> @param[in]   n_model       : number of param
  !> @param[in]   n_group       : number of group
  !> @param[in]   param_name    : param name
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine linesearch_goldstein(ctx_paral,grad,search,alpha,criterion,&
                                  cost_gb_origin,cost_gb_linesearch,    &
                                  ncells_loc,flag_valid,step_evolve,    &
                                  ctx_err)
  
    implicit none

    type(t_parallelism)          ,intent(in)   :: ctx_paral
    real(kind=4)     ,allocatable,intent(in)   :: grad  (:)
    real(kind=4)     ,allocatable,intent(in)   :: search(:)
    real(kind=4)                 ,intent(in)   :: criterion, alpha
    real(kind=8)                 ,intent(in)   :: cost_gb_origin
    real(kind=8)                 ,intent(in)   :: cost_gb_linesearch
    integer                      ,intent(in)   :: ncells_loc
    logical                      ,intent(out)  :: flag_valid
    real                         ,intent(out)  :: step_evolve
    type(t_error)                ,intent(out)  :: ctx_err
    !!
    integer            :: ic
    real(kind=8)       :: coeff_loc,searchTgrad,upper_bound,lower_bound

    ctx_err%ierr = 0

    !! -----------------------------------------------------------------
    !! compute search^T.gradient
    !! -----------------------------------------------------------------
    coeff_loc = 0.0
    do ic=1,ncells_loc
      coeff_loc = coeff_loc + search(ic) * grad(ic)
    end do
    !! sum contributions from all sub-domains
    call allreduce_sum(coeff_loc,searchTgrad,1,ctx_paral%communicator,ctx_err)
    
    !! left and right-hand side
    !! the minus comes because we have x - alpha s.
    lower_bound = cost_gb_origin - (1-criterion)*alpha*searchTgrad
    upper_bound = cost_gb_origin -    criterion *alpha*searchTgrad

    !! give the values 
    if(ctx_paral%master) then
      write(6,'(a,es10.3,a,es10.3)') "    - Goldstein cond.: lower "//&
                                     "bound |   misfit    | upper bound "
    end if
    
    if(cost_gb_linesearch < lower_bound .or.                            &
       cost_gb_linesearch > upper_bound) then
      flag_valid = .false.
      if(ctx_paral%master)  write(6,'(a,es12.5,a,es12.5,a,es12.5)')     &
        "       NOT SATISFIED:",lower_bound, "  ", cost_gb_linesearch, &
                                              "  ",upper_bound
      !! The new step is computed from second order approximation, 
      !! following the paper of [Barucq, Faucher & Pham] in Journal
      !! of computational Physics: 2nd order interpolation update
      step_evolve = -real(dble(searchTgrad*alpha*alpha) /               &
                          dble(2*(cost_gb_linesearch - cost_gb_origin - &
                                  searchTgrad*alpha)))
    else
      flag_valid = .true.
      step_evolve= alpha
      if(ctx_paral%master)  write(6,'(a,es12.5,a,es12.5,a,es12.5)')     &
        "           SATISFIED:",lower_bound, "  ", cost_gb_linesearch, &
                                              "  ",upper_bound
    end if
    !! -----------------------------------------------------------------

    return

  end subroutine linesearch_goldstein
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> compute the Armijo condition for a given tolerance K: it is 
  !> a sufficient descent direction
  !> 
  !>             J(m +alpha s) <= J(m) + K \alpha (s^T.grad(J))
  !> 
  !
  !> @param[in]   ctx_paral     : parallel   context
  !> @param[inout] ctx_ls        : linesearch context
  !> @param[in]   parameter_file : parameter file
  !> @param[in]   n_model       : number of param
  !> @param[in]   n_group       : number of group
  !> @param[in]   param_name    : param name
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine linesearch_armijo(ctx_paral,grad,search,alpha,criterion,   &
                               cost_gb_origin,cost_gb_linesearch,       &
                               ncells_loc,flag_valid,step_evolve,       &
                               ctx_err)
  
    implicit none

    type(t_parallelism)          ,intent(in)   :: ctx_paral
    real(kind=4)     ,allocatable,intent(in)   :: grad  (:)
    real(kind=4)     ,allocatable,intent(in)   :: search(:)
    real(kind=4)                 ,intent(in)   :: criterion, alpha
    real(kind=8)                 ,intent(in)   :: cost_gb_origin
    real(kind=8)                 ,intent(in)   :: cost_gb_linesearch
    integer                      ,intent(in)   :: ncells_loc
    logical                      ,intent(out)  :: flag_valid
    real                         ,intent(out)  :: step_evolve
    type(t_error)                ,intent(out)  :: ctx_err
    !!
    integer            :: ic
    real(kind=8)       :: coeff_loc,searchTgrad,upper_bound

    ctx_err%ierr = 0

    !! -----------------------------------------------------------------
    !! compute search^T.gradient
    !! -----------------------------------------------------------------
    coeff_loc = 0.0
    do ic=1,ncells_loc
      coeff_loc = coeff_loc + search(ic) * grad(ic)
    end do
    !! sum contributions from all sub-domains
    call allreduce_sum(coeff_loc,searchTgrad,1,ctx_paral%communicator,ctx_err)
    
    !! left and right-hand side
    !! the minus comes because we have x - alpha s.
    upper_bound = cost_gb_origin -    criterion *alpha*searchTgrad

    !! give the values 
    if(ctx_paral%master) then
      write(6,'(a,es10.3,a,es10.3)') "    - Armijo cond. :  misfit " // &
                                     "    | upper bound "
    end if
    
    if(cost_gb_linesearch > upper_bound) then
      flag_valid = .false.
      if(ctx_paral%master)  write(6,'(a,es12.5,a,es12.5)')     &
        "      NOT SATISFIED: ",cost_gb_linesearch,"  ",upper_bound
      !! The new step is computed from second order approximation, 
      !! following the paper of [Barucq, Faucher & Pham] in Journal
      !! of computational Physics: 2nd order interpolation update
      step_evolve = -real(dble(searchTgrad*alpha*alpha) /               &
                          dble(2*(cost_gb_linesearch - cost_gb_origin - &
                                  searchTgrad*alpha)))
    else
      flag_valid = .true.
      step_evolve= alpha
      if(ctx_paral%master)  write(6,'(a,es12.5,a,es12.5)')              &
             "          SATISFIED: ",cost_gb_linesearch,"  ",upper_bound
    end if
    !! -----------------------------------------------------------------

    return

  end subroutine linesearch_armijo
  !---------------------------------------------------------------------

end module m_linesearch_control
!------------------------------------------------------------------------------
