!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_linesearch_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize the linesearch information
!
!------------------------------------------------------------------------------
module m_linesearch_init
  
  use m_raise_error,                only : raise_error, t_error
  use m_ctx_parallelism,            only : t_parallelism
  use m_read_parameters_linesearch, only : read_parameters_linesearch
  use m_ctx_linesearch,             only : t_linesearch
  use m_print_linesearch,           only : print_linesearch_info
  use m_tag_linesearch,             only : linesearch_tag_formalism,    &
                                           tag_LINESEARCH_ARMIJO,       &
                                           tag_LINESEARCH_GOLDSTEIN,    &
                                           linesearch_tag_formalism_control

  implicit none

  private  
  public :: linesearch_init
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init linesearch context information
  !
  !> @param[in]   ctx_paral     : parallel   context
  !> @param[inout] ctx_ls        : linesearch context
  !> @param[in]   parameter_file : parameter file
  !> @param[in]   n_model       : number of param
  !> @param[in]   n_group       : number of group
  !> @param[in]   param_name    : param name
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine linesearch_init(ctx_paral,ctx_ls,parameter_file,n_model,   &
                             n_group,param_name,ctx_err)
  
    implicit none

    type(t_parallelism)          ,intent(in)   :: ctx_paral
    type(t_linesearch)           ,intent(inout):: ctx_ls
    character(len=*)             ,intent(in)   :: parameter_file
    integer                      ,intent(in)   :: n_model,n_group
    character(len=*),allocatable ,intent(in)   :: param_name(:)
    type(t_error)                ,intent(out)  :: ctx_err
    !!
    character(len=32)  :: str,str_control

    ctx_err%ierr = 0

    if(n_model <= 0 .or. n_group <= 0) then
      ctx_err%msg   = "** ERROR: linesearch n_model or n_group < 0 " // &
                      " [inesearch_init]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! get some infos from inputs
    ctx_ls%n_param     =n_model
    ctx_ls%n_group     =n_group
    allocate(ctx_ls%step_start_group(n_model,n_group))
    allocate(ctx_ls%step_current    (n_model))
    ctx_ls%step_start_group=0.0
    ctx_ls%step_current    =0.0
    allocate(ctx_ls%param_name(n_model))
    ctx_ls%param_name(1:ctx_ls%n_param) = param_name(1:ctx_ls%n_param)


    call read_parameters_linesearch(parameter_file,ctx_ls%n_param,      &
                         ctx_ls%n_group,str,ctx_ls%n_iter_max,          &
                         ctx_ls%flag_reset_iter,ctx_ls%step_start_group,&
                         str_control,ctx_ls%control_tol,                &
                         ctx_ls%blr_tol,ctx_err)
    !! we convert the input names in TAG 
    call linesearch_tag_formalism(str,ctx_ls%method,ctx_err)
    call linesearch_tag_formalism_control(str_control,                  &
                                          ctx_ls%control_method,ctx_err)

    !! Consistency check for some of the numbers
    if(minval(ctx_ls%step_start_group) <= 0.0) then
      ctx_err%msg   = "** ERROR: some step for line search are lower "//&
                      " than 0 [inesearch_init]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(ctx_ls%n_iter_max < 0) then
      ctx_err%msg   = "** ERROR: niter for line search below 0 "//&
                      "[inesearch_init]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    if(ctx_ls%control_method == tag_LINESEARCH_GOLDSTEIN .and. &
      (ctx_ls%control_tol <0. .or. ctx_ls%control_tol > 0.5)) then
      ctx_err%msg   = "** ERROR: Goldstein condition needs tolerance "//& 
                      "between 0 and 0.5 [inesearch_init]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(ctx_ls%control_method == tag_LINESEARCH_ARMIJO .and. &
      (ctx_ls%control_tol <0. .or. ctx_ls%control_tol > 1.)) then
      ctx_err%msg   = "** ERROR: Armijo condition needs tolerance "//& 
                      "between 0 and 1 [inesearch_init]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------

    !! print infos here
    call print_linesearch_info(ctx_paral,ctx_ls%n_param,                &
               ctx_ls%param_name,ctx_ls%method,ctx_ls%n_group,          &
               ctx_ls%n_iter_max,ctx_ls%step_start_group,               &
               ctx_ls%flag_reset_iter,ctx_ls%control_method,            &
               ctx_ls%control_tol,ctx_ls%blr_tol,ctx_err)

    return
  end subroutine linesearch_init

end module m_linesearch_init
