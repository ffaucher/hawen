!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_tag_linesearch.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the method for the linesearch
!
!------------------------------------------------------------------------------
module m_tag_linesearch

  use m_raise_error,              only : raise_error, t_error

  implicit none

  !> Main method
  integer, parameter :: tag_LINESEARCH_IGNORE      = 0 !! constant step
  integer, parameter :: tag_LINESEARCH_BACKTRACKING= 1 !! backtracking
  
  !> control method
  integer, parameter :: tag_LINESEARCH_GOLDSTEIN   =10 !! Goldstein cond
  integer, parameter :: tag_LINESEARCH_ARMIJO      =11 !! Armijo cond.
  

  private
  
  public :: linesearch_tag_formalism, linesearch_tag_naming
  public :: linesearch_tag_formalism_control
  public :: tag_LINESEARCH_BACKTRACKING, tag_LINESEARCH_IGNORE
  public :: tag_LINESEARCH_GOLDSTEIN, tag_LINESEARCH_ARMIJO

  contains 
    
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> decide the linesearch tag from keyword
  !
  !> @param[in]   name_in       : input name
  !> @param[out]  tag_out       : output tag
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine linesearch_tag_formalism(name_in,tag_out,ctx_err)
    implicit none

    character(len=32)      ,intent(in)   :: name_in
    integer                ,intent(inout):: tag_out
    type(t_error)          ,intent(inout):: ctx_err

    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case(trim(adjustl(name_in)))
      case('backtracking','Backtracking','BACKTRACKING')
        tag_out = tag_LINESEARCH_BACKTRACKING
      case('ignore','IGNORE','Ignore','none','NONE','0','None')
        tag_out = tag_LINESEARCH_IGNORE
      case default
        ctx_err%msg   = "** ERROR: Unrecognized linesearch method " // &
                        "[linesearch_tag_formalism]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
        
    return
    
  end subroutine linesearch_tag_formalism
    
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> decide the linesearch control tag from keyword
  !
  !> @param[in]   name_in       : input name
  !> @param[out]  tag_out       : output tag
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine linesearch_tag_formalism_control(name_in,tag_out,ctx_err)
    implicit none

    character(len=32)      ,intent(in)   :: name_in
    integer                ,intent(inout):: tag_out
    type(t_error)          ,intent(inout):: ctx_err

    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    select case(trim(adjustl(name_in)))
      case('Goldstein','goldstein','GOLDSTEIN')
        tag_out = tag_LINESEARCH_GOLDSTEIN
      case('Armijo','armijo','ARMIJO')
        tag_out = tag_LINESEARCH_ARMIJO
      case('ignore','IGNORE','Ignore','none','NONE','0','None','')
        tag_out = tag_LINESEARCH_IGNORE
      case default
        ctx_err%msg   = "** ERROR: Unrecognized linesearch control " // &
                        "method [linesearch_tag_formalism]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
        
    return
    
  end subroutine linesearch_tag_formalism_control

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> from the tag, we extract a word for printing
  !
  !> @param[in]   tag_in        : input tag
  !> @param[out]  model_name    : current model name
  !> @param[out]  str           : string out
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine linesearch_tag_naming(tag_in,model_name,str,ctx_err)
    implicit none

    integer                            ,intent(in)   :: tag_in
    character(len=*)                   ,intent(in)   :: model_name
    character(len=128)                 ,intent(inout):: str
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    str = ''
    str = 'linesearch for '// trim(adjustl(model_name))
    select case(tag_in)
      case(tag_LINESEARCH_IGNORE)        
        str = str(1:26) // ' using constant step'
      case(tag_LINESEARCH_BACKTRACKING)
        str = str(1:26) // ' using backtracking'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized linesearch method " //&
                        "[linesearch_naming]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
    
  end subroutine linesearch_tag_naming

end module m_tag_linesearch
