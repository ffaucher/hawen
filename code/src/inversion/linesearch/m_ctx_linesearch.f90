!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_linesearch
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the context for the linesearch method.
!
!------------------------------------------------------------------------------
module m_ctx_linesearch

  implicit none

  private
  public :: t_linesearch, linesearch_clean, linesearch_reset

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_linesearch contains all information for the linesearch method
  !
  !> @param[integer] method          : TAG for the linesearch method
  !> @param[integer] n_iter_max      : number of iter for linesearh
  !> @param[integer] n_param         : number of parameters involved
  !> @param[integer] n_group         : number of frequency group
  !> @param[string]  model_name      : model name
  !> @param[real]    step_start_group: list of step per param and group
  !> @param[real]    step_current    : current step for all param
  !> @param[logical] flag_reset_iter : indicate if reset every iter
  !> @param[logical] flag_goldstein  : indicate Glodstein condition
  !> @param[real]    goldstein_tol   : Glodstein tol
  !> @param[real]    blr_tol         : low rank tolerance for mumps.
  !
  !----------------------------------------------------------------------------
  type t_linesearch

    !! tag for the linesearch method
    integer                        :: method
    !! maximum number of linesearch iteration
    integer                        :: n_iter_max
    !! number of parameters for the search direction
    integer                        :: n_param
    !! number of frequency group
    integer                        :: n_group

    !! list of model name
    character(len=32), allocatable :: param_name(:)
    
    !! we allow for a specific block-low rank tolerance for linesearch
    !! only used if 0 < tol < 1
    real                           :: blr_tol
    
    !! total number of frequency (group) iteration, the step can
    !! have a different value for each group  :: size(n_param,n_freq_group)
    real,     allocatable :: step_start_group(:,:)
    !! current step value (1 per model) :: size(n_param)
    real,     allocatable :: step_current(:)

    !! indicates if the step is reset for every iteration of we keep
    !! with current value (in any cases it is reset when new freq
    logical               :: flag_reset_iter
    !! Goldstein condition verification --------------------------------
    !! so that the step is not too low or too large we check that 
    !!
    !! J(m) + (1-K) \alpha (s^T.grad(J)) 
    !!                 <= J(m -alpha s) <= J(m) + K \alpha (s^T.grad(J))
    !! -----------------------------------------------------------------
    integer               :: control_method
    real                  :: control_tol
  end type t_linesearch
  !----------------------------------------------------------------------------


  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> reset the linesearch step for a given group value
  !
  !> @param[inout] ctx_ls   : linesearch context
  !> @param[in]    i_group  : group used to reset the values
  !----------------------------------------------------------------------------
  subroutine linesearch_reset(ctx_ls,i_group)
    type(t_linesearch)    ,intent(inout)  :: ctx_ls
    integer               ,intent(in)     :: i_group

    ctx_ls%step_current(:) = ctx_ls%step_start_group(:,i_group)

    return
  end subroutine linesearch_reset

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> clean search dir information and allocation
  !
  !> @param[inout] ctx_search  : search context
  !----------------------------------------------------------------------------
  subroutine linesearch_clean(ctx_ls)
    type(t_linesearch)    ,intent(inout)  :: ctx_ls

    ctx_ls%method         = 0
    ctx_ls%n_iter_max     = 0
    ctx_ls%n_param        = 0
    ctx_ls%n_group        = 0
    ctx_ls%flag_reset_iter= .false.
    ctx_ls%control_method = 0
    ctx_ls%control_tol    = 0.
    ctx_ls%blr_tol        =-1.

    if(allocated(ctx_ls%step_start_group)) deallocate(ctx_ls%step_start_group)
    if(allocated(ctx_ls%step_current)    ) deallocate(ctx_ls%step_current)
    if(allocated(ctx_ls%param_name)      ) deallocate(ctx_ls%param_name)

    return
  end subroutine linesearch_clean

end module m_ctx_linesearch
