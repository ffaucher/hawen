!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_linesearch_compute.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the step length for the update
!> using line search algorithm
!
!------------------------------------------------------------------------------
module m_linesearch_compute

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_sum,          only: allreduce_sum
  use m_ctx_equation,           only: t_equation
  use m_ctx_acquisition,        only: t_acquisition
  use m_ctx_discretization,     only: t_discretization,                 &
                                      discretization_clean_context_src, &
                                      discretization_clean_context
  use m_ctx_rhs,                only: t_rhs,rhs_clean_allocation
  use m_ctx_domain,             only: t_domain
  use m_ctx_model,              only: t_model,model_copy,model_clean
  use m_api_solver,             only: t_solver, process_factorization,  &
                                      process_solve,                    &
                                      solver_update_lowrank,            &
                                      solver_get_lowrank
  use m_ctx_misfit_function,    only: t_misfit,misfit_copy_ctx,         &
                                      misfit_reset,misfit_clean
  use m_ctx_gradient,           only: t_gradient
  use m_ctx_regularization,     only: t_regularization,regularization_clean,& 
                                      regularization_copy_ctx,          &
                                      regularization_reset
  use m_ctx_search_dir,         only: t_search_dir
  use m_ctx_linesearch,         only: t_linesearch
  use m_print_basics,           only: separator,indicator,marker
  use m_tag_linesearch,         only: tag_LINESEARCH_BACKTRACKING,      & 
                                      tag_LINESEARCH_IGNORE,            &
                                      tag_LINESEARCH_GOLDSTEIN,         &
                                      tag_LINESEARCH_ARMIJO
  use m_model_update,           only: model_update
  use m_linesearch_control,     only: linesearch_goldstein,linesearch_armijo

  implicit none

  private

  public   :: linesearch_compute

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the update step using line search algorithm 
  !
  !> @param[in]   ctx_paral     : ctx_paral
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine linesearch_compute(ctx_paral,ctx_eq,ctx_aq,ctx_domain,     &
                                ctx_disc,ctx_model,ctx_misfit,ctx_grad, &
                                ctx_regularization,                     &
                                ctx_search,ctx_ls,ctx_solver,ctx_rhs,   &
                                frequency,mode,nfreq,nmode,             &
                                source_values,ctx_err)

    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    type(t_equation)              ,intent(inout):: ctx_eq
    type(t_acquisition)           ,intent(inout):: ctx_aq
    type(t_domain)                ,intent(inout):: ctx_domain
    type(t_discretization)        ,intent(inout):: ctx_disc
    type(t_model)                 ,intent(in)   :: ctx_model
    type(t_misfit)                ,intent(in)   :: ctx_misfit
    type(t_gradient)              ,intent(in)   :: ctx_grad
    type(t_regularization)        ,intent(in)   :: ctx_regularization
    type(t_search_dir)            ,intent(in)   :: ctx_search
    type(t_linesearch)            ,intent(inout):: ctx_ls
    type(t_solver)                ,intent(inout):: ctx_solver
    type(t_rhs)                   ,intent(inout):: ctx_rhs
    real(kind=4)                  ,intent(in)   :: frequency(:,:)
    integer                       ,intent(in)   :: mode(:)
    integer                       ,intent(in)   :: nfreq
    integer                       ,intent(in)   :: nmode
    complex(kind=8)               ,intent(in)   :: source_values(:)
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_model)                :: ctx_model_temp
    type(t_misfit)               :: ctx_misfit_temp
    type(t_regularization)       :: ctx_regul_temp
    real(kind=4),allocatable     :: search(:),grad(:)
    real                         :: alpha,step_evolve
    real(kind=8)                 :: descent,descent_gb
    integer                      :: i_param,ncells_loc,pfunc,ic,iter_ls
    character(len=32)            :: pname
    logical                      :: flag_finish
    !! in case we update the low-rank options
    integer                      :: lowrank_save_key
    real                         :: lowrank_save_tol
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! 0) If we do not perform linesearch, we leave right away
    !! -----------------------------------------------------------------
    if(ctx_ls%n_iter_max<1 .or. ctx_ls%method .eq. tag_LINESEARCH_IGNORE) then 
      if(ctx_paral%master) &
        write(6,'(a)') "- we do not perform line search and use fixed step"
      return
    end if

    !! Otherwize welcome method
    select case(ctx_ls%method)
      case(tag_LINESEARCH_BACKTRACKING)
        if(ctx_paral%master) write(6,'(2a)') indicator,                 &
                      " Backtracking line search method "
      !! Unrecognized method
      case default
        ctx_err%msg   = "** ERROR: unrecognized method [linesearch_compute] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! -----------------------------------------------------------------

    !! save the original lowrank options if it needs to be changed. 
    call solver_get_lowrank(ctx_solver,lowrank_save_key,                &
                            lowrank_save_tol,ctx_err)
    if(ctx_ls%blr_tol > 0 .and. ctx_ls%blr_tol < 1) then
      !! for to key 2: solve and facto.
      call solver_update_lowrank(ctx_solver,2,ctx_ls%blr_tol,ctx_err)
      !! reset analysis flag
      ctx_solver%flag_analysis=.false.
    end if

    !! loop over all parameters to invert >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! -----------------------------------------------------------------
    !! copy the physical models and misfit context
    call model_copy             (ctx_model,ctx_model_temp,.true.,ctx_err)
    call misfit_copy_ctx        (ctx_misfit,ctx_misfit_temp)
    call regularization_copy_ctx(ctx_regularization,ctx_regul_temp)
    allocate(grad  (ncells_loc)) ; 
    allocate(search(ncells_loc)) ; 
    do i_param = 1, ctx_ls%n_param
      !! get current infos, we work with local array per simplicity
      ncells_loc = ctx_grad%n_coeff_loc
      grad  = ctx_grad%grad    (:,i_param)
      search= ctx_search%search(:,i_param)
      pname = trim(adjustl(ctx_ls%param_name(i_param)))
      pfunc = ctx_grad%param_func(i_param)
      alpha = ctx_ls%step_current(i_param)
      flag_finish = .false.
      !! print welcome
      if(ctx_paral%master) &
        write(6,'(a,es11.4)') "- line search for parameter "//          &
                      trim(adjustl(pname))//",   initial cost=",ctx_misfit%J
      ! ----------------------------------------------------------------
      !! 1) Descent direction verification, we must have 
      !!   -S^T. G < 0
      !! Rk: this is always verified for gradient descent
      !! ---------------------------------------------------------------
      descent = 0.0
      do ic=1,ncells_loc
        descent= descent - (search(ic)*grad(ic))
      enddo
      call allreduce_sum(descent,descent_gb,1,ctx_paral%communicator,ctx_err)
      if(descent_gb >= 0.) then !! not a descent direction => exit
        if(ctx_paral%master) write(6,'(a,es12.4,a)') "-    NOT a " //   &
                 "descent direction (",descent_gb,") => EXIT LINE SEARCH"
        alpha=alpha/100. !> arbitrary
        flag_finish = .true. 
      else !! ok 
        if(ctx_paral%master) write(6,'(a,es12.4,a)') "-    descent" // &
                 " direction OK (",descent_gb,")"
        !! tiny increase of initial step
        alpha = alpha * 3./2.
      end if

      !! continue depending on the method
      ! ----------------------------------------------------------------
      select case(ctx_ls%method)
        !! backtracking >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        case(tag_LINESEARCH_BACKTRACKING)
          iter_ls = 1
          do while ( iter_ls <= ctx_ls%n_iter_max .and. .not. flag_finish)
            !! get back the initial model
            call model_copy(ctx_model,ctx_model_temp,.false.,ctx_err)
            !! update with current alpha
            call model_update(ctx_paral,ctx_model_temp,search,alpha,    &
                              trim(adjustl(pname)),pfunc,               &
                              ctx_grad%param_name,                      &
                              ctx_eq%current_angular_frequency,ctx_err)

            !! compute the misfit with this model >>>>>>>>>>>>>>>>>>>>>>
            call misfit_reset(ctx_misfit_temp)
            call regularization_reset(ctx_regul_temp)
            call linesearch_compute_residuals(ctx_paral,ctx_eq,ctx_aq,  &
                                              ctx_domain,ctx_disc,      &
                                              ctx_model_temp,ctx_solver,&
                                              ctx_rhs,ctx_misfit_temp,  &
                                              ctx_regul_temp,frequency, &
                                              mode,nfreq,nmode,         &
                                              source_values,ctx_err)
            !! print iteration infos
            if(ctx_paral%master) write(6,'(a,i0,a,es12.4,a,es12.4)')    &
              "-    backtracking iter. ", iter_ls, ") alpha=", alpha,   &
              " cost=", ctx_misfit_temp%J 
            
            !! compute control condition 
            select case(ctx_ls%control_method)
              !! Goldstein condition >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              case(tag_LINESEARCH_GOLDSTEIN)
                call linesearch_goldstein(ctx_paral,grad,search,alpha,  &
                                          ctx_ls%control_tol,           &
                                          ctx_misfit%J,                 &
                                          ctx_misfit_temp%J,ncells_loc, &
                                          flag_finish,step_evolve,ctx_err)
                if(.not. flag_finish) then
                  !! consistency check betwen 0.1 and 10*alpha
                  step_evolve = max(1E-1*alpha,step_evolve)
                  step_evolve = min(1E+1*alpha,step_evolve)
                  alpha = step_evolve
                end if
              !! Armijo condition >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              case(tag_LINESEARCH_ARMIJO)
                call linesearch_armijo(ctx_paral,grad,search,alpha,     &
                                       ctx_ls%control_tol,ctx_misfit%J,&
                                       ctx_misfit_temp%J,ncells_loc,    &
                                       flag_finish,step_evolve,ctx_err)
                if(.not. flag_finish) then
                  !! consistency check betwen 0.1 and 10*alpha
                  step_evolve = max(1E-1*alpha,step_evolve)
                  step_evolve = min(1E+1*alpha,step_evolve)
                  alpha = step_evolve
                end if
              !! simply check if it is decreasing >>>>>>>>>>>>>>>>>>>>>>
              case default
                if(ctx_misfit_temp%J < ctx_misfit%J) then
                  flag_finish=.true.
                else
                  alpha = alpha * 2./3. !> arbitrary reduce the alpha
                endif
            end select
            iter_ls = iter_ls + 1
          end do
          !! print outcome ---------------------------------------------
          if(flag_finish) then
            if(ctx_paral%master) write(6,'(a,es12.4)')      &
              "-    backtracking OK,      alpha=", alpha
          else
            alpha = alpha / 1.E2 !! arbitrary 
            if(ctx_paral%master) write(6,'(a,es12.4)')      &
              "-    backtracking KO iter, alpha=", alpha
          end if 
        ! ---------------------------------------------------------
        !! Unrecognized method
        case default
          ctx_err%msg   = "** ERROR: unrecognized method [linesearch_compute] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

      !! save final step
      ctx_ls%step_current(i_param) = alpha
    end do !! end loop over all parameters -----------------------------
    !! clean
    call misfit_clean(ctx_misfit_temp)
    call regularization_clean(ctx_regul_temp)
    call model_clean(ctx_model_temp)
    deallocate(grad  )
    deallocate(search)
    !! -----------------------------------------------------------------

    !! reset the block low-rank option.
    if(ctx_ls%blr_tol > 0 .and. ctx_ls%blr_tol < 1) then
      call solver_update_lowrank(ctx_solver,lowrank_save_key,           &
                                 lowrank_save_tol,ctx_err)
      !! reset analysis flag
      ctx_solver%flag_analysis=.false.
    end if

    return
  end subroutine linesearch_compute

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the residulas within the linesearch framework
  !
  !> @param[in]   ctx_paral     : ctx_paral
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine linesearch_compute_residuals(ctx_paral,ctx_eq,ctx_aq,ctx_domain,  &
                                          ctx_disc,ctx_model,ctx_solver,&
                                          ctx_rhs,ctx_misfit,ctx_regul, &
                                          frequency,mode,nfreq,nmode,   &
                                          source_values,ctx_err)

    !! locally needed to estimate the misfit
    use m_ctx_matrix,               only: t_matrix,matrix_clean
    use m_matrix_init,              only: matrix_init
    use m_matrix_discretization,    only: matrix_discretization
    use m_discretization_init,      only: discretization_init
    use m_api_solver,               only: process_factorization
    use m_rhs_create,               only: rhs_create
    use m_ctx_field,                only: t_field, field_clean
    use m_field_create,             only: field_create
    use m_misfit_compute,           only: misfit_compute
    use m_regularization_compute,   only: regularization_compute
    use m_wavelength_utils,         only: compute_wavelength_order   
    !! --------------------------------------------------------
    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    type(t_equation)              ,intent(inout):: ctx_eq
    type(t_acquisition)           ,intent(inout):: ctx_aq
    type(t_domain)                ,intent(inout):: ctx_domain
    type(t_discretization)        ,intent(inout):: ctx_disc
    type(t_model)                 ,intent(in)   :: ctx_model
    type(t_solver)                ,intent(inout):: ctx_solver
    type(t_rhs)                   ,intent(inout):: ctx_rhs
    type(t_misfit)                ,intent(inout):: ctx_misfit
    type(t_regularization)        ,intent(inout):: ctx_regul
    real(kind=4)                  ,intent(in)   :: frequency(:,:)
    integer                       ,intent(in)   :: mode(:)
    integer                       ,intent(in)   :: nfreq
    integer                       ,intent(in)   :: nmode
    complex(kind=8)               ,intent(in)   :: source_values(:)
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    type(t_matrix)               :: ctx_matrix
    type(t_field)                :: ctx_field
    integer                      :: ifreq,irhs_group,imode,i_fm
    logical                      :: flag_inversion=.false.
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------

    !! loop over all frequencies and modes in group 
    do imode=1,nmode ; do ifreq=1,nfreq
      
      !! global index
      i_fm = (imode-1)*nfreq + ifreq
      ctx_eq%current_mode = mode(imode)
      
      !! use the appropriate frequency: after the loop, it is reset 
      !! to the current value (ie for ifreq=1)
      !!
      !! there is a minus here, because we have 
      !!     \sigma =  \omega + i \laplace
      !! => i\sigma = i\omega -   \laplace
      !! -----------------------------------------------------------------
      ctx_eq%current_angular_frequency = dcmplx(-frequency(ifreq,2),    &
                     dble(2.d0*4.d0*datan(1.d0)*frequency(ifreq,1)))

      !! ---------------------------------------------------------------
      !! In case of automatic order, we clean and re-compute reference
      !! matrices, except if only one frequency in the group, then it 
      !! has already been prepared in the main process
      !! ---------------------------------------------------------------
      if((trim(adjustl(ctx_domain%order_method)) .eq. 'automatic').and. &
         (nfreq.ne.1 .or. nmode.ne.1)) then
        call discretization_clean_context(ctx_disc)
        call compute_wavelength_order(ctx_paral,ctx_model,ctx_domain,   &
                                      ctx_eq,ctx_err)
        call discretization_init(ctx_paral,ctx_disc,ctx_domain,         &
                                 ctx_eq%dim_var,ctx_eq%dim_solution,    &
                                 ctx_eq%name_var,ctx_eq%name_sol,       &
                                 flag_inversion,ctx_err)
      end if
      !! ---------------------------------------------------------------


      !! ---------------------------------------------------------------
      !! we estimate the number of nnz for the matrix and init context
      !! the size may change with freq if pml adjusted to the wavelength
      call matrix_init(ctx_paral,ctx_eq,ctx_domain,ctx_model,           &
                       ctx_disc,ctx_matrix,ctx_solver%flag_block,       &
                       ctx_err,o_verb_lvl=0)

      !! matrix discretization and factorization -----------------------
      call matrix_discretization(ctx_paral,ctx_eq,ctx_domain,           &
                                 ctx_model,ctx_disc,ctx_matrix,         &
                                 flag_inversion,ctx_err,o_verb_lvl=0)
      call process_factorization(ctx_paral,ctx_solver,ctx_matrix,       &
                                 ctx_err,o_flag_verb=.false.)

      !! the source values is given in input, to adjust in case
      !! of source reconstruction
      ctx_aq%src_info%current_val = source_values(i_fm)
      
      !! -----------------------------------------------------------
      !! loop over group of RHS
      ! -------------------------------------------------------
      do irhs_group=1,ctx_rhs%n_group
        ctx_rhs%i_group = irhs_group
        !! the creation may need the pml size and HDG matrices -----
        call rhs_create(ctx_paral,ctx_aq,ctx_domain,ctx_disc,ctx_rhs,   &
                        ctx_eq%current_angular_frequency,               &
                        ctx_eq%tag_scale_rhs,ctx_err)

        !! solve resulting problem
        call process_solve(ctx_paral,ctx_solver,ctx_rhs,.false.,        &
                           ctx_err,o_flag_verb=.false.)
      
        !! ---------------------------------------------------------
        !! the solution is contained in the ctx_solver
        !! distribute local informations here
        !! for HDG we also need the post-processing to retrieve the
        !! wavefields from the Lagrange multipliers
        !! ---------------------------------------------------------
        call field_create(ctx_paral,ctx_domain,ctx_disc,ctx_field,      &
                          ctx_rhs%n_rhs_in_group(irhs_group),           &
                          ctx_eq%dim_solution,ctx_eq%name_sol,          &
                          ctx_solver%solution_global,.false.,ctx_err,   &
                          o_flag_verb=.false.)

       !! ---------------------------------------------------------
       !! We compute the backward rhs and update the cost function
       !! ---------------------------------------------------------
       call misfit_compute(ctx_paral,ctx_aq,ctx_eq,ctx_disc,            &
                           ctx_field,frequency(ifreq,:),mode(imode),    &
                           ctx_misfit%tag_misfit,ctx_misfit%n_field,    &
                           ctx_misfit%data_path,                        &
                           ctx_misfit%data_field,                       &
                           irhs_group,ctx_rhs%n_rhs_in_group,           &
                           ctx_misfit%J,ctx_misfit%Jloc,                &
                           ctx_misfit%n_obs_reciprocity,                &
                           !! information on receivers offset
                           ctx_misfit%flag_rcv_offset,                  &
                           ctx_misfit%rcv_offset_x,                     &
                           ctx_misfit%rcv_offset_y,                     &
                           ctx_misfit%rcv_offset_z,                     &
                           ctx_misfit%rcv_offset_rad,ctx_err)
        !! adjust with regularization term 
        if(ctx_regul%flag) then
          call regularization_compute(ctx_paral,ctx_domain,ctx_model,   &
                                      ctx_regul,                        &
                                      ctx_eq%current_angular_frequency, &
                                      ctx_err)
          !! no need to update the gradient here...
          !! update J because regul part is reset in the computation.
          ctx_misfit%Jregularization = ctx_misfit%Jregularization +     &
                                       ctx_regul%part_misfit
          ctx_misfit%J  = ctx_misfit%J + ctx_regul%part_misfit
        
        end if
        !! clean group of rhs infos and current discretization infos
        !! regarding the acquisition.
        call discretization_clean_context_src(ctx_disc)
        call field_clean                     (ctx_field)
        call rhs_clean_allocation            (ctx_rhs)
      end do !! end group of rhs
      call matrix_clean(ctx_matrix)

    !! -----------------------------------------------------------------
    end do ; end do !! end loop over frequency and modes in group
    !! -----------------------------------------------------------------

    !! Reset the frequency value in the equation type 
    !!
    !! there is a minus here, because we have 
    !!     \sigma =  \omega + i \laplace
    !! => i\sigma = i\omega -   \laplace
    !! -----------------------------------------------------------------
    ctx_eq%current_angular_frequency = dcmplx(-frequency(1,2),          &
                   dble(2.d0*4.d0*datan(1.d0)*frequency(1,1)))
    ctx_eq%current_mode = mode(1)

    return
  end subroutine linesearch_compute_residuals

end module m_linesearch_compute
