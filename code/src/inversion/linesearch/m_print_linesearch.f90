!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_linesearch.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for the linesearch
!
!------------------------------------------------------------------------------
module m_print_linesearch

  use m_ctx_parallelism,      only : t_parallelism
  use m_raise_error,          only : t_error, raise_error
  use m_tag_linesearch,       only : linesearch_tag_naming
  use m_print_basics,         only : separator,indicator
  use m_tag_linesearch,       only : tag_LINESEARCH_ARMIJO,       &
                                     tag_LINESEARCH_GOLDSTEIN
  implicit none
  
  private
  public :: print_linesearch_info
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the linesearch
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   n_model         : number of model to look for 
  !> @param[in]   param_name      : model name 
  !> @param[in]   tag_method      : method name 
  !> @param[in]   n_group         : number of group 
  !> @param[in]   niter           : number of linesearch iteration 
  !> @param[in]   step_start_group: list of starting step per param per group
  !> @param[in]   flag_reset_iter : if reset step every iteration
  !> @param[in]   control_tag     : controling method
  !> @param[in]   control_tol     : control tolerance
  !> @param[in]   blr_tol         : block low-rank option
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine print_linesearch_info(ctx_paral,n_model,param_name,        &
                                   tag_method,n_group,n_iter,           &
                                   step_start,flag_reset_iter,          &
                                   control_tag,control_tol,blr_tol,ctx_err)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: n_model,n_iter,n_group
    character(len=*),allocatable  ,intent(in)   :: param_name(:)
    integer                       ,intent(in)   :: tag_method
    real            ,allocatable  ,intent(in)   :: step_start(:,:)
    logical                       ,intent(in)   :: flag_reset_iter
    integer                       ,intent(in)   :: control_tag
    real                          ,intent(in)   :: control_tol
    real                          ,intent(in)   :: blr_tol
    type(t_error)                 ,intent(out)  :: ctx_err
    !!
    character(len=128) :: str
    character(len=32)  :: frmt
    character(len=16)  :: ngp_char
    integer            :: k

    ctx_err%ierr=0
    
    write(ngp_char,*) n_group
    frmt="(a,"//trim(adjustl(ngp_char))//"es10.3)"
    
    if(ctx_paral%master) then
      write(6,'(2a)')   indicator, " Linesearch information  "
    endif

    !! get the method name for all models 
    do k=1,n_model
      call linesearch_tag_naming(tag_method,param_name(k),str,ctx_err)
      if(ctx_paral%master) then
        write(6,'(a)') "- " // trim(adjustl(str))
        !! list of starting step per group
        write(6,trim(adjustl(frmt))) "- list of initial steps      : ", &
                                     step_start(k,:)
      end if
    end do
    
    if(ctx_paral%master) then
      write(6,'(a,i0)') "- number of linesearch iter. : ", n_iter
      if(flag_reset_iter) then
        write(6,'(a)') "- Reset linesearch step at every iteration "
      end if
      !! additional informations on the controling method
      if(control_tag == tag_LINESEARCH_GOLDSTEIN) then
        write(6,'(a,es10.3)') "- Goldstein condition tol    : ",control_tol
      elseif(control_tag == tag_LINESEARCH_ARMIJO) then
        write(6,'(a,es10.3)') "- Armijo condition tol       : ",control_tol
      end if

      !! option for BLR
      if(blr_tol > 0 .and. blr_tol < 1) then
        write(6,'(a,es10.3)') "- Mumps low-rank tolerance   : ",blr_tol
      end if
    end if
    
    return
  end subroutine print_linesearch_info

end module m_print_linesearch
