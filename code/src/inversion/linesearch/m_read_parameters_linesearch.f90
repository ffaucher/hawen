!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_linesearch.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the linesearch input parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_linesearch
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_linesearch
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters for the gradient
  !
  !> @param[in]   parameter_file  : parameter file where infos are
  !> @param[in]   n_model         : number of model to look for 
  !> @param[in]   n_group         : number of group 
  !> @param[in]   method          : method name 
  !> @param[in]   niter           : number of linesearch iteration 
  !> @param[in]   flag_reset_iter : if reset step every iteration
  !> @param[in]   step_start_group: list of starting step per param per group
  !> @param[in]   flag_goldstein  : goldstein condition
  !> @param[in]   tol_goldstein   : goldstein tol
  !> @param[in]   blr_tol         : block low-rank option
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_linesearch(parameter_file,n_model,n_group, &
                                        method,niter,flag_reset_iter,   &
                                        step_start_group,control,       &
                                        tol_control,blr_tol,ctx_err)
    implicit none 
    
    character(len=*)     ,intent(in)   :: parameter_file
    integer              ,intent(in)   :: n_model,n_group
    character(len=32)    ,intent(out)  :: method,control
    integer              ,intent(out)  :: niter
    logical              ,intent(out)  :: flag_reset_iter
    real                 ,intent(out)  :: tol_control
    real                 ,intent(out)  :: blr_tol
    real,    allocatable ,intent(inout):: step_start_group(:,:)
    type(t_error)        ,intent(out)  :: ctx_err
    
    character(len=512)   :: str(512),paramkey,strtemp
    character(len=8)     :: stmp
    real                 :: rlist(512)
    integer              :: ilist(512)
    logical              :: llist(512)
    integer              :: k,nval,l
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    call read_parameter(parameter_file,'fwi_linesearch_method','',str,nval)
    strtemp=str(1)
    method=strtemp(1:32)
    call read_parameter(parameter_file,'fwi_linesearch_niter',0,ilist,nval)
    niter = ilist(1)
    call read_parameter(parameter_file,'fwi_linesearch_reset_iter',     &
                        .false.,llist,nval)
    flag_reset_iter = llist(1)
    !! control linesearch: Goldstein or Armijo
    call read_parameter(parameter_file,'fwi_linesearch_control','',str,nval)
    strtemp = str(1)
    control = strtemp(1:32)
    call read_parameter(parameter_file,'fwi_linesearch_control_tol',0., &
                        rlist,nval)
    tol_control = rlist(1)

    call read_parameter(parameter_file,'fwi_linesearch_lowrank',-1.0,   &
                        rlist,nval)
    blr_tol = rlist(1)
    if(blr_tol < 0. .or. blr_tol > 1.) blr_tol = -1 

    !! -----------------------------------------------------------------
    step_start_group = 0.0
    !! there are n_model to found linesearch for
    !! for every one, we can have one starting parameter per frequency
    do k=1,n_model
      write(stmp, '(i0)') k
      !! Read the starting step for each model and each frequency group
      paramkey = trim(adjustl(('fwi_linesearch_step'//trim(stmp))))
      call read_parameter(parameter_file,trim(adjustl((paramkey))),-1., &
                          rlist,nval)
      
      if(nval < 1) then
        ctx_err%msg   = "** ERROR: no initial line search step given "//&
                        " [inesearch_read_param]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if

      do l=1,n_group
        if(l <= nval) then
          step_start_group(k,l) =  rlist(l)
        else
          step_start_group(k,l) =  step_start_group(k,l-1)
        end if
      end do
    end do
    !! -----------------------------------------------------------------
    return
  end subroutine read_parameters_linesearch

end module m_read_parameters_linesearch
