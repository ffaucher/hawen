!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_gradient.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the gradient input parameters
!> such that the model function
!
!------------------------------------------------------------------------------
module m_read_parameters_gradient
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_gradient
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters for the gradient
  !
  !> @param[in]   parameter_file  : parameter file where infos are
  !> @param[in]   flag_pseudoh    : flag to indicate pseudo hessian scaling
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_gradient(parameter_file,n_model,str_grad_model,  &
                                      func_grad_model,str_grad_metric,        &
                                      str_model_param,model_param_doforder,   &
                                      flag_grad_box,grad_xmin,grad_xmax,      &
                                      grad_ymin,grad_ymax,grad_zmin,grad_zmax,&
                                      grad_rmin,grad_rmax,grad_r0,            &
                                      flag_pseudoh,flag_debug_save,ctx_err)

    implicit none 
    
    character(len=*)             ,intent(in)   :: parameter_file
    integer                      ,intent(in)   :: n_model
    character(len=32),allocatable,intent(inout):: str_grad_model(:)
    character(len=32),allocatable,intent(inout):: func_grad_model(:)
    character(len=512)           ,intent(out)  :: str_grad_metric
    character(len=512)           ,intent(out)  :: str_model_param
    integer                      ,intent(out)  :: model_param_doforder
    logical                      ,intent(out)  :: flag_grad_box
    real                         ,intent(out)  :: grad_xmin,grad_xmax
    real                         ,intent(out)  :: grad_ymin,grad_ymax
    real                         ,intent(out)  :: grad_zmin,grad_zmax
    real                         ,intent(out)  :: grad_rmin,grad_rmax
    real                         ,intent(out)  :: grad_r0(3)
    logical                      ,intent(out)  :: flag_pseudoh
    logical                      ,intent(out)  :: flag_debug_save
    type(t_error)                ,intent(out)  :: ctx_err
    
    character(len=3)     :: stmp
    character(len=512)   :: str(512)
    integer              :: nval,k,ilist(512)
    real                 :: rlist(512)
    logical              :: buffer_logical(512)

    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    !! there are n_model to be found, they MUST be present, even if  
    !! they will not be inverted: for instance in acoustics we need
    !! to know the two parameters, and (lambda,rho) is no the same
    !! as (lambda, cp), whatever rho/cp is not inverted.

    !! get the metric for the gradient inner product
    str_grad_metric=''
    call read_parameter(parameter_file,'fwi_gradient_metric','identity',&
                        str_grad_metric,nval)

    !! get the model representation / parametrization
    str_model_param=''
    call read_parameter(parameter_file,'fwi_gradient_model_param',      &
                        'piecewise-constant',str_model_param,nval)
    call read_parameter(parameter_file,                                 &
                        'fwi_gradient_model_param_doforder',0,ilist,nval)
    model_param_doforder=ilist(1)

    call read_parameter(parameter_file,'fwi_pseudo_hessian_scale',      &
                        .false.,buffer_logical,nval)
    flag_pseudoh = buffer_logical(1)
    call read_parameter(parameter_file,'fwi_debug_save_gradient',       &
                        .false.,buffer_logical,nval)
    flag_debug_save = buffer_logical(1)
    
    !! Read the name of the combination of model to invert
    call read_parameter(parameter_file,'fwi_parameter','',str,nval)
    if(n_model .ne. nval) then
      write(stmp, '(i0)') n_model
      ctx_err%msg   = "** ERROR: the required "//stmp//" model names "//&
                      "for gradient are not found [grad_read_param]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    do k=1,n_model
      str_grad_model(k) = trim(adjustl(str(k)))
    end do
    
    !! read the function for each model
    call read_parameter(parameter_file,'fwi_parameter_function','',str,nval)
    if(n_model .ne. nval) then
      write(stmp, '(i0)') n_model
      ctx_err%msg   = "** ERROR: the required "//stmp//" model param "//&
                      "functions for grad are not found [grad_read_param]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    do k=1,n_model
      func_grad_model(k) = trim(adjustl(str(k)))
    end do
    !! -----------------------------------------------------------------
    !! Note that the tests to check validity is in init_gradient
    !! -----------------------------------------------------------------

    !! bounding box for fwi (only cartesian for now)
    call read_parameter(parameter_file,'fwi_restrict_box',.false.,      &
                        buffer_logical,nval)
    flag_grad_box = buffer_logical(1) 
    grad_r0 = 0.d0
    if(flag_grad_box) then
      call read_parameter(parameter_file,'fwi_xmin',-huge(1.0),rlist,nval)
      grad_xmin=rlist(1)
      call read_parameter(parameter_file,'fwi_xmax', huge(1.0),rlist,nval)
      grad_xmax=rlist(1)
      call read_parameter(parameter_file,'fwi_ymin',-huge(1.0),rlist,nval)
      grad_ymin=rlist(1)
      call read_parameter(parameter_file,'fwi_ymax', huge(1.0),rlist,nval)
      grad_ymax=rlist(1)
      call read_parameter(parameter_file,'fwi_zmin',-huge(1.0),rlist,nval)
      grad_zmin=rlist(1)
      call read_parameter(parameter_file,'fwi_zmax', huge(1.0),rlist,nval)
      grad_zmax=rlist(1)
      !! for circular restriction
      call read_parameter(parameter_file,'fwi_rmin',-huge(1.0),rlist,nval)
      grad_rmin=rlist(1)
      call read_parameter(parameter_file,'fwi_rmax', huge(1.0),rlist,nval)
      grad_rmax=rlist(1)
      call read_parameter(parameter_file,'fwi_r0x', 0.0,rlist,nval)
      grad_r0(1)=rlist(1)
      call read_parameter(parameter_file,'fwi_r0y', 0.0,rlist,nval)
      grad_r0(2)=rlist(1)
      call read_parameter(parameter_file,'fwi_r0z', 0.0,rlist,nval)
      grad_r0(3)=rlist(1)
    end if

    return
  end subroutine read_parameters_gradient

end module m_read_parameters_gradient
