!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_grad_model_function.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module defines the function of the model for the gradient computation
!
!------------------------------------------------------------------------------
module m_grad_model_function

  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_allreduce_min,            only : allreduce_min

  implicit none

  !>
  integer, parameter :: tag_GRADMODEL_IGNORE    = 0 !!   not inverted

  integer, parameter :: tag_GRADMODEL_ID        = 1 !!      model
  integer, parameter :: tag_GRADMODEL_ID_SQ     = 2 !!      model²

  integer, parameter :: tag_GRADMODEL_INV       =10 !!   1/model
  integer, parameter :: tag_GRADMODEL_INV_SQ    =11 !!   1/model²

  integer, parameter :: tag_GRADMODEL_SQRT      =20 !!   sqrt(model)
  integer, parameter :: tag_GRADMODEL_SQRT_INV  =21 !! 1/sqrt(model)

  integer, parameter :: tag_GRADMODEL_LOG       =30 !!    log(model)
  integer, parameter :: tag_GRADMODEL_LOG_SQ    =31 !!    log(model)²
  integer, parameter :: tag_GRADMODEL_LOG_INV   =32 !!  1/log(model)
  integer, parameter :: tag_GRADMODEL_LOG_INV_SQ=33 !!  1/log(model)²

  interface grad_model_function_formalism
     module procedure grad_model_function_formalism_all
     module procedure grad_model_function_formalism_solo
  end interface

  interface grad_model_function
     module procedure grad_model_function_all
     module procedure grad_model_function_solo_real4
     module procedure grad_model_function_solo_real8
     module procedure grad_model_function_solo_cx4
     module procedure grad_model_function_solo_cx8
  end interface


  private

  public :: grad_model_function_formalism, grad_model_function_naming
  public :: grad_model_function, apply_model_function, apply_model_function_inv
  public :: tag_GRADMODEL_IGNORE,tag_GRADMODEL_ID, tag_GRADMODEL_INV

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> decide the model function from keyword
  !
  !> @param[in]   n_model       : number of model function
  !> @param[in]   name_func_in  : input name
  !> @param[out]  tag_func_out  : output tag
  !> @param[out]  n_out         : number of model that are not ignored
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_formalism_all(n_model,name_func_in,    &
                                           tag_func_out,n_out,ctx_err)
    implicit none

    integer                            ,intent(in)   :: n_model
    character(len=32),allocatable      ,intent(in)   :: name_func_in(:)
    integer          ,allocatable      ,intent(inout):: tag_func_out(:)
    integer                            ,intent(out)  :: n_out
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer    :: k
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    n_out         = 0

    do k=1,n_model
      n_out = n_out + 1
      select case(trim(adjustl(name_func_in(k))))
        case('identity','id','ID','Id','Identity','IDENTITY')
          tag_func_out(k) = tag_GRADMODEL_ID
        case('squared','sq','SQ','SQUARED','Squared','id-squared',      &
             'identity-squared')
          tag_func_out(k) = tag_GRADMODEL_ID_SQ
        case('inv','INV','Inv','inverse','Inverse','INVERSE')
          tag_func_out(k) = tag_GRADMODEL_INV
        case('inv-sq','INV-SQ','Inv-Sq','inverse-squared',              &
             'Inverse-Squared','INVERSE-SQUARED')
          tag_func_out(k) = tag_GRADMODEL_INV_SQ
        case('sqrt','SQRT','Sqrt','square-root','Square-Root','SQUARE-ROOT')
          tag_func_out(k) = tag_GRADMODEL_SQRT
        case('sqrt-inv','SQRT-INV','Sqrt-Inv','square-root-inverse')
          tag_func_out(k) = tag_GRADMODEL_SQRT_INV
        case('log','LOG','Log')
          tag_func_out(k) = tag_GRADMODEL_LOG
        case('log-inv','LOG-INV','Log-Inv','log-inverse','LOG-INVERSE')
          tag_func_out(k) = tag_GRADMODEL_LOG_INV
        case('log-sq','LOG-SQ','Log-Sq','LOG-SQUARED','log-squared')
          tag_func_out(k) = tag_GRADMODEL_LOG_SQ
        case('log-inv-sq','LOG-INV-SQ','Log-Inv-Sq','log-inverse-squared', &
             'LOG-INVERSE-SQUARE')
          tag_func_out(k) = tag_GRADMODEL_LOG_INV_SQ
        case('ignore','IGNORE','Ignore','none','NONE','0','None')
          tag_func_out(k) = tag_GRADMODEL_IGNORE
          n_out = n_out-1 !! remove current one
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function_formalism]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end do

    return

  end subroutine grad_model_function_formalism_all

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> decide the model function from keyword
  !
  !> @param[in]   n_model       : number of model function
  !> @param[in]   name_func_in  : input name
  !> @param[out]  tag_func_out  : output tag
  !> @param[out]  n_out         : number of model that are not ignored
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_formalism_solo(name_func_in,           &
                                                tag_func_out,ctx_err)
    implicit none

    character(len=*)  ,intent(in)   :: name_func_in
    integer           ,intent(inout):: tag_func_out
    type(t_error)     ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0
    select case(trim(adjustl(name_func_in)))
      case('identity','id','ID','Id','Identity','IDENTITY')
        tag_func_out = tag_GRADMODEL_ID
      case('squared','sq','SQ','SQUARED','Squared','id-squared',      &
           'identity-squared')
        tag_func_out = tag_GRADMODEL_ID_SQ
      case('inv','INV','Inv','inverse','Inverse','INVERSE')
        tag_func_out = tag_GRADMODEL_INV
      case('inv-sq','INV-SQ','Inv-Sq','inverse-squared',              &
           'Inverse-Squared','INVERSE-SQUARED')
        tag_func_out = tag_GRADMODEL_INV_SQ
      case('sqrt','SQRT','Sqrt','square-root','Square-Root','SQUARE-ROOT')
        tag_func_out = tag_GRADMODEL_SQRT
      case('sqrt-inv','SQRT-INV','Sqrt-Inv','square-root-inverse')
        tag_func_out = tag_GRADMODEL_SQRT_INV
      case('log','LOG','Log')
        tag_func_out = tag_GRADMODEL_LOG
      case('log-inv','LOG-INV','Log-Inv','log-inverse','LOG-INVERSE')
        tag_func_out = tag_GRADMODEL_LOG_INV
      case('log-sq','LOG-SQ','Log-Sq','LOG-SQUARED','log-squared')
        tag_func_out = tag_GRADMODEL_LOG_SQ
      case('log-inv-sq','LOG-INV-SQ','Log-Inv-Sq','log-inverse-squared', &
           'LOG-INVERSE-SQUARE')
        tag_func_out = tag_GRADMODEL_LOG_INV_SQ
      case('ignore','IGNORE','Ignore','none','NONE','0','None')
        tag_func_out = tag_GRADMODEL_IGNORE
      case default
        ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                        "[grad_model_function_formalism]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return

  end subroutine grad_model_function_formalism_solo

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> from the tag, we extract a word for printing
  !
  !> @param[in]   tag_in        : input tag
  !> @param[out]  model_name    : current model name
  !> @param[out]  str           : string out
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_naming(tag_in,model_name,str,ctx_err)
    implicit none

    integer                            ,intent(in)   :: tag_in
    character(len=*)                   ,intent(in)   :: model_name
    character(len=128)                 ,intent(inout):: str
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    str = ''
    select case(tag_in)
      case(tag_GRADMODEL_IGNORE    )
        str = trim(adjustl(model_name)) // ' not inverted'
      case(tag_GRADMODEL_ID        )
        str = trim(adjustl(model_name))
      case(tag_GRADMODEL_ID_SQ     )
        str = trim(adjustl(model_name)) // ' squared'
        str = str(1:20) // ' ('//trim(adjustl(model_name))//'²)'
      case(tag_GRADMODEL_INV       )
        str = 'inverse ' // trim(adjustl(model_name))
        str = str(1:20) // ' (1/'//trim(adjustl(model_name))//')'
      case(tag_GRADMODEL_INV_SQ    )
        str = 'inverse ' // trim(adjustl(model_name)) // ' squared'
        str = str(1:28) // ' (1/'//trim(adjustl(model_name))//')²'
      case(tag_GRADMODEL_SQRT      )
        str = 'square root of ' // trim(adjustl(model_name))
        str = str(1:28) // ' (\sqrt{'//trim(adjustl(model_name))//'})'
      case(tag_GRADMODEL_SQRT_INV  )
        str = 'inverse square root of ' // trim(adjustl(model_name))
        str = str(1:35) // ' ('//trim(adjustl(model_name))//')^{-1/2}'
      case(tag_GRADMODEL_LOG       )
        str = 'log of ' // trim(adjustl(model_name))
        str = str(1:20) // ' (\log('//trim(adjustl(model_name))//'))'
      case(tag_GRADMODEL_LOG_SQ    )
        str = 'square log of ' // trim(adjustl(model_name))
        str = str(1:28) // ' (\log('//trim(adjustl(model_name))//'))²'
      case(tag_GRADMODEL_LOG_INV   )
        str = 'inverse log of ' // trim(adjustl(model_name))
        str = str(1:28) // ' (\log('//trim(adjustl(model_name))//'))^{-1}'
      case(tag_GRADMODEL_LOG_INV_SQ)
        str = 'square inverse log of ' // trim(adjustl(model_name))
        str = str(1:35) // ' (\log('//trim(adjustl(model_name))//'))^{-2}'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                        "[grad_model_function_formalism]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return

  end subroutine grad_model_function_naming

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> change gradient depending on the current model function
  !> ID         =>
  !> ID_SQ      => G(dx²)       =  2 x            G(dx)
  !> INV        => G(d1/x)      = -1/x²           G(dx)
  !> INV_SQ     => G(d1/x²)     = -2/x³           G(dx)
  !> SQRT       => G(dsqrt(x))  = 1/(2sqrt(x))    G(dx)
  !> SQRT_INV   => G(1/dsqrt(x))=-1/2 x^{-3/2}    G(dx)
  !> LOG        => G(dlog(x))   = 1/x             G(dx)
  !> LOG_SQ     => G(dlog(x)²)  = 2/x log(x)      G(dx)
  !> LOG_INV    => G(d1/log(x)) =-1/x log(x)^{-2} G(dx)
  !> LOG_INV_SQ => G(d1/log(x)²)=-2/x log(x)^{-3} G(dx)
  !! -------------------------------------------------------------------
  !
  !> @param[in]    ctx_paral        : context parallelism
  !> @param[in]    model            : model values
  !> @param[inout] gradient         : gradient values
  !> @param[in]    ncoeff           : number of values in the arrays
  !> @param[in]    tag_function_in  : tag for the input gradient
  !> @param[in]    tag_function_w   : tag for the wanted gradient
  !> @param[in]    flag_pseudoh     : pseudo-hessian uses squared quantities!
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_all(ctx_paral,model,gradient,ncoeff,   &
                                 tag_function_in,tag_function_wanted,   &
                                 ctx_err,o_flag_pseudoh)
    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: tag_function_in
    integer                       ,intent(in)   :: tag_function_wanted
    real(kind=4)     ,allocatable ,intent(in)   :: model   (:)
    real(kind=4)                  ,intent(inout):: gradient(:)
    integer                       ,intent(in)   :: ncoeff
    logical,optional              ,intent(in)   :: o_flag_pseudoh
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ierr,power
    real(kind=4), allocatable :: grad_temp(:)
    real(kind=4)              :: tol=1e-30
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    ierr=0
    if(minval(abs(model)) < tol) then
      ctx_err%ierr = -8
    end if
    call allreduce_min(ctx_err%ierr,ierr,1,ctx_paral%communicator,ctx_err)
    ctx_err%ierr = 0
    
    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power=2
    end if

    !! -----------------------------------------------------------------
    !! 0.) only if we do not have it already
    !! -----------------------------------------------------------------
    if(tag_function_in .ne. tag_function_wanted) then
      !! 1.) compute gradient w.r.t. identity
      allocate(grad_temp(ncoeff))
      grad_temp = 0.0

      select case(tag_function_in)
        case(tag_GRADMODEL_ID        )
          grad_temp = gradient
        case(tag_GRADMODEL_ID_SQ     )
          grad_temp = gradient * (2. * model)**power
        case(tag_GRADMODEL_INV       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model**2)**power
        case(tag_GRADMODEL_INV_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model**3)**power
        case(tag_GRADMODEL_SQRT      )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(1./(2.*sqrt(model)))**power
        case(tag_GRADMODEL_SQRT_INV  )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) grad_temp = gradient*(1./model)**power
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(2./model* log(model))**power
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./(model*(log(model)**(2))))**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model * (log(model))**(-3))**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      !! ---------------------------------------------------------------
      !! ERROR CHECK
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: the parametrization implies inverse"// &
                        " of a physical medium which has zero-value "  // &
                        "coefficient  [grad_model_function]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if

      !! ---------------------------------------------------------------
      !! 2.) compute gradient w.r.t. what is wanted
      select case(tag_function_wanted)
        case(tag_GRADMODEL_ID        )
          gradient = grad_temp
        case(tag_GRADMODEL_ID_SQ     )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) gradient = grad_temp*(1./(2*model))**power
        case(tag_GRADMODEL_INV       )
          gradient = grad_temp*(-(model**2))**power
        case(tag_GRADMODEL_INV_SQ    )
          gradient = grad_temp*(-(model**3/2.))**power
        case(tag_GRADMODEL_SQRT      )
          gradient =  grad_temp*(2.*sqrt(model))**power
        case(tag_GRADMODEL_SQRT_INV  )
          gradient = grad_temp*(-(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          gradient =  model* grad_temp
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(model / (2.*log(model)))**power
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model * (log(model))**2)**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model*(log(model))**3 /2.)**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
      deallocate(grad_temp)
    end if

    return
  end subroutine grad_model_function_all
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> change gradient depending on the current model function
  !> ID         =>
  !> ID_SQ      => G(dx²)       =  2 x            G(dx)
  !> INV        => G(d1/x)      = -1/x²           G(dx)
  !> INV_SQ     => G(d1/x²)     = -2/x³           G(dx)
  !> SQRT       => G(dsqrt(x))  = 1/(2sqrt(x))    G(dx)
  !> SQRT_INV   => G(1/dsqrt(x))=-1/2 x^{-3/2}    G(dx)
  !> LOG        => G(dlog(x))   = 1/x             G(dx)
  !> LOG_SQ     => G(dlog(x)²)  = 2/x log(x)      G(dx)
  !> LOG_INV    => G(d1/log(x)) =-1/x log(x)^{-2} G(dx)
  !> LOG_INV_SQ => G(d1/log(x)²)=-2/x log(x)^{-3} G(dx)
  !! -------------------------------------------------------------------
  !
  !> @param[in]    model            : model values
  !> @param[inout] gradient         : gradient values
  !> @param[in]    ncoeff           : number of values in the arrays
  !> @param[in]    tag_function_in  : tag for the input gradient
  !> @param[in]    tag_function_w   : tag for the wanted gradient
  !> @param[in]    flag_pseudoh     : pseudo-hessian uses squared quantities!
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_solo_real4(model,gradient,ncoeff,      &
                                            tag_function_in,            &
                                            tag_function_wanted,ctx_err,&
                                            o_flag_pseudoh)
    implicit none

    integer                       ,intent(in)   :: tag_function_in
    integer                       ,intent(in)   :: tag_function_wanted
    real(kind=4)                  ,intent(in)   :: model
    real(kind=4)                  ,intent(inout):: gradient(:)
    integer                       ,intent(in)   :: ncoeff
    logical,optional              ,intent(in)   :: o_flag_pseudoh
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ierr,power
    real(kind=4), allocatable :: grad_temp(:)
    real(kind=4)              :: tol=tiny(1.0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    ierr=0
    if(abs(model) < tol) ierr = -8
    
    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power=2
    end if

    !! -----------------------------------------------------------------
    !! 0.) only if we do not have it already
    !! -----------------------------------------------------------------
    if(tag_function_in .ne. tag_function_wanted) then
      !! 1.) compute gradient w.r.t. identity
      allocate(grad_temp(ncoeff))
      grad_temp = 0.0

      select case(tag_function_in)
        case(tag_GRADMODEL_ID        )
          grad_temp = gradient
        case(tag_GRADMODEL_ID_SQ     )
          grad_temp = gradient * (2. * model)**power
        case(tag_GRADMODEL_INV       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model**2)**power
        case(tag_GRADMODEL_INV_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model**3)**power
        case(tag_GRADMODEL_SQRT      )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(1./(2.*sqrt(model)))**power
        case(tag_GRADMODEL_SQRT_INV  )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) grad_temp = gradient*(1./model)**power
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = 2./model* log(model) * gradient
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model*(log(model))**(-2))**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model* (log(model))**(-3))**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      !! ---------------------------------------------------------------
      !! ERROR CHECK
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: the parametrization implies inverse"// &
                        " of a physical medium which has zero-value "  // &
                        "coefficient  [grad_model_function]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !> non-shared error
      end if

      !! ---------------------------------------------------------------
      !! 2.) compute gradient w.r.t. what is wanted
      select case(tag_function_wanted)
        case(tag_GRADMODEL_ID        )
          gradient = grad_temp
        case(tag_GRADMODEL_ID_SQ     )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) gradient = grad_temp*(1./(2*model))**power
        case(tag_GRADMODEL_INV       )
          gradient = grad_temp*(-(model**2))**power
        case(tag_GRADMODEL_INV_SQ    )
          gradient = grad_temp*(-(model**3/2.))**power
        case(tag_GRADMODEL_SQRT      )
          gradient =  grad_temp*(2.*sqrt(model))**power
        case(tag_GRADMODEL_SQRT_INV  )
          gradient = grad_temp*(-(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          gradient = model* grad_temp
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(model / (2.*log(model)))**power
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model * (log(model))**2)**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model*(log(model))**3 /2.)**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      deallocate(grad_temp)
    end if

    return
  end subroutine grad_model_function_solo_real4
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> change gradient depending on the current model function
  !> ID         =>
  !> ID_SQ      => G(dx²)       =  2 x            G(dx)
  !> INV        => G(d1/x)      = -1/x²           G(dx)
  !> INV_SQ     => G(d1/x²)     = -2/x³           G(dx)
  !> SQRT       => G(dsqrt(x))  = 1/(2sqrt(x))    G(dx)
  !> SQRT_INV   => G(1/dsqrt(x))=-1/2 x^{-3/2}    G(dx)
  !> LOG        => G(dlog(x))   = 1/x             G(dx)
  !> LOG_SQ     => G(dlog(x)²)  = 2/x log(x)      G(dx)
  !> LOG_INV    => G(d1/log(x)) =-1/x log(x)^{-2} G(dx)
  !> LOG_INV_SQ => G(d1/log(x)²)=-2/x log(x)^{-3} G(dx)
  !! -------------------------------------------------------------------
  !
  !> @param[in]    model            : model values
  !> @param[inout] gradient         : gradient values
  !> @param[in]    ncoeff           : number of values in the arrays
  !> @param[in]    tag_function_in  : tag for the input gradient
  !> @param[in]    tag_function_w   : tag for the wanted gradient
  !> @param[in]    flag_pseudoh     : pseudo-hessian uses squared quantities!
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_solo_real8(model,gradient,ncoeff,      &
                                            tag_function_in,            &
                                            tag_function_wanted,ctx_err,&
                                            o_flag_pseudoh)
    implicit none

    integer                       ,intent(in)   :: tag_function_in
    integer                       ,intent(in)   :: tag_function_wanted
    real(kind=8)                  ,intent(in)   :: model
    real(kind=8)                  ,intent(inout):: gradient(:)
    integer                       ,intent(in)   :: ncoeff
    logical,optional              ,intent(in)   :: o_flag_pseudoh
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ierr,power
    real(kind=8), allocatable :: grad_temp(:)
    real(kind=8)              :: tol=tiny(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    ierr=0
    if(abs(model) < tol) ierr = -8

    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power=2
    end if

    !! -----------------------------------------------------------------
    !! 0.) only if we do not have it already
    !! -----------------------------------------------------------------
    if(tag_function_in .ne. tag_function_wanted) then
      !! 1.) compute gradient w.r.t. identity
      allocate(grad_temp(ncoeff))
      grad_temp = 0.0

      select case(tag_function_in)
        case(tag_GRADMODEL_ID        )
          grad_temp = gradient
        case(tag_GRADMODEL_ID_SQ     )
          grad_temp = gradient * (2. * model)**power
        case(tag_GRADMODEL_INV       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model**2)**power
        case(tag_GRADMODEL_INV_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model**3)**power
        case(tag_GRADMODEL_SQRT      )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(1./(2.*sqrt(model)))**power
        case(tag_GRADMODEL_SQRT_INV  )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) grad_temp = gradient*(1./model)**power
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = 2./model* log(model) * gradient
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model*(log(model))**(-2))**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model* (log(model))**(-3))**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      !! ---------------------------------------------------------------
      !! ERROR CHECK
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: the parametrization implies inverse"// &
                        " of a physical medium which has zero-value "  // &
                        "coefficient  [grad_model_function]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !> non-shared error
      end if

      !! ---------------------------------------------------------------
      !! 2.) compute gradient w.r.t. what is wanted
      select case(tag_function_wanted)
        case(tag_GRADMODEL_ID        )
          gradient = grad_temp
        case(tag_GRADMODEL_ID_SQ     )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) gradient = grad_temp*(1./(2*model))**power
        case(tag_GRADMODEL_INV       )
          gradient = grad_temp*(-(model**2))**power
        case(tag_GRADMODEL_INV_SQ    )
          gradient = grad_temp*(-(model**3/2.))**power
        case(tag_GRADMODEL_SQRT      )
          gradient =  grad_temp*(2.*sqrt(model))**power
        case(tag_GRADMODEL_SQRT_INV  )
          gradient = grad_temp*(-(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          gradient = model* grad_temp
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(model / (2.*log(model)))**power
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model * (log(model))**2)**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model*(log(model))**3 /2.)**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      deallocate(grad_temp)
    end if

    return
  end subroutine grad_model_function_solo_real8
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> change gradient depending on the current model function
  !> ID         =>
  !> ID_SQ      => G(dx²)       =  2 x            G(dx)
  !> INV        => G(d1/x)      = -1/x²           G(dx)
  !> INV_SQ     => G(d1/x²)     = -2/x³           G(dx)
  !> SQRT       => G(dsqrt(x))  = 1/(2sqrt(x))    G(dx)
  !> SQRT_INV   => G(1/dsqrt(x))=-1/2 x^{-3/2}    G(dx)
  !> LOG        => G(dlog(x))   = 1/x             G(dx)
  !> LOG_SQ     => G(dlog(x)²)  = 2/x log(x)      G(dx)
  !> LOG_INV    => G(d1/log(x)) =-1/x log(x)^{-2} G(dx)
  !> LOG_INV_SQ => G(d1/log(x)²)=-2/x log(x)^{-3} G(dx)
  !! -------------------------------------------------------------------
  !
  !> @param[in]    model            : model values
  !> @param[inout] gradient         : gradient values
  !> @param[in]    ncoeff           : number of values in the arrays
  !> @param[in]    tag_function_in  : tag for the input gradient
  !> @param[in]    tag_function_w   : tag for the wanted gradient
  !> @param[in]    flag_pseudoh     : pseudo-hessian uses squared quantities!
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_solo_cx4(model,gradient,ncoeff,        &
                                          tag_function_in,              &
                                          tag_function_wanted,ctx_err,  &
                                          o_flag_pseudoh)
    implicit none

    integer                       ,intent(in)   :: tag_function_in
    integer                       ,intent(in)   :: tag_function_wanted
    real(kind=4)                  ,intent(in)   :: model
    complex(kind=4)               ,intent(inout):: gradient(:)
    integer                       ,intent(in)   :: ncoeff
    logical,optional              ,intent(in)   :: o_flag_pseudoh
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                      :: ierr,power
    complex(kind=4), allocatable :: grad_temp(:)
    real(kind=4)                 :: tol=tiny(1.0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    ierr=0
    if(abs(model) < tol) ierr = -8
    
    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power=2
    end if

    !! -----------------------------------------------------------------
    !! 0.) only if we do not have it already
    !! -----------------------------------------------------------------
    if(tag_function_in .ne. tag_function_wanted) then
      !! 1.) compute gradient w.r.t. identity
      allocate(grad_temp(ncoeff))
      grad_temp = 0.0

      select case(tag_function_in)
        case(tag_GRADMODEL_ID        )
          grad_temp = gradient
        case(tag_GRADMODEL_ID_SQ     )
          grad_temp = gradient * (2. * model)**power
        case(tag_GRADMODEL_INV       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model**2)**power
        case(tag_GRADMODEL_INV_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model**3)**power
        case(tag_GRADMODEL_SQRT      )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(1./(2.*sqrt(model)))**power
        case(tag_GRADMODEL_SQRT_INV  )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) grad_temp = gradient*(1./model)**power
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = 2./model* log(model) * gradient
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model*(log(model))**(-2))**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model* (log(model))**(-3))**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      !! ---------------------------------------------------------------
      !! ERROR CHECK
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: the parametrization implies inverse"// &
                        " of a physical medium which has zero-value "  // &
                        "coefficient  [grad_model_function]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !> non-shared error
      end if

      !! ---------------------------------------------------------------
      !! 2.) compute gradient w.r.t. what is wanted
      select case(tag_function_wanted)
        case(tag_GRADMODEL_ID        )
          gradient = grad_temp
        case(tag_GRADMODEL_ID_SQ     )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) gradient = grad_temp*(1./(2*model))**power
        case(tag_GRADMODEL_INV       )
          gradient = grad_temp*(-(model**2))**power
        case(tag_GRADMODEL_INV_SQ    )
          gradient = grad_temp*(-(model**3/2.))**power
        case(tag_GRADMODEL_SQRT      )
          gradient =  grad_temp*(2.*sqrt(model))**power
        case(tag_GRADMODEL_SQRT_INV  )
          gradient = grad_temp*(-(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          gradient = model* grad_temp
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(model / (2.*log(model)))**power
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model * (log(model))**2)**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model*(log(model))**3 /2.)**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      deallocate(grad_temp)
    end if

    return
  end subroutine grad_model_function_solo_cx4
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> change gradient depending on the current model function
  !> ID         =>
  !> ID_SQ      => G(dx²)       =  2 x            G(dx)
  !> INV        => G(d1/x)      = -1/x²           G(dx)
  !> INV_SQ     => G(d1/x²)     = -2/x³           G(dx)
  !> SQRT       => G(dsqrt(x))  = 1/(2sqrt(x))    G(dx)
  !> SQRT_INV   => G(1/dsqrt(x))=-1/2 x^{-3/2}    G(dx)
  !> LOG        => G(dlog(x))   = 1/x             G(dx)
  !> LOG_SQ     => G(dlog(x)²)  = 2/x log(x)      G(dx)
  !> LOG_INV    => G(d1/log(x)) =-1/x log(x)^{-2} G(dx)
  !> LOG_INV_SQ => G(d1/log(x)²)=-2/x log(x)^{-3} G(dx)
  !! -------------------------------------------------------------------
  !
  !> @param[in]    model            : model values
  !> @param[inout] gradient         : gradient values
  !> @param[in]    ncoeff           : number of values in the arrays
  !> @param[in]    tag_function_in  : tag for the input gradient
  !> @param[in]    tag_function_w   : tag for the wanted gradient
  !> @param[in]    flag_pseudoh     : pseudo-hessian uses squared quantities!
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine grad_model_function_solo_cx8(model,gradient,ncoeff,        &
                                          tag_function_in,              &
                                          tag_function_wanted,ctx_err,  &
                                          o_flag_pseudoh)
    implicit none

    integer                       ,intent(in)   :: tag_function_in
    integer                       ,intent(in)   :: tag_function_wanted
    real(kind=8)                  ,intent(in)   :: model
    complex(kind=8)               ,intent(inout):: gradient(:)
    integer                       ,intent(in)   :: ncoeff
    logical,optional              ,intent(in)   :: o_flag_pseudoh
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                      :: ierr,power
    complex(kind=8), allocatable :: grad_temp(:)
    real(kind=8)                 :: tol=tiny(1.d0)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    ierr=0
    if(abs(model) < tol) ierr = -8
    
    power=1
    if(present(o_flag_pseudoh)) then
      if(o_flag_pseudoh) power=2
    end if

    !! -----------------------------------------------------------------
    !! 0.) only if we do not have it already
    !! -----------------------------------------------------------------
    if(tag_function_in .ne. tag_function_wanted) then
      !! 1.) compute gradient w.r.t. identity
      allocate(grad_temp(ncoeff))
      grad_temp = 0.0

      select case(tag_function_in)
        case(tag_GRADMODEL_ID        )
          grad_temp = gradient
        case(tag_GRADMODEL_ID_SQ     )
          grad_temp = gradient * (2. * model)**power
        case(tag_GRADMODEL_INV       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model**2)**power
        case(tag_GRADMODEL_INV_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model**3)**power
        case(tag_GRADMODEL_SQRT      )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(1./(2.*sqrt(model)))**power
        case(tag_GRADMODEL_SQRT_INV  )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) grad_temp = gradient*(1./model)**power
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = 2./model* log(model) * gradient
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-1./model*(log(model))**(-2))**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            grad_temp = gradient*(-2./model* (log(model))**(-3))**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      !! ---------------------------------------------------------------
      !! ERROR CHECK
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: the parametrization implies inverse"// &
                        " of a physical medium which has zero-value "  // &
                        "coefficient  [grad_model_function]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !> non-shared error
      end if

      !! ---------------------------------------------------------------
      !! 2.) compute gradient w.r.t. what is wanted
      select case(tag_function_wanted)
        case(tag_GRADMODEL_ID        )
          gradient = grad_temp
        case(tag_GRADMODEL_ID_SQ     )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) gradient = grad_temp*(1./(2*model))**power
        case(tag_GRADMODEL_INV       )
          gradient = grad_temp*(-(model**2))**power
        case(tag_GRADMODEL_INV_SQ    )
          gradient = grad_temp*(-(model**3/2.))**power
        case(tag_GRADMODEL_SQRT      )
          gradient =  grad_temp*(2.*sqrt(model))**power
        case(tag_GRADMODEL_SQRT_INV  )
          gradient = grad_temp*(-(2.*model**(-1.5)))**power
        case(tag_GRADMODEL_LOG       )
          gradient = model* grad_temp
        case(tag_GRADMODEL_LOG_SQ    )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(model / (2.*log(model)))**power
        case(tag_GRADMODEL_LOG_INV   )
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model * (log(model))**2)**power
        case(tag_GRADMODEL_LOG_INV_SQ)
          ctx_err%ierr = ierr
          if(ctx_err%ierr .eq. 0) &
            gradient = grad_temp*(-model*(log(model))**3 /2.)**power
        case default
          ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                          "[grad_model_function]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          return !> non-shared error
      end select
      deallocate(grad_temp)
    end if

    return
  end subroutine grad_model_function_solo_cx8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Apply the function to an input model (used in l-bfgs and model update)
  !! -------------------------------------------------------------------
  !
  !> @param[in]    ctx_paral        : context parallelism
  !> @param[inout] model            : model values
  !> @param[in]    tag_function     : tag for the function
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine apply_model_function(ctx_paral,model,tag_function,ctx_err)
    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: tag_function
    real(kind=4)     ,allocatable ,intent(inout):: model(:)
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ierr
    real(kind=4)              :: tol=1e-30
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    ierr=0
    if(minval(abs(model)) < tol) then
      ctx_err%ierr = -8
    end if
    call allreduce_min(ctx_err%ierr,ierr,1,ctx_paral%communicator,ctx_err)
    ctx_err%ierr = 0  

    !! -----------------------------------------------------------------
    !! 0.) only if we do not have it already
    !! -----------------------------------------------------------------
    select case(tag_function)
      case(tag_GRADMODEL_ID        )
      case(tag_GRADMODEL_ID_SQ     )
        model = model ** 2
      case(tag_GRADMODEL_INV       )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./model
      case(tag_GRADMODEL_INV_SQ    )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./model**2
      case(tag_GRADMODEL_SQRT      )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = sqrt(model)
      case(tag_GRADMODEL_SQRT_INV  )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./sqrt(model)
      case(tag_GRADMODEL_LOG       )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = log(model)
      case(tag_GRADMODEL_LOG_SQ    )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = log(model) ** 2
      case(tag_GRADMODEL_LOG_INV   )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./log(model)
      case(tag_GRADMODEL_LOG_INV_SQ)
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./(log(model)**2)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                        "[apply_model_function]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! ---------------------------------------------------------------
    !! ERROR CHECK
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: the parametrization implies        "// &
                      " some exceptional values [apply_model_function]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    return
  end subroutine apply_model_function

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Apply the inverse of the selected function to an input model 
  !> (used in model update), for instance, if the model is parametrized
  !> with 1/m² then we output sqrt(1/input)
  !! -------------------------------------------------------------------
  !
  !> @param[in]    ctx_paral        : context parallelism
  !> @param[inout] model            : model values
  !> @param[in]    tag_function     : tag for the function
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine apply_model_function_inv(ctx_paral,model,tag_function,ctx_err)
    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: tag_function
    real(kind=4)     ,allocatable ,intent(inout):: model(:)
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                   :: ierr
    real(kind=4)              :: tol=1e-30
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    ierr=0
    if(minval(abs(model)) < tol) then
      ctx_err%ierr = -8
    end if
    call allreduce_min(ctx_err%ierr,ierr,1,ctx_paral%communicator,ctx_err)
    ctx_err%ierr = 0  

    !! -----------------------------------------------------------------
    !! 0.) only if we do not have it already
    !! -----------------------------------------------------------------
    select case(tag_function)
      case(tag_GRADMODEL_ID        )
      case(tag_GRADMODEL_ID_SQ     )
        model = sqrt(model)
      case(tag_GRADMODEL_INV       )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./model
      case(tag_GRADMODEL_INV_SQ    )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./sqrt(model)
      case(tag_GRADMODEL_SQRT      )
        model = model**2
      case(tag_GRADMODEL_SQRT_INV  )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = 1./(model)**2
      case(tag_GRADMODEL_LOG       )
        model = exp(model)
      case(tag_GRADMODEL_LOG_SQ    )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = exp(sqrt(model))
      case(tag_GRADMODEL_LOG_INV   )
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = exp(1./model)
      case(tag_GRADMODEL_LOG_INV_SQ)
        ctx_err%ierr = ierr
        if(ctx_err%ierr .eq. 0) model = exp(1./model**2)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized function for model " //&
                        "[apply_model_function_inv]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! ---------------------------------------------------------------
    !! ERROR CHECK
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg   = "** ERROR: the parametrization implies        "// &
                      " some exceptional values [apply_model_function_inv]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    return
  end subroutine apply_model_function_inv

end module m_grad_model_function
