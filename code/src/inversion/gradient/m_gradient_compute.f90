!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_gradient_compute.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module compute the gradient with respect to the appropriate
!> parameters
!
!------------------------------------------------------------------------------
module m_gradient_compute

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: RKIND_MAT
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_get_piecewise_constant,     &
                                      model_get_pdof
  use m_ctx_field,              only: t_field
  use m_ctx_misfit_function,    only: t_misfit
  use m_ctx_gradient,           only: t_gradient
  use m_create_gradient,        only: create_gradient
  use m_create_pseudoh,         only: create_pseudoh
  use m_parameter_gradient,     only: parameter_gradient
  use m_grad_model_function,    only: grad_model_function,tag_GRADMODEL_ID
  use m_domain_restriction,     only: domain_restriction_per_cell,      &
                                      domain_restriction_per_subcell
  use m_ctx_model_representation,                                       &
                                only: tag_MODEL_PPOLY, tag_MODEL_SEP,   &
                                      tag_MODEL_PCONSTANT, tag_MODEL_DOF
  implicit none

  private

  public :: gradient_compute

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the gradient of the misfit functional using the forward
  !> and backward field. 
  !> The gradient depends on the set of parameters selected, and on the
  !> function to apply onto them.
  !> In addition, we compute the pseudo-hessian at the same time.
  !
  !> @param[in]   ctx_paral     : ctx_paral
  !> @param[in]   ctx_disc      : discretization context
  !> @param[in]   ctx_domain    : domain context
  !> @param[in]   ctx_model     : model context
  !> @param[in]   ctx_field_fwd : forward field context
  !> @param[in]   ctx_field_bwd : backward field context
  !> @param[in]   angular_freq  : angular frequency
  !> @param[inout] ctx_grad      : gradient context
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine gradient_compute(ctx_paral,ctx_disc,ctx_domain,ctx_model,  &
                              ctx_field_fwd,ctx_field_bwd,angular_freq, &
                              ctx_grad,ctx_err)

    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    type(t_discretization)        ,intent(in)   :: ctx_disc
    type(t_domain)                ,intent(in)   :: ctx_domain
    type(t_model)                 ,intent(in)   :: ctx_model
    type(t_field)                 ,intent(in)   :: ctx_field_fwd
    type(t_field)                 ,intent(in)   :: ctx_field_bwd
    complex(kind=8)               ,intent(in)   :: angular_freq
    type(t_gradient)              ,intent(inout):: ctx_grad
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                              :: i_grad, ndof_gb
    real(kind=4)           ,allocatable  :: grad_ref(:,:),grad_loc(:,:)
    real(kind=4)           ,allocatable  :: pseudoh (:,:),pseudoh_loc(:,:)
    real(kind=4)           ,allocatable  :: model_temp(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! 1) compute the gradient with respect to pre-defined parameter
    !!    e.g.:
    !!    - acoustic    => (1/kappa, rho)
    !!    - elastic iso => (lambda, mu, rho)
    !! -----------------------------------------------------------------
    allocate(grad_ref(ctx_grad%n_coeff_loc,ctx_model%n_model))
    grad_ref=0.0
    call create_gradient(ctx_paral,ctx_domain,ctx_disc,                 &
                         ctx_model,ctx_field_fwd,ctx_field_bwd,         &
                         angular_freq,grad_ref,ctx_grad%tag_metric,     &
                         ctx_err)

     ! ---------------------------------------------------------------->
     !! in the case where we need the pseudo-hessian, we compute it 
     !!         J(m) = 1/2 || Ru - d ||²
     !!   dmJ    = dm(u)* R* (Ru - d)
     !!   d²m(J) = dm(u)* R* R dm(u)  + d²m(u)* R* (Ru - d).
     !! Gauss-Newton assumes that d²m(u) = 0
     !!   HGN  = dm(u)* R* R dm(u)
     !!
     !! we replace with dm(u) = - P^{-1} dm(P) u to obtain  
     !!   HGN  = (P^{-1} dm(P) u)* R* R (P^{-1} dm(P) u)
     !!   HGN  = (dm(P)u)* P^{-*} R* R P^{-1} dm(P) u
     !!
     !! The pseudo-Hessian assumes that P^{-*} R* R P^{-1} = Identity:
     !! PSEUDO-HESSIAN = (dm(P)u)* (dm(P)u)
     if(ctx_grad%flag_pseudo_hessian_scale) then     
       allocate(pseudoh(ctx_grad%n_coeff_loc,ctx_model%n_model))
       pseudoh =0.0
       !! --------------------------------------------------------------
       !! need to be careful with the choice of misfit function
       !! so we use two additional array: and compute
       !!   (dm(P).alpha)* (dm(P).alpha) / (beta* beta)
       ! --------------------------------------------------------->
       !! WE ALWAYS TAKE THE LEAST-SQUARES PSEUDO-HESSIAN, WHICH 
       !! SEEMS TO GIVE THE BEST RESULTS 
       ! n = ctx_field_fwd%n_dofsol_loc * ctx_field_fwd%n_src
       ! allocate(pseudo_field(n,ctx_field_fwd%n_field))
       ! allocate(pseudo_scale(n,ctx_field_fwd%n_field))
       ! pseudo_field= ctx_field_fwd%field
       ! pseudo_scale= 1.0
       ! --------------------------------------------------------->
       ! call create_pseudoh(ctx_paral,ctx_domain,ctx_disc,ctx_model,   &
       !                     ctx_field_fwd,ctx_field_fwd%field,         &
       !                     pseudo_scale,angular_freq,pseudoh,ctx_err)
       call create_pseudoh(ctx_paral,ctx_domain,ctx_disc,ctx_model,     &
                           ctx_field_fwd,angular_freq,pseudoh,ctx_err)
       ! deallocate(pseudo_field)
       ! deallocate(pseudo_scale)
     end if

     !! -----------------------------------------------------------------
     !! -----------------------------------------------------------------
     !! 2) from the pre-defined parameters, we need the one required
     !!    a) get the derivative w.r.t. appropriate physical
     !!    b) transform with the appropriate function
     !! e.g. from (1/kappa, rho) we may want (log(cp),1/impendance)
     !! -----------------------------------------------------------------
     allocate(grad_loc   (ctx_grad%n_coeff_loc,ctx_model%n_model))
     grad_loc   =0.0
     call parameter_gradient(ctx_model,grad_ref,grad_loc,               &
                             ctx_grad%param_name,angular_freq,ctx_err)
     deallocate(grad_ref)
     if(ctx_grad%flag_pseudo_hessian_scale) then
       allocate(pseudoh_loc(ctx_grad%n_coeff_loc,ctx_model%n_model))
       pseudoh_loc=0.0
       call parameter_gradient(ctx_model,pseudoh,pseudoh_loc,           &
                               ctx_grad%param_name,angular_freq,ctx_err,&
                               o_flag_pseudoh=.true.)
       deallocate(pseudoh)
     end if
     !! ----------------------------------------------------------------
     !! At this point they are computed with respect to the identity
     !! ----------------------------------------------------------------
     !! next compute the appropriate function
     allocate(model_temp(ctx_grad%n_coeff_loc))
     model_temp = 0.0
     do i_grad = 1, ctx_grad%n_grad
       !! get current model: only works with piecewise-constant for now 
       !! only works if we have a picewise constant parameter ..........
       select case(trim(adjustl(ctx_model%format_disc)))
         case(tag_MODEL_PCONSTANT) 
           call model_get_piecewise_constant(ctx_model,model_temp,      &
                      trim(adjustl(ctx_grad%param_name(i_grad))),       &
                      angular_freq,ctx_err)
         case(tag_MODEL_DOF) 
           if(allocated(model_temp)) deallocate(model_temp)
           call model_get_pdof(ctx_domain,ctx_model,model_temp,         &
                          trim(adjustl(ctx_grad%param_name(i_grad))),   &
                          angular_freq,ctx_grad%param_dof%order,        &
                          ndof_gb,ctx_err)
         case default
           ctx_err%msg   = "** ERROR: model parametrization is not " // &
                           " supported [gradient_compute] **"
           ctx_err%ierr  = -1
           ctx_err%critic=.true.
           call raise_error(ctx_err)
       end select

       !! transform the gradient accordingly
       call grad_model_function(ctx_paral,model_temp,grad_loc(:,i_grad),&
                                ctx_grad%n_coeff_loc,tag_GRADMODEL_ID,  &
                                ctx_grad%param_func(i_grad),ctx_err)
       !! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       !! WARNING: 
       !!   the pseudo-hessian is always established with respect to the
       !!   identity parameter. We do NOT employ any model scaling 
       !!   because we want the pseudo-hessian to have the lowest 
       !!   values on the bottom.
       !! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       !if(ctx_grad%flag_pseudo_hessian_scale) then
       !  call grad_model_function(ctx_paral,model_temp,                &
       !  pseudoh_loc(:,i_grad),ctx_grad%n_coeff_loc,tag_GRADMODEL_ID,  &
       !  ctx_grad%param_func(i_grad),ctx_err,o_flag_pseudoh=.true.)
       !end if
       !! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     end do
     !! -----------------------------------------------------------------

     !! -----------------------------------------------------------------
     !! 4) Update global gradient
     !!
     !! -----------------------------------------------------------------
     !! restrict the area of interest
     if(ctx_grad%flag_box) then
       select case(trim(adjustl(ctx_grad%model_rep)))
         case(tag_MODEL_PCONSTANT) 
           call domain_restriction_per_cell(                             &
                               ctx_disc%dim_domain,ctx_domain,grad_loc,  &
                               ctx_grad%n_coeff_loc,ctx_grad%xmin,       &
                               ctx_grad%xmax,ctx_grad%ymin,ctx_grad%ymax,&
                               ctx_grad%zmin,ctx_grad%zmax,              &
                               ctx_grad%rmin,ctx_grad%rmax,              &
                               ctx_grad%r0,ctx_err)
         case(tag_MODEL_DOF) 
           call domain_restriction_per_subcell(                          &
                               ctx_disc%dim_domain,ctx_domain,grad_loc,  &
                               ctx_domain%n_cell_loc,                    &
                               ctx_grad%param_dof%ndof,                  &
                               ctx_grad%xmin,ctx_grad%xmax,              &
                               ctx_grad%ymin,ctx_grad%ymax,              &
                               ctx_grad%zmin,ctx_grad%zmax,              &
                               ctx_grad%rmin,ctx_grad%rmax,              &
                               ctx_grad%r0,ctx_err)
         case default
           ctx_err%msg   = "** ERROR: model parametrization is not " // &
                           " supported for domain restriction [grad_compute] **"
           ctx_err%ierr  = -1
           ctx_err%critic=.true.
           call raise_error(ctx_err)
       end select
     end if

     do i_grad = 1, ctx_grad%n_grad
       !! update the global gradient and pseudo-hessian
       ctx_grad%grad(:,i_grad) = ctx_grad%grad(:,i_grad) + grad_loc(:,i_grad)
       if(ctx_grad%flag_pseudo_hessian_scale) then
         ctx_grad%pseudoh(:,i_grad) = ctx_grad%pseudoh(:,i_grad) +      &
                                           pseudoh_loc(:,i_grad)
       end if
     end do
     deallocate(grad_loc)
     if(allocated(pseudoh_loc)) deallocate(pseudoh_loc)
     !! -----------------------------------------------------------------

    return
  end subroutine gradient_compute

end module m_gradient_compute
