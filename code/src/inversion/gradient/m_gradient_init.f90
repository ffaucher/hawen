!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_gradient_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize the gradient information
!
!------------------------------------------------------------------------------
module m_gradient_init
  
  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_read_parameters_gradient, only : read_parameters_gradient
  use m_ctx_model,                only : t_model
  use m_ctx_gradient,             only : t_gradient
  use m_print_gradient,           only : print_gradient_info
  use m_model_list,               only : model_formalism
  use m_grad_metric,              only : grad_metric_formalism
  use m_grad_model_function,      only : grad_model_function_formalism, &
                                         tag_GRADMODEL_IGNORE
  use m_ctx_model_representation, only: tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_PCONSTANT, tag_MODEL_DOF
  implicit none

  private  
  public :: gradient_init
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init gradient context information
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] ctx_grad      : gradient context
  !> @param[inout] parameter_file : parameter file
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine gradient_init(ctx_paral,ctx_model,ctx_grad,parameter_file,ctx_err)
  
    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_model)      ,intent(in)   :: ctx_model
    type(t_gradient)   ,intent(inout):: ctx_grad
    character(len=*)   ,intent(in)   :: parameter_file
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    character(len=32)  ,allocatable :: param_nameloc   (:),string_buff(:)
    character(len=32)  ,allocatable :: function_nameloc(:)
    character(len=512)              :: str_grad_metric
    character(len=512)              :: str_model_param
    integer                         :: model_param_doforder
    integer            ,allocatable :: buff_order(:)
    integer                         :: counter_ok, counter_ko, k
    
    ctx_err%ierr = 0

    if(ctx_model%n_model <= 0) then
      ctx_err%msg   = "** ERROR: there is 0 model for gradient [grad_init]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    allocate(ctx_grad%param_name(ctx_model%n_model))
    allocate(param_nameloc      (ctx_model%n_model))
    allocate(function_nameloc   (ctx_model%n_model))
    allocate(ctx_grad%param_func(ctx_model%n_model))
    ctx_grad%param_name = ''
    ctx_grad%param_func = 0  

    !! read input information 
    call read_parameters_gradient(parameter_file,ctx_model%n_model,     &
                                  param_nameloc, function_nameloc,      &
                                  str_grad_metric,                      &
                                  str_model_param,model_param_doforder, &
                                  ctx_grad%flag_box,ctx_grad%xmin,      &
                                  ctx_grad%xmax,ctx_grad%ymin,          &
                                  ctx_grad%ymax,ctx_grad%zmin,          &
                                  ctx_grad%zmax,                        &
                                  ctx_grad%rmin,ctx_grad%rmax,          &
                                  ctx_grad%r0,                          &
                                  ctx_grad%flag_pseudo_hessian_scale,   &
                                  ctx_grad%flag_debug_save,ctx_err)

    !! gradient representation (e.g., piecewise-constant, dof) 
    ctx_grad%model_rep = ''
    select case(trim(adjustl(str_model_param)))
      case('piecewise-constant','PIECEWISE-CONSTANT','constant','CONSTANT')
        ctx_grad%model_rep = tag_MODEL_PCONSTANT
        !! get input number of coeffs
        ctx_grad%n_coeff_loc= ctx_model%n_cell_loc
        ctx_grad%n_coeff_gb = ctx_model%n_cell_gb
    
      case('degree-of-freedom','dof','DOF','Degree-of-freedom')
        ctx_grad%model_rep = tag_MODEL_DOF
        !! note that the order must be the same as the model, it will 
        !! be checked later on.
        ctx_grad%param_dof%order = model_param_doforder
        if(ctx_grad%param_dof%order > 1) then
          ctx_err%msg   = "** ERROR: Gradient in Lagrange basis"//&
                          " function only works with order 1 " // &
                          "for now [gradient_init] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

        select case(ctx_model%dim_domain)
          case(1)
            ctx_grad%param_dof%ndof = model_param_doforder+1
          case(2)
            ctx_grad%param_dof%ndof = (model_param_doforder+2)      &
                                     *(model_param_doforder+1)/2
          case(3)
            ctx_grad%param_dof%ndof = (model_param_doforder+3)      &
                                     *(model_param_doforder+2)      &
                                     *(model_param_doforder+1)/6
          case default
            ctx_err%msg   = "** ERROR: Unrecognized Dimension [gradient_init] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
        end select
        
        ctx_grad%n_coeff_loc= ctx_model%n_cell_loc * ctx_grad%param_dof%ndof
        ctx_grad%n_coeff_gb = ctx_model%n_cell_gb  * ctx_grad%param_dof%ndof
        
        
      case default
        ctx_err%msg   = "** ERROR: model representation for gradient "//&
                        "not supported [gradient_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    if(trim(adjustl(ctx_model%format_disc)) .ne.                        &
       trim(adjustl(ctx_grad%model_rep))) then
      ctx_err%msg   = "** ERROR: currently the same representation "//&
                      "is required for the forward problem and "    //&
                      "inversion [gradient_init] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
             
    !! we convert the input names in TAG from the current operator
    !! models. This subroutine is in 
    !! src/discretization-operator/operator/PROPAGATOR/model
    call model_formalism(ctx_model%dim_domain,ctx_model%flag_viscous,   &
                         ctx_model%viscous_model,ctx_model%n_model,     &
                         param_nameloc,ctx_grad%param_name,ctx_err)

    !! -----------------------------------------------------------------
    !! convert the metric into tag
    !! -----------------------------------------------------------------
    ctx_grad%n_grad = 0
    call grad_metric_formalism(str_grad_metric,ctx_grad%tag_metric,ctx_err)


    !! -----------------------------------------------------------------
    !! convert the function choice into TAG, and count the 
    !! actual number of gradients
    !! -----------------------------------------------------------------
    ctx_grad%n_grad = 0
    call grad_model_function_formalism(ctx_model%n_model,function_nameloc, &
                                       ctx_grad%param_func,ctx_grad%n_grad,&
                                       ctx_err)
    if(ctx_grad%n_grad <= 0) then
      ctx_err%msg   = "** ERROR: No model to be inverted [grad_init]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! -----------------------------------------------------------------
    !! reorganize to put the non-inverted model at the end 
    !! -----------------------------------------------------------------
    allocate(string_buff(ctx_model%n_model))
    allocate(buff_order (ctx_model%n_model))
    counter_ok =  0
    counter_ko = -1
    do k=1,ctx_model%n_model
      if(ctx_grad%param_func(k) .eq. tag_GRADMODEL_IGNORE) then
        counter_ko=counter_ko+1
        string_buff(ctx_model%n_model-counter_ko) = &
                                  trim(adjustl(ctx_grad%param_name(k)))
        buff_order(ctx_model%n_model-counter_ko)  = ctx_grad%param_func(k)
                                  
      else
        counter_ok=counter_ok+1
        string_buff(counter_ok) = trim(adjustl(ctx_grad%param_name(k)))              
        buff_order (counter_ok) = ctx_grad%param_func(k)
      end if
    end do
    ctx_grad%param_name = string_buff
    ctx_grad%param_func = buff_order
    deallocate(param_nameloc   )
    deallocate(function_nameloc)    
    deallocate(string_buff     )
    deallocate(buff_order      )
    !! -----------------------------------------------------------------

    !! allocations
    allocate(ctx_grad%grad(ctx_grad%n_coeff_loc,ctx_grad%n_grad))
    ctx_grad%grad = 0.0
    if(ctx_grad%flag_pseudo_hessian_scale) then
      allocate(ctx_grad%pseudoh(ctx_grad%n_coeff_loc,ctx_grad%n_grad))
      ctx_grad%pseudoh = 0.0
    end if

    !! print infos here
    call print_gradient_info(ctx_paral,ctx_model%n_model,ctx_grad%n_grad,  &
                             ctx_grad%param_name,ctx_grad%param_func,      &
                             ctx_grad%tag_metric,                          &
                             ctx_grad%flag_box,ctx_grad%xmin,ctx_grad%xmax,&
                             ctx_grad%ymin,ctx_grad%ymax,ctx_grad%zmin,    &
                             ctx_grad%zmax,ctx_grad%rmin,ctx_grad%rmax,    &
                             ctx_grad%r0,                                  &
                             ctx_grad%flag_pseudo_hessian_scale,ctx_err)

    return
  end subroutine gradient_init

end module m_gradient_init
