!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_gradient_save.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save the gradients
!
!------------------------------------------------------------------------------
module m_gradient_save
  
  use m_raise_error,              only : raise_error, t_error
  use m_define_precision,         only : IKIND_MESH
  use m_reduce_sum,               only : reduce_sum
  use m_ctx_parallelism,          only : t_parallelism
  use m_ctx_discretization,       only : t_discretization
  use m_ctx_domain,               only : t_domain
  use m_ctx_gradient,             only : t_gradient
  use m_ctx_io,                   only : t_io
  use m_array_save,               only : array_save
  use m_io_filename,              only : io_filename
  use m_project_array,            only : project_array_local_cell_to_global
  use m_array_save_binary,        only : array_save_binary
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,            &
                                        tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_DOF
  implicit none

  private  
  public :: gradient_save
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> save the gradient 
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   ctx_disc      : discretization context
  !> @param[in]   ctx_domain    : domain context
  !> @param[in]   ctx_grad      : gradient context
  !> @param[in]   ctx_io        : io context
  !> @param[in]   iter          : current iteration number
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine gradient_save(ctx_paral,ctx_disc,ctx_domain,ctx_grad,      &
                           ctx_io,iter,ctx_err)
  
    implicit none
    
    type(t_parallelism)     ,intent(in)   :: ctx_paral
    type(t_discretization)  ,intent(in)   :: ctx_disc
    type(t_domain)          ,intent(in)   :: ctx_domain
    type(t_gradient)        ,intent(in)   :: ctx_grad
    type(t_io)              ,intent(in)   :: ctx_io
    integer                 ,intent(in)   :: iter
    type(t_error)           ,intent(out)  :: ctx_err
    !! -----------------------------------------------------------------
    !! local
    character(len=128),allocatable :: name_loc(:)
    character(len=128),allocatable :: arrayname(:)
    character(len=128)             :: arrayname_gb
    integer                        :: i_grad
    real(kind=4)      ,allocatable :: grad_loc_gb(:),grad_gb(:)
    !! the gradient only cares for the iteration
    real   (kind=8)                :: freq(2)=0.
    integer(kind=4)                :: mode   =0
    logical                        :: flag_mode = .false.
    logical                        :: flag_freq = .false.
    !! for dof representation
    integer(kind=IKIND_MESH)       :: i_cell, igb1, igb2, iloc1, iloc2
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0
    allocate(name_loc(ctx_grad%n_grad))
    do i_grad = 1, ctx_grad%n_grad
      name_loc(i_grad) = "gradient-"//trim(adjustl(ctx_grad%param_name(i_grad)))
    end do 
    allocate(arrayname(ctx_grad%n_grad))
    arrayname=''
    call io_filename(arrayname,arrayname_gb,name_loc,"gradient",        &
                     ctx_grad%n_grad,flag_freq,flag_mode,freq,mode,     &
                     ctx_err,o_iter=iter)


    !! in case of debug mode, we save the gradient at every iteration
    !! in a visualy acceptable way (e.g., paraview, sep)
    if(ctx_grad%flag_debug_save) then
      select case(trim(adjustl(ctx_grad%model_rep)))
        case(tag_MODEL_PCONSTANT)
          call array_save(ctx_paral,ctx_domain,ctx_disc,                    &
                          ctx_io%save_unstructured,ctx_io%save_structured,  &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          ctx_grad%grad,ctx_grad%n_grad,'cell',             &
                          ctx_io%workpath_grad,arrayname,arrayname_gb,ctx_err)
        case(tag_MODEL_DOF)
          call array_save(ctx_paral,ctx_domain,ctx_disc,                    &
                          ctx_io%save_unstructured,ctx_io%save_structured,  &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          ctx_grad%grad,ctx_grad%n_grad,'dof-orderfix',     &
                          ctx_io%workpath_grad,arrayname,arrayname_gb,      &
                          ctx_err,o_cste_order=ctx_grad%param_dof%order)
        case default
          ctx_err%msg   = "** ERROR: we cannot save the "//             &
                          trim(adjustl(ctx_grad%model_rep))//           &
                          " gradient representation [grad_save] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if

    !! -----------------------------------------------------------------
    !! save the binary file with the gradient values, that
    !! can be re-used for the search direction (i.e. nlcg)
    !! -----------------------------------------------------------------
    if(ctx_io%flag_bin_grad) then
      select case(trim(adjustl(ctx_grad%model_rep)))
        !! piecewise-constant ------------------------------------------
        case(tag_MODEL_PCONSTANT)
          !! global size allocation, only master sums up
          allocate(grad_loc_gb(ctx_domain%n_cell_gb))
          if(ctx_paral%master) then
            allocate(grad_gb(ctx_grad%n_coeff_gb))
          else
            allocate(grad_gb(1))
          end if
          
          grad_loc_gb = 0.0 ; grad_gb=0.0
          do i_grad = 1, ctx_grad%n_grad
            call project_array_local_cell_to_global(ctx_domain,         &
                                   ctx_grad%grad(:,i_grad),grad_loc_gb, &
                                   ctx_err)
            !! master saves in binary now
            call reduce_sum(grad_loc_gb,grad_gb,ctx_grad%n_coeff_gb,0,  &
                            ctx_paral%communicator,ctx_err)
            call array_save_binary(ctx_paral,grad_gb,ctx_grad%n_coeff_gb,&
                                   ctx_io%workpath_grad,                &
                                   arrayname(i_grad),ctx_err)
          end do
        !! multiple constant per cells ---------------------------------
        case(tag_MODEL_DOF)
          !! global size allocation, only master sums up
          allocate(grad_loc_gb(ctx_grad%n_coeff_gb))
          if(ctx_paral%master) then
            allocate(grad_gb(ctx_grad%n_coeff_gb))
          else
            allocate(grad_gb(1))
          end if

          !! fill the appropriate positions
          do i_grad = 1, ctx_grad%n_grad
            grad_loc_gb = 0.0 ; grad_gb=0.0
            do i_cell = 1, ctx_domain%n_cell_loc
              igb1 = (ctx_domain%index_loctoglob_cell(i_cell)-1)        &
                    * ctx_grad%param_dof%ndof + 1
              igb2 = igb1 + ctx_grad%param_dof%ndof - 1
              iloc1= (i_cell-1) * ctx_grad%param_dof%ndof + 1
              iloc2= iloc1 + ctx_grad%param_dof%ndof - 1
              grad_loc_gb(igb1:igb2) = ctx_grad%grad(iloc1:iloc2,i_grad)
            end do

            !! master saves in binary now
            call reduce_sum(grad_loc_gb,grad_gb,ctx_grad%n_coeff_gb,0,  &
                            ctx_paral%communicator,ctx_err)
            call array_save_binary(ctx_paral,grad_gb,                   &
                                   ctx_grad%n_coeff_gb,                 &
                                   ctx_io%workpath_grad,                &
                                   arrayname(i_grad),ctx_err)
          end do

        !! default error -----------------------------------------------
        case default
          ctx_err%msg   = "** ERROR: we cannot save the "//             &
                          trim(adjustl(ctx_grad%model_rep))//           &
                          " gradient representation in binary [grad_save] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine gradient_save

end module m_gradient_save
