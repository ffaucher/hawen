!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_gradient.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for the gradient
!
!------------------------------------------------------------------------------
module m_print_gradient

  use m_ctx_parallelism,      only : t_parallelism
  use m_raise_error,          only : t_error, raise_error
  use m_grad_model_function,  only : grad_model_function_naming
  use m_grad_metric,          only : grad_metric_naming
  use m_print_basics,         only : separator,indicator
  implicit none
  
  private
  public :: print_gradient_info
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the gradient
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   n_model         : number of models in equation 
  !> @param[in]   n_grad          : number of parameters to inverse
  !> @param[in]   param_name      : name of all parameters
  !> @param[in]   tag_function    : parameter function to inverse
  !> @param[in]   tag_metric      : metric for gradient inner product
  !> @param[in]   flag_box        : indicate if restricted area
  !> @param[in]   {xyzr}min       : possible limitation min
  !> @param[in]   {xyzr}max       : possible limitation max
  !> @param[in]   flag_pseudoh    : pseudo-hessian scaling
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine print_gradient_info(ctx_paral,n_model,n_grad,param_name,   &
                                 tag_function,tag_metric,flag_box,xmin, &
                                 xmax,ymin,ymax,zmin,zmax,rmin,rmax,r0, &
                                 flag_pseudoh,ctx_err)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: n_model,n_grad
    character(len=*),allocatable  ,intent(in)   :: param_name  (:)
    integer         ,allocatable  ,intent(in)   :: tag_function(:)
    integer                       ,intent(in)   :: tag_metric
    logical                       ,intent(in)   :: flag_box
    real                          ,intent(in)   :: xmin,xmax,ymin,ymax
    real                          ,intent(in)   :: rmin,rmax,zmin,zmax
    real                          ,intent(in)   :: r0(3)
    logical                       ,intent(in)   :: flag_pseudoh
    type(t_error)                 ,intent(out)  :: ctx_err
    !!
    character(len=128) :: str
    integer            :: k

    ctx_err%ierr=0
    
    if(ctx_paral%master) then
      !! model taken
      str= trim(adjustl(param_name(1)))
      do k=2,n_model
        str = trim(adjustl(str)) // ', ' // param_name(k)
      end do
      write(6,'(3a)') "- models for FWI are (", trim(adjustl(str)),     &
                      ") with parametrization: "
    end if
      
    !! for all model, we indicate the function
    do k=1,n_model
      call grad_model_function_naming(tag_function(k),param_name(k),str,ctx_err)
      if(ctx_paral%master) write(6,'(a)') "- " // trim(adjustl(str))
    end do
     
    !! total number of model inverted
    if(ctx_paral%master) then
      write(6,'(a,i0,a)') "- number of models inverted  : ", n_grad
      if(flag_pseudoh) &
      write(6,'(a)') "- pseudo-hessian scaling"
    end if
    
    !! print gradient metric 
    call grad_metric_naming(tag_metric,str,ctx_err)
    if(ctx_paral%master) then 
      write(6,'(a)') "- grad. inner product metric : " // trim(adjustl(str))
    endif
    
    !! possible bounding box for gradient 
    if(flag_box .and. ctx_paral%master) then
      write(6,'(a,es10.2,a,es10.2)') &
        "- gradient bounding box X    : ", xmin, " x ", xmax
      write(6,'(a,es10.2,a,es10.2)') &
        "- gradient bounding box Y    : ", ymin, " x ", ymax
      write(6,'(a,es10.2,a,es10.2)') &
        "- gradient bounding box Z    : ", zmin, " x ", zmax
      write(6,'(a,es10.2,a,es10.2,a,es10.2,a)') &
        "- gradient bounding radius  r0: (", r0(1), ",", r0(2), ",", r0(3),")"
      write(6,'(a,es10.2,a,es10.2)') &
        "- gradient bounding rmin/rmax : ", rmin, " ; ", rmax
    end if
    return
  end subroutine print_gradient_info

end module m_print_gradient
