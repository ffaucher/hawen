!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_gradient.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the context for the gradient.
!
!------------------------------------------------------------------------------
module m_ctx_gradient

  !! for model represented per dof.
  use m_array_types,            only: t_array1d_real, array_deallocate, &
                                      array_allocate
                                      
  implicit none

  private
  public :: t_gradient, gradient_clean, gradient_reset


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_grad_sub contains info on the gradient
  !> for a parameter represented with several constant
  !> per cell. There is one value per dof position, 
  !> every cell has Ndof values, and a specific order.
  !
  !> utils:
  !> @param[integer]     order_max   : maximal order
  !> @param[integer]     ndof_max    : maximal ndof
  !> @param[integer]     order       : order per cell
  !> @param[integer]     ndof        : ndof per cell
  !> @param[double]      val         : model values
  !---------------------------------------------------------------------
  type t_grad_param_subdof
  
    !! for now we assume constant order on all cells
    integer                              :: order
    !! ndof  per cells
    integer                              :: ndof

  end type t_grad_param_subdof



  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_gradient contains all information for the gradient
  !
  !> @param[integer] n_grad       : number of models in the gradient
  !> @param[integer] n_coeff      : number of coefficients per gradient
  !> @param[string]  param_name   : model name for the gradient
  !> @param[string]  param_func   : funcion for each model 
  !> @param[integer] tag_metric   : indicates the metric for the inner product
  !> @param[real]    grad         : gradient
  !> @param[real]    flag_box     : indicates if restriction box
  !> @param[real]    {x,y,z,r}min : restricted area 
  !> @param[real]    {x,y,z,r}max : restricted area 
  !> @param[real]    r0           : for circular restriction, it is the center
  !
  !----------------------------------------------------------------------------
  type t_gradient

    !! number of parameters for the gradient, and number of coeff per grad
    integer                        :: n_grad
    integer                        :: n_coeff_loc
    integer                        :: n_coeff_gb
    !! metric for the inner product 
    integer                        :: tag_metric
 
    !! model represntation
    character(len=32)              :: model_rep
    
    !! list of gradient model name
    character(len=32), allocatable :: param_name(:)
    !! for every model: the function (e.g. 1/parameter, log(parameter), etc)
    integer          , allocatable :: param_func(:)

    !! gradients (no need for double), size(n_coeff,n_grad)
    real(kind=4)     , allocatable :: grad(:,:)
    !! multiples points per cell
    type(t_grad_param_subdof)      :: param_dof
    
    !! flag to indicate the pseudo-hessian scaling
    logical                        :: flag_pseudo_hessian_scale
    real(kind=4)     , allocatable :: pseudoh(:,:)
    
    !! this indicates some possible restriction on the computational box
    !! e.g. when the water level is known
    logical                        :: flag_box
    real                           :: xmin
    real                           :: xmax
    real                           :: ymin
    real                           :: ymax
    real                           :: zmin
    real                           :: zmax
    !! possibility for circular restriction 
    real                           :: rmin
    real                           :: rmax
    real                           :: r0(3) !! 3D coordinates.
    
    !! this flag indicates if gradients should
    !! be saved in paraview format. Otherwize, we only save a binary 
    logical                        :: flag_debug_save

  end type t_gradient
  !----------------------------------------------------------------------------
  
  
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> reset gradient information to 0
  !
  !> @param[inout] ctx_gradient  : gradient context
  !----------------------------------------------------------------------------
  subroutine gradient_reset(ctx_gradient)
    implicit none
    
    type(t_gradient)    ,intent(inout)  :: ctx_gradient

    ctx_gradient%grad      = 0.0
    if(ctx_gradient%flag_pseudo_hessian_scale) then
      ctx_gradient%pseudoh = 0.0
    end if

    return
  end subroutine gradient_reset

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean gradient information and allocation
  !
  !> @param[inout] ctx_gradient  : gradient context
  !----------------------------------------------------------------------------
  subroutine gradient_clean(ctx_gradient)
    implicit none
    
    type(t_gradient)    ,intent(inout)  :: ctx_gradient

    !! parametrization
    ctx_gradient%param_dof%order = 0
    ctx_gradient%param_dof%ndof  = 0
    
    !! number of parameters for the gradient, and number of coeff per grad
    ctx_gradient%flag_pseudo_hessian_scale=.false.
    ctx_gradient%flag_debug_save          =.false.
    ctx_gradient%tag_metric =0
    ctx_gradient%n_grad     =0
    ctx_gradient%n_coeff_loc=0
    ctx_gradient%n_coeff_gb =0
    ctx_gradient%xmin       =0.0
    ctx_gradient%xmax       =0.0
    ctx_gradient%ymin       =0.0
    ctx_gradient%ymax       =0.0
    ctx_gradient%zmin       =0.0
    ctx_gradient%zmax       =0.0
    ctx_gradient%rmin       =0.0
    ctx_gradient%rmax       =0.0
    ctx_gradient%r0         =0.0
    ctx_gradient%flag_box   =.false.
    ctx_gradient%model_rep  =''
    if(allocated(ctx_gradient%param_name)) deallocate(ctx_gradient%param_name)
    if(allocated(ctx_gradient%param_func)) deallocate(ctx_gradient%param_func)
    if(allocated(ctx_gradient%grad      )) deallocate(ctx_gradient%grad      )  
    if(allocated(ctx_gradient%pseudoh   )) deallocate(ctx_gradient%pseudoh   )

    return
  end subroutine gradient_clean

end module m_ctx_gradient
