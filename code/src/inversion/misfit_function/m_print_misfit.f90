!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_misfit.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for misfit
!
!------------------------------------------------------------------------------
module m_print_misfit

  use m_ctx_parallelism,      only : t_parallelism
  use m_raise_error,          only : t_error, raise_error
  use m_misfit_norms,         only : misfit_tag_naming,                 &
                                     tag_MISFIT_RECIPROCITY_LOG,        &
                                     tag_MISFIT_RECIPROCITY
  use m_print_basics,         only : separator,indicator
  implicit none
  
  private
  public :: print_misfit_info
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the misfit functional
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   niter           : number of optimization iteration
  !> @param[in]   tag_misfit      : misfit norm tag
  !> @param[in]   path_data       : path towards the observations
  !> @param[in]   ncomponent      : number of components in the misfit
  !> @param[in]   str_component   : list of component
  !> @param[in]   nobs_reciprocity: for reciprocity: number of obs.
  !> @param[in]   niter_min       : min number of optimization iteration
  !> @param[in]   tol_stagnate    : stagnation criterion
  !> @param[in]   flag_rcv_offset : receivers offset flag
  !> @param[in]   rcv_offset_x    : offset in x
  !> @param[in]   rcv_offset_y    : offset in y
  !> @param[in]   rcv_offset_z    : offset in z
  !> @param[in]   rcv_offset_rad  : offset in circular direction
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine print_misfit_info(ctx_paral,niter,tag_misfit,path_data,    &
                               ncomponent,str_component,                &
                               nobs_reciprocity,niter_min,tol_stagnate, &
                               flag_rcv_offset,rcv_offset_x,            &
                               rcv_offset_y,rcv_offset_z,rcv_offset_rad,ctx_err)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    character(len=*)              ,intent(in)   :: path_data
    character(len=*),allocatable  ,intent(in)   :: str_component(:)
    integer                       ,intent(in)   :: tag_misfit,ncomponent
    integer                       ,intent(in)   :: nobs_reciprocity
    integer                       ,intent(in)   :: niter,niter_min
    real                          ,intent(in)   :: tol_stagnate
    logical                       ,intent(in)   :: flag_rcv_offset
    real(kind=8)                  ,intent(in)   :: rcv_offset_x(2)
    real(kind=8)                  ,intent(in)   :: rcv_offset_y(2)
    real(kind=8)                  ,intent(in)   :: rcv_offset_z(2)
    real(kind=8)                  ,intent(in)   :: rcv_offset_rad(2)
    type(t_error)                 ,intent(out)  :: ctx_err
    !!
    character(len=128) :: str
    integer            :: k


    ctx_err%ierr=0
    if(ctx_paral%master) then
      write(6,'(2a)')   indicator, " FWI information  "
      write(6,'(2a)') "- observed data path: ", trim(adjustl(path_data))
    end if
    call misfit_tag_naming(tag_misfit,str,ctx_err)
    if(ctx_paral%master) then
      write(6,'(a)') "- " // trim(adjustl(str)) 
    end if
    
    if(ctx_paral%master) then
      !! if reciprocity, we plot the number of observations
      if(tag_misfit .eq. tag_MISFIT_RECIPROCITY .or. &
         tag_misfit .eq. tag_MISFIT_RECIPROCITY_LOG) then
        write(6,'(a,1i0)') "- reciprocity n_obs data     : ",nobs_reciprocity
        
      !! we print all of the components used
      else
        write(6,'(a,i4,a)') "- misfit using     ",ncomponent, " components: "
        str="    - "// trim(adjustl(str_component(1)))
        do k=2, ncomponent
          str = trim(str) // ", "// trim(adjustl(str_component(k)))
        enddo
        write(6,'(a)') trim(adjustl(str))
      end if
      
      write(6,'(a,1i0)')    "- number of iterations       : ",niter
      if(niter_min < niter ) then
        write(6,'(a,1i0)')    "- min number of iterations   : ",niter_min
        write(6,'(a,es12.4)') "- stagnation criterion       : ",tol_stagnate
      end if
      if(flag_rcv_offset) then
        write(6,'(a)')        "- receivers outside offset are excluded: "
        write(6,'(a,2es12.4)')"-     rcv offset X (min/max) : ",rcv_offset_x(:)
        write(6,'(a,2es12.4)')"-     rcv offset Y (min/max) : ",rcv_offset_y(:)
        write(6,'(a,2es12.4)')"-     rcv offset Z (min/max) : ",rcv_offset_z(:)
        write(6,'(a,2es12.4)')"-     rcv offset cirular     : ",rcv_offset_rad(:)
      end if
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine print_misfit_info

end module m_print_misfit
