!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_misfit.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the misfit functional parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_misfit
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  use m_ctx_parallelism, only: t_parallelism
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_misfit
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters for the misfit functional
  !
  !> @param[in]   parameter_file  : parameter file where infos are
  !> @param[out]  niter           : number of iterations 
  !> @param[out]  str_misfit      : name of misfit functional norm
  !> @param[out]  misfit_ncomp    : number of componenents in the data
  !> @param[out]  misfit_comp     : list of components
  !> @param[out]  str_data        : path toward the data
  !> @param[out]  nobs_recipro    : number of obs for reciprocity
  !> @param[out]  niter_min       : min number of iterations 
  !> @param[out]  tol_stagnate    : tolerance for stagnation
  !> @param[out]  flag_rcv_offset : receivers offset flag
  !> @param[out]  rcv_offset_x    : offset in x
  !> @param[out]  rcv_offset_y    : offset in y
  !> @param[out]  rcv_offset_z    : offset in z
  !> @param[out]  rcv_offset_rad  : offset in a circular direction
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_misfit(parameter_file,niter,str_misfit,    &
                                    misfit_ncomp,misfit_comp,str_data,  &
                                    nobs_recipro,niter_min,             &
                                    tol_stagnate,flag_rcv_offset,       &
                                    rcv_offset_x,rcv_offset_y,          &
                                    rcv_offset_z,rcv_offset_rad,ctx_err)
    implicit none

    character(len=*)               ,intent(in)   :: parameter_file
    character(len=512)             ,intent(out)  :: str_misfit
    character(len=512)             ,intent(out)  :: str_data
    integer                        ,intent(out)  :: misfit_ncomp,nobs_recipro
    integer                        ,intent(out)  :: niter,niter_min
    real                           ,intent(out)  :: tol_stagnate
    character(len=512) ,allocatable,intent(inout):: misfit_comp(:)
    logical                        ,intent(out)  :: flag_rcv_offset
    real(kind=8)                   ,intent(out)  :: rcv_offset_x(2)
    real(kind=8)                   ,intent(out)  :: rcv_offset_y(2)
    real(kind=8)                   ,intent(out)  :: rcv_offset_z(2)
    real(kind=8)                   ,intent(out)  :: rcv_offset_rad(2)
    type(t_error)                  ,intent(out)  :: ctx_err
    !! local
    integer            :: int_list   (512)
    real               :: rlist      (512)
    real(kind=8)       :: dlist      (512)
    character(len=512) :: string_list(512)
    logical            :: logic_list (512)
    integer            :: nval

    ctx_err%ierr  = 0
    ctx_err%msg   = ''
    rcv_offset_x(1)  = 0.d0 ; rcv_offset_x(2)  = huge(1.d0)
    rcv_offset_y(1)  = 0.d0 ; rcv_offset_y(2)  = huge(1.d0)
    rcv_offset_z(1)  = 0.d0 ; rcv_offset_z(2)  = huge(1.d0)
    !! -----------------------------------------------------------------
    !! Reading the misfit functional infos -----------------------------
    !! -----------------------------------------------------------------    
    !! misfit function norm; data path; niterations
    call read_parameter(parameter_file,'fwi_niter',1,int_list,nval)
    niter     = int_list(1)
    call read_parameter(parameter_file,'fwi_niter_min',niter,int_list,nval)
    niter_min = int_list(1)
    call read_parameter(parameter_file,'fwi_stagnation_tol',0.0,rlist,nval)
    tol_stagnate = rlist(1)

    call read_parameter(parameter_file,'fwi_misfit'    ,'',str_misfit ,nval)
    call read_parameter(parameter_file,'fwi_path_data' ,'',str_data   ,nval)   
    call read_parameter(parameter_file,'fwi_component' ,'',string_list,nval)
    misfit_ncomp = nval
    call read_parameter(parameter_file,'fwi_reciprocity_nobs',0,int_list,nval)
    nobs_recipro = int_list(1)

    call read_parameter(parameter_file,'fwi_offset_rcv',.false.,logic_list,nval)
    flag_rcv_offset = logic_list(1)
    if(flag_rcv_offset) then
      call read_parameter(parameter_file,'fwi_offset_rcv_xmin',0.d0 ,dlist,nval)
      rcv_offset_x(1) = dlist(1)
      call read_parameter(parameter_file,'fwi_offset_rcv_xmax',huge(1.d0),dlist,nval)
      rcv_offset_x(2) = dlist(1)
      call read_parameter(parameter_file,'fwi_offset_rcv_ymin',0.d0 ,dlist,nval)
      rcv_offset_y(1) = dlist(1)
      call read_parameter(parameter_file,'fwi_offset_rcv_ymax',huge(1.d0),dlist,nval)
      rcv_offset_y(2) = dlist(1)
      call read_parameter(parameter_file,'fwi_offset_rcv_zmin',0.d0 ,dlist,nval)
      rcv_offset_z(1) = dlist(1)
      call read_parameter(parameter_file,'fwi_offset_rcv_zmax',huge(1.d0),dlist,nval)
      rcv_offset_z(2) = dlist(1)
      call read_parameter(parameter_file,'fwi_offset_rcv_rmin',0.d0,dlist,nval)
      rcv_offset_rad(1)  = dlist(1)
      call read_parameter(parameter_file,'fwi_offset_rcv_rmax',huge(1.d0),dlist,nval)
      rcv_offset_rad(2)  = dlist(1)
    end if     
    !! it can be zero in case of reciprocity misfit
    if(misfit_ncomp > 0) then
      allocate(misfit_comp (misfit_ncomp))
      misfit_comp(:) = string_list(1:misfit_ncomp)
    end if
    
    !! -----------------------------------------------------------------
    !! check that keys are defined
    if(niter <= 0) then
      ctx_err%ierr  = -1
      ctx_err%msg   = "** ERROR: optimization niter < 0 [misfit_read_param]**"
    end if
    if(niter_min <= 0) then
      ctx_err%ierr  = -1
      ctx_err%msg   = trim(adjustl(ctx_err%msg)) // "** ERROR: " //     &
                      "optimization niter_min < 0 [misfit_read_param]**"
    end if
    if(tol_stagnate <= 0.) then
      ctx_err%ierr  = -1
      ctx_err%msg   = trim(adjustl(ctx_err%msg)) // "** ERROR: " //     &
                      "stagnation criterion < 0 [misfit_read_param]**"    
    end if
    
    if(trim(adjustl(str_misfit)) .eq. '') then
      ctx_err%msg   = trim(adjustl(ctx_err%msg)) // "** ERROR: " //     &
                      "misfit norm not found [misfit_read_param]**"
      ctx_err%ierr  = -1
    endif
    if(trim(adjustl(str_data)) .eq. '') then
      ctx_err%msg   = trim(adjustl(ctx_err%msg)) // "** ERROR: " //     &
                      "path towards the data not found [misfit_read_param]**"
      ctx_err%ierr  = -1
    endif
    if(flag_rcv_offset) then
      if(rcv_offset_x(1) >= rcv_offset_x(2)) then
        ctx_err%msg   = trim(adjustl(ctx_err%msg)) // "** ERROR: " //   &
                        "inconsistant min/max offset receiver X "  //   &
                        "[misfit_read_param]**"
        ctx_err%ierr  = -1      
      end if
      if(rcv_offset_y(1) >= rcv_offset_y(2)) then
        ctx_err%msg   = trim(adjustl(ctx_err%msg)) // "** ERROR: " //   &
                        "inconsistant min/max offset receiver Y "  //   &
                        "[misfit_read_param]**"
        ctx_err%ierr  = -1      
      end if
      if(rcv_offset_z(1) >= rcv_offset_z(2)) then
        ctx_err%msg   = trim(adjustl(ctx_err%msg)) // "** ERROR: " //   &
                        "inconsistant min/max offset receiver Z "  //   &
                        "[misfit_read_param]**"
        ctx_err%ierr  = -1      
      end if
    
    end if
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------    
    return
  end subroutine read_parameters_misfit

end module m_read_parameters_misfit
