!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_misfit_compute.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the difference between observations
!> and simulations. From an input wavefield simulation, we 
!>   1) extract the signal at the receivers position 
!>   2) load the data (according to component name)
!>   3) compute the APPROPRIATE norm (depending on the misfit)
!
!------------------------------------------------------------------------------
module m_misfit_compute

  !! module used -------------------------------------------------------  
  use omp_lib
  use m_raise_error,                 only: raise_error, t_error
  use m_distribute_error,            only: distribute_error
  use m_ctx_parallelism,             only: t_parallelism
  use m_define_precision,            only: RKIND_MAT
  use m_reduce_sum,                  only: reduce_sum
  use m_allreduce_sum,               only: allreduce_sum
  use m_barrier,                     only: barrier
  use m_broadcast,                   only: broadcast
  !> 
  use m_ctx_acquisition,             only: t_acquisition
  use m_ctx_domain,                  only: t_domain
  use m_ctx_model,                   only: t_model
  use m_ctx_discretization,          only: t_discretization
  use m_ctx_equation,                only: t_equation
  use m_ctx_field,                   only: t_field
  use m_ctx_rhs,                     only: t_rhs
  use m_tag_namefield,               only: field_directional_formalism
  use m_discretization_rhs,          only: discretization_rhs_backward_sparse
  use m_extract_rcv_data,            only: extract_rcv_data
  use m_load_data,                   only: load_observation
  use m_misfit_norms,                only: misfit_norm,                 &
                                           misfit_norm_source,          &
                                           misfit_norm_reciprocity
  use m_reciprocity_formula,         only: reciprocity_fields
  use m_misfit_norms,                only: tag_MISFIT_RECIPROCITY,      &
                                           tag_MISFIT_RECIPROCITY_LOG,  &
                                           tag_MISFIT_FK_MODULUS
  !! to perform the FK transform
  use m_Fourier_transform_naive,     only:                              &
                                     compute_Fourier_transform_naive,   &
                                     compute_Fourier_inverse_naive
  !! -------------------------------------------------------------------
  implicit none

  interface process_rcv_offset
     module procedure process_rcv_offset_2dvect
     module procedure process_rcv_offset_1dvect
  end interface


  private
  public :: misfit_compute, misfit_compute_and_backrhs

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute cost function for all shots and get the backward RHS from
  !> the misfit functional evaluation. The computations depend on the 
  !> misfit criterion (norm)
  !
  !> @param[in]    ctx_paral           : parallel context
  !> @param[in]    ctx_aq              : acquisition context
  !> @param[in]    ctx_eq              : equation context
  !> @param[in]    ctx_domain          : domain context
  !> @param[in]    ctx_disc            : discretization context
  !> @param[in]    ctx_field           : field context with fields array
  !> @param[inout] ctx_rhs             : type rhs
  !> @param[in]    frequency(2)        : current frequency
  !> @param[in]    mode                : current mode
  !> @param[in]    tag_misfit          : tag for the misfit norm
  !> @param[in]    n_comp_misfit       : number of components in the misfit
  !> @param[in]    data_path           : path towards the data
  !> @param[in]    name_comp_all   (:) : name of all components
  !> @param[inout] cost_gb             : output global cost
  !> @param[inout] cost_field(:)       : output cost per field
  !> @param[inout] n_shot_reciprocity  : number of shots for reciprocity
  !> @param[in]    flag_rcv_offset     : receivers offset flag
  !> @param[in]    rcv_offset_x        : offset in x
  !> @param[in]    rcv_offset_y        : offset in y
  !> @param[in]    rcv_offset_z        : offset in z
  !> @param[in]    rcv_offset_rad      : offset in circular direction
  !> @param[inout] ctx_err             : context error
  !----------------------------------------------------------------------------
  subroutine misfit_compute_and_backrhs(ctx_paral,ctx_aq,ctx_eq,        &
                                        ctx_domain,ctx_disc,ctx_field,  &
                                        ctx_rhs,frequency,mode,         &
                                        tag_misfit,                     &
                                        n_comp_misfit,data_path,        &
                                        name_comp,cost_gb,cost_field,   &
                                        n_shot_reciprocity,             &
                                        !! information on receivers offset
                                        flag_rcv_offset,rcv_offset_x,   &
                                        rcv_offset_y,rcv_offset_z,      &
                                        rcv_offset_rad,                 &
                                        !! this is for source inversion
                                        flag_src_inv,src_num,src_den,   &
                                        ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_acquisition)         ,intent(in)   :: ctx_aq
    type(t_equation)            ,intent(in)   :: ctx_eq
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(inout):: ctx_disc
    type(t_field)               ,intent(in)   :: ctx_field
    type(t_rhs)                 ,intent(inout):: ctx_rhs
    real                        ,intent(in)   :: frequency(:)
    integer                     ,intent(in)   :: mode
    real(kind=8)                ,intent(in)   :: rcv_offset_x(2)
    real(kind=8)                ,intent(in)   :: rcv_offset_y(2)
    real(kind=8)                ,intent(in)   :: rcv_offset_z(2)
    real(kind=8)                ,intent(in)   :: rcv_offset_rad(2)
    real(kind=8)                ,intent(inout):: cost_gb
    real(kind=8),allocatable    ,intent(inout):: cost_field(:)
    integer                     ,intent(in)   :: tag_misfit,n_comp_misfit
    integer                     ,intent(in)   :: n_shot_reciprocity
    character(len=*)            ,intent(in)   :: data_path
    character(len=*),allocatable,intent(in)   :: name_comp(:)
    logical                     ,intent(in)   :: flag_src_inv,flag_rcv_offset
    complex(kind=8)             ,intent(inout):: src_num,src_den
    type(t_error)               ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                              :: nrhs,k
    integer                              :: count_src_obs,count_src_aq
    integer                              :: i_src_obs,i_src_simu,i_src_aq
    integer                              :: nrcv,n_comp,max_nrcv
    real(kind=8)                         :: cost_loc
    real(kind=8)           , allocatable :: cost_loc_field(:)
    complex(kind=8)        , allocatable :: diff   (:,:)
    complex(kind=8)        , allocatable :: diff_gb(:,:,:)
    complex(kind=8)                      :: src_num_loc,src_den_loc
    logical                              :: flag_frequency,flag_mode
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    !! return error if no receivers in the acquisition
    if(.not. ctx_aq%flag_rcv) then
      ctx_err%msg   = "** ERROR: Connot compute the misfit because no "//&
                      "receiver are given in input [misfit_rhs_backward] **"
      ctx_err%ierr  = -15
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    !! for allocation
    max_nrcv = maxval(ctx_aq%sources(:)%rcv%nrcv)
    if(.not. ctx_aq%flag_same_rcv) then
      if(tag_misfit .eq. tag_MISFIT_RECIPROCITY_LOG .or.                &
         tag_misfit .eq. tag_MISFIT_RECIPROCITY) then
        ctx_err%msg   = "** ERROR: receivers must be the same for "   //&
                        "reciprocity misfit function [misfit_rhs_backward] **"
        ctx_err%ierr  = -15
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 0) we identify the number of rhs and global source position
    !!    (required to load the appropriate observations)
    !! -----------------------------------------------------------------
    nrhs   = ctx_rhs%n_rhs_in_group(ctx_rhs%i_group)
    n_comp = ctx_eq%dim_solution
    flag_frequency = ctx_eq%flag_freq
    flag_mode      = ctx_eq%flag_mode
    !! the frequency is given in input but as it has to remove the 2pi
    !! -----------------------------------------------------------------

    !! we use global acquisition info for the observation number
    count_src_obs = ctx_aq%isrc_gb_start
    count_src_aq  = 1
    do k = 2, ctx_rhs%i_group
      count_src_obs= count_src_obs + ctx_rhs%n_rhs_in_group(k-1)*ctx_aq%isrc_step
      count_src_aq = count_src_aq  + ctx_rhs%n_rhs_in_group(k-1)
    end do

    !! -----------------------------------------------------------------
    !! 1) the discretization infos have already been done when
    !!    doing the forward rhs so no need to prepare the rcv weight
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 2) we create the "source" which is the difference between
    !!    observation and simulations, depending on the norm,
    !!    except if reciprocity misfit functional
    !! -----------------------------------------------------------------
    ctx_rhs%rhs_format='sparse'
    allocate(cost_loc_field(n_comp_misfit))
    !! this is for the global value for rhs
    allocate (diff_gb(max_nrcv,n_comp,nrhs))
    diff_gb=0.d0
    !! -----------------------------------------------------------------
    !! loop over sources in the simulations
    do i_src_simu = 1, nrhs
      i_src_obs = count_src_obs + (i_src_simu-1)*ctx_aq%isrc_step
      i_src_aq  = count_src_aq  + (i_src_simu-1)
      nrcv  = ctx_aq%sources(i_src_aq)%rcv%nrcv

      if(ctx_paral%master) then
        allocate (diff(nrcv,n_comp))
      else
        allocate (diff(1,1))
      end if
      !! ---------------------------------------------------------------
      !! compute array diff and local cost, depending on the misfit
      !! ---------------------------------------------------------------
      cost_loc       = 0.0
      cost_loc_field = 0.0
      src_den_loc    = 0.0
      src_num_loc    = 0.0
      diff           = 0.0
      select case(tag_misfit)
        case(tag_MISFIT_RECIPROCITY_LOG,tag_MISFIT_RECIPROCITY)
          if(flag_src_inv) then
            ctx_err%msg   = "** ERROR: Source inversion is not "     // &
                            "compatible with reciprocity misfit "    // &
                            "[compute_misfit] **"
            ctx_err%ierr  = -15
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          call misfit_compute_reciprocity_single_shot(ctx_paral,        &
                              ctx_aq,ctx_disc,ctx_field,nrcv,n_comp,    &
                              data_path,                                &
                              flag_frequency,flag_mode,frequency,mode,  &
                              n_shot_reciprocity,                       &
                              !! source index in the current group of simulation (nrhs)
                              i_src_simu,                               &
                              !! source index in the read aquisition
                              i_src_aq,                                 &
                              tag_misfit,cost_loc,diff,                 &
                              ctx_aq%sources(i_src_aq)%rcv%nx,          &
                              ctx_aq%sources(i_src_aq)%rcv%ny,          &
                              ctx_aq%sources(i_src_aq)%rcv%nz,          &
                              flag_rcv_offset,                          &
                              !! position of the receivers and offsets
                              ctx_aq%sources(i_src_aq)%rcv%x,           &
                              ctx_aq%sources(i_src_aq)%rcv%y,           &
                              ctx_aq%sources(i_src_aq)%rcv%z,           &
                              rcv_offset_x,rcv_offset_y,rcv_offset_z,   &
                              rcv_offset_rad,ctx_err)
        case default
          call misfit_compute_single_shot(ctx_paral,ctx_disc,ctx_field, &
                              nrcv,data_path,flag_frequency,flag_mode,  &
                              frequency,mode,i_src_obs,                 &
                              i_src_simu,n_comp,n_comp_misfit,          &
                              ctx_eq%name_sol,name_comp,tag_misfit,     &
                              cost_loc,cost_loc_field,diff,             &
                              !! for directional field computation
                              ctx_aq%sources(i_src_aq)%rcv%nx,          &
                              ctx_aq%sources(i_src_aq)%rcv%ny,          &
                              ctx_aq%sources(i_src_aq)%rcv%nz,          &
                              !! source inversion
                              flag_src_inv,ctx_aq%src_info%current_val, &
                              src_num_loc,src_den_loc,                  &
                              flag_rcv_offset,                          &
                              !! position of the the sources (possibly 
                              !! multi-pts), and number of points.
                              ctx_aq%sources(i_src_aq)%x,               &
                              ctx_aq%sources(i_src_aq)%y,               &
                              ctx_aq%sources(i_src_aq)%z,               &
                              ctx_aq%sources(i_src_aq)%nsim,            &
                              !! position of the receivers and offsets
                              ctx_aq%sources(i_src_aq)%rcv%x,           &
                              ctx_aq%sources(i_src_aq)%rcv%y,           &
                              ctx_aq%sources(i_src_aq)%rcv%z,           &
                              rcv_offset_x,rcv_offset_y,rcv_offset_z,   &
                              rcv_offset_rad,ctx_err)
          !! update local cost
          cost_field = cost_field + cost_loc_field
          !! if source reconstruction, we update the numerator and denominator
          if(flag_src_inv) then
            src_num = src_num + src_num_loc
            src_den = src_den + src_den_loc
          end if 
      end select
      !! update global cost
      cost_gb = cost_gb + cost_loc
      !! master save the diff_gb
      if(ctx_paral%master) then
        diff_gb(1:nrcv,:,i_src_simu) = diff(:,:)
      end if
      deallocate(diff)
      ! -------------------------------------------------------
    end do
    deallocate(cost_loc_field)

    !! master will convert to the backward_rhs in a sparse format
    !! -----------------------------------------------------------------
    !! the master broadcast the diff array to everyone 
    call broadcast(diff_gb,max_nrcv*n_comp*nrhs,0,                      &
                   ctx_paral%communicator,ctx_err)
    call discretization_rhs_backward_sparse(ctx_paral,ctx_disc,         &
                                      ctx_domain,nrhs,diff_gb,          &
                                      ctx_rhs%csri,ctx_rhs%icol,        &
                                      ctx_rhs%Rval,ctx_err)
    deallocate(diff_gb)
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine misfit_compute_and_backrhs

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute misfit function for all shots
  !
  !> @param[in]    ctx_paral           : parallel context
  !> @param[in]    ctx_aq              : acquisition context
  !> @param[in]    ctx_eq              : equation context
  !> @param[in]    ctx_disc            : discretization context
  !> @param[in]    ctx_field           : field context with fields array
  !> @param[in]    frequency(2)        : frequency
  !> @param[in]    mode                : mode
  !> @param[in]    tag_misfit          : tag for the misfit norm
  !> @param[in]    i_shot_group        : number of group 
  !> @param[in]    n_shot_in_group(:)  : number of shot per group
  !> @param[in]    n_comp_misfit       : number of components in the misfit
  !> @param[in]    data_path           : path towards the data
  !> @param[in]    name_comp_all   (:) : name of all components
  !> @param[inout] cost_gb             : output global cost
  !> @param[inout] cost_field(:)       : output cost per field
  !> @param[inout] n_shot_reciprocity  : number of shots for reciprocity
  !> @param[in]    flag_rcv_offset     : receivers offset flag
  !> @param[in]    rcv_offset_x        : offset in x
  !> @param[in]    rcv_offset_y        : offset in y
  !> @param[in]    rcv_offset_z        : offset in z
  !> @param[in]    rcv_offset_rad      : offset in circular direction
  !> @param[inout] ctx_err             : context error
  !----------------------------------------------------------------------------
  subroutine misfit_compute(ctx_paral,ctx_aq,ctx_eq,ctx_disc,           &
                            ctx_field,frequency,mode,tag_misfit,        &
                            n_comp_misfit,data_path,name_comp,          &
                            i_shot_group,n_shot_in_group,               &
                            cost_gb,cost_field,n_shot_reciprocity,      &
                            !! information on receivers offset
                            flag_rcv_offset,rcv_offset_x,rcv_offset_y,  &
                            rcv_offset_z,rcv_offset_rad,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_acquisition)         ,intent(in)   :: ctx_aq
    type(t_equation)            ,intent(in)   :: ctx_eq
    type(t_discretization)      ,intent(inout):: ctx_disc
    type(t_field)               ,intent(in)   :: ctx_field
    real                        ,intent(in)   :: frequency(:)
    integer                     ,intent(in)   :: mode
    real(kind=8)                ,intent(in)   :: rcv_offset_x(2)
    real(kind=8)                ,intent(in)   :: rcv_offset_y(2)
    real(kind=8)                ,intent(in)   :: rcv_offset_z(2)
    real(kind=8)                ,intent(in)   :: rcv_offset_rad(2)
    real(kind=8)                ,intent(inout):: cost_gb
    real(kind=8),allocatable    ,intent(inout):: cost_field(:)
    integer                     ,intent(in)   :: tag_misfit,n_comp_misfit
    integer                     ,intent(in)   :: n_shot_reciprocity
    integer                     ,intent(in)   :: i_shot_group
    integer         ,allocatable,intent(in)   :: n_shot_in_group(:)
    character(len=*)            ,intent(in)   :: data_path
    character(len=*),allocatable,intent(in)   :: name_comp(:)
    logical                     ,intent(in)   :: flag_rcv_offset
    type(t_error)               ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                              :: k,nshot
    integer                              :: count_src_obs,count_src_aq
    integer                              :: i_src_obs,i_src_simu,i_src_aq
    integer                              :: nrcv,n_comp
    real(kind=8)                         :: cost_loc
    real(kind=8)           , allocatable :: cost_loc_field(:)
    complex(kind=8)        , allocatable :: diff   (:,:)
    complex(kind=8)                      :: src_num,src_den
    logical                              :: flag_frequency,flag_mode
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    !! return error if no receivers in the acquisition
    if(.not. ctx_aq%flag_rcv) then
      ctx_err%msg   = "** ERROR: Connot compute the misfit because no "//&
                      "receiver are given in input [compute_misfit] **"
      ctx_err%ierr  = -15
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
    !! for allocation
    if(.not. ctx_aq%flag_same_rcv) then
      if(tag_misfit .eq. tag_MISFIT_RECIPROCITY_LOG .or.                &
         tag_misfit .eq. tag_MISFIT_RECIPROCITY) then
        ctx_err%msg   = "** ERROR: receivers must be the same for "   //&
                        "reciprocity misfit function [compute_misfit] **"
        ctx_err%ierr  = -15
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 0) we identify the number of rhs and global source position
    !!    (required to load the appropriate observations)
    !! -----------------------------------------------------------------
    nshot  = n_shot_in_group(i_shot_group)
    n_comp = ctx_eq%dim_solution
    flag_frequency = ctx_eq%flag_freq
    flag_mode      = ctx_eq%flag_mode
    !! the frequency is given in input but as it has to remove the 2pi
    !! -----------------------------------------------------------------
    
    !! we use global acquisition info for the observation number
    count_src_obs = ctx_aq%isrc_gb_start
    count_src_aq  = 1
    do k = 2, i_shot_group
      count_src_obs= count_src_obs + n_shot_in_group(k-1)*ctx_aq%isrc_step
      count_src_aq = count_src_aq  + n_shot_in_group(k-1)
    end do

    !! -----------------------------------------------------------------
    !! 1) the discretization infos have already been done when
    !!    doing the forward rhs so no need to prepare the rcv weight
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! 2) we create the "source" which is the difference between
    !!    observation and simulations, depending on the norm,
    !!    except if reciprocity misfit functional
    !! -----------------------------------------------------------------
    allocate(cost_loc_field(n_comp_misfit))
    !! -----------------------------------------------------------------
    !! loop over sources in the simulations
    do i_src_simu = 1, nshot
      !! global index of the source in the acquisition, i.e., for observations
      i_src_obs = count_src_obs + (i_src_simu-1)*ctx_aq%isrc_step
      !! local index of the source in the (possibly reduced) acquisition
      i_src_aq  = count_src_aq  + (i_src_simu-1)
      nrcv  = ctx_aq%sources(i_src_aq)%rcv%nrcv
     
      if(ctx_paral%master) then
        allocate (diff(nrcv,n_comp))
      else
        allocate (diff(1,1))
      end if
      !! ---------------------------------------------------------------
      !! compute array diff and local cost, depending on the misfit
      !! ---------------------------------------------------------------
      cost_loc       = 0.0
      cost_loc_field = 0.0
      diff           = 0.0
      select case(tag_misfit)
        case(tag_MISFIT_RECIPROCITY_LOG,tag_MISFIT_RECIPROCITY)
          call misfit_compute_reciprocity_single_shot(ctx_paral,        &
                              ctx_aq,ctx_disc,ctx_field,nrcv,n_comp,    &
                              data_path,                                &
                              flag_frequency,flag_mode,frequency,mode,  &
                              n_shot_reciprocity,                       &
                              !! source index in the current group of simulation (nrhs)
                              i_src_simu,                               &
                              !! source index in the read aquisition
                              i_src_aq,                                 &
                              tag_misfit,cost_loc,diff,                 &
                              ctx_aq%sources(i_src_aq)%rcv%nx,          &
                              ctx_aq%sources(i_src_aq)%rcv%ny,          &
                              ctx_aq%sources(i_src_aq)%rcv%nz,          &
                              flag_rcv_offset,                          &
                              !! position of the receivers and offsets
                              ctx_aq%sources(i_src_aq)%rcv%x,           &
                              ctx_aq%sources(i_src_aq)%rcv%y,           &
                              ctx_aq%sources(i_src_aq)%rcv%z,           &
                              rcv_offset_x,rcv_offset_y,rcv_offset_z,   &
                              rcv_offset_rad,ctx_err)
        case default
          call misfit_compute_single_shot(ctx_paral,ctx_disc,ctx_field, &
                              nrcv,data_path,flag_frequency,flag_mode,  &
                              frequency,mode,i_src_obs,                 &
                              i_src_simu,n_comp,n_comp_misfit,          &
                              ctx_eq%name_sol,name_comp,tag_misfit,     &
                              cost_loc,cost_loc_field,diff,             &
                              !! for directional field computation
                              ctx_aq%sources(i_src_aq)%rcv%nx,          &
                              ctx_aq%sources(i_src_aq)%rcv%ny,          &
                              ctx_aq%sources(i_src_aq)%rcv%nz,          &
                              !! phantom source inversion: not needed here
                              .false.,ctx_aq%src_info%current_val,      &
                              src_num,src_den,                          &
                              flag_rcv_offset,                          &
                              !! position of the the sources (possibly 
                              !! multi-pts), and number of points.
                              ctx_aq%sources(i_src_aq)%x,               &
                              ctx_aq%sources(i_src_aq)%y,               &
                              ctx_aq%sources(i_src_aq)%z,               &
                              ctx_aq%sources(i_src_aq)%nsim,            &
                              !! position of the receivers and offsets
                              ctx_aq%sources(i_src_aq)%rcv%x,           &
                              ctx_aq%sources(i_src_aq)%rcv%y,           &
                              ctx_aq%sources(i_src_aq)%rcv%z,           &
                              rcv_offset_x,rcv_offset_y,rcv_offset_z,   &
                              rcv_offset_rad,ctx_err)
          !! update local cost
          cost_field = cost_field + cost_loc_field
      end select
      !! update global cost
      cost_gb = cost_gb + cost_loc
      deallocate(diff)
      ! -------------------------------------------------------
    end do
    deallocate(cost_loc_field)
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine misfit_compute

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the misfit for one single shot
  !> @param[in]    ctx_paral           : parallel context
  !> @param[in]    ctx_disc            : discretization context
  !> @param[in]    ctx_field           : field context with fields array
  !> @param[in]    nrcv                : number of receiver for this shot
  !> @param[in]    tag_misfit          : tag for the misfit norm
  !> @param[in]    i_shot_simu         : current simulated shot number
  !> @param[in]    i_shot_obs          : corresponding observed shot number
  !> @param[in]    n_comp              : number of components per fields
  !> @param[in]    n_comp_misfit       : number of components in the misfit
  !> @param[in]    flag_frequency      : indicates if frequency in problem
  !> @param[in]    flag_mode           : indicates if mode in problem
  !> @param[in]    frequency(2)        : frequency
  !> @param[in]    mode                : mode
  !> @param[in]    data_path           : path towards the data
  !> @param[in]    name_comp_all   (:) : name of all components
  !> @param[in]    name_comp_misfit(:) : name of misfit components
  !> @param[inout] cost_output_gb      : output global cost
  !> @param[inout] cost_output(:)      : output cost per components
  !> @param[inout] diff_output(:,:)    : ouptut diff array (for backward rhs)
  !> @param[in]    normalrcv_x         : receivers normal direction in x
  !> @param[in]    normalrcv_y         : receivers normal direction in y
  !> @param[in]    normalrcv_z         : receivers normal direction in z
  !> @param[in]    flag_rcv_offset     : offset to exclude receivers
  !> @param[in]    xsrc                : pt-source position in x 
  !> @param[in]    ysrc                : pt-source position in y 
  !> @param[in]    zsrc                : pt-source position in z 
  !> @param[in]    nptsrc              : number of pt-sources in the source
  !> @param[in]    rcv_x               : receiver positions in x
  !> @param[in]    rcv_y               : receiver positions in y
  !> @param[in]    rcv_z               : receiver positions in z
  !> @param[in]    rcv_offset_x        : offset in x
  !> @param[in]    rcv_offset_y        : offset in y
  !> @param[in]    rcv_offset_z        : offset in z
  !> @param[in]    rcv_offset_rad      : offset in circular direction
  !> @param[inout] ctx_err             : context error
  !----------------------------------------------------------------------------
  subroutine misfit_compute_single_shot(ctx_paral,ctx_disc,ctx_field,   &
                    nrcv,data_path,flag_frequency,flag_mode,frequency,  &
                    mode,i_shot_obs,i_shot_simu,n_comp,n_comp_misfit,   &
                    name_comp_all,name_comp_misfit,tag_misfit,          &
                    cost_output_gb,cost_output,diff_output,             &
                    !! for directional field computation
                    normalrcv_x,normalrcv_y,normalrcv_z,                &
                    !! for source reconstruction -----------------------
                    flag_src_inv,src_val,src_num,src_den,               &           
                    !! for offset receivers computation ----------------
                    flag_rcv_offset,xsrc,ysrc,zsrc,nptsrc,rcv_x,rcv_y,  &
                    rcv_z,rcvoff_x,rcvoff_y,rcvoff_z,rcvoff_rad,ctx_err)
    implicit none
    type(t_parallelism)            ,intent(in)    :: ctx_paral
    type(t_discretization)         ,intent(in)    :: ctx_disc
    type(t_field)                  ,intent(in)    :: ctx_field
    integer                        ,intent(in)    :: nrcv
    integer                        ,intent(in)    :: tag_misfit
    integer                        ,intent(in)    :: i_shot_simu
    integer                        ,intent(in)    :: i_shot_obs
    integer                        ,intent(in)    :: n_comp
    integer                        ,intent(in)    :: n_comp_misfit
    logical                        ,intent(in)    :: flag_frequency
    logical                        ,intent(in)    :: flag_mode
    real                           ,intent(in)    :: frequency(:)
    integer                        ,intent(in)    :: mode
    character(len=*)               ,intent(in)    :: data_path
    character(len=*), allocatable  ,intent(in)    :: name_comp_all   (:)
    character(len=*), allocatable  ,intent(in)    :: name_comp_misfit(:)
    real   (kind=8)                ,intent(out)   :: cost_output_gb
    real   (kind=8) , allocatable  ,intent(inout) :: cost_output(:)
    complex(kind=8) , allocatable  ,intent(inout) :: diff_output(:,:)
    real(kind=8)    , allocatable  ,intent(in)    :: normalrcv_x(:)
    real(kind=8)    , allocatable  ,intent(in)    :: normalrcv_y(:)
    real(kind=8)    , allocatable  ,intent(in)    :: normalrcv_z(:)
    logical                        ,intent(in)    :: flag_src_inv
    complex(kind=8)                ,intent(in)    :: src_val
    complex(kind=8)                ,intent(inout) :: src_num,src_den
    !! for receivers offset test
    logical                        ,intent(in)    :: flag_rcv_offset
    real   (kind=8)                ,intent(in)    :: xsrc(:),ysrc(:),zsrc(:)
    integer(kind=4)                ,intent(in)    :: nptsrc
    real(kind=8)                   ,intent(in)    :: rcv_x(:),rcv_y(:),rcv_z(:)
    real(kind=8)                   ,intent(in)    :: rcvoff_x(2)
    real(kind=8)                   ,intent(in)    :: rcvoff_y(2)
    real(kind=8)                   ,intent(in)    :: rcvoff_z(2)
    real(kind=8)                   ,intent(in)    :: rcvoff_rad(2)
    type(t_error)                  ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    integer                              :: i1,i2,i_sol,i_loc
    integer                , allocatable :: i_ref(:)
    integer                              :: i_field
    real(kind=8)                         :: cost_loc
    complex(kind=RKIND_MAT), allocatable :: simu_loc(:,:),simu(:,:)
    complex(kind=RKIND_MAT), allocatable :: observations(:)
    complex(kind=8)        , allocatable :: diff_loc(:)
    complex(kind=8)                      :: src_num_loc,src_den_loc
    character(len=128)                   :: str_misfit,str_ref
    !! to compute the directional components
    integer                              :: field_loc_n,i_dir
    complex(kind=RKIND_MAT), allocatable :: simulations     (:)
    complex(kind=RKIND_MAT), allocatable :: observations_dir(:,:)
    real(kind=8)           , allocatable :: direction (:,:)
    character(len=64)      , allocatable :: field_loc_name(:)
    !! in case we need to transform
    complex(kind=RKIND_MAT), allocatable :: transform_sim(:)
    complex(kind=RKIND_MAT), allocatable :: transform_obs(:)
    complex(kind=8)        , allocatable :: transform_diff(:)
    real(kind=8)           , allocatable :: transform_x(:)
    real(kind=8)           , allocatable :: transform_xinv(:)
    !! -----------------------------------------------------------------
    
    ctx_err%ierr  = 0  
    !! initialize cost
    cost_output   = 0.0
    cost_output_gb= 0.0
    src_num       = 0.0 
    src_den       = 0.0
    !! -----------------------------------------------------------------
    !! 1) we extract the receivers information for this simulated shot
    !!    only master will keep everything
    !! -----------------------------------------------------------------
    allocate(simu_loc(nrcv,n_comp))   
    simu_loc = 0.0
    i1 = (i_shot_simu-1)*ctx_field%n_dofsol_loc+1
    i2 = i1 + ctx_field%n_dofsol_loc - 1
    !! extract receivers infos (the shot is required to get the rcv)
    call extract_rcv_data(ctx_disc,ctx_field%field(i1:i2,:),n_comp,nrcv,&
                          i_shot_simu,simu_loc,ctx_err)
    !! reduce so that only master keeps the result
    if(ctx_paral%master) then
      allocate(simu(nrcv,n_comp))
    else
      allocate(simu(1,1))
    end if
    simu = 0.0
    !! force int for mpi
    call reduce_sum(simu_loc,simu,nrcv*n_comp,0,ctx_paral%communicator,ctx_err)
    deallocate(simu_loc)

    !! ---------------------------------------------------------------
    !! receivers offset:
    !!   We compare the source position with the position
    !!   of the receivers for possible exclusion depening
    !!   on the offset given
    !! ---------------------------------------------------------------
    if(flag_rcv_offset .and. ctx_paral%master) then
      call process_rcv_offset(ctx_disc%dim_domain,nrcv,xsrc,ysrc,zsrc,  &
                              nptsrc,rcv_x,rcv_y,rcv_z,rcvoff_x,        &
                              rcvoff_y,rcvoff_z,rcvoff_rad,simu,ctx_err)
    endif
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! 2) Master will compute the norm for ALL components that are 
    !!    indicated in the input
    !! -----------------------------------------------------------------
    if(ctx_paral%master) then
      diff_output   = 0.0    
      allocate(diff_loc    (nrcv))
      allocate(observations(nrcv))
      allocate(simulations (nrcv))
      !! at most we have one per direction.
      allocate(direction       (ctx_disc%dim_domain,nrcv))
      allocate(observations_dir(ctx_disc%dim_domain,nrcv))
      allocate(field_loc_name  (ctx_disc%dim_domain))
      allocate(i_ref           (ctx_disc%dim_domain))

      !! in the case we are using the transform ------------------------
      if(tag_misfit .eq. tag_MISFIT_FK_MODULUS) then
        allocate(transform_sim (nrcv))
        allocate(transform_obs (nrcv))
        allocate(transform_diff(nrcv))
        allocate(transform_x   (nrcv))
        allocate(transform_xinv(nrcv))
        transform_x   = 0.d0
        transform_xinv= 0.d0
        transform_sim = 0.d0
        transform_obs = 0.d0
        transform_diff= 0.d0
        ! for now it only works if we have 1D receiver map
        if(abs(maxval(rcv_y)-minval(rcv_y)) > tiny(1.0) .or.            &
           abs(maxval(rcv_z)-minval(rcv_z)) > tiny(1.0)) then
          ctx_err%msg   = "** ERROR: Using the FK transform for the " //&
                        "the misfit computation requires that the " //  &
                        "receivers are on a line [misfit_compute] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
        end if
      end if
      !! ---------------------------------------------------------------

      !! loop over all components of the misfit function.
      do i_field = 1, n_comp_misfit

        if(ctx_err%ierr .ne. 0) cycle

        str_misfit= trim(adjustl(name_comp_misfit(i_field)))
        i_loc     = i_field

        !! we check if we have a directional component (e.g., normal 
        !! and tangent) or if it is a direct one.
        field_loc_n    = 0
        field_loc_name = ''
        direction      = 1.d0
        call field_directional_formalism(ctx_disc%dim_domain,str_misfit,&
                                         normalrcv_x,normalrcv_y,       &
                                         normalrcv_z,                   &
                                         field_loc_n,field_loc_name,    &
                                         direction,ctx_err)

        !! -------------------------------------------------------------
        !! we loop over all of the component found, it is 1 if we have
        !! the raw component, or usually the number of the dimension if
        !! we have a directional one. 
        !! Remark: for the backward RHS, we need to keep the direction.
        !!         for instance, we have, in 2D with normal velocity: 
        !!        \f$   1/2 \Vert R(v_n)  - d_n \Vert^2 \f$
        !!        \f$ = 1/2 \Vert R(v_x n_x + v_z n_z)  - d_n \Vert^2 \f$
        !! and the backward rhs is 
        !!        \f$   n_x* R* (R(v_n)  - d_n)\f$  for \f$ v_x \f$ 
        !!        \f$   n_z* R* (R(v_n)  - d_n)\f$  for \f$ v_z \f$.
        !! -------------------------------------------------------------
        observations_dir = 0.d0
        i_ref            = 0 

        do i_dir = 1, field_loc_n
          if(ctx_err%ierr .ne. 0) cycle
         
          !! identify the global index ---------------------------------
          str_misfit = trim(adjustl(field_loc_name(i_dir)))

          do i_sol = 1, n_comp
            str_ref = trim(adjustl(name_comp_all(i_sol)))
            if (trim(adjustl(str_misfit)) .eq. trim(adjustl(str_ref))) then
              i_ref(i_dir) = i_sol
              exit
            end if
          end do
          
          !! load corresponding observations ---------------------------
          if(i_ref(i_dir) .ne. 0) then
            observations = 0.d0
            call load_observation(data_path,str_ref,flag_frequency,     &
                                  flag_mode,dble(frequency),mode,       &
                                  i_shot_obs,observations,ctx_err)
          else 
            ctx_err%msg   = "** ERROR: unrecognized field "       //  &
                            "components for the misfit computation"// &
                            " [misfit_compute] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            cycle
          end if

          if(ctx_err%ierr .ne. 0) cycle

          !! -------------------------------------------------------------
          !! receivers offset for the observations (the simulations 
          !! have already been done above).
          !!   We compare the source position with the position
          !!   of the receivers for possible exclusion depening
          !!   on the offset given
          !! -------------------------------------------------------------
          if(flag_rcv_offset) then
            call process_rcv_offset(ctx_disc%dim_domain,nrcv,xsrc,ysrc, &
                                    zsrc,nptsrc,rcv_x,rcv_y,rcv_z,      &
                                    rcvoff_x,rcvoff_y,rcvoff_z,         &
                                    rcvoff_rad,observations,ctx_err)
          endif

          !! keep the current directional solution
          observations_dir(i_dir,:) = observations(:)
        
        !! -------------------------------------------------------------
        end do !! end loop over the directional component
        !! -------------------------------------------------------------
        
        !! -------------------------------------------------------------
        !! compute the global directional solution by summing and 
        !! applying the directions
        !! Remark that the direction = 1.0 if we have the raw component.
        !! -------------------------------------------------------------
        observations = 0.d0
        simulations  = 0.d0
        do i_dir = 1, field_loc_n
          simulations  = simulations + cmplx(simu(:,i_ref(i_dir))       &
                                           * direction(i_dir,:),kind=RKIND_MAT)
          observations = observations + cmplx(observations_dir(i_dir,:) &
                                           * direction(i_dir,:),kind=RKIND_MAT)
        end do
        
        !! Compute the normed difference, with th choice of norm.
        !! specificity in the case we need the FK transform
        !! -----------------------------------------------------------
        diff_loc=0.0 ; cost_loc=0.0
        select case(tag_misfit)
          case(tag_MISFIT_FK_MODULUS)
            !! 1) compute the FK transform of the obervations and 
            !!    simulations
            call compute_Fourier_transform_naive(observations,rcv_x,    &
                                 nrcv,transform_obs,transform_x,ctx_err)
            call compute_Fourier_transform_naive(simulations,rcv_x,     &
                                 nrcv,transform_sim,transform_x,ctx_err)

            !! 2) compute the norm, careful that the number of value
            !!    corresponds also to the number of K-frequencies.
            call misfit_norm(tag_misfit,nrcv,transform_obs,             &
                             transform_sim,transform_diff,cost_loc,ctx_err)

            !! 3) Apply inverse transform to get the RHS in diff_loc
            call compute_Fourier_inverse_naive(transform_diff,          &
                                transform_x,nrcv,diff_loc,              &
                                transform_xinv,rcv_x(1),ctx_err)
            !! transform_x2 should be xrcv
            !! if(maxval(abs(transform_xinv-rcv_x)) > tiny(1.0)) then
            !!   ctx_err%msg   = "** ERROR: Inconsistency in the FK "// &
            !!                   "inverse transform [misfit_compute] **"
            !!   ctx_err%ierr  = -1
            !!   ctx_err%critic=.true.
            !!   cycle
            !! end if
          
          case default
            call misfit_norm(tag_misfit,nrcv,observations,simulations,  &
                             diff_loc,cost_loc,ctx_err)
        end select
        
        !! update field output cost and ouptput diff array
        cost_output(i_loc)   = cost_output(i_loc) + cost_loc
        cost_output_gb       = cost_output_gb     + cost_loc
        !! the direction can get out of the derivation, see above comment.
        do i_dir = 1, field_loc_n
          diff_output(:,i_ref(i_dir)) = diff_output(:,i_ref(i_dir)) +   &
                                        direction(i_dir,:)*diff_loc(:)
        end do
            
        !! possibilities for source reconstruction
        !! -------------------------------------------------------------
        if(flag_src_inv) then
          src_num_loc=0.0
          src_den_loc=0.0
          call misfit_norm_source(nrcv,observations,simulations,        &
                                  src_val,src_num_loc,src_den_loc,ctx_err)
          !! update global ones
          src_num = src_num + src_num_loc
          src_den = src_den + src_den_loc
        end if

      !! ---------------------------------------------------------------
      end do !! end loop over misfit components
      !! ---------------------------------------------------------------

      deallocate(observations)
      deallocate(observations_dir)
      deallocate(i_ref)
      deallocate(simulations)
      deallocate(field_loc_name)
      deallocate(diff_loc)
      deallocate(direction)
      if(tag_misfit .eq. tag_MISFIT_FK_MODULUS) then
        deallocate(transform_sim )
        deallocate(transform_obs )
        deallocate(transform_diff)
        deallocate(transform_x   )
        deallocate(transform_xinv)
      end if
    ! ------------------------------------------------------------------
    end if !! end if master
    ! ------------------------------------------------------------------

    !! possibility for non-shared error on master only 
    !! it includes a mpi_barrier
    call distribute_error(ctx_err,ctx_paral%communicator)  
    
    !! broadcast master info to everyone
    call broadcast(cost_output_gb,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(cost_output,n_comp_misfit,0,ctx_paral%communicator,ctx_err)
    if(flag_src_inv) then
      call broadcast(src_num,1,0,ctx_paral%communicator,ctx_err)
      call broadcast(src_den,1,0,ctx_paral%communicator,ctx_err)
    end if
    deallocate(simu)

    return 
  end subroutine misfit_compute_single_shot


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the reciprocity misfit for one single shot
  !> 
  !> @param[in]    ctx_paral           : parallel context
  !> @param[in]    ctx_disc            : discretization context
  !> @param[in]    ctx_field           : field context with fields array
  !> @param[in]    nrcv                : number of receiver for this shot
  !> @param[in]    ncomp               : total number of components
  !> @param[in]    data_path           : path towards the data
  !> @param[in]    flag_frequency      : indicates if frequency in problem
  !> @param[in]    flag_mode           : indicates if mode in problem
  !> @param[in]    frequency(2)        : frequency
  !> @param[in]    mode                : mode
  !> @param[in]    n_shot_obs          : number of probing observations
  !> @param[in]    i_shot_simu         : current simulated shot number
  !> @param[in]    tag_misfit          : tag for the misfit norm
  !> @param[inout] cost_output_gb      : output global cost
  !> @param[inout] diff_output(:,:)    : ouptut diff array (for backward rhs)
  !> @param[in]    normalrcv_x         : receivers normal direction in x
  !> @param[in]    normalrcv_y         : receivers normal direction in y
  !> @param[in]    normalrcv_z         : receivers normal direction in z
  !> @param[in]    flag_rcv_offset     : offset to exclude receivers
  !> @param[in]    xsrc                : pt-source position in x 
  !> @param[in]    ysrc                : pt-source position in y 
  !> @param[in]    zsrc                : pt-source position in z 
  !> @param[in]    nptsrc              : number of pt-sources in the source
  !> @param[in]    rcv_x               : receiver positions in x
  !> @param[in]    rcv_y               : receiver positions in y
  !> @param[in]    rcv_z               : receiver positions in z
  !> @param[in]    rcv_offset_x        : offset in x
  !> @param[in]    rcv_offset_y        : offset in y
  !> @param[in]    rcv_offset_z        : offset in z
  !> @param[in]    rcv_offset_rad      : offset in circular direction
  !> @param[inout] ctx_err             : context error
  !----------------------------------------------------------------------------
  subroutine misfit_compute_reciprocity_single_shot(ctx_paral,ctx_aq,   &
                    ctx_disc,ctx_field,nrcv,ncomp,data_path,            &
                    flag_frequency,flag_mode,frequency,mode,n_shot_obs, &
                    i_shot_simu,i_shot_simu_in_aq,tag_misfit,           &
                    cost_output_gb,diff_output,                         &
                    normalrcv_x,normalrcv_y,normalrcv_z,                &
                    !! for offset receivers computation, contained in aq.            
                    flag_rcv_offset,                                    &
                    rcv_x,rcv_y,rcv_z,rcvoff_x,rcvoff_y,rcvoff_z,       &
                    rcvoff_rad,ctx_err)
                              
    implicit none

    type(t_parallelism)            ,intent(in)    :: ctx_paral
    type(t_acquisition)            ,intent(in)    :: ctx_aq
    type(t_discretization)         ,intent(in)    :: ctx_disc
    type(t_field)                  ,intent(in)    :: ctx_field
    integer                        ,intent(in)    :: nrcv,ncomp
    integer                        ,intent(in)    :: tag_misfit
    integer                        ,intent(in)    :: i_shot_simu
    integer                        ,intent(in)    :: i_shot_simu_in_aq
    integer                        ,intent(in)    :: n_shot_obs
    logical                        ,intent(in)    :: flag_frequency
    logical                        ,intent(in)    :: flag_mode
    real                           ,intent(in)    :: frequency(:)
    integer                        ,intent(in)    :: mode
    real(kind=8)    , allocatable  ,intent(in)    :: normalrcv_x(:)
    real(kind=8)    , allocatable  ,intent(in)    :: normalrcv_y(:)
    real(kind=8)    , allocatable  ,intent(in)    :: normalrcv_z(:)
    character(len=*)               ,intent(in)    :: data_path
    real   (kind=8)                ,intent(out)   :: cost_output_gb
    complex(kind=8) , allocatable  ,intent(inout) :: diff_output(:,:)
    !! for receivers offset test
    logical                        ,intent(in)    :: flag_rcv_offset
    real   (kind=8)                ,intent(in)    :: rcv_x(:),rcv_y(:),rcv_z(:)
    real   (kind=8)                ,intent(in)    :: rcvoff_x(2)
    real   (kind=8)                ,intent(in)    :: rcvoff_y(2)
    real   (kind=8)                ,intent(in)    :: rcvoff_z(2)
    real   (kind=8)                ,intent(in)    :: rcvoff_rad(2)
    type(t_error)                  ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    integer                              :: i1,i2,ishot_start_mpi
    integer                              :: ishot_end_mpi,nshot_mpi
    integer                              :: i_shot_obs,i_comp
    real(kind=8)                         :: cost_loc
    complex(kind=RKIND_MAT), allocatable :: simu_loc(:,:),simu(:,:)
    complex(kind=RKIND_MAT), allocatable :: simu_saved(:,:)
    complex(kind=RKIND_MAT), allocatable :: obs(:,:),obs_comp(:)
    complex(kind=8)        , allocatable :: diff_loc    (:,:)
    complex(kind=8)        , allocatable :: diff_loc_mpi(:,:)
    character(len=128)                   :: str_ref
    character(len=32)      , allocatable :: name_fields_reciprocity(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! initialize 
    cost_output_gb= 0.0
    diff_output   = 0.0

    !! -----------------------------------------------------------------
    !! 1) we extract the receivers information for this simulated shot
    !!    only master will keep everything
    !! -----------------------------------------------------------------
    allocate(simu_loc(nrcv,ncomp))   
    simu_loc = 0.0
    i1 = (i_shot_simu-1)*ctx_field%n_dofsol_loc+1
    i2 = i1 + ctx_field%n_dofsol_loc - 1
    !! extract receivers infos (the shot is required to get the rcv)
    call extract_rcv_data(ctx_disc,ctx_field%field(i1:i2,:),ncomp,nrcv, &
                          i_shot_simu,simu_loc,ctx_err)
    !! reduce so that all have the global receivers restriction
    allocate(simu(nrcv,ncomp))
    allocate(simu_saved(nrcv,ncomp))
    simu       = 0.d0
    simu_saved = 0.d0
    !! force int for mpi
    !! we use all reduce so that all mpi processors are working
    call allreduce_sum(simu_loc,simu,nrcv*ncomp,ctx_paral%communicator,ctx_err)
    deallocate(simu_loc)
   
    !! -----------------------------------------------------------------
    !! receivers offset:
    !!   We compare the source position with the position
    !!   of the receivers for possible exclusion depening
    !!   on the offset given
    !! -----------------------------------------------------------------
    if(flag_rcv_offset) then
      !! compute offset for according to current source for the simulation
      !! we use the index of the source in the acquisition
      call process_rcv_offset(ctx_disc%dim_domain,nrcv,                 &
                              ctx_aq%sources(i_shot_simu_in_aq)%x,      &
                              ctx_aq%sources(i_shot_simu_in_aq)%y,      &
                              ctx_aq%sources(i_shot_simu_in_aq)%z,      &
                              ctx_aq%sources(i_shot_simu_in_aq)%nsim,   &
                              rcv_x,rcv_y,rcv_z,rcvoff_x,               &
                              rcvoff_y,rcvoff_z,rcvoff_rad,simu,ctx_err)
    endif
    !! copy the simulation in order to deal with the different offset
    !! regarding the observed sources .................................. 
    simu_saved = simu
    !! .................................................................
    
    !! adjust local mpi shots that we deal with 
    nshot_mpi       = ceiling(real(n_shot_obs) / real(ctx_paral%nproc))
    ishot_start_mpi = ctx_paral%myrank*nshot_mpi + 1
    ishot_end_mpi   = ishot_start_mpi + nshot_mpi - 1
    if(ishot_end_mpi > n_shot_obs) ishot_end_mpi = n_shot_obs
    !! -----------------------------------------------------------------
     
    !! -----------------------------------------------------------------
    !! 2) the loading of observations depends on:
    !!    a) the formulation (i.e., acoustic, elastic) 
    !!    b) the dimension 
    !!    c) the receivers normal
    !! -----------------------------------------------------------------
    allocate(obs     (nrcv,ncomp))
    allocate(diff_loc(nrcv,ncomp))
    allocate(obs_comp(nrcv))
    allocate(name_fields_reciprocity(ncomp))
    allocate(diff_loc_mpi(nrcv,ncomp))
    diff_loc_mpi = 0.0

    !! -----------------------------------------------------------------
    !! get what are the component needed for this simulation sources 
    !! from the receivers normal
    !! we assume that all the observed receivers correspond with it
    !! -----------------------------------------------------------------
    call reciprocity_fields(ctx_disc%dim_domain,                        &
                            name_fields_reciprocity,                    &
                            maxval(abs(normalrcv_x(:))),                &
                            maxval(abs(normalrcv_y(:))),                &
                            maxval(abs(normalrcv_z(:))),ctx_err)      

    !! -----------------------------------------------------------------
    !! loop over all sources in the observations set 
    !! -----------------------------------------------------------------
    do i_shot_obs = ishot_start_mpi, ishot_end_mpi

      if(ctx_err%ierr .ne. 0) cycle !! non-shared error

      !! reset the simulations to adapt to the offset of the current obs source
      simu = simu_saved

      obs = 0.0
      !! load appropriate components
      do i_comp = 1, ncomp
        if(ctx_err%ierr .ne. 0) cycle !! non-shared error

        obs_comp=0.0 
        str_ref = trim(adjustl(name_fields_reciprocity(i_comp)))
        if(str_ref .ne. '') then
          call load_observation(data_path,str_ref,flag_frequency,       &
                                flag_mode,dble(frequency),mode,         &
                                i_shot_obs,obs_comp,ctx_err)
          if(ctx_err%ierr .ne. 0) cycle !! non-shared error

        end if
        obs(:,i_comp) = obs_comp(:)
      end do     

      !! ---------------------------------------------------------------
      !! receivers offset:
      !!   We compare the source position with the position
      !!   of the receivers for possible exclusion depening
      !!   on the offset given
      !! ---------------------------------------------------------------
      if(flag_rcv_offset) then
        !! remove the offset for the source of the simulation
        call process_rcv_offset(ctx_disc%dim_domain,nrcv,               &
                                ctx_aq%sources(i_shot_simu_in_aq)%x,    &
                                ctx_aq%sources(i_shot_simu_in_aq)%y,    &
                                ctx_aq%sources(i_shot_simu_in_aq)%z,    &
                                ctx_aq%sources(i_shot_simu_in_aq)%nsim, &
                                rcv_x,rcv_y,rcv_z,rcvoff_x,             &
                                rcvoff_y,rcvoff_z,rcvoff_rad,obs,ctx_err)
      endif

      !! ---------------------------------------------------------------
      !! we have all the required components to compute the 
      !! reciprocity: obs(:,:) and simu(:,:) that contain components
      !! ---------------------------------------------------------------
      !! compute appropriate values for diff and cost
      cost_loc = 0.0
      diff_loc = 0.0
      call misfit_norm_reciprocity(tag_misfit,ctx_disc%dim_domain,      &
                                   nrcv,obs,simu,diff_loc,cost_loc,     &
                                   normalrcv_x,normalrcv_y,             &
                                   normalrcv_z,ctx_err)
      !! update global output cost and diff rhs array
      cost_output_gb    = cost_output_gb    + cost_loc
      diff_loc_mpi(:,:) = diff_loc_mpi(:,:) + diff_loc(:,:)
    end do

    !! possibility for non-shared error on one of the mpi processors
    !! the distribution includes a mpi_barrier
    call distribute_error(ctx_err,ctx_paral%communicator)

    deallocate(obs)
    deallocate(diff_loc)
    deallocate(obs_comp)
    deallocate(name_fields_reciprocity)
    deallocate(simu)
    deallocate(simu_saved)
    call barrier(ctx_paral%communicator,ctx_err)
    !! assemble global cost 
    call allreduce_sum(cost_output_gb,cost_loc,1,ctx_paral%communicator,ctx_err)
    cost_output_gb = cost_loc
    call reduce_sum(diff_loc_mpi,diff_output,nrcv*ncomp,0,              &
                    ctx_paral%communicator,ctx_err)
    deallocate(diff_loc_mpi)

    return 
  end subroutine misfit_compute_reciprocity_single_shot


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> verify receivers offset and exclude point that are outside
  !> using omp parallelism
  !> 
  !----------------------------------------------------------------------------
  subroutine process_rcv_offset_2dvect(dim_domain,nrcv,xsrc,ysrc,zsrc,  &
                                       nptsrc,rcv_x,rcv_y,rcv_z,        &
                                       offset_x,offset_y,offset_z,      &
                                       offset_r,data_vect,ctx_err)
    implicit none

    integer                        ,intent(in)    :: dim_domain
    integer                        ,intent(in)    :: nrcv
    real(kind=8)                   ,intent(in)    :: xsrc(:)
    real(kind=8)                   ,intent(in)    :: ysrc(:)
    real(kind=8)                   ,intent(in)    :: zsrc(:)
    integer                        ,intent(in)    :: nptsrc
    real(kind=8)                   ,intent(in)    :: rcv_x(:)
    real(kind=8)                   ,intent(in)    :: rcv_y(:)
    real(kind=8)                   ,intent(in)    :: rcv_z(:)
    real(kind=8)                   ,intent(in)    :: offset_x(2)
    real(kind=8)                   ,intent(in)    :: offset_y(2)
    real(kind=8)                   ,intent(in)    :: offset_z(2)
    real(kind=8)                   ,intent(in)    :: offset_r(2)
    complex(kind=RKIND_MAT) , allocatable  ,intent(inout) :: data_vect(:,:)
    type(t_error)                  ,intent(inout) :: ctx_err
    !!
    integer                   :: ircv, i_pt
    real(kind=8), allocatable :: pt_r(:), pt_s(:), dist(:)

    allocate(pt_r(dim_domain))
    allocate(pt_s(dim_domain))
    allocate(dist(dim_domain))
    
    !! ctx_err is inout so we do not re-initialize it 
    select case (dim_domain)
      case(2)    
        !! -------------------------------------------------------------
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(ircv,pt_s,pt_r,i_pt,dist)
        !$OMP DO
        do ircv=1,nrcv
          
          pt_r(1) = rcv_x(ircv)
          pt_r(2) = rcv_z(ircv)
          !! loop over all point-sources of the current source
          do i_pt = 1, nptsrc
             pt_s(1)  = xsrc(i_pt)
             pt_s(2)  = zsrc(i_pt)
             dist   = abs(pt_s - pt_r) 
             
             if(dist(1) > offset_x(2)  .or. dist(1) < offset_x(1) .or.  & !! X
                dist(2) > offset_z(2)  .or. dist(2) < offset_z(1) .or.  & !! Z
                norm2(dist) < offset_r(1) .or. norm2(dist) > offset_r(2)) then
                data_vect(ircv,:) = 0.0
             end if
           end do
           !! end loop over all pt-src
        
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      
      case(3)
        !! -------------------------------------------------------------
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(ircv,pt_s,pt_r,i_pt,dist)
        !$OMP DO
        do ircv=1,nrcv
          
          pt_r(1) = rcv_x(ircv)
          pt_r(2) = rcv_y(ircv)
          pt_r(3) = rcv_z(ircv)
          !! loop over all point-sources of the current source
          do i_pt = 1, nptsrc
             pt_s(1)  = xsrc(i_pt)
             pt_s(2)  = ysrc(i_pt)
             pt_s(3)  = zsrc(i_pt)
             dist   = abs(pt_s - pt_r) 
             
             if(dist(1) > offset_x(2)  .or. dist(1) < offset_x(1) .or.  & !! X
                dist(2) > offset_y(2)  .or. dist(2) < offset_y(1) .or.  & !! Y
                dist(3) > offset_z(2)  .or. dist(3) < offset_z(1) .or.  & !! Z
                norm2(dist) < offset_r(1) .or. norm2(dist) > offset_r(2)) then
                data_vect(ircv,:) = 0.0
             end if
           end do
           !! end loop over all pt-src
        
        end do
        !$OMP END DO
        !$OMP END PARALLEL
        
        
      case default
        ctx_err%msg   = "** ERROR: Incorrect dimension [process_rcv_offset]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
    end select
   
    deallocate(pt_r)
    deallocate(pt_s)
    deallocate(dist)

    return
  end subroutine process_rcv_offset_2dvect


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> verify receivers offset and exclude point that are outside
  !> using omp parallelism
  !> 
  !----------------------------------------------------------------------------
  subroutine process_rcv_offset_1dvect(dim_domain,nrcv,xsrc,ysrc,zsrc,  &
                                       nptsrc,rcv_x,rcv_y,rcv_z,        &
                                       offset_x,offset_y,offset_z,      &
                                       offset_r,data_vect,ctx_err)
    implicit none

    integer                        ,intent(in)    :: dim_domain
    integer                        ,intent(in)    :: nrcv
    real(kind=8)                   ,intent(in)    :: xsrc(:)
    real(kind=8)                   ,intent(in)    :: ysrc(:)
    real(kind=8)                   ,intent(in)    :: zsrc(:)
    integer                        ,intent(in)    :: nptsrc
    real(kind=8)                   ,intent(in)    :: rcv_x(:)
    real(kind=8)                   ,intent(in)    :: rcv_y(:)
    real(kind=8)                   ,intent(in)    :: rcv_z(:)
    real(kind=8)                   ,intent(in)    :: offset_x(2)
    real(kind=8)                   ,intent(in)    :: offset_y(2)
    real(kind=8)                   ,intent(in)    :: offset_z(2)
    real(kind=8)                   ,intent(in)    :: offset_r(2)
    complex(kind=RKIND_MAT) , allocatable  ,intent(inout) :: data_vect(:)
    type(t_error)                  ,intent(inout) :: ctx_err
    !!
    integer                   :: ircv, i_pt
    real(kind=8), allocatable :: pt_r(:), pt_s(:), dist(:)
    
    
    allocate(pt_r(dim_domain))
    allocate(pt_s(dim_domain))
    allocate(dist(dim_domain))
    
    !! ctx_err is inout so we do not re-initialize it 
    select case (dim_domain)
      case(2)    
        !! -------------------------------------------------------------
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(ircv,pt_s,pt_r,i_pt,dist)
        !$OMP DO
        do ircv=1,nrcv
          
          pt_r(1) = rcv_x(ircv)
          pt_r(2) = rcv_z(ircv)
          !! loop over all point-sources of the current source
          do i_pt = 1, nptsrc
             pt_s(1)  = xsrc(i_pt)
             pt_s(2)  = zsrc(i_pt)
             dist   = abs(pt_s - pt_r) 
             
             if(dist(1) > offset_x(2)  .or. dist(1) < offset_x(1) .or.  & !! X
                dist(2) > offset_z(2)  .or. dist(2) < offset_z(1) .or.  & !! Z
                norm2(dist) < offset_r(1) .or. norm2(dist) > offset_r(2)) then
                data_vect(ircv) = 0.0
             end if
           end do
           !! end loop over all pt-src
        
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      
      case(3)
        !! -------------------------------------------------------------
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(ircv,pt_s,pt_r,i_pt,dist)
        !$OMP DO
        do ircv=1,nrcv
          
          pt_r(1) = rcv_x(ircv)
          pt_r(2) = rcv_y(ircv)
          pt_r(3) = rcv_z(ircv)
          !! loop over all point-sources of the current source
          do i_pt = 1, nptsrc
             pt_s(1)  = xsrc(i_pt)
             pt_s(2)  = ysrc(i_pt)
             pt_s(3)  = zsrc(i_pt)
             dist   = abs(pt_s - pt_r) 
             
             if(dist(1) > offset_x(2)  .or. dist(1) < offset_x(1) .or.  & !! X
                dist(2) > offset_y(2)  .or. dist(2) < offset_y(1) .or.  & !! Y
                dist(3) > offset_z(2)  .or. dist(3) < offset_z(1) .or.  & !! Z
                norm2(dist) < offset_r(1) .or. norm2(dist) > offset_r(2)) then
                data_vect(ircv) = 0.0
             end if
           end do
           !! end loop over all pt-src
        
        end do
        !$OMP END DO
        !$OMP END PARALLEL
        
      case default
        ctx_err%msg   = "** ERROR: Incorrect dimension [process_rcv_offset]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
    end select

    deallocate(pt_r)
    deallocate(pt_s)
    deallocate(dist)

    return
  end subroutine process_rcv_offset_1dvect

end module m_misfit_compute
