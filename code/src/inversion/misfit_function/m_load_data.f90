!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_load_data.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to load data from disk
!
!------------------------------------------------------------------------------
module m_load_data
  
  use m_raise_error,            only : raise_error, t_error
  use m_io_filename,            only : io_filename

  implicit none


  interface load_observation
     module procedure load_observation_complex4
     module procedure load_observation_complex8
  end interface


  private  
  public :: load_observation
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> load observation from disk
  !
  !> @param[in]   path_obs      : observed path
  !> @param[in]   field_name    : name of the field to load 
  !> @param[in]   flag_freq     : indicates if we take the frequency
  !> @param[in]   flag_mode     : indicates if we take the mode
  !> @param[in]   frequency     : frequency
  !> @param[in]   mode          : mode
  !> @param[in]   isrc          : source number
  !> @param[inout] obs           : loaded array
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine load_observation_complex4(path_obs,field_name,flag_freq,   &
                                       flag_mode,frequency,mode,isrc,   &
                                       obs,ctx_err)
    implicit none

    character(len=*)            ,intent(in)   :: path_obs
    character(len=*)            ,intent(in)   :: field_name
    logical                     ,intent(in)   :: flag_freq,flag_mode
    real(kind=8)                ,intent(in)   :: frequency(2)
    integer                     ,intent(in)   :: mode
    integer                     ,intent(in)   :: isrc
    complex(kind=4), allocatable,intent(inout):: obs(:)
    type(t_error)               ,intent(out)  :: ctx_err
    !!
    character(len=1024)            :: filename
    character(len=512)             :: name_loc
    integer                        :: unit_file=110
    logical                        :: file_exists

    ctx_err%ierr = 0
    
    call io_filename(name_loc,trim(adjustl(field_name)),flag_freq,      &
                     flag_mode,frequency,mode,ctx_err,o_isrc=isrc)
    filename = trim(adjustl(path_obs))//trim(adjustl(name_loc))//".txt"

    !! check file exists
    inquire(file=trim(adjustl(filename)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%msg   = "** ERROR: Observation file does not exist: "   //& 
                      trim(adjustl(filename)) // " [load_data]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! non-shared error at upper level
      return
    endif

    !! read file
    obs=0.0
    open(unit=unit_file,file=trim(adjustl(filename)), status='old',     &
         action='read', iostat=ctx_err%ierr)
    read (unit_file,fmt=*) obs
    close(unit_file,iostat=ctx_err%ierr)

    return
  end subroutine load_observation_complex4

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> load observation from disk
  !
  !> @param[in]   path_obs      : observed path
  !> @param[in]   field_name    : name of the field to load 
  !> @param[in]   flag_freq     : indicates if we take the frequency
  !> @param[in]   flag_mode     : indicates if we take the mode
  !> @param[in]   frequency     : frequency
  !> @param[in]   mode          : mode
  !> @param[in]   isrc          : source number
  !> @param[inout] obs           : loaded array
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine load_observation_complex8(path_obs,field_name,flag_freq,   &
                                       flag_mode,frequency,mode,isrc,   &
                                       obs,ctx_err)
    implicit none

    character(len=*)            ,intent(in)   :: path_obs
    character(len=*)            ,intent(in)   :: field_name
    logical                     ,intent(in)   :: flag_freq,flag_mode
    real(kind=8)                ,intent(in)   :: frequency(2)
    integer                     ,intent(in)   :: mode
    integer                     ,intent(in)   :: isrc
    complex(kind=8), allocatable,intent(inout):: obs(:)
    type(t_error)               ,intent(out)  :: ctx_err
    !!
    character(len=1024)            :: filename
    character(len=512)             :: name_loc
    integer                        :: unit_file=110
    logical                        :: file_exists

    ctx_err%ierr = 0
    
    
    call io_filename(name_loc,trim(adjustl(field_name)),flag_freq,      &
                     flag_mode,frequency,mode,ctx_err,o_isrc=isrc)
    filename = trim(adjustl(path_obs))//trim(adjustl(name_loc))//".txt"

    !! check file exists
    inquire(file=trim(adjustl(filename)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%msg   = "** ERROR: Observation file does not exist: "   //& 
                      trim(adjustl(filename)) // " [load_data]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      !! non-shared error at upper level
      return
    endif
    
    !! read file
    obs=0.0
    open(unit=unit_file,file=trim(adjustl(filename)), status='old',     &
         action='read', iostat=ctx_err%ierr)
    read (unit_file,fmt=*) obs
    close(unit_file,iostat=ctx_err%ierr)

    return
  end subroutine load_observation_complex8

end module m_load_data
