!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_misfit_function.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the context for the misfit functional.
!> it contains the norm tag and the basic informations.
!
!------------------------------------------------------------------------------
module m_ctx_misfit_function

  implicit none

  private
  public :: t_misfit, misfit_clean, misfit_copy_ctx, misfit_reset

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_misfit contains all information for the misfit functional
  !
  !> @param[integer] tag_misfit   : tag for the misfit functional norm
  !> @param[integer] n_field      : number of fields concerned in the data
  !> @param[integer] niter        : number of iteration max of minimization
  !> @param[integer] niter_min    : number of iteration min of minimization
  !> @param[string]  data_field   : name of the fields
  !> @param[real]    Jloc         : local misfit per fields
  !> @param[real]    Jregul       : local misfit for regularization part
  !> @param[real]    J            : global misfit value
  !> @param[real]    tol_stagnate : stagnation criterion
  !> @param[integer] n_obs_reciprocity 
  !>                              : number of observation for 
  !>                                reciprocity-type misfit functional
  !> @param[string]  data_path    : path to the data
  !> @param[logical] flag_rcv_offset: offset to exclude receivers
  !> @param[real]    rcv_offset_x : offset in x
  !> @param[real]    rcv_offset_y : offset in y
  !> @param[real]    rcv_offset_z : offset in z
  !> @param[real]    rcv_offset_r : offset in circular direction
  !
  !----------------------------------------------------------------------------
  type t_misfit

    !! misfit functional tag
    integer                       :: tag_misfit
    !! iteration informations
    integer                       :: niter
    integer                       :: niter_min
    
    !! list of components that are to be taken into account in the data
    integer                       :: n_field   
    character(len=9), allocatable :: data_field(:)
    !! we have a separate cost for every fields 
    real(kind=8), allocatable     :: Jloc(:)
    real(kind=8)                  :: Jregularization
    !! total misfit cost
    real(kind=8)                  :: J
    
    !! for Reciprocity-based misfit functional, we can have different 
    !! number of sources
    integer                       :: n_obs_reciprocity
    
    !! path to the data
    character(len=512)            :: data_path=''
    
    !! stagnation tolerance 
    real                          :: tol_stagnate

    !! receivers offset: we can exclude data point that are too far 
    !! from the source. In case of simultaneous point source, we 
    !! will take the min/max of all points
    logical                       :: flag_rcv_offset
    !! two values: (1) = minimal offset ; (2) = maximal offset
    real(kind=8)                  :: rcv_offset_x(2)
    real(kind=8)                  :: rcv_offset_y(2)
    real(kind=8)                  :: rcv_offset_z(2)
    real(kind=8)                  :: rcv_offset_rad(2)
    
  end type t_misfit
  !----------------------------------------------------------------------------
  
  
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> copy misfit context
  !
  !> @param[inout] ctx_misfit      : misfit context
  !----------------------------------------------------------------------------
  subroutine misfit_copy_ctx(ctx_misfit_in,ctx_misfit_out)
    type(t_misfit)    ,intent(in)     :: ctx_misfit_in
    type(t_misfit)    ,intent(inout)  :: ctx_misfit_out

    ctx_misfit_out%tag_misfit        = ctx_misfit_in%tag_misfit
    ctx_misfit_out%n_field           = ctx_misfit_in%n_field
    ctx_misfit_out%J                 = 0.d0
    ctx_misfit_out%Jregularization   = 0.d0
    ctx_misfit_out%n_obs_reciprocity = ctx_misfit_in%n_obs_reciprocity
    ctx_misfit_out%data_path         = trim(adjustl(ctx_misfit_in%data_path))
    ctx_misfit_out%flag_rcv_offset   = ctx_misfit_in%flag_rcv_offset
    ctx_misfit_out%rcv_offset_x      = ctx_misfit_in%rcv_offset_x
    ctx_misfit_out%rcv_offset_y      = ctx_misfit_in%rcv_offset_y
    ctx_misfit_out%rcv_offset_z      = ctx_misfit_in%rcv_offset_z
    ctx_misfit_out%rcv_offset_rad    = ctx_misfit_in%rcv_offset_rad
    
    if(allocated(ctx_misfit_in%data_field)) then
      allocate(ctx_misfit_out%data_field(ctx_misfit_out%n_field))
      ctx_misfit_out%data_field = ctx_misfit_in%data_field
    end if 
    if(allocated(ctx_misfit_in%Jloc)) then
      allocate(ctx_misfit_out%Jloc(ctx_misfit_out%n_field))
      ctx_misfit_out%Jloc       = 0.0
    end if
    return
  end subroutine misfit_copy_ctx

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> reset misfit information, i.e. set to 0
  !
  !> @param[inout] ctx_misfit      : misfit context
  !----------------------------------------------------------------------------
  subroutine misfit_reset(ctx_misfit)
    type(t_misfit)    ,intent(inout)  :: ctx_misfit

    ctx_misfit%J               = 0.0
    ctx_misfit%Jregularization = 0.0
    if(allocated(ctx_misfit%Jloc)) ctx_misfit%Jloc=0.0
   
    return
  end subroutine misfit_reset

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean misfit information and allocation
  !
  !> @param[inout] ctx_misfit      : misfit context
  !----------------------------------------------------------------------------
  subroutine misfit_clean(ctx_misfit)
    type(t_misfit)    ,intent(inout)  :: ctx_misfit

    ctx_misfit%tag_misfit        =-1
    ctx_misfit%n_field           = 0
    ctx_misfit%niter             = 0
    ctx_misfit%niter_min         = 0
    ctx_misfit%J                 = 0.0
    ctx_misfit%Jregularization   = 0.0
    ctx_misfit%tol_stagnate      = 0.0
    ctx_misfit%n_obs_reciprocity = 0
    ctx_misfit%data_path         = ''
    if(allocated(ctx_misfit%data_field)) deallocate(ctx_misfit%data_field)
    if(allocated(ctx_misfit%Jloc))       deallocate(ctx_misfit%Jloc      )
    ctx_misfit%flag_rcv_offset   = .false.
    ctx_misfit%rcv_offset_x  = 0.d0
    ctx_misfit%rcv_offset_y  = 0.d0
    ctx_misfit%rcv_offset_z  = 0.d0
    ctx_misfit%rcv_offset_rad= 0.d0
    
    return
  end subroutine misfit_clean

end module m_ctx_misfit_function
