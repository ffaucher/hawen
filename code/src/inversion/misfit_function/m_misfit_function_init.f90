!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_misfit_function_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize the misfit functional information
!
!------------------------------------------------------------------------------
module m_misfit_function_init
  
  use m_raise_error,            only : raise_error, t_error
  use m_ctx_parallelism,        only : t_parallelism
  use m_read_parameters_misfit, only : read_parameters_misfit
  use m_ctx_misfit_function,    only : t_misfit
  use m_print_misfit,           only : print_misfit_info
  use m_misfit_norms,           only : misfit_tag_formalism,            &
                                       tag_MISFIT_RECIPROCITY,          &
                                       tag_MISFIT_RECIPROCITY_LOG
  use m_tag_namefield,          only : field_tag_formalism

  implicit none

  private  
  public :: misfit_init
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init misfit information
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] ctx_misfit    : misfit context
  !> @param[inout] parameter_file : parameter file
  !> @param[in]   n_field_ref   : number of reference wavefield tag
  !> @param[in]   field_name_ref: list of the reference tag
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine misfit_init(ctx_paral,ctx_misfit,parameter_file,           &
                         n_field_ref,field_name_ref,ctx_err)
  
    implicit none

    type(t_parallelism)          ,intent(in)   :: ctx_paral
    type(t_misfit)               ,intent(inout):: ctx_misfit
    character(len=*)             ,intent(in)   :: parameter_file
    integer                      ,intent(in)   :: n_field_ref
    character(len=*),allocatable ,intent(in)   :: field_name_ref(:)
    type(t_error)                ,intent(out)  :: ctx_err
    !!
    character(len=512)             :: str_misfit,str_sol
    character(len=512),allocatable :: misfit_comp(:)
    integer                        :: i_sol,i_field
    integer           ,allocatable :: counter(:)

    ctx_err%ierr = 0

    !! initialize the values 
    ctx_misfit%J=0.0

    !! read from parameter files    
    call read_parameters_misfit(parameter_file,ctx_misfit%niter,        &
                         str_misfit,ctx_misfit%n_field,misfit_comp,     &
                         ctx_misfit%data_path,                          &
                         ctx_misfit%n_obs_reciprocity,                  &
                         ctx_misfit%niter_min,ctx_misfit%tol_stagnate,  &
                         ctx_misfit%flag_rcv_offset,                    &
                         ctx_misfit%rcv_offset_x,                       &
                         ctx_misfit%rcv_offset_y,                       &
                         ctx_misfit%rcv_offset_z,                       &
                         ctx_misfit%rcv_offset_rad,ctx_err)

    !! define the tag from the keyword ---------------------------------
    call misfit_tag_formalism(trim(adjustl(str_misfit)),                &
                              ctx_misfit%tag_misfit,ctx_err)
   
    !! retreive the components -----------------------------------------
    if(ctx_misfit%tag_misfit .eq. tag_MISFIT_RECIPROCITY       .or.     &
       ctx_misfit%tag_misfit .eq. tag_MISFIT_RECIPROCITY_LOG) then
      ctx_misfit%n_field = 0 !! automatic
      if(ctx_misfit%n_obs_reciprocity <=0) then
        ctx_err%msg   = "** ERROR: reciprocity needs at least 1 " //    &
                        "obs [misfit_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      endif
    else       
      if(ctx_misfit%n_field .eq. 0) then
        ctx_err%msg   = "** ERROR: No component given [misfit_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      else
        allocate(ctx_misfit%data_field(ctx_misfit%n_field))
        allocate(ctx_misfit%Jloc      (ctx_misfit%n_field))
        ctx_misfit%Jloc = 0.0
      endif 
      
      !! similar to acquisition procedure ------------------------------
      call field_tag_formalism(ctx_misfit%n_field,misfit_comp,          &
                               ctx_misfit%data_field,ctx_err)

      !! ---------------------------------------------------------------
      !! consistency check between equation fields and misfit 
      !! ---------------------------------------------------------------
      allocate(counter(n_field_ref)) !! verify the field are given only once
      counter = 0
      do i_field = 1, ctx_misfit%n_field
        str_misfit=trim(adjustl(ctx_misfit%data_field(i_field)))
        do i_sol = 1, n_field_ref
          str_sol = trim(adjustl(field_name_ref(i_sol)))
          if(trim(adjustl(str_sol)) .eq. trim(adjustl(str_misfit))) then
            counter(i_sol) = counter(i_sol) + 1
          end if
        end do
      end do
      !! check that it is correct, i.e. max is 1 and sum equates 
      if(maxval(counter) > 1 ) then
        ctx_err%msg   = "** ERROR: Some of the fields for the misfit "// &
                        "are given more than once [misfit_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      ! the following does not work if normal/tangent components are used.
      !if(sum(counter) < ctx_misfit%n_field ) then
      !  ctx_err%msg   = "** ERROR: Some of the fields for the misfit "// &
      !                  "are not members of equation [misfit_init] **"
      !  ctx_err%ierr  = -1
      !  ctx_err%critic=.true.
      !  call raise_error(ctx_err)
      !end if
      deallocate(counter)
      !! ---------------------------------------------------------------

    end if  

    !! print infos here
    call print_misfit_info(ctx_paral,ctx_misfit%niter,                  &
                           ctx_misfit%tag_misfit,ctx_misfit%data_path,  &
                           ctx_misfit%n_field,ctx_misfit%data_field,    &
                           ctx_misfit%n_obs_reciprocity,                &
                           ctx_misfit%niter_min,ctx_misfit%tol_stagnate,&
                           ctx_misfit%flag_rcv_offset,                  &
                           ctx_misfit%rcv_offset_x,                     &
                           ctx_misfit%rcv_offset_y,                     &
                           ctx_misfit%rcv_offset_z,                     &
                           ctx_misfit%rcv_offset_rad,ctx_err)

    return
  end subroutine misfit_init

end module m_misfit_function_init
