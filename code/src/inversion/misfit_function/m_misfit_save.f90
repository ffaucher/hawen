!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_misfit_save.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save the residuals informations
!
!------------------------------------------------------------------------------
module m_misfit_save
  
  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_ctx_misfit_function,      only : t_misfit
  use m_ctx_io,                   only : t_io
  use m_print_freq,               only : convert_frequency_unit
  implicit none

  private  
  public :: misfit_save
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> save the misfit functional, using an ascii file. 
  !
  !> @param[in]   ctx_paral     : parallel conte
  !> @param[in]   ctx_misfit    : misfit context
  !> @param[in]   ctx_io        : io context
  !> @param[in]   iter_global   : global iteration number
  !> @param[in]   iter_local    : local iteration number
  !> @param[in]   frequency     : list of group frequencies
  !> @param[in]   mode          : list of group modes
  !> @param[in]   nfreq         : number of frequencies
  !> @param[in]   nmode         : number of modes
  !> @param[in]   flag_freq     : indicate if equation has frequency
  !> @param[in]   flag_mode     : indicate if equation has mode
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine misfit_save(ctx_paral,ctx_misfit,ctx_io,iter_global,       &
                         iter_local,frequency,mode,nfreq,nmode,         &
                         flag_freq,flag_mode,ctx_err)
  
    implicit none
    
    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_misfit)           ,intent(in)   :: ctx_misfit    
    type(t_io)               ,intent(in)   :: ctx_io        
    integer                  ,intent(in)   :: iter_global   
    integer                  ,intent(in)   :: iter_local
    real(kind=4)             ,intent(in)   :: frequency(:,:)
    integer                  ,intent(in)   :: mode(:)
    integer                  ,intent(in)   :: nfreq
    integer                  ,intent(in)   :: nmode
    logical                  ,intent(in)   :: flag_freq
    logical                  ,intent(in)   :: flag_mode
    type(t_error)            ,intent(out)  :: ctx_err
    !! -----------------------------------------------------------------
    !! local
    character(len=1024)            :: filename,str
    integer                        :: unit_file = 710
    character(len=4)               :: unt
    integer                        :: ifreq,ifield,imode
    real                           :: f
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0

    filename = trim(adjustl(ctx_io%workpath_residual)) // "/misfit-function.txt"

    !! Only master works
    if(ctx_paral%master) then
      ! ----------------------------------------------------------------
      !! if it is the very first iteration, we create the file 
      !! otherwize we just open it
      !! -----------------------------------------------------------------
      if(iter_global == 1) then
        open(unit=unit_file, file=trim(adjustl(filename)),              &
             status='replace', action='write', iostat=ctx_err%ierr)
      else
        open(unit=unit_file, file=trim(adjustl(filename)),              &
             position='append', action='write', iostat=ctx_err%ierr)
      end if
      !! in case of error
      if(ctx_err%ierr .ne. 0) then
        ctx_err%msg   = "** ERROR: opening misfit file [misfit_save]**"
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      ! ----------------------------------------------------------------
      
      
      ! ----------------------------------------------------------------
      !! in case we plot the frequency and/or mode informations
      ! ----------------------------------------------------------------
      if(iter_local == 1) then
        write(unit_file,'(a)') &
           "**************************************************"//&
           "**************************************************"
        if(flag_freq) then
          write(unit_file,'(a,i0,a)') "---------- ",nfreq," simultaneous frequency:"
          !! print list of frequency:
          do ifreq = 1,nfreq
            !! get unit for printing
            f=frequency(ifreq,1)
            call convert_frequency_unit(f,unt)
  
            !! print current frequency 
            write(unit_file,'(a,f12.4,a,f0.4)') "   o complex frequency ",&
                                        f,trim(unt)//" +   ", frequency(ifreq,2)
          end do
        else
          write(unit_file,'(a)') "---------- this problem has no frequency"
        end if
        if(flag_mode) then
          write(unit_file,'(a,i0,a)') "---------- ",nmode," simultaneous modes:"
          !! print list of modes
          do imode = 1,nmode
            !! print current mode
            write(unit_file,'(a,i0)') "   o mode              ",mode(imode)
          end do
        else
          write(unit_file,'(a)') "---------- this problem has no mode"
        end if
        ! ---------------------------------------------------------

        write(unit_file,'(a)')                                          &
           "--------------------------------------------------"       //&
           "--------------------------------------------------"
        !! we recall the name of the colmuns
        str="global iter. | local iter. |      J      |"
        do ifield=1,ctx_misfit%n_field
          str = trim(adjustl(str)) // "     "//                         &
                ctx_misfit%data_field(ifield)//"     |"
        end do
        !! finally the regularization term 
        str = trim(adjustl(str)) // "  regularization |"

        write(unit_file,'(a)') trim(adjustl(str))
        write(unit_file,'(a)')                                          &
           "--------------------------------------------------"       //&
           "--------------------------------------------------"

      end if  
      ! ----------------------------------------------------------------

      ! ----------------------------------------------------------------
      !! write what we have to
      ! ----------------------------------------------------------------
      write(str,'(i9,a,i9,a,es12.5)') iter_global,"     ",iter_local ,  &
                                      "     ",ctx_misfit%J
      do ifield=1,ctx_misfit%n_field
        write(str,'(a,es12.5,a)') trim(str) // "  ",ctx_misfit%Jloc(ifield)
      end do
      !! regularization at the end 
      write(str,'(a,es12.5,a)') trim(str) // "   ",ctx_misfit%Jregularization
      write(unit_file,'(a)') trim(str)
      
      ! ----------------------------------------------------------------
      close(unit=unit_file)
    end if

    return
  end subroutine misfit_save

end module m_misfit_save
