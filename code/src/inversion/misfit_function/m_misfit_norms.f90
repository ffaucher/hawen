!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_misfit_norms.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the norm tags for the misfit functional, it serves
!> to evaluate the functional but also to obtain the right-hand side for
!> the backward problem. This rhs is given by
!>   \f$  \partial_u J \f$.
!> Therefore, we need to derivate the misfit with respect to \f$u\f$.
!> This step must be adapted to the choice of misfit.
!> Assume that the misfit takes the phase of u, then we to derivate
!> this function with respect to u.
!>
!> For instance, by default (l2 norm) we only have a restriction of u
!> and \f$J = J(Ru) \f$, then the derivative is 
!> \f$R* \partial_x J(x)\f$, that is, we use the chain rule.
!> 
!
!
!------------------------------------------------------------------------------
module m_misfit_norms

  use m_raise_error,            only : raise_error, t_error
  use m_reciprocity_formula,    only : reciprocity_formula
  use m_define_precision,       only : RKIND_MAT
  use, intrinsic                    :: ieee_arithmetic

  implicit none

  !> tag least-squares: direct difference between fields
  integer, parameter :: tag_MISFIT_L2              = 1
  integer, parameter :: tag_MISFIT_L2_REAL         = 2
  integer, parameter :: tag_MISFIT_L2_IMAG         = 3
  !> tag log-norm: difference between log(fields)
  integer, parameter :: tag_MISFIT_LOG             = 5
  !> tag frequency domain: phase and amplitude
  integer, parameter :: tag_MISFIT_L2_MODUL        = 21
  integer, parameter :: tag_MISFIT_L2_PHASE        = 22
  integer, parameter :: tag_MISFIT_LOG_MODUL       = 23
  !> tag reciprocity: using multi-components data with reciprocity
  integer, parameter :: tag_MISFIT_RECIPROCITY     = 10
  integer, parameter :: tag_MISFIT_RECIPROCITY_LOG = 11
  !> tag power: using the fields at a decided power
  integer, parameter :: tag_MISFIT_POWER2_L2       = 102
  integer, parameter :: tag_MISFIT_POWER3_L2       = 103
  !> tag for FK transform
  integer, parameter :: tag_MISFIT_FK_MODULUS      = 201 !! using |FK(u)|

  private
  public :: misfit_tag_formalism, misfit_tag_naming, misfit_norm
  public :: misfit_norm_reciprocity, misfit_norm_source
  public :: tag_MISFIT_RECIPROCITY, tag_MISFIT_RECIPROCITY_LOG
  public :: tag_MISFIT_FK_MODULUS
           
  contains 
    
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> decide the misfit tag from keyword
  !
  !> @param[in]   name_in      : input name
  !> @param[out]  tag_misfit   : output tag
  !> @param[inout] ctx_err      : error context
  !----------------------------------------------------------------------------
  subroutine misfit_tag_formalism(name_in,tag_misfit,ctx_err)
    implicit none

    character(len=*)                   ,intent(in)   :: name_in
    integer                            ,intent(out)  :: tag_misfit
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------

    ctx_err%ierr  = 0

    select case (trim(adjustl(name_in)))
      case('l2','L2','least-squares','LEAST-SQUARES','Least-Squares')
        tag_misfit = tag_MISFIT_L2
      case('l2_real','L2_real','least-squares_real',                    &
           'LEAST-SQUARES_REAL','Least-Squares_Real')
        tag_misfit = tag_MISFIT_L2_REAL
      case('l2_imag','L2_imag','least-squares_imag',                    &
           'LEAST-SQUARES_IMAG','Least-Squares_Imag')
        tag_misfit = tag_MISFIT_L2_IMAG
      case('log','LOG')
        tag_misfit = tag_MISFIT_LOG
      case('l2_modulus','L2_modulus','modulus')
        tag_misfit = tag_MISFIT_L2_MODUL
      case('log_modulus','LOG_modulus')
        tag_misfit = tag_MISFIT_LOG_MODUL
      case('l2_phase','L2_phase','phase')
        tag_misfit = tag_MISFIT_L2_PHASE
      case('l2_power2','L2_power2','L2_POWER2','least-squares_power2')
        tag_misfit = tag_MISFIT_POWER2_L2
      case('l2_power3','L2_power3','L2_POWER3','least-squares_power3')
        tag_misfit = tag_MISFIT_POWER3_L2
      case('FKtransform_modulus','FKTRANSFORM_MODULUS','FK_modulus')
        tag_misfit = tag_MISFIT_FK_MODULUS
      
      !! Rk: no need for log with power that just add a constant scale...
      case('reciprocity','Reciprocity','RECIPROCITY','reciprocity-gap', &
           'Reciprocity-gap','RECIPROCITY-GAP','FRgWI')
        tag_misfit = tag_MISFIT_RECIPROCITY
      case('reciprocity_log','Reciprocity_Log','RECIPROCITY_LOG','FRgWI-log')
        tag_misfit = tag_MISFIT_RECIPROCITY_LOG
      case default
        ctx_err%msg   = "** ERROR: Unrecognized misfit norm  [misfit_init] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
        
    return
    
  end subroutine misfit_tag_formalism

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> from the tag, we extract a word for printing
  !
  !> @param[in]   tag_misfit    : input tag
  !> @param[out]  str           : string out
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine misfit_tag_naming(tag_misfit,str,ctx_err)
    implicit none

    integer                            ,intent(in)   :: tag_misfit
    character(len=128)                 ,intent(inout):: str
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    str = ''
    select case (tag_misfit)
      case(tag_MISFIT_L2)
        str='least-squares misfit functional '
      case(tag_MISFIT_L2_REAL)
        str='real part least-squares misfit functional'
      case(tag_MISFIT_L2_IMAG)
        str='imaginary part least-squares misfit functional'
      case(tag_MISFIT_LOG)
        str='log(field) misfit functional'
      case(tag_MISFIT_L2_MODUL )
        str='modulus misfit functional'
      case(tag_MISFIT_L2_PHASE )
        str='phase misfit functional'
      case(tag_MISFIT_LOG_MODUL)
        str='log(modulus) misfit functional'
      case(tag_MISFIT_POWER2_L2)
        str='least-squares with squared field misfit functional '
      case(tag_MISFIT_POWER3_L2)
        str='least-squares with cubed field misfit functional '
      case(tag_MISFIT_RECIPROCITY)
        str='reciprocity-gap waveform inversion'
      case(tag_MISFIT_RECIPROCITY_LOG)
        str='reciprocity-gap waveform inversion using log'
      case(tag_MISFIT_FK_MODULUS)
        str='modulus of the FK transform'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized misfit " //&
                        "[misfit_tag_naming]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
    
  end subroutine misfit_tag_naming

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> compute the norms between two vectors
  !
  !> @param[in]   tag_misfit    : input tag
  !> @param[in]   n_val         : number of values to test
  !> @param[in]   obs           : observations
  !> @param[in]   simu          : simulations
  !> @param[out]  rhs           : rhs for back-propagation
  !> @param[out]  cost          : local cost
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine misfit_norm(tag_misfit,n_val,obs,simu,rhs,cost,ctx_err)
    implicit none

    integer                     ,intent(in)    :: tag_misfit
    integer                     ,intent(in)    :: n_val
    complex(kind=RKIND_MAT)     ,intent(in)    :: obs(:),simu(:)
    complex(kind=8),allocatable ,intent(inout) :: rhs(:)
    real(kind=8)                ,intent(inout) :: cost
    type(t_error)               ,intent(inout) :: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8)  :: a1,a2,b1,b2,modulus
    real   (kind=4)  :: tol
    complex(kind=8)  :: diff,dmodulus,dtheta
    integer          :: k
    !! -----------------------------------------------------------------
    
    tol = 1.E-20 !! tolerance, e.g., for log norm 
    
    rhs= 0.0
    cost= 0.0
    select case (tag_misfit)
      case(tag_MISFIT_L2)
        do k=1,n_val
          diff  = dcmplx(simu(k)-obs(k))
          rhs(k)= diff
          cost  = cost + dble(diff*conjg(diff))
        end do
        
      !! ***************************************************************
      !! we have 
      !!    J = 1/2 || R(y(u))  -d ||^2
      !! R is the restriction to the receivers
      !! y is a function (e.g., log, phase, etc.)
      !!
      !! the rhs is given by 
      !!  \partial_u J  = y'(u)*  R*  (R(y(u))  -d )
      !! Hence we need to derive this function y properly.
      !! ***************************************************************
        
      case(tag_MISFIT_L2_REAL)
        !! can be seen as a restriction: assume the simulation are 
        !! a vector: (x1, x2, ..., y1, y2, ...)
        !! with x the real part and y the imaginary part, then, it 
        !! only keeps the real part, i.e., 
        !! R* must enforce the real part only.
        do k=1,n_val
          diff  = dcmplx(real(simu(k))-real(obs(k)), 0.d0)
          rhs(k)= diff
          cost  = cost + dble(diff*conjg(diff))
        end do
        
      case(tag_MISFIT_L2_IMAG)     
        !! -------------------------------------------------------------
        !! can be seen as a restriction: assume the simulation are 
        !! a vector: (x1, x2, ..., y1, y2, ...)
        !! with x the real part and y the imaginary part, then, it 
        !! only keeps the imaginary part, i.e., 
        !! R* must enforce the imaginary part only.
        !! -------------------------------------------------------------
        do k=1,n_val
          diff  = dcmplx(aimag(simu(k))-aimag(obs(k)))
          !! on the imaginary part only
          rhs(k)= dcmplx(0.0,1.0)*diff
          cost  = cost + dble(diff*conjg(diff))
        end do

      case(tag_MISFIT_LOG)
        do k=1,n_val
          if(abs(simu(k)) > tol .and. abs(obs(k)) > tol) then
            diff  = dcmplx(log(simu(k))-log(obs(k)))
            rhs(k)= diff / conjg(simu(k))
            cost  = cost + dble(diff*conjg(diff))
          endif
        end do
        
      !! case(tag_MISFIT_LOG_REAL)
        !! not that simple, if we take a vector that splits the 
        !! real and imaginary part for the simu:
        !! (x1, x2, ..., y1, y2, ...)
        !! when taking the log of that, how do you deal with 
        !! negative values that lead to a complex number then?

      case(tag_MISFIT_L2_MODUL )
        !! modulus for x=a+ib is given by sqrt(a² + b²) = abs(x)
        !! we have J = 1/2 || R f(u) - d ||², with 
        !! f(u) = sqrt(u conj(u))
        !! the RHS is given by dJ / du = dJ/df.df/du
        !! with df/du = 1/2 conj(u) / sqrt(u conj(u))
        !!            = 1/2 conj(u) / abs(u).
        !! with the adjoint gives
        !!            
        do k=1,n_val
          if(abs(simu(k)) > tol) then
            diff  = abs(simu(k)) - abs(obs(k))
            rhs(k)= diff * conjg(1.d0/2.d0 * conjg(simu(k))/abs(simu(k)))
            cost  = cost + dble(diff*conjg(diff))
          end if
        end do
              
      case(tag_MISFIT_L2_PHASE )
        !! for x=a+ib is given by A exp(i \theta)
        !! with \theta the phase = arctan(b/a).
        !! we have
        !!
        !!  u = A exp(i \theta)
        !!    derivation:
        !! du = dA exp(i \theta) + i d\theta A exp(i \theta)
        !!
        !!    divide by u = A exp(i \theta)
        !! du/u = dA/A + i d\theta 
        !!  such that
        !! d\theta = i (dA/A - du/u) 
        !! dA is the derivative of the modulus that we have computed
        !! above
        !!
        !! the RHS is given by dJ / du = dJ/df.df/du
        !! =  dJ/d\theta . d\theta/du
        do k=1,n_val
          if(abs(simu(k)) > tol .and. abs(obs(k)) > tol) then

            !! argument is given by the fortran function ATAN2(y,x)
            a1 = dble(real (simu(k))) ; a2 = dble(real (obs(k)))
            b1 = dble(aimag(simu(k))) ; b2 = dble(aimag(obs(k)))
            diff    = datan2(b1,a1) - datan2(b2,a2)
            modulus = abs(simu(k))
            dmodulus= dcmplx(1.d0/2.d0 * conjg(simu(k))/modulus)
            dtheta  = dcmplx(0.d0,1.d0) * (dmodulus/modulus - 1.d0/simu(k))
            rhs(k)= diff * conjg(dtheta)
            cost  = cost + dble(diff*conjg(diff))

          end if
        end do

      case(tag_MISFIT_LOG_MODUL)
        !! same but using (log(w))' = w'/w, with w = modulus.
        do k=1,n_val
          if(abs(simu(k)) > tol .and. abs(obs(k)) > tol) then
            diff  = log(abs(simu(k))) - log(abs(obs(k)))
            rhs(k)= diff * conjg(1.d0/abs(simu(k)) * 1.d0/2.d0 *        &
                                 conjg(simu(k))/abs(simu(k)))
            cost  = cost + dble(diff*conjg(diff))
          end if
        end do

      !! norms with powered fields -------------------------------------
      case(tag_MISFIT_POWER2_L2)
        do k=1,n_val
          !!    J = 1/2(R(U²) - d²)² ; du(J)= (2RU)* (R(U²) - d²)
          diff  = dcmplx(simu(k)**2.-obs(k)**2.)
          rhs(k)= 2.d0*conjg(simu(k))*diff 
          cost  = cost + dble(diff*conjg(diff))
        end do

      case(tag_MISFIT_POWER3_L2)
        do k=1,n_val
          !!    J = 1/2(R(U³) - d³)² ; du(J)= (3RU²)* (R(U³) - d³)
          diff  = dcmplx(simu(k)**3.-obs(k)**3.)
          rhs(k)= 3.d0*conjg(simu(k)**2.)*diff 
          cost  = cost + dble(diff*conjg(diff))
        end do
     
      !! using the FK transform ----------------------------------------
      case(tag_MISFIT_FK_MODULUS)
        do k=1,n_val
          !!    J = 1/2 ( |FK(R(u))| - |FK(d)| )² 
          !!      = 1/2 (   h(g(u))  - |FK(d)| )² 
          !! 
          !! NOTHE THAT (FK)* = FK^{-1}.
          !!
          !!  we have dJ/du = dJ/dh dh/dg dg/du
          !!     0) dJ/dh =  (h(g(u)) - |FK(d)|)
          !!     a) h(g) = | g | = sqrt(g conj(g))
          !!       dh/dg = 1/2 conj(g) / sqrt(g conj(g))
          !!             = 1/2 conj(g) /  abs(g).
          !!     b) g=Fk(u)
          !!      dg/duj= Fk^{-1}
          !!
          !! in the end we should have
          !! dJ/duj = FK^{-1}(1/2 conj( conj(FK(Ru))/abs(FK(Ru)) )
          !!                                  * (|FK(R(u))| - |FK(d)|))
          if(abs(simu(k)) > tol) then
            diff  = abs(simu(k)) - abs(obs(k))
            rhs(k)= diff * conjg(1.d0/2.d0 *                            &
                !!(conjg(simu(k))/abs(simu(k)) + simu(k)/abs(simu(k)) ) &
                   conjg(simu(k))/abs(simu(k)) &
                  )
            cost  = cost + dble(diff*conjg(diff))
          end if
        end do
              
      !! ---------------------------------------------------------------
      case default
        ctx_err%msg   = "** ERROR: Unrecognized misfit "             // &
                        "[misfit_norm]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
    end select
    
    !! check for bounds 
    if(ieee_is_nan(cost) .or. abs(cost) > Huge(1.D0) ) then
      ctx_err%msg   = "** ERROR: the cost function is NaN or Infinity "//&
                      "[misfit_norm]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
    end if
    
    return
  end subroutine misfit_norm

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> compute the norms using reciprocity: depends if acoustic or elastic
  !
  !> @param[in]   tag_misfit    : input tag
  !> @param[in]   dim_domain    : domain dimension
  !> @param[in]   n_val         : number of values to test
  !> @param[in]   obs           : observations
  !> @param[in]   simu          : simulations
  !> @param[out]  rhs           : rhs for back-propagation
  !> @param[out]  cost          : local cost
  !> @param[out]  normals_rcvx  : normals at receivers position x
  !> @param[out]  normals_rcvy  : normals at receivers position y
  !> @param[out]  normals_rcvz  : normals at receivers position z
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine misfit_norm_reciprocity(tag_misfit,dim_domain,n_val,obs,   &
                                     simu,rhs,cost,normals_rcvx,        &
                                     normals_rcvy,normals_rcvz,ctx_err)
    implicit none

    integer                             ,intent(in)   :: tag_misfit,dim_domain
    integer                             ,intent(in)   :: n_val
    complex(kind=RKIND_MAT),allocatable ,intent(in)   :: obs(:,:),simu(:,:)
    complex(kind=8)        ,allocatable ,intent(inout):: rhs(:,:)
    real(kind=8)           ,allocatable ,intent(in)   :: normals_rcvx(:)
    real(kind=8)           ,allocatable ,intent(in)   :: normals_rcvy(:)
    real(kind=8)           ,allocatable ,intent(in)   :: normals_rcvz(:)
    real(kind=8)                        ,intent(inout):: cost
    type(t_error)                       ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=4)  :: tol
    logical       :: flag_log
    !! -----------------------------------------------------------------
   
    tol = 1.E-20 !! for log norm only
    
    rhs = 0.0
    cost= 0.0
    select case (tag_misfit)
      case(tag_MISFIT_RECIPROCITY)
        flag_log = .false. 
      case(tag_MISFIT_RECIPROCITY_LOG)
        flag_log = .true. 
      case default
        ctx_err%msg   = "** ERROR: Unrecognized misfit "             // &
                        "[misfit_norm_reciprocity]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return
    end select
    
    !! -----------------------------------------------------------------
    !! The reciprocity depends on the equation
    !! -----------------------------------------------------------------
    call reciprocity_formula(dim_domain,obs,simu,n_val,cost,rhs,        &
                             normals_rcvx,normals_rcvy,normals_rcvz,    &
                             flag_log,ctx_err)

    !! check for bounds 
    if(ieee_is_nan(cost) .or. abs(cost) > Huge(1.D0) ) then
      ctx_err%msg   = "** ERROR: the cost function is NaN or Infinity "//&
                      "[misfit_norm]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
    end if

    return
  end subroutine misfit_norm_reciprocity

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> compute the norms between observations and simulations in the 
  !> context of source reconstruction. We only support the L2 for now.
  !
  !> @param[in]   n_val         : number of values to test
  !> @param[in]   obs           : observations
  !> @param[in]   simu          : simulations
  !> @param[in]   src_val       : current source value
  !> @param[out]  num           : numerator
  !> @param[out]  den           : denominator
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine misfit_norm_source(n_val,obs,simu,src_val,num,den,ctx_err)
    implicit none

    integer                     ,intent(in)   :: n_val
    complex(kind=RKIND_MAT)     ,intent(in)   :: obs(:),simu(:)
    complex(kind=8)             ,intent(in)   :: src_val
    complex(kind=8)             ,intent(inout):: num
    complex(kind=8)             ,intent(inout):: den
    type(t_error)               ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer          :: k
    !! -----------------------------------------------------------------
    
    !! only L2 norm for now. -------------------------------------------
    num = 0.0
    den = 0.0
    do k=1,n_val
      num = num +   obs(k)*conjg(simu(k)/src_val)
      den = den + (simu(k)/src_val)*conjg(simu(k)/src_val)
    end do
    if( ieee_is_nan(abs(den)) .or. abs(real(den)) > Huge(1.D0) .or.     &
                             abs(imag(den)) > Huge(1.D0) .or.           &
        ieee_is_nan(abs(num)) .or. abs(real(num)) > Huge(1.D0) .or.     &
                             abs(imag(num)) > Huge(1.D0)) then
        ctx_err%msg   = "** ERROR: source update has NaN or Infinity "//&
                        "value [misfit_norm_source]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine misfit_norm_source


end module m_misfit_norms
