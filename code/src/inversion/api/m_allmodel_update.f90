!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_allmodel_update.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an api to update all models
!> that are inverted
!
!------------------------------------------------------------------------------
module m_allmodel_update

  !! module used --------------------------------------------------------------
  use m_raise_error,                 only: raise_error, t_error
  use m_ctx_parallelism,             only: t_parallelism
  use m_ctx_model,                   only: t_model
  use m_ctx_gradient,                only: t_gradient
  use m_ctx_search_dir,              only: t_search_dir
  use m_ctx_linesearch,              only: t_linesearch
  use m_model_update,                only: model_update
  !! --------------------------------------------------------------------------

  implicit none

  private
  public :: allmodel_update

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> update all models in the current physical equation
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[inout] ctx_model      : context model
  !> @param[in]    ctx_grad       : context gradient
  !> @param[in]    ctx_search     : context search direction
  !> @param[in]    ctx_linesearch : context linesearch
  !> @param[in]    freq           : current frequency
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine allmodel_update(ctx_paral,ctx_model,ctx_grad,ctx_search,   &
                             ctx_linesearch,freq,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_model)               ,intent(inout):: ctx_model
    type(t_gradient)            ,intent(in)   :: ctx_grad
    type(t_search_dir)          ,intent(in)   :: ctx_search
    type(t_linesearch)          ,intent(in)   :: ctx_linesearch
    complex(kind=8)             ,intent(in)   :: freq
    type(t_error)               ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                              :: k
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    do k = 1, ctx_search%n_search
      call model_update(ctx_paral,ctx_model,ctx_search%search(:,k),     &
                        ctx_linesearch%step_current(k),                 &
                        trim(adjustl(ctx_search%param_name(k))),        &
                        ctx_grad%param_func(k),ctx_grad%param_name,freq,&
                        ctx_err)
    end do

    return
  end subroutine allmodel_update

end module m_allmodel_update
