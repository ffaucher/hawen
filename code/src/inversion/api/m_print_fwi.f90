!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_fwi.f90
!
!> @author 
!> F. Faucher, Faculty of Mathematics, University of Vienna
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen printing for general FWI infos, for
!> instance, it prints the time of the operations, a welcome message
!> at the beginning of each iteration, etc.
!
!------------------------------------------------------------------------------
module m_print_fwi

  use m_ctx_parallelism,      only : t_parallelism
  use m_print_basics,         only : separator,indicator,marker,        &
                                     cast_time,cast_memory
  use m_print_freq,           only : convert_frequency_unit
  implicit none
  
  private
  public :: print_fwi_welcome_iter,print_fwi_outcome_iter
  public :: print_fwi_iter_time,print_fwi_iter_mem
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial information for FWI with welcome message 
  !> for each group of frequencies and modes. 
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   igp             : current group
  !> @param[in]   ngp             : total number of group 
  !> @param[in]   nfreq           : number of frequency in the group
  !> @param[in]   nmode           : number of modes in the group
  !> @param[in]   frequency       : list of frequency in group
  !> @param[in]   mode            : list of mode in group
  !> @param[in]   iter            : current iteration
  !> @param[in]   niter           : total number of itartions
  !> @param[in]   flag_freq       : do we consider frequency in equation
  !> @param[in]   flag_mode       : do we consider mode in equation
  !> @param[in]   iter_gb         : global iteration
  !----------------------------------------------------------------------------
  subroutine print_fwi_welcome_iter(ctx_paral,igp,ngp,nfreq,nmode,      &
                                    frequency,mode,iter,niter,flag_freq,&
                                    flag_mode,iter_gb)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: nfreq,nmode,igp,ngp
    integer                       ,intent(in)   :: iter,niter,iter_gb
    real                          ,intent(in)   :: frequency(:,:)
    integer                       ,intent(in)   :: mode     (:)
    logical                       ,intent(in)   :: flag_freq
    logical                       ,intent(in)   :: flag_mode
    !!
    character(len=128) :: str
    character(len=4)   :: unt
    integer            :: ifreq,imode
    real               :: f

    !! -----------------------------------------------------------------
    !! first print group and frequency information
    !! -----------------------------------------------------------------    
    if(ctx_paral%master) then
      write(6,'(a)')  marker
      write(6,'(2a)') indicator, " FWI start iteration "
    endif 
    
    !! list of frequencies and modes in this group
    if(ctx_paral%master .and. (flag_freq .or. flag_mode)) then
      !! suffix for frequency and mode
      str = ''

      if(flag_freq) then !! count the frequency ------------------------
        if(nfreq == 1) then
          str = " containing 1 frequency"
        else
          write(str,'(a,i0,a)') " containing ",nfreq," frequencies"
        end if
      end if
      if(flag_mode) then !! count the modes ------------------------
        if(.not. flag_freq) then
          if(nmode == 1) then
            str = " containing 1 mode"
          else
            write(str,'(a,i0,a)') " containing ",nmode," modes"
          end if
        else
          if(nmode == 1) then
            write(str,'(a)') trim(adjustl(str)) // " and 1 mode"
          else
            write(str,'(a,i0,a)') trim(adjustl(str))//" and ",nmode," modes"
          end if
        end if
      end if
      
      !! print group info 
      write(6,'(a,i0,a,i0,a)') "- Group ", igp," / ",ngp, trim(str)

      !! list of frequencies in the group
      if(flag_freq) then
        do ifreq = 1,nfreq
          !! get memory unit for printing
          f=frequency(ifreq,1) ; 
          call convert_frequency_unit(f,unt)

          !! print current frequency 
          write(6,'(a,f12.4,a,f0.4)') "----- complex frequency : ",       &
                                      f,trim(unt)//" +   ", frequency(ifreq,2)
        end do
      end if
      !! list of modes in the group
      if(flag_mode) then
        do imode = 1,nmode
          !! print current frequency 
          write(6,'(a,i0)') "----- mode              : ",mode(imode)
        end do
      end if

      !! a small break on the screen
      write(6,'(a)')  "-------------------------"
    endif


    !! list of modes ---------------------------------------------------
    if(ctx_paral%master .and. flag_mode) then
      !! suffix for single mode
      str = ''
      if(nfreq == 1) then
        str = " containing 1 mode"
      else
        write(str,'(a,i0,a)') " containing ",nmode," modes"
      end if
      !! print group info 
      write(6,'(a,i0,a,i0,a)') "- Group ", igp," / ",ngp, trim(str)
      
      !! print list of frequency:
      do ifreq = 1,nfreq
        !! get memory unit for printing
        f=frequency(ifreq,1) ; unt="  Hz"
        if(f < 0.1) then !! too low
          f=f*1000. ; unt=" mHz"
          if(f < 0.1) then
            f=f*1000. ; unt=" uHz"
            if(f < 0.1) then
              f=f*1000. ; unt=" nHz"
            end if
          end if
        else if(f>1000.) then !! too high
          f=f/1000. ; unt=" kHz"
          if(f>1000.) then
            f=f/1000. ; unt=" MHz"
            if(f>1000.) then
              f=f/1000. ; unt=" GHz"
            end if
          end if
        endif
        !! print current frequency 
        write(6,'(a,f12.4,a,f0.4)') "----- complex frequency : ",       &
                                    f,trim(unt)//" +   ", frequency(ifreq,2)
      end do
      write(6,'(a)')  "-------------------------"
    endif

 
    !! -----------------------------------------------------------------
    !! first iteration information
    !! -----------------------------------------------------------------
    if(ctx_paral%master) then
      write(6,'(a,i0,a,i0)') "- current iteration ", iter," / ",niter
      write(6,'(a,i0,a,i0)') "- global  iteration ", iter_gb
      write(6,'(a)')  marker
    end if  
    !! -----------------------------------------------------------------
    return
  end subroutine print_fwi_welcome_iter

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print information after current iteration is done, including the
  !> evolution of the misfit function over the last 5 iterations
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   iter            : current iteration
  !> @param[in]   niter           : total number of itartions
  !> @param[in]   iter_gb         : global iteration
  !> @param[in]   time_tot        : total iteration time
  !> @param[in]   improve         : last 5 iteration improvement
  !> @param[in]   flag_stagnate   : flag for stagnation
  !----------------------------------------------------------------------------
  subroutine print_fwi_outcome_iter(ctx_paral,iter,niter,iter_gb,       &
                                    time_tot,improve,flag_stagnate)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: iter,niter,iter_gb
    real(kind=8)                  ,intent(in)   :: time_tot
    real(kind=8)                  ,intent(in)   :: improve
    logical                       ,intent(in)   :: flag_stagnate
    !!
    character(len=64) :: str_time

    !! -----------------------------------------------------------------
    !! first print group and frequency information
    !! -----------------------------------------------------------------    
    if(ctx_paral%master) then
      write(6,'(a)')  marker
      write(6,'(2a)') indicator, " FWI end iteration "
    endif 
    
    if(ctx_paral%master) then
      !! infos iterations
      write(6,'(a,i0,a,i0)') "- current iteration ", iter," / ",niter
      write(6,'(a,i0,a,i0)') "- global  iteration ", iter_gb
      if(iter > 4) then
        write(6,'(a,f0.4,a)') "- misfit improv. last 5 iter : ",improve*100.,"%"
      end if
      if(flag_stagnate) then
        write(6,'(2a)') indicator, " STAGNATION BELOW TOLERANCE => next freq."
      end if
      !! total time
      call cast_time(time_tot,str_time)
      write(6,'(3a)') indicator, " Total iteration time: ",trim(adjustl(str_time))
      write(6,'(a)')  marker
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine print_fwi_outcome_iter

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print information on timing infos: this is a generic subroutine
  !> that works with an input operation (e.g., gradient, linesearch, etc.)
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   operation       : type of operation done 
  !> @param[in]   timing          : time to do it
  !----------------------------------------------------------------------------
  subroutine print_fwi_iter_time(ctx_paral,operation,timing)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    character(len=*)              ,intent(in)   :: operation
    real(kind=8)                  ,intent(in)   :: timing
    !!
    character(len=64) :: str_time,str

    if(ctx_paral%master) then
      call cast_time(timing,str_time)
      str=""
      select case (trim(adjustl(operation)))
        case('matrix_create')
          str="- matrix creation time       : "
        case('matrix_facto')
          str="- matrix factorization time  : "
        case('matrix_solve')
          str="- matrix solve (fwd/bwd) time: "
        case('rhs')
          str="- rhs (fwd/bwd) creation time: "
        case('solve')
          str="- solve system (fwd/bwd) time: "
        case('field')
          str="- field distribute time      : "
        case('gradient')
          str="- gradient computation time  : "
        case('search_direction','search_dir')
          str="- search direction time      : "
        case('linesearch')
          str="- linesearch computation time: "
        case('clean_io')
          str="- delete array on disk time  : "
        case default
          str='- '      // trim(adjustl(operation)) // ' time'
          str=str(1:29) //": "
      end select
      write(6,'(2a)') str(1:31), trim(adjustl(str_time))
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine print_fwi_iter_time
  
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print information on memory infos: this is a generic subroutine
  !> that works with an input operation (e.g., gradient, linesearch, etc.)
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   operation       : type of operation done 
  !> @param[in]   mem             : memory to do it
  !----------------------------------------------------------------------------
  subroutine print_fwi_iter_mem(ctx_paral,operation,mem)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    character(len=*)              ,intent(in)   :: operation
    integer(kind=8)               ,intent(in)   :: mem
    !!
    character(len=64) :: str_mem,str

    if(ctx_paral%master) then
      call cast_memory(mem,str_mem)
      str=""
      select case (trim(adjustl(operation)))
        case('matrix_facto')
          str="- matrix factorization memory: "
        case default
          str='- '      // trim(adjustl(operation)) // ' memory'
          str=str(1:29) //": "
      end select
      write(6,'(2a)') str(1:31), trim(adjustl(str_mem))
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine print_fwi_iter_mem

end module m_print_fwi
