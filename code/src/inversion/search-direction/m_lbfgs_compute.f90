!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_lbfgs_compute.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to perform the Limited-BFGS search direction
!> following the steps given by [Nocedal]
!
!------------------------------------------------------------------------------
module m_lbfgs_compute
  
  use m_raise_error,                only : raise_error, t_error
  use m_define_precision,           only : IKIND_MESH
  use m_distribute_error,           only : distribute_error
  use m_ctx_parallelism,            only : t_parallelism
  use m_ctx_domain,                 only : t_domain
  use m_gradient_load_bin,          only : gradient_load_per_cell
  use m_allreduce_sum,              only : allreduce_sum
  use m_allreduce_min,              only : allreduce_min
  use m_grad_model_function,        only : apply_model_function

  implicit none

  private  

  public  :: lbfgs_compute
  private :: model_load_lbfgs_per_cell

  contains

  
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> compute limited-BFGS search direction
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[inout] ctx_err          : error context
  !---------------------------------------------------------------------
  subroutine lbfgs_compute(ctx_paral,ctx_domain,grad_in,search,         &
                           param_name,tag_function,iter_gb,iter_loc,    &
                           ncells,workpath_grad,workpath_model,happrox, &
                           flag_happrox,ctx_err)
  
    implicit none

    type(t_parallelism)         , intent(in)    :: ctx_paral
    type(t_domain)              , intent(in)    :: ctx_domain
    real            ,allocatable, intent(in)    :: grad_in (:)
    real            ,allocatable, intent(inout) :: search  (:)
    real(kind=4)                                :: happrox (:)
    character(len=*)            , intent(in)    :: workpath_grad
    character(len=*)            , intent(in)    :: workpath_model
    character(len=*)            , intent(in)    :: param_name
    integer(kind=IKIND_MESH)    , intent(in)    :: ncells
    integer                     , intent(in)    :: iter_gb
    integer                     , intent(in)    :: tag_function,iter_loc
    logical                     , intent(in)    :: flag_happrox
    type(t_error)               , intent(out)   :: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: ncells_gb,ic
    integer                  :: m,i
    real(kind=4),allocatable :: model_cur(:),model_old(:),grad_cur(:)
    real(kind=4),allocatable :: grad_old(:)
    real(kind=8),allocatable :: s(:),r(:),q(:),y(:),alpha_iter(:)
    real(kind=8)             :: local_coeff,coeff,rho,local_num
    real(kind=8)             :: num,den,alpha,beta,local_den,gam
    real(kind=8)             :: tol=tiny(1.0) !! simple precision
    real(kind=4)             :: myinfinity = HUGE(1.0) !! HUGE(1.0d0)
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0
    
    !! 0. if an Hessian approximation is given, we need to check its
    !!    values are usable
    !! -----------------------------------------------------------------
    if(flag_happrox) then
      !! in case there are some problem with hessian scaling
      if(minval(happrox(:)) < tiny(1.0))      then 
        ctx_err%ierr=-21
        ctx_err%msg   = "** ERROR: Hessian approximation in L-BFGS has "//&
                        "values close to zero [lbfgs_compute]**"
        ctx_err%critic=.true.
      end if
      if(maxval(happrox(:)) > myinfinity) then 
        ctx_err%ierr=-20
        ctx_err%msg   = "** ERROR: Hessian approximation in L-BFGS has "//&
                        "too high values [lbfgs_compute]**"
        ctx_err%critic=.true.
      end if
      !! possibility for non-shared error
      call distribute_error(ctx_err,ctx_paral%communicator) 
    end if
    !! -----------------------------------------------------------------    
    
    ncells_gb=ctx_domain%n_cell_gb
    !! allocate working arrays, including previous iterations stuff
    allocate(model_cur(ncells)) !! careful, the "cur" (current)
    allocate(model_old(ncells)) !! is the current loop iteration,
    allocate(grad_cur (ncells)) !! it may not but the iter_loc.
    allocate(grad_old (ncells))
    !! working for l-bfgs
    allocate(s(ncells))
    allocate(r(ncells))
    allocate(q(ncells))
    allocate(y(ncells))
    !! per consistency with the loop but only iter_loc are used
    allocate(alpha_iter(iter_gb)) 
    alpha_iter = 0.0
    
    !! -----------------------------------------------------------------
    !! the integer m defines the number of inner and outer 
    !! loop, it is said to be taken between 3 and 20 in 
    !! [Nocedal and Wright]
    if(iter_loc <= 3)                      m = 1 
    if(iter_loc >  3 .and. iter_loc <= 5 ) m = 2
    if(iter_loc >  5 .and. iter_loc <= 7 ) m = 3
    if(iter_loc >  7 .and. iter_loc <= 10) m = 4
    if(iter_loc > 10 .and. iter_loc <= 12) m = 5
    if(iter_loc > 12 .and. iter_loc <= 18) m = 6
    if(iter_loc > 18 .and. iter_loc <= 24) m = 8
    if(iter_loc > 25)                      m = 10
    !! -----------------------------------------------------------------

    !! initialization of q with the gradient at this iteration
    q(:) = grad_in(:)
    gam  = 1.0
    !! -----------------------------------------------------------------
    !! 1) loop, from iter-1 to iter-m 
    !!
    !! -----------------------------------------------------------------
    do i=iter_gb-1,iter_gb-m,-1
      !! a) load models and gradients ----------------------------------
      if(i==iter_gb-1) then
        grad_cur = grad_in
      else
        call gradient_load_per_cell(                                      &
                           ctx_paral,ctx_domain,trim(adjustl(param_name)),&
                           i+1,ncells_gb,trim(adjustl(workpath_grad)),    &
                           grad_cur,ctx_err)
      endif
      call gradient_load_per_cell(                                      &
                         ctx_paral,ctx_domain,trim(adjustl(param_name)),&
                         i,ncells_gb,trim(adjustl(workpath_grad)),      &
                         grad_old,ctx_err)
      !! 
      call model_load_lbfgs_per_cell(ctx_paral,ctx_domain,              &
                            trim(adjustl(param_name)),i,tag_function,   &
                            ncells_gb,trim(adjustl(workpath_model)),    &
                            model_cur,ctx_err)
      call model_load_lbfgs_per_cell(ctx_paral,ctx_domain,              &
                            trim(adjustl(param_name)),i-1,tag_function, &
                            ncells_gb,trim(adjustl(workpath_model)),    &
                            model_old,ctx_err)
      !! b) formulation ------------------------------------------------
      !! s_i = model_{i+1} - model_i
      !! y_i =  grad_{i+1} -  grad_i
      s = model_cur - model_old
      y =  grad_cur -  grad_old
      !! rho_i = 1/(y_i^T s_i)
      local_coeff = 0.
      do ic=1,ncells
        local_coeff = local_coeff + y(ic)*s(ic)
      enddo
      !! sum contributions from all sub-domains
      call allreduce_sum(local_coeff,coeff,1,ctx_paral%communicator,ctx_err)
      rho = 1.0
      if(abs(coeff) > tol ) rho = 1./coeff

      !! alpha = rho s_i^T q -------------------------------------------
      local_coeff = 0.
      do ic=1,ncells
        local_coeff = local_coeff + rho*s(ic)*q(ic)
      enddo
      !! sum contributions from all sub-domains
      call allreduce_sum(local_coeff,alpha,1,ctx_paral%communicator,ctx_err)
      !! keep this alpha in memory
      alpha_iter(i) = alpha
      
      !! update q = q - alpha*y ----------------------------------------
      q(:) = q(:) - alpha*y(:)
      
      !! c) if i = (iter-1) we take the opportunity to compute gamma ---
      if(i==iter_gb-1 .and. (.not. flag_happrox)) then
        local_num=0.0
        local_den=0.0
        do ic=1,ncells
          local_num = local_num + s(ic)*y(ic)
          local_den = local_den + y(ic)*y(ic)
        enddo
        !! sum contributions from all sub-domains
        call allreduce_sum(local_num,num,1,ctx_paral%communicator,ctx_err)
        call allreduce_sum(local_den,den,1,ctx_paral%communicator,ctx_err)
        gam = 1.0
        if(abs(den) > tol) gam = num / den
      endif
    enddo
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! Intermediate step for r = H0 q
    if(flag_happrox) then
      !! given in input, using a diagonal approximation
      r(:) = q(:) / happrox(:)
    else
      !! H0 is taken from [Eq. (7.20), Nocedal and Wright]: GAM diagonal
      r(:) = gam*q(:) 
    end if
    !! -----------------------------------------------------------------
    
    
    !! -----------------------------------------------------------------
    !! 2) loop, from iter-m to iter-1
    !!
    !! -----------------------------------------------------------------
    do i=iter_gb-m,iter_gb-1
      !! a) load models and gradients ----------------------------------
      if(i==iter_gb-1) then
        grad_cur = grad_in
      else
        call gradient_load_per_cell(                                      &
                           ctx_paral,ctx_domain,trim(adjustl(param_name)),&
                           i+1,ncells_gb,trim(adjustl(workpath_grad)),  &
                           grad_cur,ctx_err)
      endif
      call gradient_load_per_cell(                                      &
                         ctx_paral,ctx_domain,trim(adjustl(param_name)),&
                         i,ncells_gb,trim(adjustl(workpath_grad)),      &
                         grad_old,ctx_err)
      !!
      call model_load_lbfgs_per_cell(ctx_paral,ctx_domain,              &
                            trim(adjustl(param_name)),i,tag_function,   &
                            ncells_gb,trim(adjustl(workpath_model)),    &
                            model_cur,ctx_err)
      call model_load_lbfgs_per_cell(ctx_paral,ctx_domain,              &
                            trim(adjustl(param_name)),i-1,tag_function, &
                            ncells_gb,trim(adjustl(workpath_model)),    &
                            model_old,ctx_err)
      !! b) formulation ------------------------------------------------
      !!s_i = model_{i+1} - model_i
      !!y_i =  grad_{i+1} -  grad_i
      s = model_cur - model_old
      y =  grad_cur -  grad_old
      !! Compute rho_i = 1/(y_i^T s_i)
      local_coeff = 0.
      do ic=1,ncells
        local_coeff = local_coeff + y(ic)*s(ic)
      enddo
      !! sum contributions from all sub-domains
      call allreduce_sum(local_coeff,coeff,1,ctx_paral%communicator,ctx_err)
      rho = 1.0
      if(abs(coeff) > tol ) rho = 1./coeff

      !! beta = rho_i * y_i^T*r
      local_coeff = 0.
      do ic=1,ncells
        local_coeff = local_coeff + rho*y(ic)*r(ic)
      enddo
      !! sum contributions from all sub-domains
      call allreduce_sum(local_coeff,beta,1,ctx_paral%communicator,ctx_err)
      !! r is updated
      do ic=1,ncells
        r(ic) = r(ic) + s(ic)*(alpha_iter(i) - beta)
      enddo
    enddo
    !! -----------------------------------------------------------------
    
    !! the search direction is given by r ------------------------------
    search(:) = real(r(:),kind=4)
    !! clean
    deallocate(model_cur )
    deallocate(model_old )
    deallocate(grad_cur  )
    deallocate(grad_old  )
    deallocate(s         )
    deallocate(r         )
    deallocate(q         )
    deallocate(y         )
    deallocate(alpha_iter)
    !! -----------------------------------------------------------------

    return
  end subroutine lbfgs_compute

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> load given iteration binary model
  !> we need to apply the appropriate function, depending on the 
  !> gradient parametrization (e.g., inverse, squared, etc)
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[in]   ctx_domain       : domain context
  !> @param[in]   tag_function     : function of the model (e.g., inverse)
  !> @param[in]   iter             : iteration to load 
  !> @param[in]   ncell_gb         : global number of cell 
  !> @param[in]   param_name       : name of the parameter gradient
  !> @param[in]   workpath_grad    : folder where to find the binaries
  !> @param[inout] grad             : loaded gradient 
  !> @param[inout] ctx_err          : error context
  !---------------------------------------------------------------------
  subroutine model_load_lbfgs_per_cell(ctx_paral,ctx_domain,param_name, &
                                       iter,tag_function,ncell_gb,      &
                                       workpath_model,model,ctx_err)
    !! -----------------------------------------------------------------
    !! locally needed only
    use m_io_filename,        only : io_filename
    use m_array_load_binary,  only : array_load_binary
    use m_project_array,      only : project_array_global_cell_to_local
    !! -----------------------------------------------------------------
    
    implicit none

    type(t_parallelism)         , intent(in)    :: ctx_paral
    type(t_domain)              , intent(in)    :: ctx_domain
    integer                     , intent(in)    :: tag_function
    integer(kind=IKIND_MESH)    , intent(in)    :: ncell_gb
    integer                     , intent(in)    :: iter
    character(len=*)            , intent(in)    :: param_name
    character(len=*)            , intent(in)    :: workpath_model
    real(kind=4)    ,allocatable, intent(inout) :: model(:)
    type(t_error)               , intent(out)   :: ctx_err
    !! -----------------------------------------------------------------
    real      ,allocatable   :: global_array(:)
    character(len=128)       :: name_gb
    !! only cares for the iteration
    real   (kind=8)                :: freq(2)=0.
    integer(kind=4)                :: mode   =0
    logical                        :: flag_mode = .false.
    logical                        :: flag_freq = .false.
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    allocate(global_array(ncell_gb))
    global_array=0.0
    call io_filename(name_gb,trim(adjustl(param_name)),flag_freq,       &
                     flag_mode,freq,mode,ctx_err,o_iter=iter)
    call array_load_binary(ctx_paral,global_array,ncell_gb,             &
                           trim(adjustl(workpath_model)),               &
                           trim(adjustl(name_gb)),.false.,ctx_err)
    call project_array_global_cell_to_local(ctx_domain,global_array,    &
                                            model,ctx_err)
    !! we apply the appropriate function
    call apply_model_function(ctx_paral,model,tag_function,ctx_err)
    deallocate(global_array)
    return
  end subroutine model_load_lbfgs_per_cell

end module m_lbfgs_compute
