!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_tag_search_dir.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the function of the search direction
!
!------------------------------------------------------------------------------
module m_tag_search_dir

  use m_raise_error,              only : raise_error, t_error

  implicit none

  !>
  integer, parameter :: tag_SEARCH_IGNORE    = 0 !!   not inverted

  integer, parameter :: tag_SEARCH_GRADIENT  = 1 !! gradient-descent
  integer, parameter :: tag_SEARCH_LBFGS     =10 !! l-bfgs
  integer, parameter :: tag_SEARCH_NLCG_FR   =20 !! nlcg Flectcher-Reeves
  integer, parameter :: tag_SEARCH_NLCG_PR   =21 !! nlcg Polak-Ribière
  integer, parameter :: tag_SEARCH_NLCG_HS   =22 !! nlcg H-S
  integer, parameter :: tag_SEARCH_NLCG_DY   =23 !! nlcg Dai-Yuan

  private
  
  public :: search_dir_tag_formalism, search_dir_tag_naming
  public :: tag_SEARCH_GRADIENT, tag_SEARCH_LBFGS, tag_SEARCH_NLCG_DY
  public :: tag_SEARCH_NLCG_FR, tag_SEARCH_NLCG_HS, tag_SEARCH_NLCG_PR

  contains 
    
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> decide the search tag from keyword
  !
  !> @param[in]   n_model       : number of model function
  !> @param[in]   name_func_in  : input name
  !> @param[out]  tag_func_out  : output tag
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine search_dir_tag_formalism(n_model,name_func_in,tag_func_out,ctx_err)
    implicit none

    integer                            ,intent(in)   :: n_model
    character(len=*) ,allocatable      ,intent(in)   :: name_func_in(:)
    integer          ,allocatable      ,intent(inout):: tag_func_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer    :: k
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    do k=1,n_model
      select case(trim(adjustl(name_func_in(k))))
        case('gradient-descent','Gradient-descent','GRADIENT-DESCENT',  &
             'Gradient-Descent','GRADIENT','gradient','Gradient')
          tag_func_out(k) = tag_SEARCH_GRADIENT
        case('l-bfgs','L-bfgs','L-BFGS','lbfgs','LBFGS','Lbfgs')
          tag_func_out(k) = tag_SEARCH_LBFGS
        case('nlcg_FR','Nlcg_FR','NLCG_FR','nlcg-FR','Nlcg-FR','NLCG-FR',&
             'nlcg_fr','Nlcg_fr','NLCG_fr','nlcg-fr','Nlcg-fr','NLCG-fr')
          tag_func_out(k) = tag_SEARCH_NLCG_FR
        case('nlcg_PR','Nlcg_PR','NLCG_PR','nlcg-PR','Nlcg-PR','NLCG-PR',&
             'nlcg_pr','Nlcg_pr','NLCG_pr','nlcg-pr','Nlcg-pr','NLCG-pr')
          tag_func_out(k) = tag_SEARCH_NLCG_PR
        case('nlcg_HS','Nlcg_HS','NLCG_HS','nlcg-HS','Nlcg-HS','NLCG-HS',&
             'nlcg_hs','Nlcg_hs','NLCG_hs','nlcg-hs','Nlcg-hs','NLCG-hs')
          tag_func_out(k) = tag_SEARCH_NLCG_HS
        case('nlcg_DY','Nlcg_DY','NLCG_DY','nlcg-DY','Nlcg-DY','NLCG-DY',&
             'nlcg_dy','Nlcg_dy','NLCG_dy','nlcg-dy','Nlcg-dy','NLCG-dy')
          tag_func_out(k) = tag_SEARCH_NLCG_DY
        case('ignore','IGNORE','Ignore','none','NONE','0','None')
          tag_func_out(k) = tag_SEARCH_IGNORE
        case default
          ctx_err%msg   = "** ERROR: Unrecognized search direction " // &
                          "[search_tag_formalism]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end do
        
    return
    
  end subroutine search_dir_tag_formalism

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> from the tag, we extract a word for printing
  !
  !> @param[in]   tag_in        : input tag
  !> @param[out]  model_name    : current model name
  !> @param[out]  str           : string out
  !> @param[inout] ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine search_dir_tag_naming(tag_in,model_name,str,ctx_err)
    implicit none

    integer                            ,intent(in)   :: tag_in
    character(len=*)                   ,intent(in)   :: model_name
    character(len=128)                 ,intent(inout):: str
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    str = ''
    str = 'update '// trim(adjustl(model_name)) 
    select case(tag_in)
      case(tag_SEARCH_IGNORE)
        str = trim(adjustl(model_name)) // ' not inverted'
      case(tag_SEARCH_GRADIENT)
        str = str(1:20) // ' with gradient descent'
      case(tag_SEARCH_LBFGS)
        str = str(1:20) // ' with l-bfgs'
      case(tag_SEARCH_NLCG_FR)
        str = str(1:20) // ' with NLCG Fletcher-Reeves'
      case(tag_SEARCH_NLCG_PR)
        str = str(1:20) // ' with NLCG Polak-Ribière'
      case(tag_SEARCH_NLCG_HS)
        str = str(1:20) // ' with NLCG Hestenes-Stiefel'
      case(tag_SEARCH_NLCG_DY)
        str = str(1:20) // ' with NLCG Day-Yuan'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized search direction " //&
                        "[search_tag_formalism]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
    
  end subroutine search_dir_tag_naming

end module m_tag_search_dir
