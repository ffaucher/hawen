!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_search_dir.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the search direction input parameters
!
!------------------------------------------------------------------------------
module m_read_parameters_search_dir
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_search_dir
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters for the gradient
  !
  !> @param[in]   parameter_file  : parameter file where infos are
  !> @param[in]   n_model         : number of model to look for 
  !> @param[in]   str_seach       : string to indicate the search method
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_search_dir(parameter_file,n_model,str_search, &
                                        ctx_err)
    implicit none 
    
    character(len=*)             ,intent(in)   :: parameter_file
    integer                      ,intent(in)   :: n_model
    character(len=32),allocatable,intent(inout):: str_search(:)
    type(t_error)                ,intent(out)  :: ctx_err

    character(len=512)   :: str(512)
    character(len=512)   :: strtemp
    integer              :: k,nval
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! there are n_model to be found at most, if less are indicated,
    !! we repeat the last method
    call read_parameter(parameter_file,'fwi_search_direction','',str,nval)
    if(nval < 1) then
      ctx_err%msg   = "** ERROR: missing search direction [search_read_param]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    do k=1,n_model
      if(k<=nval) then
        strtemp = str(k)
      end if  
      str_search(k) = trim(adjustl(strtemp(1:32)))
    end do


    return
  end subroutine read_parameters_search_dir

end module m_read_parameters_search_dir
