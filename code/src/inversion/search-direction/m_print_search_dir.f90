!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_search_dir.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for the gradient
!
!------------------------------------------------------------------------------
module m_print_search_dir

  use m_ctx_parallelism,      only : t_parallelism
  use m_raise_error,          only : t_error, raise_error
  use m_tag_search_dir,       only : search_dir_tag_naming
  use m_print_basics,         only : separator,indicator
  implicit none
  
  private
  public :: print_search_dir_info
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the search direction
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   n_search        : number of models inverted
  !> @param[in]   param_name      : name of all parameters
  !> @param[in]   tag_method      : search direction method
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine print_search_dir_info(ctx_paral,n_search,param_name,       &
                                   tag_method,ctx_err)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: n_search
    character(len=*),allocatable  ,intent(in)   :: param_name(:)
    integer         ,allocatable  ,intent(in)   :: tag_method(:)
    type(t_error)                 ,intent(out)  :: ctx_err
    !!
    character(len=128) :: str
    integer            :: k

    ctx_err%ierr=0

    !! for all model, we indicate the function
    do k=1,n_search
      call search_dir_tag_naming(tag_method(k),param_name(k),str,ctx_err)
      if(ctx_paral%master) write(6,'(a)') "- " // trim(adjustl(str))
    end do
    
    return
  end subroutine print_search_dir_info

end module m_print_search_dir
