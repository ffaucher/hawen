!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_search_dir.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the context for the search direction.
!
!------------------------------------------------------------------------------
module m_ctx_search_dir

  implicit none

  private
  public :: t_search_dir, search_dir_clean, search_dir_reset

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Type t_sdir_param_subdof contains info on the search
  !> direction for a parameter represented with several 
  !> constant per cell. 
  !
  !> utils:
  !> @param[integer]     order   : order
  !> @param[integer]     ndof    : ndof
  !---------------------------------------------------------------------
  type t_sdir_param_subdof
  
    !! for now we assume constant order on all cells
    integer                              :: order
    !! ndof  per cells
    integer                              :: ndof

  end type t_sdir_param_subdof
  
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_search_dir contains all information for the search direction
  !
  !> @param[integer] n_search     : number of models in the search dir
  !> @param[integer] n_coeff      : number of coefficients per search dir
  !> @param[integer] method       : method TAG for the search direction
  !> @param[real]    search       : search direction
  !> @param[real]    flag_pseudo_hessian_scale: pseudo hessian scaling
  !
  !---------------------------------------------------------------------
  type t_search_dir

    !! number of parameters for the search direction, and number of coeff
    integer                        :: n_search
    integer                        :: n_coeff_loc
    integer                        :: n_coeff_gb

    !! list of search direction model name
    character(len=32), allocatable :: param_name(:)
    !! list of method name, possible one per search direction
    integer,           allocatable :: method(:)

    !! search direction (no need for double), size(n_coeff,n_grad)
    real(kind=4)     , allocatable :: search(:,:)

    !! this flag indicates if search directions should
    !! be saved in paraview format. Otherwize, we only save a binary 
    logical                        :: flag_debug_save
    
    !! for multiple constant per cell parametrization
    type(t_sdir_param_subdof)      :: param_dof
  end type t_search_dir
  !---------------------------------------------------------------------
  
  
  contains  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> reset search_dir information to 0
  !
  !> @param[inout] ctx_search  : search_dir context
  !---------------------------------------------------------------------
  subroutine search_dir_reset(ctx_search)
    type(t_search_dir)    ,intent(inout)  :: ctx_search

    ctx_search%search = 0.0

    return
  end subroutine search_dir_reset

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> clean search dir information and allocation
  !
  !> @param[inout] ctx_search  : search context
  !---------------------------------------------------------------------
  subroutine search_dir_clean(ctx_search)
    type(t_search_dir)    ,intent(inout)  :: ctx_search

    ctx_search%n_search        =0
    ctx_search%n_coeff_loc     =0
    ctx_search%n_coeff_gb      =0
    ctx_search%param_dof%order =0
    ctx_search%param_dof%ndof  =0
    ctx_search%flag_debug_save =.false.
    if(allocated(ctx_search%param_name)) deallocate(ctx_search%param_name)
    if(allocated(ctx_search%method))     deallocate(ctx_search%method)
    if(allocated(ctx_search%search))     deallocate(ctx_search%search)

    return
  end subroutine search_dir_clean

end module m_ctx_search_dir
