!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_search_dir_load_bin.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to load the binary search-directions
!
!------------------------------------------------------------------------------
module m_search_dir_load_bin
  
  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_ctx_domain,               only : t_domain
  use m_io_filename,              only : io_filename
  use m_array_load_binary,        only : array_load_binary
  use m_project_array,            only : project_array_global_cell_to_local, &
                                         project_array_global_subcell_to_local
  implicit none

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads the search direction of the selected iteration
  !---------------------------------------------------------------------
  interface search_dir_load_per_cell
     module procedure search_dir_load_Nint4
     module procedure search_dir_load_Nint8
  end interface search_dir_load_per_cell
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads the search direction of the selected iteration using multiple
  !> constant per cells
  !---------------------------------------------------------------------
  interface search_dir_load_per_subcell
     module procedure search_dir_load_subcell_int4
     module procedure search_dir_load_subcell_int8
  end interface search_dir_load_per_subcell
  !---------------------------------------------------------------------
  
  private  
  public :: search_dir_load_per_cell
  public :: search_dir_load_per_subcell
    
  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> load given iteration binary gradient
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[in]   ctx_paral        : domain context
  !> @param[in]   iter             : iteration to load 
  !> @param[in]   ncell_gb         : global number of cell 
  !> @param[in]   param_name       : name of the parameter search direction
  !> @param[in]   workpath_search  : folder where to find the binaries
  !> @param[inout] search           : loaded search direction
  !> @param[inout] ctx_err          : error context
  !---------------------------------------------------------------------
  subroutine search_dir_load_Nint4(ctx_paral,ctx_domain,param_name,iter,&
                                   ncell_gb,workpath_search,search,ctx_err)
  
    implicit none

    type(t_parallelism)         , intent(in)    :: ctx_paral
    type(t_domain)              , intent(in)    :: ctx_domain
    integer                     , intent(in)    :: iter,ncell_gb
    character(len=*)            , intent(in)    :: param_name
    character(len=*)            , intent(in)    :: workpath_search
    real(kind=4)    ,allocatable, intent(inout) :: search(:)
    type(t_error)               , intent(out)   :: ctx_err
    !! -----------------------------------------------------------------
    real      ,allocatable   :: global_array(:)
    character(len=128)       :: name_loc,name_gb
    !! the search direction only cares for the iteration
    real   (kind=8)                :: freq(2)=0.
    integer(kind=4)                :: mode   =0
    logical                        :: flag_mode = .false.
    logical                        :: flag_freq = .false.
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    allocate(global_array(ncell_gb))
    global_array= 0.0
    name_loc    = "search-direction-"//trim(adjustl(param_name))
    call io_filename(name_gb,trim(adjustl(name_loc)),flag_freq,         &
                     flag_mode,freq,mode,ctx_err,o_iter=iter)
    call array_load_binary(ctx_paral,global_array,ncell_gb,             &
                           trim(adjustl(workpath_search)),              &
                           trim(adjustl(name_gb)),.false.,ctx_err)
    call project_array_global_cell_to_local(ctx_domain,global_array,    &
                                            search,ctx_err)
    deallocate(global_array)
    return
  end subroutine search_dir_load_Nint4

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> load given iteration binary gradient
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[in]   ctx_paral        : domain context
  !> @param[in]   iter             : iteration to load 
  !> @param[in]   ncell_gb         : global number of cell 
  !> @param[in]   param_name       : name of the parameter search direction
  !> @param[in]   workpath_search  : folder where to find the binaries
  !> @param[inout] search           : loaded search direction
  !> @param[inout] ctx_err          : error context
  !---------------------------------------------------------------------
  subroutine search_dir_load_Nint8(ctx_paral,ctx_domain,param_name,iter,&
                                   ncell_gb,workpath_search,search,ctx_err)
  
    implicit none

    type(t_parallelism)         , intent(in)    :: ctx_paral
    type(t_domain)              , intent(in)    :: ctx_domain
    integer(kind=8)             , intent(in)    :: ncell_gb
    integer                     , intent(in)    :: iter
    character(len=*)            , intent(in)    :: param_name
    character(len=*)            , intent(in)    :: workpath_search
    real(kind=4)    ,allocatable, intent(inout) :: search(:)
    type(t_error)               , intent(out)   :: ctx_err
    !! -----------------------------------------------------------------
    real      ,allocatable   :: global_array(:)
    character(len=128)       :: name_loc,name_gb
    !! the search direction only cares for the iteration
    real   (kind=8)                :: freq(2)=0.
    integer(kind=4)                :: mode   =0
    logical                        :: flag_mode = .false.
    logical                        :: flag_freq = .false.
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    allocate(global_array(ncell_gb))
    global_array= 0.0
    name_loc    = "search-direction-"//trim(adjustl(param_name))
    call io_filename(name_gb,trim(adjustl(name_loc)),flag_freq,         &
                     flag_mode,freq,mode,ctx_err,o_iter=iter)
    call array_load_binary(ctx_paral,global_array,ncell_gb,             &
                           trim(adjustl(workpath_search)),              &
                           trim(adjustl(name_gb)),.false.,ctx_err)
    call project_array_global_cell_to_local(ctx_domain,global_array,    &
                                            search,ctx_err)
    deallocate(global_array)
    return
  end subroutine search_dir_load_Nint8

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> load given iteration binary gradient
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[in]   ctx_paral        : domain context
  !> @param[in]   iter             : iteration to load 
  !> @param[in]   ncell_gb         : global number of cell 
  !> @param[in]   param_name       : name of the parameter search direction
  !> @param[in]   workpath_search  : folder where to find the binaries
  !> @param[inout] search           : loaded search direction
  !> @param[inout] ctx_err          : error context
  !---------------------------------------------------------------------
  subroutine search_dir_load_subcell_int4(ctx_paral,ctx_domain,         &
                                          param_name,iter,ncell_gb,     &
                                          npt_per_cell,workpath_search, &
                                          search,ctx_err)
  
    implicit none

    type(t_parallelism)         , intent(in)    :: ctx_paral
    type(t_domain)              , intent(in)    :: ctx_domain
    integer                     , intent(in)    :: iter,ncell_gb
    integer                     , intent(in)    :: npt_per_cell
    character(len=*)            , intent(in)    :: param_name
    character(len=*)            , intent(in)    :: workpath_search
    real(kind=4)    ,allocatable, intent(inout) :: search(:)
    type(t_error)               , intent(out)   :: ctx_err
    !! -----------------------------------------------------------------
    real      ,allocatable   :: global_array(:)
    character(len=128)       :: name_loc,name_gb
    !! the search direction only cares for the iteration
    real   (kind=8)                :: freq(2)=0.
    integer(kind=4)                :: mode   =0
    integer                        :: ncoeff_gb
    logical                        :: flag_mode = .false.
    logical                        :: flag_freq = .false.
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    ncoeff_gb = ncell_gb * npt_per_cell

    allocate(global_array(ncoeff_gb))
    
    global_array= 0.0
    name_loc    = "search-direction-"//trim(adjustl(param_name))
    call io_filename(name_gb,trim(adjustl(name_loc)),flag_freq,         &
                     flag_mode,freq,mode,ctx_err,o_iter=iter)
    call array_load_binary(ctx_paral,global_array,ncoeff_gb,            &
                           trim(adjustl(workpath_search)),              &
                           trim(adjustl(name_gb)),.false.,ctx_err)
    
    call project_array_global_subcell_to_local(ctx_domain,global_array, &
                                               search,npt_per_cell,ctx_err)
    
    deallocate(global_array)
    return
  end subroutine search_dir_load_subcell_int4

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> load given iteration binary gradient
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[in]   ctx_paral        : domain context
  !> @param[in]   iter             : iteration to load 
  !> @param[in]   ncell_gb         : global number of cell 
  !> @param[in]   param_name       : name of the parameter search direction
  !> @param[in]   workpath_search  : folder where to find the binaries
  !> @param[inout] search           : loaded search direction
  !> @param[inout] ctx_err          : error context
  !---------------------------------------------------------------------
  subroutine search_dir_load_subcell_int8(ctx_paral,ctx_domain,         &
                                          param_name,iter,ncell_gb,     &
                                          npt_per_cell,workpath_search, &
                                          search,ctx_err)
  
    implicit none

    type(t_parallelism)         , intent(in)    :: ctx_paral
    type(t_domain)              , intent(in)    :: ctx_domain
    integer                     , intent(in)    :: iter,ncell_gb
    integer                     , intent(in)    :: npt_per_cell
    character(len=*)            , intent(in)    :: param_name
    character(len=*)            , intent(in)    :: workpath_search
    real(kind=8)    ,allocatable, intent(inout) :: search(:)
    type(t_error)               , intent(out)   :: ctx_err
    !! -----------------------------------------------------------------
    real   (kind=8) ,allocatable   :: global_array(:)
    character(len=128)       :: name_loc,name_gb
    !! the search direction only cares for the iteration
    real   (kind=8)                :: freq(2)=0.
    integer(kind=4)                :: mode   =0
    integer                        :: ncoeff_gb
    logical                        :: flag_mode = .false.
    logical                        :: flag_freq = .false.
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    ncoeff_gb = ncell_gb * npt_per_cell

    allocate(global_array(ncoeff_gb))
    
    global_array= 0.0
    name_loc    = "search-direction-"//trim(adjustl(param_name))
    call io_filename(name_gb,trim(adjustl(name_loc)),flag_freq,         &
                     flag_mode,freq,mode,ctx_err,o_iter=iter)
    call array_load_binary(ctx_paral,global_array,ncoeff_gb,            &
                           trim(adjustl(workpath_search)),              &
                           trim(adjustl(name_gb)),.false.,ctx_err)
    
    call project_array_global_subcell_to_local(ctx_domain,global_array, &
                                               search,npt_per_cell,ctx_err)
    
    deallocate(global_array)
    return
  end subroutine search_dir_load_subcell_int8
  
end module m_search_dir_load_bin
