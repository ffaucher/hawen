!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_search_dir_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize the search direction information
!
!------------------------------------------------------------------------------
module m_search_dir_init
  
  use m_raise_error,                only : raise_error, t_error
  use m_ctx_parallelism,            only : t_parallelism
  use m_read_parameters_search_dir, only : read_parameters_search_dir
  use m_ctx_gradient,               only : t_gradient
  use m_ctx_search_dir,             only : t_search_dir
  use m_print_search_dir,           only : print_search_dir_info
  use m_tag_search_dir,             only : search_dir_tag_formalism,    &
                                           tag_SEARCH_GRADIENT,         &
                                           tag_SEARCH_NLCG_FR,          &
                                           tag_SEARCH_NLCG_PR,          &
                                           tag_SEARCH_NLCG_HS,          &
                                           tag_SEARCH_NLCG_DY,          &
                                           tag_SEARCH_LBFGS

  implicit none

  private  
  public :: search_dir_init
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init search direction context information
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[inout] ctx_search       : search dir context
  !> @param[in]   ctx_grad         : gradient context
  !> @param[inout] parameter_file   : parameter file
  !> @param[out]  flag_save_search : indicates if method needs previous search-dir
  !> @param[out]  flag_save_grad   : indicates if method needs previous gradient
  !> @param[out]  flag_save_model  : indicates if method needs previous model
  !> @param[inout] ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine search_dir_init(ctx_paral,ctx_search,ctx_grad,             &
                             parameter_file,flag_save_search,           &
                             flag_save_grad,flag_save_model,ctx_err)
  
    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_search_dir) ,intent(inout):: ctx_search
    type(t_gradient)   ,intent(in)   :: ctx_grad
    character(len=*)   ,intent(in)   :: parameter_file
    logical            ,intent(out)  :: flag_save_search
    logical            ,intent(out)  :: flag_save_grad
    logical            ,intent(out)  :: flag_save_model
    type(t_error)      ,intent(out)  :: ctx_err
    !! -----------------------------------------------------------------
    character(len=32)  ,allocatable:: str(:)
    integer                        :: i_search
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0

    !! get some infos from gradient
    ctx_search%n_coeff_loc     = ctx_grad%n_coeff_loc
    ctx_search%n_coeff_gb      = ctx_grad%n_coeff_gb 
    ctx_search%n_search        = ctx_grad%n_grad
    ctx_search%flag_debug_save = ctx_grad%flag_debug_save
    !! in case of basis function representation per cell
    ctx_search%param_dof%ndof  = ctx_grad%param_dof%ndof
    ctx_search%param_dof%order = ctx_grad%param_dof%order
    
    allocate(ctx_search%search(ctx_search%n_coeff_loc,ctx_search%n_search))
    allocate(ctx_search%param_name(ctx_search%n_search))
    allocate(ctx_search%method(ctx_search%n_search))
    ctx_search%search = 0.0    
    ctx_search%method = 0    
    ctx_search%param_name(1:ctx_search%n_search) = &
                              ctx_grad%param_name(1:ctx_search%n_search)

    !! read method(s)
    allocate(str(ctx_search%n_search))
    call read_parameters_search_dir(parameter_file,ctx_search%n_search,  &
                                    str,ctx_err)

    !! we convert the input names in TAG, depending on the method, we 
    !! may be required to save the previous gradient, model, and search
    call search_dir_tag_formalism(ctx_search%n_search,str,              &
                                  ctx_search%method,ctx_err)
    deallocate(str)
    
    flag_save_search = .false.
    flag_save_grad   = .false.
    flag_save_model  = .false.
    do i_search=1,ctx_search%n_search
      select case(ctx_search%method(i_search))
        case(tag_SEARCH_LBFGS   )
          flag_save_grad   = .true.
          flag_save_model  = .true.
        case(tag_SEARCH_NLCG_FR,tag_SEARCH_NLCG_PR,                     &
             tag_SEARCH_NLCG_HS,tag_SEARCH_NLCG_DY)
          flag_save_search = .true.
          flag_save_grad   = .true.
        case default
      end select
    end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! the pseudo-hessian cannot be employed with some method maybe ...
    !! -----------------------------------------------------------------
    !if(all(ctx_search%method .eq. tag_SEARCH_LBFGS) .and.               &
    !       ctx_grad%flag_pseudo_hessian_scale) then
    !  ctx_err%msg   = "** ERROR: the use of pseudo-hessian is not "  // &
    !                  "compatible with Limited-BFGS [search_init]**"
    !  ctx_err%ierr  = -1
    !  ctx_err%critic=.true.
    !  call raise_error(ctx_err)
    !end if

    !! print infos here
    call print_search_dir_info(ctx_paral,ctx_search%n_search,            &
                               ctx_search%param_name,ctx_search%method,  &
                               ctx_err)

    return
  end subroutine search_dir_init

end module m_search_dir_init
