!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_search_dir_save.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save the search directions
!
!------------------------------------------------------------------------------
module m_search_dir_save
  
  use m_raise_error,              only : raise_error, t_error
  use m_define_precision,         only : IKIND_MESH
  use m_reduce_sum,               only : reduce_sum
  use m_ctx_parallelism,          only : t_parallelism
  use m_ctx_discretization,       only : t_discretization
  use m_ctx_domain,               only : t_domain
  use m_ctx_search_dir,           only : t_search_dir
  use m_ctx_io,                   only : t_io
  use m_array_save,               only : array_save
  use m_io_filename,              only : io_filename
  use m_project_array,            only : project_array_local_cell_to_global
  use m_array_save_binary,        only : array_save_binary
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,            &
                                        tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_DOF
  implicit none

  private  
  public :: search_dir_save
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> save the search direction
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   ctx_disc      : discretization context
  !> @param[in]   ctx_domain    : domain context
  !> @param[in]   ctx_search    : search-direction context
  !> @param[in]   ctx_io        : io context
  !> @param[in]   tag_param     : model parametrization
  !> @param[in]   iter          : current iteration number
  !> @param[inout] ctx_err      : error context
  !> @param[in]   o_flag_scaled : optional flag in case of scale version
  !----------------------------------------------------------------------------
  subroutine search_dir_save(ctx_paral,ctx_disc,ctx_domain,ctx_search,  &
                             ctx_io,tag_param,iter,ctx_err,o_flag_scaled)
  
    implicit none
    
    type(t_parallelism)     ,intent(in)   :: ctx_paral
    type(t_discretization)  ,intent(in)   :: ctx_disc
    type(t_domain)          ,intent(in)   :: ctx_domain
    type(t_search_dir)      ,intent(in)   :: ctx_search
    type(t_io)              ,intent(in)   :: ctx_io
    character(len=*)        ,intent(in)   :: tag_param
    integer                 ,intent(in)   :: iter
    type(t_error)           ,intent(out)  :: ctx_err
    logical, optional       ,intent(in)   :: o_flag_scaled
    !! -----------------------------------------------------------------
    !! local
    character(len=128),allocatable :: name_loc(:)
    character(len=128),allocatable :: arrayname(:)
    character(len=128)             :: arrayname_gb,basename
    integer                        :: i_search
    real(kind=4)      ,allocatable :: search_loc_gb(:),search_gb(:)
    logical                        :: flag_scaled
    !! the search direction only cares for the iteration
    real   (kind=8)                :: freq(2)=0.
    integer(kind=4)                :: mode   =0
    logical                        :: flag_mode = .false.
    logical                        :: flag_freq = .false.
    !! for dof representation
    integer(kind=IKIND_MESH)       :: i_cell, igb1, igb2, iloc1, iloc2
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0

    !! basename depends if scaled version or not
    basename    = "search-direction"
    flag_scaled = .false.
    if(present(o_flag_scaled)) then
      flag_scaled = o_flag_scaled
      if(flag_scaled) basename = "search-direction-scaled"
    end if
    
    allocate(name_loc(ctx_search%n_search))
    do i_search = 1, ctx_search%n_search
      name_loc(i_search) = trim(adjustl(basename))// "-" //             &
                           trim(adjustl(ctx_search%param_name(i_search)))
    end do 
    allocate(arrayname(ctx_search%n_search))
    arrayname=''
    call io_filename(arrayname,arrayname_gb,name_loc,                   &
                     trim(adjustl(basename)),ctx_search%n_search,       &
                     flag_freq,flag_mode,freq,mode,ctx_err,o_iter=iter)

    !! in case of debug mode, we save the searchient at every iteration
    !! in a visualy acceptable way (e.g., paraview, sep)
    if(ctx_search%flag_debug_save) then     
      select case(trim(adjustl(tag_param)))
        case(tag_MODEL_PCONSTANT)
          call array_save(ctx_paral,ctx_domain,ctx_disc,                    &
                          ctx_io%save_unstructured,ctx_io%save_structured,  &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          ctx_search%search,ctx_search%n_search,'cell',     &
                          ctx_io%workpath_search,arrayname,arrayname_gb,ctx_err)

        case(tag_MODEL_DOF)
          call array_save(ctx_paral,ctx_domain,ctx_disc,                    &
                          ctx_io%save_unstructured,ctx_io%save_structured,  &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          ctx_search%search,ctx_search%n_search,            &
                          'dof-orderfix' ,                                  &
                          ctx_io%workpath_search,arrayname,arrayname_gb,    &
                          ctx_err,o_cste_order=ctx_search%param_dof%order)

        !! default error.
        case default
          ctx_err%msg   = "** ERROR: we currently cannot save the " //    &
                          "search direction for "//                       &
                          trim(adjustl(tag_param))//                      &
                          " model representation [search_dir_save] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

    end if

    !! -----------------------------------------------------------------
    !! save the binary file with the searchient values, that
    !! can be re-used for the search direction (i.e. nlcg)
    !! no need for the scaled version 
    !! -----------------------------------------------------------------
    if(ctx_io%flag_bin_search .and. (.not. flag_scaled)) then
      select case(trim(adjustl(tag_param)))
        case(tag_MODEL_PCONSTANT)
          !! global size allocation, only master sums up
          allocate(search_loc_gb(ctx_domain%n_cell_gb))
          if(ctx_paral%master) then
            allocate(search_gb(ctx_search%n_coeff_gb))
          else
            allocate(search_gb(1))
          end if
          
          do i_search = 1, ctx_search%n_search
            search_loc_gb = 0.0 ; search_gb=0.0
            call project_array_local_cell_to_global(ctx_domain,         &
                        ctx_search%search(:,i_search),search_loc_gb,ctx_err)
            !! master saves in binary now
            call reduce_sum(search_loc_gb,search_gb,                    &
                            ctx_search%n_coeff_gb,0,                    &
                            ctx_paral%communicator,ctx_err)
            call array_save_binary(ctx_paral,search_gb,                 &
                                   ctx_search%n_coeff_gb,               &
                                   ctx_io%workpath_search,              &
                                   arrayname(i_search),ctx_err)
          end do

        !! model dof ===================================================        
        case(tag_MODEL_DOF)
          
          !! global size allocation, only master sums up
          allocate(search_loc_gb(ctx_search%n_coeff_gb))
          if(ctx_paral%master) then
            allocate(search_gb(ctx_search%n_coeff_gb))
          else
            allocate(search_gb(1))
          end if

          !! fill the appropriate positions
          do i_search = 1, ctx_search%n_search
            search_loc_gb = 0.0 ; search_gb=0.0
            do i_cell = 1, ctx_domain%n_cell_loc
              igb1 = (ctx_domain%index_loctoglob_cell(i_cell)-1)        &
                    * ctx_search%param_dof%ndof + 1
              igb2 = igb1 + ctx_search%param_dof%ndof - 1
              iloc1= (i_cell-1) * ctx_search%param_dof%ndof + 1
              iloc2= iloc1 + ctx_search%param_dof%ndof - 1
              search_loc_gb(igb1:igb2) = ctx_search%search(iloc1:iloc2,i_search)
            end do

            !! master saves in binary now
            call reduce_sum(search_loc_gb,search_gb,                    &
                            ctx_search%n_coeff_gb,0,                    &
                            ctx_paral%communicator,ctx_err)
            call array_save_binary(ctx_paral,search_gb,                 &
                                   ctx_search%n_coeff_gb,               &
                                   ctx_io%workpath_search,              &
                                   arrayname(i_search),ctx_err)
          end do
         
        !! default error ===============================================
        case default
          ctx_err%msg   = "** ERROR: we currently cannot save the " //    &
                          "search direction for "//                       &
                          trim(adjustl(tag_param))//                      &
                          " model representation [search_dir_save] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine search_dir_save

end module m_search_dir_save
