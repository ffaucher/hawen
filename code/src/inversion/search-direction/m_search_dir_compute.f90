!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_search_dir_compute.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the selected search direction from 
!> the gradient of the cost function
!
!------------------------------------------------------------------------------
module m_search_dir_compute

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_distribute_error,       only: distribute_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_allreduce_min,          only: allreduce_min
  use m_allreduce_max,          only: allreduce_max
  use m_ctx_domain,             only: t_domain
  use m_ctx_model,              only: t_model
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_io,                 only: t_io
  use m_ctx_gradient,           only: t_gradient
  use m_ctx_search_dir,         only: t_search_dir
  use m_gradient_load_bin,      only: gradient_load_per_cell,           &
                                      gradient_load_per_subcell
  use m_search_dir_load_bin,    only: search_dir_load_per_cell,         &
                                      search_dir_load_per_subcell
  use m_tag_search_dir,         only: tag_SEARCH_GRADIENT, tag_SEARCH_LBFGS, &
                                      tag_SEARCH_NLCG_DY, tag_SEARCH_NLCG_FR,&
                                      tag_SEARCH_NLCG_HS, tag_SEARCH_NLCG_PR
  use m_nlcg_compute,           only: nlcg_compute
  use m_lbfgs_compute,          only: lbfgs_compute
  use m_ctx_model_representation,                                       &
                                only: tag_MODEL_PPOLY, tag_MODEL_SEP,   &
                                      tag_MODEL_PCONSTANT, tag_MODEL_DOF
  implicit none

  private

  public   :: search_dir_compute, search_dir_scale

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> compute the search direction from the gradient computed, which is
  !> given in input
  !
  !> @param[in]   ctx_paral     : ctx_paral
  !> @param[in]   ctx_domain    : domain context
  !> @param[in]   ctx_io        : io context to load old iterations
  !> @param[in]   ctx_grad      : gradient context
  !> @param[inout] ctx_search    : search direction context
  !> @param[in]   iter_loc      : global iteration
  !> @param[in]   iter_gb       : local iteration
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine search_dir_compute(ctx_paral,ctx_domain,ctx_io,ctx_grad,   &
                                ctx_search,iter_gb,iter_loc,ctx_err)

    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    type(t_domain)                ,intent(in)   :: ctx_domain
    type(t_io)                    ,intent(in)   :: ctx_io
    type(t_gradient)              ,intent(in)   :: ctx_grad
    type(t_search_dir)            ,intent(inout):: ctx_search
    integer                       ,intent(in)   :: iter_gb,iter_loc
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    real(kind=4),allocatable     :: search_loc(:),grad_loc(:)
    real(kind=4),allocatable     :: grad_old(:),search_old(:)
    integer                      :: i_search
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! loop over all parameters to invert 
    do i_search = 1, ctx_search%n_search
     
      ! ----------------------------------------------------------------
      !! 1) check what is the search direction formula to employ.
      ! ----------------------------------------------------------------
      select case(ctx_search%method(i_search))
        !! gradient descent >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        case(tag_SEARCH_GRADIENT)
          ctx_search%search(:,i_search) = ctx_grad%grad(:,i_search)
        
        !! limited-bfgs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        case(tag_SEARCH_LBFGS   )
          !! =========================================================
          select case(trim(adjustl(ctx_grad%model_rep)))
            !! piecewise-constant -------------------------------------
            case(tag_MODEL_PCONSTANT)
              !! if iteration if lower than 2 then we keep the gradient
              if(iter_loc < 2) then 
                ctx_search%search(:,i_search) = ctx_grad%grad(:,i_search)
              else
                allocate(search_loc  (ctx_grad%n_coeff_loc))
                allocate(grad_loc    (ctx_grad%n_coeff_loc))
                grad_loc   = ctx_grad%grad(:,i_search)
                search_loc = 0.0
      
                  call lbfgs_compute(ctx_paral,ctx_domain,grad_loc,search_loc,&
                              trim(adjustl(ctx_search%param_name(i_search))), &
                                     ctx_grad%param_func(i_search),iter_gb,   &
                                     iter_loc,ctx_domain%n_cell_loc,          &
                                     trim(adjustl(ctx_io%workpath_grad)),     &
                                     trim(adjustl(ctx_io%workpath_model)),    &
                                     ctx_grad%pseudoh(:,i_search),            &
                                     ctx_grad%flag_pseudo_hessian_scale,      &
                                     ctx_err)
                ctx_search%search(:,i_search) = search_loc(:)
                deallocate(grad_loc)
                deallocate(search_loc)
              end if

            !! default error ===========================================
            case default
              !! 
              ctx_err%msg   = "** ERROR: Unrecognized gradient model " // &
                              "representation for l-bfgs "             // &
                              "[search_dir_compute]**"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              call raise_error(ctx_err)
          end select
          !! ===========================================================
            
        !! nlcg >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        case(tag_SEARCH_NLCG_FR,tag_SEARCH_NLCG_PR,                     &
             tag_SEARCH_NLCG_HS,tag_SEARCH_NLCG_DY)
          
            !! if iteration if lower than 2 then we keep the gradient
            if(iter_loc < 2) then 
              ctx_search%search(:,i_search) = ctx_grad%grad(:,i_search)
            else
              !! ---------------------------------------------------------
              !! we must use domain dimension because we disregard 
              !! parametrization at this point.
              !! ---------------------------------------------------------
              allocate(search_loc  (ctx_grad%n_coeff_loc))
              allocate(grad_loc    (ctx_grad%n_coeff_loc))
              allocate(grad_old    (ctx_grad%n_coeff_loc))
              allocate(search_old  (ctx_grad%n_coeff_loc))
              grad_loc    = ctx_grad%grad(:,i_search)
              search_loc  = 0.
              grad_old    = 0.
              !! ---------------------------------------------------------
              !! load the gradient and search at previous iteration
              !! ---------------------------------------------------------
              grad_old = 0.0   ; search_old = 0.0
 
              !! =======================================================
              select case(trim(adjustl(ctx_grad%model_rep)))
                !! piecewise-constant -------------------------------------
                case(tag_MODEL_PCONSTANT)

                  call gradient_load_per_cell(ctx_paral,ctx_domain,           &
                              trim(adjustl(ctx_search%param_name(i_search))), &
                                     iter_gb-1,ctx_grad%n_coeff_gb,           &
                                     trim(adjustl(ctx_io%workpath_grad)),     &
                                     grad_old,ctx_err)
                  call search_dir_load_per_cell(ctx_paral,ctx_domain,         &
                              trim(adjustl(ctx_search%param_name(i_search))), &
                                     iter_gb-1,ctx_search%n_coeff_gb,         &
                                     trim(adjustl(ctx_io%workpath_search)),   &
                                     search_old,ctx_err)

                case(tag_MODEL_DOF)

                  call gradient_load_per_subcell(ctx_paral,ctx_domain,        &
                              trim(adjustl(ctx_search%param_name(i_search))), &
                                     iter_gb-1,ctx_domain%n_cell_gb,          &
                                     ctx_grad%param_dof%ndof,                 &
                                     trim(adjustl(ctx_io%workpath_grad)),     &
                                     grad_old,ctx_err)
                  call search_dir_load_per_subcell(ctx_paral,ctx_domain,      &
                              trim(adjustl(ctx_search%param_name(i_search))), &
                                     iter_gb-1,ctx_domain%n_cell_gb,          &
                                     ctx_grad%param_dof%ndof,                 &
                                     trim(adjustl(ctx_io%workpath_search)),   &
                                     search_old,ctx_err)


                !! =========================================================
                !! default error ===========================================
                case default
                  !! 
                  ctx_err%msg   = "** ERROR: Unrecognized gradient model "//&
                                  "representation for nlcg inversion "    //&
                                  "[search_dir_compute]**"
                  ctx_err%ierr  = -1
                  ctx_err%critic=.true.
                  call raise_error(ctx_err)
              end select
              !! =========================================================

              !! ---------------------------------------------------------
              !! compute appropriate formulation
              !! ---------------------------------------------------------
              call nlcg_compute(ctx_paral,ctx_search%method(i_search),    &
                                grad_loc,grad_old,search_loc,search_old,  &
                                ctx_grad%n_coeff_loc,ctx_err)
              ctx_search%search(:,i_search) = search_loc(:)
              deallocate(grad_old)
              deallocate(grad_loc)
              deallocate(search_loc)
              deallocate(search_old)
            end if
            ! --------------------------------------------------------


        !! Unrecognized method >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        case default
          !! Remark that tag_SEARCH_IGNORE should not appear here.
          ctx_err%msg   = "** ERROR: Unrecognized search direction " // &
                          "[search_dir_compute]**"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select
    end do
    !! -----------------------------------------------------------------

    return
  end subroutine search_dir_compute

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Scale the search direction to 1., possibly using a pseudo-hessian
  !> scaling in addition. This normalization makes it more consistant
  !> for the update model, where the search direction is always of the 
  !> same magnitude now.
  !
  !> @param[in]   ctx_paral     : ctx_paral
  !> @param[in]   ctx_grad      : gradient context
  !> @param[inout] ctx_search    : search direction context
  !> @param[inout] ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine search_dir_scale(ctx_paral,ctx_grad,ctx_search,ctx_err)
    implicit none

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    type(t_gradient)              ,intent(in)   :: ctx_grad
    type(t_search_dir)            ,intent(inout):: ctx_search
    type(t_error)                 ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer                      :: i_search
    real(kind=4)                 :: myinfinity = HUGE(1.0) !! HUGE(1.0d0)
    real(kind=4)                 :: max_loc,max_gb
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! loop over all parameters to invert 
    do i_search = 1, ctx_search%n_search
      ! ----------------------------------------------------------------
      !! 1) check if we use pseudo-hessian scaling
      ! ----------------------------------------------------------------
      !! Not used for l-bfgs, already done in its own loop.
      !! ---------------------------------------------------------------
      if(ctx_grad%flag_pseudo_hessian_scale .and.                       &
         ctx_search%method(i_search) .ne. tag_SEARCH_LBFGS) then 

        !! in case there are some problem with pseudo-hessian scaling
        if(minval(ctx_grad%pseudoh(:,i_search)) < tiny(1.0)) then
          ctx_err%ierr=-21
          ctx_err%msg = "** ERROR: Pseudo-hessian scaling si too " //   &
                          "close to zero [search_dir_scale]**"
          ctx_err%critic=.true.
        end if
        if(maxval(ctx_grad%pseudoh(:,i_search)) > myinfinity) then
          ctx_err%ierr=-20
          ctx_err%msg = "** ERROR: Pseudo-hessian scaling si too " //   &
                          "large [search_dir_scale]**"
          ctx_err%critic=.true.
        end if
        !! possibility for non-shared error
        call distribute_error(ctx_err,ctx_paral%communicator) 
        !! -------------------------------------------------------------

        ctx_search%search(:,i_search) = ctx_search%search(:,i_search) / &
                                        ctx_grad%pseudoh(:,i_search)
      end if
      ! ----------------------------------------------------------------
      !! 2) scale to one
      ! ----------------------------------------------------------------
      max_loc = maxval(abs(ctx_search%search(:,i_search)))
      call allreduce_max(max_loc,max_gb,1,ctx_paral%communicator,ctx_err)
      if(max_gb > tiny(1.0)) then
        ctx_search%search(:,i_search) = ctx_search%search(:,i_search) / max_gb
      else 
        ctx_err%msg   = "** ERROR: The search direction is too close" //&
                        " to zero [search_dir_scale]**"
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if 
    end do 
    !! -----------------------------------------------------------------
    return
  end subroutine search_dir_scale

end module m_search_dir_compute
