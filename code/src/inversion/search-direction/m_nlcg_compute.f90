!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_nlcg.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to perform the non-linear conjugate 
!> gradient depending on the choice of method
!
!------------------------------------------------------------------------------
module m_nlcg_compute
  
  use m_raise_error,                only : raise_error, t_error
  use m_define_precision,           only : IKIND_MESH
  use m_ctx_parallelism,            only : t_parallelism
  use m_allreduce_sum,              only : allreduce_sum
  use m_tag_search_dir,             only : tag_SEARCH_NLCG_FR,          &
                                           tag_SEARCH_NLCG_PR,          &
                                           tag_SEARCH_NLCG_HS,          &
                                           tag_SEARCH_NLCG_DY
  implicit none

  private  
  public :: nlcg_compute
    
  contains

  
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> compute non-linear conjugate gradient, depending on the method, 
  !> we have the choice betweeen: 
  !>  - formula from Polak-Ribière
  !>  - formula from Fletcher-Reeves
  !>  - formula from Hestenes-Stiefel
  !>  - formula from Dai-Yuan
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[in]   nlcg_method      : NLCG formulation
  !> @param[in]   grad             : current gradient iteration
  !> @param[in]   grad_old         : gradient at previous iteration
  !> @param[inout] search          : current search direction (to update)
  !> @param[in]   search_old       : direction at previous iteration
  !> @param[inout] ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine nlcg_compute(ctx_paral,nlcg_method,grad,grad_old,search,   &
                          search_old,nparam,ctx_err)
  
    implicit none

    type(t_parallelism)         , intent(in)    :: ctx_paral
    integer                     , intent(in)    :: nlcg_method
    real            ,allocatable, intent(in)    :: grad      (:)
    real            ,allocatable, intent(in)    :: grad_old  (:)
    real            ,allocatable, intent(inout) :: search    (:)
    real            ,allocatable, intent(in)    :: search_old(:)
    integer(kind=IKIND_MESH)    , intent(in)    :: nparam
    type(t_error)               , intent(out)   :: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)  :: ic
    real   (kind=8)           :: numerator, denominator, num, den, beta
    !! -----------------------------------------------------------------

    ctx_err%ierr = 0

    numerator   = 0.0
    denominator = 0.0
    !! -----------------------------------------------------------------
    !! the coefficient depends on the NLCG formula
    !! -----------------------------------------------------------------
    select case (nlcg_method)
      case (tag_SEARCH_NLCG_FR) !! Fletcher--Reeves
        do ic=1,nparam
          numerator   = numerator   + grad    (ic)*grad    (ic)
          denominator = denominator + grad_old(ic)*grad_old(ic)
        enddo 
      case (tag_SEARCH_NLCG_PR) !! Polak--Ribière
        do ic=1,nparam
          numerator   = numerator   +(-grad(ic))*(-grad(ic) + grad_old(ic))
          denominator = denominator + grad_old(ic)*grad_old(ic)
        enddo 

      case (tag_SEARCH_NLCG_HS) !! Hestenes--Stiefel
        do ic=1,nparam
          numerator   = numerator   + (-grad(ic))*(-grad(ic) + grad_old(ic))
          denominator = denominator + search_old(ic)*(-grad(ic) + grad_old(ic))
        enddo 

      case (tag_SEARCH_NLCG_DY) !! Dai--Yuan
        do ic=1,nparam
          numerator   = numerator   + grad(ic)*grad(ic)
          denominator = denominator + search_old(ic)*(-grad(ic)+grad_old(ic))
        enddo
      case default
        ctx_err%msg   = "** ERROR: Unrecognize non-linear conjugate " //&
                        "gradient formulation [nlcg]**"
        ctx_err%critic=.true.
        call raise_error(ctx_err)      
    end select
    !! -----------------------------------------------------------------

    !! sum contributions from all sub-domains
    call allreduce_sum(numerator  ,num,1,ctx_paral%communicator,ctx_err)
    call allreduce_sum(denominator,den,1,ctx_paral%communicator,ctx_err)
    !! in case we have a 0 denominator
    if(abs(den) > tiny(1.0)) then
      beta = num / den
    else
      beta = 1.0
    endif

    !! get the final search direction from num/den
    search = grad + real(beta * search_old,kind=4)

    return
  end subroutine nlcg_compute

end module m_nlcg_compute
