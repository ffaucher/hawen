!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_source_inv_init.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize the source inversion context
!
!------------------------------------------------------------------------------
module m_source_inv_init
  
  use m_raise_error,              only : raise_error, t_error
  use m_ctx_parallelism,          only : t_parallelism
  use m_ctx_acquisition,          only : t_acquisition, SRC_DIRAC
  use m_ctx_source_inv,           only : t_source_inversion
  use m_read_parameter,           only : read_parameter

  implicit none

  private  
  public :: source_inv_init
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init source inversion context
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   ctx_aq          : acquisition context
  !> @param[inout] ctx_src_inv     : source inversion context
  !> @param[in]   parameter_file  : parameter file
  !> @param[in]   n_freq_in_group : number of parallel frequency and modes
  !> @param[inout] ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine source_inv_init(ctx_paral,ctx_aq,ctx_src_inv,              &
                             parameter_file,n_freq_in_group,ctx_err)
  
    implicit none

    type(t_parallelism)      ,intent(in)   :: ctx_paral
    type(t_acquisition)      ,intent(in)   :: ctx_aq
    type(t_source_inversion) ,intent(inout):: ctx_src_inv
    character(len=*)         ,intent(in)   :: parameter_file
    integer                  ,intent(in)   :: n_freq_in_group
    type(t_error)            ,intent(out)  :: ctx_err
    !!
    integer         :: nval
    logical         :: buffer_logical(512)
    
    ctx_err%ierr = 0

    !! -----------------------------------------------------------------
    !! 0) do we invert the source value or not
    call read_parameter(parameter_file,'fwi_source_inversion',.false.,  &
                        buffer_logical,nval)
    ctx_src_inv%flag_source_inv = buffer_logical(1)
    !! in any case we allocate the source value because we will save it
    ctx_src_inv%n_source = ctx_aq%nsrc_loc
    ctx_src_inv%n_freq   = n_freq_in_group
    allocate(ctx_src_inv%src_val(1,ctx_src_inv%n_freq))
    ctx_src_inv%src_val=0.0

    !! 1) continue only if we invert the source >>>>>>>>>>>>>>>>>>>>>>>>
    if(ctx_src_inv%flag_source_inv) then
      !! indicates if all sources have the same value (which changes
      !! with frequency)
      call read_parameter(parameter_file,'fwi_source_inversion_shared', &
                          .true.,buffer_logical,nval)
      ctx_src_inv%flag_common_source = buffer_logical(1)

      !! copy from acquisition
      ctx_src_inv%src_type = ctx_aq%src_type
      !! consistency check
      if(.not. ctx_src_inv%flag_common_source) then
        ctx_err%msg   = "** ERROR: the source inversion requires same"//&
                        " value for all shots [source_inv_init]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      !! only dirac are supported
      if(ctx_src_inv%src_type .ne. SRC_DIRAC) then
        ctx_err%msg   = "** ERROR: the source inversion requires "    //&
                        " Dirac source for now [source_inv_init]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      !! ---------------------------------------------------------------
      if(ctx_paral%master) then
        write(6,'(a)') "- source reconstruction using L2 norm"
      end if
    end if

    !! 2) allocation >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! remains to zero if not source_inversion
    allocate(ctx_src_inv%num    (1,ctx_src_inv%n_freq))
    allocate(ctx_src_inv%den    (1,ctx_src_inv%n_freq))

    ctx_src_inv%num    =0.0
    ctx_src_inv%den    =0.0

    return
  end subroutine source_inv_init

end module m_source_inv_init
