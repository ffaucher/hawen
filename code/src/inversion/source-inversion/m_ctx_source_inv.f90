!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_source_inv.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the context for the source inversion.
!
!------------------------------------------------------------------------------
module m_ctx_source_inv

  use m_raise_error,                 only: raise_error, t_error
  use m_ctx_acquisition,             only: SRC_NONE
  implicit none

  private
  public :: t_source_inversion, source_inversion_set,                   &
            source_inversion_reset, source_inversion_clean,             &
            source_inversion_update

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_source_inv contains all information for the inversion of 
  !>                   the source function
  !
  !> @param[logical] flag_source_inv   : indicates if source inversion
  !> @param[logical] flag_common_source: indicates if one source for all
  !> @param[integer] n_source          : number of sources
  !> @param[integer] n_freq            : number of parallel freq
  !> @param[complex] src_val           : source values
  !> @param[complex] num               : computational array
  !> @param[complex] den               : computational array
  !
  !---------------------------------------------------------------------
  type t_source_inversion
    !! indicates the type of source, only dirac is supported for now
    integer                      :: src_type
    !! indicates if we invert the source value or not
    logical                      :: flag_source_inv
    !! consider the same source value for everyone
    logical                      :: flag_common_source
    !! number of sources 
    integer                      :: n_source
    !! number of parallel frequencies
    integer                      :: n_freq
    !! current source value
    complex(kind=8),allocatable  :: src_val(:,:)

    !! current working arrays
    complex(kind=8), allocatable :: num(:,:) !! numerator
    complex(kind=8), allocatable :: den(:,:) !! denominator

  end type t_source_inversion
  !---------------------------------------------------------------------
    
  contains  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> reset source inversion context
  !
  !> @param[inout] ctx_src_inv  : source inversion context
  !---------------------------------------------------------------------
  subroutine source_inversion_reset(ctx_src_inv,n_freq_in_group)
    implicit none   
    type(t_source_inversion)    ,intent(inout)  :: ctx_src_inv
    integer                     ,intent(in)     :: n_freq_in_group

    !! -----------------------------------------------------------------
    !! reallocate to the propoer number of frequency
    if(ctx_src_inv%n_freq .ne. n_freq_in_group) then
      deallocate(ctx_src_inv%src_val)
      ctx_src_inv%n_freq = n_freq_in_group
      allocate(ctx_src_inv%src_val(1,n_freq_in_group))
    end if
    ctx_src_inv%src_val= 0.0

    !! -----------------------------------------------------------------
    !! depends if we invert or not
    if(ctx_src_inv%flag_source_inv) then
      if(ctx_src_inv%n_freq .ne. n_freq_in_group) then
        ctx_src_inv%n_freq = n_freq_in_group
        deallocate(ctx_src_inv%num)
        deallocate(ctx_src_inv%den)
        allocate(ctx_src_inv%num(1,n_freq_in_group))
        allocate(ctx_src_inv%den(1,n_freq_in_group))
      end if
      ctx_src_inv%num = 0.0
      ctx_src_inv%den = 0.0
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine source_inversion_reset

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> update the source value
  !
  !> @param[inout] ctx_src_inv  : source inversion context
  !> @param[inout] ctx_err      : error context
  !---------------------------------------------------------------------
  subroutine source_inversion_update(ctx_src_inv,ctx_err)
    implicit none   
    type(t_source_inversion)    ,intent(inout)  :: ctx_src_inv
    type(t_error)               ,intent(inout)  :: ctx_err
    !! -----------------------------------------------------------------
    integer :: ifreq
    !! -----------------------------------------------------------------
    ctx_err%ierr=0

    if(ctx_src_inv%flag_source_inv) then
      if(ctx_src_inv%flag_common_source) then
        !! loop over all frequency
        do ifreq=1,ctx_src_inv%n_freq
          if(abs(ctx_src_inv%den(1,ifreq)) > 1.E-30) then
            ctx_src_inv%src_val(1,ifreq) = ctx_src_inv%num(1,ifreq) /   &
                                           ctx_src_inv%den(1,ifreq)
          else
            ctx_err%msg   = "** ERROR: The source update has exceptional "//&
                            "values [source_update] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
        end do
      else
        ctx_err%msg   = "** ERROR: We only support one source value "// &
                        "[source_update] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      endif
    end if
    !! -----------------------------------------------------------------

    return
  end subroutine source_inversion_update

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> set initial source value from input 
  !
  !> @param[inout] ctx_src_inv  : source inversion context
  !---------------------------------------------------------------------
  subroutine source_inversion_set(ctx_src_inv,i_freq,src_val)
    implicit none   
    type(t_source_inversion)    ,intent(inout)  :: ctx_src_inv
    integer                     ,intent(in)     :: i_freq
    complex(kind=8)             ,intent(in)     :: src_val
    
    ctx_src_inv%src_val(1,i_freq) = src_val

    return
  end subroutine source_inversion_set

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> clean gradient information and allocation
  !
  !> @param[inout] ctx_gradient  : gradient context
  !---------------------------------------------------------------------
  subroutine source_inversion_clean(ctx_src_inv)
    type(t_source_inversion)    ,intent(inout)  :: ctx_src_inv

    ctx_src_inv%flag_source_inv    = .false.
    ctx_src_inv%flag_common_source = .false.
    ctx_src_inv%src_type           = SRC_NONE
    ctx_src_inv%n_source           = 0
    ctx_src_inv%n_freq             = 0
    if(allocated(ctx_src_inv%src_val)) deallocate(ctx_src_inv%src_val)
    if(allocated(ctx_src_inv%num    )) deallocate(ctx_src_inv%num    )
    if(allocated(ctx_src_inv%den    )) deallocate(ctx_src_inv%den    )

    return
  end subroutine source_inversion_clean

!------------------------------------------------------------------------------
end module m_ctx_source_inv
!------------------------------------------------------------------------------
