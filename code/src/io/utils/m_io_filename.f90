!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_io_filename.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to generate io filenames: we need a local
!> name and a global name. the global name is for paraview format
!> which can emcompass several (local name) fields.
!
!------------------------------------------------------------------------------
module m_io_filename

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  !> interface get filename
  interface io_filename
    module procedure io_filename_multiple
    module procedure io_filename_single
  end interface io_filename


  private
  public  :: io_filename

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> generate filename: we have a local field name and a global one
  !> (for paraview). We DO NOT treat the folder name here, it is 
  !> already decided in type t_io. Also we DO NOT add file extension 
  !> here.
  !
  !> @param[inout] nameloc_out    : output local names
  !> @param[inout] namegb_out     : output global name
  !> @param[in]    nameloc_in     : input local names
  !> @param[in]    namegb_in      : input global name
  !> @param[in]    nloc           : number of local field
  !> @param[in]    flag_freq      : indicate if we print the frequency
  !> @param[in]    flag_mode      : indicate if we print the mode
  !> @param[in]    freq           : value for frequency
  !> @param[in]    mode           : value for mode
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_iter         : [OPTIONAL] iteration
  !> @param[in]    o_isrc         : [OPTIONAL] source number
  !----------------------------------------------------------------------------
  subroutine io_filename_multiple(nameloc_out,namegb_out,nameloc_in,    &
                                  namegb_in,nloc,flag_freq,flag_mode,   &
                                  freq,mode,ctx_err,o_iter,o_isrc)
    implicit none
    character(len=*),allocatable ,intent(inout):: nameloc_out(:)
    character(len=*)             ,intent(inout):: namegb_out
    character(len=*),allocatable ,intent(in)   :: nameloc_in(:)
    character(len=*)             ,intent(in)   :: namegb_in
    integer                      ,intent(in)   :: nloc
    logical                      ,intent(in)   :: flag_freq,flag_mode
    real(kind=8)                 ,intent(in)   :: freq(2)
    integer(kind=4)              ,intent(in)   :: mode
    type(t_error)                ,intent(inout):: ctx_err
    !! optional
    integer,optional             ,intent(in)   :: o_iter
    integer,optional             ,intent(in)   :: o_isrc
    !! local
    integer              :: k
    character(len=32)    :: stamp
    character(len=512)   :: suffix
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    suffix=''
    !! mode first
    if(flag_mode) then
      write(stamp,'(i0)')  mode 
      suffix ="_mode"// trim(adjustl(stamp))
    end if

    if(flag_freq) then !! frequency next
      write(stamp,'(es20.5)')  freq(2) !! LAPLACE
      suffix =trim(adjustl(suffix)) // "_frequency_"// trim(adjustl(stamp))
      write(stamp,'(es20.5)')  freq(1) !! FOURIER
      suffix =trim(adjustl(suffix))//"_"// trim(adjustl(stamp))//"Hz"
    end if
    
    !! then sources
    if(present(o_isrc)) then
      write(stamp,'(i0.6)')  o_isrc
      suffix =trim(adjustl(suffix))//"_src"// trim(adjustl(stamp))
    end if

    !! then iterations
    if(present(o_iter)) then
      write(stamp,'(i0.6)')  o_iter
      suffix =trim(adjustl(suffix))//"_iteration_"// trim(adjustl(stamp))
    end if

    !! final names
    do k=1,nloc
      nameloc_out(k)=trim(adjustl(nameloc_in(k)))//trim(adjustl(suffix))
    end do
    namegb_out      =trim(adjustl(namegb_in))    //trim(adjustl(suffix))

    return
  end subroutine io_filename_multiple

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> generate filename: we have a local field name and a global one
  !> (for paraview). We DO NOT treat the folder name here, it is 
  !> already decided in type t_io. Also we DO NOT add file extension 
  !> here.
  !
  !> @param[inout] namegb_out     : output global name
  !> @param[in]    namegb_in      : input global name
  !> @param[in]    flag_freq      : indicate if we print the frequency
  !> @param[in]    flag_mode      : indicate if we print the mode
  !> @param[in]    freq           : value for frequency
  !> @param[in]    mode           : value for mode
  !> @param[inout] ctx_err        : context error
  !> @param[in]    o_iter         : [OPTIONAL] iteration
  !> @param[in]    o_isrc         : [OPTIONAL] source number
  !----------------------------------------------------------------------------
  subroutine io_filename_single(namegb_out,namegb_in,flag_freq,flag_mode, &
                                freq,mode,ctx_err,o_iter,o_isrc)
    implicit none
    character(len=*)             ,intent(inout):: namegb_out
    character(len=*)             ,intent(in)   :: namegb_in
    logical                      ,intent(in)   :: flag_freq,flag_mode
    real(kind=8)                 ,intent(in)   :: freq(2)
    integer(kind=4)              ,intent(in)   :: mode
    type(t_error)                ,intent(inout):: ctx_err
    !! optional
    integer,optional             ,intent(in)   :: o_iter
    integer,optional             ,intent(in)   :: o_isrc
    !! local
    character(len=32)    :: stamp
    character(len=512)   :: suffix
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    suffix=''
    !! mode first
    if(flag_mode) then
      write(stamp,'(i0)')  mode 
      suffix ="_mode"// trim(adjustl(stamp))
    end if

    if(flag_freq) then !! frequency next
      write(stamp,'(es20.5)')  freq(2) !! LAPLACE
      suffix =trim(adjustl(suffix)) // "_frequency_"// trim(adjustl(stamp))
      write(stamp,'(es20.5)')  freq(1) !! FOURIER
      suffix =trim(adjustl(suffix))//"_"// trim(adjustl(stamp))//"Hz"
    end if
    
    !! then sources
    if(present(o_isrc)) then
      write(stamp,'(i0.6)')  o_isrc
      suffix =trim(adjustl(suffix))//"_src"// trim(adjustl(stamp))
    end if

    !! then iterations
    if(present(o_iter)) then
      write(stamp,'(i0.6)')  o_iter
      suffix =trim(adjustl(suffix))//"_iteration_"// trim(adjustl(stamp))
    end if

    !! final name
    namegb_out      =trim(adjustl(namegb_in))    //trim(adjustl(suffix))

    return
  end subroutine io_filename_single
  
end module m_io_filename
