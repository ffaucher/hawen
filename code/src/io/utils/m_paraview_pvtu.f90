!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE module m_vtk_unstructured
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save paraview file PVTU format.
!> The pvtu is simply one file that accounts for all 
!> the parallel subdomain: it lists what the processors
!> have been writing.
!
!------------------------------------------------------------------------------
module m_paraview_pvtu

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none
  
  private
  public  :: save_paraview_pvtu

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create PVTU header for mesh: only one cpus has to write
  !> this header which is linked to the parallel files.
  !
  !> @param[in]    str_representation: representation solution (dof or cell)
  !> @param[in]    pvtu_filename     : name of the pvtu file
  !> @param[in]    shortname         : shortname for arrays saved
  !> @param[in]    nproc             : number of cpus involved
  !> @param[in]    fieldname         : name of the fields
  !> @param[in]    nfield            : number of fields
  !> @param[in]    flag_cx           : indicates if cx data type
  !> @param[in]    ikind             : kind for int  (mesh)
  !> @param[in]    rkind_mesh        : kind for real (mesh)
  !> @param[in]    rkind_sol         : kind for real (fields saved)
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine save_paraview_pvtu(str_representation,pvtu_filename,       &
                                shortname,nproc,fieldname,nfield,       &
                                flag_cx,ikind,rkindmesh,rkindsol,ctx_err)

    character(len=*)            , intent(in)   :: str_representation
    character(len=*),             intent(in)   :: pvtu_filename,shortname
    character(len=*),allocatable, intent(in)   :: fieldname(:)
    integer         ,             intent(in)   :: nfield,nproc,ikind
    integer         ,             intent(in)   :: rkindmesh,rkindsol
    logical         ,             intent(in)   :: flag_cx
    type(t_error)   ,             intent(out)  :: ctx_err
    !! local 
    character(len=256)  :: procname,local_name,filename,shortname_loc
    character(len=32)   :: fmt_iproc,ifmt,str_data
    character(len=2)    :: char_int,char_rmesh,char_rsol
    integer             :: k,ifield,ndigits,k1,k2
    integer             :: unit_io=121

    ctx_err%ierr=0
    
    !! filename is from last "/" to ".pvtu"
    k1=1
    k2=len_trim(adjustl(pvtu_filename))

    do k=1,len_trim(adjustl(pvtu_filename))
      if(pvtu_filename(k:k) == "/") k1=k
    end do
    do k=k1+1,len_trim(adjustl(pvtu_filename))-4
      if(pvtu_filename(k:k+4) == ".pvtu") k2=k-1
    end do
    filename=''
    filename(1:k2-k1)=pvtu_filename(k1+1:k2)

    !! the data type depends if solution per cell or dof
    str_data=''
    select case(trim(adjustl(str_representation)))
      case('cell')
        str_data='CellData'

      case('dof','dof-orderfix')
        str_data='PointData'

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh array "   //       &  
                        "representation " //                            &
                        trim(adjustl(str_representation))//             &
                        "in [paraview_pvtu] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)             
    end select

    !! kind of int and real
    select case(ikind)
      case(8)
        char_int='64'
      case(4)
        char_int='32'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized int size  [pvtu_header] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    select case(rkindsol)
      case(8)
        char_rsol='64'
      case(4)
        char_rsol='32'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized real size  [pvtu_header] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    select case(rkindmesh)
      case(8)
        char_rmesh='64'
      case(4)
        char_rmesh='32'
      case default
        ctx_err%msg   = "** ERROR: Unrecognized real size  [pvtu_header] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    !! -----------------------------------------------------------------
    !! Open header part
    !! -----------------------------------------------------------------
    open(unit_io,file=trim(adjustl(pvtu_filename)),status='replace')
    !! open(unit_io,file=trim(adjustl(fullname)),access='stream',status='replace')

    write(unit_io,'(a)') '<?xml version="1.0"?>'
    write(unit_io,'(a)') '<VTKFile type="PUnstructuredGrid">'
    write(unit_io,'(a)') '<PUnstructuredGrid>'
    write(unit_io,'(a)') '  <PPoints>'
    
    !! this is the coordinates: we have three positions: x,y,z
    write(unit_io,'(a)') '    <PDataArray type="Float'//char_rmesh//    &
                         '" Name="Position" NumberOfComponents="3" ' // &
                         'format="ascii">'
    !! infos for the mesh
    write(unit_io,'(a)') '    </PDataArray>'
    write(unit_io,'(a)') '  </PPoints>'
    write(unit_io,'(a)') '  <PCells>'
    write(unit_io,'(a)') '    <PDataArray type="Int'//char_int//        &
                         '" Name="connectivity" format="ascii">'
    write(unit_io,'(a)') '    </PDataArray>'
    write(unit_io,'(a)') '    <PDataArray type="Int'//char_int//        &
                         '" Name="offsets"      format="ascii">'
    write(unit_io,'(a)') '    </PDataArray>'
    write(unit_io,'(a)') '    <PDataArray type="UInt8" Name="types"    '//&
                         '    format="ascii">'
    write(unit_io,'(a)') '    </PDataArray>'
    write(unit_io,'(a)') '  </PCells>'
       
    
    write(unit_io,'(a)') '  <P'//trim(adjustl(str_data))//' Scalars="'//&
                         trim(adjustl(shortname))//'">'
   
    !! loop over local name
    do ifield=1,nfield
      !! shortname is given by the string found before the first '_'
      local_name = trim(adjustl(fieldname(ifield)))
      shortname_loc=''
      do k=1,len_trim(adjustl(local_name))
        if(local_name(k:k) == "_") exit
      end do
      shortname_loc=''
      shortname_loc(1:k-1) = local_name(1:k-1)
      
      !! real and imaginary part 
      if(flag_cx) then
        write(unit_io,'(a)') '    <PDataArray type="Float'//char_rsol// &
                             '" Name="Real(' //trim(shortname_loc)   // &
                             ')" format="ascii">'
        write(unit_io,'(a)') '    </PDataArray>'
        write(unit_io,'(a)') '    <PDataArray type="Float'//char_rsol// &
                             '" Name="Imag('//trim(shortname_loc)    // &
                             ')" format="ascii">'
        write(unit_io,'(a)') '    </PDataArray>'
      else
        write(unit_io,'(a)') '    <PDataArray type="Float'//char_rsol// &
                             '" Name="'//trim(shortname_loc)         // &
                             '" format="ascii">'
        write(unit_io,'(a)') '    </PDataArray>'
      endif
    end do
    write(unit_io,'(a)') '  </P'//trim(adjustl(str_data))//'>'
    
    !! list of attached file: one per processors
    !! adjust int size with nprocessors
    ndigits  = ceiling(log10(real(nproc)+1.0)) + 1 
    write(ifmt,'(i0)') ndigits
    fmt_iproc = '(i0.'//trim(adjustl(ifmt))//')'

    do k=0,nproc-1
      write(ifmt,trim(adjustl(fmt_iproc))) k 
      procname = trim(adjustl(filename))//"_cpu"//trim(adjustl(ifmt))//".vtu"
      write(unit_io,'(a)') '  <Piece Source="'//trim(adjustl(procname))//'"/>'
    end do
   
    !! -----------------------------------------------------------------
    !! close file
    write(unit_io,'(a)') '</PUnstructuredGrid>' 
    write(unit_io,'(a)') '</VTKFile>' 
    close(unit_io)

    return
  end subroutine save_paraview_pvtu
  
end module m_paraview_pvtu
