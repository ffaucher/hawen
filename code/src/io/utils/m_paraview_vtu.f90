!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE module m_paraview_vtu
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save unstructured grid using VTU format
!> with binary (there must be 1 main .pvtu and 1 .vtu per cpus).
!
!------------------------------------------------------------------------------
module m_paraview_vtu

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  interface save_paraview_vtu
     module procedure save_paraview_vtu_real4
     module procedure save_paraview_vtu_real8
     module procedure save_paraview_vtu_complex4
     module procedure save_paraview_vtu_complex8
  end interface
  
  private
  public  :: save_paraview_vtu

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in vtu
  !
  !> @param[in]    filename       : name of the file
  !> @param[in]    values         : values to be saved
  !----------------------------------------------------------------------------
  subroutine save_paraview_vtu_real4(vtu_filename,values,ctx_err)
    character(len=*),             intent(in)   :: vtu_filename
    real   (kind=4)             , intent(in)   :: values(:)
    type(t_error)               , intent(out)  :: ctx_err
    !! local 
    integer                    :: io,unit_io=110
    integer(kind=4)            :: mem !! must for int4 for paraview

    ctx_err%ierr=0
    
    !! size does not check if allocated, 
    !! sizeof is not intrinsic procedure => e.g. ko for ifort
    mem = int(size(values)*4) !! must for int4 for paraview (Real4 here)
    
    !! open file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    open(unit_io,file=trim(adjustl(vtu_filename)),action='write',       &
         form='unformatted',access='stream',convert='big_endian',       &
         iostat=io,position='append',status='old')
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) real(values)
    !! close file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    close(unit_io)
    
    return
  end subroutine save_paraview_vtu_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in vtu
  !
  !> @param[in]    filename       : name of the file
  !> @param[in]    values         : values to be saved
  !----------------------------------------------------------------------------
  subroutine save_paraview_vtu_real8(vtu_filename,values,ctx_err)
    character(len=*),             intent(in)   :: vtu_filename
    real   (kind=8)             , intent(in)   :: values(:)
    type(t_error)               , intent(out)  :: ctx_err
    !! local 
    integer                    :: io,unit_io=110
    integer(kind=4)            :: mem !! must for int4 for paraview

    ctx_err%ierr=0

    !! size does not check if allocated, 
    !! sizeof is not intrinsic procedure => e.g. ko for ifort
    mem = int(size(values)*8) !! must for int4 for paraview (Real4 here)
    
    !! open file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    open(unit_io,file=trim(adjustl(vtu_filename)),action='write',       &
         form='unformatted',access='stream',convert='big_endian',       &
         iostat=io,position='append',status='old')
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) real(values)
    !! close file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    close(unit_io)
    
    return
  end subroutine save_paraview_vtu_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in vtu
  !
  !> @param[in]    filename       : name of the file
  !> @param[in]    values         : values to be saved
  !----------------------------------------------------------------------------
  subroutine save_paraview_vtu_complex4(vtu_filename,values,ctx_err)
    character(len=*),             intent(in)   :: vtu_filename
    complex(kind=4)             , intent(in)   :: values(:)
    type(t_error)               , intent(out)  :: ctx_err
    !! local 
    integer                    :: io,unit_io=110
    integer(kind=4)            :: mem !! must for int4 for paraview

    ctx_err%ierr=0

    !! size does not check if allocated, 
    !! sizeof is not intrinsic procedure => e.g. ko for ifort
    mem = int(size(values)*4) !! must for int4 for paraview (Real4 here)
    
    !! open file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    open(unit_io,file=trim(adjustl(vtu_filename)),action='write',       &
         form='unformatted',access='stream',convert='big_endian',       &
         iostat=io,position='append',status='old')
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) real(values)
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) aimag(values)
    !! close file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    close(unit_io)
    
    return
  end subroutine save_paraview_vtu_complex4
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in vtu
  !
  !> @param[in]    filename       : name of the file
  !> @param[in]    values         : values to be saved
  !----------------------------------------------------------------------------
  subroutine save_paraview_vtu_complex8(vtu_filename,values,ctx_err)
    character(len=*),             intent(in)   :: vtu_filename
    complex(kind=8)             , intent(in)   :: values(:)
    type(t_error)               , intent(out)  :: ctx_err
    !! local 
    integer                    :: io,unit_io=110
    integer(kind=4)            :: mem !! must for int4 for paraview

    ctx_err%ierr=0

    !! size does not check if allocated, 
    !! sizeof is not intrinsic procedure => e.g. ko for ifort
    mem = int(size(values)*8) !! must for int4 for paraview (Real4 here)

    !! open file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    open(unit_io,file=trim(adjustl(vtu_filename)),action='write',       &
         form='unformatted',access='stream',convert='big_endian',       &
         iostat=io,position='append',status='old')
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) real(values,kind=8)
    write(unit_io,iostat=io) mem
    write(unit_io,iostat=io) real(aimag(values),kind=8)
    !! close file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    close(unit_io)
    
    return
  end subroutine save_paraview_vtu_complex8

end module m_paraview_vtu
