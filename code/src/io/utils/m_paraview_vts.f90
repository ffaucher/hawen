!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_paraview_vts.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save structured grid using VTK format
!> for now we only allow ascii... this is the .vts file
!
!------------------------------------------------------------------------------
module m_paraview_vts

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  interface save_paraview_vts
     module procedure save_paraview_vts_3d_complex4
     module procedure save_paraview_vts_3d_complex8
     module procedure save_paraview_vts_3d_real4
     module procedure save_paraview_vts_3d_real8
  end interface
  
  interface save_paraview_header_vts
     module procedure save_paraview_header_vts_3D
  end interface
  
  private
  public  :: save_paraview_vts, save_paraview_header_vts

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create VTK header for structured 3D array 
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    dx             : discretization step in x
  !> @param[in]    dy             : discretization step in y
  !> @param[in]    dz             : discretization step in z
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine save_paraview_header_vts_3D(basename,filename,nx,ny,nz,    &
                                         dx,dy,dz,ox,oy,oz)
    character(len=*),intent(in)   :: basename 
    character(len=*),intent(in)   :: filename
    integer         ,intent(in)   :: nx,ny,nz
    real(kind=8)    ,intent(in)   :: dx,dy,dz
    real(kind=8)    ,intent(in)   :: ox,oy,oz
    !! local 
    character(len=256)  :: fullname
    integer             :: xx,yy,zz
    integer             :: unit_io=121

    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".vtk"
    !! open file
    open(unit_io,file=trim(adjustl(fullname)))
    !! open(unit_io,file=trim(adjustl(fullname)),access='stream',status='replace')
    
    !! -----------------------------------------------------------------
    !! Header part
    !! -----------
    !!     - Version
    !!     - Name
    !!     - Type
    !! -----------------------------------------------------------------
    write(unit_io,'(a)') '# vtk DataFile Version 4.2'
    write(unit_io,'(a)')  trim(adjustl(filename))
    write(unit_io,'(a)')  'ASCII'
    write(unit_io,'(a)')  ' '

    !! Data type : Rectilinear grid for structured save
    write(unit_io,'(a)')        'DATASET RECTILINEAR_GRID'
    write(unit_io,'(a,3i10)')   'DIMENSIONS ',nx,ny,nz
    !! X coordinates : structured positionning
    write(unit_io,'(a,i10,a)')  'X_COORDINATES ',nx,' float'
    do xx=1,nx
       write(unit_io,'(es20.10)') ox+dx*(xx-1)
    enddo
    !! Y coordinates : structured positionning
    write(unit_io,'(a,i10,a)')  'Y_COORDINATES ',ny,' float'
    do yy=1,ny
       write(unit_io,'(es20.10)') oy+dy*(yy-1)
    enddo
    !! Z coordinates  
    write(unit_io,'(a,i10,a)')  'Z_COORDINATES ',nz,' float'
    do zz=1,nz
       write(unit_io,'(es20.10)') oz+dz*(zz-1)
    enddo
    write(unit_io,'(a)')  ' '
    write(unit_io,'(a,1i10)')       'POINT_DATA',nx*ny*nz
    !! -----------------------------------------------------------------
    !! close file
    close(unit_io)

    return
  end subroutine save_paraview_header_vts_3D


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    fieldname      : name of the current field
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    values         : 3D values
  !----------------------------------------------------------------------------
  subroutine save_paraview_vts_3d_real4(basename,filename,fieldname,  &
                                          nx,ny,nz,values)
    character(len=*),intent(in)   :: basename 
    character(len=*),intent(in)   :: filename
    character(len=*),intent(in)   :: fieldname
    integer         ,intent(in)   :: nx,ny,nz
    real,allocatable,intent(in)   :: values(:,:,:)
    !! local 
    character(len=256)  :: fullname,shortname
    integer             :: xx,yy,zz,k
    integer             :: unit_io=14
    
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".vtk"
    !! shortname is given by the string found before the first '_'
    shortname=''
    do k=1,len_trim(adjustl(fieldname))
      if(fieldname(k:k) == "_") then
        exit
      end if
    end do
    shortname(1:k-1) = fieldname(1:k-1)
    
    !! open file
    open(unit_io,file=trim(adjustl(fullname)),position="append")

    !! Structured Real data
    !! -----------------------------------------------------------------
    write(unit_io,'(a)')            'FIELD '//trim(adjustl(fieldname))//' 1'
    write(unit_io,'(a,i2,a,i10,a)')  trim(shortname),1," ",nx*ny*nz," float"
    do zz=1,nz ; do yy=1,ny ; do xx=1,nx 
       write(unit_io,'(es20.10)') real(values(xx,yy,zz),kind=4)
    enddo ; enddo ; enddo
    
    write(unit_io,'(a)')  ' '

    !! close file
    close(unit_io)
    return
  end subroutine save_paraview_vts_3d_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    fieldname      : name of the current field
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    values         : 3D values
  !----------------------------------------------------------------------------
  subroutine save_paraview_vts_3d_complex4(basename,filename,fieldname, &
                                             nx,ny,nz,values)
    character(len=*)   ,intent(in)   :: basename 
    character(len=*)   ,intent(in)   :: filename
    character(len=*)   ,intent(in)   :: fieldname
    integer            ,intent(in)   :: nx,ny,nz
    complex,allocatable,intent(in)   :: values(:,:,:)
    !! local 
    character(len=256)  :: fullname,shortname
    integer             :: xx,yy,zz,k
    integer             :: unit_io=14
    
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".vtk"
    !! shortname is given by the string found before the firs '_'
    shortname=''
    do k=1,len_trim(adjustl(fieldname))
      if(fieldname(k:k) == "_") then
        exit
      end if
    end do
    shortname(1:k-1) = fieldname(1:k-1)   

    !! open file
    open(unit_io,file=trim(adjustl(fullname)),position="append")

    !! Structured Real part
    write(unit_io,'(a)')            'FIELD '//trim(adjustl(fieldname))//' 2'
    write(unit_io,'(a,i2,a,i10,a)') 'Real('//trim(shortname)//')',1,' ',&
                                    nx*ny*nz," float"
    do zz=1,nz ; do yy=1,ny ; do xx=1,nx 
       write(unit_io,'(es20.10)') real(values(xx,yy,zz),kind=4)
    enddo ; enddo ; enddo
    write(unit_io,'(a)')  ' '

    !! Structured Imaginary part
    write(unit_io,'(a,i2,a,i10,a)') 'Imag('//trim(shortname)//')',1,' ',&
                                    nx*ny*nz," float"
    do zz=1,nz ; do yy=1,ny ; do xx=1,nx 
       write(unit_io,'(es20.10)') aimag(values(xx,yy,zz))
    enddo ; enddo ; enddo
    write(unit_io,'(a)')  ' '

    !! close file
    close(unit_io)
    return
  end subroutine save_paraview_vts_3d_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    fieldname      : name of the current field
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    values         : 3D values
  !----------------------------------------------------------------------------
  subroutine save_paraview_vts_3d_complex8(basename,filename,fieldname, &
                                             nx,ny,nz,values)
    character(len=*)           ,intent(in)   :: basename 
    character(len=*)           ,intent(in)   :: filename
    character(len=*)           ,intent(in)   :: fieldname
    integer                    ,intent(in)   :: nx,ny,nz
    complex(kind=8),allocatable,intent(in)   :: values(:,:,:)
    !! local 
    character(len=256)  :: fullname,shortname
    integer             :: xx,yy,zz,k
    integer             :: unit_io=14
    
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".vtk"
    !! shortname is given by the string found before the firs '_'
    shortname=''
    do k=1,len_trim(adjustl(fieldname))
      if(fieldname(k:k) == "_") then
        exit
      end if
    end do
    shortname(1:k-1) = fieldname(1:k-1)   

    !! open file
    open(unit_io,file=trim(adjustl(fullname)),position="append")

    !! Structured Real part
    write(unit_io,'(a)')            'FIELD '//trim(adjustl(fieldname))//' 2'
    write(unit_io,'(a,i2,a,i10,a)') 'Real('//trim(shortname)//')',1,' ',&
                                    nx*ny*nz," float"
    do zz=1,nz ; do yy=1,ny ; do xx=1,nx 
       write(unit_io,'(es20.10)') real(values(xx,yy,zz),kind=4)
    enddo ; enddo ; enddo
    write(unit_io,'(a)')  ' '

    !! Structured Imaginary part
    write(unit_io,'(a,i2,a,i10,a)') 'Imag('//trim(shortname)//')',1,' ',&
                                    nx*ny*nz," float"
    do zz=1,nz ; do yy=1,ny ; do xx=1,nx 
       write(unit_io,'(es20.10)') real(aimag(values(xx,yy,zz)))
    enddo ; enddo ; enddo
    write(unit_io,'(a)')  ' '

    !! close file
    close(unit_io)
    return
  end subroutine save_paraview_vts_3d_complex8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    fieldname      : name of the current field
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    values         : 3D values
  !----------------------------------------------------------------------------
  subroutine save_paraview_vts_3d_real8(basename,filename,fieldname,  &
                                          nx,ny,nz,values)
    character(len=*)        ,intent(in)   :: basename 
    character(len=*)        ,intent(in)   :: filename
    character(len=*)        ,intent(in)   :: fieldname
    integer                 ,intent(in)   :: nx,ny,nz
    real(kind=8),allocatable,intent(in)   :: values(:,:,:)
    !! local 
    character(len=256)  :: fullname,shortname
    integer             :: xx,yy,zz,k
    integer             :: unit_io=14
    
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".vtk"
    
    !! open file
    open(unit_io,file=trim(adjustl(fullname)),position="append")
    !! shortname is given by the string found before the firs '_'
    shortname=''
    do k=1,len_trim(adjustl(fieldname))
      if(fieldname(k:k) == "_") then
        exit
      end if
    end do
    shortname(1:k-1) = fieldname(1:k-1)  
    
    !! Structured Real data
    !! -----------------------------------------------------------------
    write(unit_io,'(a)')            'FIELD '//trim(adjustl(fieldname))//' 1'
    write(unit_io,'(a,i2,a,i10,a)')  trim(shortname),1," ",nx*ny*nz," float"
    do zz=1,nz ; do yy=1,ny ; do xx=1,nx 
       write(unit_io,'(es20.10)') real(values(xx,yy,zz),kind=4)
    enddo ; enddo ; enddo
    write(unit_io,'(a)')  ' '

    !! close file
    close(unit_io)
    return
  end subroutine save_paraview_vts_3d_real8
  
end module m_paraview_vts
