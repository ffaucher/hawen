!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_sep.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save structured grid using SEP format
!
!------------------------------------------------------------------------------
module m_sep_save

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  !! -------------------------------------------------------------------
  implicit none

  interface save_sep_format
     module procedure save_sep_format_real4
     module procedure save_sep_format_real8
     module procedure save_sep_format_complex4
     module procedure save_sep_format_complex8
  end interface

  private
  public  :: save_sep_format

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create SEP header
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    ox             : origin in x
  !> @param[in]    oy             : origin in y
  !> @param[in]    oz             : origin in z
  !> @param[in]    dx             : discretization in x
  !> @param[in]    dy             : discretization in y
  !> @param[in]    dz             : discretization in z
  !> @param[in]    array          : 3D values to be saved
  !----------------------------------------------------------------------------
  subroutine save_sep_write_header(basename,filename,nx,ny,nz,dx,dy,dz, &
                                   ox,oy,oz,kind_save)
    character(len=*),intent(in)   :: basename 
    character(len=*),intent(in)   :: filename
    integer         ,intent(in)   :: nx,ny,nz
    real(kind=8)    ,intent(in)   :: dx,dy,dz
    real(kind=8)    ,intent(in)   :: ox,oy,oz
    integer         ,intent(in)   :: kind_save
    !! local 
    character(len=256)  :: fullname
    character(len=32)   :: stamp
    integer             :: unit_io=105

    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".H"
    !! open file
    open(unit_io,file=trim(adjustl(fullname)),status='replace')
    
    !! -----------------------------------------------------------------
    !! Header part
    !! -----------
    !!     - Version
    !!     - Name
    !!     - Type
    !! -----------------------------------------------------------------
    write(unit_io,'(a)') '############################################' // &
                         '##############'
    write(unit_io,'(a)') '# SEP file created using Hawen Software'
    write(unit_io,'(a)') '#                [Faculty of Mathematics, Univ. of Vienna]'
    write(unit_io,'(a)') '# Filename: ' // trim(adjustl(filename))
    write(unit_io,'(a)') '############################################' // &
                         '##############'

    !! dimensions
    write(stamp,'(i30)')  nx
    write(unit_io,'(a)') 'n1='//trim(adjustl(stamp))
    write(stamp,'(i30)')  ny
    write(unit_io,'(a)') 'n2='//trim(adjustl(stamp))
    write(stamp,'(i30)')  nz
    write(unit_io,'(a)') 'n3='//trim(adjustl(stamp))
    !! origins    
    write(stamp,'(es20.10)') ox
    write(unit_io,'(a)')    'o1='//trim(adjustl(stamp))
    write(stamp,'(es20.10)') oy
    write(unit_io,'(a)')    'o2='//trim(adjustl(stamp))
    write(stamp,'(es20.10)') oz
    write(unit_io,'(a)')    'o3='//trim(adjustl(stamp))
    !! discretization steps
    write(stamp,'(es20.10)') dx
    write(unit_io,'(a)')    'd1='//trim(adjustl(stamp))
    write(stamp,'(es20.10)') dy
    write(unit_io,'(a)')    'd2='//trim(adjustl(stamp))
    write(stamp,'(es20.10)') dz
    write(unit_io,'(a)')    'd3='//trim(adjustl(stamp))
    !! local format depends on the kind
    select case(kind_save)
      case(4)
        write(unit_io,'(a)') 'data_format=native_float'
      case(8)
        write(unit_io,'(a)') 'data_format=double'
      case default
        write(unit_io,'(a)') 'data_format=UNKNOWN'
    end select
    !! local filename
    fullname = trim(adjustl(filename))//".H@"
    write(unit_io,'(a)') 'in='//trim(adjustl(fullname))
    
    !! -----------------------------------------------------------------
    !! close file
    close(unit_io)

    return
  end subroutine save_sep_write_header


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in SEP with the header creation first
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    ox             : origin in x
  !> @param[in]    oy             : origin in y
  !> @param[in]    oz             : origin in z
  !> @param[in]    dx             : discretization in x
  !> @param[in]    dy             : discretization in y
  !> @param[in]    dz             : discretization in z
  !> @param[in]    array          : 3D values to be saved
  !----------------------------------------------------------------------------
  subroutine save_sep_format_real4(basename,filename,nx,ny,nz,ox,oy,oz, &
                                   dx,dy,dz,array)
    character(len=*),intent(in)   :: basename 
    character(len=*),intent(in)   :: filename
    integer         ,intent(in)   :: nx,ny,nz
    real(kind=8)    ,intent(in)   :: ox,oy,oz
    real(kind=8)    ,intent(in)   :: dx,dy,dz
    real,allocatable,intent(in)   :: array(:,:,:)
    !! local 
    character(len=256)  :: fullname
    integer             :: unit_io=17
    
    !! create sep file
    call save_sep_write_header(basename,filename,nx,ny,nz,dx,dy,dz, &
                               ox,oy,oz,4)
    !! save array
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".H@"
    open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
          status='replace',recl=4*nx*ny*nz)
    write(unit=unit_io,rec=1) real(array)
    close(unit=unit_io)
    
    return
  end subroutine save_sep_format_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in SEP with the header creation first
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    ox             : origin in x
  !> @param[in]    oy             : origin in y
  !> @param[in]    oz             : origin in z
  !> @param[in]    dx             : discretization in x
  !> @param[in]    dy             : discretization in y
  !> @param[in]    dz             : discretization in z
  !> @param[in]    array          : 3D values to be saved
  !----------------------------------------------------------------------------
  subroutine save_sep_format_real8(basename,filename,nx,ny,nz,ox,oy,oz, &
                                   dx,dy,dz,array)
    character(len=*)        ,intent(in)   :: basename 
    character(len=*)        ,intent(in)   :: filename
    integer                 ,intent(in)   :: nx,ny,nz
    real(kind=8)            ,intent(in)   :: ox,oy,oz
    real(kind=8)            ,intent(in)   :: dx,dy,dz
    real(kind=8),allocatable,intent(in)   :: array(:,:,:)
    !! local 
    character(len=256)  :: fullname
    integer             :: unit_io=17
    
    !! create sep file
    call save_sep_write_header(basename,filename,nx,ny,nz,dx,dy,dz, &
                               ox,oy,oz,8)
    !! save array
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(filename))//".H@"
    open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
          recl=8*nx*ny*nz)
    write(unit=unit_io,rec=1) array
    close(unit=unit_io)
    
    return
  end subroutine save_sep_format_real8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in SEP with the header creation first
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    ox             : origin in x
  !> @param[in]    oy             : origin in y
  !> @param[in]    oz             : origin in z
  !> @param[in]    dx             : discretization in x
  !> @param[in]    dy             : discretization in y
  !> @param[in]    dz             : discretization in z
  !> @param[in]    array          : 3D values to be saved
  !----------------------------------------------------------------------------
  subroutine save_sep_format_complex4(basename,filename,nx,ny,nz,ox,oy,oz, &
                                      dx,dy,dz,array)
    character(len=*)           ,intent(in)   :: basename 
    character(len=*)           ,intent(in)   :: filename
    integer                    ,intent(in)   :: nx,ny,nz
    real(kind=8)               ,intent(in)   :: ox,oy,oz
    real(kind=8)               ,intent(in)   :: dx,dy,dz
    complex(kind=4),allocatable,intent(in)   :: array(:,:,:)
    !! local 
    character(len=256)  :: fullname,tempname
    integer             :: unit_io=17
    
    !! create sep file for the real part
    !! -----------------------------------------------------------------
    tempname=trim(adjustl(filename))//"_real"
    call save_sep_write_header(trim(adjustl(basename)),                 &
                               trim(adjustl(tempname)),nx,ny,nz,dx,dy,  &
                               dz,ox,oy,oz,4)
    !! save real part 
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(tempname))//".H@"
    open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
          recl=4*nx*ny*nz)
    write(unit=unit_io,rec=1) real(array)
    close(unit=unit_io)

    !! create sep file for the imaginary part
    !! -----------------------------------------------------------------
    tempname=trim(adjustl(filename))//"_imaginary"
    call save_sep_write_header(trim(adjustl(basename)),                 &
                               trim(adjustl(tempname)),nx,ny,nz,dx,dy,  &
                               dz,ox,oy,oz,4)
    !! save real part 
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(tempname))//".H@"
    open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
          recl=4*nx*ny*nz)
    write(unit=unit_io,rec=1) aimag(array)
    close(unit=unit_io)
    
    return
  end subroutine save_sep_format_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save array in SEP with the header creation first
  !
  !> @param[in]    basename       : folder where to save
  !> @param[in]    filename       : name of the file
  !> @param[in]    nx             : number of nodes in x
  !> @param[in]    ny             : number of nodes in y
  !> @param[in]    nz             : number of nodes in z
  !> @param[in]    ox             : origin in x
  !> @param[in]    oy             : origin in y
  !> @param[in]    oz             : origin in z
  !> @param[in]    dx             : discretization in x
  !> @param[in]    dy             : discretization in y
  !> @param[in]    dz             : discretization in z
  !> @param[in]    array          : 3D values to be saved
  !----------------------------------------------------------------------------
  subroutine save_sep_format_complex8(basename,filename,nx,ny,nz,ox,oy,oz, &
                                      dx,dy,dz,array)
    character(len=*)           ,intent(in)   :: basename 
    character(len=*)           ,intent(in)   :: filename
    integer                    ,intent(in)   :: nx,ny,nz
    real(kind=8)               ,intent(in)   :: ox,oy,oz
    real(kind=8)               ,intent(in)   :: dx,dy,dz
    complex(kind=8),allocatable,intent(in)   :: array(:,:,:)
    !! local 
    character(len=256)  :: fullname,tempname
    integer             :: unit_io=17
    
    !! create sep file for the real part
    !! -----------------------------------------------------------------
    tempname=trim(adjustl(filename))//"_real"
    call save_sep_write_header(trim(adjustl(basename)),                 &
                               trim(adjustl(tempname)),nx,ny,nz,dx,dy,  &
                               dz,ox,oy,oz,8)
    !! save real part 
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(tempname))//".H@"
    open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
          recl=8*nx*ny*nz)
    write(unit=unit_io,rec=1) real(array)
    close(unit=unit_io)

    !! create sep file for the imaginary part
    !! -----------------------------------------------------------------
    tempname=trim(adjustl(filename))//"_imaginary"
    call save_sep_write_header(trim(adjustl(basename)),                 &
                               trim(adjustl(tempname)),nx,ny,nz,dx,dy,  &
                               dz,ox,oy,oz,8)
    !! save real part 
    fullname = trim(adjustl(basename))//"/"//trim(adjustl(tempname))//".H@"
    open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
          recl=8*nx*ny*nz)
    write(unit=unit_io,rec=1) aimag(array)
    close(unit=unit_io)
    
    return
  end subroutine save_sep_format_complex8

end module m_sep_save
