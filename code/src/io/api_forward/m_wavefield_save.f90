!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_wavefield_save.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO to save the solution wavefields
!
!------------------------------------------------------------------------------
module m_wavefield_save

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: RKIND_MAT
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_equation,           only: t_equation
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_acquisition,        only: t_acquisition
  use m_ctx_io,                 only: t_io
  use m_ctx_field,              only: t_field
  !>
  use m_barrier,                only: barrier
  use m_reduce_sum,             only: reduce_sum
  use m_time,                   only: time
  use m_print_field,            only: print_info_field_save
  use m_io_filename,            only: io_filename
  use m_array_save,             only: array_save
  use m_extract_rcv_data,       only: extract_rcv_data
  !! -------------------------------------------------------------------
  implicit none
  
  private
  public  :: wavefield_save

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save solution wavefields and receivers infos
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_acquisition   : type acquisition
  !> @param[in]    ctx_equation      : type equation
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_disc          : type Discretization
  !> @param[in]    ctx_io            : type IO
  !> @param[in]    ctx_field         : type field
  !> @param[in]    index_first       : first source index
  !> @param[in]    index_step        : acquisition step
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine wavefield_save(ctx_paral,ctx_aq,ctx_eq,ctx_domain,ctx_disc,&
                            ctx_io,ctx_field,index_first,index_step,    &
                            ctx_err,o_flag_verb)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_acquisition)         ,intent(in)   :: ctx_aq
    type(t_equation)            ,intent(in)   :: ctx_eq
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    type(t_io)                  ,intent(in)   :: ctx_io
    type(t_field)               ,intent(in)   :: ctx_field
    integer                     ,intent(in)   :: index_first,index_step
    type(t_error)               ,intent(inout):: ctx_err
    logical,           optional ,intent(in)   :: o_flag_verb
    !! local
    real   (kind=8) :: time0,time1
    logical         :: flag_verbose
    
    ctx_err%ierr=0
    flag_verbose = .false.
    if(present(o_flag_verb)) flag_verbose = o_flag_verb
    
    call time(time0)
    !! save the global wavefield solutions first 
    if(ctx_io%save_wavefield) then
      call full_save(ctx_paral,ctx_eq,ctx_domain,ctx_disc,ctx_io,       &
                     ctx_field,index_first,index_step,ctx_err)
    endif

    !! save the receivers informations
    if(ctx_io%save_receivers) then
      call datarcv_save(ctx_paral,ctx_aq,ctx_eq,ctx_disc,ctx_io,        &
                        ctx_field,index_first,index_step,ctx_err)
    end if
       
    !! print time
    if(flag_verbose) then
      if(ctx_io%save_wavefield .or. ctx_io%save_receivers) then
       call time(time1)
       call print_info_field_save(ctx_paral,time1-time0,ctx_err)
      end if
    end if

    return
  end subroutine wavefield_save
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save the full solution wavefields array
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_eq            : type equation
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_disc          : type Discretization
  !> @param[in]    ctx_io            : type IO
  !> @param[in]    ctx_field         : type field
  !> @param[in]    index_first       : first source index
  !> @param[in]    index_step        : acquisition step
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine full_save(ctx_paral,ctx_eq,ctx_domain,ctx_disc,ctx_io,     &
                       ctx_field,index_first,index_step,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_equation)            ,intent(in)   :: ctx_eq
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    type(t_io)                  ,intent(in)   :: ctx_io
    type(t_field)               ,intent(in)   :: ctx_field
    integer                     ,intent(in)   :: index_first,index_step
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    integer                              :: isrc,i1,i2,ifield,isrc_gb
    complex(kind=RKIND_MAT), allocatable :: wavefield(:,:)
    character(len=128)     , allocatable :: fieldname(:),savename(:)
    character(len=128)                   :: fieldname_main,savename_main
    real   (kind=8)                      :: freq(2)
    integer(kind=4)                      :: mode
    logical                              :: flag_mode, flag_freq
    !! -----------------------------------------------------------------
    ctx_err%ierr=0

    !! read from equation context the infos
    !! we have \omega + 1i \laplace
    !! which is written as 
    !!    i \omega - \laplace.
    !! so we can multiply by -i or use the minus .......................
    freq(2)  = dble(-real(ctx_eq%current_angular_frequency))
    freq(1)  = aimag(ctx_eq%current_angular_frequency) / (2.d0*4.d0*datan(1.d0))
    mode     = ctx_eq%current_mode
    flag_mode= ctx_eq%flag_mode
    flag_freq= ctx_eq%flag_freq
    
    !! -----------------------------------------------------------------
    !! we save the wavefields
    !! local solution and names
    allocate(wavefield(ctx_field%n_dofsol_loc,ctx_io%n_wfd_component))
    allocate(fieldname(ctx_io%n_wfd_component))
    allocate(savename (ctx_io%n_wfd_component))
    do ifield=1,ctx_io%n_wfd_component
      fieldname(ifield) = trim(adjustl(ctx_field%name_field(            &
                                       ctx_io%i_wfd_component(ifield))))
    end do
    fieldname_main="wavefield"

    !! loop over all sources
    do isrc=1,ctx_field%n_src
      i1 = (isrc-1)*ctx_field%n_dofsol_loc+1
      i2 = i1 + ctx_field%n_dofsol_loc - 1
      wavefield = 0
      do ifield=1,ctx_io%n_wfd_component
        wavefield(:,ifield) = ctx_field%field(i1:i2,                    & 
                                 ctx_io%i_wfd_component(ifield))
      end do
      !! gloal source nomber  
      isrc_gb=index_first + (isrc-1)*index_step
      call io_filename(savename,savename_main,fieldname,fieldname_main, &
                       ctx_io%n_wfd_component,flag_freq,flag_mode,freq, &
                       mode,ctx_err,o_isrc=isrc_gb)

      call array_save(ctx_paral,ctx_domain,ctx_disc,                    &
                      ctx_io%save_unstructured,ctx_io%save_structured,  &
                      ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                      wavefield,ctx_io%n_wfd_component,'dof',           &
                      trim(adjustl(ctx_io%workpath_wave)),              &
                      savename,trim(adjustl(savename_main)),ctx_err)
    end do

    deallocate(wavefield)
    deallocate(fieldname)
    deallocate(savename )
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------
 
    return
  end subroutine full_save
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save the receivers data are saved here
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_aq            : type acquisition
  !> @param[in]    ctx_eq            : type equation
  !> @param[in]    ctx_disc          : type Discretization
  !> @param[in]    ctx_io            : type IO
  !> @param[in]    ctx_field         : type field
  !> @param[in]    index_first       : first source index
  !> @param[in]    index_step        : acquisition step
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine datarcv_save(ctx_paral,ctx_aq,ctx_eq,ctx_disc,ctx_io,      &
                          ctx_field,index_first,index_step,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_acquisition)         ,intent(in)   :: ctx_aq
    type(t_equation)            ,intent(in)   :: ctx_eq
    type(t_discretization)      ,intent(in)   :: ctx_disc
    type(t_io)                  ,intent(in)   :: ctx_io
    type(t_field)               ,intent(in)   :: ctx_field
    integer                     ,intent(in)   :: index_first,index_step
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    integer                              :: isrc,ifield,isrc_gb,nrcv,i1,i2
    complex(kind=RKIND_MAT), allocatable :: wavefield(:,:)    
    complex(kind=RKIND_MAT), allocatable :: rcv_data   (:,:)
    complex(kind=RKIND_MAT), allocatable :: rcv_data_gb(:,:)    
    character(len=128)     , allocatable :: fieldname(:),savename(:)
    character(len=128)                   :: fieldname_main,savename_main
    character(len=256)                   :: savename_final
    real   (kind=8)                      :: freq(2)
    integer(kind=4)                      :: mode
    logical                              :: flag_mode, flag_freq
    !! -----------------------------------------------------------------
    ctx_err%ierr=0

    !! read from equation context the infos
    !! we have \omega + 1i \laplace
    !! which is written as 
    !!    i \omega - \laplace.
    !! so we can multiply by -i or use the minus .......................
    freq(2)  = dble(-real(ctx_eq%current_angular_frequency))
    freq(1)  = aimag(ctx_eq%current_angular_frequency) / (2.d0*4.d0*datan(1.d0))
    mode     = ctx_eq%current_mode
    flag_mode= ctx_eq%flag_mode
    flag_freq= ctx_eq%flag_freq

    !! -----------------------------------------------------------------
    !! local names
    allocate(fieldname(ctx_io%n_rcv_component))
    allocate(savename (ctx_io%n_rcv_component))
    do ifield=1,ctx_io%n_rcv_component
      fieldname(ifield) = trim(adjustl(ctx_field%name_field(            &
                                       ctx_io%i_rcv_component(ifield))))
    end do
    fieldname_main="data-record"
    !! -----------------------------------------------------------------
    
    allocate(wavefield(ctx_field%n_dofsol_loc,ctx_io%n_rcv_component))
    !! loop over all shots 
    do isrc=1,ctx_field%n_src
      !! careful with current aq source
      isrc_gb=index_first + (isrc-1)*index_step
      nrcv   =ctx_aq%sources(isrc)%rcv%nrcv
      if(nrcv > 0) then
        !! local wavefield from which to extract the data
        i1 = (isrc-1)*ctx_field%n_dofsol_loc+1
        i2 = i1 + ctx_field%n_dofsol_loc - 1
        wavefield = 0
        do ifield=1,ctx_io%n_rcv_component
          wavefield(:,ifield) = ctx_field%field(i1:i2,                  &
                                ctx_io%i_rcv_component(ifield))
        end do
        call io_filename(savename,savename_main,fieldname,              &
                         fieldname_main,ctx_io%n_rcv_component,         &
                         flag_freq,flag_mode,freq,mode,ctx_err,         &
                         o_isrc=isrc_gb)

        !! extract rcv data on all subdomain
        allocate(rcv_data(nrcv,ctx_io%n_rcv_component))
        rcv_data=0.0        
        call extract_rcv_data(ctx_disc,wavefield,ctx_io%n_rcv_component,&
                              nrcv,isrc,rcv_data,ctx_err)

        !! reduce so that only master keeps the result
        if(ctx_paral%master) then
          allocate(rcv_data_gb(nrcv,ctx_io%n_rcv_component))
        else
          allocate(rcv_data_gb(1,1))
        end if
        rcv_data_gb = 0.0
        !! force int for mpi
        call reduce_sum(rcv_data,rcv_data_gb,                           &
                        nrcv*ctx_io%n_rcv_component,0,                  &
                        ctx_paral%communicator,ctx_err)
        deallocate(rcv_data)

        !! master is going to save in ascii
        if(ctx_paral%master) then

          do ifield=1,ctx_io%n_rcv_component
            savename_final=trim(adjustl(ctx_io%workpath_rcv)) // "/" // &
                           trim(adjustl(fieldname_main))      // "_" // &
                           trim(adjustl(savename(ifield)))    //".txt"
            open (unit=500,file=trim(adjustl(savename_final)),status='replace')
            write(500,fmt=*) rcv_data_gb(:,ifield)
            close(500)
          end do
        end if
        deallocate(rcv_data_gb)
        call barrier(ctx_paral%communicator,ctx_err)
      end if
    end do
    !! -----------------------------------------------------------------
    deallocate(wavefield)
    deallocate(fieldname)
    deallocate(savename )

    return
  end subroutine datarcv_save
  
end module m_wavefield_save
