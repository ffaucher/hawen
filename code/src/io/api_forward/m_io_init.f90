!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_io_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module intializes the I/O information depending on the equation and 
!> type
!
!------------------------------------------------------------------------------
module m_io_init

  use m_raise_error,            only : t_error, raise_error
  use m_distribute_error,       only : distribute_error
  use m_ctx_parallelism,        only : t_parallelism
  use m_ctx_equation,           only : t_equation
  use m_ctx_io,                 only : t_io
  use m_print_io,               only : print_info_io
  use m_read_parameters_io,     only : read_parameters_io
  implicit none

  private
  public :: io_init

  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init I/O information
  !
  !> @param[in]    ctx_paral         : Parallelism context
  !> @param[in]    ctx_equation      : Equation context
  !> @param[inout] ctx_io            : IO context
  !> @param[in]    parameter_file    : parameter file
  !> @param[out]   ctx_err           : error context
  !----------------------------------------------------------------------------
  subroutine io_init(ctx_paral,ctx_equation,ctx_io,parameter_file,ctx_err)
    
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_equation)       ,intent(in)   :: ctx_equation
    type(t_io)             ,intent(inout):: ctx_io
    character(len=*)       ,intent(in)   :: parameter_file
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    integer                       :: isave,k
    character(len=16),allocatable :: wf_comp(:),rcv_comp(:)
    character(len=1024)           :: str
    logical                       :: flag_found
    
    ctx_err%ierr  = 0

    !! init context 
    ctx_io%workpath           = ''      ; ctx_io%workpath_model     = ''
    ctx_io%workpath_wave      = ''      ; ctx_io%workpath_rcv       = ''
    ctx_io%save_wavefield     = .false. ; ctx_io%save_receivers     = .false.
    ctx_io%n_wfd_component    = 0       ; ctx_io%n_rcv_component    = 0
    ctx_io%save_structured    = .false. ; ctx_io%save_unstructured  = .false.
    ctx_io%format_struct_vtk  = .false. ; ctx_io%format_struct_sep  = .false.
    ctx_io%format_unstruct_vtk= .false.


    !! read infos from parameter file ----------------------------------    
    !! workpath
    !! save_wavefield  ;  save_receivers
    !! n_wfd_component ;  n_rcv_component
    !! save_structured ;  save_unstructured
    !! dx,dy,dz
    !! format_struct_vtk; format_struct_sep; format_unstruct_vtk
    !!
    !! In addition, we read the names of the wavefield and receivers
    !! fields to be saved (if any)
    call read_parameters_io(ctx_paral,parameter_file,                   &
                      ctx_io%workpath,ctx_io%save_wavefield,            &
                      ctx_io%save_receivers,ctx_io%format_unstruct_vtk, &
                      ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                      ctx_io%n_wfd_component,wf_comp,                   &
                      ctx_io%n_rcv_component,rcv_comp,ctx_io%dx,        &
                      ctx_io%dy,ctx_io%dz,ctx_err)
    if(ctx_io%format_unstruct_vtk) ctx_io%save_unstructured = .true.  
    if(ctx_io%format_struct_vtk .or. ctx_io%format_struct_sep) &
                                   ctx_io%save_structured   = .true.
    !! -----------------------------------------------------------------
    !! create local workpath
    ctx_io%workpath_model = adjustl(trim(ctx_io%workpath)) // "/model"
    ctx_io%workpath_rcv   = adjustl(trim(ctx_io%workpath)) // "/receivers"
    ctx_io%workpath_wave  = adjustl(trim(ctx_io%workpath)) // "/wavefield"
    !! create subfolder for wavefields receivers and model infos
    if(ctx_paral%master) then
      !! synchronous version
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath))
      if(ctx_err%ierr == 0)                                             &
      call execute_command_line(trim(adjustl(str)),wait=.true.,         &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_model))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_wave))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_rcv))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)      
    endif
    !! distribute error 
    call distribute_error(ctx_err,ctx_paral%communicator)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! consistency check for wavefield and receivers fields
    !! -----------------------------------------------------------------
    if(ctx_io%save_wavefield) then
      allocate(ctx_io%i_wfd_component(ctx_io%n_wfd_component))
      do isave=1,ctx_io%n_wfd_component
        flag_found = .false.
        do k=1,ctx_equation%dim_solution
          if(wf_comp(isave) .eq. ctx_equation%name_sol(k) .and.         &
            (.not. flag_found)) then
            ctx_io%i_wfd_component(isave) = k
            flag_found                    =.true.
          end if
        end do
        if(.not. flag_found) then
          ctx_err%msg   = "** ERROR: Wavefield name "                // & 
                          trim(adjustl(wf_comp(isave))) // " cannot "// &
                          "be found for current equation [io_init] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
      end do
    end if
    !! ---------------------------
    if(ctx_io%save_receivers) then
      allocate(ctx_io%i_rcv_component(ctx_io%n_rcv_component))
      do isave=1,ctx_io%n_rcv_component
        flag_found = .false.
        do k=1,ctx_equation%dim_solution
          if(rcv_comp(isave) .eq. ctx_equation%name_sol(k) .and.        &
             (.not.flag_found)) then
            ctx_io%i_rcv_component(isave) = k
            flag_found                    =.true.
          end if
        end do
        if(.not. flag_found) then
          ctx_err%msg   = "** ERROR: Receivers field name "           //& 
                          trim(adjustl(rcv_comp(isave))) // " cannot "//&
                          "be found for current equation [io_init] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
      end do
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! print the informations ------------------------------------------
    call print_info_io(ctx_paral,ctx_io%workpath,                        &
               ctx_io%save_wavefield,ctx_io%save_receivers,              &
               ctx_io%n_wfd_component,ctx_io%n_rcv_component,            &
               ctx_io%format_unstruct_vtk,ctx_io%format_struct_vtk,      &
               ctx_io%format_struct_sep,wf_comp,rcv_comp,ctx_err)
    if(allocated(wf_comp )) deallocate(wf_comp )
    if(allocated(rcv_comp)) deallocate(rcv_comp)
    
    return
  end subroutine io_init

end module m_io_init
