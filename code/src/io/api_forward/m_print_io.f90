!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_io.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for I/O information
!
!------------------------------------------------------------------------------
module m_print_io

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  use m_barrier,          only : barrier
  implicit none
  
  private
  public :: print_info_io
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print information regarding what is saved and where
  !
  !> @param[in]    ctx_paral         : context parallelism
  !> @param[in]    workpath          : workpath where to save
  !> @param[in]    n_wf              : number of saved wavefield components
  !> @param[in]    n_rcv             : number of saved receiver components
  !> @param[in]    flag_unstruct_vtk : flag for unstructured VTK
  !> @param[in]    flag_struct_vtk   : flag for structured VTK
  !> @param[in]    flag_struct_sep   : flag for structured SEP
  !> @param[in]    wf_comp           : list of components
  !> @param[in]    rcv_comp          : list of components
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine print_info_io(ctx_paral,workpath,save_wavefield,           &
                           save_receivers,n_wf,n_rcv,flag_unstruct_vtk, &
                           flag_struct_vtk,flag_struct_sep,wf_comp,     &
                           rcv_comp,ctx_err)
  
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    character(len=*)            ,intent(in)   :: workpath
    integer                     ,intent(in)   :: n_wf,n_rcv
    logical                     ,intent(in)   :: save_wavefield
    logical                     ,intent(in)   :: save_receivers
    logical                     ,intent(in)   :: flag_unstruct_vtk
    logical                     ,intent(in)   :: flag_struct_vtk
    logical                     ,intent(in)   :: flag_struct_sep
    character(len=*),allocatable,intent(in)   :: wf_comp(:),rcv_comp(:)
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    integer            :: k
    character(len=512) :: str
    ctx_err%ierr  = 0

    !! print infos
    if(ctx_paral%master) then
      write(6,'(a)')    separator
      write(6,'(2a)')   indicator, " modeling I/O information "
      write(6,'(2a)') "- workpath where to save     : ",trim(adjustl(workpath))
      !! WAVEFIELDS ----------------------------------------------------
      if(save_wavefield) then
        write(6,'(a,i4,a)') "- save ",n_wf," wavefield components:"
        str="    - "// trim(adjustl(wf_comp(1)))
        do k=2, n_wf
          str = trim(str) // ", "// trim(adjustl(wf_comp(k)))
        enddo
        write(6,'(a)') trim(str)
        !! format
        if(flag_unstruct_vtk) &
          write(6,'(a)') "    - using unstructured VTK format"
        if(flag_struct_vtk) write(6,'(a)') "    - using structured VTK format"
        if(flag_struct_sep) write(6,'(a)') "    - using structured SEP format"
      else
        write(6,'(a)') "- wavefield is NOT saved "
      end if
      !! RECEIVERS -----------------------------------------------------
      if(save_receivers) then
        write(6,'(a,i4,a)') "- save ",n_rcv," receivers components:"
        str="    - "// trim(adjustl(rcv_comp(1)))
        do k=2, n_rcv
          str = trim(str) // ", "// trim(adjustl(rcv_comp(k)))
        enddo
        write(6,'(a)') trim(str)
      else
        write(6,'(a)') "- receivers data are NOT saved "
      end if
    end if
    return
    
  end subroutine print_info_io

end module m_print_io
