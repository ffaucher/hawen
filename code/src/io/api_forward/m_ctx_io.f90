!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_io.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the I/O informations for the wave modeling: 
!> the components (full wavefields and/or receivers informations) 
!> that are saved 
!
!------------------------------------------------------------------------------
module m_ctx_io
    
  implicit none
  private
  public :: t_io, io_clean
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_io_modeling contains info on the modeling procedure
  !
  !> utils:
  !> @param[string]  workpath            : output workpath
  !> @param[logical] save_wavefield      : flag to save wavefields
  !> @param[logical] save_receivers      : flag to save receivers
  !> @param[integer] n_wfd_component     : number of component to save for WF
  !> @param[integer] n_rcv_component     : number of component to save for RCV
  !> @param[integer] i_wfd_component(:)  : index solution for WF component
  !> @param[integer] i_rcv_component(:)  : index solution for RCV component
  !> @param[logical] save_structured     : save structured grid
  !> @param[logical] save_unstructured   : save unstructured grid
  !> @param[logical] format_struct_vtk   : format structured vtk
  !> @param[logical] format_struct_sep   : format structured sep
  !> @param[logical] format_unstruct_vtk : format unstructured vtk
  !----------------------------------------------------------------------------
  type t_io

    !! workpath
    character(len=512)       :: workpath
    character(len=512)       :: workpath_model
    character(len=512)       :: workpath_wave
    character(len=512)       :: workpath_rcv
    !! indicate if we save the wavefield and/or receivers informations
    logical                  :: save_wavefield
    logical                  :: save_receivers
    !! associated components
    integer                  :: n_wfd_component
    integer                  :: n_rcv_component
    integer     ,allocatable :: i_wfd_component(:) 
    integer     ,allocatable :: i_rcv_component(:)
    !! format for save
    logical                  :: save_structured
    real                     :: dx,dy,dz !! only for structured save
    logical                  :: save_unstructured
    logical                  :: format_struct_vtk
    logical                  :: format_struct_sep
    logical                  :: format_unstruct_vtk
  end type t_io
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean io information
  !
  !> @param[inout] ctx_io         : IO    context
  !----------------------------------------------------------------------------
  subroutine io_clean(ctx_io)
    type(t_io)             ,intent(inout):: ctx_io
    
    
    ctx_io%workpath           = ''
    ctx_io%workpath_model     = ''
    ctx_io%workpath_wave      = ''
    ctx_io%workpath_rcv       = ''
    ctx_io%save_wavefield     = .false.
    ctx_io%save_receivers     = .false.
    ctx_io%n_wfd_component    = 0
    ctx_io%n_rcv_component    = 0
    ctx_io%save_structured    = .false.
    ctx_io%save_unstructured  = .false.
    ctx_io%format_struct_vtk  = .false.
    ctx_io%format_struct_sep  = .false.
    ctx_io%format_unstruct_vtk= .false.
    if(allocated(ctx_io%i_wfd_component)) deallocate(ctx_io%i_wfd_component) 
    if(allocated(ctx_io%i_rcv_component)) deallocate(ctx_io%i_rcv_component)
    ctx_io%dx                 = 0.
    ctx_io%dy                 = 0.
    ctx_io%dz                 = 0.

    return
  end subroutine io_clean  

end module m_ctx_io
