!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_io.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read parameters that are needed for IO infos
!
!------------------------------------------------------------------------------
module m_read_parameters_io
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  use m_ctx_parallelism, only: t_parallelism
  use m_barrier,         only: barrier
  use m_tag_namefield,   only : field_tag_formalism
  !! -------------------------------------------------------------------

  implicit none
  private
  public :: read_parameters_io

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read all parameters that are required for IO informations for modeling
  !
  !> @param[in]   ctx_paral        : parallel context
  !> @param[in]   parameter_file   : parameter file where infos are
  !> @param[out]  workpath         : where everything is saved
  !> @param[out]  flag_wf          : flag if wavefields are saved
  !> @param[out]  flag_rcv         : flag if receivers info are saved
  !> @param[out]  flag_unstruct_vtk: flag for unstructured vtk format
  !> @param[out]  flag_struct_vtk  : flag for structured vtk format
  !> @param[out]  flag_struct_sep  : flag for structured sep format
  !> @param[out]  wf_ncomp         : number of wavefield compoenent to save
  !> @param[out]  wf_comp          : list of component names (vx,vz,ux,...)
  !> @param[out]  rcv_ncomp        : number of receivers compoenent to save
  !> @param[out]  rcv_comp         : list of component names (vx,vz,ux,...)
  !> @param[out]  d{x,y,z}         : discretization for cartesian save
  !> @param[out]  ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_io(ctx_paral,parameter_file,workpath,      &
                                flag_wf,flag_rcv,flag_unstruct_vtk,     &
                                flag_struct_vtk,flag_struct_sep,        &
                                wf_ncomp,wf_comp,rcv_ncomp,rcv_comp,    &
                                dx,dy,dz,ctx_err)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    character(len=*)              ,intent(in)   :: parameter_file
    character(len=*)              ,intent(inout):: workpath
    logical                       ,intent(out)  :: flag_wf,flag_rcv
    logical                       ,intent(out)  :: flag_unstruct_vtk
    logical                       ,intent(out)  :: flag_struct_vtk
    logical                       ,intent(out)  :: flag_struct_sep
    integer                       ,intent(out)  :: wf_ncomp
    character(len=*), allocatable ,intent(inout):: wf_comp(:)
    integer                       ,intent(out)  :: rcv_ncomp
    character(len=*), allocatable ,intent(inout):: rcv_comp(:)
    real                          ,intent(out)  :: dx,dy,dz
    type(t_error)                 ,intent(out)  :: ctx_err
    !! local
    integer            :: nval
    real               :: real_list     (512)
    logical            :: buffer_logical(512)
    character(len=512) :: string_list   (512)
    character(len=512),allocatable :: str(:)
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! Reading the equation --------------------------------------------
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'workpath'      ,'',workpath ,nval)
    call read_parameter(parameter_file,'save_wavefield',.false., &
                        buffer_logical,nval)
    flag_wf = buffer_logical(1)
    call read_parameter(parameter_file,'save_receivers',.false., &
                        buffer_logical,nval)
    flag_rcv= buffer_logical(1)
    
    !! format save
    !! -----------    
    flag_unstruct_vtk  = .false.
    flag_struct_vtk    = .false.
    flag_struct_sep    = .false.
    dx = 0. ; dy=0. ; dz=0.
    wf_ncomp =0
    rcv_ncomp=0

    !! wavefield infos
    if(flag_wf) then
      call read_parameter(parameter_file,'save_structured_vtk',.false., &
                          buffer_logical,nval)
      flag_struct_vtk= buffer_logical(1)
          
      call read_parameter(parameter_file,'save_structured_sep',.false., &
                          buffer_logical,nval)
      flag_struct_sep= buffer_logical(1)
      call read_parameter(parameter_file,'save_unstructured_vtk',.false.,&
                          buffer_logical,nval)
      flag_unstruct_vtk= buffer_logical(1)
      
      !! for structured we need the discretization step
      if(flag_struct_vtk .or. flag_struct_sep) then
        call read_parameter(parameter_file,'dx',1.,real_list,nval)
        dx = real_list(1)
        call read_parameter(parameter_file,'dy',1.,real_list,nval)
        dy = real_list(1)
        call read_parameter(parameter_file,'dz',1.,real_list,nval)
        dz = real_list(1)
      end if
    
      !! list of components to be saved: formalize names, 
      !! consitency check with equation is done later
      call read_parameter(parameter_file,'save_wavefield_component','', &
                          string_list,nval)
      if(nval > 0) then
        wf_ncomp = nval
        allocate(wf_comp(wf_ncomp))
        allocate(str    (wf_ncomp))
        str  = string_list(1:wf_ncomp)
        !! name formalism
        call field_tag_formalism(wf_ncomp,str,wf_comp,ctx_err)
        deallocate(str)
      else
        ctx_err%msg   = "** ERROR: cannot save wavefield when no components " // &
                        "are given [read_param_io] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    !! -----------------------------------------------------------------
    !! same with receivers components
    if(flag_rcv) then
      !! list of components to be saved: consitency check is done later
      call read_parameter(parameter_file,'save_receivers_component','', &
                          string_list,nval)
      if(nval > 0) then
        rcv_ncomp = nval
        allocate(rcv_comp(rcv_ncomp))
        allocate(str     (rcv_ncomp))
        str  = string_list(1:rcv_ncomp)
        !! name formalism
        
        call field_tag_formalism(rcv_ncomp,str,rcv_comp,ctx_err)
        deallocate(str)
      else
        ctx_err%msg   = "** ERROR: cannot save receivers when no components " // &
                        "are given [read_param_io] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end if
    
    !! -----------------------------------------------------------------
    !! mpi_barrier waits for every processors
    call barrier(ctx_paral%communicator,ctx_err)

    return
  end subroutine read_parameters_io

end module m_read_parameters_io
