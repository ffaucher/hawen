!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_io_init.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module intializes the I/O information depending on the equation and 
!> type
!
!------------------------------------------------------------------------------
module m_io_init

  use m_raise_error,            only : t_error, raise_error
  use m_distribute_error,       only : distribute_error
  use m_ctx_parallelism,        only : t_parallelism
  use m_ctx_equation,           only : t_equation
  use m_ctx_io,                 only : t_io
  use m_print_io,               only : print_info_io
  use m_read_parameters_io,     only : read_parameters_io
  implicit none

  private
  public :: io_init

  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> init I/O information
  !
  !> @param[in]    ctx_paral         : Parallelism context
  !> @param[in]    ctx_equation      : Equation context
  !> @param[inout] ctx_io            : IO context
  !> @param[in]    parameter_file    : parameter file
  !> @param[out]   ctx_err           : error context
  !----------------------------------------------------------------------------
  subroutine io_init(ctx_paral,ctx_equation,ctx_io,parameter_file,ctx_err)
    
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_equation)       ,intent(in)   :: ctx_equation
    type(t_io)             ,intent(inout):: ctx_io
    character(len=*)       ,intent(in)   :: parameter_file
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    character(len=1024)           :: str
    
    ctx_err%ierr  = 0
    if( ctx_equation%dim_solution < 1) then
      ctx_err%msg   = "** ERROR: EQ DIMENSION ERROR [io_init] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! init context 
    ctx_io%workpath            = ''
    ctx_io%workpath_model      = ''
    ctx_io%workpath_grad       = ''
    ctx_io%workpath_search     = ''
    ctx_io%workpath_linesearch = ''
    ctx_io%workpath_residual   = ''
    ctx_io%workpath_source     = ''

    ctx_io%save_structured    = .false. ; ctx_io%save_unstructured  = .false.
    ctx_io%format_struct_vtk  = .false. ; ctx_io%format_struct_sep  = .false.
    ctx_io%format_unstruct_vtk= .false.


    !! read infos from parameter file ----------------------------------    
    !! workpath
    !! save_structured ;  save_unstructured
    !! dx,dy,dz
    !! format_struct_vtk; format_struct_sep; format_unstruct_vtk
    !!
    call read_parameters_io(parameter_file,ctx_io%workpath,             &
                            ctx_io%format_unstruct_vtk,                 &
                            ctx_io%format_struct_vtk,                   &
                            ctx_io%format_struct_sep,ctx_io%dx,         &
                            ctx_io%dy,ctx_io%dz,ctx_io%clean_final,     &
                            ctx_err)
    if(ctx_io%format_unstruct_vtk) ctx_io%save_unstructured = .true.  
    if(ctx_io%format_struct_vtk .or. ctx_io%format_struct_sep) &
                                   ctx_io%save_structured   = .true.
    !! -----------------------------------------------------------------
    !! create local workpath
    ctx_io%workpath_model      = adjustl(trim(ctx_io%workpath)) // "/model"
    ctx_io%workpath_grad       = adjustl(trim(ctx_io%workpath)) // "/gradient"
    ctx_io%workpath_search     = adjustl(trim(ctx_io%workpath)) // "/search-direction"
    ctx_io%workpath_linesearch = adjustl(trim(ctx_io%workpath)) // "/linesearch"
    ctx_io%workpath_residual   = adjustl(trim(ctx_io%workpath)) // "/residual"
    ctx_io%workpath_source     = adjustl(trim(ctx_io%workpath)) // "/source-value"
    !! create subfolders
    if(ctx_paral%master) then
      !! synchronous version
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath))
      if(ctx_err%ierr == 0)                                             &
      call execute_command_line(trim(adjustl(str)),wait=.true.,         &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_model))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_grad))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      
      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_search))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)

      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_linesearch))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)    

      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_residual))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)    

      str = 'mkdir -p ' //adjustl(trim(ctx_io%workpath_source))
      if(ctx_err%ierr == 0)                                             &
        call execute_command_line(trim(adjustl(str)),wait=.true.,       &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)       
    endif
    !! distribute error 
    call distribute_error(ctx_err,ctx_paral%communicator)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! print the informations ------------------------------------------
    call print_info_io(ctx_paral,ctx_io%workpath,                       &
                       ctx_io%format_unstruct_vtk,                      &
                       ctx_io%format_struct_vtk,                        &
                       ctx_io%format_struct_sep,ctx_err)   
    return
  end subroutine io_init

end module m_io_init
