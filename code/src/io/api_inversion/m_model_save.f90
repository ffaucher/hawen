!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_model_save.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to save models
!
!------------------------------------------------------------------------------
module m_model_save

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH
  use m_distribute_error,       only: distribute_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_reduce_sum,             only: reduce_sum
  use m_ctx_equation,           only: t_equation
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_ctx_model,              only: t_model
  use m_model_eval,             only: model_get_piecewise_constant,     &
                                      model_get_piecewise_polynomial,   &
                                      model_get_pdof
  use m_ctx_io,                 only: t_io
  use m_array_save,             only: array_save
  use m_io_filename,            only: io_filename
  use m_project_array,          only: project_array_local_cell_to_global
  use m_array_save_binary,      only: array_save_binary
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,            &
                                        tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_DOF
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: model_save

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save selected models for current iteration
  !
  !> @param[in]    ctx_paral           : type model
  !> @param[in]    ctx_equation        : context equation
  !> @param[in]    ctx_domain          : context domain
  !> @param[in]    ctx_discretization  : context discretization
  !> @param[in]    ctx_model           : context model
  !> @param[in]    ctx_io              : context io
  !> @param[in]    nmodel              : number of models
  !> @param[in]    name_model(:)       : model names
  !> @param[in]    iteration           : current iteration
  !> @param[in]    model_frequency     : frequency to compute the models
  !> @param[inout] ctx_err             : context error
  !> @param[in]    o_flag_param_grad   : indicates the gradient parametrization
  !> @param[in]    o_frequency         : indicates if frequency key 
  !> @param[in]    o_mode              : indicates if mode key 
  !----------------------------------------------------------------------------
  subroutine model_save(ctx_paral,ctx_equation,ctx_domain,              &
                        ctx_discretization,ctx_model,ctx_io,nmodel,     &
                        name_model,iteration,model_frequency,           &
                        ctx_err,o_flag_param_grad,o_frequency,o_mode)
    implicit none
    type(t_parallelism)          ,intent(in)   :: ctx_paral
    type(t_equation)             ,intent(in)   :: ctx_equation
    type(t_domain)               ,intent(in)   :: ctx_domain
    type(t_discretization)       ,intent(in)   :: ctx_discretization
    type(t_model)                ,intent(in)   :: ctx_model
    type(t_io)                   ,intent(in)   :: ctx_io
    integer                      ,intent(in)   :: nmodel
    character(len=*) ,allocatable,intent(in)   :: name_model(:)
    integer                      ,intent(in)   :: iteration
    complex(kind=8)              ,intent(in)   :: model_frequency
    type(t_error)                ,intent(inout):: ctx_err
    logical          ,optional   ,intent(in)   :: o_flag_param_grad
    real             ,optional   ,intent(in)   :: o_frequency(:)
    integer          ,optional   ,intent(in)   :: o_mode
    !! local
    integer                        :: imodel,cste_order
    real,allocatable               :: model(:,:),model_loc(:)
    character(len=128)             :: name_loc,refname
    character(len=128),allocatable :: fieldname(:),outname(:)
    character(len=128)             :: outname_gb
    real(kind=4)      ,allocatable :: model_loc_gb(:),model_gb(:)
    !! to generate the appropriate file name
    real   (kind=8)                :: freq(2)
    integer(kind=4)                :: mode
    logical                        :: flag_mode
    logical                        :: flag_freq
    !! for dof representation
    integer(kind=IKIND_MESH)       :: i_cell, igb1, igb2, iloc1, iloc2
    integer(kind=4)                :: ndof_gb, npt_per_cell, ndof_sum
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    refname = "model"
    if(present(o_flag_param_grad)) then
      if(o_flag_param_grad) then
        refname = "model-param-grad"
      end if
    end if

    flag_mode = .false. 
    flag_freq = .false. 
    if(present(o_frequency) .and. ctx_equation%flag_freq) then
      flag_freq = .true.
      freq = dble(o_frequency)
    end if
    if(present(o_mode)      .and. ctx_equation%flag_mode) then 
      flag_mode = .true.
      mode      = o_mode
    end if

    allocate(fieldname(nmodel))
    allocate(outname  (nmodel))
    !! get the filenames first
    do imodel=1,nmodel
      fieldname(imodel)=trim(adjustl(name_model(imodel)))
    end do

    !! -----------------------------------------------------------------
    !! extract model depends if we have a piecewise-constant or polynomial
    !!
    select case(trim(adjustl(ctx_model%format_disc)))

      case(tag_MODEL_PCONSTANT,tag_MODEL_SEP) ! >>>>>>>>>>>>>>>>>>>>>>>>

        !! evaluate the model per cell.
        ndof_gb    = int(ctx_domain%n_cell_loc,kind=4)
        allocate(model_loc(ndof_gb))
        allocate(model    (ndof_gb,nmodel))
        do imodel=1,nmodel
          name_loc =trim(adjustl(name_model(imodel)))
          model_loc= 0.0
          !! eval piecewise constant per cell: 
          !! WE SAVE REAL 4 HERE, BECAUSE OF MODEL_LOC, WE CAN JUST 
          !! CHANGE MODEL_LOC TO REAL 8 TO SAVE IN DOUBLE.
          !! ***********************************************************       
          call model_get_piecewise_constant(ctx_model,model_loc,        &
                                            trim(adjustl(name_loc)),    &
                                            model_frequency,ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator) 
          model(:,imodel)  =model_loc
          fieldname(imodel)=trim(adjustl(name_loc))
        end do
        deallocate(model_loc)
        ! ---------------------------------------------------------

        ! ----------------------------------------------------------------
        !! step 2. now that we have all the models, we can save them
        !!         depending on what we want
        ! ----------------------------------------------------------------
        !!
        !! a) frequency model >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !!    
        outname=''
        if(present(o_frequency)) then
          call io_filename(outname,outname_gb,fieldname,                &
                           trim(adjustl(refname)),nmodel,flag_freq,     &
                           flag_mode,freq,mode,ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator)
          !! save piecewise-constant here.
          call array_save(ctx_paral,ctx_domain,ctx_discretization,          &
                          ctx_io%save_unstructured,ctx_io%save_structured,  &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          model,nmodel,'cell',ctx_io%workpath_model,outname,&
                          outname_gb,ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator) 
        end if

        !!
        !! b) iteration models: always >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !!
        outname=''
        !! only works with the iterations here 
        flag_mode = .false. 
        flag_freq = .false. 
        call io_filename(outname,outname_gb,fieldname,                  &
                         trim(adjustl(refname)),nmodel,flag_freq,       &
                         flag_mode,freq,mode,ctx_err,o_iter=iteration)
        call distribute_error(ctx_err,ctx_paral%communicator) 
        
        !! save piecewise-constant here.
        call array_save(ctx_paral,ctx_domain,ctx_discretization,          &
                        ctx_io%save_unstructured,ctx_io%save_structured,  &
                        ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                        model,nmodel,'cell',ctx_io%workpath_model,outname,&
                        outname_gb,ctx_err)

        !!
        !! c) binary files >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !! save a binary file with all model values, that can be re-used 
        !! for the search direction (i.e. l-bfgs)
        !!
        if(ctx_io%flag_bin_model) then
          !! the array model already contains the local informations
          allocate(model_loc_gb(ctx_domain%n_cell_gb))
          if(ctx_paral%master) then
            allocate(model_gb(ctx_domain%n_cell_gb))
          else
            allocate(model_gb(1))
          end if
          !!
          model_loc_gb = 0.0 ; model_gb=0.0
          do imodel = 1, nmodel
            call project_array_local_cell_to_global(ctx_domain,         &
                                     model(:,imodel),model_loc_gb,ctx_err)
            !! master saves in binary now
            call reduce_sum(model_loc_gb,model_gb,                       &
                            ctx_domain%n_cell_gb,0,                      &
                            ctx_paral%communicator,ctx_err)
            call array_save_binary(ctx_paral,model_gb,                   &
                                   ctx_domain%n_cell_gb,                 &
                                   ctx_io%workpath_model,outname(imodel),&
                                   ctx_err)
          end do
          deallocate(model_loc_gb)
          deallocate(model_gb)
        end if
        !! -------------------------------------------------------------
        deallocate(model)
        !! -------------------------------------------------------------

      case (tag_MODEL_DOF)
        !! -------------------------------------------------------------
        !! for model per dof.
        !! -------------------------------------------------------------
        do imodel=1,nmodel
          name_loc =trim(adjustl(name_model(imodel)))
          !! eval model: 
          !! WE SAVE REAL 4 HERE, BECAUSE OF MODEL_LOC, WE CAN JUST 
          !! CHANGE MODEL_LOC TO REAL 8 TO SAVE IN DOUBLE.
          !! ***********************************************************   
          call model_get_pdof(ctx_domain,ctx_model,model_loc,           &
                              name_loc,model_frequency,cste_order,      &
                              ndof_gb,ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator) 
          if(imodel == 1) then
            allocate(model(ndof_gb,nmodel))
          end if
  
          model(:,imodel)  =model_loc
          fieldname(imodel)=trim(adjustl(name_loc))
          deallocate(model_loc)
        end do

        ! ----------------------------------------------------------------
        !! step 2. now that we have all the models, we can save them
        !!         depending on what we want
        ! ----------------------------------------------------------------
        !!
        !! a) frequency model >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !!    
        outname=''
        if(present(o_frequency)) then
          call io_filename(outname,outname_gb,fieldname,                &
                           trim(adjustl(refname)),nmodel,flag_freq,     &
                           flag_mode,freq,mode,ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator)
          !! save here.
          call array_save(ctx_paral,ctx_domain,ctx_discretization,          &
                          ctx_io%save_unstructured,ctx_io%save_structured,  &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          model,nmodel,'dof-orderfix',ctx_io%workpath_model,&
                          outname,outname_gb,ctx_err,o_cste_order=cste_order)
          call distribute_error(ctx_err,ctx_paral%communicator)
        end if

        !!
        !! b) iteration models: always >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !!
        outname=''
        !! only works with the iterations here 
        flag_mode = .false. 
        flag_freq = .false. 
        call io_filename(outname,outname_gb,fieldname,                  &
                         trim(adjustl(refname)),nmodel,flag_freq,       &
                         flag_mode,freq,mode,ctx_err,o_iter=iteration)
        call distribute_error(ctx_err,ctx_paral%communicator) 
        
        !! save here.
        call array_save(ctx_paral,ctx_domain,ctx_discretization,          &
                        ctx_io%save_unstructured,ctx_io%save_structured,  &
                        ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                        model,nmodel,'dof-orderfix',ctx_io%workpath_model,&
                        outname,outname_gb,ctx_err,o_cste_order=cste_order)
        !!
        !! c) binary files >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !! save a binary file with all model values, that can be re-used 
        !! for the search direction (i.e. l-bfgs)
        !!
        if(ctx_io%flag_bin_model) then
          !! we uses the npt_per_cell.
          npt_per_cell = ndof_gb / ctx_domain%n_cell_loc
          ndof_sum     = npt_per_cell * ctx_domain%n_cell_gb
          if(npt_per_cell*ctx_domain%n_cell_loc .ne. ndof_gb) then
            ctx_err%msg   = "** ERROR: Saving the model with " //       &
                            "multiple constant requires the same " //   &
                            "number of pt per cell [model_save] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
          
          !! allocation
          allocate(model_loc_gb(ndof_sum))
          if(ctx_paral%master) then
            allocate(model_gb(ndof_sum))
          else
            allocate(model_gb(1))
          end if
          
          !! assemble
          do imodel = 1, nmodel
            model_loc_gb = 0.0 ; model_gb=0.0
            
            do i_cell = 1, ctx_domain%n_cell_loc
              igb1 = (ctx_domain%index_loctoglob_cell(i_cell)-1)        &
                    * npt_per_cell + 1
              igb2 = igb1 + npt_per_cell - 1
              
              iloc1= (i_cell-1) * npt_per_cell + 1
              iloc2=   iloc1    + npt_per_cell - 1
              model_loc_gb(igb1:igb2) = model(iloc1:iloc2,imodel)
            end do

            !! master saves in binary now
            call reduce_sum(model_loc_gb,model_gb,ndof_sum,0,           &
                            ctx_paral%communicator,ctx_err)
            call array_save_binary(ctx_paral,model_gb,ndof_sum,         &
                                   ctx_io%workpath_model,               &
                                   outname(imodel),ctx_err)
          end do
          
          deallocate(model_loc_gb)
          deallocate(model_gb)
        end if
        !! -------------------------------------------------------------
        deallocate(model)
        !! -------------------------------------------------------------

      case (tag_MODEL_PPOLY)
        !! -------------------------------------------------------------
        !! for piecwise-polynomial, we must separate if we also save
        !! in structured representation or not. 
        !! -------------------------------------------------------------
        ctx_err%msg   = "** ERROR: we cannot save the "//               &
                        trim(adjustl(ctx_model%format_disc))//          &
                        " model representation [model_save] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
        
      case default
        ctx_err%msg   = "** ERROR: we cannot save the "//               &
                        trim(adjustl(ctx_model%format_disc))//          &
                        " model representation [model_save] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
    end select
    !! -----------------------------------------------------------------
    deallocate(fieldname)
    deallocate(outname)
    
    return
  end subroutine model_save
 
end module m_model_save
