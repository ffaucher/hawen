!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_io_delete.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module delete the outputs at the end that are less important:
!> gradient, serach-direction, model-iteration
!
!------------------------------------------------------------------------------
module m_io_delete

  use m_raise_error,            only : t_error, raise_error
  use m_distribute_error,       only : distribute_error
  use m_ctx_parallelism,        only : t_parallelism
  use m_ctx_io,                 only : t_io
  
  implicit none

  private
  public :: io_delete

  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> remove temporary informations at the end
  !
  !> @param[in]    ctx_paral         : Parallelism context
  !> @param[in]    ctx_io            : IO context
  !> @param[out]   ctx_err           : error context
  !----------------------------------------------------------------------------
  subroutine io_delete(ctx_paral,ctx_io,ctx_err)
    
    type(t_parallelism)    ,intent(in)   :: ctx_paral
    type(t_io)             ,intent(inout):: ctx_io
    type(t_error)          ,intent(out)  :: ctx_err
    !! local
    character(len=1024)           :: str
    
    ctx_err%ierr  = 0
    !! -----------------------------------------------------------------
    if(ctx_paral%master) then
      !! remove depending on the level of clean 
      if(ctx_io%clean_final .ne. 0) then
        str = 'rm -rf '//adjustl(trim(ctx_io%workpath_model ))//'/*.bin'
        if(ctx_err%ierr == 0)                                           &
          call execute_command_line(trim(adjustl(str)),wait=.true.,     &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
        
        str = 'rm -rf '//adjustl(trim(ctx_io%workpath_grad  ))//'/*'
        if(ctx_err%ierr == 0)                                           &
          call execute_command_line(trim(adjustl(str)),wait=.true.,     &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)

        str = 'rm -rf '//adjustl(trim(ctx_io%workpath_search))//'/*'
        if(ctx_err%ierr == 0)                                           &
          call execute_command_line(trim(adjustl(str)),wait=.true.,     &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      end if

      !! we also remove iteration models
      if(ctx_io%clean_final > 1) then
        str='rm -rf '//adjustl(trim(ctx_io%workpath_model ))//'/*iteration*'
        if(ctx_err%ierr == 0)                                           &
          call execute_command_line(trim(adjustl(str)),wait=.true.,     &
                                cmdstat=ctx_err%ierr,cmdmsg=ctx_err%msg)
      end if
    end if
    !! distribute error 
    call distribute_error(ctx_err,ctx_paral%communicator)
    !! -----------------------------------------------------------------
    
    return
  end subroutine io_delete

end module m_io_delete
