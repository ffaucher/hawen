!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_io.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for I/O information
!
!------------------------------------------------------------------------------
module m_print_io

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  use m_barrier,          only : barrier
  implicit none
  
  private
  public :: print_info_io
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print information regarding what is saved and where
  !
  !> @param[in]    ctx_paral         : context parallelism
  !> @param[in]    workpath          : workpath where to save
  !> @param[in]    flag_unstruct_vtk : flag for unstructured VTK
  !> @param[in]    flag_struct_vtk   : flag for structured VTK
  !> @param[in]    flag_struct_sep   : flag for structured SEP
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine print_info_io(ctx_paral,workpath,flag_unstruct_vtk,        &
                           flag_struct_vtk,flag_struct_sep,ctx_err)
  
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    character(len=*)            ,intent(in)   :: workpath
    logical                     ,intent(in)   :: flag_unstruct_vtk
    logical                     ,intent(in)   :: flag_struct_vtk
    logical                     ,intent(in)   :: flag_struct_sep
    type(t_error)               ,intent(inout):: ctx_err

    ctx_err%ierr  = 0

    !! print infos
    if(ctx_paral%master) then
      write(6,'(a)')    separator
      write(6,'(2a)')   indicator, " inversion I/O information "
      write(6,'(2a)') "- workpath where to save     : ",trim(adjustl(workpath))
      !! format
      if(flag_unstruct_vtk) write(6,'(a)') "    - using unstructured VTK format"
      if(flag_struct_vtk)   write(6,'(a)') "    - using structured VTK format"
      if(flag_struct_sep)   write(6,'(a)') "    - using structured SEP format"
    end if
    return
    
  end subroutine print_info_io

end module m_print_io
