!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_io.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read parameters that are needed for IO infos
!
!------------------------------------------------------------------------------
module m_read_parameters_io
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------

  implicit none
  private
  public :: read_parameters_io

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read all parameters that are required for IO informations for inversion
  !
  !> @param[in]   parameter_file   : parameter file where infos are
  !> @param[out]  workpath         : where everything is saved
  !> @param[out]  flag_unstruct_vtk: flag for unstructured vtk format
  !> @param[out]  flag_struct_vtk  : flag for structured vtk format
  !> @param[out]  flag_struct_sep  : flag for structured sep format
  !> @param[out]  d{x,y,z}         : discretization for cartesian save
  !> @param[out]  clean_final      : indicates if we remove dumped arrays
  !> @param[out]  ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_io(parameter_file,workpath,flag_unstruct_vtk, &
                                flag_struct_vtk,flag_struct_sep,dx,dy,dz,  &
                                clean_final,ctx_err)

    character(len=*)              ,intent(in)   :: parameter_file
    character(len=*)              ,intent(inout):: workpath
    logical                       ,intent(out)  :: flag_unstruct_vtk
    logical                       ,intent(out)  :: flag_struct_vtk
    logical                       ,intent(out)  :: flag_struct_sep
    real                          ,intent(out)  :: dx,dy,dz
    integer                       ,intent(out)  :: clean_final
    type(t_error)                 ,intent(out)  :: ctx_err
    !! local
    integer            :: nval
    real               :: real_list     (512)
    integer            :: int_list      (512)
    logical            :: buffer_logical(512)
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! Reading the equation --------------------------------------------
    !! -----------------------------------------------------------------
    call read_parameter(parameter_file,'fwi_output_clean',0,int_list,nval)
    clean_final = int_list(1)
    call read_parameter(parameter_file,'workpath'      ,'',workpath ,nval)
    
    !! format save
    !! -----------    
    flag_unstruct_vtk  = .false.
    flag_struct_vtk    = .false.
    flag_struct_sep    = .false.
    dx = 0. ; dy=0. ; dz=0.

    call read_parameter(parameter_file,'save_structured_vtk',.false., &
                        buffer_logical,nval)
    flag_struct_vtk= buffer_logical(1)
          
    call read_parameter(parameter_file,'save_structured_sep',.false., &
                        buffer_logical,nval)
    flag_struct_sep= buffer_logical(1)
    call read_parameter(parameter_file,'save_unstructured_vtk',.false.,&
                        buffer_logical,nval)
    flag_unstruct_vtk= buffer_logical(1)
      
    !! for structured we need the discretization step
    if(flag_struct_vtk .or. flag_struct_sep) then
      call read_parameter(parameter_file,'dx',1.,real_list,nval)
      dx = real_list(1)
      call read_parameter(parameter_file,'dy',1.,real_list,nval)
      dy = real_list(1)
      call read_parameter(parameter_file,'dz',1.,real_list,nval)
      dz = real_list(1)
    end if
    
    return
  end subroutine read_parameters_io

end module m_read_parameters_io
