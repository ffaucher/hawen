!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_io.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines the I/O informations for the wave inversion: 
!
!------------------------------------------------------------------------------
module m_ctx_io
    
  implicit none
  private
  public :: t_io, io_clean
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_io contains info on the inverse procedure
  !
  !> utils:
  !> @param[string]  workpath            : output workpath
  !> @param[logical] save_structured     : save structured grid
  !> @param[logical] save_unstructured   : save unstructured grid
  !> @param[logical] format_struct_vtk   : format structured vtk
  !> @param[logical] format_struct_sep   : format structured sep
  !> @param[logical] format_unstruct_vtk : format unstructured vtk
  !> @param[integer] clean_final         : delete outputs
  !----------------------------------------------------------------------------
  type t_io

    !! workpath
    character(len=512)       :: workpath
    character(len=512)       :: workpath_model
    character(len=512)       :: workpath_grad
    character(len=512)       :: workpath_search
    character(len=512)       :: workpath_linesearch
    character(len=512)       :: workpath_residual
    character(len=512)       :: workpath_source
    !! format for save
    logical                  :: save_structured
    real                     :: dx,dy,dz !! only for structured save
    logical                  :: save_unstructured
    logical                  :: format_struct_vtk
    logical                  :: format_struct_sep
    logical                  :: format_unstruct_vtk
    !! for search direction method, need to save privous iteration infos
    logical                  :: flag_bin_search = .false.
    logical                  :: flag_bin_grad   = .false.
    logical                  :: flag_bin_model  = .false.
    !! indicates if we clean fwi array that have been dumped at the end
    integer                  :: clean_final
  end type t_io
  
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean io information
  !
  !> @param[inout] ctx_io         : IO    context
  !----------------------------------------------------------------------------
  subroutine io_clean(ctx_io)
    type(t_io)             ,intent(inout):: ctx_io
    
    
    ctx_io%workpath           = ''
    ctx_io%workpath_model     = ''
    ctx_io%workpath_grad      = ''
    ctx_io%workpath_search    = ''
    ctx_io%workpath_linesearch= ''
    ctx_io%workpath_residual  = ''
    ctx_io%workpath_source    = ''
    ctx_io%save_structured    = .false.
    ctx_io%save_unstructured  = .false.
    ctx_io%format_struct_vtk  = .false.
    ctx_io%format_struct_sep  = .false.
    ctx_io%format_unstruct_vtk= .false.
    ctx_io%flag_bin_search    = .false.
    ctx_io%flag_bin_grad      = .false.
    ctx_io%flag_bin_model     = .false.
    ctx_io%dx                 = 0.
    ctx_io%dy                 = 0.
    ctx_io%dz                 = 0.
    ctx_io%clean_final        = 0

    return
  end subroutine io_clean  

end module m_ctx_io
