!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_array_save_binary.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO when we have to save some array in binary
!
!------------------------------------------------------------------------------
module m_array_save_binary

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_barrier,                only: barrier
  use m_ctx_parallelism,        only: t_parallelism
  !! -------------------------------------------------------------------
  implicit none

  interface array_save_binary
     module procedure array_save_binary_single_real4
     module procedure array_save_binary_single_real8
     module procedure array_save_binary_multiple_real4
     module procedure array_save_binary_multiple_real8
  end interface
  
  private
  public  :: array_save_binary

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it simply saves array input binary array from the master
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    array             : array to be saved
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : name of the field
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_binary_single_real4(ctx_paral,array,size_array, &
                                            str_folder,fieldname,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=4)                ,intent(in)   :: array(:)
    integer                     ,intent(in)   :: size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*)            ,intent(in)   :: fieldname
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: unit_io = 1010
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! master save 
    if(ctx_paral%master) then 
      fullname = trim(adjustl(str_folder))//"/"//trim(adjustl(fieldname))//".bin"
      open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
            status='replace',recl=4*size_array)
      write(unit=unit_io,rec=1) real(array)
      close(unit=unit_io)
    end if
    !! wait for all
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine array_save_binary_single_real4
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it simply saves array input binary array from the master
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    array             : array to be saved
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : name of the field
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_binary_single_real8(ctx_paral,array,size_array, &
                                            str_folder,fieldname,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=8)                ,intent(in)   :: array(:)
    integer                     ,intent(in)   :: size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*)            ,intent(in)   :: fieldname
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: unit_io = 1010
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! master save 
    if(ctx_paral%master) then 
      fullname = trim(adjustl(str_folder))//"/"//trim(adjustl(fieldname))//".bin"
      open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',    &
            status='replace',recl=8*size_array)
      write(unit=unit_io,rec=1) array
      close(unit=unit_io)
    end if
    !! wait for all
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine array_save_binary_single_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it simply saves multiple array input binary array from the master
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    array    (:,:)    : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname(:)      : name of the field
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_binary_multiple_real4(ctx_paral,array,narray,   &
                                              size_array,str_folder,    &
                                              fieldname,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=4)    ,allocatable,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray,size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: k,unit_io = 1011
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! master save 
    if(ctx_paral%master) then 
      do k=1,narray
        fullname = trim(adjustl(str_folder))//"/"//                     &
                   trim(adjustl(fieldname(k)))//".bin"
        open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',&
              status='replace',recl=4*size_array)
        write(unit=unit_io,rec=1) real(array(:,k))
        close(unit=unit_io)
      end do
    end if
    !! wait for all
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine array_save_binary_multiple_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it simply saves multiple array input binary array from the master
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    array    (:,:)    : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname(:)      : name of the field
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_binary_multiple_real8(ctx_paral,array,narray,   &
                                              size_array,str_folder,    &
                                              fieldname,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=8)    ,allocatable,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray,size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: k,unit_io = 1011
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! master save 
    if(ctx_paral%master) then 
      do k=1,narray
        fullname = trim(adjustl(str_folder))//"/"//                     &
                   trim(adjustl(fieldname(k)))//".bin"
        open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',&
              status='replace',recl=8*size_array)
        write(unit=unit_io,rec=1) array(:,k)
        close(unit=unit_io)
      end do
    end if
    !! wait for all
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine array_save_binary_multiple_real8

end module m_array_save_binary
