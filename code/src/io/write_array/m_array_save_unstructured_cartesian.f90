!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_array_save_unstructured_cartesian.f90
!        it returns an error because we cannot save
!        unstructured cartesian domain.       
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO to save solution on an unstructured 
!> domain: with cartesian: it returns an error
!
!------------------------------------------------------------------------------
module m_array_save_unstructured

  !! module used -------------------------------------------------------
  use m_ctx_parallelism,        only: t_parallelism
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  !! -------------------------------------------------------------------
  implicit none

  interface array_save_unstructured
     module procedure array_save_unstructured_real4
     module procedure array_save_unstructured_real8
     module procedure array_save_unstructured_complex4
     module procedure array_save_unstructured_complex8
  end interface
  
  private
  public  :: array_save_unstructured

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_real4(ctx_paral,ctx_domain,ctx_disc,   &
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           ctx_err)


    implicit none
    type(t_parallelism)         ,optional, intent(in)   :: ctx_paral
    type(t_domain)              ,optional, intent(in)   :: ctx_domain
    type(t_discretization)      ,optional, intent(in)   :: ctx_disc
    real(kind=4),allocatable             , intent(in)   :: array(:,:)
    integer                     ,optional, intent(in)   :: narray
    character(len=*)            ,optional, intent(in)   :: str_representation
    character(len=*),allocatable,optional, intent(in)   :: fieldname(:)
    character(len=*)            ,optional, intent(in)   :: fieldname_gb,basename
    type(t_error)               ,          intent(inout):: ctx_err

    !! avoid compilation warning 
    if(present(ctx_paral))          ctx_err%ierr = -1 
    if(present(ctx_domain))         ctx_err%ierr = -1 
    if(present(ctx_disc))           ctx_err%ierr = -1 
    if(present(narray))             ctx_err%ierr = -1 
    if(present(str_representation)) ctx_err%ierr = -1 
    if(present(fieldname))          ctx_err%ierr = -1 
    if(present(fieldname_gb))       ctx_err%ierr = -1 
    if(present(basename))           ctx_err%ierr = -1 
    if(allocated(array)) then
    endif
    !! >>>
    
    ctx_err%msg   = "** Cannot save cartesian grid using an " // &
                    "unstructured format ** [array_save_unstructured]"
    ctx_err%ierr  = -1
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    return

  end subroutine array_save_unstructured_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_real8(ctx_paral,ctx_domain,ctx_disc,   &
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           ctx_err)


    implicit none
    type(t_parallelism)         ,optional, intent(in)   :: ctx_paral
    type(t_domain)              ,optional, intent(in)   :: ctx_domain
    type(t_discretization)      ,optional, intent(in)   :: ctx_disc
    real(kind=8),allocatable             , intent(in)   :: array(:,:)
    integer                     ,optional, intent(in)   :: narray
    character(len=*)            ,optional, intent(in)   :: str_representation
    character(len=*),allocatable,optional, intent(in)   :: fieldname(:)
    character(len=*)            ,optional, intent(in)   :: fieldname_gb,basename
    type(t_error)               ,          intent(inout):: ctx_err

    !! avoid compilation warning 
    if(present(ctx_paral))          ctx_err%ierr = -1 
    if(present(ctx_domain))         ctx_err%ierr = -1 
    if(present(ctx_disc))           ctx_err%ierr = -1 
    if(present(narray))             ctx_err%ierr = -1 
    if(present(str_representation)) ctx_err%ierr = -1 
    if(present(fieldname))          ctx_err%ierr = -1 
    if(present(fieldname_gb))       ctx_err%ierr = -1 
    if(present(basename))           ctx_err%ierr = -1 
    if(allocated(array)) then
    endif
    !! >>>
    
    ctx_err%msg   = "** Cannot save cartesian grid using an " // &
                    "unstructured format ** [array_save_unstructured]"
    ctx_err%ierr  = -1
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    return 
    
  end subroutine array_save_unstructured_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_complex4(ctx_paral,ctx_domain,ctx_disc,&
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           ctx_err)


    implicit none
    type(t_parallelism)         ,optional, intent(in)   :: ctx_paral
    type(t_domain)              ,optional, intent(in)   :: ctx_domain
    type(t_discretization)      ,optional, intent(in)   :: ctx_disc
    complex(kind=4),allocatable          , intent(in)   :: array(:,:)
    integer                     ,optional, intent(in)   :: narray
    character(len=*)            ,optional, intent(in)   :: str_representation
    character(len=*),allocatable,optional, intent(in)   :: fieldname(:)
    character(len=*)            ,optional, intent(in)   :: fieldname_gb,basename
    type(t_error)               ,          intent(inout):: ctx_err

    !! avoid compilation warning 
    if(present(ctx_paral))          ctx_err%ierr = -1 
    if(present(ctx_domain))         ctx_err%ierr = -1 
    if(present(ctx_disc))           ctx_err%ierr = -1 
    if(present(narray))             ctx_err%ierr = -1 
    if(present(str_representation)) ctx_err%ierr = -1 
    if(present(fieldname))          ctx_err%ierr = -1 
    if(present(fieldname_gb))       ctx_err%ierr = -1 
    if(present(basename))           ctx_err%ierr = -1 
    if(allocated(array)) then
    endif
    !! >>>
    
    ctx_err%msg   = "** Cannot save cartesian grid using an " // &
                    "unstructured format ** [array_save_unstructured]"
    ctx_err%ierr  = -1
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    return 

  end subroutine array_save_unstructured_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_complex8(ctx_paral,ctx_domain,ctx_disc,&
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           ctx_err)

    implicit none
    type(t_parallelism)         ,optional, intent(in)   :: ctx_paral
    type(t_domain)              ,optional, intent(in)   :: ctx_domain
    type(t_discretization)      ,optional, intent(in)   :: ctx_disc
    complex(kind=8),allocatable          , intent(in)   :: array(:,:)
    integer                     ,optional, intent(in)   :: narray
    character(len=*)            ,optional, intent(in)   :: str_representation
    character(len=*),allocatable,optional, intent(in)   :: fieldname(:)
    character(len=*)            ,optional, intent(in)   :: fieldname_gb,basename
    type(t_error)               ,          intent(inout):: ctx_err

    !! avoid compilation warning 
    if(present(ctx_paral))          ctx_err%ierr = -1 
    if(present(ctx_domain))         ctx_err%ierr = -1 
    if(present(ctx_disc))           ctx_err%ierr = -1 
    if(present(narray))             ctx_err%ierr = -1 
    if(present(str_representation)) ctx_err%ierr = -1 
    if(present(fieldname))          ctx_err%ierr = -1 
    if(present(fieldname_gb))       ctx_err%ierr = -1 
    if(present(basename))           ctx_err%ierr = -1 
    if(allocated(array)) then
    endif
    !! >>>
    
    ctx_err%msg   = "** Cannot save cartesian grid using an " // &
                    "unstructured format ** [array_save_unstructured]"
    ctx_err%ierr  = -1
    ctx_err%critic=.true.
    call raise_error(ctx_err)
    
    return
  end subroutine array_save_unstructured_complex8

end module m_array_save_unstructured
