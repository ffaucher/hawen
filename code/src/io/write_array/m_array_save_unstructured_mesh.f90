!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_array_save_unstructured.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO to save solution on an unstructured 
!> domain: with mesh. For instance, we use the parallel pvtu format.
!
!------------------------------------------------------------------------------
module m_array_save_unstructured

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH,RKIND_MESH,RKIND_MAT
  use m_ctx_parallelism,        only: t_parallelism
  use m_barrier,                only: barrier
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_paraview_pvtu,          only: save_paraview_pvtu
  use m_paraview_header_vtu,    only: paraview_header_vtu,              &
                                      paraview_close_vtu,               &
                                      VTK_OMAX_2D, VTK_OMAX_3D
  use m_paraview_vtu,           only: save_paraview_vtu
  !! -------------------------------------------------------------------
  implicit none

  interface array_save_unstructured
     module procedure array_save_unstructured_real4
     module procedure array_save_unstructured_real8
     module procedure array_save_unstructured_complex4
     module procedure array_save_unstructured_complex8
  end interface
  interface array_restrict_order
     module procedure array_restrict_order_real4
     module procedure array_restrict_order_real8
     module procedure array_restrict_order_complex4
     module procedure array_restrict_order_complex8
  end interface
  interface array_restrict_cst_order
     module procedure array_restrict_cste_order_real4
     module procedure array_restrict_cste_order_real8
     module procedure array_restrict_cste_order_complex4
     module procedure array_restrict_cste_order_complex8
  end interface
  
  private
  public  :: array_save_unstructured

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Print Warning because too high order for visualization
  !
  !> @param[in]    ctx_paral         : context parallelism
  !> @param[in]    dm                : domain dimension
  !> @param[in]    omax              : order max allowed
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine print_warning_order(ctx_paral,dm,omax,ctx_err)
  
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    integer                     ,intent(in)   :: dm,omax
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    ctx_err%ierr  = 0

    !! print infos
    if(ctx_paral%master) then
      write(6,'(a)')      " "
      write(6,'(a)')      "             *************************************************"
      write(6,'(a)')      "             *****          WARNING VISUALIZATION           **"
      write(6,'(a)')      "             *************************************************"
      write(6,'(a)')      "             ***** WARNING: VISUALIZATION with PARAVIEW 5.8 **"
      write(6,'(a,i2,a)') "             ***** DOES NOT HANDLE ORDER HIGHER THAN ",omax, &
                                                                       "     **"
      write(6,'(a,i2,a)') "             ***** IN ",dm,                                  &
                                        "D. Cells with higher order will     **"
      write(6,'(a)')      "             ***** use order 1 visualization instead        **"
      write(6,'(a)')      "             *************************************************"
      write(6,'(a)')      " "
    end if
    return
    
  end subroutine print_warning_order    

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_cste_o       : indicates if constant order
  !> @param[in]    cste_order        : constant order value
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_real4(ctx_paral,ctx_domain,ctx_disc,   &
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           flag_cste_o,cste_order,ctx_err)

    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real(kind=4),allocatable    ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_cste_o
    integer                     ,intent(in)   :: cste_order
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=256) :: vtu_filename
    character(len=256) :: pvtu_filename
    character(len=128) :: shortname
    character(len=16)  :: ifmt,fmt_iproc
    integer            :: imodel,k,ndigits,ndof_vol_cste
    integer            :: ikindmesh=IKIND_MESH
    integer            :: rkindmesh=RKIND_MESH
    integer            :: rkindsol =4    !! fixed by routine
    logical            :: flag_cx=.false.
    real(kind=4), allocatable :: array_reduced(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    !! only work for simplex for now >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(trim(adjustl(ctx_disc%format_cell)) .ne. 'simplex') then
        ctx_err%msg   = "** ERROR: only simplex are supported " //      &
                        "[save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
  
    ndof_vol_cste = 0
    if(flag_cste_o) then
      if(cste_order < 0) then
        ctx_err%msg   = "** ERROR: trying to save dof with order " //   &
                        "< 0 in [save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      !! compute the constant ndof
      select case(ctx_domain%dim_domain)
        case(1)
          ndof_vol_cste = (cste_order+1)
        case(2)
          ndof_vol_cste = (cste_order+2)*(cste_order+1)/2
        case(3)
          ndof_vol_cste = (cste_order+3)*(cste_order+2)*(cste_order+1)/6
        case default
          ctx_err%msg   = "** ERROR: unrecognized dim [save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

    end if

    !! -----------------------------------------------------------------
    !! vtu & pvtu name per processor
    ndigits  = ceiling(log10(real(ctx_paral%nproc)+1.0)) + 1 
    write(ifmt, '(i0)') ndigits
    fmt_iproc = '(i0.'//trim(adjustl(ifmt))//')'
    write(ifmt,fmt_iproc) ctx_paral%myrank
    vtu_filename = trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //"_cpu"//               &
                   trim(adjustl(ifmt))//".vtu"
    pvtu_filename= trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //".pvtu"
    !! short name is the name before first underscore
    do k=1,len_trim(adjustl(fieldname_gb))
      if(fieldname_gb(k:k) == "_") then
        exit
      end if
    end do
    shortname=''
    shortname(1:k-1) = fieldname_gb(1:k-1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! Create header file .PVTU first: only the master >>>>>>>>>>>>>>>>>
    if(ctx_paral%master) then
      call save_paraview_pvtu(str_representation,pvtu_filename,         &
                              shortname,ctx_paral%nproc,fieldname,      &
                              narray,flag_cx,ikindmesh,rkindmesh,       &
                              rkindsol,ctx_err)
    end if
    !! Create header file .VTU >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! Create vtu header: the subroutine is in discretization-operator/io
    !! it depends on the mesh elements and the total number of dof 
    !! i.e. must separate array saved per cell from dof.
    call paraview_header_vtu(ctx_domain,ctx_disc,str_representation,    &
                             vtu_filename,shortname,fieldname,narray,   &
                             rkindsol,flag_cx,flag_cste_o,cste_order,ctx_err)

    !! add arrays to vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do imodel=1,narray
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      !! WARNING: I CANNOT HANDLE PARAVIEW 5.8 LAGRANGE FOR 
      !! DIMENSION 2 WITH ORDER > VTK_OMAX_2D 
      !! DIMENSION 3 WITH ORDER > VTK_OMAX_3D
      !! WE FORCE ORDER 1 FOR THE CELL WITH SUCH CHARACTERISTICS
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      if(trim(adjustl(str_representation)) .eq. 'dof' .or.              &
         trim(adjustl(str_representation)) .eq. 'dof-orderfix') then 
        
        !! dof-orderfix requires a constant order. 
        if(trim(adjustl(str_representation)) .eq. 'dof-orderfix'  .and. &
           .not.(flag_cste_o)) then
          ctx_err%msg   = "** ERROR: multiple constant per cell "    // &
                          "requires a fixed order representation "   // &
                          "[save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
         
         
        !! 2D ORDER 
        if(ctx_domain%dim_domain == 2 .and.                             &
          (ctx_disc%order_max_gb>VTK_OMAX_2D  .or.                      &
          (flag_cste_o .and. cste_order > VTK_OMAX_2D))) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_2D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err)     
          else
            call array_restrict_order(ctx_domain,ctx_disc,              &
                                      array(:,imodel),array_reduced,    &
                                      VTK_OMAX_2D,                      &
                                      ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! 3D ORDER > xxx
        elseif(ctx_domain%dim_domain == 3 .and.                         &
          (ctx_disc%order_max_gb>VTK_OMAX_3D  .or.                      &
          (flag_cste_o .and. cste_order > VTK_OMAX_3D))) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_3D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err)      
          else
            call array_restrict_order(ctx_domain,ctx_disc,              &
                                      array(:,imodel),array_reduced,    &
                                      VTK_OMAX_3D,                      &
                                      ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! FINE HERE
        else
          call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
        end if
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      else !! Cell representation ok whatsoever
        call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
      end if
    end do

    !! closure for vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call paraview_close_vtu(vtu_filename,ctx_err)
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------

    return

  end subroutine array_save_unstructured_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_cste_o       : indicates if constant order
  !> @param[in]    cste_order        : constant order value
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_real8(ctx_paral,ctx_domain,ctx_disc,   &
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           flag_cste_o,cste_order,ctx_err)

    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real(kind=8),allocatable    ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_cste_o
    integer                     ,intent(in)   :: cste_order
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=256) :: vtu_filename
    character(len=256) :: pvtu_filename
    character(len=128) :: shortname
    character(len=16)  :: ifmt,fmt_iproc
    integer            :: imodel,k,ndigits,ndof_vol_cste
    integer            :: ikindmesh=IKIND_MESH
    integer            :: rkindmesh=RKIND_MESH
    integer            :: rkindsol =8      !! fixed by routine
    logical            :: flag_cx=.false.  !> fixed by routine
    real(kind=8), allocatable :: array_reduced(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! only work for simplex for now >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(trim(adjustl(ctx_disc%format_cell)) .ne. 'simplex') then
        ctx_err%msg   = "** ERROR: only simplex are supported " //      &
                        "[save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
  
    ndof_vol_cste = 0
    if(flag_cste_o) then
      if(cste_order < 0) then
        ctx_err%msg   = "** ERROR: trying to save dof with order " //   &
                        "< 0 in [save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      !! compute the constant ndof
      select case(ctx_domain%dim_domain)
        case(1)
          ndof_vol_cste = (cste_order+1)
        case(2)
          ndof_vol_cste = (cste_order+2)*(cste_order+1)/2
        case(3)
          ndof_vol_cste = (cste_order+3)*(cste_order+2)*(cste_order+1)/6
        case default
          ctx_err%msg   = "** ERROR: unrecognized dim [save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

    end if

    !! -----------------------------------------------------------------
    !! vtu & pvtu name per processor
    ndigits  = ceiling(log10(real(ctx_paral%nproc)+1.0)) + 1 
    write(ifmt, '(i0)') ndigits
    fmt_iproc = '(i0.'//trim(adjustl(ifmt))//')'
    write(ifmt,fmt_iproc) ctx_paral%myrank
    vtu_filename = trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //"_cpu"//               &
                   trim(adjustl(ifmt))//".vtu"
    pvtu_filename= trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //".pvtu"
    !! short name is the name before first underscore
    do k=1,len_trim(adjustl(fieldname_gb))
      if(fieldname_gb(k:k) == "_") then
        exit
      end if
    end do
    shortname=''
    shortname(1:k-1) = fieldname_gb(1:k-1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! Create header file .PVTU first: only the master >>>>>>>>>>>>>>>>>
    if(ctx_paral%master) then
      call save_paraview_pvtu(str_representation,pvtu_filename,         &
                              shortname,ctx_paral%nproc,fieldname,      &
                              narray,flag_cx,ikindmesh,rkindmesh,       &
                              rkindsol,ctx_err)
    end if
    !! Create header file .VTU >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! Create vtu header: the subroutine is in discretization-operator/io
    !! it depends on the mesh elements and the total number of dof 
    !! i.e. must separate array saved per cell from dof.
    call paraview_header_vtu(ctx_domain,ctx_disc,str_representation,    &
                             vtu_filename,shortname,fieldname,narray,   &
                             rkindsol,flag_cx,flag_cste_o,cste_order,ctx_err)

    !! add arrays to vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do imodel=1,narray
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      !! WARNING: I CANNOT HANDLE PARAVIEW 5.8 LAGRANGE FOR 
      !! DIMENSION 2 WITH ORDER > xxx
      !! DIMENSION 3 WITH ORDER > xxx
      !! WE FORCE ORDER 1 FOR THE CELL WITH SUCH CHARACTERISTICS
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      if(trim(adjustl(str_representation)) .eq. 'dof' .or.              &
         trim(adjustl(str_representation)) .eq. 'dof-orderfix') then 
        
        !! dof-orderfix requires a constant order. 
        if(trim(adjustl(str_representation)) .eq. 'dof-orderfix'  .and. &
           .not.(flag_cste_o)) then
          ctx_err%msg   = "** ERROR: multiple constant per cell "    // &
                          "requires a fixed order representation "   // &
                          "[save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

        !! 2D ORDER > VTK_OMAX_2D
        if(ctx_domain%dim_domain == 2 .and.                             &
           ctx_disc%order_max_gb>VTK_OMAX_2D) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_2D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err) 
          else
            call array_restrict_order(ctx_domain,ctx_disc,              &
                                      array(:,imodel),array_reduced,    &
                                      VTK_OMAX_2D,                      &
                                      ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! 3D ORDER > xxx
        elseif(ctx_domain%dim_domain == 3 .and.                         &
               ctx_disc%order_max_gb>VTK_OMAX_3D) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_3D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err)    
          else
            call array_restrict_order(ctx_domain,ctx_disc,              &
                                      array(:,imodel),array_reduced,    &
                                      VTK_OMAX_3D,                      &
                                      ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! FINE HERE
        else
          call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
        end if
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      else !! Cell representation ok whatsoever
        call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
      end if
    end do

    !! closure for vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call paraview_close_vtu(vtu_filename,ctx_err)
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------

    return

  end subroutine array_save_unstructured_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_cste_o       : indicates if constant order
  !> @param[in]    cste_order        : constant order value
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_complex4(ctx_paral,ctx_domain,ctx_disc,&
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           flag_cste_o,cste_order,ctx_err)

    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=4),allocatable ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_cste_o
    integer                     ,intent(in)   :: cste_order
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=256) :: vtu_filename
    character(len=256) :: pvtu_filename
    character(len=128) :: shortname
    character(len=16)  :: ifmt,fmt_iproc
    integer            :: imodel,k,ndigits,ndof_vol_cste
    integer            :: ikindmesh=IKIND_MESH
    integer            :: rkindmesh=RKIND_MESH
    integer            :: rkindsol =4    !! fixed by routine
    logical            :: flag_cx=.true. !> fixed by routine
    complex(kind=4), allocatable :: array_reduced(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! only work for simplex for now >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(trim(adjustl(ctx_disc%format_cell)) .ne. 'simplex') then
        ctx_err%msg   = "** ERROR: only simplex are supported " //      &
                        "[save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
  
    ndof_vol_cste = 0
    if(flag_cste_o) then
      if(cste_order < 0) then
        ctx_err%msg   = "** ERROR: trying to save dof with order " //   &
                        "< 0 in [save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      !! compute the constant ndof
      select case(ctx_domain%dim_domain)
        case(1)
          ndof_vol_cste = (cste_order+1)
        case(2)
          ndof_vol_cste = (cste_order+2)*(cste_order+1)/2
        case(3)
          ndof_vol_cste = (cste_order+3)*(cste_order+2)*(cste_order+1)/6
        case default
          ctx_err%msg   = "** ERROR: unrecognized dim [save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

    end if

    !! -----------------------------------------------------------------
    !! vtu & pvtu name per processor
    ndigits  = ceiling(log10(real(ctx_paral%nproc)+1.0)) + 1 
    write(ifmt, '(i0)') ndigits
    fmt_iproc = '(i0.'//trim(adjustl(ifmt))//')'
    write(ifmt,fmt_iproc) ctx_paral%myrank
    vtu_filename = trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //"_cpu"//               &
                   trim(adjustl(ifmt))//".vtu"
    pvtu_filename= trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //".pvtu"
    !! short name is the name before first underscore
    do k=1,len_trim(adjustl(fieldname_gb))
      if(fieldname_gb(k:k) == "_") then
        exit
      end if
    end do
    shortname=''
    shortname(1:k-1) = fieldname_gb(1:k-1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! Create header file .PVTU first: only the master >>>>>>>>>>>>>>>>>
    if(ctx_paral%master) then
      call save_paraview_pvtu(str_representation,pvtu_filename,         &
                              shortname,ctx_paral%nproc,fieldname,      &
                              narray,flag_cx,ikindmesh,rkindmesh,       &
                              rkindsol,ctx_err)
    end if
    !! Create header file .VTU >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! Create vtu header: the subroutine is in discretization-operator/io
    !! it depends on the mesh elements and the total number of dof 
    !! i.e. must separate array saved per cell from dof.
    call paraview_header_vtu(ctx_domain,ctx_disc,str_representation,    &
                             vtu_filename,shortname,fieldname,narray,   &
                             rkindsol,flag_cx,flag_cste_o,cste_order,ctx_err)

    !! add arrays to vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do imodel=1,narray
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      !! WARNING: I CANNOT HANDLE PARAVIEW 5.8 LAGRANGE FOR 
      !! DIMENSION 2 WITH ORDER > xxx
      !! DIMENSION 3 WITH ORDER > xxx
      !! WE FORCE ORDER 1 FOR THE CELL WITH SUCH CHARACTERISTICS
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      if(trim(adjustl(str_representation)) .eq. 'dof' .or.              &
         trim(adjustl(str_representation)) .eq. 'dof-orderfix') then 
        
        !! dof-orderfix requires a constant order. 
        if(trim(adjustl(str_representation)) .eq. 'dof-orderfix'  .and. &
           .not.(flag_cste_o)) then
          ctx_err%msg   = "** ERROR: multiple constant per cell "    // &
                          "requires a fixed order representation "   // &
                          "[save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

        !! 2D ORDER > xxx
        if(ctx_domain%dim_domain == 2 .and.                             &
           ctx_disc%order_max_gb > VTK_OMAX_2D) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_2D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err)     
          else
            call array_restrict_order(ctx_domain,ctx_disc,              &
                                      array(:,imodel),array_reduced,    &
                                      VTK_OMAX_2D,                      &
                                      ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! 3D ORDER > xxx
        elseif(ctx_domain%dim_domain == 3 .and.                         &
               ctx_disc%order_max_gb > VTK_OMAX_3D) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_3D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err)       
          else
            call array_restrict_order(ctx_domain,ctx_disc,array(:,imodel),&
                                    array_reduced,VTK_OMAX_3D,            &
                                    ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! FINE HERE
        else
          call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
        end if
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      else !! Cell representation ok whatsoever
        call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
      end if

    end do

    !! closure for vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call paraview_close_vtu(vtu_filename,ctx_err)
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------

    return

  end subroutine array_save_unstructured_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using VTK format: 
  !> We need to save the mesh informations (nodes, etc)
  !> and the solution values over the dof or cells.
  !> It uses pvtu format where every processor save its
  !> own contribution.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname,
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_cste_o       : indicates if constant order
  !> @param[in]    cste_order        : constant order value
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_unstructured_complex8(ctx_paral,ctx_domain,ctx_disc,&
                                           array,narray,str_representation, &
                                           fieldname,fieldname_gb,basename, &
                                           flag_cste_o,cste_order,ctx_err)

    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=8),allocatable ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_cste_o
    integer                     ,intent(in)   :: cste_order
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=256) :: vtu_filename
    character(len=256) :: pvtu_filename
    character(len=128) :: shortname
    character(len=16)  :: ifmt,fmt_iproc
    integer            :: imodel,k,ndigits,ndof_vol_cste
    integer            :: ikindmesh=IKIND_MESH
    integer            :: rkindmesh=RKIND_MESH
    integer            :: rkindsol =8    !! fixed by routine
    logical            :: flag_cx=.true. !> fixed by routine
    complex(kind=8), allocatable :: array_reduced(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! only work for simplex for now >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(trim(adjustl(ctx_disc%format_cell)) .ne. 'simplex') then
        ctx_err%msg   = "** ERROR: only simplex are supported " //      &
                        "[save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------
  
    ndof_vol_cste = 0
    if(flag_cste_o) then
      if(cste_order < 0) then
        ctx_err%msg   = "** ERROR: trying to save dof with order " //   &
                        "< 0 in [save_unstructured] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      !! compute the constant ndof
      select case(ctx_domain%dim_domain)
        case(1)
          ndof_vol_cste = (cste_order+1)
        case(2)
          ndof_vol_cste = (cste_order+2)*(cste_order+1)/2
        case(3)
          ndof_vol_cste = (cste_order+3)*(cste_order+2)*(cste_order+1)/6
        case default
          ctx_err%msg   = "** ERROR: unrecognized dim [save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
      end select

    end if

    !! -----------------------------------------------------------------
    !! vtu & pvtu name per processor
    ndigits  = ceiling(log10(real(ctx_paral%nproc)+1.0)) + 1 
    write(ifmt, '(i0)') ndigits
    fmt_iproc = '(i0.'//trim(adjustl(ifmt))//')'
    write(ifmt,fmt_iproc) ctx_paral%myrank
    vtu_filename = trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //"_cpu"//               &
                   trim(adjustl(ifmt))//".vtu"
    pvtu_filename= trim(adjustl(basename))//"/"//                       &
                   trim(adjustl(fieldname_gb)) //".pvtu"
    !! short name is the name before first underscore
    do k=1,len_trim(adjustl(fieldname_gb))
      if(fieldname_gb(k:k) == "_") then
        exit
      end if
    end do
    shortname=''
    shortname(1:k-1) = fieldname_gb(1:k-1)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! Create header file .PVTU first: only the master >>>>>>>>>>>>>>>>>
    if(ctx_paral%master) then
      call save_paraview_pvtu(str_representation,pvtu_filename,         &
                              shortname,ctx_paral%nproc,fieldname,      &
                              narray,flag_cx,ikindmesh,rkindmesh,       &
                              rkindsol,ctx_err)
    end if
    !! Create header file .VTU >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! Create vtu header: the subroutine is in discretization-operator/io
    !! it depends on the mesh elements and the total number of dof 
    !! i.e. must separate array saved per cell from dof.
    call paraview_header_vtu(ctx_domain,ctx_disc,str_representation,    &
                             vtu_filename,shortname,fieldname,narray,   &
                             rkindsol,flag_cx,flag_cste_o,cste_order,ctx_err)

    !! add arrays to vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    do imodel=1,narray
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      !! WARNING: I CANNOT HANDLE PARAVIEW 5.8 LAGRANGE FOR 
      !! DIMENSION 2 WITH ORDER > xxx
      !! DIMENSION 3 WITH ORDER > xxx
      !! WE FORCE ORDER 1 FOR THE CELL WITH SUCH CHARACTERISTICS
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      if(trim(adjustl(str_representation)) .eq. 'dof' .or.              &
         trim(adjustl(str_representation)) .eq. 'dof-orderfix') then 
        
        !! dof-orderfix requires a constant order. 
        if(trim(adjustl(str_representation)) .eq. 'dof-orderfix'  .and. &
           .not.(flag_cste_o)) then
          ctx_err%msg   = "** ERROR: multiple constant per cell "    // &
                          "requires a fixed order representation "   // &
                          "[save_unstructured] **"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if

        !! 2D ORDER > xxx
        if(ctx_domain%dim_domain == 2 .and.                             &
           ctx_disc%order_max_gb>VTK_OMAX_2D) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_2D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err)     
          else
            call array_restrict_order(ctx_domain,ctx_disc,              &
                                      array(:,imodel),array_reduced,    &
                                      VTK_OMAX_2D,                      &
                                      ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! 3D ORDER > xxx
        elseif(ctx_domain%dim_domain == 3 .and.                         &
               ctx_disc%order_max_gb>VTK_OMAX_3D) then
          if(imodel == 1) then
            call print_warning_order(ctx_paral,ctx_domain%dim_domain,   &
                                     VTK_OMAX_3D,ctx_err)
          end if
          !! depends if constant order or not
          if(flag_cste_o) then
            call array_restrict_cst_order(ctx_domain,array(:,imodel),   &
                                          array_reduced,                &
                                          ctx_domain%n_node_per_cell,   &
                                          ndof_vol_cste,ctx_err)
          else
            call array_restrict_order(ctx_domain,ctx_disc,              &
                                      array(:,imodel),array_reduced,    &
                                      VTK_OMAX_3D,                      &
                                      ctx_domain%n_node_per_cell,ctx_err)
          end if
          call save_paraview_vtu(vtu_filename,array_reduced,ctx_err)
          deallocate(array_reduced)
        !! FINE HERE
        else
          call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
        end if
      ! ---------------------------------------------------------
      ! ---------------------------------------------------------
      else !! Cell representation ok whatsoever
        call save_paraview_vtu(vtu_filename,array(:,imodel),ctx_err)
      end if
    end do

    !! closure for vtu file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    call paraview_close_vtu(vtu_filename,ctx_err)
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------

    return

  end subroutine array_save_unstructured_complex8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_order_real4(ctx_domain,ctx_disc,array_in,   &
                                        array_out,omax,n_node,          &
                                        ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real(kind=4)                ,intent(in)   :: array_in (:)
    real(kind=4),allocatable    ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: omax,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer                  :: order, i_order, ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    n_dof_new=0
    !! 1./ count the new n_dof
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      n_dof_new = n_dof_new + ndof_loc
    end do
    
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    iloc1 = 1 
    iloc2 = 1
    igb1  = 1
    igb2  = 1
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      iloc2 = iloc1 + ndof_loc - 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
      iloc1 = iloc2 + 1
      igb1  = igb1  + ctx_disc%n_dof_per_order(ctx_disc%order_to_index (order))
    end do
    
    return

  end subroutine array_restrict_order_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_order_real8(ctx_domain,ctx_disc,array_in,   &
                                        array_out,omax,n_node,          &
                                        ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real(kind=8)                ,intent(in)   :: array_in (:)
    real(kind=8),allocatable    ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: omax,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer                  :: order, i_order, ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    n_dof_new=0
    !! 1./ count the new n_dof
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      n_dof_new = n_dof_new + ndof_loc
    end do
    
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    iloc1 = 1 
    iloc2 = 1
    igb1  = 1
    igb2  = 1
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      iloc2 = iloc1 + ndof_loc - 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
      iloc1 = iloc2 + 1
      igb1  = igb1  + ctx_disc%n_dof_per_order(ctx_disc%order_to_index (order))
    end do
    
    return

  end subroutine array_restrict_order_real8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_order_complex4(ctx_domain,ctx_disc,array_in,&
                                           array_out,omax,n_node,       &
                                           ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=4)             ,intent(in)   :: array_in (:)
    complex(kind=4),allocatable ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: omax,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer                  :: order, i_order, ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    n_dof_new=0
    !! 1./ count the new n_dof
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      n_dof_new = n_dof_new + ndof_loc
    end do
    
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    iloc1 = 1
    igb1  = 1
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      iloc2 = iloc1 + ndof_loc - 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
      iloc1 = iloc2 + 1
      igb1  = igb1  + ctx_disc%n_dof_per_order(ctx_disc%order_to_index (order))
    end do
    
    return

  end subroutine array_restrict_order_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_order_complex8(ctx_domain,ctx_disc,array_in,&
                                           array_out,omax,n_node,       &
                                           ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=8)             ,intent(in)   :: array_in (:)
    complex(kind=8),allocatable ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: omax,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer                  :: order, i_order, ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    n_dof_new=0
    !! 1./ count the new n_dof
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      n_dof_new = n_dof_new + ndof_loc
    end do
    
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    iloc1 = 1 
    iloc2 = 1
    igb1  = 1
    igb2  = 1
    do i_cell=1,ctx_domain%n_cell_loc
      order = ctx_domain%order(i_cell)
      if(order > omax) then !! force order 1
        ndof_loc   = n_node
      else
        i_order    = ctx_disc%order_to_index (order)
        ndof_loc   = ctx_disc%n_dof_per_order(i_order)
      end if
      iloc2 = iloc1 + ndof_loc - 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
      iloc1 = iloc2 + 1
      igb1  = igb1  + ctx_disc%n_dof_per_order(ctx_disc%order_to_index (order))
    end do
    
    return

  end subroutine array_restrict_order_complex8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_cste_order_real4(ctx_domain,array_in,    &
                                       array_out,n_node,ndof_in,ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    real(kind=4)                ,intent(in)   :: array_in (:)
    real(kind=4),allocatable    ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: ndof_in,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer(kind=IKIND_MESH) :: ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! 1./ count with constant orders, force P1
    n_dof_new = ctx_domain%n_cell_loc * n_node
       
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    ndof_loc   = n_node
    do i_cell=1,ctx_domain%n_cell_loc
      iloc1 = (i_cell-1) * (ndof_loc) + 1
      iloc2 = iloc1 + ndof_loc - 1
      igb1  = (i_cell-1) * (ndof_in) + 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
    end do
    
    return

  end subroutine array_restrict_cste_order_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_cste_order_real8(ctx_domain,array_in,    &
                                       array_out,n_node,ndof_in,ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    real(kind=8)                ,intent(in)   :: array_in (:)
    real(kind=8),allocatable    ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: ndof_in,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer(kind=IKIND_MESH) :: ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! 1./ count with constant orders, force P1
    n_dof_new = ctx_domain%n_cell_loc * n_node
       
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    ndof_loc   = n_node
    do i_cell=1,ctx_domain%n_cell_loc
      iloc1 = (i_cell-1) * (ndof_loc) + 1
      iloc2 = iloc1 + ndof_loc - 1
      igb1  = (i_cell-1) * (ndof_in) + 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
    end do
    
    return

  end subroutine array_restrict_cste_order_real8
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_cste_order_complex4(ctx_domain,array_in,    &
                                       array_out,n_node,ndof_in,ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    complex(kind=4)             ,intent(in)   :: array_in (:)
    complex(kind=4),allocatable ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: ndof_in,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer(kind=IKIND_MESH) :: ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! 1./ count with constant orders, force P1
    n_dof_new = ctx_domain%n_cell_loc * n_node
       
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    ndof_loc   = n_node
    do i_cell=1,ctx_domain%n_cell_loc
      iloc1 = (i_cell-1) * (ndof_loc) + 1
      iloc2 = iloc1 + ndof_loc - 1
      igb1  = (i_cell-1) * (ndof_in) + 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
    end do
    
    return

  end subroutine array_restrict_cste_order_complex4
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Restrict the saved array where all cell of order > omax are
  !> instead set to order 1. This is due to dificulties with 
  !> Paraview arbitrary order
  !
  !> @param[in]    ctx_domain        : type domain (mesh)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array_in          : input array
  !> @param[inout] array_out         : output array
  !> @param[in]    omax              : maximal order
  !> @param[in]    n_node            : number of node per element
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_restrict_cste_order_complex8(ctx_domain,array_in,    &
                                       array_out,n_node,ndof_in,ctx_err)

    implicit none
    type(t_domain)              ,intent(in)   :: ctx_domain
    complex(kind=8)             ,intent(in)   :: array_in (:)
    complex(kind=8),allocatable ,intent(inout):: array_out(:)
    integer                     ,intent(in)   :: ndof_in,n_node
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH) :: i_cell
    integer(kind=IKIND_MESH) :: iloc1, iloc2, igb1, igb2
    integer(kind=IKIND_MESH) :: ndof_loc, n_dof_new
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! 1./ count with constant orders, force P1
    n_dof_new = ctx_domain%n_cell_loc * n_node
       
    !! 2./  new array
    allocate(array_out(n_dof_new))
    array_out = 0.0
    ndof_loc   = n_node
    do i_cell=1,ctx_domain%n_cell_loc
      iloc1 = (i_cell-1) * (ndof_loc) + 1
      iloc2 = iloc1 + ndof_loc - 1
      igb1  = (i_cell-1) * (ndof_in) + 1
      igb2  = igb1  + ndof_loc - 1
      array_out(iloc1:iloc2) = array_in(igb1:igb2)
    end do
    
    return

  end subroutine array_restrict_cste_order_complex8

end module m_array_save_unstructured
