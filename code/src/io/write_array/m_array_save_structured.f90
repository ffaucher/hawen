!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_array_save_structured.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO to save using a structured output 
!> we may need to convert mesh infos to cartesian mapping, this
!> is done in the discretization-method/$(DISCRETIZATION)/io
!
!------------------------------------------------------------------------------
module m_array_save_structured

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_define_precision,       only: IKIND_MESH
  use m_ctx_parallelism,        only: t_parallelism
  use m_barrier,                only: barrier
  use m_reduce_sum,             only: reduce_sum
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  use m_extract_structured_grid,only: extract_structured_grid
  !>
  use m_paraview_vts,           only: save_paraview_header_vts, &
                                      save_paraview_vts
  use m_sep_save,               only: save_sep_format
  !! -------------------------------------------------------------------
  implicit none

  interface array_save_structured
     module procedure array_save_structured_real4
     module procedure array_save_structured_real8
     module procedure array_save_structured_complex4
     module procedure array_save_structured_complex8
  end interface
  
  private
  public  :: array_save_structured

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using SEP or VTK format: 
  !> the partitioned model is reduced to a global array, which 
  !> creation depends if we have a mesh or a cartesian grid.
  !> For mesh we also need to treat the case where the model
  !> is given by cell or by dof.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_struct_vtk   : flag to indicate vtk
  !> @param[in]    flag_struct_sep   : flag to indicate sep
  !> @param[in]    order_subcell     : in case of multiple pts per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_structured_real4(ctx_paral,ctx_domain,ctx_disc,  &
                                         array,narray,str_representation,&
                                         fieldname,fieldname_gb,basename,&
                                         flag_struct_vtk,flag_struct_sep,&
                                         order_subcell,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real(kind=4),allocatable    ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    integer                     ,intent(in)   :: order_subcell
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_struct_vtk
    logical                     ,intent(in)   :: flag_struct_sep
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer                       :: imodel
    integer                       :: nx_gb,ny_gb,nz_gb
    real(kind=4),allocatable      :: model_gb (:,:,:)
    real(kind=8)                  :: dx,dy,dz,ox,oy,oz
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! create approriate output for all models, depending on the type
    do imodel=1,narray
      ! -----------------------------------------
      !! Extract structured grid for the current model
      !! ONLY MASTER HAS THE WHOLE SOLUTION
      ! -----------------------------------------
      call extract_structured_grid(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,imodel,str_representation,     &
                                   model_gb,nx_gb,ny_gb,nz_gb,ox,oy,oz, &
                                   dx,dy,dz,order_subcell,ctx_err)
      
      ! ----------------------------------------------------------------
      !! master is going to save depending on if it is SEP or VTK
      !! if VTK the first model gives the header
      ! ----------------------------------------------------------------
      if(ctx_paral%master) then
        if(flag_struct_sep) then
          call save_sep_format(basename,trim(adjustl(fieldname(imodel))), &
                               nx_gb,ny_gb,nz_gb,ox,oy,oz,dx,dy,dz,model_gb)
        end if
        
        if(flag_struct_vtk) then
          if(imodel==1) then
            call save_paraview_header_vts(basename,fieldname_gb,nx_gb,  &
                                          ny_gb,nz_gb,dx,dy,dz,ox,oy,oz)
          end if
          call save_paraview_vts(basename,fieldname_gb,                 &
                                 fieldname(imodel),nx_gb,ny_gb,nz_gb,   &
                                 model_gb)
        end if
      end if
      deallocate(model_gb)
      ! ----------------------------------------------------------------
      call barrier(ctx_paral%communicator,ctx_err)
    end do !! end loop over all models
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_structured_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using SEP or VTK format: 
  !> the partitioned model is reduced to a global array, which 
  !> creation depends if we have a mesh or a cartesian grid.
  !> For mesh we also need to treat the case where the model
  !> is given by cell or by dof.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_struct_vtk   : flag to indicate vtk
  !> @param[in]    flag_struct_sep   : flag to indicate sep
  !> @param[in]    order_subcell     : in case of multiple pts per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_structured_real8(ctx_paral,ctx_domain,ctx_disc,  &
                                         array,narray,str_representation,&
                                         fieldname,fieldname_gb,basename,&
                                         flag_struct_vtk,flag_struct_sep,&
                                         order_subcell,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    real(kind=8),allocatable    ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    integer                     ,intent(in)   :: order_subcell
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_struct_vtk
    logical                     ,intent(in)   :: flag_struct_sep
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer                       :: imodel
    integer                       :: nx_gb,ny_gb,nz_gb
    real(kind=8),allocatable      :: model_gb (:,:,:)
    real(kind=8)                  :: dx,dy,dz,ox,oy,oz
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! create approriate output for all models, depending on the type
    do imodel=1,narray
      ! -----------------------------------------
      !! Extract structured grid for the current model
      !! ONLY MASTER HAS THE WHOLE SOLUTION
      ! -----------------------------------------
      call extract_structured_grid(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,imodel,str_representation,     &
                                   model_gb,nx_gb,ny_gb,nz_gb,ox,oy,oz, &
                                   dx,dy,dz,order_subcell,ctx_err)
      
      ! ----------------------------------------------------------------
      !! master is going to save depending on if it is SEP or VTK
      !! if VTK the first model gives the header
      ! ----------------------------------------------------------------
      if(ctx_paral%master) then
        if(flag_struct_sep) then
          call save_sep_format(basename,trim(adjustl(fieldname(imodel))), &
                               nx_gb,ny_gb,nz_gb,ox,oy,oz,dx,dy,dz,model_gb)
        end if
        
        if(flag_struct_vtk) then
          if(imodel==1) then
            call save_paraview_header_vts(basename,fieldname_gb,nx_gb,  &
                                          ny_gb,nz_gb,dx,dy,dz,ox,oy,oz)
          end if
          call save_paraview_vts(basename,fieldname_gb,                 &
                                   fieldname(imodel),nx_gb,ny_gb,nz_gb, &
                                   model_gb)
        end if
      end if
      deallocate(model_gb)
      ! ----------------------------------------------------------------
      call barrier(ctx_paral%communicator,ctx_err)
    end do !! end loop over all models
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_structured_real8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using SEP or VTK format: 
  !> the partitioned model is reduced to a global array, which 
  !> creation depends if we have a mesh or a cartesian grid.
  !> For mesh we also need to treat the case where the model
  !> is given by cell or by dof.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_struct_vtk   : flag to indicate vtk
  !> @param[in]    flag_struct_sep   : flag to indicate sep
  !> @param[in]    order_subcell     : in case of multiple pts per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_structured_complex4(ctx_paral,ctx_domain,ctx_disc,&
                                         array,narray,str_representation, &
                                         fieldname,fieldname_gb,basename, &
                                         flag_struct_vtk,flag_struct_sep, &
                                         order_subcell,ctx_err)

    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=4),allocatable ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    integer                     ,intent(in)   :: order_subcell
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_struct_vtk
    logical                     ,intent(in)   :: flag_struct_sep
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer                       :: imodel
    integer                       :: nx_gb,ny_gb,nz_gb
    complex(kind=4),allocatable   :: model_gb (:,:,:)
    real(kind=8)                  :: dx,dy,dz,ox,oy,oz
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! create approriate output for all models, depending on the type
    do imodel=1,narray
      ! -----------------------------------------
      !! Extract structured grid for the current model
      !! ONLY MASTER HAS THE WHOLE SOLUTION
      ! -----------------------------------------
      call extract_structured_grid(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,imodel,str_representation,     &
                                   model_gb,nx_gb,ny_gb,nz_gb,ox,oy,oz, &
                                   dx,dy,dz,order_subcell,ctx_err)
      ! ----------------------------------------------------------------
      !! master is going to save depending on if it is SEP or VTK
      !! if VTK the first model gives the header
      ! ----------------------------------------------------------------
      if(ctx_paral%master) then
        if(flag_struct_sep) then
          call save_sep_format(basename,trim(adjustl(fieldname(imodel))), &
                               nx_gb,ny_gb,nz_gb,ox,oy,oz,dx,dy,dz,model_gb)
        end if
        
        if(flag_struct_vtk) then
          if(imodel==1) then
            call save_paraview_header_vts(basename,fieldname_gb,nx_gb,  &
                                          ny_gb,nz_gb,dx,dy,dz,ox,oy,oz)
          end if
          call save_paraview_vts(basename,fieldname_gb,                 &
                                   fieldname(imodel),nx_gb,ny_gb,nz_gb, &
                                   model_gb)
        end if
      end if
      deallocate(model_gb)
      ! ----------------------------------------------------------------
      call barrier(ctx_paral%communicator,ctx_err)
    end do !! end loop over all models
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_structured_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save structured array using SEP or VTK format: 
  !> the partitioned model is reduced to a global array, which 
  !> creation depends if we have a mesh or a cartesian grid.
  !> For mesh we also need to treat the case where the model
  !> is given by cell or by dof.
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_discret       : type discretization
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    fieldname         : name of the field, size narray
  !> @param[in]    fieldname_gb      : global name (e.g., for vtk)
  !> @param[in]    basename          : path to the file to be saved
  !> @param[in]    flag_struct_vtk   : flag to indicate vtk
  !> @param[in]    flag_struct_sep   : flag to indicate sep
  !> @param[in]    order_subcell     : in case of multiple pts per cell
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_structured_complex8(ctx_paral,ctx_domain,ctx_disc,&
                                         array,narray,str_representation, &
                                         fieldname,fieldname_gb,basename, &
                                         flag_struct_vtk,flag_struct_sep, &
                                         order_subcell,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    complex(kind=8),allocatable ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    integer                     ,intent(in)   :: order_subcell
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb,basename
    logical                     ,intent(in)   :: flag_struct_vtk
    logical                     ,intent(in)   :: flag_struct_sep
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    integer                       :: imodel
    integer                       :: nx_gb,ny_gb,nz_gb
    complex(kind=8),allocatable   :: model_gb (:,:,:)
    real(kind=8)                  :: dx,dy,dz,ox,oy,oz
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! create approriate output for all models, depending on the type
    do imodel=1,narray
      ! -----------------------------------------
      !! Extract structured grid for the current model
      !! ONLY MASTER HAS THE WHOLE SOLUTION
      ! -----------------------------------------
      call extract_structured_grid(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,imodel,str_representation,     &
                                   model_gb,nx_gb,ny_gb,nz_gb,ox,oy,oz, &
                                   dx,dy,dz,order_subcell,ctx_err)
      
      ! ----------------------------------------------------------------
      !! master is going to save depending on if it is SEP or VTK
      !! if VTK the first model gives the header
      ! ----------------------------------------------------------------
      if(ctx_paral%master) then
        if(flag_struct_sep) then
          call save_sep_format(basename,trim(adjustl(fieldname(imodel))), &
                               nx_gb,ny_gb,nz_gb,ox,oy,oz,dx,dy,dz,model_gb)
        end if
        
        if(flag_struct_vtk) then
          if(imodel==1) then
            call save_paraview_header_vts(basename,fieldname_gb,nx_gb,ny_gb, &
                                       nz_gb,dx,dy,dz,ox,oy,oz)
          end if
          call save_paraview_vts(basename,fieldname_gb,                 &
                                   fieldname(imodel),nx_gb,ny_gb,nz_gb, &
                                   model_gb)
        end if
      end if
      deallocate(model_gb)
      ! ----------------------------------------------------------------
      call barrier(ctx_paral%communicator,ctx_err)
    end do !! end loop over all models
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_structured_complex8
end module m_array_save_structured
