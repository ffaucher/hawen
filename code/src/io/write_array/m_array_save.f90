!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_array_save.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO when we have to save the results that are 
!> on the grid, several possibilities: 
!>  -> save structured   file from node infos
!>  -> save structured   file from cell infos
!>  -> save structured   file from dof  infos
!>  -> save unstructured file from cell infos
!>  -> save unstructured file from dof  infos
!
!------------------------------------------------------------------------------
module m_array_save

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  use m_ctx_domain,             only: t_domain
  use m_ctx_discretization,     only: t_discretization
  !> to save
  use m_array_save_structured,  only: array_save_structured
  use m_array_save_unstructured,only: array_save_unstructured
  !! -------------------------------------------------------------------
  implicit none

  interface array_save
     module procedure array_save_real4
     module procedure array_save_real8
     module procedure array_save_complex4
     module procedure array_save_complex8
  end interface
  
  private
  public  :: array_save

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> api to save array for structured and unstructured solutions
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_dg            : type DG
  !> @param[in]    save_structured   : indicate if save structured
  !> @param[in]    save_unstructured : indicate if save unstructured
  !> @param[in]    format_struct_vtk : indicate if save structured vtk
  !> @param[in]    format_struct_sep : indicate if save structured sep
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname_gb      : name of the field, size narray 
  !> @param[in]    basename          : global name (e.g., for vtk)
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_real4(ctx_paral,ctx_domain,ctx_disc,            &
                              save_unstructured,save_structured,        &
                              format_struct_vtk,format_struct_sep,      &
                              array,narray,str_representation,          &
                              str_folder,fieldname,fieldname_gb,        &
                              ctx_err,o_cste_order)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    logical                     ,intent(in)   :: save_unstructured
    logical                     ,intent(in)   :: save_structured
    logical                     ,intent(in)   :: format_struct_vtk
    logical                     ,intent(in)   :: format_struct_sep
    real(kind=4),allocatable    ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb
    type(t_error)               ,intent(inout):: ctx_err
    integer,          optional  ,intent(in)   :: o_cste_order
    !! local
    !! -----------------------------------------------------------------
    logical      :: flag_cste_order
    integer      :: cste_order
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    cste_order = -1
    flag_cste_order = .false.
    if(present(o_cste_order)) then
      cste_order=o_cste_order
      flag_cste_order = .true.
    end if
    
    !! get the fieldnames 
    !! -----------------------------------------------------------------
    !! structured save
    !! -----------------------------------------------------------------
    if(save_structured) then
      call array_save_structured(ctx_paral,ctx_domain,ctx_disc,            &
                                array,narray,str_representation,fieldname, &
                                fieldname_gb,str_folder,                   &
                                format_struct_vtk,format_struct_sep,       &
                                cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! unstructured save
    !! -----------------------------------------------------------------  
    if(save_unstructured) then
      call array_save_unstructured(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,narray,str_representation,     &
                                   fieldname,fieldname_gb,str_folder,   &
                                   flag_cste_order,cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> api to save array for structured and unstructured solutions
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_dg            : type DG
  !> @param[in]    save_structured   : indicate if save structured
  !> @param[in]    save_unstructured : indicate if save unstructured
  !> @param[in]    format_struct_vtk : indicate if save structured vtk
  !> @param[in]    format_struct_sep : indicate if save structured sep
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname_gb      : name of the field, size narray 
  !> @param[in]    basename          : global name (e.g., for vtk)
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_real8(ctx_paral,ctx_domain,ctx_disc,            &
                              save_unstructured,save_structured,        &
                              format_struct_vtk,format_struct_sep,      &
                              array,narray,str_representation,          &
                              str_folder,fieldname,fieldname_gb,        &
                              ctx_err,o_cste_order)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    logical                     ,intent(in)   :: save_unstructured
    logical                     ,intent(in)   :: save_structured
    logical                     ,intent(in)   :: format_struct_vtk
    logical                     ,intent(in)   :: format_struct_sep
    real(kind=8),allocatable    ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb
    type(t_error)               ,intent(inout):: ctx_err
    integer,          optional  ,intent(in)   :: o_cste_order
    !! local
    !! -----------------------------------------------------------------
    logical      :: flag_cste_order
    integer      :: cste_order
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    cste_order = -1
    flag_cste_order = .false.
    if(present(o_cste_order)) then
      cste_order=o_cste_order
      flag_cste_order = .true.
    end if

    !! get the fieldnames 
    !! -----------------------------------------------------------------
    !! structured save
    !! -----------------------------------------------------------------
    if(save_structured) then
      call array_save_structured(ctx_paral,ctx_domain,ctx_disc,            &
                                array,narray,str_representation,fieldname, &
                                fieldname_gb,str_folder,                   &
                                format_struct_vtk,format_struct_sep,       &
                                cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! unstructured save
    !! -----------------------------------------------------------------  
    if(save_unstructured) then
      call array_save_unstructured(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,narray,str_representation,     &
                                   fieldname,fieldname_gb,str_folder,   &
                                   flag_cste_order,cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_real8


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> api to save array for structured and unstructured solutions
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_dg            : type DG
  !> @param[in]    save_structured   : indicate if save structured
  !> @param[in]    save_unstructured : indicate if save unstructured
  !> @param[in]    format_struct_vtk : indicate if save structured vtk
  !> @param[in]    format_struct_sep : indicate if save structured sep
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname_gb      : name of the field, size narray 
  !> @param[in]    basename          : global name (e.g., for vtk)
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_complex4(ctx_paral,ctx_domain,ctx_disc,         &
                                 save_unstructured,save_structured,     &
                                 format_struct_vtk,format_struct_sep,   &
                                 array,narray,str_representation,       &
                                 str_folder,fieldname,fieldname_gb,     &
                                 ctx_err,o_cste_order)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    logical                     ,intent(in)   :: save_unstructured
    logical                     ,intent(in)   :: save_structured
    logical                     ,intent(in)   :: format_struct_vtk
    logical                     ,intent(in)   :: format_struct_sep
    complex(kind=4),allocatable ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb
    type(t_error)               ,intent(inout):: ctx_err
    integer,          optional  ,intent(in)   :: o_cste_order
    !! local
    !! -----------------------------------------------------------------
    logical      :: flag_cste_order
    integer      :: cste_order
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    cste_order = -1
    flag_cste_order = .false.
    if(present(o_cste_order)) then
      cste_order=o_cste_order
      flag_cste_order = .true.
    end if

    !! get the fieldnames 
    !! -----------------------------------------------------------------
    !! structured save
    !! -----------------------------------------------------------------
    if(save_structured) then
      call array_save_structured(ctx_paral,ctx_domain,ctx_disc,            &
                                array,narray,str_representation,fieldname, &
                                fieldname_gb,str_folder,                   &
                                format_struct_vtk,format_struct_sep,       &
                                cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! unstructured save
    !! -----------------------------------------------------------------  
    if(save_unstructured) then
      call array_save_unstructured(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,narray,str_representation,     &
                                   fieldname,fieldname_gb,str_folder,   &
                                   flag_cste_order,cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_complex4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> api to save array for structured and unstructured solutions
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[in]    ctx_domain        : type domain (mesh, cartesian infos)
  !> @param[in]    ctx_dg            : type DG
  !> @param[in]    save_structured   : indicate if save structured
  !> @param[in]    save_unstructured : indicate if save unstructured
  !> @param[in]    format_struct_vtk : indicate if save structured vtk
  !> @param[in]    format_struct_sep : indicate if save structured sep
  !> @param[in]    array             : array to be saved of size (:,narray)
  !> @param[in]    narray            : number of array to be saved ,fieldname, 
  !> @param[in]    str_representation: representation for mesh (dof or cell)
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname_gb      : name of the field, size narray 
  !> @param[in]    basename          : global name (e.g., for vtk)
  !> @param[inout] ctx_err           : context error
  !----------------------------------------------------------------------------
  subroutine array_save_complex8(ctx_paral,ctx_domain,ctx_disc,         &
                                 save_unstructured,save_structured,     &
                                 format_struct_vtk,format_struct_sep,   &
                                 array,narray,str_representation,       &
                                 str_folder,fieldname,fieldname_gb,     &
                                 ctx_err,o_cste_order)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    type(t_domain)              ,intent(in)   :: ctx_domain
    type(t_discretization)      ,intent(in)   :: ctx_disc
    logical                     ,intent(in)   :: save_unstructured
    logical                     ,intent(in)   :: save_structured
    logical                     ,intent(in)   :: format_struct_vtk
    logical                     ,intent(in)   :: format_struct_sep
    complex(kind=8),allocatable ,intent(in)   :: array(:,:)
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_representation
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    character(len=*)            ,intent(in)   :: fieldname_gb
    type(t_error)               ,intent(inout):: ctx_err
    integer,          optional  ,intent(in)   :: o_cste_order
    !! local
    !! -----------------------------------------------------------------
    logical      :: flag_cste_order
    integer      :: cste_order
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    cste_order = -1
    flag_cste_order = .false.
    if(present(o_cste_order)) then
      cste_order=o_cste_order
      flag_cste_order = .true.
    end if

    !! get the fieldnames 
    !! -----------------------------------------------------------------
    !! structured save
    !! -----------------------------------------------------------------
    if(save_structured) then
      call array_save_structured(ctx_paral,ctx_domain,ctx_disc,            &
                                array,narray,str_representation,fieldname, &
                                fieldname_gb,str_folder,                   &
                                format_struct_vtk,format_struct_sep,       &
                                cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    
    
    !! -----------------------------------------------------------------
    !! unstructured save
    !! -----------------------------------------------------------------  
    if(save_unstructured) then
      call array_save_unstructured(ctx_paral,ctx_domain,ctx_disc,       &
                                   array,narray,str_representation,     &
                                   fieldname,fieldname_gb,str_folder,   &
                                   flag_cste_order,cste_order,ctx_err)
    end if
    !! -----------------------------------------------------------------
    return
  end subroutine array_save_complex8
end module m_array_save
