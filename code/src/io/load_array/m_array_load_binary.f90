!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_array_load_binary.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for IO when we have to load a binary array 
!
!------------------------------------------------------------------------------
module m_array_load_binary

  !! module used -------------------------------------------------------
  use m_raise_error,            only: raise_error, t_error
  use m_ctx_parallelism,        only: t_parallelism
  !! -------------------------------------------------------------------
  implicit none

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads an array with given filename, adding the possibility
  !> so that only the master reads it
  !---------------------------------------------------------------------
  interface array_load_binary
     module procedure array_load_binary_single_real4
     module procedure array_load_binary_single_real8
     module procedure array_load_binary_multiple_real4
     module procedure array_load_binary_multiple_real8
     module procedure array_load_binary_single_real4_int8
     module procedure array_load_binary_single_real8_int8
     module procedure array_load_binary_multiple_real4_int8
     module procedure array_load_binary_multiple_real8_int8
  end interface
  !---------------------------------------------------------------------

  private
  public  :: array_load_binary

  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads an array with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : name of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_single_real4(ctx_paral,array,size_array, &
                                            str_folder,fieldname,       &
                                            flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=4)                ,intent(inout):: array(:)
    integer                     ,intent(in)   :: size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*)            ,intent(in)   :: fieldname
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    fullname = trim(adjustl(str_folder))//"/"//trim(adjustl(fieldname))//".bin"
    !! check file exists
    inquire(file=trim(adjustl(fullname)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //       &
                   " does not exist [array_load_binary] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',&
              form='unformatted',recl=4*size_array)
        read (unit=unit_io,rec=1) array
        close(unit=unit_io)
      end if
    else !! everybody reads
      open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',  &
            form='unformatted',recl=4*size_array)
      read (unit=unit_io,rec=1) array
      close(unit=unit_io)
    end if
    return
  end subroutine array_load_binary_single_real4
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads an array with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : name of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_single_real8(ctx_paral,array,size_array, &
                                            str_folder,fieldname,       &
                                            flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=8)                ,intent(inout):: array(:)
    integer                     ,intent(in)   :: size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*)            ,intent(in)   :: fieldname
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    fullname = trim(adjustl(str_folder))//"/"//trim(adjustl(fieldname))//".bin"
    !! check file exists
    inquire(file=trim(adjustl(fullname)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //       &
                   " does not exist [array_load_binary] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',&
              form='unformatted',recl=8*size_array)
        read (unit=unit_io,rec=1) array
        close(unit=unit_io)
      end if
    else !! everybody reads
      open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',  &
            form='unformatted',recl=8*size_array)
      read (unit=unit_io,rec=1) array
      close(unit=unit_io)
    end if
    return
  end subroutine array_load_binary_single_real8


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads multiple arrays with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    narray            : number of array
  !> @param[in]    size_array        : size of the arrays
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : names of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_multiple_real4(ctx_paral,array,narray,   &
                                              size_array,str_folder,    &
                                              fieldname,                &
                                              flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=4)    ,allocatable,intent(inout):: array(:,:)
    integer                     ,intent(in)   :: size_array,narray
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: k, unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! check file exists
    do k=1,narray
      fullname = trim(adjustl(str_folder))//"/"//                       &
                 trim(adjustl(fieldname(k)))//".bin"
      inquire(file=trim(adjustl(fullname)),exist=file_exists)
      if(.not. file_exists) then
        ctx_err%ierr=-1
        ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //     &
                     " does not exist [array_load_binary] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end do
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        do k=1,narray
          fullname = trim(adjustl(str_folder))//"/"//                   &
                     trim(adjustl(fieldname(k)))//".bin"
          open (unit=unit_io,file=trim(adjustl(fullname)),              &
                access='direct',form='unformatted',recl=4*size_array)
          read (unit=unit_io,rec=1) array(:,k)
          close(unit=unit_io)
        end do
      end if
    else !! everybody reads
      do k=1,narray
        fullname = trim(adjustl(str_folder))//"/"//                     &
                   trim(adjustl(fieldname(k)))//".bin"
        open (unit=unit_io,file=trim(adjustl(fullname)),                &
              access='direct',form='unformatted',recl=4*size_array)
        read (unit=unit_io,rec=1) array(:,k)
        close(unit=unit_io)
      end do
    end if
    return
  end subroutine array_load_binary_multiple_real4

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads multiple arrays with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    narray            : number of array
  !> @param[in]    size_array        : size of the arrays
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : names of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_multiple_real8(ctx_paral,array,narray,   &
                                              size_array,str_folder,    &
                                              fieldname,                &
                                              flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=8)    ,allocatable,intent(inout):: array(:,:)
    integer                     ,intent(in)   :: size_array,narray
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: k, unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! check file exists
    do k=1,narray
      fullname = trim(adjustl(str_folder))//"/"//                       &
                 trim(adjustl(fieldname(k)))//".bin"
      inquire(file=trim(adjustl(fullname)),exist=file_exists)
      if(.not. file_exists) then
        ctx_err%ierr=-1
        ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //     &
                     " does not exist [array_load_binary] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end do
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        do k=1,narray
          fullname = trim(adjustl(str_folder))//"/"//                   &
                     trim(adjustl(fieldname(k)))//".bin"
          open (unit=unit_io,file=trim(adjustl(fullname)),              &
                access='direct',form='unformatted',recl=8*size_array)
          read (unit=unit_io,rec=1) array(:,k)
          close(unit=unit_io)
        end do
      end if
    else !! everybody reads
      do k=1,narray
        fullname = trim(adjustl(str_folder))//"/"//                     &
                   trim(adjustl(fieldname(k)))//".bin"
        open (unit=unit_io,file=trim(adjustl(fullname)),                &
              access='direct',form='unformatted',recl=8*size_array)
        read (unit=unit_io,rec=1) array(:,k)
        close(unit=unit_io)
      end do
    end if
    return
  end subroutine array_load_binary_multiple_real8

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads an array with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : name of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_single_real4_int8(ctx_paral,array,size_array, &
                                            str_folder,fieldname,       &
                                            flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=4)                ,intent(inout):: array(:)
    integer(kind=8)             ,intent(in)   :: size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*)            ,intent(in)   :: fieldname
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    fullname = trim(adjustl(str_folder))//"/"//trim(adjustl(fieldname))//".bin"
    !! check file exists
    inquire(file=trim(adjustl(fullname)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //       &
                   " does not exist [array_load_binary] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',&
              form='unformatted',recl=4*size_array)
        read (unit=unit_io,rec=1) array
        close(unit=unit_io)
      end if
    else !! everybody reads
      open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',  &
            form='unformatted',recl=4*size_array)
      read (unit=unit_io,rec=1) array
      close(unit=unit_io)
    end if
    return
  end subroutine array_load_binary_single_real4_int8
  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads an array with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    size_array        : size of the array
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : name of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_single_real8_int8(ctx_paral,array,size_array, &
                                            str_folder,fieldname,       &
                                            flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=8)                ,intent(inout):: array(:)
    integer(kind=8)             ,intent(in)   :: size_array
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*)            ,intent(in)   :: fieldname
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    fullname = trim(adjustl(str_folder))//"/"//trim(adjustl(fieldname))//".bin"
    !! check file exists
    inquire(file=trim(adjustl(fullname)),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //       &
                   " does not exist [array_load_binary] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',&
              form='unformatted',recl=8*size_array)
        read (unit=unit_io,rec=1) array
        close(unit=unit_io)
      end if
    else !! everybody reads
      open (unit=unit_io,file=trim(adjustl(fullname)),access='direct',  &
            form='unformatted',recl=8*size_array)
      read (unit=unit_io,rec=1) array
      close(unit=unit_io)
    end if
    return
  end subroutine array_load_binary_single_real8_int8


  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads multiple arrays with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    narray            : number of array
  !> @param[in]    size_array        : size of the arrays
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : names of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_multiple_real4_int8(ctx_paral,array,narray,   &
                                              size_array,str_folder,    &
                                              fieldname,                &
                                              flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=4)    ,allocatable,intent(inout):: array(:,:)
    integer(kind=8)             ,intent(in)   :: size_array
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: k, unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! check file exists
    do k=1,narray
      fullname = trim(adjustl(str_folder))//"/"//                       &
                 trim(adjustl(fieldname(k)))//".bin"
      inquire(file=trim(adjustl(fullname)),exist=file_exists)
      if(.not. file_exists) then
        ctx_err%ierr=-1
        ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //     &
                     " does not exist [array_load_binary] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end do
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        do k=1,narray
          fullname = trim(adjustl(str_folder))//"/"//                   &
                     trim(adjustl(fieldname(k)))//".bin"
          open (unit=unit_io,file=trim(adjustl(fullname)),              &
                access='direct',form='unformatted',recl=4*size_array)
          read (unit=unit_io,rec=1) array(:,k)
          close(unit=unit_io)
        end do
      end if
    else !! everybody reads
      do k=1,narray
        fullname = trim(adjustl(str_folder))//"/"//                     &
                   trim(adjustl(fieldname(k)))//".bin"
        open (unit=unit_io,file=trim(adjustl(fullname)),                &
              access='direct',form='unformatted',recl=4*size_array)
        read (unit=unit_io,rec=1) array(:,k)
        close(unit=unit_io)
      end do
    end if
    return
  end subroutine array_load_binary_multiple_real4_int8

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> it loads multiple arrays with given filename, adding the possibility
  !> so that only the master reads it
  !
  !> @param[in]    ctx_paral         : type parallelism
  !> @param[inout] array             : array to load
  !> @param[in]    narray            : number of array
  !> @param[in]    size_array        : size of the arrays
  !> @param[in]    str_folder        : path to the file to be saved
  !> @param[in]    fieldname         : names of the file
  !> @param[in]    flag_master_only  : only the master keeps it
  !> @param[inout] ctx_err           : context error
  !---------------------------------------------------------------------
  subroutine array_load_binary_multiple_real8_int8(ctx_paral,array,narray,   &
                                              size_array,str_folder,    &
                                              fieldname,                &
                                              flag_master_only,ctx_err)
    implicit none
    type(t_parallelism)         ,intent(in)   :: ctx_paral
    real(kind=8)    ,allocatable,intent(inout):: array(:,:)
    integer(kind=8)             ,intent(in)   :: size_array
    integer                     ,intent(in)   :: narray
    character(len=*)            ,intent(in)   :: str_folder
    character(len=*),allocatable,intent(in)   :: fieldname(:)
    logical                     ,intent(in)   :: flag_master_only
    type(t_error)               ,intent(inout):: ctx_err
    !! local
    !! -----------------------------------------------------------------
    character(len=1024):: fullname
    integer            :: k, unit_io = 1011
    logical            :: file_exists
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0
    
    !! check file exists
    do k=1,narray
      fullname = trim(adjustl(str_folder))//"/"//                       &
                 trim(adjustl(fieldname(k)))//".bin"
      inquire(file=trim(adjustl(fullname)),exist=file_exists)
      if(.not. file_exists) then
        ctx_err%ierr=-1
        ctx_err%msg ="** Binary file "// trim(adjustl(fullname)) //     &
                     " does not exist [array_load_binary] "
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
    end do
    
    !! read file: master only or everyone
    if(flag_master_only) then
      if(ctx_paral%master) then
        do k=1,narray
          fullname = trim(adjustl(str_folder))//"/"//                   &
                     trim(adjustl(fieldname(k)))//".bin"
          open (unit=unit_io,file=trim(adjustl(fullname)),              &
                access='direct',form='unformatted',recl=8*size_array)
          read (unit=unit_io,rec=1) array(:,k)
          close(unit=unit_io)
        end do
      end if
    else !! everybody reads
      do k=1,narray
        fullname = trim(adjustl(str_folder))//"/"//                     &
                   trim(adjustl(fieldname(k)))//".bin"
        open (unit=unit_io,file=trim(adjustl(fullname)),                &
              access='direct',form='unformatted',recl=8*size_array)
        read (unit=unit_io,rec=1) array(:,k)
        close(unit=unit_io)
      end do
    end if
    return
  end subroutine array_load_binary_multiple_real8_int8


end module m_array_load_binary
