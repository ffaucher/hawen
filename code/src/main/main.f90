!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! PROGRAM main.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the main program fortran file
!
!------------------------------------------------------------------------------
program main
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_check_argument,  only: check_input_argument
  use m_parallelism_init,only: init_parallelism, end_parallelism
  use m_ctx_parallelism, only: t_parallelism
  use m_time,            only: time
  use m_print_basics,    only: cast_time
  use m_process,         only: process
  !! -------------------------------------------------------------------
  implicit none
  !! -------------------------------------------------------------------
  !! local
  type(t_parallelism)   :: paral
  type(t_error)         :: ctx_err
  character(len=512)    :: parameter_file
  character(len=64)     :: str_time
  logical               :: check_param
  real(kind=8)          :: time0,time1
  !! initialize error context ------------------------------------------
  ctx_err%ierr  =0
  ctx_err%msg   =""
  ctx_err%critic=.false.
  
  !! initialize mpi ----------------------------------------------------
  call init_parallelism(paral,ctx_err)
  !! check input arguments ---------------------------------------------
  check_param=.false.
  call check_input_argument(paral,parameter_file,check_param,ctx_err)
  !! run process -------------------------------------------------------
  call time(time0)
  if(check_param) then
    write(6,'(a)') " ********** error ********** "
    write(6,'(a)') " Checking the parameter file only is not yet implemented"
    write(6,'(a)') " *************************** "
    ! call check_parameter_file
  else
    call process(paral,parameter_file,ctx_err)
  endif
  call time(time1)
  if(paral%master) then
    call cast_time((time1-time0),str_time)
    write(6,'(a)')          " Program ends "
    write(6,'(2a)') " Total time : " , trim(adjustl(str_time))
    write(6,'(a)')    " ------------------------------------------------- "
  endif
  !! end mpi -----------------------------------------------------------
  call end_parallelism(paral,ctx_err)
    
end program main
