!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex3d_read_tetgen.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the domain mesh files (from tetgen in 3D)
!
!------------------------------------------------------------------------------
module m_mesh_simplex3d_read_tetgen
  
  use m_define_precision,       only : IKIND_MESH, RKIND_MESH
  use m_broadcast,              only : broadcast  
  use m_time,                   only : time
  use m_raise_error,            only : raise_error, t_error
  use m_distribute_error,       only : distribute_error
  use m_ctx_domain,             only : t_domain
  use m_mesh_simplex_interface, only : mesh_interface_simplex
  use m_ctx_parallelism,        only : t_parallelism
                                     
  implicit none

  private  
  public :: mesh_read_tetgen, mesh_read_init_tetgen
   
  contains  



  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine reads the input 3D mesh files in TETGEN format
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read_tetgen(ctx_paral,mesh,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: mesh
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH)  , allocatable :: inter_node(:,:)
    integer                   , allocatable :: mark_inter(:)
    logical                                 :: file_exists
    integer                                 :: cline,mark
    integer                                 :: unit_node=120, unit_ele =121
    integer                                 :: unit_face=122, unit_neig=123
                                            !! tetrahedron mesh
    real   (kind=8)                         :: node_coo  (3) ! 3 coo per node
    integer(kind=IKIND_MESH)                :: node_cell (4) ! 4 node per cell
    integer(kind=IKIND_MESH)                :: face_node (3) ! 4 node per face
    integer(kind=IKIND_MESH)                :: node_neigh(4) ! 4 neighbors
    integer(kind=IKIND_MESH)                :: k
    real   (kind=8)                         :: time0,time1

    ctx_err%ierr  = 0
    
    !! general informations for triangle meshes
    mesh%format_element         ='tetrahedron'
    mesh%n_node_per_cell        =4 ! 4 nodes per tetrahedron
    mesh%n_node_per_face        =3 ! 3 nodes define the tetrahedron face
    mesh%n_neigh_per_cell       =4 ! 4 neighbors per tetrahedron
    mesh%n_node_per_face_shared =3
    !! interface is a triangle, for two adjacents there are 4 possible
    !! orientations (see context_dg for more infos)
    mesh%n_interface_combi      =4 
    
    call time(time0)
    !! check that all files exist --------------------------------------
    !! .edge ; .ele ; .face ; .neigh ; .node
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.node'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.node')   //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.ele'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.ele')    //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    !! unused ?
    !! inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.edge'),exist=file_exists)
    !! if(.not. file_exists) then
    !!   ctx_err%ierr=-1
    !!   ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //&
    !!                 trim(trim(adjustl(mesh%file_mesh))//'.edge')   //&
    !!                " DOEST NOT EXIST [mesh_read_tetgen] "
    !!   ctx_err%critic=.true.
    !! endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.neigh')  //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.face'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.face')   //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    if(ctx_err%ierr.ne.0) call raise_error(ctx_err)
    !! end check files exist -------------------------------------------

    !! Open files
    open (unit=unit_node,file=trim(trim(adjustl(mesh%file_mesh))//'.node' ))
    open (unit=unit_ele ,file=trim(trim(adjustl(mesh%file_mesh))//'.ele'  ))
    open (unit=unit_face,file=trim(trim(adjustl(mesh%file_mesh))//'.face' ))
    open (unit=unit_neig,file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'))
    !! Read number of nodes and elements and edges
    read(unit_node,*) mesh%n_node_gb
    read(unit_ele ,*) mesh%n_cell_gb
    read(unit_face,*) mesh%n_face_gb 
    read(unit_neig,*) cline !! garbage  
   
    !! -------------------------------------------------------------------
    !! Only the master is going to read the global information on the mesh
    !! -------------------------------------------------------------------
    if(ctx_paral%master) then
      !! allocation
      allocate(mesh%x_node    (mesh%dim_domain      ,mesh%n_node_gb))
      allocate(mesh%cell_node (mesh%n_node_per_cell ,mesh%n_cell_gb))
      allocate(mesh%cell_neigh(mesh%n_neigh_per_cell,mesh%n_cell_gb))
      allocate(mesh%cell_face (mesh%n_neigh_per_cell,mesh%n_cell_gb))
      allocate(inter_node(mesh%n_node_per_face,mesh%n_face_gb))
      allocate(mark_inter(mesh%n_face_gb))      
      mesh%cell_face = 0

      !! a) nodes --------------------------------------------------------
      do k=1,mesh%n_node_gb
        read(unit_node,*) cline,  node_coo(:)
        mesh%x_node   (:,cline) = real(node_coo(:),kind=RKIND_MESH)
        !! mesh%mark_node(cline)   = mark
      end do
      !! b) elem  ------------------------------------------------------
      do k=1,mesh%n_cell_gb
         read(unit_ele ,*) cline, node_cell(:)
         mesh%cell_node (:,cline) = node_cell (:)
         read(unit_neig,*) cline, node_neigh(:)
         mesh%cell_neigh(:,cline) = node_neigh(:)
      end do
      !! c) face  ------------------------------------------------------
      do k=1,mesh%n_face_gb
         read(unit_face,*) cline, face_node(:), mark
         inter_node(:,cline) = face_node(:)
         mark_inter(cline)   = mark
      end do
     
      !! get interface informations in 3D
      call mesh_interface_simplex(3,mesh%n_cell_gb,                          &
                                  mesh%n_neigh_per_cell,mesh%n_face_gb,      &
                                  mesh%cell_node,                            &
                                  mesh%cell_neigh,mesh%cell_face,inter_node, &
                                  mark_inter,mesh%tag_n,                     &
                                  mesh%tag_bc_array,                         &
                                  mesh%n_face_interior_gb,                   &
                                  mesh%n_face_boundary_gb,ctx_err)

      !! check count
      if(mesh%n_face_interior_gb+mesh%n_face_boundary_gb.ne.mesh%n_face_gb) then
        ctx_err%ierr=-1
        ctx_err%msg =trim(adjustl(ctx_err%msg)) //                      &
                     "** ERROR: Incorrect number of edges found " //    &
                     " [mesh_read_tetgen] **"
        ctx_err%critic=.true.
      end if

      deallocate(inter_node)
      deallocate(mark_inter)
      
      !! bound coordinates
      mesh%bounds_xmin=minval(mesh%x_node(1,:))
      mesh%bounds_xmax=maxval(mesh%x_node(1,:))
      mesh%bounds_ymin=minval(mesh%x_node(2,:))
      mesh%bounds_ymax=maxval(mesh%x_node(2,:))
      mesh%bounds_zmin=minval(mesh%x_node(3,:))
      mesh%bounds_zmax=maxval(mesh%x_node(3,:))

    end if    
    !! -------------------------------------------------------------------
    !! only the master has read the informations
    !! -------------------------------------------------------------------
    !! close files
    close(unit_node)
    close(unit_ele)
    close(unit_face)
    close(unit_neig)

    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 

    call broadcast(mesh%bounds_xmin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_xmax,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_ymin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_ymax,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_zmin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_zmax,1,0,ctx_paral%communicator,ctx_err)
    call time(time1)
    mesh%time_read = time1-time0
    
    return
  end subroutine mesh_read_tetgen
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine reads the input 3D mesh files in tetgen format
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read_init_tetgen(ctx_paral,mesh,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: mesh
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    logical     :: file_exists
    integer     :: cline,ntag_face
    integer     :: unit_node=120, unit_ele =121
    integer     :: unit_face=122, unit_neig=123
    real(kind=8):: time0,time1
    integer(kind=IKIND_MESH) :: ncell_loc, nface_loc
    integer     :: int_size       = 4
    integer     :: int_size_mesh  = IKIND_MESH
    integer     :: real_size      = 4
    integer     :: real_size_mesh = RKIND_MESH
    integer(kind=8) :: mem_count

    ctx_err%ierr  = 0
    call time(time0)

    !! general informations
    mesh%format_element  ='tetrahedron'
    mesh%n_node_per_cell =4 ! 4 nodes per tetrahedron
    mesh%n_node_per_face =3 ! 3 nodes define the tetrahedron face
    mesh%n_neigh_per_cell=4 ! 4 neighbors per tetrahedron
    !! interface is a triangle, for two adjacents there are 4 possible
    !! orientations (see context_dg for more infos)
    mesh%n_interface_combi=4 
    
    !! check that all files exist --------------------------------------
    !! .edge ; .ele ; .face ; .neigh ; .node
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.node'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.node')   //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.ele'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.ele')    //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    !! unused ?
    !! inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.edge'),exist=file_exists)
    !! if(.not. file_exists) then
    !!   ctx_err%ierr=-1
    !!   ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //&
    !!                 trim(trim(adjustl(mesh%file_mesh))//'.edge')   //&
    !!                " DOEST NOT EXIST [mesh_read_tetgen] "
    !!   ctx_err%critic=.true.
    !! endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.neigh')  //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.face'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.face')   //   &
                   " DOEST NOT EXIST [mesh_read_tetgen] "
      ctx_err%critic=.true.
    endif
    if(ctx_err%ierr.ne.0) call raise_error(ctx_err)
    !! end check files exist -------------------------------------------

    !! Open files
    open (unit=unit_node,file=trim(trim(adjustl(mesh%file_mesh))//'.node' ))
    open (unit=unit_ele ,file=trim(trim(adjustl(mesh%file_mesh))//'.ele'  ))
    open (unit=unit_face,file=trim(trim(adjustl(mesh%file_mesh))//'.face' ))
    open (unit=unit_neig,file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'))
    !! Read number of nodes and elements and edges
    read(unit_node,*) mesh%n_node_gb
    read(unit_ele ,*) mesh%n_cell_gb
    read(unit_face,*) mesh%n_face_gb, ntag_face
    read(unit_neig,*) cline !! garbage
    !! close files
    close(unit_node)
    close(unit_ele)
    close(unit_face)
    close(unit_neig)
    
    !! We must have One tag per face for boundary conditions 
    if(ntag_face < 1) then
      ctx_err%ierr=-1
      ctx_err%msg ="** Tetgen mesh file .face must have one tag"   //  &
                   " for boundary conditions (very first line)"    //  &
                   " [mesh_read_tetgen] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if

    !! anticipate memory requirement

    mem_count=0

    !! memory peark is during partitioning 
    mem_count =(9*int_size_mesh)                 + &  !! 8 integers parameters
               (6*real_size_mesh)                + &  !! bounding box
               (6*int_size)                      + &  !! 6 integers 
               (8*2*real_size)                   + &  !! 8 real (kind=8)
               (1*256)                           + &  !! 1 character(len=256)
               (1*32)                            + &  !! 2 character(len=32)
               (3*real_size*10)                  + &  !! ~ 10 tags array
               !! mesh%x_node
               ((mesh%n_node_gb)*real_size_mesh) + &
               !! mesh%cell_node 
               ((mesh%n_cell_gb*mesh%n_node_per_cell)*int_size_mesh)  + &
               !! mesh%neigh 
               ((mesh%n_cell_gb*mesh%n_neigh_per_cell)*int_size_mesh) + &
               !! mesh%cell_face
               ((mesh%n_cell_gb*mesh%n_neigh_per_cell)*int_size_mesh) + &
               !! mesh%cell_bary
               ((mesh%n_cell_gb*mesh%dim_domain)*real_size_mesh)      + &
               !! mesh%order
               ((mesh%n_cell_gb)*int_size) + ((mesh%n_face_gb)*int_size)


    !! peak when reading, in monoproc we need additional array
    mesh%memory_peak_read = mem_count +  &
               !! interface node and interface markers
               ((mesh%n_face_gb*mesh%n_node_per_cell)*int_size_mesh) + &
               ((mesh%n_face_gb                     )*int_size)
    
    !! peak when partitioning, we have simultaneously monoproc memory
    !! and additional requirement
    mesh%memory_peak_part = mem_count        +  &
        ((mesh%n_cell_gb*int_size  ))        +  &  !! proc cell
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! cells_pproc 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! ghost_pproc  
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! offset_cells 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! offset_ghost 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! counter_cells
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! counter_ghost
        ((mesh%n_cell_gb*int_size_mesh))     +  &  !! ordered_cellindex
        ((mesh%n_cell_gb*mesh%n_node_per_cell *int_size_mesh))+& !ordered_cellnode
        ((mesh%n_cell_gb*mesh%n_neigh_per_cell*int_size_mesh))+& !ordered_cellneigh
        ((mesh%n_cell_gb*mesh%n_neigh_per_cell*int_size_mesh))+& !ordered_cellface
        ((mesh%n_cell_gb*mesh%dim_domain)*real_size_mesh) + & !! barycenters
        ((mesh%n_cell_gb*int_size))          +  &  !! ordered_cellorder
        ((mesh%n_node_gb)*real_size_mesh)    +  &  !! temp_xnode
        ((mesh%n_node_gb)*int_size_mesh)     +  &  !! index_loctoglob_node
        ((mesh%n_face_gb)*int_size_mesh)     +  &  !! index_loctoglob_face
        !! another cell_node required at partitioning
        ((mesh%n_cell_gb*mesh%n_node_per_cell)*int_size_mesh)

    !! approximate memory peak on slave assuming good partitioning
    !! peak when partitioning, we have simultaneously monoproc memory
    !! and other proc infos 
    if(ctx_paral%nproc == 1) then
      ncell_loc = mesh%n_cell_gb
      nface_loc = mesh%n_face_gb
    else
      ncell_loc = int(1.1*ceiling(real(mesh%n_cell_gb) / real(ctx_paral%nproc)))
      nface_loc = int(1.4*ceiling(real(mesh%n_face_gb) / real(ctx_paral%nproc)))
    end if

    mesh%memory_peak_part = mesh%memory_peak_part +  &
        ((ncell_loc*int_size)               )     +  & !! ordered_ghostorder
        ((ncell_loc*mesh%n_node_per_cell))        +  & !! array_itemp
        !! cell_face_orientation
        ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh)
    
    !! add ctx_mesh corresponding to every non-master proc 
    mesh%memory_peak_other =  (9*int_size_mesh)    + &  !! 8 integers parameters
               (6*real_size_mesh)                  + &  !! bounding box
               (6*int_size)                        + &  !! 6 integers 
               (8*2*real_size)                     + &  !! 8 real (kind=8)
               (1*256)                             + &  !! 1 character(len=256)
               (1*32)                              + &  !! 2 character(len=32)
               (3*real_size*10)                    + &  !! ~ 10 tags array
               ((ncell_loc)*real_size_mesh)        + &  !! mesh%x_node
               !! mesh%cell_node is counted twice for ghost infos during 
               !! partitioning
               ((ncell_loc*mesh%n_node_per_cell)*int_size_mesh)*2+ & 
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! mesh%neigh 
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! mesh%cell_face
               ((ncell_loc)*int_size)               +  &  !! mesh%order
               ((mesh%n_node_gb)*real_size_mesh)    +  &  !! temp_xnode
               ((mesh%n_node_gb)*int_size_mesh)     +  &  !! index_loctoglob_node
               ((nface_loc)*int_size_mesh)          +  &  !! index_loctoglob_face
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! local_cell_face
               ((nface_loc)*int_size)               +  &  !! order_face
               !! cell_face_orientation
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh)

    call time(time1)
    mesh%time_init = time1-time0
    
    return
  end subroutine mesh_read_init_tetgen

end module m_mesh_simplex3d_read_tetgen
