!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex_compute_faces.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for simplex mesh to compute all faces
!> from the list of cell and nodes
!
!------------------------------------------------------------------------------
module m_mesh_simplex_compute_faces

  use omp_lib
  use m_define_precision,       only : IKIND_MESH, RKIND_MESH
  use m_raise_error,            only : raise_error, t_error
  use m_mesh_simplex_interface, only : interface_2D_triangle,           &
                                       interface_3D_tetrahedron

  implicit none


  private
  public :: mesh_simplex_faces_triangle,mesh_simplex_faces_tetra
  public :: mesh_simplex_faces_tetra_hash

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve all faces of all cells
  !
  !> @param[in]    n_cells              : total number of cells
  !> @param[in]    n_face_bdry          : total number of boundary faces
  !> @param[in]    cell_node            : array of the cell's node    [3,ncells]
  !> @param[in]    list_face_boundary   : array of the boundary faces
  !> @param[in]    list_marker_boundary : array of marker
  !> @param[inout] list_face            : array of all faces
  !> @param[inout] list_marker          : array of all markers
  !> @param[out]   ctx_err              : error context
  !----------------------------------------------------------------------------
  subroutine mesh_simplex_faces_triangle(n_cells,n_face_bdry,cell_node, &
                                         list_face_boundary,            &
                                         list_marker_boundary,list_face,&
                                         list_marker,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)   :: n_cells,n_face_bdry
    integer(kind=IKIND_MESH), allocatable, intent(in)   :: cell_node         (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(in)   :: list_face_boundary(:,:)
    integer                 , allocatable, intent(in)   :: list_marker_boundary(:)
    integer(kind=IKIND_MESH), allocatable, intent(inout):: list_face(:,:)
    integer                 , allocatable, intent(inout):: list_marker(:)
    type(t_error)                        , intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh
    integer(kind=IKIND_MESH)              :: i_face,counter
    integer(kind=IKIND_MESH)              :: nneigh=3
    integer(kind=IKIND_MESH)              :: face_node_try(2)
    integer(kind=IKIND_MESH)              :: face_node    (2)
    logical                               :: flag_found

    ctx_err%ierr = 0

    !! we first copy the boundary informations
    list_marker                 =0
    list_marker(1:n_face_bdry)  =list_marker_boundary(:)
    list_face  (:,:)            =0
    list_face  (:,1:n_face_bdry)=list_face_boundary  (:,:)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! loop over all cells
    !!
    !! $OMP PARALLEL would require a critical and may cause trouble
    !! when looking in an array that is constantly increasing
    !!
    !! -----------------------------------------------------------------
    counter    = n_face_bdry
    do icell=1, n_cells

      !! loop over all face of this cell
      do ineigh=1, nneigh

        !! get the two nodes (triangle face) of the current face
        face_node(:) = cell_node(interface_2D_triangle(:,ineigh),icell)
        flag_found   = .false.

        !! loop over all found already found
        do i_face=1, counter
          if(flag_found) exit !! ok
          !! check this face
          face_node_try(:) = list_face  (:,i_face)
          
          !! it seems much more efficient to separate the if, so that we
          !! can leave as soon as one is ko, we do not need to check them
          !! all.
          if (any(face_node_try == face_node(1))) then
          if (any(face_node_try == face_node(2))) then
          !! if (any(face_node_try == face_node(1)) .and.            &
          !!     any(face_node_try == face_node(2))) then
          
               flag_found=.true.
               exit
          end if
          end if
        end do

        !! if it is a new one
        if(.not. flag_found) then
          counter             = counter + 1
          list_face(:,counter)= face_node(:)
        end if
      end do

    end do
    !! -----------------------------------------------------------------
    return
  end subroutine mesh_simplex_faces_triangle

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve all faces of all cells for tetrahedron
  !
  !> @param[in]    n_cells              : total number of cells
  !> @param[in]    n_face_bdry          : total number of boundary faces
  !> @param[in]    cell_node            : array of the cell's node    [4,ncells]
  !> @param[in]    list_face_boundary   : array of the boundary faces
  !> @param[in]    list_marker_boundary : array of marker
  !> @param[inout] list_face            : array of all faces
  !> @param[inout] list_marker          : array of all markers
  !> @param[out]   ctx_err              : error context
  !----------------------------------------------------------------------------
  subroutine mesh_simplex_faces_tetra(n_cells,n_face_bdry,cell_node,    &
                                      list_face_boundary,               &
                                      list_marker_boundary,list_face,   &
                                      list_marker,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)   :: n_cells,n_face_bdry
    integer(kind=IKIND_MESH), allocatable, intent(in)   :: cell_node         (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(in)   :: list_face_boundary(:,:)
    integer                 , allocatable, intent(in)   :: list_marker_boundary(:)
    integer(kind=IKIND_MESH), allocatable, intent(inout):: list_face(:,:)
    integer                 , allocatable, intent(inout):: list_marker(:)
    type(t_error)                        , intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh
    integer(kind=IKIND_MESH)              :: i_face,counter
    integer(kind=IKIND_MESH)              :: nneigh=4
    integer(kind=IKIND_MESH)              :: face_node_try(3)
    integer(kind=IKIND_MESH)              :: face_node    (3)
    logical                               :: flag_found

    ctx_err%ierr = 0

    !! we first copy the boundary informations
    list_marker                 =0
    list_marker(1:n_face_bdry)  =list_marker_boundary(:)
    list_face  (:,:)            =0
    list_face  (:,1:n_face_bdry)=list_face_boundary  (:,:)
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! loop over all cells
    !!
    !! $OMP PARALLEL would require a critical and may cause trouble
    !! when looking in an array that is constantly increasing
    !!
    !! -----------------------------------------------------------------
    counter    = n_face_bdry

    do icell=1, n_cells

      !! loop over all face of this cell
      do ineigh=1, nneigh

        !! get the three nodes (tetrahedron face = triangle) of the current face
        face_node(:) = cell_node(interface_3D_tetrahedron(:,ineigh),icell)
        flag_found   = .false.

        !! loop over all faces already found
        do i_face=1, counter
          if(flag_found) exit !! ok
          !! check this face
          face_node_try(:) = list_face  (:,i_face)
          
          !! it seems much more efficient to separate the if, so that we
          !! can leave as soon as one is ko, we do not need to check them
          !! all.
          if (any(face_node_try == face_node(1))) then
          if (any(face_node_try == face_node(2))) then
          if (any(face_node_try == face_node(3))) then
          !! if (any(face_node_try == face_node(1)) .and.            &
          !!     any(face_node_try == face_node(2)) .and.            &
          !!     any(face_node_try == face_node(3))) then
               flag_found=.true.
               exit
          end if
          end if
          end if
        end do

        !! if it is a new one
        if(.not. flag_found) then
          counter             = counter + 1
          list_face(:,counter)= face_node(:)
        end if
      end do

    end do
    !! -----------------------------------------------------------------
    return
  end subroutine mesh_simplex_faces_tetra


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve all faces of all cells for tetrahedron, we use another
  !>  version to remove duplicates a little more efficiently.
  !
  !> @param[in]    n_cells              : total number of cells
  !> @param[in]    n_face_bdry          : total number of boundary faces
  !> @param[in]    cell_node            : array of the cell's node    [4,ncells]
  !> @param[in]    list_face_boundary   : array of the boundary faces
  !> @param[in]    list_marker_boundary : array of marker
  !> @param[inout] list_face            : array of all faces
  !> @param[inout] list_marker          : array of all markers
  !> @param[out]   ctx_err              : error context
  !----------------------------------------------------------------------------
  subroutine mesh_simplex_faces_tetra_hash(n_cells,n_face_bdry,cell_node,&
                                      list_face_boundary,                &
                                      list_marker_boundary,list_face,    &
                                      list_marker,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)   :: n_cells,n_face_bdry
    integer(kind=IKIND_MESH), allocatable, intent(in)   :: cell_node         (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(in)   :: list_face_boundary(:,:)
    integer                 , allocatable, intent(in)   :: list_marker_boundary(:)
    integer(kind=IKIND_MESH), allocatable, intent(inout):: list_face(:,:)
    integer                 , allocatable, intent(inout):: list_marker(:)
    type(t_error)                        , intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh
    integer(kind=IKIND_MESH)              :: i_face,counter
    integer(kind=IKIND_MESH)              :: nneigh=4
    integer(kind=IKIND_MESH)              :: face_node_try(3)
    integer(kind=IKIND_MESH)              :: face_node    (3)
    logical                               :: flag_found
    !! update flo
    integer(kind=IKIND_MESH)              :: nface_max,counter_loc
    integer(kind=IKIND_MESH), allocatable :: list_face_hash (:,:)
    integer(kind=IKIND_MESH), allocatable :: count_face_hash(:,:)
    
    ctx_err%ierr = 0

    !! we first copy the boundary informations
    list_marker                 =0
    list_marker(1:n_face_bdry)  =list_marker_boundary(:)
    list_face  (:,:)            =0
    list_face  (:,1:n_face_bdry)=list_face_boundary  (:,:)
    !! -----------------------------------------------------------------

    !! 
    nface_max = size(list_face,2)
    allocate(list_face_hash (3,nface_max))
    allocate(count_face_hash(2,nface_max))
    list_face_hash(:,1:n_face_bdry)=list_face_boundary  (:,:)
    counter_loc = n_face_bdry
    count_face_hash(1,:) = 0
    count_face_hash(2,1:n_face_bdry) = 1

    !! -----------------------------------------------------------------
    !! loop over all cells
    !!
    !! $OMP PARALLEL would require a critical and may cause trouble
    !! when looking in an array that is constantly increasing
    !!
    !! -----------------------------------------------------------------
    counter    = n_face_bdry

    do icell=1, n_cells

      !! loop over all face of this cell
      do ineigh=1, nneigh

        !! get the three nodes (tetrahedron face = triangle) of the current face
        face_node(:) = cell_node(interface_3D_tetrahedron(:,ineigh),icell)
        flag_found   = .false.

        !! loop over all faces already found
        do i_face=1, counter_loc
          if(flag_found) exit !! ok
          !! check this face
          face_node_try(:) = list_face_hash(:,i_face)
          !! -----------------------------------------------------------
          !! it seems much more efficient to separate the if, so that we
          !! can leave as soon as one is ko, we do not need to check them
          !! all.
          if (any(face_node_try == face_node(1))) then
          if (any(face_node_try == face_node(2))) then
          if (any(face_node_try == face_node(3))) then

              flag_found=.true.
              !! update counter of the face found 
              count_face_hash(1,i_face) = count_face_hash(1,i_face) + 1
              !! if it is complete, we remove from the list
              if(count_face_hash(1,i_face) == count_face_hash(2,i_face)) then
                list_face_hash(:,i_face:counter_loc-1) =                &
                list_face_hash(:,i_face+1:counter_loc)
                counter_loc = counter_loc - 1
              end if
              exit
          end if
          end if
          end if
          !! -----------------------------------------------------------

        end do

        !! if it is a new one
        if(.not. flag_found) then
          counter             = counter + 1
          list_face(:,counter)= face_node(:)
          !! update the hash table as well 
          counter_loc = counter_loc+1
          count_face_hash(1,counter_loc) = 1
          count_face_hash(2,counter_loc) = 2 !! share by at most 2 cells.
          list_face_hash (:,counter_loc) = face_node(:) 
          
        end if
      end do

    end do
    !! -----------------------------------------------------------------
    deallocate(list_face_hash )
    deallocate(count_face_hash)
    
    
    return
  end subroutine mesh_simplex_faces_tetra_hash


end module m_mesh_simplex_compute_faces
