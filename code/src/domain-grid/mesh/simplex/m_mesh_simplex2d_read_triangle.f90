!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex2d_read_triangle.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the domain mesh files (triangles in 2D)
!
!------------------------------------------------------------------------------
module m_mesh_simplex2d_read_triangle
  
  use m_define_precision,       only : IKIND_MESH, RKIND_MESH
  use m_broadcast,              only : broadcast  
  use m_time,                   only : time
  use m_raise_error,            only : raise_error, t_error
  use m_distribute_error,       only : distribute_error
  use m_ctx_domain,             only : t_domain
  use m_mesh_simplex_interface, only : mesh_interface_simplex
  use m_ctx_parallelism,        only : t_parallelism
                                     
  implicit none

  private  
  public :: mesh_read_triangle, mesh_read_init_triangle
   
  contains  



  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine reads the input 2D mesh files in triangle format
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read_triangle(ctx_paral,mesh,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: mesh
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH)  , allocatable :: inter_node(:,:)
    integer                   , allocatable :: mark_inter(:)
    logical                                 :: file_exists
    integer                                 :: cline,mark
    integer                                 :: unit_node=120, unit_ele =121
    integer                                 :: unit_edge=122, unit_neig=123
    real   (kind=8)                         :: node_coo (2)
    integer(kind=IKIND_MESH)                :: node_cell(3),edge_node(2)
    integer(kind=IKIND_MESH)                :: node_neigh(3)
    integer(kind=IKIND_MESH)                :: k
    real   (kind=8)                         :: time0,time1

    ctx_err%ierr  = 0
    
    !! general informations for triangle meshes
    mesh%format_element         ='triangle'
    mesh%n_node_per_cell        =3
    mesh%n_node_per_face        =2
    mesh%n_neigh_per_cell       =3
    mesh%n_node_per_face_shared =2
    mesh%n_interface_combi      =2  !! triangle interface is a segment
                                    !! 2 orientation possible
    
    call time(time0)
    !! check that all files exist --------------------------------------
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.node'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.node')   //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.ele'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.ele')    //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.edge'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.edge')   //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.neigh')  //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    if(ctx_err%ierr.ne.0) call raise_error(ctx_err)
    !! end check files exist -------------------------------------------

    !! Open files
    open (unit=unit_node,file=trim(trim(adjustl(mesh%file_mesh))//'.node' ))
    open (unit=unit_ele ,file=trim(trim(adjustl(mesh%file_mesh))//'.ele'  ))
    open (unit=unit_edge,file=trim(trim(adjustl(mesh%file_mesh))//'.edge' ))
    open (unit=unit_neig,file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'))
    !! Read number of nodes and elements and edges
    read(unit_node,*) mesh%n_node_gb
    read(unit_ele ,*) mesh%n_cell_gb
    read(unit_edge,*) mesh%n_face_gb 
    read(unit_neig,*) cline !! garbage  
   
    !! -------------------------------------------------------------------
    !! Only the master is going to read the global information on the mesh
    !! -------------------------------------------------------------------
    if(ctx_paral%master) then
      !! allocation
      allocate(mesh%x_node         (mesh%dim_domain      ,mesh%n_node_gb))
      allocate(mesh%cell_node      (mesh%n_node_per_cell ,mesh%n_cell_gb))
      allocate(mesh%cell_neigh     (mesh%n_neigh_per_cell,mesh%n_cell_gb))
      allocate(mesh%cell_face      (mesh%n_neigh_per_cell,mesh%n_cell_gb))
      allocate(inter_node(mesh%n_node_per_face,mesh%n_face_gb))
      allocate(mark_inter(mesh%n_face_gb))      
      mesh%cell_face = 0

      !! a) nodes --------------------------------------------------------
      do k=1,mesh%n_node_gb
        read(unit_node,*) cline,  node_coo(:), mark
        mesh%x_node   (:,cline) = real(node_coo(:),kind=RKIND_MESH)
        !! mesh%mark_node(cline)   = mark
      end do
      !! b) elem  --------------------------------------------------------
      do k=1,mesh%n_cell_gb
         read(unit_ele ,*) cline, node_cell(:)
         mesh%cell_node (:,cline) = node_cell (:)
         read(unit_neig,*) cline, node_neigh(:)
         mesh%cell_neigh(:,cline) = node_neigh(:)
      end do
      !! c) edge  --------------------------------------------------------
      do k=1,mesh%n_face_gb
         read(unit_edge,*) cline, edge_node(:), mark
         inter_node(:,cline) = edge_node(:)
         mark_inter(cline)   = mark
      end do
      
      !! get interface informations for 2D simplex
      call mesh_interface_simplex(2,mesh%n_cell_gb,                          &
                                  mesh%n_neigh_per_cell,mesh%n_face_gb,      &
                                  mesh%cell_node,                            &
                                  mesh%cell_neigh,mesh%cell_face,inter_node, &
                                  mark_inter,mesh%tag_n,                     &
                                  mesh%tag_bc_array,                         &
                                  mesh%n_face_interior_gb,                   &
                                  mesh%n_face_boundary_gb,ctx_err)
      
      !! check count 
      if(mesh%n_face_interior_gb+mesh%n_face_boundary_gb.ne.mesh%n_face_gb) then
        ctx_err%ierr=-1
        ctx_err%msg =trim(adjustl(ctx_err%msg)) //                      &
                     "** ERROR: Incorrect number of edges found " //    &
                     " [mesh_read_triangle] **"
        ctx_err%critic=.true.
      end if

      deallocate(inter_node)
      deallocate(mark_inter)
      
      !! bound coordinates
      mesh%bounds_xmin=minval(mesh%x_node(1,:))
      mesh%bounds_xmax=maxval(mesh%x_node(1,:))
      mesh%bounds_ymin=0.
      mesh%bounds_ymax=0.
      mesh%bounds_zmin=minval(mesh%x_node(2,:))
      mesh%bounds_zmax=maxval(mesh%x_node(2,:))

    end if    
    !! -------------------------------------------------------------------
    !! only the master has read the informations
    !! -------------------------------------------------------------------
    !! close files
    close(unit_node)
    close(unit_ele)
    close(unit_edge)
    close(unit_neig)

    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 
    
    call broadcast(mesh%bounds_xmin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_xmax,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_ymin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_ymax,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_zmin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_zmax,1,0,ctx_paral%communicator,ctx_err)
    call time(time1)
    mesh%time_read = time1-time0
    
    return
  end subroutine mesh_read_triangle
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine reads the input 2D mesh files in triangle format
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read_init_triangle(ctx_paral,mesh,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: mesh
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    logical     :: file_exists
    integer     :: cline
    integer     :: unit_node=120, unit_ele =121
    integer     :: unit_edge=122, unit_neig=123
    real(kind=8):: time0,time1
    integer     :: int_size       = 4
    integer     :: real_size      = 4
    integer     :: int_size_mesh  = IKIND_MESH
    integer     :: real_size_mesh = RKIND_MESH
    integer(kind=8)          :: mem_count
    integer(kind=IKIND_MESH) :: ncell_loc, nface_loc

    ctx_err%ierr  = 0
    call time(time0)

    !! general informations
    mesh%format_element  ='triangle'
    mesh%n_node_per_cell  =3
    mesh%n_node_per_face  =2
    mesh%n_neigh_per_cell =3
    mesh%n_interface_combi=2 !! triangle interface is a segment so 2 orientation
    !! check that all files exist --------------------------------------
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.node'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.node')   //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.ele'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.ele')    //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.edge'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.edge')   //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 2D PARAMETER FILE "               //        &
                    trim(trim(adjustl(mesh%file_mesh))//'.neigh')  //        &
                   " DOEST NOT EXIST [mesh_read_triangle] "
      ctx_err%critic=.true.
    endif
    if(ctx_err%ierr.ne.0) call raise_error(ctx_err)
    !! end check files exist -------------------------------------------

    !! Open files
    open (unit=unit_node,file=trim(trim(adjustl(mesh%file_mesh))//'.node' ))
    open (unit=unit_ele ,file=trim(trim(adjustl(mesh%file_mesh))//'.ele'  ))
    open (unit=unit_edge,file=trim(trim(adjustl(mesh%file_mesh))//'.edge' ))
    open (unit=unit_neig,file=trim(trim(adjustl(mesh%file_mesh))//'.neigh'))
    !! Read number of nodes and elements and edges
    read(unit_node,*) mesh%n_node_gb
    read(unit_ele ,*) mesh%n_cell_gb
    read(unit_edge,*) mesh%n_face_gb 
    read(unit_neig,*) cline !! garbage  
    !! close files
    close(unit_node)
    close(unit_ele)
    close(unit_edge)
    close(unit_neig)

    !! anticipate memory requirement

    mem_count=0

    !! memory peark is during partitioning 
    mem_count =(9*int_size_mesh)                 + &  !! 8 integers parameters
               (6*real_size_mesh)                + &  !! bounding box
               (6*int_size)                      + &  !! 6 integers 
               (8*2*real_size)                   + &  !! 8 real (kind=8)
               (1*256)                           + &  !! 1 character(len=256)
               (1*32)                            + &  !! 2 character(len=32)
               (3*real_size*10)                  + &  !! ~ 10 tags array
               !! mesh%x_node
               ((mesh%n_node_gb)*real_size_mesh) + &
               !! mesh%cell_node
               ((mesh%n_cell_gb*mesh%n_node_per_cell)*int_size_mesh)  + &
               !! mesh%cell_bary
               ((mesh%n_cell_gb*mesh%dim_domain)*real_size_mesh)      + &
               !! mesh%neigh 
               ((mesh%n_cell_gb*mesh%n_neigh_per_cell)*int_size_mesh) + &
               !! mesh%cell_face
               ((mesh%n_cell_gb*mesh%n_neigh_per_cell)*int_size_mesh) + &
               !! mesh%order
               ((mesh%n_cell_gb)*int_size) + ((mesh%n_face_gb)*int_size)

    !! peak when reading, in monoproc we need additional array
    mesh%memory_peak_read = mem_count +  &
               !! interface node and interface markers
               ((mesh%n_face_gb*mesh%n_node_per_cell)*int_size_mesh) + &
               ((mesh%n_face_gb                     )*int_size)
    
    !! peak when partitioning, we have simultaneously monoproc memory
    !! and additional requirement
    mesh%memory_peak_part = mem_count        +  &
        ((mesh%n_cell_gb*int_size  ))        +  &  !! proc cell
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! cells_pproc 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! ghost_pproc  
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! offset_cells 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! offset_ghost 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! counter_cells
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! counter_ghost
        ((mesh%n_cell_gb*int_size_mesh))     +  &  !! ordered_cellindex
        ((mesh%n_cell_gb*mesh%n_node_per_cell *int_size_mesh))+& !ordered_cellnode
        ((mesh%n_cell_gb*mesh%n_neigh_per_cell*int_size_mesh))+& !ordered_cellneigh
        ((mesh%n_cell_gb*mesh%n_neigh_per_cell*int_size_mesh))+& !ordered_cellface
        ((mesh%n_cell_gb*int_size))          +  &  !! ordered_cellorder
        ((mesh%n_node_gb)*real_size_mesh)    +  &  !! temp_xnode
        ((mesh%n_node_gb)*int_size_mesh)     +  &  !! index_loctoglob_node
        ((mesh%n_face_gb)*int_size_mesh)     +  &  !! index_loctoglob_face
        ((mesh%n_cell_gb*mesh%dim_domain)*real_size_mesh) + & !! barycenters
        !! another cell_node required at partitioning
        ((mesh%n_cell_gb*mesh%n_node_per_cell)*int_size_mesh)
    
    !! approximate memory peak on slave assuming good partitioning
    !! peak when partitioning, we have simultaneously monoproc memory
    !! and other proc infos 
    if(ctx_paral%nproc == 1) then
      ncell_loc = mesh%n_cell_gb
      nface_loc = mesh%n_face_gb
    else
      ncell_loc = int(1.1*ceiling(real(mesh%n_cell_gb) / real(ctx_paral%nproc)))
      nface_loc = int(1.4*ceiling(real(mesh%n_face_gb) / real(ctx_paral%nproc)))
    end if

    mesh%memory_peak_part = mesh%memory_peak_part +  &
        ((ncell_loc*int_size)               ) +  & !! ordered_ghostorder
        ((ncell_loc*mesh%n_node_per_cell))    +  & !! array_itemp
        !! cell_face_orientation
        ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh)
    
    !! add ctx_mesh corresponding to every non-master proc 
    mesh%memory_peak_other =  (9*int_size_mesh)    + &  !! 8 integers parameters
               (6*real_size_mesh)                  + &  !! bounding box
               (6*int_size)                        + &  !! 6 integers 
               (8*2*real_size)                     + &  !! 8 real (kind=8)
               (1*256)                             + &  !! 1 character(len=256)
               (1*32)                              + &  !! 2 character(len=32)
               (3*real_size*10)                    + &  !! ~ 10 tags array
               ((ncell_loc)*real_size_mesh)        + &  !! mesh%x_node
               !! mesh%cell_node is counted twice for ghost infos during 
               !! partitioning
               ((ncell_loc*mesh%n_node_per_cell)*int_size_mesh)*2+ & !! mesh%cell_node 
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! mesh%neigh 
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! mesh%cell_face
               ((ncell_loc)*int_size)               +  &  !! mesh%order
               ((mesh%n_node_gb)*real_size_mesh)    +  &  !! temp_xnode
               ((mesh%n_node_gb)*int_size_mesh)     +  &  !! index_loctoglob_node
               ((nface_loc)*int_size_mesh)          +  &  !! index_loctoglob_face
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! local_cell_face
               ((nface_loc)*int_size)               +  &  !! order_face
               !! cell_face_orientation
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh)

    call time(time1)
    mesh%time_init = time1-time0
    
    return
  end subroutine mesh_read_init_triangle

end module m_mesh_simplex2d_read_triangle
