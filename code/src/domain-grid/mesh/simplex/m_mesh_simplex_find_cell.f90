!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex_find_cell.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to find the cell in which belong a given point
!> for 2D triangle mesh and 3D tetrahedron mesh.
!
!------------------------------------------------------------------------------
module m_mesh_simplex_find_cell

  use omp_lib  
  use m_raise_error,        only : raise_error, t_error
  use m_define_precision,   only : IKIND_MESH,RKIND_MESH
  
  implicit none

  private
  public  :: find_cell_simplex
  private :: find_cell_triangle, find_cell_tetrahedron, on_side_2d, on_side_3d
  
  contains



  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The interface finds the cell in which the given point is
  !>  we need to deal with 2D or 3D mesh, here we work with simplex
  !
  !> @param[in]   coo_input     : coordinates of the position to look for
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   n_cell        : number of local cells to search
  !> @param[in]   cell_neigh    : list of list of neighbors (2D array)
  !> @param[in]   dim_domain    : domain dimension
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine find_cell_simplex(coo_input,cell,x_node,cell_node,n_cell,  &
                               dim_domain,cell_neigh,ctx_err)
    
    implicit none

    real   (kind=8)                      ,intent(in)   :: coo_input(:)
    integer(kind=IKIND_MESH)             ,intent(out)  :: cell
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node(:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    integer                              ,intent(in)   :: dim_domain
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_neigh(:,:)
    type(t_error)                        ,intent(out)  :: ctx_err
    
    ctx_err%ierr  = 0
    cell = 0

    !! depends on the dimension
    select case(dim_domain)
      case(1)
        !! 1D LINE
        call find_cell_line(coo_input,cell,x_node,cell_node,n_cell,ctx_err)
      case(2)
        !! 2D TRIANGLE
        call find_cell_triangle(coo_input,cell,x_node,cell_node, &
                                n_cell,cell_neigh,ctx_err)
      case(3)
        !! 3D TETRAHEDRON
        call find_cell_tetrahedron(coo_input,cell,x_node,cell_node, &
                                   n_cell,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Unrecognized dimension for " // & 
                        " [find_cell_simplex] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select    
    !! -----------------------------------------------------------------
    return
  end subroutine find_cell_simplex
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the cell in which the given point for 1D 
  !>  line 
  !
  !> @param[in]   coo_input     : coordinates of the position to look for
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   n_cell        : number of cells to search
  !> @param[in]   format_cell   : format of the cell (e.g. simplex)
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine find_cell_line(coo_input,cell,x_node,cell_node,n_cell,ctx_err)
    
    implicit none
    
    real   (kind=8)                      ,intent(in)   :: coo_input(1)
    integer(kind=IKIND_MESH)             ,intent(out)  :: cell
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node   (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    type(t_error)                        ,intent(out)  :: ctx_err
    !! 
    integer(kind=IKIND_MESH)             :: i_cell,node
    integer                              :: k
    integer                              :: n_edge = 2 !! segment
    real   (kind=8)         ,allocatable :: xline   (:,:)
    real   (kind=8)                      :: xmin,xmax
    
    ctx_err%ierr  = 0
    cell          = 0

    !! check if it is in the subdomain.   
    xmin=dble(minval(x_node(1,:)))
    xmax=dble(maxval(x_node(1,:)))
    
    !! if not we leave 
    if(coo_input(1) < xmin .or. coo_input(1) > xmax) return
    !! otherwize we look for the cell.
   
    !! loop over all cell using OMP 
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,k,node,xline)
    allocate(xline     (1,n_edge))
    !$OMP DO
    do i_cell=1,n_cell
      if(cell .ne. 0) cycle !! for omp
      !! get coordinates of the nodes of the cell
      do k=1,n_edge
        node      =  cell_node(k,i_cell)
        xline(:,k) = dble(x_node(:,node))
      end do
 
      !! ---------------------------------------------------------------
      if(coo_input(1) >= minval(xline(1,:)) .and.                       &
         coo_input(1) <= maxval(xline(1,:))) then
        cell = i_cell
      end if
      !! ---------------------------------------------------------------

    end do
    !$OMP END DO
    deallocate(xline)
    !$OMP END PARALLEL

    return
  end subroutine find_cell_line

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the cell in which the given point for 2D 
  !>  triangle mesh
  !
  !> @param[in]   coo_input     : coordinates of the position to look for
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   n_cell        : number of cells to search
  !> @param[in]   cell_neigh    : neighbor information
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine find_cell_triangle(coo_input,cell,x_node,cell_node,n_cell,cell_neigh,ctx_err)
    
    implicit none
    
    real   (kind=8)                      ,intent(in)   :: coo_input(2)
    integer(kind=IKIND_MESH)             ,intent(out)  :: cell
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node   (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    integer(kind=IKIND_MESH),allocatable, intent(in)   :: cell_neigh(:,:)
    type(t_error)                        ,intent(out)  :: ctx_err
    !! 
    integer(kind=IKIND_MESH)             :: i_cell,node
    integer                              :: k
    integer                              :: n_edge = 3 !! triangle
    logical                 ,allocatable :: flag_in (:)
    real   (kind=8)         ,allocatable :: xtri    (:,:)
    real   (kind=8)                      :: xmin,xmax,zmin,zmax
    
    ctx_err%ierr  = 0
    cell          = 0

    !! check if it is in the subdomain.   
    xmin=dble(minval(x_node(1,:)))
    xmax=dble(maxval(x_node(1,:)))
    zmin=dble(minval(x_node(2,:)))
    zmax=dble(maxval(x_node(2,:)))
    !! if not we leave 
    if(coo_input(1) < xmin .or. coo_input(1) > xmax .or. &
       coo_input(2) < zmin .or. coo_input(2) > zmax) return

    !! otherwize we look for the cell.
    call find_triangle_walk(coo_input, cell, x_node, cell_node, cell_neigh, ctx_err)
    
    if (cell /= 0) return ! walk found it, no backup necessary
    
    !! loop over all cell using OMP 
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,k,node,xtri,flag_in)
    allocate(flag_in   (n_edge))
    allocate(xtri    (2,n_edge))
    !$OMP DO
    do i_cell=1,n_cell  
      if(cell .ne. 0) cycle !! for omp
      !! get coordinates of the nodes of the cell
      do k=1,n_edge
        node      =  cell_node(k,i_cell)
        xtri(:,k) = dble(x_node(:,node))
      end do
 
      !! It is in the cell if coo_input and node(k) are on the same side
      !! of the line node(i)--node(j) for all k where i != j != k
      !! ---------------------------------------------------------------
      flag_in(:) = .false.
      call on_side_2d(coo_input,xtri(:,1),xtri(:,2),xtri(:,3),flag_in(1))
      call on_side_2d(coo_input,xtri(:,2),xtri(:,1),xtri(:,3),flag_in(2))
      call on_side_2d(coo_input,xtri(:,3),xtri(:,1),xtri(:,2),flag_in(3))
      if(flag_in(1) .and. flag_in(2) .and. flag_in(3)) then
        cell = i_cell
      end if
    end do
    !$OMP END DO
    deallocate(flag_in)
    deallocate(xtri)
    !$OMP END PARALLEL

    return
  end subroutine find_cell_triangle

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the cell containing a given point for 2D 
  !>  triangle mesh using a barycentrical walk described in the 
  !>  article "Efficient implementation of characteristic-based schemes 
  !>  on unstructured triangular grids" by S. Cacace and R. Ferretti
  !
  !> @param[in]   coo           : coordinates of the position to look for
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   neighbors     : list of neighbors for all cell 
  !>                               (if <0 -> boundary, if =0 on another proc)
  !> @param[out]  ctx_err       : error context
  !> @param[in]   cell_0        : starting cell (default value = 1)
  !> @note : compared to the brute force algorithm we need the neigbor array
  !----------------------------------------------------------------------------
  subroutine find_triangle_walk(coo, cell, x_node, cell_node, neighbors, ctx_err, cell_0)
    
    implicit none

    real(kind=8)                         , intent(in)  :: coo(2) !> coordinates of the searched point
    integer(kind=IKIND_MESH)             , intent(out) :: cell !> output : index of the cell containing the point
    real(kind=RKIND_MESH),    allocatable, intent(in)  :: x_node(:,:) !> nodes of the mesh
    integer(kind=IKIND_MESH), allocatable, intent(in)  :: cell_node(:,:) !> connectivity
    integer(kind=IKIND_MESH), allocatable, intent(in)  :: neighbors(:,:) !> neighbors elements
    type(t_error)                        , intent(out) :: ctx_err
    integer,                     optional, intent(in)  :: cell_0
    !! local
    logical      :: find
    real(kind=8) :: theta(3)
    integer      :: chg, it, MAX_ITERATIONS, err


    option : if (present(cell_0)) then
      cell = cell_0
    else
      cell = 1
    end if option

    MAX_ITERATIONS = 100000
    it = 0
    find = .false.
    LOOP_TRIANGLES: do 
      if (find .or. it > MAX_ITERATIONS ) exit 
      call compute_barycenters_2d(coo, x_node(:,cell_node(:,cell)), theta, err) 
      if (err < 0) then
        write (ctx_err%msg, '(a,x,i0)') "** ERROR: cannot compute barycenters in the cell", cell
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      if (all(theta >= 0.0_RKIND_MESH)) then
        find = .true.
      else
        chg = maxloc(abs(theta), 1, theta < 0.0_RKIND_MESH .and. (neighbors(:, cell) > 1))
        if (chg /= 0) then
          cell = neighbors(chg, cell)
        else    
          cell = 0 !! do not find the cell
          exit LOOP_TRIANGLES
        end if
      end if
      it = it + 1
    end do LOOP_TRIANGLES
    if (it > MAX_ITERATIONS) cell = 0 !! we do not find the cell
  end subroutine find_triangle_walk

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the cell in which the given point for 3D 
  !>  tetrahedral mesh
  !
  !> @param[in]   coo_input     : coordinates of the position to look for
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   n_cell        : number of cells to search
  !> @param[in]   format_cell   : format of the cell (e.g. simplex)
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine find_cell_tetrahedron(coo_input,cell,x_node,cell_node,     &
                                   n_cell,ctx_err)
    
    implicit none

    real   (kind=8)                      ,intent(in)   :: coo_input(3)
    integer(kind=IKIND_MESH)             ,intent(out)  :: cell
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node   (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    type(t_error)                        ,intent(out)  :: ctx_err
    !! 
    integer(kind=IKIND_MESH)             :: i_cell,node
    integer                              :: k
    integer                              :: n_edge = 4 !! tetrahedron
    logical                 ,allocatable :: flag_in (:)
    real   (kind=8)         ,allocatable :: xtet    (:,:)
    real   (kind=8)                      :: xmin,xmax,ymin,ymax,zmin,zmax
    
    ctx_err%ierr  = 0
    cell          = 0

    !! check if it is in the subdomain.   
    xmin=dble(minval(x_node(1,:)))
    xmax=dble(maxval(x_node(1,:)))
    ymin=dble(minval(x_node(2,:)))
    ymax=dble(maxval(x_node(2,:)))
    zmin=dble(minval(x_node(3,:)))
    zmax=dble(maxval(x_node(3,:)))
    !! if not we leave 
    if(coo_input(1) < xmin .or. coo_input(1) > xmax .or. &
       coo_input(2) < ymin .or. coo_input(2) > ymax .or. &
       coo_input(3) < zmin .or. coo_input(3) > zmax) return
    !! otherwize we look for the cell.


    !! loop over all cell using OMP 
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,k,node,xtet,flag_in)
    allocate(flag_in   (n_edge))
    allocate(xtet    (3,n_edge))
    !$OMP DO
    do i_cell=1,n_cell  
      if(cell .ne. 0) cycle !! for omp
      !! get coordinates of the nodes of the cell
      do k=1,n_edge
        node      =  cell_node(k,i_cell)
        xtet(:,k) = dble(x_node(:,node))
      end do
 
      !! It is in the cell if coo_input and node(l) are on the same side
      !! of the plane node(i)--node(j)--node(k) 
      !! for all k where i != j != k != l
      !! ---------------------------------------------------------------
      flag_in(:) = .false.
      call on_side_3d(coo_input,xtet(:,1),xtet(:,2),xtet(:,3),xtet(:,4),flag_in(1))
      call on_side_3d(coo_input,xtet(:,2),xtet(:,1),xtet(:,3),xtet(:,4),flag_in(2))
      call on_side_3d(coo_input,xtet(:,3),xtet(:,1),xtet(:,2),xtet(:,4),flag_in(3))
      call on_side_3d(coo_input,xtet(:,4),xtet(:,1),xtet(:,2),xtet(:,3),flag_in(4))
      if(flag_in(1) .and. flag_in(2) .and. flag_in(3) .and. flag_in(4)) then
        cell = i_cell
      end if
    end do
    !$OMP END DO
    deallocate(flag_in)
    deallocate(xtet)
    !$OMP END PARALLEL

    return
  end subroutine find_cell_tetrahedron


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine verifies if the two nodes are on the same 
  !>  side of the ref line
  !
  !----------------------------------------------------------------------------
  subroutine on_side_2d(test1,test2,ref1,ref2,flag)
    
    implicit none

    real   (kind=8),  intent(in)    :: test1(2)
    real   (kind=8),  intent(in)    :: test2(2)
    real   (kind=8),  intent(in)    :: ref1 (2)
    real   (kind=8),  intent(in)    :: ref2 (2)
    logical        ,  intent(out)   :: flag
    !! local
    real   (kind=8) :: vector_1(2)
    real   (kind=8) :: vector_2(2)
    real   (kind=8) :: vector_3(2)
    real   (kind=8) :: testval
    
    vector_1 = ref2  - ref1
    vector_2 = test1 - ref1
    vector_3 = test2 - ref1
    !! compute product
    testval=(dble(vector_1(1)*vector_2(2)) - dble(vector_1(2)*vector_2(1))) * &
            (dble(vector_1(1)*vector_3(2)) - dble(vector_1(2)*vector_3(1)))
    if(testval .ge. 0.) then
      flag=.true.
    else
      flag=.false.
    end if
    
    return
    
  end subroutine on_side_2d

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine verifies if two nodes are on the same 
  !>  side of the surface defined by the three ref nodes
  !
  !----------------------------------------------------------------------------
  subroutine on_side_3d(test1,test2,ref1,ref2,ref3,flag)
    
    implicit none

    real   (kind=8),  intent(in)    :: test1(3)
    real   (kind=8),  intent(in)    :: test2(3)
    real   (kind=8),  intent(in)    :: ref1 (3)
    real   (kind=8),  intent(in)    :: ref2 (3)
    real   (kind=8),  intent(in)    :: ref3 (3)
    logical        ,  intent(out)   :: flag
    !! local
    real   (kind=8) :: vector_1 (3)
    real   (kind=8) :: vector_2 (3)
    real   (kind=8) :: vector_3 (3)
    real   (kind=8) :: vector_4 (3)
    real   (kind=8) :: crossprod(3)
    real   (kind=8) :: testval
    
    vector_1 = ref2  - ref1
    vector_2 = ref3  - ref1
    vector_3 = test1 - ref1
    vector_4 = test2 - ref1
    !! compute cross product between 1 and 2 
    crossprod(1) = vector_1(2)*vector_2(3)-vector_2(2)*vector_1(3)
    crossprod(2) = vector_1(3)*vector_2(1)-vector_2(3)*vector_1(1)
    crossprod(3) = vector_1(1)*vector_2(2)-vector_2(1)*vector_1(2)

    !! compute sum of dot product
    testval = (crossprod(1)*vector_3(1) + crossprod(2)*vector_3(2) +    &
               crossprod(3)*vector_3(3))                           *    &
              (crossprod(1)*vector_4(1) + crossprod(2)*vector_4(2) +    &
               crossprod(3)*vector_4(3))

    if(testval .ge. 0.) then
      flag=.true.
    else
      flag=.false.
    end if

    return
  end subroutine on_side_3d

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the barycentrical coordinates (theta) of 
  !>  of a point (coo) relatively to the triangle xtri
  !
  !----------------------------------------------------------------------------
  subroutine compute_barycenters_2d(coo, xtri, theta, err)

    implicit none
    real(kind=8), intent(in)  :: coo(2)
    real(kind=8), intent(in)  :: xtri(2,3)
    real(kind=8), intent(out) :: theta(3)
    integer,      intent(out) :: err
    real(kind=8) :: denom, a, b, c, d, rhs(2)

    a = xtri(1,1) - xtri(1,3) 
    b = xtri(1,2) - xtri(1,3) 
    c = xtri(2,1) - xtri(2,3) 
    d = xtri(2,2) - xtri(2,3) 

    denom = a * d - b * c
    err = 0

    !! FIXME : set kind for the threshold
    if (abs(denom) < 1e-10 * sqrt(sum(xtri**2))) then
      err = -1
      return
    endif

    rhs = coo - xtri(:, 3)

    theta(1) = (rhs(1) * d - rhs(2) * b) /denom
    theta(2) = (rhs(2) * a - rhs(1) * c) /denom
    theta(3) = 1.d0 - theta(1) - theta(2)

  end subroutine compute_barycenters_2d

end module m_mesh_simplex_find_cell
