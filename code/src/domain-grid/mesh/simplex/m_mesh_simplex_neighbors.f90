!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex_neighbors.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for simplex mesh to deal with the neighbors:
!> for all cell we identify the neighbors
!
!------------------------------------------------------------------------------
module m_mesh_simplex_neighbors

  use omp_lib
  use m_define_precision,       only : IKIND_MESH, RKIND_MESH
  use m_raise_error,            only : raise_error, t_error
  use m_mesh_simplex_interface, only : interface_2D_triangle,           &
                                       interface_3D_tetrahedron

  implicit none


  private
  public :: mesh_simplex_neighbors_tetra, mesh_simplex_neighbors_triangle
  public :: mesh_simplex_neighbors_segment

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve all neighbors of all cells, using OMP parallelism
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_node       : array of the cell's node    [4,ncells]
  !> @param[inout] cell_neigh      : array of the cell's neighbor[4,ncells]
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_simplex_neighbors_tetra(n_cells,cell_node,cell_neigh,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)    :: n_cells
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: cell_node (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_neigh(:,:)
    type(t_error)                        , intent(out)   :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh
    integer(kind=IKIND_MESH)              :: icell_neigh,ineigh_neigh
    integer(kind=IKIND_MESH)              :: nneigh=4
    integer(kind=IKIND_MESH)              :: face_node_neigh(3)
    integer(kind=IKIND_MESH)              :: face_node      (3)
    logical                               :: flag_found

    ctx_err%ierr = 0

    !! -----------------------------------------------------------------
    !! loop over all cells using open mp. it can be time consuming
    !! especially as we do not know the boundary cells that will
    !! induce a loop over all cells...
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(ineigh,icell_neigh,icell),   &
    !$OMP&         PRIVATE(face_node_neigh,face_node,ineigh_neigh,flag_found)
    !$OMP DO
    do icell=1, n_cells
      !! loop over all neighbors
      do ineigh=1, nneigh !! = 4 with tetra
        !! get the three nodes (tetrahedron face) of the current face
        face_node(:) = cell_node(interface_3D_tetrahedron(:,ineigh),icell)
        flag_found = .false.
        !! if already done by symmetry
        if(cell_neigh(ineigh,icell) > 0) flag_found=.true.

        !! loop over all other cell to find the neighbor
        do icell_neigh=1, n_cells
          if(flag_found) exit !! ok

          !! check all other cells
          if(icell_neigh .ne. icell) then
            do ineigh_neigh=1, nneigh
              face_node_neigh(:) = cell_node(interface_3D_tetrahedron(:,&
                                             ineigh_neigh),icell_neigh)

              !! it seems much more efficient to separate the if, so that we
              !! can leave as soon as one is ko, we do not need to check all
              if (any(face_node_neigh == face_node(3))) then
              if (any(face_node_neigh == face_node(2))) then
              if (any(face_node_neigh == face_node(1))) then
                  cell_neigh(ineigh,icell) = icell_neigh
                  !! do the opposite one as well
                  cell_neigh(ineigh_neigh,icell_neigh) = icell

                  flag_found=.true.
                  exit
              end if
              end if
              end if
            end do
          end if

        end do
      end do
    end do
    !$OMP END DO
    !$OMP END PARALLEL

    return
  end subroutine mesh_simplex_neighbors_tetra

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve all neighbors of all cells, using OMP parallelism
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_node       : array of the cell's node    [4,ncells]
  !> @param[inout] cell_neigh      : array of the cell's neighbor[4,ncells]
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_simplex_neighbors_triangle(n_cells,cell_node,         &
                                             cell_neigh,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)    :: n_cells
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: cell_node (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_neigh(:,:)
    type(t_error)                        , intent(out)   :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh
    integer(kind=IKIND_MESH)              :: icell_neigh,ineigh_neigh
    integer(kind=IKIND_MESH)              :: nneigh=3
    integer(kind=IKIND_MESH)              :: face_node_neigh(2)
    integer(kind=IKIND_MESH)              :: face_node      (2)
    logical                               :: flag_found

    ctx_err%ierr = 0

    !! loop over all cells
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(ineigh,icell_neigh,icell),   &
    !$OMP&         PRIVATE(face_node_neigh,face_node,ineigh_neigh,flag_found)
    !$OMP DO
    do icell=1, n_cells
      !! loop over all neighbors
      do ineigh=1, nneigh !! = 3 with triangle
        !! get the two nodes (triangle face) of the current face
        face_node(:) = cell_node(interface_2D_triangle(:,ineigh),icell)
        flag_found = .false.
        !! if already done by symmetry
        if(cell_neigh(ineigh,icell) > 0) flag_found=.true.

        !! loop over all other cell to find the neighbor
        do icell_neigh=1, n_cells
          if(flag_found) exit !! ok

          !! check all other cells
          if(icell_neigh .ne. icell) then
            do ineigh_neigh=1, nneigh
              face_node_neigh(:) = cell_node(interface_2D_triangle(:,   &
                                             ineigh_neigh),icell_neigh)

              !! it seems much more efficient to separate the if, so that we
              !! can leave as soon as one is ko, we do not need to check all
              if (any(face_node_neigh == face_node(1))) then
              if (any(face_node_neigh == face_node(2))) then

                cell_neigh(ineigh,icell) = icell_neigh
                !! do the opposite one as well
                cell_neigh(ineigh_neigh,icell_neigh) = icell

                flag_found=.true.
                exit
              end if
              end if

            end do
          end if

        end do
      end do
    end do
    !$OMP END DO
    !$OMP END PARALLEL

    return
  end subroutine mesh_simplex_neighbors_triangle

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve all neighbors of all cells, using OMP parallelism
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_node       : array of the cell's node    [2,ncells]
  !> @param[inout] cell_neigh      : array of the cell's neighbor[2,ncells]
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_simplex_neighbors_segment(n_cells,cell_node,cell_neigh,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)    :: n_cells
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: cell_node (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_neigh(:,:)
    type(t_error)                        , intent(out)   :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh
    integer(kind=IKIND_MESH)              :: icell_neigh,ineigh_neigh
    integer(kind=IKIND_MESH)              :: nneigh=2
    integer(kind=IKIND_MESH)              :: face_node_neigh(1)
    integer(kind=IKIND_MESH)              :: face_node      (1)
    logical                               :: flag_found

    ctx_err%ierr = 0

    !! loop over all cells
    !! -----------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(ineigh,icell_neigh,icell),   &
    !$OMP&         PRIVATE(face_node_neigh,face_node,ineigh_neigh,flag_found)
    !$OMP DO
    do icell=1, n_cells
      !! loop over all neighbors
      do ineigh=1, nneigh !! = 2 with segment
        !! get the two nodes (triangle face) of the current face
        face_node(1) = cell_node(ineigh,icell)
        flag_found = .false.
        !! if already done by symmetry
        if(cell_neigh(ineigh,icell) > 0) flag_found=.true.

        !! loop over all other cell to find the neighbor
        do icell_neigh=1, n_cells
          if(flag_found) exit !! ok

          !! check all other cells
          if(icell_neigh .ne. icell) then
            do ineigh_neigh=1, nneigh
              face_node_neigh(1) = cell_node(ineigh_neigh,icell_neigh)
              !! is is that one
              if (face_node_neigh(1) == face_node(1)) then
                cell_neigh(ineigh,icell) = icell_neigh
                !! do the opposite one as well
                cell_neigh(ineigh_neigh,icell_neigh) = icell
                flag_found=.true.
                exit
              end if
            end do
          end if

        end do
      end do
    end do
    !$OMP END DO
    !$OMP END PARALLEL

    return
  end subroutine mesh_simplex_neighbors_segment

end module m_mesh_simplex_neighbors
