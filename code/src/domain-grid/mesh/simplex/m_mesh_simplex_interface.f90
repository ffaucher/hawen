!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex_interface.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for simplex mesh to deal with the interface
!> between element:
!> - for all interface in the sub-mesh (part of the mesh on the current
!>   processor), we create the local numbering and identify if boundary.
!
!------------------------------------------------------------------------------
module m_mesh_simplex_interface

  !! module used -------------------------------------------------------
  use omp_lib
  use m_define_precision,     only : IKIND_MESH, RKIND_MESH
  use m_raise_error,          only : raise_error, t_error
  use m_tag_bc,               only : tag_absorbing, tag_freesurface,    &
                                     tag_wallsurface, tag_ghost_cell,   &
                                     tag_pml, tag_pml_circular,         &
                                     tag_dirichlet, tag_neumann,        &
                                     tag_planewave,tag_center_neumann,  &
                                     tag_aldirichlet_dlrobin,           &
                                     tag_deltapdirichlet_dlrobin,       &
                                     tag_deltapdirichlet_dldirichlet,   &
                                     tag_alrbc_dlrobin,                 &
                                     tag_alrbc_dldirichlet,             &
                                     tag_analytic_orthorhombic,         &
                                     tag_deltapdirichlet,               &
                                     tag_analytic_elasticiso,           &
                                     tag_planewave_isoP,                &
                                     tag_planewave_isoS,                &
                                     tag_planewave_vtiYSH,              &
                                     tag_planewave_vtiYSV,              &
                                     tag_planewave_vtiYqP,              &
                                     tag_planewave_scatt,               &
                                     tag_lagrangep,                     &
                                     tag_lagrangep_dlrobin
                                                      
                                     

  implicit none

  !> 2D triangle, edge node are ordered such that 2-3, 3-1 and 1-2
  !> because the first edge takes the opposite point to the first node ==> 2-3
  !> the second edge opposite to second node ==> 3-1, ...
  integer, parameter :: interface_2D_triangle(2,3)   = &
                        reshape([2,3,  3,1,  1,2], [2,3])
  !> 3D tetrahedron: we have 4 possibilities because we always turn
  !> in the same side
  integer, parameter :: interface_3D_tetrahedron(3,4)= &
                        reshape([2,4,3,   1,3,4,   1,4,2,   1,2,3], [3,4])


  private
  public :: mesh_interface_simplex, interface_3D_tetrahedron,           &
            interface_2D_triangle, mesh_interface_simplex_orient
  private:: mesh_interface_triangle, mesh_interface_tetrahedron
  private:: triangle_face_order_orientation, tetra_face_order_orientation

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve interface information:
  !>    -> global array cell_face(iface,ncells) = index face
  !>    -> appropriat neighbor tag for non-interior cell from the edge marker
  !
  !> @param[in]    dm              : dimension (2D=triangle, 3D=tetra)
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    nneigh_per_cell : number of neighbor per cell
  !> @param[in]    ninter          : total number of interfaces
  !> @param[in]    cell_node       : array of the cell's node
  !> @param[inout] cell_neigh      : array of the cell's neighbor
  !> @param[inout] cell_face       : array of the cell's face ind
  !> @param[in]    x_node          : nodes coordinates
  !> @param[in]    inter_node      : array of interfaces nodes
  !> @param[in]    mark_inter      : array of the mark of the interface
  !> @param[inout] cell_inter_size : computed size of all cells interfaces
  !> @param[in]    tag_n           : number of tag per BC
  !> @param[in]    tag_array       : array with all tags for BC, numbers are
  !>                                 ( 1,:) absorbing
  !>                                 ( 2,:) free-surface
  !>                                 ( 3,:) wall-surface
  !>                                 ( 4,:) dirichlet
  !>                                 ( 5,:) neumann
  !>                                 ( 6,:) planewave
  !>                                 ( 7,:) atmo-dirichlet (for helio spherical)
  !>                                 ( 8,:) center-neumann (for helio spherical)
  !>                                 ( 9,:) plm
  !>                                 (10,:) plm circular
  !>                                 (11,:) 
  !>                                 (12,:) 
  !>                                 (13,:) 
  !>                                 (14,:) al absorbing - dl robin
  !>                                 (15,:) al absorbing - dl dirichlet 
  !>                                 (16,:) delta p dirichlet
  !>                                 (17,:) planewave iso P
  !>                                 (18,:) planewave iso S
  !>                                 (19,:) analytic elastic iso
  !> @param[out]   n_face_interior : number of interior faces
  !> @param[out]   n_face_boundary : number of boundary faces
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_interface_simplex(dm,ncells,nneigh_per_cell,ninter,         &
                                    cell_node,cell_neigh,cell_face,inter_node,&
                                    mark_inter,tag_n,tag_array,               &
                                    n_face_interior,n_face_boundary,ctx_err)

    implicit none
    integer                              , intent(in)    :: dm
    integer(kind=IKIND_MESH)             , intent(in)    :: ncells,ninter
    integer                              , intent(in)    :: nneigh_per_cell
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: cell_node (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_neigh(:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_face (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: inter_node(:,:)
    integer                 , allocatable, intent(in)    :: mark_inter(:)
    integer                              , intent(in)    :: tag_n
    integer                              , intent(in)    :: tag_array(:,:)
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_interior
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_boundary
    type(t_error)                        , intent(out)   :: ctx_err

    ctx_err%ierr = 0

    select case (dm)

      !! ---------------------------------------------------------------
      !! 1D Segments
      !! ---------------------------------------------------------------
      case(1)
        !! consistency check
        if(nneigh_per_cell .ne. 2) then
          ctx_err%ierr=-1
          ctx_err%msg ="** ERROR: inconsistent 1D simplex n_neighbors " // &
                       " [mesh_interface_simplex] **"
          ctx_err%critic=.true.
          return
        end if
        call mesh_interface_segment(ncells,nneigh_per_cell,ninter,      &
                                    cell_node,cell_neigh,cell_face,     &
                                    inter_node,mark_inter,tag_n,        &
                                    tag_array,                          &
                                    n_face_interior,n_face_boundary,    &
                                    ctx_err)

      !! ---------------------------------------------------------------
      !! 2D Triangles
      !! ---------------------------------------------------------------
      case(2)
        !! consistency check
        if(nneigh_per_cell .ne. 3) then
          ctx_err%ierr=-1
          ctx_err%msg ="** ERROR: inconsistent 2D simplex n_neighbors " // &
                       " [mesh_interface_simplex] **"
          ctx_err%critic=.true.
          return
        end if
        call mesh_interface_triangle(ncells,nneigh_per_cell,ninter,     &
                                     cell_node,cell_neigh,cell_face,    &
                                     inter_node,mark_inter,tag_n,       &
                                     tag_array,                         &
                                     n_face_interior,n_face_boundary,   &
                                     ctx_err)
      !! ---------------------------------------------------------------
      !! 3D Tetrahedra
      !! ---------------------------------------------------------------
      case(3)
        !! consistency check
        if(nneigh_per_cell .ne. 4) then
          ctx_err%ierr=-1
          ctx_err%msg ="** ERROR: inconsistent 3D simplex n_neighbors " // &
                       " [mesh_interface_simplex] **"
          ctx_err%critic=.true.
          return
        end if
        call mesh_interface_tetrahedron(ncells,nneigh_per_cell,ninter,  &
                                        cell_node,cell_neigh,cell_face, &
                                        inter_node,mark_inter,tag_n,    &
                                        tag_array,                      &
                                        n_face_interior,n_face_boundary,&
                                        ctx_err)
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** ERROR: unrecognized simplex dimension in" // &
                     " [mesh_interface_simplex] **"
        ctx_err%critic=.true.
        return
    end select

    return
  end subroutine mesh_interface_simplex

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve interface information in 1D mesh
  !>    -> global array cell_face(2,ncells) = index face left, right
  !>    -> appropriate neighbor tag for non-interior cell from the edge marker
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    nneigh_per_cell : number of neighbor per cell
  !> @param[in]    ninter          : total number of interfaces
  !> @param[in]    cell_node       : array of the cell's node    [2,ncells]
  !> @param[inout] cell_neigh      : array of the cell's neighbor[2,ncells]
  !> @param[inout] cell_face       : array of the cell's face ind[2,ncells]
  !> @param[in]    x_node          : nodes coordinates           [1,nnodes]
  !> @param[in]    inter_node      : array of interfaces nodes
  !> @param[in]    mark_inter      : array of the mark of the interface
  !> @param[inout] cell_inter_size : computed size of all cells interfaces
  !> @param[in]    tag_n           : number of tag per BC
  !> @param[in]    tag_array       : array with all tags for BC, numbers are
  !>                                 ( 1,:) absorbing
  !>                                 ( 2,:) free-surface
  !>                                 ( 3,:) wall-surface
  !>                                 ( 4,:) dirichlet
  !>                                 ( 5,:) neumann
  !>                                 ( 6,:) planewave
  !>                                 ( 7,:) atmo-dirichlet (for helio spherical)
  !>                                 ( 8,:) center-neumann (for helio spherical)
  !>                                 ( 9,:) plm
  !>                                 (10,:) plm circular
  !>                                 (11,:) 
  !>                                 (12,:) 
  !>                                 (13,:) 
  !>                                 (14,:) al absorbing - dl robin
  !>                                 (15,:) al absorbing - dl dirichlet
  !>                                 (16,:) delta p dirichlet
  !>                                 (17,:) planewave iso P
  !>                                 (18,:) planewave iso S
  !>                                 (19,:) analytic elastic iso
  !> @param[out]   n_face_interior : number of interior faces
  !> @param[out]   n_face_boundary : number of boundary faces
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_interface_segment(ncells,nneigh_per_cell,ninter,      &
                                    cell_node,cell_neigh,cell_face,     &
                                    inter_node,mark_inter,              &
                                    tag_n,tag_array,                    &
                                    n_face_interior,n_face_boundary,    &
                                    ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)    :: ncells,ninter
    integer                              , intent(in)    :: nneigh_per_cell
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: cell_node (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_neigh(:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_face (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: inter_node(:,:)
    integer                 , allocatable, intent(in)    :: mark_inter(:)
    integer                              , intent(in)    :: tag_n
    integer                              , intent(in)    :: tag_array(:,:)
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_interior
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_boundary
    type(t_error)                        , intent(out)   :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh,neigh
    integer(kind=IKIND_MESH)              :: iinter,inter_index,jneigh
    integer(kind=IKIND_MESH)              :: ninter_bc,cinter,iloc
    integer(kind=IKIND_MESH)              :: cinter_bc,internode(1)
    integer(kind=IKINd_MESH), allocatable :: inter_bc_index(:)
    integer                               :: itag
    character(len=16)                     :: stmp

    ctx_err%ierr = 0
    !! -----------------------------------------------------------------
    allocate(inter_bc_index(ninter))

    !! we first separate the interface which are actually
    !! on the boundaries because we will need to find the tag
    ninter_bc = 0
    do iinter=1,ninter
      if(mark_inter(iinter) .ne. 0) then
        ninter_bc = ninter_bc + 1
        inter_bc_index(ninter_bc) = iinter
      endif
    enddo
    n_face_boundary = ninter_bc
    cinter_bc = ninter - n_face_boundary !! boundary faces will be at the end
    !! -----------------------------------------------------------------

    cinter   = 0
    !! we have removed OMP because the loop is quite fast. Also OMP implies
    !! a change in the local numbering of faces as we use an increment, thus
    !! OMP CRITICAL update.
    !! It means that all numbers are changed with omp, which makes it
    !! hard to reproduce test case
    do icell=1,ncells

      !! check all interfaces of the current cell
      !! Remark: Boundary faces are positioned at the end of the array !
      do ineigh=1,nneigh_per_cell
        if(cell_face(ineigh,icell) .eq. 0) then !! new one

          neigh                   = cell_neigh(ineigh,icell)

          if(neigh > 0) then !! not boundary: do the opposite as well
            cinter                  = cinter + 1
            cell_face(ineigh,icell) = cinter
            do jneigh=1,nneigh_per_cell
              if(cell_neigh(jneigh,neigh) .eq. icell) then
                cell_face(jneigh,neigh) = cinter
                exit
              end if
            end do

          else !! boundary face: get the tag as well, update at the end
            cinter_bc               = cinter_bc + 1
            cell_face(ineigh,icell) = cinter_bc

            internode(1) = cell_node(ineigh,icell)

            !! retrieve the key value global from index and mark_inter
            inter_index=0

            do iinter=1,ninter_bc
              iloc = inter_bc_index(iinter) !! convert local to global
              if(internode(1) .eq. inter_node(1,iloc)) then
                inter_index=iloc
                exit
              endif
            enddo

            !! update marker where we force to be negative
            if(inter_index .eq. 0) then
              ctx_err%ierr=-1
              ctx_err%msg ="** ERROR: Edge not found for segment cell " // &
                           " [mesh_utils_inter_infos_segment] **"
              ctx_err%critic=.true.
              return
            else
              !! get the tag from the marker array
              cell_neigh(ineigh,icell) = 0
              do itag=1,tag_n !! number of possible tag
                !! numbers are:
                !!     1) absorbing
                !!     2) free-surface
                !!     3) wall-surface
                !!     4) dirichlet
                !!     5) neumann
                !!     6) planewave
                !!     7) atmo-dirichlet (for helio spherical)
                !!     8) center-neumann (for helio spherical)
                !!     9) plm
                !!    10) plm circular
                !!    11) atmo_deltap_dirichlet
                !!    12) 
                !!    13) 
                !!    14) al absorbing - dl robin 
                !!    15) al absorbing - dl dirichlet
                !!    16) deltap dirichlet
                !!    17) planewave iso P
                !!    18) planewave iso S
                !!    19) analytic elastic iso
                if    (mark_inter(inter_index) .eq. tag_array(1,itag))  then
                  cell_neigh(ineigh,icell) = tag_absorbing
                elseif(mark_inter(inter_index) .eq. tag_array(2,itag))  then
                  cell_neigh(ineigh,icell) = tag_freesurface
                elseif(mark_inter(inter_index) .eq. tag_array(3,itag))  then
                  cell_neigh(ineigh,icell) = tag_wallsurface
                elseif(mark_inter(inter_index) .eq. tag_array(4,itag))  then
                  cell_neigh(ineigh,icell) = tag_dirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(5,itag))  then
                  cell_neigh(ineigh,icell) = tag_neumann
                elseif(mark_inter(inter_index) .eq. tag_array(6,itag))  then
                  cell_neigh(ineigh,icell) = tag_planewave
                elseif(mark_inter(inter_index) .eq. tag_array(7,itag))  then
                  cell_neigh(ineigh,icell) = tag_aldirichlet_dlrobin
                elseif(mark_inter(inter_index) .eq. tag_array(8,itag))  then
                  cell_neigh(ineigh,icell) = tag_center_neumann
                elseif(mark_inter(inter_index) .eq. tag_array(11,itag)) then
                  cell_neigh(ineigh,icell) = tag_deltapdirichlet_dldirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(12,itag)) then
                  cell_neigh(ineigh,icell) = tag_analytic_orthorhombic
                elseif(mark_inter(inter_index) .eq. tag_array(13,itag)) then
                  cell_neigh(ineigh,icell) = tag_deltapdirichlet_dlrobin
                elseif(mark_inter(inter_index) .eq. tag_array(14,itag)) then
                  cell_neigh(ineigh,icell) = tag_alrbc_dlrobin
                elseif(mark_inter(inter_index) .eq. tag_array(15,itag)) then
                  cell_neigh(ineigh,icell) = tag_alrbc_dldirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(16,itag)) then
                  cell_neigh(ineigh,icell) = tag_deltapdirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(17,itag)) then
                  cell_neigh(ineigh,icell) = tag_planewave_isoP
                elseif(mark_inter(inter_index) .eq. tag_array(18,itag)) then
                  cell_neigh(ineigh,icell) = tag_planewave_isoS
                elseif(mark_inter(inter_index) .eq. tag_array(19,itag)) then
                  cell_neigh(ineigh,icell) = tag_analytic_elasticiso
                elseif(mark_inter(inter_index) .eq. tag_array(20,itag)) then
                  cell_neigh(ineigh,icell) = tag_planewave_vtiYSH
                elseif(mark_inter(inter_index) .eq. tag_array(21,itag)) then
                  cell_neigh(ineigh,icell) = tag_planewave_vtiYSV
                elseif(mark_inter(inter_index) .eq. tag_array(22,itag)) then
                  cell_neigh(ineigh,icell) = tag_planewave_vtiYqP
                elseif(mark_inter(inter_index) .eq. tag_array(23,itag)) then
                  cell_neigh(ineigh,icell) = tag_planewave_scatt
                elseif(mark_inter(inter_index) .eq. tag_array(24,itag)) then
                  cell_neigh(ineigh,icell) = tag_lagrangep
                elseif(mark_inter(inter_index) .eq. tag_array(25,itag)) then
                  cell_neigh(ineigh,icell) = tag_lagrangep_dlrobin
 
                !! careful numbering has been changed
                elseif(mark_inter(inter_index) .eq.                     &
                       tag_pml-tag_array(9,itag)) then
                  !! for now, we just keep the original tag to identify
                  !! afterwards the concerned cells
                  cell_neigh(ineigh,icell) = tag_array(9,itag)
                elseif(mark_inter(inter_index) .eq. tag_array(10,itag)) then
                  cell_neigh(ineigh,icell) = tag_pml_circular
                endif
              enddo
              !! if not found
              if(cell_neigh(ineigh,icell) == 0) then
                write(stmp,*) mark_inter(inter_index)
                ctx_err%ierr=-1
                ctx_err%msg ="** ERROR: Edge boundary marker "              // &
                             trim(adjustl(stmp)) // " not recognized as "   // &
                             " known BC [mesh_utils_inter_infos_segment] **"
                ctx_err%critic=.true.
                return
              endif
            endif
          end if

        end if !! end if boundary

      end do !! end all neighbors

    enddo !! end do for all cells

    n_face_interior = cinter
    deallocate(inter_bc_index)
    return

  end subroutine mesh_interface_segment

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve interface information:
  !>    -> global array cell_face(3,ncells) = index face
  !>    -> appropriat neighbor tag for non-interior cell from the edge marker
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    nneigh_per_cell : number of neighbor per cell
  !> @param[in]    ninter          : total number of interfaces
  !> @param[in]    cell_node       : array of the cell's node    [3,ncells]
  !> @param[inout] cell_neigh      : array of the cell's neighbor[3,ncells]
  !> @param[inout] cell_face       : array of the cell's face ind[3,ncells]
  !> @param[in]    x_node          : nodes coordinates           [2,nnodes]
  !> @param[in]    inter_node      : array of interfaces nodes
  !> @param[in]    mark_inter      : array of the mark of the interface
  !> @param[inout] cell_inter_size : computed size of all cells interfaces
  !> @param[in]    tag_n           : number of tag per BC
  !> @param[in]    tag_array       : array with all tags for BC, numbers are
  !>                                 ( 1,:) absorbing
  !>                                 ( 2,:) free-surface
  !>                                 ( 3,:) wall-surface
  !>                                 ( 4,:) dirichlet
  !>                                 ( 5,:) neumann
  !>                                 ( 6,:) planewave
  !>                                 ( 7,:) atmo-dirichlet (for helio spherical)
  !>                                 ( 8,:) center-neumann (for helio spherical)
  !>                                 ( 9,:) plm
  !>                                 (10,:) plm circular
  !>                                 (11,:) 
  !>                                 (12,:) 
  !>                                 (13,:) 
  !>                                 (14,:) al absorbing - dl robin
  !>                                 (15,:) al absorbing - dl dirichlet
  !>                                 (16,:) deltap dirichlet
  !>                                 (17,:) planewave iso P
  !>                                 (18,:) planewave iso S
  !>                                 (19,:) analytic elastic iso
  !> @param[out]   n_face_interior : number of interior faces
  !> @param[out]   n_face_boundary : number of boundary faces
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_interface_triangle(ncells,nneigh_per_cell,ninter,     &
                                     cell_node,cell_neigh,cell_face,    &
                                     inter_node,mark_inter,             &
                                     tag_n,tag_array,                   &
                                     n_face_interior,n_face_boundary,   &
                                     ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)    :: ncells,ninter
    integer                              , intent(in)    :: nneigh_per_cell
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: cell_node (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_neigh(:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_face (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: inter_node(:,:)
    integer                 , allocatable, intent(in)    :: mark_inter(:)
    integer                              , intent(in)    :: tag_n
    integer                              , intent(in)    :: tag_array(:,:)
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_interior
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_boundary
    type(t_error)                        , intent(out)   :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh,neigh
    integer(kind=IKIND_MESH)              :: iinter,inter_index,jneigh
    integer(kind=IKIND_MESH)              :: ninter_bc,cinter,iloc
    integer(kind=IKIND_MESH)              :: cinter_bc,internode(2)
    integer(kind=IKINd_MESH), allocatable :: inter_bc_index(:)
    integer                               :: itag
    character(len=16)                     :: stmp

    ctx_err%ierr = 0
    !! -----------------------------------------------------------------
    allocate(inter_bc_index(ninter))

    !! we first separate the interface which are actually
    !! on the boundaries because we will need to find the tag
    ninter_bc = 0
    do iinter=1,ninter
      if(mark_inter(iinter) .ne. 0) then
        ninter_bc = ninter_bc + 1
        inter_bc_index(ninter_bc) = iinter
      endif
    enddo
    n_face_boundary = ninter_bc
    cinter_bc = ninter - n_face_boundary !! boundary faces will be at the end
    !! -----------------------------------------------------------------

    cinter   = 0
    !! we have removed OMP because the loop is quite fast. Also OMP implies
    !! a change in the local numbering of faces as we use an increment, thus
    !! OMP CRITICAL update.
    !! It means that all numbers are changed with omp, which makes it
    !! hard to reproduce test case
    do icell=1,ncells

      !! check all interfaces of the current cell
      !! Remark: Boundary faces are positioned at the end of the array !
      do ineigh=1,nneigh_per_cell
        if(cell_face(ineigh,icell) .eq. 0) then !! new one

          neigh                   = cell_neigh(ineigh,icell)

          if(neigh > 0) then !! not boundary: do the opposite as well
            cinter                  = cinter + 1
            cell_face(ineigh,icell) = cinter
            do jneigh=1,nneigh_per_cell
              if(cell_neigh(jneigh,neigh) .eq. icell) then
                cell_face(jneigh,neigh) = cinter
                exit
              end if
            end do

          else !! boundary face: get the tag as well, update at the end
            cinter_bc               = cinter_bc + 1
            cell_face(ineigh,icell) = cinter_bc

            internode(1) = cell_node(interface_2D_triangle(1,ineigh),icell)
            internode(2) = cell_node(interface_2D_triangle(2,ineigh),icell)
            !! retrieve the key value global from index and mark_inter
            inter_index=0

            do iinter=1,ninter_bc
              iloc = inter_bc_index(iinter) !! convert local to global
              if(any(internode .eq. inter_node(1,iloc)) .and. &
                 any(internode .eq. inter_node(2,iloc))) then
                inter_index=iloc
                exit
              endif
            enddo

            !! update marker where we force to be negative
            if(inter_index .eq. 0) then
              ctx_err%ierr=-1
              ctx_err%msg ="** ERROR: Edge not found for triangle cell " // &
                           " [mesh_utils_inter_infos_triangle] **"
              ctx_err%critic=.true.
              return
            else
                
              !! get the tag from the marker array
              cell_neigh(ineigh,icell) = 0
              do itag=1,tag_n !! number of possible tag
                !! numbers are:
                !!     1) absorbing
                !!     2) free-surface
                !!     3) wall-surface
                !!     4) dirichlet
                !!     5) neumann
                !!     6) planewave
                !!     7) atmo-dirichlet (for helio spherical)
                !!     8) center-neumann (for helio spherical)
                !!     9) plm
                !!    10) plm circular
                !!    11) deltap dirichlet dl dirichlet (for helio spherical)
                !!    12) analytic orthorhombic
                !!    13) deltap dirichlet dl robin (for helio spherical)
                !!    14) al absorbing - dl robin
                !!    15) al absorbing - dl dirichlet
                !!    16) deltap dirichlet
                !!    17) planewave iso P
                !!    18) planewave iso S
                !!    19) analytic elastic iso
                
                if    (mark_inter(inter_index) .eq. tag_array(1,itag)) then
                  cell_neigh(ineigh,icell) = tag_absorbing
                elseif(mark_inter(inter_index) .eq. tag_array(2,itag)) then
                  cell_neigh(ineigh,icell) = tag_freesurface
                elseif(mark_inter(inter_index) .eq. tag_array(3,itag)) then
                  cell_neigh(ineigh,icell) = tag_wallsurface
                elseif(mark_inter(inter_index) .eq. tag_array(4,itag)) then
                  cell_neigh(ineigh,icell) = tag_dirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(5,itag)) then
                  cell_neigh(ineigh,icell) = tag_neumann
                elseif(mark_inter(inter_index) .eq. tag_array(6,itag)) then
                  cell_neigh(ineigh,icell) = tag_planewave
                elseif(mark_inter(inter_index) .eq. tag_array(7,itag)) then
                  cell_neigh(ineigh,icell) = tag_aldirichlet_dlrobin
                elseif(mark_inter(inter_index) .eq. tag_array(8,itag)) then
                  cell_neigh(ineigh,icell) = tag_center_neumann
                elseif(mark_inter(inter_index) .eq. tag_array(11,itag))then
                  cell_neigh(ineigh,icell) = tag_deltapdirichlet_dldirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(12,itag))then
                  cell_neigh(ineigh,icell) = tag_analytic_orthorhombic
                elseif(mark_inter(inter_index) .eq. tag_array(13,itag))then
                  cell_neigh(ineigh,icell) = tag_deltapdirichlet_dlrobin
                elseif(mark_inter(inter_index) .eq. tag_array(14,itag))then
                  cell_neigh(ineigh,icell) = tag_alrbc_dlrobin
                elseif(mark_inter(inter_index) .eq. tag_array(15,itag))then
                  cell_neigh(ineigh,icell) = tag_alrbc_dldirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(16,itag))then
                  cell_neigh(ineigh,icell) = tag_deltapdirichlet
                elseif(mark_inter(inter_index) .eq. tag_array(17,itag))then
                  cell_neigh(ineigh,icell) = tag_planewave_isoP
                elseif(mark_inter(inter_index) .eq. tag_array(18,itag))then
                  cell_neigh(ineigh,icell) = tag_planewave_isoS
                elseif(mark_inter(inter_index) .eq. tag_array(19,itag))then
                  cell_neigh(ineigh,icell) = tag_analytic_elasticiso
                elseif(mark_inter(inter_index) .eq. tag_array(20,itag))then
                  cell_neigh(ineigh,icell) = tag_planewave_vtiYSH
                elseif(mark_inter(inter_index) .eq. tag_array(21,itag))then
                  cell_neigh(ineigh,icell) = tag_planewave_vtiYSV
                elseif(mark_inter(inter_index) .eq. tag_array(22,itag))then
                  cell_neigh(ineigh,icell) = tag_planewave_vtiYqP
                elseif(mark_inter(inter_index) .eq. tag_array(23,itag))then
                  cell_neigh(ineigh,icell) = tag_planewave_scatt
                elseif(mark_inter(inter_index) .eq. tag_array(24,itag))then
                  cell_neigh(ineigh,icell) = tag_lagrangep
                elseif(mark_inter(inter_index) .eq. tag_array(25,itag))then
                  cell_neigh(ineigh,icell) = tag_lagrangep_dlrobin
                !! careful numbering has been changed
                elseif(mark_inter(inter_index) .eq.                     &
                       tag_pml-tag_array(9,itag)) then
                  !! for now, we just keep the original tag to identify
                  !! afterwards the concerned cells
                  cell_neigh(ineigh,icell) = tag_array(9,itag)
                elseif(mark_inter(inter_index) .eq. tag_array(10,itag)) then
                  cell_neigh(ineigh,icell) = tag_pml_circular
                endif
              enddo
              !! if not found
              if(cell_neigh(ineigh,icell) == 0) then
                write(stmp,*) mark_inter(inter_index)
                ctx_err%ierr=-1
                ctx_err%msg ="** ERROR: Edge boundary marker "              // &
                             trim(adjustl(stmp)) // " not recognized as "   // &
                             " known BC [mesh_utils_inter_infos_triangle] **"
                ctx_err%critic=.true.
                return
              endif
            endif
          end if

        end if !! end if boundary

      end do !! end all neighbors

    enddo !! end do for all cells

    n_face_interior = cinter
    deallocate(inter_bc_index)
    return

  end subroutine mesh_interface_triangle

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve interface information:
  !>    -> global array cell_face(4,ncells) = index face
  !>    -> appropriat neighbor tag for non-interior cell from the face marker
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    nneigh_per_cell : number of neighbor per cell
  !> @param[in]    ninter          : total number of interfaces
  !> @param[in]    cell_node       : array of the cell's node    [4,ncells]
  !> @param[inout] cell_neigh      : array of the cell's neighbor[4,ncells]
  !> @param[inout] cell_face       : array of the cell's face ind[4,ncells]
  !> @param[in]    x_node          : nodes coordinates           [3,nnodes]
  !> @param[in]    inter_node      : array of interfaces nodes
  !> @param[in]    mark_inter      : array of the mark of the interface
  !> @param[inout] cell_inter_size : computed size of all cells interfaces
  !> @param[in]    tag_n           : number of tag per BC
  !> @param[in]    tag_array       : array with all tags for BC, numbers are
  !>                                 ( 1,:) absorbing
  !>                                 ( 2,:) free-surface
  !>                                 ( 3,:) wall-surface
  !>                                 ( 4,:) dirichlet
  !>                                 ( 5,:) neumann
  !>                                 ( 6,:) planewave
  !>                                 ( 7,:) atmo-dirichlet (for helio spherical)
  !>                                 ( 8,:) center-neumann (for helio spherical)
  !>                                 ( 9,:) plm
  !>                                 (10,:) plm circular
  !>                                 (11,:) 
  !>                                 (12,:) 
  !>                                 (13,:) 
  !>                                 (14,:) al absorbing - dl robin
  !>                                 (15,:) al absorbing - dl dirichlet
  !>                                 (16,:) deltap dirichlet
  !>                                 (17,:) planewave iso P
  !>                                 (18,:) planewave iso S
  !>                                 (19,:) analytic elastic iso
  !> @param[out]   n_face_interior : number of interior faces
  !> @param[out]   n_face_boundary : number of boundary faces
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_interface_tetrahedron(ncells,nneigh_per_cell,ninter,  &
                                        cell_node,cell_neigh,cell_face, &
                                        inter_node,mark_inter,          &
                                        tag_n,tag_array,                &
                                        n_face_interior,n_face_boundary,&
                                        ctx_err)

    implicit none
    integer(kind=IKIND_MESH)             , intent(in)    :: ncells,ninter
    integer                              , intent(in)    :: nneigh_per_cell
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: cell_node (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_neigh(:,:)
    integer(kind=IKIND_MESH), allocatable, intent(inout) :: cell_face (:,:)
    integer(kind=IKIND_MESH), allocatable, intent(in)    :: inter_node(:,:)
    integer                 , allocatable, intent(in)    :: mark_inter(:)
    integer                              , intent(in)    :: tag_n
    integer                              , intent(in)    :: tag_array(:,:)
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_interior
    integer(kind=IKIND_MESH)             , intent(out)   :: n_face_boundary
    type(t_error)                        , intent(out)   :: ctx_err
    !! local
    integer(kind=IKIND_MESH)              :: icell,ineigh,neigh,internode(3)
    integer(kind=IKIND_MESH)              :: iinter,inter_index,iloc,jneigh
    integer(kind=IKIND_MESH)              :: cinter_bc,ninter_bc,cinter
    integer(kind=IKIND_MESH)              :: hash_iinter, hash_nindex
    integer(kind=IKIND_MESH), allocatable :: inter_bc_index(:)
    integer(kind=IKIND_MESH), allocatable :: hash_index_list(:)
    integer                               :: itag, tag_unique
    character(len=16)                     :: stmp

    ctx_err%ierr = 0
    allocate(inter_bc_index(ninter))
    !! -----------------------------------------------------------------
    !! we first separate the interface which are actually
    !! on the boundaries because we will need to find the tag
    ninter_bc = 0
    do iinter=1,ninter
      if(mark_inter(iinter) .ne. 0) then
        ninter_bc = ninter_bc + 1
        inter_bc_index(ninter_bc) = iinter
      endif
    enddo
    n_face_boundary = ninter_bc
    cinter_bc = ninter - n_face_boundary !! boundary faces will be at the end
    !! -----------------------------------------------------------------

    cinter   = 0

    !! -----------------------------------------------------------------
    !! we first check for the faces that are shared by two elements:
    !! this is relatively fast, and we avoid the OMP loop as we have
    !! a counter, which would require some CRITICAL instructions.
    !! -----------------------------------------------------------------
    do icell=1,ncells

      !! check all interfaces of the current cell
      !! Remark: Boundary faces are positioned at
      !! the end of the array
      do ineigh=1,nneigh_per_cell
        if(cell_face(ineigh,icell) .eq. 0) then !! new one
          neigh  = cell_neigh(ineigh,icell)
          if(neigh > 0) then !! not boundary: do the opposite as well
            cinter                  = cinter + 1
            cell_face(ineigh,icell) = cinter
            do jneigh=1,nneigh_per_cell
              if(cell_neigh(jneigh,neigh) .eq. icell) then
                cell_face(jneigh,neigh) = cinter
                exit
              end if
            end do

          else !! boundary face: we only save the index
            cinter_bc               = cinter_bc + 1
            cell_face(ineigh,icell) = cinter_bc
          end if
          !! Domain boundary faces are treated afterwards...
        end if
      end do !! end all neighbors

    end do !! end do for all cells
    !! -----------------------------------------------------------------
    n_face_interior = cinter

    !! -----------------------------------------------------------------
    !! we now check the faces that are actually on the boundary
    !! there are two options:
    !!   - if there is only one tag, we only have to make sure this is
    !!     the only tag that is present
    !!   - if there are several tags, we have to identify the
    !!     appropriate one; this is an heavy step and we use a
    !!     simplified hash table.
    !! -----------------------------------------------------------------
    !! numbers are:
    !!     1) absorbing
    !!     2) free-surface
    !!     3) wall-surface
    !!     4) dirichlet
    !!     5) neumann
    !!     6) planewave
    !!     7) al-dirichlet dl-robin (for helio spherical)
    !!     8) center-neumann (for helio spherical)
    !!     9) plm
    !!    10) plm circular       
    !!    11) deltap-dirichlet dl-dirichlet (for helio spherical)
    !!    12) analytic orthorhombic
    !!    13) deltap-dirichlet dl-robin (for helio spherical)  
    !!    14)
    !!    15)
    !!    16) deltap dirichlet
    !!    17) planewave iso P
    !!    18) planewave iso S
    !!    19) analytic elastic iso
    
    if(tag_n == 1) then !! only one tag
      !! identify the boundary condition, there can only be one possibility
      tag_unique = 0
      if(tag_array( 1,1) .ne. 0) tag_unique = tag_absorbing
      if(tag_array( 2,1) .ne. 0) tag_unique = tag_freesurface
      if(tag_array( 3,1) .ne. 0) tag_unique = tag_wallsurface
      if(tag_array( 4,1) .ne. 0) tag_unique = tag_dirichlet
      if(tag_array( 5,1) .ne. 0) tag_unique = tag_neumann
      if(tag_array( 6,1) .ne. 0) tag_unique = tag_planewave
      if(tag_array( 7,1) .ne. 0) tag_unique = tag_aldirichlet_dlrobin
      if(tag_array( 8,1) .ne. 0) tag_unique = tag_center_neumann
      if(tag_array(11,1) .ne. 0) tag_unique = tag_deltapdirichlet_dldirichlet
      if(tag_array(12,1) .ne. 0) tag_unique = tag_analytic_orthorhombic
      if(tag_array(13,1) .ne. 0) tag_unique = tag_deltapdirichlet_dlrobin
      if(tag_array(10,1) .ne. 0) tag_unique = tag_pml_circular
      if(tag_array(14,1) .ne. 0) tag_unique = tag_alrbc_dlrobin
      if(tag_array(15,1) .ne. 0) tag_unique = tag_alrbc_dldirichlet
      if(tag_array(16,1) .ne. 0) tag_unique = tag_deltapdirichlet
      if(tag_array(17,1) .ne. 0) tag_unique = tag_planewave_isoP
      if(tag_array(18,1) .ne. 0) tag_unique = tag_planewave_isoS
      if(tag_array(19,1) .ne. 0) tag_unique = tag_analytic_elasticiso
      if(tag_array(20,1) .ne. 0) tag_unique = tag_planewave_vtiYSH
      if(tag_array(21,1) .ne. 0) tag_unique = tag_planewave_vtiYSV
      if(tag_array(22,1) .ne. 0) tag_unique = tag_planewave_vtiYqP
      if(tag_array(23,1) .ne. 0) tag_unique = tag_planewave_scatt
      if(tag_array(24,1) .ne. 0) tag_unique = tag_lagrangep
      if(tag_array(25,1) .ne. 0) tag_unique = tag_lagrangep_dlrobin

      !! for now, we just keep the original tag to identify
      !! afterwards the concerned cells
      if(tag_pml-tag_array(9,1) .ne. 0) tag_unique = tag_array(9,1)

      if(tag_unique == 0) then
        ctx_err%ierr=-1
        ctx_err%msg ="** ERROR: The unique boundary tag has not " //    &
                     "been found [mesh_utils_inter_infos_tetrahedron] **"
        ctx_err%critic=.true.
        return
      end if

      !! we get the boundary and indicate this unique face tag
      !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,ineigh)
      !$OMP DO
      do icell=1,ncells
        do ineigh=1,nneigh_per_cell
          !! in case of a boundary
          if(cell_face(ineigh,icell) > n_face_interior) then
            cell_neigh(ineigh,icell) = tag_unique
          end if
        end do
      end do
      !$OMP END DO
      !$OMP END PARALLEL

    !! =================================================================
    !! in the case where we must identify the proper tags
    !! =================================================================
    else
      !! ---------------------------------------------------------------
      !! we now check the faces that are actually on the boundary
      !! this is an heavy step and we use simplified hash table.
      !! ---------------------------------------------------------------
      hash_nindex = n_face_boundary
      allocate(hash_index_list(hash_nindex))
      !! initialize with all consecutives indexes
      do iinter= 1, n_face_boundary
        hash_index_list(iinter) = inter_bc_index(iinter) !! convert local to global
      end do

      !! loop over all cells, it seems that omp is not very
      !! effective because of the critical.
      !! Hence it is removed for now. It also need to be validated.
      !! $OMP PARALLEL DEFAULT(shared) PRIVATE(icell,ineigh,neigh), &
      !! $OMP&         PRIVATE(internode,inter_index,iinter,iloc),  &
      !! $OMP&         PRIVATE(hash_iinter,itag,stmp)
      !! $OMP DO
      do icell=1,ncells
        if(ctx_err%ierr .ne. 0) cycle

        !! check all interfaces of the current cell
        do ineigh=1,nneigh_per_cell
          if(ctx_err%ierr .ne. 0) cycle

          !! in case of a boundary
          if(cell_face(ineigh,icell) > n_face_interior) then

            neigh = cell_neigh(ineigh,icell)

            !! interior cells will have a neighbor > 0, indicating
            !! which cells is the neighbor.
            !! In the case of a boundary face, neigh will be <= 0.
            if(neigh <= 0) then

              !! get the nodes that composed this face.
              internode(1) = cell_node(interface_3D_tetrahedron(1,ineigh),icell)
              internode(2) = cell_node(interface_3D_tetrahedron(2,ineigh),icell)
              internode(3) = cell_node(interface_3D_tetrahedron(3,ineigh),icell)
              !! retrieve the key value global from index and mark_inter
              !! using the hash table.
              inter_index=0
              do iinter=1,hash_nindex
                iloc = hash_index_list(iinter)
                !! not sure how to improve this.
                if((inter_node(1,iloc) .eq. internode(1) .and. &
                    inter_node(2,iloc) .eq. internode(2) .and. &
                    inter_node(3,iloc) .eq. internode(3)) .or. &
                   (inter_node(1,iloc) .eq. internode(1) .and. &
                    inter_node(2,iloc) .eq. internode(3) .and. &
                    inter_node(3,iloc) .eq. internode(2)) .or. &
                   (inter_node(1,iloc) .eq. internode(2) .and. &
                    inter_node(2,iloc) .eq. internode(1) .and. &
                    inter_node(3,iloc) .eq. internode(3)) .or. &
                   (inter_node(1,iloc) .eq. internode(2) .and. &
                    inter_node(2,iloc) .eq. internode(3) .and. &
                    inter_node(3,iloc) .eq. internode(1)) .or. &
                   (inter_node(1,iloc) .eq. internode(3) .and. &
                    inter_node(2,iloc) .eq. internode(1) .and. &
                    inter_node(3,iloc) .eq. internode(2)) .or. &
                   (inter_node(1,iloc) .eq. internode(3) .and. &
                    inter_node(2,iloc) .eq. internode(2) .and. &
                    inter_node(3,iloc) .eq. internode(1))) then
                  inter_index=iloc
                  hash_iinter=iinter
                  exit
                endif
              enddo
              !! -------------------------------------------------------
              !! if the face has been found, we update
              if(inter_index .ne. 0) then

                !! update hash table -----------------------------------
                !! $OMP CRITICAL
                hash_nindex = hash_nindex - 1
                if(hash_nindex > 0) then
                  hash_index_list(hash_iinter:hash_nindex) =              &
                    hash_index_list(hash_iinter+1:hash_nindex+1)
                end if
                !! $OMP END CRITICAL
                !! -----------------------------------------------------

                !! update tag ------------------------------------------
                cell_neigh(ineigh,icell) = 0
                do itag=1,tag_n !! number of possible tags
                  !! numbers are:
                  !!     1) absorbing
                  !!     2) free-surface
                  !!     3) wall-surface
                  !!     4) dirichlet
                  !!     5) neumann
                  !!     6) planewave
                  !!     7) al-dirichlet dl-robin (for helio spherical)
                  !!     8) center-neumann (for helio spherical)
                  !!     9) plm
                  !!    10) plm circular  
                  !!    11) deltap-dirichlet dl-dirichlet (for helio spherical)
                  !!    12) analytic orthorhombic
                  !!    13) deltap-dirichlet dl-robin (for helio spherical)    
                  !!    14) al absorbing - dl robin
                  !!    15) al absorbing - dl dirichlet
                  !!    16) deltap dirichlet
                  !!    17) planewave iso P
                  !!    18) planewave iso S
                  !!    19) analytic elastic iso
                  !! --------------------------------------------------- %
                  if    (mark_inter(inter_index) .eq. tag_array(1,itag)) then
                    cell_neigh(ineigh,icell) = tag_absorbing
                  elseif(mark_inter(inter_index) .eq. tag_array(2,itag)) then
                    cell_neigh(ineigh,icell) = tag_freesurface
                  elseif(mark_inter(inter_index) .eq. tag_array(3,itag)) then
                    cell_neigh(ineigh,icell) = tag_wallsurface
                  elseif(mark_inter(inter_index) .eq. tag_array(4,itag)) then
                    cell_neigh(ineigh,icell) = tag_dirichlet
                  elseif(mark_inter(inter_index) .eq. tag_array(5,itag))  then
                    cell_neigh(ineigh,icell) = tag_neumann
                  elseif(mark_inter(inter_index) .eq. tag_array(6,itag)) then
                    cell_neigh(ineigh,icell) = tag_planewave
                  elseif(mark_inter(inter_index) .eq. tag_array(7,itag)) then
                    cell_neigh(ineigh,icell) = tag_aldirichlet_dlrobin
                  elseif(mark_inter(inter_index) .eq. tag_array(8,itag)) then
                    cell_neigh(ineigh,icell) = tag_center_neumann
                  elseif(mark_inter(inter_index) .eq. tag_array(11,itag))then
                    cell_neigh(ineigh,icell) = tag_deltapdirichlet_dldirichlet
                  elseif(mark_inter(inter_index) .eq. tag_array(12,itag))then
                    cell_neigh(ineigh,icell) = tag_analytic_orthorhombic
                  elseif(mark_inter(inter_index) .eq. tag_array(13,itag))then
                    cell_neigh(ineigh,icell) = tag_deltapdirichlet_dlrobin
                  elseif(mark_inter(inter_index) .eq. tag_array(14,itag))then
                    cell_neigh(ineigh,icell) = tag_alrbc_dlrobin
                  elseif(mark_inter(inter_index) .eq. tag_array(15,itag))then
                    cell_neigh(ineigh,icell) = tag_alrbc_dldirichlet
                  elseif(mark_inter(inter_index) .eq. tag_array(16,itag))then
                    cell_neigh(ineigh,icell) = tag_deltapdirichlet  
                  elseif(mark_inter(inter_index) .eq. tag_array(17,itag))then
                    cell_neigh(ineigh,icell) = tag_planewave_isoP  
                  elseif(mark_inter(inter_index) .eq. tag_array(18,itag))then
                    cell_neigh(ineigh,icell) = tag_planewave_isoS  
                  elseif(mark_inter(inter_index) .eq. tag_array(19,itag))then
                    cell_neigh(ineigh,icell) = tag_analytic_elasticiso
                  elseif(mark_inter(inter_index) .eq. tag_array(20,itag))then
                    cell_neigh(ineigh,icell) = tag_planewave_vtiYSH
                  elseif(mark_inter(inter_index) .eq. tag_array(21,itag))then
                    cell_neigh(ineigh,icell) = tag_planewave_vtiYSV
                  elseif(mark_inter(inter_index) .eq. tag_array(22,itag))then
                    cell_neigh(ineigh,icell) = tag_planewave_vtiYqP
                  elseif(mark_inter(inter_index) .eq. tag_array(23,itag))then
                    cell_neigh(ineigh,icell) = tag_planewave_scatt
                  elseif(mark_inter(inter_index) .eq. tag_array(24,itag))then
                    cell_neigh(ineigh,icell) = tag_lagrangep
                  elseif(mark_inter(inter_index) .eq. tag_array(25,itag))then
                    cell_neigh(ineigh,icell) = tag_lagrangep_dlrobin
                  !! careful numbering has been changed
                  elseif(mark_inter(inter_index) .eq.                     &
                         tag_pml-tag_array(9,itag)) then
                    !! for now, we just keep the original tag to identify
                    !! afterwards the concerned cells
                    cell_neigh(ineigh,icell) = tag_array(9,itag)
                  elseif(mark_inter(inter_index) .eq. tag_array(10,itag)) then
                    cell_neigh(ineigh,icell) = tag_pml_circular
                  endif
                end do

                !! if tag not found
                if(cell_neigh(ineigh,icell) == 0) then
                  write(stmp,*) mark_inter(inter_index)
                  ctx_err%ierr=-1
                  ctx_err%msg ="** ERROR: Edge boundary marker "      //&
                          trim(adjustl(stmp)) // " not recognized as "//&
                          " known BC [mesh_utils_inter_infos_tetrahedron] **"
                  ctx_err%critic=.true.
                  cycle
                endif

              !! not found in the list
              else
                ctx_err%ierr=-1
                ctx_err%msg ="** ERROR: Edge not found for the " //     &
                             "current tetrahedron cell         " //     &
                             " [mesh_utils_inter_infos_tetrahedron] **"
                ctx_err%critic=.true.
                cycle
              end if
              !! -------------------------------------------------------

            end if !! end if boudary -----------------------------------

          end if !! end if face not done yet ---------------------------

        end do !! end loop over neighbors ------------------------------

      end do !! end loop over cells ------------------------------------
      !! $OMP END DO
      !! $OMP END PARALLEL
      deallocate(hash_index_list)
    end if

    deallocate(inter_bc_index)

    return

  end subroutine mesh_interface_tetrahedron

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve interface infos : order and orientation
  !
  !> @param[in]    dim_domain      : domain dimension
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_neigh      : array of the cell's neighbor
  !> @param[in]    cell_face       : array of the cell's face ind
  !> @param[in]    index_loctoglob_cell: global cell index
  !> @param[in]    order_cell_array: order per cell
  !> @param[inout] order_face      : face order
  !> @param[inout] orientation     : face orientation
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_interface_simplex_orient(dim_domain,ncells,                &
                                           cell_neigh,cell_face,cell_node,   &
                                           ghost_node,index_loctoglob_cell,  &
                                           order_cell_array,order_face,      &
                                           orientation,ctx_err)

    implicit none

    integer                              ,intent(in)  :: dim_domain
    integer(kind=IKIND_MESH)             ,intent(in)  :: ncells
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_neigh(:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_face (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_node (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: ghost_node(:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: index_loctoglob_cell(:)
    integer                 ,allocatable ,intent(in)  :: order_cell_array(:)
    integer                 ,allocatable,intent(inout):: order_face (:)
    integer                 ,allocatable,intent(inout):: orientation(:,:)
    type(t_error)                        ,intent(out) :: ctx_err

    ctx_err%ierr = 0

    select case(dim_domain)
      !! 1D LINE
      case(1)
        !! in one-dimension, there is no possible orientation
        call line_face_order_orientation(ncells,cell_neigh,cell_face,   &
                                         index_loctoglob_cell,          &
                                         order_cell_array,order_face,   &
                                         orientation,ctx_err)
      !! 2D TRIANGLE
      case(2)
        call triangle_face_order_orientation(ncells,cell_neigh,         &
                                             cell_face,                 &
                                             index_loctoglob_cell,      &
                                             order_cell_array,          &
                                             order_face,                &
                                             orientation,ctx_err)

      !! 3D TETRAHEDRON
      case(3)
        call tetra_face_order_orientation(ncells,cell_neigh,cell_face,  &
                                          cell_node,ghost_node,         &
                                          index_loctoglob_cell,         &
                                          order_cell_array,order_face,  &
                                          orientation,ctx_err)

      case default
        ctx_err%msg ="** ERROR: unrecognized  dimension "            // &
                     " [mesh_utils_interface_order_orientation] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine mesh_interface_simplex_orient

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  orientation in 1D, we only count the one on the left.
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_neigh      : array of the cell's neighbor
  !> @param[in]    cell_face       : array of the cell's face ind
  !> @param[in]    index_loctoglob_cell: global cell index
  !> @param[in]    order_cell_array: order per cell
  !> @param[inout] order_face      : face order
  !> @param[inout] orientation     : face orientation
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine line_face_order_orientation(ncells,cell_neigh,cell_face,   &
                                         index_loctoglob_cell,          &
                                         order_cell_array,order_face,   &
                                         orientation,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)            ,intent(in)   :: ncells
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: cell_neigh(:,:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: cell_face (:,:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: index_loctoglob_cell(:)
    integer                 ,allocatable,intent(in)   :: order_cell_array(:)
    integer                 ,allocatable,intent(inout):: order_face (:)
    integer                 ,allocatable,intent(inout):: orientation(:,:)
    type(t_error)                        ,intent(out) :: ctx_err
    !!
    integer(kind=IKIND_MESH)  :: i_ghost,i_cell,i_face,neigh
    integer                   :: i_neigh,order_neigh!order_cell
    integer                   :: nneigh_per_cell=2
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    i_ghost = ncells

    !! in one-dimension, the order of the 'face' is forced to be one.
    order_face(:) = 1

    !! loop over cells
    do i_cell=1,ncells
      do i_neigh=1,nneigh_per_cell
        !! local face number
        i_face = cell_face (i_neigh,i_cell)

        !! the order is the max of the adjacent cell
        !! order_cell = order_cell_array(i_cell)
        neigh = cell_neigh(i_neigh,i_cell)
        !! carefull with ghost index
        if(neigh .eq. tag_ghost_cell) then
          i_ghost = i_ghost + 1 !! already init with ncell_loc
            neigh = i_ghost
        end if

        !! non-boundary: check adjacent cell ---------------------------
        if(neigh>0) then
          order_neigh        = order_cell_array(neigh)
          !! order_face(i_face) = max(order_cell,order_neigh)
          if(index_loctoglob_cell(i_cell) <= index_loctoglob_cell(neigh)) then
            orientation(i_neigh,i_cell) = 1
          else
            orientation(i_neigh,i_cell) = 2
          end if
        !! boundary ----------------------------------------------------
        else
          !! order_face (i_face)         = order_cell
          orientation(i_neigh,i_cell) = 1
        end if
      end do
    end do

    return
  end subroutine line_face_order_orientation

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve interface order and orientation for 2D triangles mesh
  !>   - example 1: for 2D triangle mesh; the interface is a segment,
  !>                there are two possible combinations to ensure that
  !>                the normal of each triangle that share a segment
  !>                is outward: at order 2, we have 3 dof per face (line)
  !>                1          3          2
  !>                x----------x----------x  INTERFACE
  !>                2          3          1
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_neigh      : array of the cell's neighbor
  !> @param[in]    cell_face       : array of the cell's face ind
  !> @param[in]    index_loctoglob_cell: global cell index
  !> @param[in]    order_cell_array: order per cell
  !> @param[inout] order_face      : face order
  !> @param[inout] orientation     : face orientation
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine triangle_face_order_orientation(ncells,cell_neigh,         &
                                             cell_face,                 &
                                             index_loctoglob_cell,      &
                                             order_cell_array,          &
                                             order_face,                &
                                             orientation,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)            ,intent(in)   :: ncells
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: cell_neigh(:,:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: cell_face (:,:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: index_loctoglob_cell(:)
    integer                 ,allocatable,intent(in)   :: order_cell_array(:)
    integer                 ,allocatable,intent(inout):: order_face (:)
    integer                 ,allocatable,intent(inout):: orientation(:,:)
    type(t_error)                        ,intent(out) :: ctx_err
    !!
    integer(kind=IKIND_MESH)  :: i_ghost,i_cell,i_face,neigh
    integer                   :: i_neigh,order_cell,order_neigh
    integer                   :: nneigh_per_cell=3
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    i_ghost = ncells

    !! loop over cells
    do i_cell=1,ncells
      do i_neigh=1,nneigh_per_cell
        !! local face number
        i_face = cell_face (i_neigh,i_cell)

        !! the order is the max of the adjacent cell
        order_cell = order_cell_array(i_cell)
        neigh = cell_neigh(i_neigh,i_cell)
        !! carefull with ghost index
        if(neigh .eq. tag_ghost_cell) then
          i_ghost = i_ghost + 1 !! already init with ncell_loc
            neigh = i_ghost
        end if

        !! non-boundary: check adjacent cell ---------------------------
        if(neigh>0) then
          order_neigh        = order_cell_array(neigh)
          order_face(i_face) = max(order_cell,order_neigh)
          if(index_loctoglob_cell(i_cell) <= index_loctoglob_cell(neigh)) then
            orientation(i_neigh,i_cell) = 1
          else
            orientation(i_neigh,i_cell) = 2
          end if
        !! boundary ----------------------------------------------------
        else
          order_face (i_face)         = order_cell
          orientation(i_neigh,i_cell) = 1
        end if
      end do
    end do

    return
  end subroutine triangle_face_order_orientation

  !---------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  Retrieve interface order and orientation for 3D tetrahedral mesh
  !>
  !>   - example 2: for 3D tetrahedral mesh; the interface is a triangle,
  !>                there are four possible combinations to ensure that
  !>                the outward normal on both side:
  !>                - we fix one of the two neighbors  (1 case)
  !>                - for the other to have outward normal, one of the
  !>                  three line of the interface (i.e. triangle) must
  !>                  be reverted; i.e. 3 more cases.
  !>                There is a total of 4 cases.
  !
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_neigh      : array of the cell's neighbor
  !> @param[in]    cell_face       : array of the cell's face ind
  !> @param[in]    index_loctoglob_cell: global cell index
  !> @param[in]    order_cell_array: order per cell
  !> @param[inout] order_face      : face order
  !> @param[inout] orientation     : face orientation
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine tetra_face_order_orientation(ncells,cell_neigh,            &
                                          cell_face,cell_node,          &
                                          ghost_node,                   &
                                          index_loctoglob_cell,         &
                                          order_cell_array,             &
                                          order_face,                   &
                                          orientation,ctx_err)

    implicit none
    integer(kind=IKIND_MESH)            ,intent(in)   :: ncells
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: cell_neigh(:,:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: cell_face (:,:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: cell_node (:,:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: index_loctoglob_cell(:)
    integer                 ,allocatable,intent(in)   :: order_cell_array(:)
    integer(kind=IKIND_MESH),allocatable,intent(in)   :: ghost_node(:,:)
    integer                 ,allocatable,intent(inout):: order_face (:)
    integer                 ,allocatable,intent(inout):: orientation(:,:)
    type(t_error)                        ,intent(out) :: ctx_err
    !!
    integer(kind=IKIND_MESH)  :: i_ghost,i_cell,i_face,neigh,jneigh
    integer(kind=IKIND_MESH)  :: inodface1(3),inodface2(3)
    integer                   :: i_neigh,order_cell,order_neigh
    integer                   :: face_combi(3,3)
    integer                   :: nneigh_per_cell=4
    logical                   :: flag_ghost
    !! -----------------------------------------------------------------
    ctx_err%ierr = 0

    !! combination of node ordering for the orientation
    face_combi(1,:) = ([1,3,2])
    face_combi(2,:) = ([2,1,3])
    face_combi(3,:) = ([3,2,1])

    i_ghost = 0
    !! loop over cells
    do i_cell=1,ncells
      do i_neigh=1,nneigh_per_cell
        flag_ghost=.false.
        !! local face number
        i_face = cell_face (i_neigh,i_cell)

        !! the order is the max of the adjacent cell
        order_cell = order_cell_array(i_cell)
        neigh = cell_neigh(i_neigh,i_cell)
        !! carefull with ghost index
        if(neigh .eq. tag_ghost_cell) then
          i_ghost   = i_ghost + 1 !! init with 0, we use ghost_node array
          neigh     = ncells  + i_ghost
          flag_ghost=.true.
        end if

        !! non-boundary: check adjacent cell ---------------------------
        if(neigh>0) then
          order_neigh        = order_cell_array(neigh)
          order_face(i_face) = max(order_cell,order_neigh)
          if(index_loctoglob_cell(i_cell) <= index_loctoglob_cell(neigh)) then
            orientation(i_neigh,i_cell) = 1
          else
            !! we need to check what is the orientation
            !! the three nodes of the current face
            inodface1(1) = cell_node(interface_3D_tetrahedron(1,i_neigh),i_cell)
            inodface1(2) = cell_node(interface_3D_tetrahedron(2,i_neigh),i_cell)
            inodface1(3) = cell_node(interface_3D_tetrahedron(3,i_neigh),i_cell)

            if(flag_ghost) then
              !! we use the ghost_node array
              do jneigh=1,nneigh_per_cell
                inodface2(1)=ghost_node(interface_3D_tetrahedron(1,jneigh),i_ghost)
                inodface2(2)=ghost_node(interface_3D_tetrahedron(2,jneigh),i_ghost)
                inodface2(3)=ghost_node(interface_3D_tetrahedron(3,jneigh),i_ghost)
                if (minval(inodface2) > 0) exit !! good face with all node found
              end do
            else
              !! the three nodes of the neighbor: need to take the good one!
              do jneigh=1,nneigh_per_cell
                if(cell_neigh(jneigh,neigh) .eq. i_cell) then
                  inodface2(1)=cell_node(interface_3D_tetrahedron(1,jneigh),neigh)
                  inodface2(2)=cell_node(interface_3D_tetrahedron(2,jneigh),neigh)
                  inodface2(3)=cell_node(interface_3D_tetrahedron(3,jneigh),neigh)
                  exit
                end if
              end do
            end if

            if(inodface1(1) .eq. inodface2(face_combi(1,1)) .and.   &
               inodface1(2) .eq. inodface2(face_combi(1,2)) .and.   &
               inodface1(3) .eq. inodface2(face_combi(1,3))) then
              orientation(i_neigh,i_cell) = 2
            elseif(inodface1(1) .eq. inodface2(face_combi(2,1)) .and. &
                   inodface1(2) .eq. inodface2(face_combi(2,2)) .and. &
                   inodface1(3) .eq. inodface2(face_combi(2,3))) then
              orientation(i_neigh,i_cell) = 3
            elseif(inodface1(1) .eq. inodface2(face_combi(3,1)) .and. &
                   inodface1(2) .eq. inodface2(face_combi(3,2)) .and. &
                   inodface1(3) .eq. inodface2(face_combi(3,3))) then
              orientation(i_neigh,i_cell) = 4
            else
              ctx_err%msg ="** ERROR: unrecognized cell orientation"  //&
                           " [tetra_face_order_orientation] **"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              call raise_error(ctx_err)
            end if
          end if

        !! boundary ----------------------------------------------------
        else
          order_face (i_face)         = order_cell
          orientation(i_neigh,i_cell) = 1
        end if
      end do
    end do

    return
  end subroutine tetra_face_order_orientation

end module m_mesh_simplex_interface
