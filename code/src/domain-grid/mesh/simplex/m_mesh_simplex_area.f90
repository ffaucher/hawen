!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex_area.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the area of simplex element:
!> area of triangle    in 2D
!> area of tetrahedron in 3D
!
!------------------------------------------------------------------------------
module m_mesh_simplex_area

  use m_define_precision,     only : RKIND_MESH
  use m_raise_error,          only : raise_error, t_error

  implicit none

  private  
  public  :: mesh_area_simplex
  
  contains  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  Compute area of a simplex from node coordinates:
  !>  - triangle    in 2D
  !>  - tetrahedron in 3D
  !
  !> @param[in]    dm              : domain dimension
  !> @param[in]    xnode           : xnode
  !> @param[out]   area            : area
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine mesh_area_simplex(dm,xnode,area,ctx_err)
    
    implicit none
    integer                              , intent(in)    :: dm
    real(kind=RKIND_MESH)                , intent(in)    :: xnode(:,:)
    real(kind=8)                         , intent(out)   :: area
    type(t_error)                        , intent(out)   :: ctx_err
    !!

    ctx_err%ierr = 0

    select case (dm)
      case (3) !!  TETRAHEDRON
        call mesh_area_tetrahedron(xnode,area,ctx_err)
      case (2) !!  TRIANGLE
        call mesh_area_triangle(xnode,area,ctx_err)
      case (1) !!  EDGE
        area = dsqrt(dble(xnode(1,1)-xnode(1,2))**2)
      case default
        ctx_err%msg   = "** ERROR: Unrecognized dimension for " // & 
                        " [mesh_area_simplex] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine mesh_area_simplex


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  Compute area of a triangle from node coordinates by
  !>  computing the face length and using Heron's formula.
  !
  !> @param[in]    xnode           : xnode
  !> @param[in]    area            : area
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine mesh_area_triangle(xnode,area,ctx_err)
    
    implicit none
    real(kind=RKIND_MESH)                , intent(in)    :: xnode(:,:)
    real(kind=8)                         , intent(out)   :: area
    type(t_error)                        , intent(out)   :: ctx_err
    !!
    real(kind=8) :: a,b,c,s 

    ctx_err%ierr = 0

    !! compute the three edges size 
    !! xnode contains the coordinates of the three nodes:
    !! xnode(:,1) => x,z for node 1
    !! xnode(:,2) => x,z for node 2
    !! xnode(:,3) => x,z for node 3
    !! -----------------------------------------------------------------
    area = 0.0
    a = dsqrt(dble(xnode(1,1)-xnode(1,2))**2+ &
              dble(xnode(2,1)-xnode(2,2))**2)
    b = dsqrt(dble(xnode(1,1)-xnode(1,3))**2+ &
              dble(xnode(2,1)-xnode(2,3))**2)
    c = dsqrt(dble(xnode(1,2)-xnode(1,3))**2+ &
              dble(xnode(2,2)-xnode(2,3))**2)
    !! s is the semiperimeter
    s = (a+b+c)/2.
    !! Triangle area using Heron's formula
    area = dsqrt(s*(s-a)*(s-b)*(s-c))

    return
  end subroutine mesh_area_triangle


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  Compute area of a tetrahedron from node coordinates
  !
  !> @param[in]    xnode           : xnode
  !> @param[in]    area            : area
  !> @param[out]   ctx_err         : error context
  !---------------------------------------------------------------------
  subroutine mesh_area_tetrahedron(xnode,area,ctx_err)
    
    implicit none
    real(kind=RKIND_MESH)                , intent(in)    :: xnode(:,:)
    real(kind=8)                         , intent(out)   :: area
    type(t_error)                        , intent(out)   :: ctx_err
    !!
    real(kind=8) :: u(3),v(3),w(3),crossprod(3)

    ctx_err%ierr = 0

    !! compute the three edges size 
    !! xnode contains the coordinates of the four nodes:
    !! xnode(:,1) => x,y,z for node 1
    !! xnode(:,2) => x,y,z for node 2
    !! xnode(:,3) => x,y,z for node 3
    !! xnode(:,4) => x,y,z for node 34
    !! -----------------------------------------------------------------
    area = 0.0
    
    !! the area is given by 
    !!  1/6* (node1-node4) . [(node2-node4)x(node3-node4)]
    !!  using dot and cross product.
    !!
    u = dble(xnode(:,1) - xnode(:,4))
    v = dble(xnode(:,2) - xnode(:,4))
    w = dble(xnode(:,3) - xnode(:,4))
    !! cross produce between v and w 
    crossprod(1) = v(2)*w(3)-w(2)*v(3)
    crossprod(2) = v(3)*w(1)-w(3)*v(1)
    crossprod(3) = v(1)*w(2)-w(1)*v(2)
    !! compute dot product
    area = 1./6.*abs(u(1)*crossprod(1)+u(2)*crossprod(2)+u(3)*crossprod(3))
    return
  end subroutine mesh_area_tetrahedron
  
end module m_mesh_simplex_area
