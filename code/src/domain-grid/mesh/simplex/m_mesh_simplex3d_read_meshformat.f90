!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex3d_read_meshformat.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the domain mesh files (from .mesh in 3D)
!
!------------------------------------------------------------------------------
module m_mesh_simplex3d_read_meshformat
  
  use m_define_precision,       only : IKIND_MESH, RKIND_MESH
  use m_broadcast,              only : broadcast  
  use m_barrier,                only : barrier
  use m_time,                   only : time
  use m_raise_error,            only : raise_error, t_error
  use m_distribute_error,       only : distribute_error
  use m_ctx_domain,             only : t_domain
  use m_mesh_simplex_interface, only : mesh_interface_simplex
  use m_mesh_simplex_neighbors, only : mesh_simplex_neighbors_tetra
  use m_mesh_simplex_compute_faces, only : mesh_simplex_faces_tetra,    &
                                           mesh_simplex_faces_tetra_hash
  use m_ctx_parallelism,        only : t_parallelism
  
  implicit none

  private  
  public :: mesh_read_meshformat3d, mesh_read_init_meshformat3d
   
  contains  



  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine reads the input 3D mesh files in .MESH format
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read_meshformat3d(ctx_paral,mesh,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: mesh
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    integer(kind=IKIND_MESH)  , allocatable :: inter_node(:,:)
    integer                   , allocatable :: mark_inter(:)
    logical                                 :: file_exists,end_file
    character(len=512)                      :: current_line
    integer                                 :: mark,io
    integer                                 :: unit_mesh=120,counter
                                            !! tetrahedron mesh
    real   (kind=8)                         :: node_coo  (3) ! 3 coo per node
    integer(kind=IKIND_MESH)                :: node_cell (4) ! 4 node per cell
    integer(kind=IKIND_MESH)                :: face_node (3) ! 4 node per face
    integer(kind=IKIND_MESH)                :: k
    real   (kind=8)                         :: time0,time1
    !! Because in the .mesh, we only have the BOUNDARY edges
    integer(kind=IKIND_MESH)                :: nface_bound
    integer(kind=IKIND_MESH)  , allocatable :: inter_node_bdry(:,:)
    integer                   , allocatable :: mark_inter_bdry(:)

    ctx_err%ierr  = 0
    
    !! general informations for triangle meshes
    mesh%format_element         ='tetrahedron'
    mesh%n_node_per_cell        =4 ! 4 nodes per tetrahedron
    mesh%n_node_per_face        =3 ! 3 nodes define the tetrahedron face
    mesh%n_neigh_per_cell       =4 ! 4 neighbors per tetrahedron
    mesh%n_node_per_face_shared =3
    !! interface is a triangle, for two adjacents there are 4 possible
    !! orientations (see context_dg for more infos)
    mesh%n_interface_combi      =4 
    
    call time(time0)
    !! check that mesh file exist --------------------------------------
    !! .edge ; .ele ; .face ; .neigh ; .node
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.mesh'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.mesh')   //   &
                   " DOEST NOT EXIST [mesh_read_meshformat] "
      ctx_err%critic=.true.
    endif
    !! end check files exist -------------------------------------------

    !! -----------------------------------------------------------------
    !! only master reads everything
    if(ctx_paral%master) then
      !! Open file -----------------------------------------------------
      open (unit=unit_mesh,file=trim(trim(adjustl(mesh%file_mesh))//'.mesh' ))
      !! allocation
      allocate(mesh%x_node         (mesh%dim_domain      ,mesh%n_node_gb))
      allocate(mesh%cell_node      (mesh%n_node_per_cell ,mesh%n_cell_gb))
      allocate(mesh%cell_neigh     (mesh%n_neigh_per_cell,mesh%n_cell_gb))
      allocate(mesh%cell_face      (mesh%n_neigh_per_cell,mesh%n_cell_gb))   
      mesh%x_node     = 0.
      mesh%cell_node  = 0
      mesh%cell_neigh =-1
      mesh%cell_face  = 0

      end_file = .false.
      counter  = 0

      !! read file for interesting fields 
      do while(.not. end_file .and. counter .ne. 3)
        read(unit_mesh,"(a)",iostat=io)  current_line
        if(is_iostat_end(io)) then
          end_file=.true.
        else
          !! check line is non-empty or not commentary
          if(len_trim(adjustl(current_line)).ne.0 .and.                 &
             current_line(1:1) .ne. "#") then
            !! check if it is an useful keyword
            select case (trim(adjustl(current_line)))
              case('Vertices')
                !! the next lines are the nodes
                read(unit_mesh ,*) mesh%n_node_gb
                !! a) nodes --------------------------------------------
                do k=1,mesh%n_node_gb
                  read(unit_mesh,*)  node_coo(:)  !, cline
                  mesh%x_node(:,k) = real(node_coo(:),kind=RKIND_MESH)  !! cline is unused tag
                end do
                counter = counter + 1
              case('Triangles')
                ! -------------------------------------------------
                !! the next lines are the BOUNDARY faces only
                ! -------------------------------------------------
                read(unit_mesh ,*) nface_bound
                !! b) boundary markers ---------------------------------
                allocate(inter_node_bdry(mesh%n_node_per_face,nface_bound))
                allocate(mark_inter_bdry(nface_bound))
                do k=1,nface_bound
                   read(unit_mesh,*) face_node(:), mark
                   inter_node_bdry(:,k) = face_node(:)
                   mark_inter_bdry  (k) = mark
                end do
                counter = counter + 1             
              case('Tetrahedra')
                !! the next lines are the tetrahedra
                read(unit_mesh ,*) mesh%n_cell_gb
                do k=1,mesh%n_cell_gb
                  read(unit_mesh ,*)    node_cell(:)  !! , cline
                  mesh%cell_node(:,k) = node_cell(:)  !! cline is unused tag
                end do
                counter = counter + 1
              !! case('Dimension') !! unused possibilities
              !! case('Corners')   !! unused possibilities
              !! case('Edges')     !! unused possibilities
              case default
                !! nothing useful
            end select
          end if
        end if
      end do
      !! close files
      close(unit_mesh)
      !! -----------------------------------------------------------------
      !! Because we have the number of boundary edges only,
      !! we need to anticipate the Total number of edges...
      mesh%n_face_gb = (4*mesh%n_cell_gb + nface_bound)/2
      !! -----------------------------------------------------------------

      !! ---------------------------------------------------------------
      !! We compute the list of all edges
      !! ---------------------------------------------------------------
      allocate(inter_node(mesh%n_node_per_face,mesh%n_face_gb))
      allocate(mark_inter(mesh%n_face_gb))
      mark_inter = 0
      inter_node = 0
      !! call mesh_simplex_faces_tetra(mesh%n_cell_gb,nface_bound,      &
      !!                               mesh%cell_node,inter_node_bdry,  &
      !!                               mark_inter_bdry,inter_node,      &
      !!                               mark_inter,ctx_err)
      call mesh_simplex_faces_tetra_hash(mesh%n_cell_gb,nface_bound,    &
                                    mesh%cell_node,inter_node_bdry,     &
                                    mark_inter_bdry,inter_node,         &
                                    mark_inter,ctx_err)
      deallocate(inter_node_bdry)
      deallocate(mark_inter_bdry)

      ! ----------------------------------------------------------------
      
            !! ---------------------------------------------------------------
      !! Interface information, we first need to identify the 
      !! neighbors of every cells...
      !! ---------------------------------------------------------------
      call mesh_simplex_neighbors_tetra(mesh%n_cell_gb,mesh%cell_node,  &
                                        mesh%cell_neigh,ctx_err)

      !! get interface informations in 3D
      call mesh_interface_simplex(3,mesh%n_cell_gb,                          &
                                  mesh%n_neigh_per_cell,mesh%n_face_gb,      &
                                  mesh%cell_node,                            &
                                  mesh%cell_neigh,mesh%cell_face,inter_node, &
                                  mark_inter,mesh%tag_n,                     &
                                  mesh%tag_bc_array,                         &
                                  mesh%n_face_interior_gb,                   &
                                  mesh%n_face_boundary_gb,ctx_err)

      !! check count 
      if(mesh%n_face_interior_gb+mesh%n_face_boundary_gb.ne.mesh%n_face_gb) then
        ctx_err%ierr=-1
        ctx_err%msg =trim(adjustl(ctx_err%msg)) //                      &
                     "** ERROR: Incorrect number of edges found "    // &
                     " [mesh_read_meshformat] **"
        ctx_err%critic=.true.
      end if

      deallocate(inter_node)
      deallocate(mark_inter)
      
      !! bound coordinates
      mesh%bounds_xmin=minval(mesh%x_node(1,:))
      mesh%bounds_xmax=maxval(mesh%x_node(1,:))
      mesh%bounds_ymin=minval(mesh%x_node(2,:))
      mesh%bounds_ymax=maxval(mesh%x_node(2,:))
      mesh%bounds_zmin=minval(mesh%x_node(3,:))
      mesh%bounds_zmax=maxval(mesh%x_node(3,:))

      !! ---------------------------------------------------------------
    end if !! end if master
    !! -----------------------------------------------------------------

    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 
    
    !! send the global infos to everyone
    call broadcast(mesh%bounds_xmin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_xmax,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_ymin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_ymax,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_zmin,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%bounds_zmax,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%n_node_gb  ,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%n_cell_gb  ,1,0,ctx_paral%communicator,ctx_err)
    call broadcast(mesh%n_face_gb  ,1,0,ctx_paral%communicator,ctx_err)

    call time(time1)
    mesh%time_read = time1-time0
    
    return
  end subroutine mesh_read_meshformat3d
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine reads the input 3D mesh files in .mesh format
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read_init_meshformat3d(ctx_paral,mesh,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: mesh
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    logical                  :: file_exists,end_file    
    character(len=512)       :: current_line
    integer                  :: counter,io
    integer                  :: unit_mesh=120
    real(kind=8)             :: time0,time1
    integer(kind=IKIND_MESH) :: ncell_loc, nface_loc
    integer                  :: int_size       = 4
    integer                  :: int_size_mesh  = IKIND_MESH
    integer                  :: real_size      = 4
    integer                  :: real_size_mesh = RKIND_MESH
    integer(kind=8)          :: mem_count
    
    !! Because in the .mesh, we only have the BOUNDARY edges
    integer                                 :: nface_bound

    ctx_err%ierr  = 0
    call time(time0)

    !! general informations
    mesh%format_element  ='tetrahedron'
    mesh%n_node_per_cell =4 ! 4 nodes per tetrahedron
    mesh%n_node_per_face =3 ! 3 nodes define the tetrahedron face
    mesh%n_neigh_per_cell=4 ! 4 neighbors per tetrahedron
    !! interface is a triangle, for two adjacents there are 4 possible
    !! orientations (see context_dg for more infos)
    mesh%n_interface_combi=4 
    
    !! check that mesh file exist --------------------------------------
    !! .edge ; .ele ; .face ; .neigh ; .node
    inquire(file=trim(trim(adjustl(mesh%file_mesh))//'.mesh'),exist=file_exists)
    if(.not. file_exists) then
      ctx_err%ierr=-1
      ctx_err%msg ="** MESH FILE 3D PARAMETER FILE "               //   &
                    trim(trim(adjustl(mesh%file_mesh))//'.mesh')   //   &
                   " DOEST NOT EXIST [mesh_read_meshformat] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    !! end check files exist -------------------------------------------

    !! Open file -------------------------------------------------------
    open (unit=unit_mesh,file=trim(trim(adjustl(mesh%file_mesh))//'.mesh' ))
    !! look for informations: 
    !! number of nodes, number of cells, number of faces
    counter = 0
    mesh%dim_domain = 0
    mesh%n_node_gb  = 0
    mesh%n_face_gb  = 0
    mesh%n_cell_gb  = 0
    !! -----------------------------------------------------------------
    end_file = .false.
    do while(.not. end_file .and. counter .ne. 4)
      read(unit_mesh,"(a)",iostat=io)  current_line
      if(is_iostat_end(io)) then
        end_file=.true.
      else
        !! check line is non-empty or not commentary
        if(len_trim(adjustl(current_line)).ne.0 .and.                   &
           current_line(1:1) .ne. "#") then
          !! check if it is an useful keyword
          select case (trim(adjustl(current_line)))
            case('Dimension')
              !! the next line is the dimension
              read(unit_mesh ,*) mesh%dim_domain
              counter = counter + 1
            case('Dimension 3')
              !! the next line is the dimension
              mesh%dim_domain = 3
              counter = counter + 1
            case('Vertices')
              !! the next line is the number of nodes
              read(unit_mesh ,*) mesh%n_node_gb
              counter = counter + 1
            case('Triangles')
              !! the next line is the number faces
              read(unit_mesh ,*) nface_bound
              counter = counter + 1
            case('Tetrahedra')
              !! the next line is the number cells
              read(unit_mesh ,*) mesh%n_cell_gb
              counter = counter + 1
            !! case('Corners')
            !! case('Edges')
            case default
              !! nothing useful
          end select
        end if
      end if
    end do
    !! close files
    close(unit_mesh)
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    !! Because we have the number of boundary edges only,
    !! we need to anticipate the Total number of edges...
    mesh%n_face_gb = (4*mesh%n_cell_gb + nface_bound)/2
    !! -----------------------------------------------------------------
    
    !! consistency checks
    if(mesh%dim_domain .ne. 3) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: dimension in .mesh file [mesh_read_mesh] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(mesh%n_node_gb <= 0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: No node found in .mesh file [mesh_read_mesh] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(mesh%n_face_gb <= 0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: No face found in .mesh file [mesh_read_mesh] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    if(mesh%n_cell_gb <= 0) then
      ctx_err%ierr=-1
      ctx_err%msg ="** ERROR: No cell found in .mesh file [mesh_read_mesh] "
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    !! -----------------------------------------------------------------

    !! anticipate memory requirement
    mem_count=0

    !! memory peark is during partitioning 
    mem_count =(9*int_size_mesh)                 + &  !! 8 integers parameters
               (6*real_size_mesh)                + &  !! bounding box
               (6*int_size)                      + &  !! 6 integers 
               (8*2*real_size)                   + &  !! 8 real (kind=8)
               (1*256)                           + &  !! 1 character(len=256)
               (1*32)                            + &  !! 2 character(len=32)
               (3*real_size*10)                  + &  !! ~ 10 tags array
               !! mesh%x_node
               ((mesh%n_node_gb)*real_size_mesh) + &
               !! mesh%cell_node 
               ((mesh%n_cell_gb*mesh%n_node_per_cell)*int_size_mesh)  + &
               !! mesh%cell_bary
               ((mesh%n_cell_gb*mesh%dim_domain)*real_size_mesh)      + &
               !! mesh%neigh 
               ((mesh%n_cell_gb*mesh%n_neigh_per_cell)*int_size_mesh) + &
               !! mesh%cell_face
               ((mesh%n_cell_gb*mesh%n_neigh_per_cell)*int_size_mesh) + &
               !! mesh%order
               ((mesh%n_cell_gb)*int_size) + ((mesh%n_face_gb)*int_size)


    !! peak when reading, in monoproc we need additional array
    mesh%memory_peak_read = mem_count +  &
               !! interface node and interface markers
               ((mesh%n_face_gb*mesh%n_node_per_cell)*int_size_mesh) + &
               ((mesh%n_face_gb                     )*int_size)
    
    !! peak when partitioning, we have simultaneously monoproc memory
    !! and additional requirement
    mesh%memory_peak_part = mem_count            +  &
        ((mesh%n_cell_gb*int_size  ))        +  &  !! proc cell
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! cells_pproc 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! ghost_pproc  
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! offset_cells 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! offset_ghost 
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! counter_cells
        ((ctx_paral%nproc*int_size_mesh))    +  &  !! counter_ghost
        ((mesh%n_cell_gb*int_size_mesh))     +  &  !! ordered_cellindex
        ((mesh%n_cell_gb*mesh%n_node_per_cell *int_size_mesh))+& !ordered_cellnode
        ((mesh%n_cell_gb*mesh%n_neigh_per_cell*int_size_mesh))+& !ordered_cellneigh
        ((mesh%n_cell_gb*mesh%n_neigh_per_cell*int_size_mesh))+& !ordered_cellface
        ((mesh%n_cell_gb*mesh%dim_domain)*real_size_mesh) + & !! barycenters
        ((mesh%n_cell_gb*int_size))          +  &  !! ordered_cellorder
        ((mesh%n_node_gb)*real_size_mesh)    +  &  !! temp_xnode
        ((mesh%n_node_gb)*int_size_mesh)     +  &  !! index_loctoglob_node
        ((mesh%n_face_gb)*int_size_mesh)     +  &  !! index_loctoglob_face
        !! another cell_node required at partitioning
        ((mesh%n_cell_gb*mesh%n_node_per_cell)*int_size_mesh)

    !! approximate memory peak on slave assuming good partitioning
    !! peak when partitioning, we have simultaneously monoproc memory
    !! and other proc infos 
    if(ctx_paral%nproc == 1) then
      ncell_loc = mesh%n_cell_gb
      nface_loc = mesh%n_face_gb
    else
      ncell_loc = int(1.1*ceiling(real(mesh%n_cell_gb) / real(ctx_paral%nproc)))
      nface_loc = int(1.4*ceiling(real(mesh%n_face_gb) / real(ctx_paral%nproc)))
    end if

    mesh%memory_peak_part = mesh%memory_peak_part +  &
        ((ncell_loc*int_size)               ) +  & !! ordered_ghostorder
        ((ncell_loc*mesh%n_node_per_cell))    +  & !! array_itemp
        !! cell_face_orientation
        ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh)

    !! add ctx_mesh corresponding to every non-master proc 
    mesh%memory_peak_other =  (9*int_size_mesh)     + &  !! 8 integers parameters
               (6*real_size_mesh)                   + &  !! bounding box
               (6*int_size)                         + &  !! 6 integers 
               (8*2*real_size)                      + &  !! 8 real (kind=8)
               (1*256)                              + &  !! 1 character(len=256)
               (1*32)                               + &  !! 2 character(len=32)
               (3*real_size*10)                     + &  !! ~ 10 tags array
               ((ncell_loc)*real_size_mesh)         + &  !! mesh%x_node
               !! mesh%cell_node is counted twice for ghost infos during 
               !! partitioning
               ((ncell_loc*mesh%n_node_per_cell)*int_size_mesh)*2+ & 
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! mesh%neigh 
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! mesh%cell_face
               ((ncell_loc)*int_size)               +  &  !! mesh%order
               ((mesh%n_node_gb)*real_size_mesh)    +  &  !! temp_xnode
               ((mesh%n_node_gb)*int_size_mesh)     +  &  !! index_loctoglob_node
               ((nface_loc)*int_size_mesh)          +  &  !! index_loctoglob_face
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh) + & !! local_cell_face
               ((nface_loc)*int_size)               +  &  !! order_face
               !! cell_face_orientation
               ((ncell_loc*mesh%n_neigh_per_cell)*int_size_mesh)

    call time(time1)
    mesh%time_init = time1-time0
    
    return
  end subroutine mesh_read_init_meshformat3d

end module m_mesh_simplex3d_read_meshformat
