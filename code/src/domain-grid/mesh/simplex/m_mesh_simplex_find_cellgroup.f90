!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_simplex_find_cellgroup.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to find the cells that are within a certain
!> radius of a given position, for 1D segment, 2D triangle mesh and 
!> 3D tetrahedral mesh.
!
!------------------------------------------------------------------------------
module m_mesh_simplex_find_cellgroup

  use omp_lib  
  use m_raise_error,        only : raise_error, t_error
  use m_define_precision,   only : IKIND_MESH,RKIND_MESH
  
  implicit none

  private
  
  public  :: find_cellgroup_simplex
  private :: find_cellgroup_segment 
  private :: find_cellgroup_triangle
  private :: find_cellgroup_tetrahedron
  
  contains



  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The interface finds the group of cells that are at maximal distance
  !>  R from a given point. 
  !
  !> @param[in]   x0            : coordinates of the central position
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   n_cell        : number of local cells to search
  !> @param[in]   cell_neigh    : list of list of neighbors (2D array)
  !> @param[in]   dim_domain    : domain dimension
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine find_cellgroup_simplex(x0,radius,n_cell_found,cells_found, &
                                    x_node,cell_node,n_cell,            &
                                    dim_domain,ctx_err)
    
    implicit none

    real   (kind=8)                      ,intent(in)   :: x0(:)
    real   (kind=8)                      ,intent(in)   :: radius
    integer(kind=IKIND_MESH)             ,intent(out)  :: n_cell_found
    integer(kind=IKIND_MESH),allocatable ,intent(out)  :: cells_found(:)
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node(:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    integer                              ,intent(in)   :: dim_domain
    type(t_error)                        ,intent(out)  :: ctx_err
    
    ctx_err%ierr  = 0
    n_cell_found  = 0
    
    !! depends on the dimension
    select case(dim_domain)
      case(1)
        !! 1D LINE
        call find_cellgroup_segment(x0,radius,n_cell_found,cells_found, &
                                    x_node,cell_node,n_cell,ctx_err)
      case(2)
        !! 2D TRIANGLE
        call find_cellgroup_triangle(x0,radius,n_cell_found,cells_found,&
                                     x_node,cell_node,n_cell,ctx_err)
      case(3)
        !! 3D TETRAHEDRON
        call find_cellgroup_tetrahedron(x0,radius,n_cell_found,         &
                                        cells_found,x_node,cell_node,   &
                                        n_cell,ctx_err)

      case default
        ctx_err%msg   = "** ERROR: Unrecognized dimension for " // & 
                        " [find_cell_simplex] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select    
    !! -----------------------------------------------------------------
    return
  end subroutine find_cellgroup_simplex
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the cell in which the given point for 1D line.
  !
  !> @param[in]   x0              : coordinates of the central point of the area
  !> @param[in]   radius          : radius of the area
  !> @param[out]  n_cell_in_group : number of cell found in the area
  !> @param[out]  cell_in_group   : output cell number
  !> @param[in]   x_node          : list of node coordinates for all cells
  !> @param[in]   cell_node       : list of node for all cells
  !> @param[in]   n_cell          : number of cells in the subdomain
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine find_cellgroup_segment(x0,radius,n_cell_in_group,          &
                                    cell_in_group,x_node,cell_node,     &
                                    n_cell,ctx_err)
    
    implicit none
    
    real   (kind=8)                      ,intent(in)   :: x0(1)
    real   (kind=8)                      ,intent(in)   :: radius
    integer(kind=IKIND_MESH)             ,intent(out)  :: n_cell_in_group
    integer(kind=IKIND_MESH),allocatable ,intent(out)  :: cell_in_group(:)
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node   (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    type(t_error)                        ,intent(out)  :: ctx_err
    !! 
    integer                              :: k
    integer(kind=IKIND_MESH)             :: i_cell,node
    real   (kind=8)                      :: xmin,xmax,xrmin,xrmax,dist
    integer(kind=IKIND_MESH),allocatable :: temp_array(:)
    
    ctx_err%ierr  = 0
    
    n_cell_in_group = 0
    
    !! -----------------------------------------------------------------
    !! check if this subdomain is concerned ----------------------------  
    xrmin = x0(1)-radius
    xrmax = x0(1)+radius
    xmin=dble(minval(x_node(1,:)))
    xmax=dble(maxval(x_node(1,:)))
    if(xrmax < xmin .or. xrmin > xmax) return
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! loop over each cell, and fill a temporary array
    allocate(temp_array(n_cell))
    temp_array      = 0
    n_cell_in_group = 0
    do i_cell=1,n_cell
      
      !! 2 nodes in each segment ---------------------------------------
      do k=1,2

        node = cell_node(k,i_cell)
        dist = abs(x_node(1,node) - x0(1))
        if(dist .le. radius) then
          n_cell_in_group             = n_cell_in_group + 1
          temp_array(n_cell_in_group) = i_cell
               !! cell must be counted once:
          exit !! we go to the next cell then
        end if
      
      end do ! end loop over edges
      !! ---------------------------------------------------------------

    end do
    
    !! save
    allocate(cell_in_group(n_cell_in_group))
    cell_in_group(1:n_cell_in_group) = temp_array(1:n_cell_in_group)
    deallocate(temp_array)

    return
  end subroutine find_cellgroup_segment


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the cell in which the given point for 2D simplexes.
  !
  !> @param[in]   x0              : coordinates of the central point of the area
  !> @param[in]   radius          : radius of the area
  !> @param[out]  n_cell_in_group : number of cell found in the area
  !> @param[out]  cell_in_group   : output cell number
  !> @param[in]   x_node          : list of node coordinates for all cells
  !> @param[in]   cell_node       : list of node for all cells
  !> @param[in]   n_cell          : number of cells in the subdomain
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine find_cellgroup_triangle(x0,radius,n_cell_in_group,         &
                                     cell_in_group,x_node,cell_node,    &
                                     n_cell,ctx_err)
    
    implicit none
    
    real   (kind=8)                      ,intent(in)   :: x0(2)
    real   (kind=8)                      ,intent(in)   :: radius
    integer(kind=IKIND_MESH)             ,intent(out)  :: n_cell_in_group
    integer(kind=IKIND_MESH),allocatable ,intent(out)  :: cell_in_group(:)
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node   (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    type(t_error)                        ,intent(out)  :: ctx_err
    !! 
    integer                              :: k
    integer(kind=IKIND_MESH)             :: i_cell,node
    real   (kind=8)                      :: dist,pt(2)
    real   (kind=8)                      :: xmin,xmax,zmin,zmax
    integer(kind=IKIND_MESH),allocatable :: temp_array(:)
    
    ctx_err%ierr  = 0
    
    n_cell_in_group = 0
    
    !! -----------------------------------------------------------------
    ! check if this subdomain is concerned: Does x0 belong to the domain 
    ! represented by the rectangle using the limit coordinates: 
    xmin=dble(minval(x_node(1,:)))
    xmax=dble(maxval(x_node(1,:)))
    zmin=dble(minval(x_node(2,:)))
    zmax=dble(maxval(x_node(2,:)))
    ! this check is probably not optimal.
    if(x0(1)-radius > xmax) then
      if(x0(1)+radius < xmin) then
        if(x0(2)-radius > zmax) then
          if(x0(2)+radius < zmin) then
            return
          end if
        end if
      end if
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! loop over each cell, and fill a temporary array
    allocate(temp_array(n_cell))
    temp_array      = 0
    n_cell_in_group = 0
    do i_cell=1,n_cell
      
      !! 3 nodes in each cell ------------------------------------------
      do k=1,3
      
        node = cell_node(k,i_cell)
        pt(1) = x_node(1,node)-x0(1) 
        pt(2) = x_node(2,node)-x0(2) 
        dist=norm2(pt)
        if(dist .le. radius) then
          n_cell_in_group             = n_cell_in_group + 1
          temp_array(n_cell_in_group) = i_cell
               !! cell must be counted once:
          exit !! we go to the next cell then
        end if
      
      end do ! end loop over edges
      !! ---------------------------------------------------------------

    end do
    
    !! save
    allocate(cell_in_group(n_cell_in_group))
    cell_in_group(1:n_cell_in_group) = temp_array(1:n_cell_in_group)
    deallocate(temp_array)

    return
  end subroutine find_cellgroup_triangle


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the cell in which the given point for 3D simplexes.
  !
  !> @param[in]   x0              : coordinates of the central point of the area
  !> @param[in]   radius          : radius of the area
  !> @param[out]  n_cell_in_group : number of cell found in the area
  !> @param[out]  cell_in_group   : output cell number
  !> @param[in]   x_node          : list of node coordinates for all cells
  !> @param[in]   cell_node       : list of node for all cells
  !> @param[in]   n_cell          : number of cells in the subdomain
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine find_cellgroup_tetrahedron(x0,radius,n_cell_in_group,      &
                                        cell_in_group,x_node,cell_node, &
                                        n_cell,ctx_err)
    
    implicit none
    
    real   (kind=8)                      ,intent(in)   :: x0(3)
    real   (kind=8)                      ,intent(in)   :: radius
    integer(kind=IKIND_MESH)             ,intent(out)  :: n_cell_in_group
    integer(kind=IKIND_MESH),allocatable ,intent(out)  :: cell_in_group(:)
    real   (kind=RKIND_MESH),allocatable ,intent(in)   :: x_node   (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)   :: cell_node(:,:)
    integer(kind=IKIND_MESH)             ,intent(in)   :: n_cell
    type(t_error)                        ,intent(out)  :: ctx_err
    !! 
    integer                              :: k
    integer(kind=IKIND_MESH)             :: i_cell,node
    real   (kind=8)                      :: dist,pt(3)
    real   (kind=8)                      :: xmin,xmax,zmin,zmax,ymin,ymax
    integer(kind=IKIND_MESH),allocatable :: temp_array(:)
    
    ctx_err%ierr  = 0
    
    n_cell_in_group = 0
    
    !! -----------------------------------------------------------------
    !! check if this subdomain is concerned ----------------------------  
    xmin=dble(minval(x_node(1,:)))
    xmax=dble(maxval(x_node(1,:)))
    ymin=dble(minval(x_node(2,:)))
    ymax=dble(maxval(x_node(2,:)))
    zmin=dble(minval(x_node(3,:)))
    zmax=dble(maxval(x_node(3,:)))
    pt(1) = xmin-x0(1) ; pt(2) = ymin-x0(2); pt(3) = zmin-x0(3) ; dist=norm2(pt)

    ! this check is probably not optimal.
    if(x0(1)-radius > xmax) then
      if(x0(1)+radius < xmin) then
        if(x0(2)-radius > ymax) then
          if(x0(2)+radius < ymin) then
            if(x0(3)-radius > zmin) then
              if(x0(3)+radius < zmax) then
                return
              end if
            end if
          end if
        end if
      end if
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! loop over each cell, and fill a temporary array
    allocate(temp_array(n_cell))
    temp_array      = 0
    n_cell_in_group = 0
    do i_cell=1,n_cell
      
      !! 4 nodes in each cell ------------------------------------------
      do k=1,4
      
        node = cell_node(k,i_cell)
        pt(1) = x_node(1,node)-x0(1) 
        pt(2) = x_node(2,node)-x0(2) 
        pt(3) = x_node(3,node)-x0(3) 
        dist=norm2(pt)
        if(dist .le. radius) then
          n_cell_in_group             = n_cell_in_group + 1
          temp_array(n_cell_in_group) = i_cell
               !! cell must be counted once:
          exit !! we go to the next cell then
        end if
      
      end do ! end loop over edges
      !! ---------------------------------------------------------------

    end do
    
    !! save
    allocate(cell_in_group(n_cell_in_group))
    cell_in_group(1:n_cell_in_group) = temp_array(1:n_cell_in_group)
    deallocate(temp_array)

    return
  end subroutine find_cellgroup_tetrahedron

end module m_mesh_simplex_find_cellgroup
