!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_find_index_cell.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used for the domain mesh for reordering or partitioning
!
!------------------------------------------------------------------------------
module m_mesh_find_index_cell

  use m_define_precision,     only : IKIND_MESH, RKIND_MESH
  use m_raise_error,          only : raise_error, t_error

  implicit none

  private  
  public :: mesh_find_index_cell

  contains  


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine checks if the index icell_gb is already in the index 
  !>  array and gives the local index index_cell. If the cell is not already
  !>  in the array, it returns the value ncell+1
  !> @details
  !
  !----------------------------------------------------------------------------
  subroutine mesh_find_index_cell(ncell,icell_gb,index_cell, &
                                  index_loctoglob_cell)
  
    implicit none
    integer(kind=IKIND_MESH)            , intent(in) :: ncell
    integer(kind=IKIND_MESH)            , intent(in) :: icell_gb
    integer(kind=IKIND_MESH)            , intent(out):: index_cell
    integer(kind=IKIND_MESH),allocatable, intent(in) :: index_loctoglob_cell(:)
    !! local
    integer(kind=IKIND_MESH) :: c
  
    !! Check if cell is already in index array 
    index_cell=0
    do c=1,ncell
      if(index_loctoglob_cell(c) == icell_gb) then
        index_cell=c
        return
      endif
    enddo
    !! otherwise just give next
    if(index_cell==0) then
      index_cell=ncell+1
    endif
    
    return
  end subroutine mesh_find_index_cell
  
end module m_mesh_find_index_cell
