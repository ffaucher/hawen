!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_domain_derivative_model.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to compute the derivative of a model given
!> by a piece-wise constant representation: 1 value per cell.
!
!------------------------------------------------------------------------------
module m_domain_derivative_model

  use omp_lib  
  use m_define_precision,         only : IKIND_MESH
  use m_raise_error,              only : raise_error, t_error
  use m_ctx_domain,               only : t_domain
  use m_tag_bc,                   only : tag_ghost_cell
                                        
  implicit none

  private  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the directional derivative of a model given over the
  !> domain (mesh)
  !
  !> @param[in]   mesh          : mesh context
  !> @param[in]   model_repr    : how is the model represented
  !> @param[in]   model         : the array model, it contains the values
  !>                              for the local cell, and then for the 
  !>                              ghosts
  !> @param[in]   model_deriv   : the output derivative, one per directions
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  interface model_domain_gradient
     module procedure model_domain_gradient_real4
  end interface

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Compute the divergence of a model given over the domain (mesh)
  !
  !> @param[in]   mesh          : mesh context
  !> @param[in]   model_repr    : how is the model represented
  !> @param[in]   model         : the array model, it contains the values
  !>                              for the local cell, and then for the 
  !>                              ghosts
  !> @param[in]   model_deriv   : the output derivative, one per directions
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  interface model_domain_divergence
     module procedure model_domain_divergence_vect_real4
     module procedure model_domain_divergence_arr2d_real4
  end interface

  public :: model_domain_gradient, model_domain_divergence

  contains  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the directional derivative of a model
  !>  given per cell
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[in]   model_repr    : how is the model represented
  !> @param[in]   model         : the array model, it contains the values
  !>                              for the local cell, and then for the 
  !>                              ghosts
  !> @param[in]   model_deriv   : the output derivative, one per directions
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine model_domain_gradient_real4(ctx_mesh,model_representation,&
                                           model,model_deriv,ctx_err)

    implicit none

    type(t_domain)     ,intent(in)        :: ctx_mesh
    character(len=*)   ,intent(in)        :: model_representation
    real               ,intent(in)        :: model(:)
    real               ,intent(inout)     :: model_deriv(:,:)
    type(t_error)      ,intent(out)       :: ctx_err

    ctx_err%ierr = 0
    model_deriv  = 0.d0

    !! we can only have piecewise-constant for now.
    select case(trim(adjustl(model_representation)))
      case('piecewise-constant')
        call model_gradient_pconstant(ctx_mesh,model,model_deriv,ctx_err)
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** ERROR: model directional derivation only " //  &
                     "works with piecewise-constant for now "       //  &
                     "[mesh_model_deriv] **"
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    

    return
  end subroutine model_domain_gradient_real4

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the divergence of a model given per cell
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[in]   model_repr    : how is the model represented
  !> @param[in]   model         : the array model, it contains the values
  !>                              for the local cell, and then for the 
  !>                              ghosts
  !> @param[in]   model_div     : the output divergence
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine model_domain_divergence_vect_real4(ctx_mesh,model_representation,&
                                           model,model_divergence,ctx_err)

    implicit none

    type(t_domain)     ,intent(in)        :: ctx_mesh
    character(len=*)   ,intent(in)        :: model_representation
    real               ,intent(in)        :: model(:)
    real               ,intent(inout)     :: model_divergence(:)
    type(t_error)      ,intent(out)       :: ctx_err
    !! 
    integer(kind=IKIND_MESH)  :: i_cell
    real,allocatable          :: model_deriv(:,:)

    ctx_err%ierr = 0
    model_divergence = 0.d0

    !! we can only have piecewise-constant for now.
    select case(trim(adjustl(model_representation)))
      case('piecewise-constant')
        !! compute both directional derivative
        allocate(model_deriv(ctx_mesh%n_cell_loc,ctx_mesh%dim_domain)) 
        model_deriv = 0.d0
        call model_gradient_pconstant(ctx_mesh,model,model_deriv,ctx_err)
        !! sum for the divergence
        do i_cell = 1,ctx_mesh%n_cell_loc
          model_divergence(i_cell) = sum(model_deriv(i_cell,:))
        end do
        deallocate(model_deriv)
      
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** ERROR: model directional derivation only " //  &
                     "works with piecewise-constant for now "       //  &
                     "[mesh_model_deriv] **"
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return
  end subroutine model_domain_divergence_vect_real4

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the divergence of a 2D model given per cell
  !>  the input contains information of size (n_cell, dim_domain)
  !>  and the output gives, e.g. in 2D: dx(:,1) + dz(:,2)
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[in]   model_repr    : how is the model represented
  !> @param[in]   model         : the array model, it contains the values
  !>                              for the local cell, and then for the 
  !>                              ghosts
  !> @param[in]   model_div     : the output divergence
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine model_domain_divergence_arr2d_real4(ctx_mesh,              &
                                           model_representation,model,  &
                                           model_divergence,ctx_err)

    implicit none

    type(t_domain)     ,intent(in)        :: ctx_mesh
    character(len=*)   ,intent(in)        :: model_representation
    real               ,intent(in)        :: model(:,:)
    real               ,intent(inout)     :: model_divergence(:)
    type(t_error)      ,intent(out)       :: ctx_err
    !! 
    integer             :: i_dim
    real,allocatable    :: model_deriv(:,:)

    ctx_err%ierr = 0
    model_divergence = 0.d0
    
    !! we can only have piecewise-constant for now.
    select case(trim(adjustl(model_representation)))
      case('piecewise-constant')
        !! loop over each direction
        allocate(model_deriv(ctx_mesh%n_cell_loc,ctx_mesh%dim_domain)) 
        do i_dim = 1 ,ctx_mesh%dim_domain
          model_deriv=0.d0
          call model_gradient_pconstant(ctx_mesh,model(:,i_dim),model_deriv,ctx_err)
          !! update the divergence 
          model_divergence(:) = model_divergence(:) + model_deriv(:,i_dim)
        end do  
        deallocate(model_deriv)
      
      case default
        ctx_err%ierr=-1
        ctx_err%msg ="** ERROR: model directional derivation only " //  &
                     "works with piecewise-constant for now "       //  &
                     "[mesh_model_deriv] **"
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    
    return
  end subroutine model_domain_divergence_arr2d_real4
 
  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the directional derivative of a 
  !>  piecewise constant model given per cell
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[in]   model_repr    : how is the model represented
  !> @param[in]   model         : the array model, it contains the values
  !>                              for the local cell, and then for the 
  !>                              ghosts
  !> @param[in]   model_deriv   : the output derivative, one per directions
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine model_gradient_pconstant(ctx_mesh,model,model_deriv,ctx_err)

    implicit none

    type(t_domain)     ,intent(in)        :: ctx_mesh
    real               ,intent(in)        :: model(:)
    real               ,intent(inout)     :: model_deriv(:,:)
    type(t_error)      ,intent(out)       :: ctx_err
    !! non-OMP variables
    integer                  :: dim_domain,n_node
    !! local
    integer                  :: k,i_face,count_ghost
    integer(kind=IKIND_MESH) :: neigh,i_cell
    real                     :: mloc
    real(kind=8)             :: diff_model,tol(3)
    integer     ,allocatable :: count_neigh(:)
    real(kind=8),allocatable :: cooloc(:),coobar(:),diff_dist(:)

    ctx_err%ierr = 0
    model_deriv  = 0.0
    dim_domain   = ctx_mesh%dim_domain
    n_node       = ctx_mesh%n_node_per_cell
    count_ghost  = 0
    allocate(cooloc     (dim_domain))
    allocate(coobar     (dim_domain))
    allocate(diff_dist  (dim_domain))
    allocate(count_neigh(dim_domain))


    !! ----------------------------------------------------------------- 
    !! the tolerance distance must depend on the size of the domain
    !! we apply a Finite-difference like formulation, so if it is 
    !! too small, it becomes unstable.
    tol = tiny(1.0)
    tol(1) = dble(ctx_mesh%bounds_xmax - ctx_mesh%bounds_xmin) / 1.e6 !! arbitrary 
    if(ctx_mesh%dim_domain == 2) then
      tol(2) = dble(ctx_mesh%bounds_zmax - ctx_mesh%bounds_zmin) / 1.e6 !! arbitrary 
    elseif(ctx_mesh%dim_domain == 3) then
      tol(2) = dble(ctx_mesh%bounds_ymax - ctx_mesh%bounds_ymin) / 1.e6 !! arbitrary 
      tol(3) = dble(ctx_mesh%bounds_zmax - ctx_mesh%bounds_zmin) / 1.e6 !! arbitrary 
    end if
    !! -----------------------------------------------------------------

    !! loop over all cells >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! We cannot rely on OMP because of the ghost nodes if they are put
    !! at the end ...
    !! -----------------------------------------------------------------
    do i_cell=1, ctx_mesh%n_cell_loc

      !! local model value and barycenter of the cell
      mloc      = model(i_cell)
      coobar(:) = dble(ctx_mesh%cell_bary(:,i_cell))

      !! ---------------------------------------------------------------
      !! loop over all the neighbors which are existing
      count_neigh = 0
      do i_face=1,ctx_mesh%n_neigh_per_cell
        neigh      = ctx_mesh%cell_neigh(i_face,i_cell)
        !! adjust for ghost
        if(neigh == tag_ghost_cell) then
          count_ghost = count_ghost + 1
          neigh       = ctx_mesh%n_cell_loc + count_ghost
        end if
        
        if(neigh > 0) then
          diff_model  = model(neigh) - mloc
          cooloc(:)   = dble(ctx_mesh%cell_bary(:,neigh))
          diff_dist(:)= cooloc(:) - coobar(:)
          !! if the distance is too small, not taken into account
          do k=1,dim_domain
            if(diff_dist(k) > tol(k) ) then
              count_neigh(k) = count_neigh(k) + 1
              model_deriv(i_cell,k)=model_deriv(i_cell,k) +             &
                                    real(diff_model/diff_dist(k))
            end if
          end do
        end if
      end do
      !! ---------------------------------------------------------------
      !! we average 
      do k=1,dim_domain
        if(count_neigh(k) > 0) then
          model_deriv(i_cell,k) = model_deriv(i_cell,k) / count_neigh(k)
          !! remains zero otherwise.
        end if
      end do
    end do
    !! end loop over cells >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    deallocate(coobar)
    deallocate(cooloc)
    deallocate(diff_dist)
    deallocate(count_neigh)

    return
  end subroutine model_gradient_pconstant

end module m_domain_derivative_model
