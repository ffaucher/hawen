!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_gather_neigh.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to gather all neighbors information to everyone
!> it can be useful to deal with specific model parametrization
!>
!
!------------------------------------------------------------------------------
module m_mesh_gather_neigh
  
  use m_raise_error,        only : raise_error, t_error
  use m_ctx_parallelism,    only : t_parallelism
  use m_allreduce_sum,      only : allreduce_sum
  use m_reduce_sum,         only : reduce_sum
  use m_ctx_domain,         only : t_domain
  use m_define_precision,   only : IKIND_MESH
  use m_tag_bc,             only : tag_ghost_cell

  implicit none

  private
  public  :: mesh_gather_all_neigh
  
  contains



  !--------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The routine gather all neighbors from all mpi processors
  !>  and gives a common output array.
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   coo_input     : coordinates of the position to look for
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   index_glob    : list of global indexes for the cell
  !> @param[in]   n_cell        : number of local cells to search
  !> @param[in]   n_cell        : number of global cell in mesh
  !> @param[in]   dim_domain    : domain dimension
  !> @param[in]   format_cell   : format of the cell (e.g. simplex)
  !> @param[in]   mpi_check     : check that we have only one value among
  !>                              the processors
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine mesh_gather_all_neigh(ctx_paral,ctx_mesh,cell_neigh_gb,    &
                                   flag_master_only,ctx_err)
    
    type(t_parallelism)                  ,intent(in)   :: ctx_paral
    type(t_domain)                       ,intent(in)   :: ctx_mesh
    integer(kind=IKIND_MESH),allocatable ,intent(inout):: cell_neigh_gb(:,:)
    logical                              ,intent(in)   :: flag_master_only
    type(t_error)                        ,intent(inout):: ctx_err
    !! local 
    integer(kind=4)                      :: k,n
    integer(kind=IKIND_MESH)             :: ic,ighost,ind_gb
    integer(kind=IKIND_MESH),allocatable :: cell_neigh_loc(:,:)

    !! -----------------------------------------------------------------
    allocate(cell_neigh_loc(ctx_mesh%n_neigh_per_cell,ctx_mesh%n_cell_gb))
    cell_neigh_loc = 0
    ighost         = ctx_mesh%n_cell_loc
    
    !! loop over all cell to fill a global array that we will reduce.
    !! we do not use OMP because of the ghost increment counter...
    do ic=1,ctx_mesh%n_cell_loc

      do k =1,ctx_mesh%n_neigh_per_cell     
        !! Ghost cells needs specific counter
        if(ctx_mesh%cell_neigh(k,ic) == tag_ghost_cell) then
          ighost = ighost+1
          ind_gb = ctx_mesh%index_loctoglob_cell(ighost)
        
        !! Domain boundary
        elseif(ctx_mesh%cell_neigh(k,ic) < 0) then
          ind_gb = ctx_mesh%cell_neigh(k,ic)
        else !! Normal
          ind_gb = ctx_mesh%index_loctoglob_cell(ctx_mesh%cell_neigh(k,ic))
        end if
        
        cell_neigh_loc(k,ctx_mesh%index_loctoglob_cell(ic)) = ind_gb
      end do
    end do
    
    !! master only or everyone
    n = int(ctx_mesh%n_cell_gb*ctx_mesh%n_neigh_per_cell)
    if(flag_master_only) then 
      call reduce_sum(cell_neigh_loc,cell_neigh_gb,                     &
                      n,0,ctx_paral%communicator,ctx_err)
    else
      call allreduce_sum(cell_neigh_loc,cell_neigh_gb,n,                &
                         ctx_paral%communicator,ctx_err)    
    end if 
    deallocate(cell_neigh_loc)
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    return
  end subroutine mesh_gather_all_neigh
 
end module m_mesh_gather_neigh
