!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_domain.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read parameters that are needed for meshes
!
!------------------------------------------------------------------------------
module m_read_parameters_domain
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  use m_ctx_parallelism, only: t_parallelism
  use m_barrier,         only: barrier
  use m_define_precision,only: RKIND_MESH
  use m_tag_bc,          only: n_boundary_tag
  use m_tag_media,       only: n_medium_tag
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public :: read_parameters_domain
  public :: read_parameters_domain_media
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read all parameters that are required for mesh
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   parameter_file : parameter file where infos are
  !> @param[out]  meshfile      : filename for the mesh
  !> @param[out]  meshformat    : format for the mesh
  !> @param[out]  meshpart      : mesh partitioner
  !> @param[out]  mesh_bctag    : boundary tag correspondance
  !> @param[out]  order         : global order of polynomial
  !> @param[out]  order_file    : in case of multiple order
  !> @param[out]  omax          : indicate maximal order for automatic order
  !> @param[out]  omin          : indicate minimal order for automatic order
  !> @param[out]  flag_quad     : indicate if integral evaluation with quadrature
  !> @param[out]  penalization  : indicate possibility for fixed dg penalization
  !> @param[out]  Zimpedance    : type of impedance for ABC
  !> @param[out]  PML{...}      : all PML informations
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_domain(ctx_paral,parameter_file,meshfile,  &
                                    meshformat,meshpart,mesh_bctag,     &
                                    order,polynomial_order_file,omax,   &
                                    omin,flag_quad,penalization,        &
                                    Zimpedance,                         &
                                    !! for PML
                                    flag_pml,pml_size_format,pml_size,  &
                                    pml_format,pml_coeff,pml_n,ctx_err)

    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    character(len=*)   ,intent(in)   :: parameter_file
    character(len=512) ,intent(out)  :: meshfile,meshformat,meshpart
    character(len=512) ,intent(out)  :: polynomial_order_file
    character(len=512) ,intent(out)  :: Zimpedance
    integer            ,intent(out)  :: order,omax,omin
    logical            ,intent(out)  :: flag_quad
    real               ,intent(out)  :: penalization
    integer,allocatable,intent(inout):: mesh_bctag(:,:)
    logical            ,intent(out)  :: flag_pml
    character(len=512) ,intent(out)  :: pml_format,pml_size_format
    real(kind=8)       ,intent(out)  :: pml_coeff,pml_size
    integer            ,intent(out)  :: pml_n
    type(t_error)      ,intent(out)  :: ctx_err
    !! local
    integer            :: bc1,bc2,n_bc_gb,i_tag
    integer            :: ntag,i,nval
    integer            :: nval_bc(n_boundary_tag)
    integer            :: itemp_bc(512,n_boundary_tag)
    integer            :: itemp1(512),nval1
    logical            :: ltemp(512)
    real               :: rtemp1(512)
    logical            :: flag_found
    !! local for reading the pml
    integer            :: ipml (512)
    integer            :: ipmlc(512)
    integer            :: npml,npmlc
    real               :: rtemp(512)


    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! Reading the mesh ------------------------------------------------
    !! -----------------------------------------------------------------
    nval_bc=0
    call read_parameter(parameter_file,'mesh_file'     ,'',meshfile  ,nval)
    call read_parameter(parameter_file,'mesh_format'   ,'',meshformat,nval)
    call read_parameter(parameter_file,'mesh_partition','',meshpart  ,nval)
    !! polynomial order for discretization 
    call read_parameter(parameter_file,'polynomial_order'  ,2,itemp1 ,nval)
    order=itemp1(1)

    !! possibility for fixed penalization term (not recommended)
    !! if (-1) it is automatic during matrix creation, otherwize
    !! it will take the given value.
    flag_found = .false. 
    call read_parameter(parameter_file,'hdg_stabilization',-1.,rtemp1 ,nval,&
                        o_flag_found=flag_found)
    if(.not. flag_found) then
      call read_parameter(parameter_file,'hdg_penalization',-1.,rtemp1 ,nval)
    end if
    penalization = rtemp1(1)
   

    !! how to evaluate integrals, by default with polynomial exact evaluation
    flag_quad = .false.
    call read_parameter(parameter_file,'evaluate_int_quadrature',.false.,ltemp,nval)
    flag_quad = ltemp(1)
    
    !! -----------------------------------------------------------------
    !! list of possible boundary conditions: 
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! (1) in case of Perfectly Matched Layers (PML) >>>>>>>>>>>>>>>>>>>
    flag_pml   = .false.
    pml_format = ''
    pml_coeff  = 0.
    pml_size_format = ''
    pml_size   = 0.
    pml_n      = 0

    !! the pml can be circular, or the direction must be specified, 
    !! i.e. it is normal to the boundary
    call read_parameter(parameter_file,'mesh_tag_pml',0,ipml,npml,      &
                        o_flag_found=flag_found)
    if(.not. flag_found) npml = 0
    call read_parameter(parameter_file,'mesh_tag_pml_circular',0,ipmlc, &
                        npmlc,o_flag_found=flag_found)
    if(.not. flag_found) npmlc = 0
    if(npmlc > 1) then 
      ctx_err%msg   = "** ERROR: There cannot be more than one "//      &
                      "circular PML [read_domain_parameters] **"
      ctx_err%ierr  = -1 
    end if
    
    !! can only be one of the two types, not both
    if(npml > 0 .and. npmlc > 0) then 
      ctx_err%msg   = "** ERROR: There can be only one type of PML: "// &
                      "circular or normal [read_domain_parameters] **"
      ctx_err%ierr  = -1   
    else
      if(npml  > 0) pml_n = npml
      if(npmlc > 0) pml_n = npmlc
    end if 

    !! size if any pml
    if(ctx_err%ierr == 0 .and. (npml > 0 .or. npmlc > 0)) then 
      flag_pml = .true.
      call read_parameter(parameter_file,'pml_size',0.,rtemp,nval1)
      pml_size = rtemp(1)
      if(pml_size < tiny(1.0)) then
        ctx_err%msg   = "** ERROR: PML size must be > 0 [read_domain] **"
        ctx_err%ierr  = -1
      end if

      !! additional informations
      !! if we have a pml, we also need some additional information 
      !! format is cosine or classic 
      call read_parameter(parameter_file,'pml_format','cosine',pml_format,nval1)
      !! size format can be per wavelength or fixed (in meters)
      call read_parameter(parameter_file,'pml_size_format','wavelength',&
                          pml_size_format,nval1)
      !! the coeff should be around 2% of the velocity ??
      call read_parameter(parameter_file,'pml_coeff' ,0.,rtemp,nval1)
      pml_coeff=rtemp(1)
      if(pml_coeff < tiny(1.0)) then
        ctx_err%msg   = "** ERROR: PML coefficient must be > 0 "     // &
                        "[read_parameters_domain] **"
        ctx_err%ierr  = -1
      end if
      
      !! DEFAULT values or error ---------------------------------------
      select case (trim(adjustl(pml_format)))
        case('cos','COS','Cos','cosine','COSINE','Cosine')
          pml_format = 'cosine'
        case('classic','default','CLASSIC','Classic','DEFAULT','Default')
          pml_format = 'default'
        case default
          ctx_err%msg   = "** ERROR: Unknown PML format, should be " // &
                          "'cosine' or 'classic' [read_parameters_domain] **"
          ctx_err%ierr  = -1
        end select
      select case (trim(adjustl(pml_size_format)))
        case('wavelength','WAVELENGTH','Wavelength')
          pml_size_format = 'wavelength'

        case('fixed-size','size','SIZE','Size','FIXED','fixed','Fixed-size')
          pml_size_format = 'fixed'

        case default
          ctx_err%msg   = "** ERROR: Unknown PML size format, can be "//&
                          "'wavelength' or 'fixed' [read_parameters_domain] **"
          ctx_err%ierr=-1
      end select
    end if
    !! return ERROR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(ctx_err%ierr .ne. 0) then
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    end if
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! (2) ABC, FREE-SURFACE, WALL-SURFACE, PLANEWAVE, DIRICHLET, NEUMANN
    !! note that depending on the propagator, wall-surface and 
    !! free-surface usually match Neumann or Dirichlet.
    call read_parameter(parameter_file,'mesh_tag_absorbing'   ,0,       &
                        itemp_bc(:,1),nval_bc(1),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(1)=0
    !! if there is an ABC, we need to save the impedance type 
    Zimpedance = 'Sommerfeld_0'
    call read_parameter(parameter_file,'bc_impedance','Sommerfeld_0', &
                        Zimpedance,nval)

    call read_parameter(parameter_file,'mesh_tag_free-surface',0,       &
                        itemp_bc(:,2),nval_bc(2),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(2)=0
    call read_parameter(parameter_file,'mesh_tag_wall-surface',0,       &
                        itemp_bc(:,3),nval_bc(3),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(3)=0
    call read_parameter(parameter_file,'mesh_tag_dirichlet'   ,0,       &
                        itemp_bc(:,4),nval_bc(4),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(4)=0
    call read_parameter(parameter_file,'mesh_tag_neumann'     ,0,       &
                        itemp_bc(:,5),nval_bc(5),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(5)=0
    call read_parameter(parameter_file,'mesh_tag_planewave'   ,0,       &
                        itemp_bc(:,6),nval_bc(6),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(6)=0
    call read_parameter(parameter_file,'mesh_tag_aldirichlet_dlrobin',0,&
                        itemp_bc(:,7),nval_bc(7),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(7)=0
    call read_parameter(parameter_file,'mesh_tag_center_neumann',0,     &
                        itemp_bc(:,8),nval_bc(8),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(8)=0
    call read_parameter(parameter_file,'mesh_tag_deltapdirichlet_dldirichlet',0, &
                        itemp_bc(:,11),nval_bc(11),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(11)=0
    call read_parameter(parameter_file,'mesh_tag_analytic_orthorhombic',0,       &
                        itemp_bc(:,12),nval_bc(12),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(12)=0
    call read_parameter(parameter_file,'mesh_tag_deltapdirichlet_dlrobin',0,     &
                        itemp_bc(:,13),nval_bc(13),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(13)=0
    call read_parameter(parameter_file,'mesh_tag_alrbc_dlrobin',0,     &
                        itemp_bc(:,14),nval_bc(14),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(14)=0
    call read_parameter(parameter_file,'mesh_tag_alrbc_dldirichlet',0,     &
                        itemp_bc(:,15),nval_bc(15),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(15)=0
    call read_parameter(parameter_file,'mesh_tag_deltapdirichlet',0,    &
                        itemp_bc(:,16),nval_bc(16),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(16)=0
    call read_parameter(parameter_file,'mesh_tag_planewave_isoP',0,     &
                        itemp_bc(:,17),nval_bc(17),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(17)=0
    call read_parameter(parameter_file,'mesh_tag_planewave_isoS',0,     &
                        itemp_bc(:,18),nval_bc(18),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(18)=0
    call read_parameter(parameter_file,'mesh_tag_analytic_elasticiso',0,&
                        itemp_bc(:,19),nval_bc(19),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(19)=0
    call read_parameter(parameter_file,'mesh_tag_planewave_vtiYSH',0,   &
                        itemp_bc(:,20),nval_bc(20),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(20)=0
    call read_parameter(parameter_file,'mesh_tag_planewave_vtiYSV',0,   &
                        itemp_bc(:,21),nval_bc(21),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(21)=0
    call read_parameter(parameter_file,'mesh_tag_planewave_vtiYqP',0,   &
                        itemp_bc(:,22),nval_bc(22),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(22)=0
    call read_parameter(parameter_file,'mesh_tag_planewave_scattered',0,&
                        itemp_bc(:,23),nval_bc(23),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(23)=0
    call read_parameter(parameter_file,'mesh_tag_lagrangep',0,          &
                        itemp_bc(:,24),nval_bc(24),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(24)=0
    call read_parameter(parameter_file,'mesh_tag_lagrangep_dlrobin',0,  &
                        itemp_bc(:,25),nval_bc(25),o_flag_found=flag_found)
    if(.not. flag_found) nval_bc(25)=0
    !! -----------------------------------------------------------------

    !! sum-up PML and others BC tags: max number of tag
    ntag=maxval(nval_bc)
    ntag=max(ntag,pml_n)
    if(ntag <= 0) then
      ctx_err%msg   = "** ERROR: need to indicate boundary conditions " //&
                      "[read_parameters_domain] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    endif
    

    n_bc_gb = n_boundary_tag
    allocate(mesh_bctag(n_bc_gb,ntag))
    mesh_bctag=0
    do i_tag=1,n_bc_gb
      if(nval_bc(i_tag) > 0 .and. i_tag .ne. 9 .and. i_tag .ne. 10) then
        mesh_bctag(i_tag,:               )=itemp_bc(1               ,i_tag) 
        mesh_bctag(i_tag,1:nval_bc(i_tag))=itemp_bc(1:nval_bc(i_tag),i_tag)
      endif
    end do
    if(npml > 0) then !! PML -------------------------------------------
      mesh_bctag(9,:)      = ipml(1)
      mesh_bctag(9,1:npml) = ipml(1:npml)
    end if
    if(npmlc > 0) then !! PML-Radius -----------------------------------
      mesh_bctag(10,:)      = ipmlc(1) 
      mesh_bctag(10,1:npmlc)= ipmlc(1:npmlc)
    end if
    

    !! -----------------------------------------------------------------
    !! formal checks.
    !! -----------------------------------------------------------------
    if(trim(adjustl(meshfile)) .eq. '') then
      ctx_err%msg   = "** ERROR: you need to enter a mesh file "// &
                      "[read_parameters_domain] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    if(trim(adjustl(meshformat)) .eq. '') then
      ctx_err%msg   = "** ERROR: you need to specify a mesh format "// &
                      "[read_parameters_domain] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    if(trim(adjustl(meshpart)) .eq. '') then
      ctx_err%msg   = "** ERROR: you need to specify a mesh partitioner "// &
                      "[read_parameters_domain] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    endif
    if(order==0) then
      ctx_err%msg   = "** ERROR: cannot use order 0 with HDG because" // &
                      " there would be no dof left on the element"    // &
                      " faces [read_parameters_domain] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)      
    else if(order == -2) then
      !! automatic order from wavelength using the maximal frequency
      call read_parameter(parameter_file,'polynomial_order_max',8,itemp1,nval)
      omax = itemp1(1)
      call read_parameter(parameter_file,'polynomial_order_min',1,itemp1,nval)
      omin = itemp1(1)
      if(omin < 1) omin=1
    else if(order < 0 .and. order .ne. -2) then
      !! read from file 
      call read_parameter(parameter_file,'polynomial_order_file','',    &
                          polynomial_order_file,nval)
      if(trim(adjustl(polynomial_order_file)) .eq. '') then
        ctx_err%msg   = "** ERROR: you need to specify a file for the " // &
                        "order of the cell when order <= 0 "            // &
                        "[read_parameters_domain] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)      
      end if
    endif
    
    !! check all tags are > 0
    if( (minval(mesh_bctag( 1,:))<=0 .and. nval_bc(1) > 0) .or. &
        (minval(mesh_bctag( 2,:))<=0 .and. nval_bc(2) > 0) .or. &
        (minval(mesh_bctag( 3,:))<=0 .and. nval_bc(3) > 0) .or. &
        (minval(mesh_bctag( 4,:))<=0 .and. nval_bc(4) > 0) .or. &
        (minval(mesh_bctag( 5,:))<=0 .and. nval_bc(5) > 0) .or. &
        (minval(mesh_bctag( 6,:))<=0 .and. nval_bc(6) > 0) .or. &
        (minval(mesh_bctag( 7,:))<=0 .and. nval_bc(7) > 0) .or. &
        (minval(mesh_bctag( 8,:))<=0 .and. nval_bc(8) > 0) .or. &
        (minval(mesh_bctag(11,:))<=0 .and. nval_bc(11)> 0) .or. &
        (minval(mesh_bctag(12,:))<=0 .and. nval_bc(12)> 0) .or. &
        (minval(mesh_bctag(13,:))<=0 .and. nval_bc(13)> 0) .or. &
        (minval(mesh_bctag(14,:))<=0 .and. nval_bc(14)> 0) .or. &
        (minval(mesh_bctag(15,:))<=0 .and. nval_bc(15)> 0) .or. &
        (minval(mesh_bctag(16,:))<=0 .and. nval_bc(16)> 0) .or. &
        (minval(mesh_bctag(17,:))<=0 .and. nval_bc(17)> 0) .or. &
        (minval(mesh_bctag(18,:))<=0 .and. nval_bc(18)> 0) .or. &
        (minval(mesh_bctag(19,:))<=0 .and. nval_bc(19)> 0) .or. &
        (minval(mesh_bctag(20,:))<=0 .and. nval_bc(20)> 0) .or. &
        (minval(mesh_bctag(21,:))<=0 .and. nval_bc(21)> 0) .or. &
        (minval(mesh_bctag(22,:))<=0 .and. nval_bc(22)> 0) .or. &
        (minval(mesh_bctag(23,:))<=0 .and. nval_bc(23)> 0) .or. &
        (minval(mesh_bctag(24,:))<=0 .and. nval_bc(24)> 0) .or. &
        (minval(mesh_bctag(25,:))<=0 .and. nval_bc(25)> 0) .or. &
        (minval(mesh_bctag( 9,:))<=0 .and. npml       > 0) .or. &
        (minval(mesh_bctag(10,:))<=0 .and. npmlc      > 0) ) then
      ctx_err%msg   = "** ERROR: all mesh boundary tags must be > 0 " //&
                      "[read_parameters_domain] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    endif
    !! check all values are different
    do i=1,ntag
      do bc1=1,n_bc_gb-1
        do bc2=bc1+1,n_bc_gb
          if( mesh_bctag(bc1,i) .eq. mesh_bctag(bc2,i) .and.            &
              mesh_bctag(bc2,i) .ne. 0 ) then 
            ctx_err%msg   = "** ERROR: mesh boundary tags must be " //  &
                            "different [read_parameters_domain] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            call raise_error(ctx_err) 
          end if
        end do
      end do
    end do

    !! -----------------------------------------------------------------
    !! mpi_barrier waits for every processors
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------
    
    return
  end subroutine read_parameters_domain

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read all parameters that are required for mesh
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   parameter_file: parameter file where infos are
  !> @param[in]   dim_domain    : domain dimension
  !> @param[out]  mediumfile    : file regarding the medium information
  !> @param[out]  n_medium      : the number of different media
  !> @param[out]  n_zone        : the number of different zones in the domain
  !> @param[out]  ntag_max      : max. number of tags for one type of medium
  !> @param[out]  xinterface1D  : in 1D, we just separate the interval.
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_domain_media(ctx_paral,parameter_file,       &
                                          dim_domain,mediumfile,n_medium, &
                                          n_zone,medium_tag,              &
                                          ntag_max_for_one_medium,        &
                                          xinterface1D,ctx_err)

    implicit none

    type(t_parallelism)                ,intent(in)    :: ctx_paral
    character(len=*)                   ,intent(in)    :: parameter_file
    integer                            ,intent(in)    :: dim_domain
    character(len=512)                 ,intent(out)   :: mediumfile
    integer                            ,intent(out)   :: n_medium
    integer                            ,intent(out)   :: n_zone
    integer              , allocatable ,intent(inout) :: medium_tag(:,:)
    integer                            ,intent(out)   :: ntag_max_for_one_medium
    real(kind=RKIND_MESH), allocatable ,intent(inout) :: xinterface1D(:)
    type(t_error)                      ,intent(out)   :: ctx_err
    !! local
    integer            :: k,c1,c2,l1,l2
    integer            :: nval,nval_medium(n_medium_tag)
    integer            :: itemp_medium(512,n_medium_tag)
    real(kind=8)       :: rtemp1(512)
    logical            :: flag_found


    ctx_err%ierr  = 0
    
    !! initialization
    mediumfile = ''
    n_medium   = 0

    !! -----------------------------------------------------------------
    !! Reading the information for multiple media
    !! depending on the dimension
    !! -----------------------------------------------------------------
    select case (dim_domain)

      !! dimension 1 ___________________________________________________
      case(1)
        call read_parameter(parameter_file,'medium_interface1D',0.d0,   &
                            rtemp1,nval,o_flag_found=flag_found)
        if(flag_found) then
          allocate(xinterface1D(nval))
          xinterface1D(:) = real(rtemp1(1:nval),kind=RKIND_MESH)
          n_zone          = nval + 1
          do k=1,nval-1
            if( (xinterface1D(k+1)-xinterface1D(k)) <= 0.d0 ) then
              ctx_err%msg   = "** ERROR: the 1D interval given in the"//&
                              " parameter file for muliple media must"//&
                              " be a stictly increasing list. "       //&
                              "[read_parameters_domain_multi] **"
              ctx_err%ierr  = -1
              ctx_err%critic=.true.
              call raise_error(ctx_err) 
            end if
          end do
        else 
          allocate(xinterface1D(1))
          xinterface1D(1) = 0.d0
          n_zone = 1
        end if

      !! other dimensions ______________________________________________
      case default
      
        !! -------------------------------------------------------------
        !! In 2d/3d, we have to give a tag per cell
        !! by reading another file ?
        !! -------------------------------------------------------------
        call read_parameter(parameter_file,'medium_cell_tag_file','',mediumfile,nval)
        ctx_err%msg   = "** ERROR: the consideration of multiple " //   &
                        "media is not treated in dimension higher "//   &
                        "than one. [read_parameters_domain_multi] **"
        ctx_err%ierr  = -70
        ctx_err%critic=.true.
        call raise_error(ctx_err) 
    end select
    !! -----------------------------------------------------------------
    
    
    !! -----------------------------------------------------------------
    !! read the tags indicated
    itemp_medium = 0
    !! For stellar problem without Cowling approximation ---------------
    call read_parameter(parameter_file,'medium_tag_stellar_interior',0, &
                        itemp_medium(:,1),nval_medium(1),o_flag_found=flag_found)
    if(.not. flag_found)   nval_medium(1)=0
    call read_parameter(parameter_file,'medium_tag_stellar_atmo'    ,0, &
                        itemp_medium(:,2),nval_medium(2),o_flag_found=flag_found)
    if(.not. flag_found)   nval_medium(2)=0
    
    !! for acoustic-elastic problems -----------------------------------
    call read_parameter(parameter_file,'medium_tag_acoustic',0,         &
                        itemp_medium(:,3),nval_medium(3),o_flag_found=flag_found)
    if(.not. flag_found)   nval_medium(3)=0
    call read_parameter(parameter_file,'medium_tag_elastic',0,          &
                        itemp_medium(:,4),nval_medium(4),o_flag_found=flag_found)
    if(.not. flag_found)   nval_medium(4)=0

    !! -----------------------------------------------------------------
    !! count the number of media and save tags
    !! -----------------------------------------------------------------   
    ntag_max_for_one_medium = maxval(nval_medium)
    if(ntag_max_for_one_medium <= 0) then
      ctx_err%msg   = "** ERROR: need to indicate at least one type " //&
                      "for equations that contain a coupling "        //&
                      "[read_parameters_domain_media] **"
      ctx_err%ierr  = -62
      ctx_err%critic=.true.
      call raise_error(ctx_err) 
    endif

    
    allocate(medium_tag(n_medium_tag,ntag_max_for_one_medium))
    medium_tag(:,:) = 0
    n_medium        = 0
    do k=1,n_medium_tag
      if(nval_medium(k) > 0) then
        n_medium                       = n_medium + 1 
        medium_tag(k,:               ) = itemp_medium(1,k) !! init
        medium_tag(k,1:nval_medium(k)) = itemp_medium(1:nval_medium(k),k)
        !! also check that tag > 0
        if(minval(medium_tag(k,1:nval_medium(k))) <= 0) then
          ctx_err%msg   = "** ERROR: the tags to indicate the type " // &
                          " medium must be > 0 "                     // &
                          "[read_parameters_domain_media] **"
          ctx_err%ierr  = -63
          ctx_err%critic=.true.
          call raise_error(ctx_err)
        end if
      end if
    end do
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! formal checks.
    !! -----------------------------------------------------------------
    !! check all values are different
    do l1=1,n_medium_tag ; do l2=l1+1,n_medium_tag
    do c1=1,ntag_max_for_one_medium ; do c2=1,ntag_max_for_one_medium
      if(l1.ne.l2 .or. c1.ne.c2) then
        if( medium_tag(l1,c1) .eq. medium_tag(l2,c2) .and.            &
            medium_tag(l1,c1) .ne. 0) then 
          ctx_err%msg   = "** ERROR: medium tags must be " //  &
                          "all different [read_parameters_domain_media] **"
          ctx_err%ierr  = -64
          ctx_err%critic=.true.
          call raise_error(ctx_err) 
        end if
      end if
      end do ; end do
    end do ; end do
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! mpi_barrier waits for every processors
    call barrier(ctx_paral%communicator,ctx_err)
    !! -----------------------------------------------------------------

    return
  end subroutine read_parameters_domain_media

end module m_read_parameters_domain
