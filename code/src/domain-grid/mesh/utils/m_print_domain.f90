!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_print_mesh.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for meshes
!
!------------------------------------------------------------------------------
module m_print_domain

  use m_raise_error,      only : t_error
  use m_ctx_parallelism,  only : t_parallelism
  use m_tag_media
  use m_barrier,          only : barrier
  use m_allreduce_max,    only : allreduce_max
  use m_print_basics,     only : separator,indicator,cast_memory,cast_time
  implicit none
  
  private
  public :: print_info_mesh_welcome,print_info_mesh_end
  public :: print_info_mesh_init,print_info_mesh_bbox
  public :: print_info_media
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print welcome info for the mesh
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   meshfile      : mesh filename
  !> @param[in]   meshformat    : mesh format
  !> @param[in]   meshpart      : mesh partitioner
  !> @param[in]   dims          : dimension
  !> @param[in]   order         : order, <0 for variational
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_mesh_welcome(ctx_paral,meshfile,meshformat,meshpart, &
                                     dims,order,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    character(len=*)   ,intent(in)   :: meshfile,meshformat,meshpart
    integer            ,intent(in)   :: dims,order
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      write(6,'(a)')    separator
      write(6,'(2a)')   indicator, " Mesh information "
      if(dims.eq.2) write(6,'(a)') "- two-dimensional domain"
      if(dims.eq.3) write(6,'(a)') "- three-dimensional domain"
      write(6,'(2a)') "- mesh file basename         : ",trim(adjustl(meshfile))
      write(6,'(2a)') "- mesh file format           : ",trim(adjustl(meshformat))
      write(6,'(2a)') "- mesh partitioner           : ",trim(adjustl(meshpart))
      if(order>0) then
        write(6,'(a,i7)') "- polynomial constant order  : ",order
      else
        write(6,'(a)') "- polynomial order varies with cell"
      endif
    endif
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_mesh_welcome

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> time info for the mesh
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   time_init     : time spent dealing with initialize
  !> @param[in]   time_read     : time spent dealing with reading
  !> @param[in]   time_part     : time spent dealing with partitioner
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_mesh_end(ctx_paral,time_init,time_read,time_part,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    real(kind=8)       ,intent(in)   :: time_init
    real(kind=8)       ,intent(in)   :: time_read
    real(kind=8)       ,intent(in)   :: time_part
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    character(len=64) :: str_time
    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      call cast_time(time_init,str_time)
      write(6,'(2a)') "- mesh time to init infos    : ",trim(adjustl(str_time))
      call cast_time(time_read,str_time)
      write(6,'(2a)') "- mesh time for monoproc read: ",trim(adjustl(str_time))
      call cast_time(time_part,str_time)
      write(6,'(2a)') "- mesh time for partitioning : ",trim(adjustl(str_time))
      write(6,'(a)')    separator
    endif
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_mesh_end

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> mesh bounding box
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   [xyz]min      : bounding box dimension
  !> @param[in]   [xyz]max      : bounding box dimension
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_mesh_bbox(ctx_paral,xmin,xmax,ymin,ymax,zmin,zmax,ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    real(kind=8)       ,intent(in)   :: xmin,xmax,ymin,ymax,zmin,zmax
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      write(6,'(a,es12.5,a,es12.5)') "- mesh bounding box X        : ", xmin, " x ", xmax
      write(6,'(a,es12.5,a,es12.5)') "- mesh bounding box Y        : ", ymin, " x ", ymax
      write(6,'(a,es12.5,a,es12.5)') "- mesh bounding box Z        : ", zmin, " x ", zmax
    endif

    return
  end subroutine print_info_mesh_bbox

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print initial info for the mesh, number of cells, nodes, global memory
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   nnode         : global number of nodes
  !> @param[in]   ncell         : global number of cells
  !> @param[in]   nedge         : global number of edges
  !> @param[in]   mem_read      : estimate memory for reading the mesh
  !> @param[in]   mem_part      : estimate memory for partitioning the mesh
  !> @param[in]   mem_other     : estimate memory non-master processors
  !> @param[in]   type_element  : type of element
  !> @param[in]   Zimpedance    : type of impedance for ABC
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_mesh_init(ctx_paral,nnode,ncell,nedge,mem_read, &
                                  mem_part,mem_other,type_element,      &
                                  Zimpedance,ctx_err)
    !! mesh size precision
    use m_define_precision,     only : IKIND_MESH
    
    type(t_parallelism)       ,intent(in)   :: ctx_paral
    integer(kind=IKIND_MESH)  ,intent(in)   :: nnode,ncell,nedge
    integer(kind=8)           ,intent(in)   :: mem_read,mem_part,mem_other
    character(len=*)          ,intent(in)   :: type_element,Zimpedance
    type(t_error)             ,intent(out)  :: ctx_err
    !!
    character(len=7)  :: frmt
    character(len=64) :: str_mem
    
    frmt='(a,i10)'
    if(IKIND_MESH==8) then
      if(ncell > 1.D10) frmt='(a,i20)'
    endif
    ctx_err%ierr  = 0
    if(ctx_paral%master) then
      select case(trim(adjustl(type_element)))
        case ('triangle')
          write(6,'(a)') "- Mesh based upon 2D TRIANGLE cells "
        case ('tetrahedron')
          write(6,'(a)') "- Mesh based upon 3D TETRAHEDRON cells "
        case default
          write(6,'(a)') "- Mesh based upon "     // &
                         trim(adjustl(type_element)) // " cells"
      end select
    
      write(6,frmt)    "- total number of mesh cells : ",ncell
      write(6,frmt)    "- total number of mesh nodes : ",nnode
      write(6,frmt)    "- total number of mesh faces : ",nedge
      !! ABC information 
      if(trim(adjustl(Zimpedance)) .ne. '') then
        write(6,'(a)') "- Absorbing condition type   : " //             &
                       trim(adjustl(Zimpedance))
      end if


      call cast_memory(mem_read,str_mem)
      write(6,'(2a)')  "- memory peak master read    : ",trim(adjustl(str_mem))
      if(ctx_paral%nproc.ne.1) then
        call cast_memory(mem_part,str_mem)
        write(6,'(2a)')  "- memory peak master partit. : ",trim(adjustl(str_mem))
        call cast_memory(mem_other,str_mem)
        write(6,'(2a)')  "- memory peak other estim.   : ",trim(adjustl(str_mem))
      endif
    endif
    call barrier(ctx_paral%communicator,ctx_err)
    return
  end subroutine print_info_mesh_init



  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print information regarding the case of multiple media.
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   dim_domain    : domain dimension
  !> @param[in]   n_cell_medium : number of cells per medium type
  !> @param[in]   n_media       : number of different media
  !> @param[in]   n_zone        : number of different zones
  !> @param[in]   mem_loc       : memory per processor
  !> @param[in]   xinterface1D  : list of interface position for 1D only
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine print_info_media(ctx_paral,dim_domain,n_cell_medium,       &
                              n_media,n_zone,mem_loc,xinterface1D,      &
                              ctx_err)
    type(t_parallelism),intent(in)   :: ctx_paral
    integer(kind=4)    ,intent(in)   :: dim_domain
    integer(kind=4)    ,intent(in)   :: n_cell_medium(:)
    integer(kind=4)    ,intent(in)   :: n_media
    integer(kind=4)    ,intent(in)   :: n_zone
    integer(kind=8)    ,intent(in)   :: mem_loc
    real   (kind=8)    ,intent(in)   :: xinterface1D(:)
    type(t_error)      ,intent(out)  :: ctx_err
    !! 
    character(len=64)                :: str_medium
    character(len=64)                :: str_mem
    integer                          :: k
    integer(kind=8)                  :: max_mem

    ctx_err%ierr  = 0
    
    call allreduce_max(mem_loc,max_mem,1,ctx_paral%communicator,ctx_err)
    call cast_memory(max_mem,str_mem)
    
    if(ctx_paral%master) then
      
      write(6,'(a,i5,a,i3,a)') "- There are ",n_zone," areas for ",     &
                                n_media," different media."
      ! print the positions if 1D
      if(dim_domain == 1) then
        do k=1,n_zone-1
          write(6,'(a,i3,a,es11.5)') "- Interface n°",k," in x=",xinterface1D(k)
        end do
      end if
      !! ---------------------------------------------------------------      
      do k=1,n_medium_tag
        if(n_cell_medium(k) .ne. 0) then
          select case(k) 
            case(1)
              str_medium='Acoustic cells: '
            case(2)
              str_medium='Elastic  cells: '
            case(3)
              str_medium='Stellar interior   cells: '
            case(4)
              str_medium='Stellar atmosphere cells: '
            case default
              str_medium='Unknown medium cells: '
          end select
          write(6,'(2a,i10)') "     - ",trim(str_medium),n_cell_medium(k)
        end if
      end do
      
      write(6,'(2a)') "- Max. mem. for media on 1 core: ",trim(adjustl(str_mem))

    endif

    return
  end subroutine print_info_media

end module m_print_domain
