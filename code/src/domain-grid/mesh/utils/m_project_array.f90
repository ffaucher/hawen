!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_project_array.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to project global arrays to local mesh cell subdomains
!
!------------------------------------------------------------------------------
module m_project_array

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_define_precision,         only: IKIND_MESH
  !> domain  
  use m_ctx_domain,               only: t_domain
  !! -------------------------------------------------------------------
  implicit none

  interface project_array_local_cell_to_global
     module procedure project_local_cell_array_to_global_real4
     module procedure project_local_cell_array_to_global_real8
  end interface
  
  interface project_array_global_cell_to_local
     module procedure project_global_cell_array_to_local_real4
     module procedure project_global_cell_array_to_local_real8
  end interface

  interface project_array_global_subcell_to_local
     module procedure project_global_subcell_array_to_local_real4
     module procedure project_global_subcell_array_to_local_real8
  end interface
  
  private
  !! this is to project global sep.
  public :: project_sep_global_to_cell_local
  public :: project_sep_global_spherical_to_cell_local
  public :: project_sep_global_spherical3d_to_cell_local
  !! this is to project global sep extending 0 if out of the medium.
  public :: project_sep_global_to_cell_local_extend0
  public :: project_sep_global_spherical_to_cell_local_extend0
  public :: project_sep_global_spherical3d_to_cell_local_extend0
  !! this is to project ascii array per cell.
  public :: project_ascii_global_to_cell_local
  !! this is to project cell arrays.
  public :: project_array_local_cell_to_global
  public :: project_array_global_cell_to_local
  !! this is to project sub-cells arrays
  public :: project_array_global_subcell_to_local

  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a SEP global array to local subdomain represented on cells
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    sep_{XYZ}min   : sep model physical min
  !> @param[in]    sep_n{XYZ}     : sep model size
  !> @param[in]    sep_d{XYZ}     : sep model discretization step
  !> @param[in]    sep_model      : sep model
  !> @param[out]   model          : output subdomain model according to 
  !>                                the input cartesian domain ctx_domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_sep_global_to_cell_local(ctx_mesh,sep_xmin,sep_ymin, &
                                      sep_zmin,sep_nx,sep_ny,sep_nz,      &
                                      sep_dx,sep_dy,sep_dz,sep_model,     &
                                      model,ctx_err)
    implicit none
    
    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: sep_xmin,sep_ymin,sep_zmin
    integer                  ,intent(in)   :: sep_nx,sep_ny,sep_nz
    real(kind=8)             ,intent(in)   :: sep_dx,sep_dy,sep_dz
    real(kind=8),allocatable ,intent(in)   :: sep_model(:,:,:)
    real(kind=8),allocatable ,intent(inout):: model    (:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    integer                    :: inode_x,inode_y,inode_z
    real(kind=8)               :: coo2d(2),coo3d(3),coo1d
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    model         = 0.0

    !! -----------------------------------------------------------------
    !! convert local cell barycenter to the closest SEP node 
    !! -----------------------------------------------------------------
    select case(ctx_mesh%dim_domain)
      !! ---------------------------------------------------------------
      !! 1D domain
      !! ---------------------------------------------------------------
      case(1)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! cell barycenter
          coo1d    = dble(ctx_mesh%cell_bary(1,i_cell))

          !! find the closest node 
          inode_x = nint((coo1d - sep_xmin)/sep_dx) + 1
          !! consistency check:
          if(inode_x < 1 .or. inode_x > sep_nx) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_array] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif
          !! save value
          model(i_cell) = sep_model(inode_x,1,1)
        end do

      !! ---------------------------------------------------------------
      !! 2D domain
      !! ---------------------------------------------------------------
      case(2)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! cell barycenter
          coo2d(:) = dble(ctx_mesh%cell_bary(:,i_cell))

          !! find the closest node 
          inode_x = nint((coo2d(1) - sep_xmin)/sep_dx) + 1
          inode_z = nint((coo2d(2) - sep_zmin)/sep_dz) + 1
          !! consistency check:
          if(inode_x < 1 .or. inode_x > sep_nx .or. inode_z < 1 .or.    &
             inode_z > sep_nz) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_array] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif
          !! save value
          model(i_cell) = sep_model(inode_x,1,inode_z)
        end do

      !! ---------------------------------------------------------------
      !! 3D domain
      !! ---------------------------------------------------------------
      case(3)
        do i_cell=1,ctx_mesh%n_cell_loc
          coo3d(:) = dble(ctx_mesh%cell_bary(:,i_cell))

          !! find the closest node 
          inode_x = nint((coo3d(1) - sep_xmin)/sep_dx) + 1
          inode_y = nint((coo3d(2) - sep_ymin)/sep_dy) + 1
          inode_z = nint((coo3d(3) - sep_zmin)/sep_dz) + 1

          !! consistency check:
          if(inode_x < 1 .or. inode_x > sep_nx .or. inode_z < 1 .or.    &
             inode_z > sep_nz .or. inode_y < 1 .or. inode_y > sep_ny) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_array] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif

          !! save value
          model(i_cell) = sep_model(inode_x,inode_y,inode_z)
        end do

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_sep_array] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_sep_global_to_cell_local


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a SEP global array to local subdomain represented on cells.
  !> Instead of returning an error if the position is not found, we 
  !> return a 0, i.e., we artificially extend the medium.
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    sep_{XYZ}min   : sep model physical min
  !> @param[in]    sep_n{XYZ}     : sep model size
  !> @param[in]    sep_d{XYZ}     : sep model discretization step
  !> @param[in]    sep_model      : sep model
  !> @param[out]   model          : output subdomain model according to 
  !>                                the input cartesian domain ctx_domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_sep_global_to_cell_local_extend0(                  &
                                        ctx_mesh,sep_xmin,sep_ymin,     &
                                        sep_zmin,sep_nx,sep_ny,sep_nz,  &
                                        sep_dx,sep_dy,sep_dz,sep_model, &
                                        model,ctx_err)
    implicit none
    
    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: sep_xmin,sep_ymin,sep_zmin
    integer                  ,intent(in)   :: sep_nx,sep_ny,sep_nz
    real(kind=8)             ,intent(in)   :: sep_dx,sep_dy,sep_dz
    real(kind=8),allocatable ,intent(in)   :: sep_model(:,:,:)
    real(kind=8),allocatable ,intent(inout):: model    (:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    integer                    :: inode_x,inode_y,inode_z
    real(kind=8)               :: coo2d(2),coo3d(3),coo1d
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    model         = 0.0

    !! -----------------------------------------------------------------
    !! convert local cell barycenter to the closest SEP node 
    !! -----------------------------------------------------------------
    select case(ctx_mesh%dim_domain)
      !! ---------------------------------------------------------------
      !! 1D domain
      !! ---------------------------------------------------------------
      case(1)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! cell barycenter
          coo1d    = dble(ctx_mesh%cell_bary(1,i_cell))
          !! find the closest node 
          inode_x = nint((coo1d - sep_xmin)/sep_dx) + 1
          !! consistency check 
          if(inode_x < 1 .or. inode_x > sep_nx) then
            !! return with the initial 0
          else
            model(i_cell) = sep_model(inode_x,1,1)
          endif
        end do

      !! ---------------------------------------------------------------
      !! 2D domain
      !! ---------------------------------------------------------------
      case(2)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! cell barycenter
          coo2d(:) = dble(ctx_mesh%cell_bary(:,i_cell))

          !! find the closest node 
          inode_x = nint((coo2d(1) - sep_xmin)/sep_dx) + 1
          inode_z = nint((coo2d(2) - sep_zmin)/sep_dz) + 1
          !! consistency check:
          if(inode_x < 1 .or. inode_x > sep_nx .or. inode_z < 1 .or.    &
             inode_z > sep_nz) then
            !! return with the initial 0
          else
            !! save value
            model(i_cell) = sep_model(inode_x,1,inode_z)
          endif
        end do

      !! ---------------------------------------------------------------
      !! 3D domain
      !! ---------------------------------------------------------------
      case(3)
        do i_cell=1,ctx_mesh%n_cell_loc
          coo3d(:) = dble(ctx_mesh%cell_bary(:,i_cell))

          !! find the closest node 
          inode_x = nint((coo3d(1) - sep_xmin)/sep_dx) + 1
          inode_y = nint((coo3d(2) - sep_ymin)/sep_dy) + 1
          inode_z = nint((coo3d(3) - sep_zmin)/sep_dz) + 1

          !! consistency check:
          if(inode_x < 1 .or. inode_x > sep_nx .or. inode_z < 1 .or.    &
             inode_z > sep_nz .or. inode_y < 1 .or. inode_y > sep_ny) then
            !! return with the initial 0
          else
            !! save value
            model(i_cell) = sep_model(inode_x,inode_y,inode_z)
          endif
        end do

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_sep_array] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_sep_global_to_cell_local_extend0

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a 1D SEP to local subdomain using a spherical symmetry
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    center_point   : coordinates of the center
  !> @param[in]    sep_n{XYZ}     : sep model size
  !> @param[in]    sep_d{XYZ}     : sep model discretization step
  !> @param[in]    sep_model      : sep model
  !> @param[out]   model          : output subdomain model according to 
  !>                                the input cartesian domain ctx_domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_sep_global_spherical_to_cell_local(ctx_mesh,       &
                                             center_point,sep_nx,sep_dx,&
                                             sep_model,model,ctx_err)
    implicit none

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: center_point(3)
    integer                  ,intent(in)   :: sep_nx
    real(kind=8)             ,intent(in)   :: sep_dx
    real(kind=8),allocatable ,intent(in)   :: sep_model(:)
    real(kind=8),allocatable ,intent(inout):: model    (:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    integer                    :: inode
    real(kind=8)               :: dist, vect(3)
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    model         = 0.0

    !! -----------------------------------------------------------------
    !! convert local cell barycenter to the closest SEP node 
    !! -----------------------------------------------------------------
    select case(ctx_mesh%dim_domain)
      !! ---------------------------------------------------------------
      !! 1D domain cannot be here, it is a normal sep in this case
      !! ---------------------------------------------------------------
      case(1)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! cell barycenter, using the absolute value because of symmetry
          vect(1) = dble(ctx_mesh%cell_bary(1,i_cell)) - center_point(1)
          vect(2) = 0.d0
          vect(3) = 0.d0
          dist = norm2(vect)

          !! find the closest node 
          inode   = nint((dist)/sep_dx) + 1
          !! consistency check:
          if(inode < 1 .or. inode > sep_nx) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_array] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif
          !! save value
          model(i_cell) = sep_model(inode)
        end do
      !! ---------------------------------------------------------------
      !! 2D
      !! ---------------------------------------------------------------
      case(2)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! distance of the center
          vect(1) = ctx_mesh%cell_bary(1,i_cell) - center_point(1)
          vect(2) = 0.0 !! my convention is y=0
          vect(3) = ctx_mesh%cell_bary(2,i_cell) - center_point(3)
          dist = norm2(vect)
          
          !! DO NOT remove sep_xmin, because it is considered as 
          !! the central point *****************************************
          !! inode   = nint((dist - sep_xmin)/sep_dx) + 1
          inode   = nint((dist)/sep_dx) + 1

          !! consistency check:
          if(inode < 1 .or. inode > sep_nx) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_spherical] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif
          !! save value
          model(i_cell) = sep_model(inode)
        end do
      !! ---------------------------------------------------------------
      !! 3D
      !! ---------------------------------------------------------------
      case(3)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! distance of the center
          vect(1) = ctx_mesh%cell_bary(1,i_cell) - center_point(1)
          vect(2) = ctx_mesh%cell_bary(2,i_cell) - center_point(2)
          vect(3) = ctx_mesh%cell_bary(3,i_cell) - center_point(3)
          dist = norm2(vect)
          
          !! DO NOT remove sep_xmin, because it is considered as 
          !! the central point *****************************************
          !! inode   = nint((dist - sep_xmin)/sep_dx) + 1
          inode   = nint((dist)/sep_dx) + 1

          !! consistency check:
          if(inode < 1 .or. inode > sep_nx) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_spherical] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif
          !! save value
          model(i_cell) = sep_model(inode)
        end do

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_sep_global_array_spherical_sym] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_sep_global_spherical_to_cell_local

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a 3D SEP to local subdomain using a spherical symmetry
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    center_point   : coordinates of the center
  !> @param[in]    sep_n{XYZ}     : sep model size
  !> @param[in]    sep_d{XYZ}     : sep model discretization step
  !> @param[in]    sep_model      : sep model
  !> @param[out]   model          : output subdomain model according to 
  !>                                the input cartesian domain ctx_domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_sep_global_spherical3d_to_cell_local(              &
                                             ctx_mesh,                  &
                                             sep_xmin,sep_ymin,sep_zmin,&
                                             sep_nx,sep_ny,sep_nz,      &
                                             sep_dx,sep_dy,sep_dz,      &
                                             sep_model,model,ctx_err)
    implicit none

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: sep_xmin,sep_ymin,sep_zmin
    integer                  ,intent(in)   :: sep_nx,sep_ny,sep_nz
    real(kind=8)             ,intent(in)   :: sep_dx,sep_dy,sep_dz
    real(kind=8),allocatable ,intent(in)   :: sep_model(:,:,:)
    real(kind=8),allocatable ,intent(inout):: model    (:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    integer                    :: inode_r,inode_t,inode_p
    real(kind=8)               :: dist, vect(3)
    real(kind=8), parameter    :: pi = 4.d0*datan(1.d0)
    real(kind=8)               :: theta_rad, phi_rad
    real(kind=8)               :: theta_deg, phi_deg
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    model         = 0.0

    !! -----------------------------------------------------------------
    !! convert local cell barycenter to the closest SEP node 
    !! -----------------------------------------------------------------
    select case(ctx_mesh%dim_domain)

      !! ---------------------------------------------------------------
      !! 1D domain cannot be here, it is a spherical 1d here
      !! ---------------------------------------------------------------
      case(1)
        ctx_err%msg   = "** ERROR: for 1D problem, you must use the " //&
                        " dedicated format spherical1d [project_sep_array] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error raised at upper level

      !! ---------------------------------------------------------------
      !! 2D
      !! ---------------------------------------------------------------
      case(2)
        do i_cell=1,ctx_mesh%n_cell_loc
          
          !! distance of the center wih is assumed to be (0,0,0)
          vect(1) = ctx_mesh%cell_bary(1,i_cell)
          vect(2) = 0.0 !! my convention is y=0
          vect(3) = ctx_mesh%cell_bary(2,i_cell)
          dist      = norm2(vect)
          if(dist > tiny(1.d0)) then
            theta_rad = datan2(vect(3),vect(1))
          else
            theta_rad = 0.d0
          end if
          ! ------------------------------------------------------------
          !! below is not very good because of the convention angle
          ! if(abs(vect(1)) > tiny(1.d0)) then
          !   theta_rad = datan(vect(3)/vect(1))
          ! else
          !   if(abs(vect(3)) > tiny(1.d0)) then
          !     theta_rad = vect(3)/abs(vect(3))  * pi/2.d0
          !   else
          !     theta_rad = 0.d0
          !   end if
          ! end if
          ! ------------------------------------------------------------
         
          !! convert to degrees
          theta_deg = theta_rad * 180.d0 / pi
          
          !! set betwee 0 and 359.9
          if(theta_deg < 0) theta_deg = 360 + theta_deg
          if(theta_deg > 360-tiny(1.d0)) theta_deg = 0.0
                    
          !! find the closest node 
          inode_r = nint((dist-sep_xmin)     /sep_dx) + 1
          inode_t = nint((theta_deg-sep_zmin)/sep_dz) + 1

          !! consistency check:
          if(inode_r < 1 .or. inode_r > sep_nx .or. inode_t < 1 .or.    &
             inode_t > sep_nz) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_sph3d] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif
          !! save value
          model(i_cell) = sep_model(inode_r,1,inode_t)
          
        end do

      !! ---------------------------------------------------------------
      !! 3D
      !! ---------------------------------------------------------------
      case(3)
        do i_cell=1,ctx_mesh%n_cell_loc
          
          !! distance of the center wih is assumed to be (0,0,0)
          vect(1) = ctx_mesh%cell_bary(1,i_cell)
          vect(2) = ctx_mesh%cell_bary(2,i_cell)
          vect(3) = ctx_mesh%cell_bary(3,i_cell)
          dist = norm2(vect)
          
          !! now we need the angle theta and phi
          if(abs(dist) > tiny(1.d0)) then
            
            theta_rad = acos(vect(3)/dist)
            if(abs(vect(1)) > tiny(1.d0) .and.                          &
               abs(vect(2)) > tiny(1.d0)) then
              phi_rad = datan2(vect(2), vect(1))
            else
              phi_rad = 0.d0
            end if
          else
            theta_rad = 0.d0
            phi_rad   = 0.d0
          end if
          
          !! convert to degrees
          theta_deg = theta_rad * 180.d0 / pi
          phi_deg   =   phi_rad * 180.d0 / pi
          !! theta is between 0 and 180, while phi is between -180 and 180.

          !! find the closest node 
          inode_r = nint((dist     - sep_xmin)/sep_dx) + 1
          inode_p = nint((phi_deg  - sep_ymin)/sep_dy) + 1
          inode_t = nint((theta_deg- sep_zmin)/sep_dz) + 1
          
          if(inode_r < 1 .or. inode_r > sep_nx .or.                     &
             inode_t < 1 .or. inode_t > sep_nz .or.                     &
             inode_p < 1 .or. inode_p > sep_ny) then
            ctx_err%msg   = "** ERROR: mesh cell barycenter outside " //&
                            " the read sep file in [project_sep_sph3d] **"
            ctx_err%ierr  = -1
            ctx_err%critic=.true.
            return !! non-shared error raised at upper level
          endif
          !! save value
          model(i_cell) = sep_model(inode_r,inode_p,inode_t)
          
        end do

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_sep_global_array_spherical_sym] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_sep_global_spherical3d_to_cell_local


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a 3D SEP to local subdomain using a spherical symmetry.
  !> Instead of returning an error if the position is not found, we 
  !> return a 0, i.e., we artificially extend the medium.
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    sep_{XYZ}min   : sep model physical min
  !> @param[in]    sep_n{XYZ}     : sep model size
  !> @param[in]    sep_d{XYZ}     : sep model discretization step
  !> @param[in]    sep_model      : sep model
  !> @param[out]   model          : output subdomain model according to 
  !>                                the input cartesian domain ctx_domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_sep_global_spherical3d_to_cell_local_extend0(      &
                                             ctx_mesh,                  &
                                             sep_xmin,sep_ymin,sep_zmin,&
                                             sep_nx,sep_ny,sep_nz,      &
                                             sep_dx,sep_dy,sep_dz,      &
                                             sep_model,model,ctx_err)
    implicit none

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: sep_xmin,sep_ymin,sep_zmin
    integer                  ,intent(in)   :: sep_nx,sep_ny,sep_nz
    real(kind=8)             ,intent(in)   :: sep_dx,sep_dy,sep_dz
    real(kind=8),allocatable ,intent(in)   :: sep_model(:,:,:)
    real(kind=8),allocatable ,intent(inout):: model    (:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    integer                    :: inode_r,inode_t,inode_p
    real(kind=8)               :: dist, vect(3)
    real(kind=8), parameter    :: pi = 4.d0*datan(1.d0)
    real(kind=8)               :: theta_rad, phi_rad
    real(kind=8)               :: theta_deg, phi_deg
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    model         = 0.0

    !! -----------------------------------------------------------------
    !! convert local cell barycenter to the closest SEP node 
    !! -----------------------------------------------------------------
    select case(ctx_mesh%dim_domain)

      !! ---------------------------------------------------------------
      !! 1D domain cannot be here, it is a spherical 1d here
      !! ---------------------------------------------------------------
      case(1)
        ctx_err%msg   = "** ERROR: for 1D problem, you must use the " //&
                        " dedicated format spherical1d [project_sep_array] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        return !! non-shared error raised at upper level

      !! ---------------------------------------------------------------
      !! 2D
      !! ---------------------------------------------------------------
      case(2)
        do i_cell=1,ctx_mesh%n_cell_loc
          
          !! distance of the center wih is assumed to be (0,0,0)
          vect(1) = ctx_mesh%cell_bary(1,i_cell)
          vect(2) = 0.0 !! my convention is y=0
          vect(3) = ctx_mesh%cell_bary(2,i_cell)
          dist      = norm2(vect)
          if(dist > tiny(1.d0)) then
            theta_rad = datan2(vect(3),vect(1))
          else
            theta_rad = 0.d0
          end if
          ! ------------------------------------------------------------
          !! below is not very good because of the convention angle
          ! if(abs(vect(1)) > tiny(1.d0)) then
          !   theta_rad = datan(vect(3)/vect(1))
          ! else
          !   if(abs(vect(3)) > tiny(1.d0)) then
          !     theta_rad = vect(3)/abs(vect(3))  * pi/2.d0
          !   else
          !     theta_rad = 0.d0
          !   end if
          ! end if
          ! ------------------------------------------------------------
         
          !! convert to degrees
          theta_deg = theta_rad * 180.d0 / pi
          
          !! set betwee 0 and 359.9
          if(theta_deg < 0) theta_deg = 360 + theta_deg
          if(theta_deg > 360-tiny(1.d0)) theta_deg = 0.0
                    
          !! find the closest node 
          inode_r = nint((dist-sep_xmin)     /sep_dx) + 1
          inode_t = nint((theta_deg-sep_zmin)/sep_dz) + 1

          !! consistency check:
          if(inode_r < 1 .or. inode_r > sep_nx .or. inode_t < 1 .or.    &
             inode_t > sep_nz) then
            !! return with the initial 0
          else
            !! save value
            model(i_cell) = sep_model(inode_r,1,inode_t)
          endif
          
        end do

      !! ---------------------------------------------------------------
      !! 3D
      !! ---------------------------------------------------------------
      case(3)
        do i_cell=1,ctx_mesh%n_cell_loc
          
          !! distance of the center wih is assumed to be (0,0,0)
          vect(1) = ctx_mesh%cell_bary(1,i_cell)
          vect(2) = ctx_mesh%cell_bary(2,i_cell)
          vect(3) = ctx_mesh%cell_bary(3,i_cell)
          dist = norm2(vect)
          
          !! now we need the angle theta and phi
          if(abs(dist) > tiny(1.d0)) then
            
            theta_rad = acos(dble(vect(3)/dist))
            if(abs(vect(1)) > tiny(1.d0) .and.                          &
               abs(vect(2)) > tiny(1.d0)) then
              phi_rad = datan2(vect(2), vect(1))
            else
              phi_rad = 0.d0
            end if
          else
            theta_rad = 0.d0
            phi_rad   = 0.d0
          end if
          
          !! convert to degrees
          theta_deg = theta_rad * 180.d0 / pi
          phi_deg   =   phi_rad * 180.d0 / pi
          !! theta is between 0 and 180, while phi is between -180 and 180.
          !! find the closest node 
          inode_r = nint((dist     - sep_xmin)/sep_dx) + 1
          inode_p = nint((phi_deg  - sep_ymin)/sep_dy) + 1
          inode_t = nint((theta_deg- sep_zmin)/sep_dz) + 1
          !! consistency check:
          if(inode_r < 1 .or. inode_r > sep_nx .or.                     &
             inode_t < 1 .or. inode_t > sep_nz .or.                     &
             inode_p < 1 .or. inode_p > sep_ny) then
            !! return with the initial 0
          else
            !! save value
            model(i_cell) = sep_model(inode_r,inode_p,inode_t)
          endif
          
        end do

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_sep_global_array_spherical_sym] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_sep_global_spherical3d_to_cell_local_extend0
  
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a 1D SEP to local subdomain using a spherical symmetry.
  !> Instead of returning an error if the position is not found, we 
  !> return a 0, i.e., we artificially extend the medium.
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    center_point   : coordinates of the center
  !> @param[in]    sep_n{XYZ}     : sep model size
  !> @param[in]    sep_d{XYZ}     : sep model discretization step
  !> @param[in]    sep_model      : sep model
  !> @param[out]   model          : output subdomain model according to 
  !>                                the input cartesian domain ctx_domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_sep_global_spherical_to_cell_local_extend0(ctx_mesh, &
                                             center_point,sep_nx,sep_dx,  &
                                             sep_model,model,ctx_err)
    implicit none

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: center_point(3)
    integer                  ,intent(in)   :: sep_nx
    real(kind=8)             ,intent(in)   :: sep_dx
    real(kind=8),allocatable ,intent(in)   :: sep_model(:)
    real(kind=8),allocatable ,intent(inout):: model    (:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    integer                    :: inode
    real(kind=8)               :: dist, vect(3)
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    model         = 0.0

    !! -----------------------------------------------------------------
    !! convert local cell barycenter to the closest SEP node 
    !! -----------------------------------------------------------------
    select case(ctx_mesh%dim_domain)
      !! ---------------------------------------------------------------
      !! 1D domain cannot be here, it is a normal sep in this case
      !! ---------------------------------------------------------------
      case(1)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! cell barycenter, using the absolute value because of symmetry
          vect(1) = dble(ctx_mesh%cell_bary(1,i_cell)) - center_point(1)
          vect(2) = 0.d0
          vect(3) = 0.d0
          dist = norm2(vect)

          !! find the closest node 
          inode   = nint((dist)/sep_dx) + 1
          !! consistency check:
          if(inode < 1 .or. inode > sep_nx) then
            !! return with the initial 0
          else
            !! save value
            model(i_cell) = sep_model(inode)
          end if
        end do
      !! ---------------------------------------------------------------
      !! 2D
      !! ---------------------------------------------------------------
      case(2)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! distance of the center
          vect(1) = ctx_mesh%cell_bary(1,i_cell) - center_point(1)
          vect(2) = 0.0 !! my convention is y=0
          vect(3) = ctx_mesh%cell_bary(2,i_cell) - center_point(3)
          dist = norm2(vect)
          
          !! DO NOT remove sep_xmin, because it is considered as 
          !! the central point *****************************************
          !! inode   = nint((dist - sep_xmin)/sep_dx) + 1
          inode   = nint((dist)/sep_dx) + 1

          !! consistency check:
          if(inode < 1 .or. inode > sep_nx) then
            !! return with the initial 0  
          else
            !! save value
            model(i_cell) = sep_model(inode)
          end if
        end do
      !! ---------------------------------------------------------------
      !! 3D
      !! ---------------------------------------------------------------
      case(3)
        do i_cell=1,ctx_mesh%n_cell_loc
          !! distance of the center
          vect(1) = ctx_mesh%cell_bary(1,i_cell) - center_point(1)
          vect(2) = ctx_mesh%cell_bary(2,i_cell) - center_point(2)
          vect(3) = ctx_mesh%cell_bary(3,i_cell) - center_point(3)
          dist = norm2(vect)
          
          !! DO NOT remove sep_xmin, because it is considered as 
          !! the central point *****************************************
          !! inode   = nint((dist - sep_xmin)/sep_dx) + 1
          inode   = nint((dist)/sep_dx) + 1

          !! consistency check:
          if(inode < 1 .or. inode > sep_nx) then
            !! return with the initial 0
          else
            !! save value
            model(i_cell) = sep_model(inode)
          end if
        end do

      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_sep_global_array_spherical_sym] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_sep_global_spherical_to_cell_local_extend0

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a global array to local subdomain
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    in_model       : global input model
  !> @param[out]   model          : output subdomain model according to 
  !>                                the input cartesian domain ctx_domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_ascii_global_to_cell_local(ctx_mesh,in_model,model,ctx_err)
    
    implicit none

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8),allocatable ,intent(in)   :: in_model(:)
    real(kind=8),allocatable ,intent(inout):: model   (:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------   
    select case(ctx_mesh%dim_domain)
      !! 1D, 2D or 3D domain
      case(1,2,3)
        do i_cell=1,ctx_mesh%n_cell_loc
          model(i_cell) = in_model(ctx_mesh%index_loctoglob_cell(i_cell))
        end do
    
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_ascii_array] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_ascii_global_to_cell_local


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a local array by cell to global array by cell.
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    model_in_loc   : local input model
  !> @param[out]   model_out_gb   : output global model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_local_cell_array_to_global_real4(ctx_mesh,         &
                                      model_in_loc,model_out_gb,ctx_err)

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real                     ,intent(in)   :: model_in_loc(:)
    real,allocatable         ,intent(inout):: model_out_gb(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out_gb = 0.0
    !! -----------------------------------------------------------------   
    select case(ctx_mesh%dim_domain)
      !! 1D, 2D or 3D domain
      case(1,2,3)
        do i_cell=1,ctx_mesh%n_cell_loc
          model_out_gb(ctx_mesh%index_loctoglob_cell(i_cell)) =         &
          model_in_loc(i_cell)
        end do
    
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_local_cell_array_to_global] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_local_cell_array_to_global_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a local array by cell to global array by cell.
  !
  !> @param[in]    ctx_mesh       : mesh domain infos
  !> @param[in]    model_in_loc   : local input model
  !> @param[out]   model_out_gb   : output global model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine project_local_cell_array_to_global_real8(ctx_mesh,         &
                                      model_in_loc,model_out_gb,ctx_err)

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: model_in_loc(:)
    real(kind=8),allocatable ,intent(inout):: model_out_gb(:)
    type(t_error)            ,intent(inout):: ctx_err
    !! local
    integer(kind=IKIND_MESH)   :: i_cell
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out_gb = 0.0
    !! -----------------------------------------------------------------
    select case(ctx_mesh%dim_domain)
      !! 1D, 2D or 3D domain
      case(1,2,3)
        do i_cell=1,ctx_mesh%n_cell_loc
          model_out_gb(ctx_mesh%index_loctoglob_cell(i_cell)) =         &
          model_in_loc(i_cell)
        end do
    
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_local_cell_array_to_global] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_local_cell_array_to_global_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a global array by cell to a local array by cell.
  !
  !> @param[in]    ctx_mesh         : mesh domain infos
  !> @param[in]    model_in_gb      : local input model
  !> @param[out]   model_out_loc    : output global model
  !> @param[inout] ctx_err          : context error
  !> @param[optional] o_flag_ghost  : indicates if ghost are also included
  !----------------------------------------------------------------------------
  subroutine project_global_cell_array_to_local_real4(ctx_mesh,model_in_gb, &
                                                      model_out_loc,ctx_err,&
                                                      o_flag_ghost)

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=4)             ,intent(in)   :: model_in_gb  (:)
    real(kind=4),allocatable ,intent(inout):: model_out_loc(:)
    type(t_error)            ,intent(inout):: ctx_err
    logical, optional        ,intent(in)   :: o_flag_ghost
    !! local
    integer(kind=IKIND_MESH)   :: i_cell, n_cell
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out_loc = 0.0
    n_cell = ctx_mesh%n_cell_loc
    if(present(o_flag_ghost)) then
      if(o_flag_ghost) n_cell = n_cell + ctx_mesh%n_ghost_loc
    end if
    !! -----------------------------------------------------------------   
    select case(ctx_mesh%dim_domain)
      !! 1D, 2D or 3D domain
      case(1,2,3)
        do i_cell=1,n_cell
          model_out_loc(i_cell) =                                       &
                      model_in_gb(ctx_mesh%index_loctoglob_cell(i_cell))
        end do
    
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_global_cell_array_to_local] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_global_cell_array_to_local_real4


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a global array by cell to a local array by cell.
  !
  !> @param[in]       ctx_mesh       : mesh domain infos
  !> @param[in]       model_in_gb    : local input model
  !> @param[out]      model_out_loc  : output global model
  !> @param[inout]    ctx_err        : context error
  !> @param[optional] o_flag_ghost   : indicates if ghost are also included
  !----------------------------------------------------------------------------
  subroutine project_global_cell_array_to_local_real8(ctx_mesh,model_in_gb, &
                                                      model_out_loc,ctx_err,&
                                                      o_flag_ghost)

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: model_in_gb  (:)
    real(kind=8),allocatable ,intent(inout):: model_out_loc(:)
    type(t_error)            ,intent(inout):: ctx_err
    logical, optional        ,intent(in)   :: o_flag_ghost
    !! local
    integer(kind=IKIND_MESH)   :: i_cell, n_cell
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out_loc = 0.0
    n_cell = ctx_mesh%n_cell_loc
    if(present(o_flag_ghost)) then
      if(o_flag_ghost) n_cell = n_cell + ctx_mesh%n_ghost_loc
    end if
    !! -----------------------------------------------------------------   
    select case(ctx_mesh%dim_domain)
      !! 1D, 2D or 3D domain
      case(1,2,3)
        do i_cell=1,n_cell
          model_out_loc(i_cell) =                                       &
                      model_in_gb(ctx_mesh%index_loctoglob_cell(i_cell))
        end do
    
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_global_cell_array_to_local] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_global_cell_array_to_local_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a global array by dof to a local array by dof.
  !
  !> @param[in]    ctx_mesh         : mesh domain infos
  !> @param[in]    model_in_gb      : local input model
  !> @param[out]   model_out_loc    : output global model
  !> @param[in]    npt_per_cell     : number of coeff per cells
  !> @param[inout] ctx_err          : context error
  !> @param[optional] o_flag_ghost  : indicates if ghost are also included
  !----------------------------------------------------------------------------
  subroutine project_global_subcell_array_to_local_real4(               &
                                    ctx_mesh,model_in_gb,model_out_loc, &
                                    npt_per_cell,ctx_err,o_flag_ghost)

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=4)             ,intent(in)   :: model_in_gb  (:)
    real(kind=4),allocatable ,intent(inout):: model_out_loc(:)
    integer                  ,intent(in)   :: npt_per_cell
    type(t_error)            ,intent(inout):: ctx_err
    logical, optional        ,intent(in)   :: o_flag_ghost
    !! local
    integer(kind=IKIND_MESH)   :: i_cell, n_cell
    integer(kind=IKIND_MESH)   :: igb1, igb2, iloc1, iloc2
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out_loc = 0.0
    n_cell = ctx_mesh%n_cell_loc
    if(present(o_flag_ghost)) then
      if(o_flag_ghost) n_cell = n_cell + ctx_mesh%n_ghost_loc
    end if
    
    !! -----------------------------------------------------------------   
    select case(ctx_mesh%dim_domain)
      !! 1D, 2D or 3D domain
      case(1,2,3)
        do i_cell=1,n_cell
          
          igb1  = (ctx_mesh%index_loctoglob_cell(i_cell)-1)*npt_per_cell + 1
          igb2  = igb1 + npt_per_cell - 1
          
          iloc1 = (i_cell-1)*npt_per_cell + 1
          iloc2 =  iloc1   + npt_per_cell - 1
          
          model_out_loc(iloc1:iloc2) = model_in_gb(igb1:igb2)
        end do
    
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_global_cell_array_to_local] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_global_subcell_array_to_local_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> project a global array by dof to a local array by dof.
  !
  !> @param[in]    ctx_mesh         : mesh domain infos
  !> @param[in]    model_in_gb      : local input model
  !> @param[out]   model_out_loc    : output global model
  !> @param[in]    npt_per_cell     : number of coeff per cells
  !> @param[inout] ctx_err          : context error
  !> @param[optional] o_flag_ghost  : indicates if ghost are also included
  !----------------------------------------------------------------------------
  subroutine project_global_subcell_array_to_local_real8(               &
                                    ctx_mesh,model_in_gb,model_out_loc, &
                                    npt_per_cell,ctx_err,o_flag_ghost)

    type(t_domain)           ,intent(in)   :: ctx_mesh
    real(kind=8)             ,intent(in)   :: model_in_gb  (:)
    real(kind=8),allocatable ,intent(inout):: model_out_loc(:)
    integer                  ,intent(in)   :: npt_per_cell
    type(t_error)            ,intent(inout):: ctx_err
    logical, optional        ,intent(in)   :: o_flag_ghost
    !! local
    integer(kind=IKIND_MESH)   :: i_cell, n_cell
    integer(kind=IKIND_MESH)   :: igb1, igb2, iloc1, iloc2
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out_loc = 0.0
    n_cell = ctx_mesh%n_cell_loc
    if(present(o_flag_ghost)) then
      if(o_flag_ghost) n_cell = n_cell + ctx_mesh%n_ghost_loc
    end if
    
    !! -----------------------------------------------------------------   
    select case(ctx_mesh%dim_domain)
      !! 1D, 2D or 3D domain
      case(1,2,3)
        do i_cell=1,n_cell
          
          igb1  = (ctx_mesh%index_loctoglob_cell(i_cell)-1)*npt_per_cell + 1
          igb2  = igb1 + npt_per_cell - 1
          
          iloc1 = (i_cell-1)*npt_per_cell + 1
          iloc2 =  iloc1   + npt_per_cell - 1
          
          model_out_loc(iloc1:iloc2) = model_in_gb(igb1:igb2)
        end do
    
      case default
        ctx_err%msg   = "** ERROR: unrecognized mesh dimension "  // &
                        "[project_global_cell_array_to_local] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine project_global_subcell_array_to_local_real8
  
end module m_project_array
