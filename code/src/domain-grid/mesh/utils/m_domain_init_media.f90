!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_domain_init_media.f90
!
!> @author
!> F. Faucher [INRIA Makutu]
!
! DESCRIPTION:
!> @brief
!> The module is used to initialized the domain information in 
!> the context of a coupling of media.
!
!------------------------------------------------------------------------------
module m_domain_init_media

  use omp_lib  
  use m_define_precision,     only : IKIND_MESH, RKIND_MESH
  use m_raise_error,          only : raise_error, t_error
  use m_distribute_error,     only : distribute_error
  use m_time,                 only : time
  use m_ctx_domain,           only : t_domain
  use m_tag_bc
  use m_tag_media
  use m_ctx_parallelism,      only : t_parallelism
  use m_allreduce_sum,        only : allreduce_sum
  use m_barrier,              only : barrier

  implicit none

  private  
  public :: domain_init_media
    
  contains  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine is the interface to initialize the media.
  !>
  !> @details
  !
  !> @param[in]    ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[in]    dim_domain    : domain dimension
  !> @param[in]    medium_tag_in : array containing the tags read
  !> @param[in]    ntag_1medium_max : maximal number of tags read for 1 type
  !> @param[in]    global_boundary_info : list of boundary tags read
  !> @param[out]   ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine domain_init_media(ctx_paral,ctx_mesh,dim_domain,           &
                               medium_tag_in,ntag_1medium_max,          &
                               global_boundary_info,ctx_err)

    implicit none
    type(t_parallelism),intent(in)        :: ctx_paral
    type(t_domain)     ,intent(inout)     :: ctx_mesh
    integer            ,intent(in)        :: dim_domain
    integer,allocatable,intent(in)        :: medium_tag_in(:,:)
    integer            ,intent(in)        :: ntag_1medium_max
    integer            ,intent(in)        :: global_boundary_info(:,:)
    type(t_error)      ,intent(inout)     :: ctx_err
    
    ctx_err%ierr = 0

    !! -----------------------------------------------------------------
    !! selection with the dimension
    !! -----------------------------------------------------------------
    select case(dim_domain)
      case(1)
        call domain_init_media_1D(ctx_paral,ctx_mesh,medium_tag_in,     &
                                  ntag_1medium_max,global_boundary_info,&
                                  ctx_err)
      case default
        !! xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        !! We have to read the ctx_media%file_medium
        !! to see what are the nature of the cells
        !! xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      
        ctx_err%msg   = "** ERROR: The consideration of coupling is "// &
                        "not possible for this dimension problem "   // &
                        "[domain_init_medi]"
        ctx_err%ierr  = -65
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
        
    return

  end subroutine domain_init_media
  !----------------------------------------------------------------------------

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine is the interface to initialize the media in 1D.
  !>
  !> @details
  !
  !> @param[in]    ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[in]    medium_tag_in : array containing the tags read
  !> @param[in]    ntag_read_max : maximal number of tags read for 1 type
  !> @param[in]    global_boundary_info : list of boundary tags read
  !> @param[out]   ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine domain_init_media_1D(ctx_paral,ctx_mesh,medium_tag_in,     &
                                  ntag_read_max,global_boundary_info,   &
                                  ctx_err)

    implicit none
    type(t_parallelism),intent(in)        :: ctx_paral
    type(t_domain)     ,intent(inout)     :: ctx_mesh
    integer            ,intent(in)        :: medium_tag_in(:,:)
    integer            ,intent(in)        :: ntag_read_max
    integer            ,intent(in)        :: global_boundary_info(:,:)
    type(t_error)      ,intent(inout)     :: ctx_err
    !! local
    integer(kind=8)            :: mem
    integer                    :: i_tag_in,i_tag_main,medium2,k
    integer                    :: local_zone,main_tag,medium1,n_zone
    integer                    :: counter_ghost,ind_global
    integer                    :: flag_boundary_found
    integer, allocatable       :: tag_zone_main(:)
    integer, allocatable       :: cell_tag_global_loc(:)
    integer, allocatable       :: cell_tag_global_gb (:)
    integer(kind=IKIND_MESH)   :: n_cell_loc,i_cell,i_neigh
    integer(kind=IKIND_MESH)   :: n_neigh_per_cell
    integer, parameter         :: int_size  = 4
    integer, parameter         :: real_size = 4
    real(kind=8)               :: xloc

    ctx_err%ierr = 0

    n_cell_loc       = ctx_mesh%n_cell_loc
    n_neigh_per_cell = ctx_mesh%n_neigh_per_cell

    !! memory count: 
    ctx_mesh%ctx_media%memory_loc = 0
    mem = (n_cell_loc*int_size)                    + & !! tag_cell
          (n_cell_loc*n_neigh_per_cell*int_size)   + & !! cell_neigh
          (n_medium_tag*IKIND_MESH)                + & !! counting the cells
          (n_medium_tag*IKIND_MESH)                + & !! counting the cells
          ((ctx_mesh%ctx_media%n_zone-1)*real_size)+ & !! xinterface1D
          !! these are the arrays already defined 
          (2*int_size) !! 2 integers
    ctx_mesh%ctx_media%memory_loc = mem 
    
    !! allocation
    allocate(ctx_mesh%ctx_media%tag_cell                   (n_cell_loc))
    allocate(ctx_mesh%ctx_media%cell_neigh(n_neigh_per_cell,n_cell_loc))
    allocate(ctx_mesh%ctx_media%n_cell_medium_loc        (n_medium_tag))
    allocate(ctx_mesh%ctx_media%n_cell_medium_gb         (n_medium_tag))
    !! initialization to the default neighbor information.
    ctx_mesh%ctx_media%cell_neigh        = ctx_mesh%cell_neigh
    ctx_mesh%ctx_media%n_cell_medium_loc = 0
    ctx_mesh%ctx_media%n_cell_medium_gb  = 0
    ctx_mesh%ctx_media%tag_cell          = 0

    !! 1 dimension: ----------------------------------------------------
    !! we have one tag for each of the zone, given in the input 
    !! array  medium_tag_in(n_medium_tag,ntag_max_for_one_medium)
    !! we convert these input tags to the tags of the code now.
    !!
    !! (1): medium_tag_stellar_interior
    !! (2): medium_tag_stellar_atmo
    !! (3): medium_tag_acoustic
    !! (4): medium_tag_elastic
    allocate(tag_zone_main(ctx_mesh%ctx_media%n_zone))
    tag_zone_main = 0
    do i_tag_main=1, n_medium_tag
      main_tag = 0
      if    (i_tag_main .eq. 1) then ; main_tag = tag_medium_interior
      elseif(i_tag_main .eq. 2) then ; main_tag = tag_medium_atmo
      elseif(i_tag_main .eq. 3) then ; main_tag = tag_medium_acoustic
      elseif(i_tag_main .eq. 4) then ; main_tag = tag_medium_elastic
      else
        ctx_err%msg   = "** ERROR: Unrecognized tag for media "   //    &
                        "[domain_init_media_1D]"
        ctx_err%ierr  = -65
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      end if
      
      do i_tag_in = 1, ntag_read_max
        local_zone = medium_tag_in(i_tag_main,i_tag_in)
        if(local_zone > 0) then
          if(local_zone <= ctx_mesh%ctx_media%n_zone) then
            tag_zone_main(local_zone) = main_tag
          else
            ctx_err%msg   = "** ERROR: There are too many input tags "//&
                            "for the type of media compared to the "  //&
                            "list of interval given by keyword "      //&
                            "medium_interface1D [domain_init_media_1D]"
            ctx_err%ierr  = -66
            ctx_err%critic=.true.
            call raise_error(ctx_err)
          end if
        end if
      end do
    end do

    if(minval(tag_zone_main) <= 0) then
      ctx_err%msg   = "** ERROR: There are not enough input tags "//    &
                      "for the type of media compared to the "    //    &
                      "list of interval given by keyword "        //    &
                      "medium_interface1D [domain_init_media_1D]"
      ctx_err%ierr  = -67
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if    
    !! -----------------------------------------------------------------

    !! --------------------------------------------------------------------------
    !! in 1D the tags are listed in increasing manner starting at 1, e.g.:
    !! --------------------------------------------------------------------------
    !! xinterval =       x0      xa      xb      xc      x1
    !!                   |  TAG1  | TAG2  | TAG3  | TAG4  |
    !! --------------------------------------------------------------------------

    !! ----------------------------------------------------------------- %
    !! only loop over the local cells
    !! ----------------------------------------------------------------- %
    n_zone = ctx_mesh%ctx_media%n_zone

    do i_cell=1,n_cell_loc
      
      if(ctx_err%ierr .ne. 0) cycle
      
      !! check if position compared to the input interface 1D
      !! special cases are below the first x1d and above the last one.
      xloc     = dble(ctx_mesh%cell_bary(1,i_cell))
      main_tag = 0
      if(xloc <= ctx_mesh%ctx_media%xinterface1D(1)) then
        main_tag = tag_zone_main(1)
      elseif(xloc > ctx_mesh%ctx_media%xinterface1D(n_zone-1)) then
        main_tag = tag_zone_main(n_zone)
      else
        !! need to find which interval is it in.
        do k=1,ctx_mesh%ctx_media%n_zone-2
          if(xloc <= ctx_mesh%ctx_media%xinterface1D(k+1) .and.         & 
             xloc  > ctx_mesh%ctx_media%xinterface1D(k) ) then
            main_tag = tag_zone_main(k+1)
          end if
        end do
      end if
      
      !! verification and save
      if(main_tag <= 0 .or. main_tag > n_medium_tag) then
        ctx_err%msg   = "** ERROR: The current cell has no type " //    &
                        "of medium [domain_init_media_1D]"
        ctx_err%ierr  = -68
        ctx_err%critic=.true.
        !! non-shared error call raise_error(ctx_err)
      else ! ----------------------------------------------------------- %
        !! save in array
        ctx_mesh%ctx_media%n_cell_medium_loc(main_tag) =                &
        ctx_mesh%ctx_media%n_cell_medium_loc(main_tag) + 1
        ctx_mesh%ctx_media%tag_cell(i_cell)            = main_tag
      end if
    end do
    !! ----------------------------------------------------------------- %
    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! Face tags now --------------------------------------------------- %
    !! already initialized to the default neighbor informations
    !! ----------------------------------------------------------------- %
    !!
    !! We create a global array to avoid problem with the ghost
    allocate(cell_tag_global_loc(ctx_mesh%n_cell_gb))
    allocate(cell_tag_global_gb (ctx_mesh%n_cell_gb))
    cell_tag_global_loc = 0
    cell_tag_global_gb  = 0
    do i_cell = 1, ctx_mesh%n_cell_loc
      cell_tag_global_loc(ctx_mesh%index_loctoglob_cell(i_cell)) =      &
                                     ctx_mesh%ctx_media%tag_cell(i_cell)
    end do
    call allreduce_sum(cell_tag_global_loc,cell_tag_global_gb,          &
                       ctx_mesh%n_cell_gb,ctx_paral%communicator,ctx_err)
    deallocate(cell_tag_global_loc)
                       
    !!
    !!
    !!
    !! ----------------------------------------------------------------- %
    counter_ghost = 0
    do i_cell = 1, n_cell_loc

      if(ctx_err%ierr .ne. 0) cycle
      
      !! only in the case where there is a discrepency between the 
      !! media, we must include the information. 
      medium1 = ctx_mesh%ctx_media%tag_cell(i_cell)
      medium2 = medium1 ! initialization for now.

      !! loop over the faces ------------------------------------------- %
      do i_neigh = 1, n_neigh_per_cell

        select case(ctx_mesh%cell_neigh(i_neigh,i_cell))

          !! ------------------------------------
          !! the neighbor is on another processor
          !! we need to be sure the interface is 
          !! not there then.
          !! -------------------------------------
          case(tag_ghost_cell)
            counter_ghost = counter_ghost + 1
            ind_global = ctx_mesh%index_loctoglob_cell(n_cell_loc+counter_ghost)

          case default
            if(ctx_mesh%cell_neigh(i_neigh,i_cell) > 0) then !! existing here
              ind_global = ctx_mesh%index_loctoglob_cell(               &
                                     ctx_mesh%cell_neigh(i_neigh,i_cell))
            else             
              !! It is a physical boundary so we just keep the same
              ind_global = ctx_mesh%index_loctoglob_cell(i_cell)
            end if
        end select
        !! ------------------------------------------------------------- %
        medium2 = cell_tag_global_gb(ind_global)

        !! we now compare medium1 / medium2 ---------------------------- %
        if(medium1 .ne. medium2) then
          !! Then we need to indicate the change in the medium
          if    ((medium1 .eq. tag_medium_acoustic .and.          &
                  medium2 .eq. tag_medium_elastic)) then
            ctx_mesh%ctx_media%cell_neigh(i_neigh,i_cell) =       &
                              tag_medium_interface_acoustic_to_elastic
          elseif((medium2 .eq. tag_medium_acoustic .and.          &
                  medium1 .eq. tag_medium_elastic)) then
            ctx_mesh%ctx_media%cell_neigh(i_neigh,i_cell) =       &
                              tag_medium_interface_elastic_to_acoustic
          elseif((medium1 .eq. tag_medium_interior .and.          &
                  medium2 .eq. tag_medium_atmo)) then
            ctx_mesh%ctx_media%cell_neigh(i_neigh,i_cell) =       &
                              tag_medium_interface_interior_to_atmo
          elseif((medium2 .eq. tag_medium_interior .and.          &
                  medium1 .eq. tag_medium_atmo)) then
            ctx_mesh%ctx_media%cell_neigh(i_neigh,i_cell) =       &
                              tag_medium_interface_atmo_to_interior
          else
            ctx_err%msg   = "** ERROR: Interface between two " // &
                            "unknown media [domain_init_media_1D]"
            ctx_err%ierr  = -69
            ctx_err%critic=.true.
            !! non-shared error here. call raise_error(ctx_err)
          end if
        end if
        !! ------------------------------------------------------------- 
        
      end do !! end loop over neighbors

    end do !! end loop over cell

    !! -----------------------------------------------------------------
    !! possibility for non-shared error
    call distribute_error(ctx_err,ctx_paral%communicator) 

    !! sum up ----------------------------------------------------------      
    !! wait for all here
    call barrier(ctx_paral%communicator,ctx_err)
    !! sum the counters
    call allreduce_sum(ctx_mesh%ctx_media%n_cell_medium_loc,            &
                       ctx_mesh%ctx_media%n_cell_medium_gb,             &
                       n_medium_tag,ctx_paral%communicator,ctx_err)

    deallocate(tag_zone_main)
    deallocate(cell_tag_global_gb)
    !! ----------------------------------------------------------------- 
    
    !! -----------------------------------------------------------------
    !! For interior/atmo interface, we have to identify what is the 
    !! boundary condition to be set at the interface between the 
    !! media. This is not necessary for acoustic-elastic for instance. 
    !! -----------------------------------------------------------------
    !! only a few can be understood, these are the conditions at the
    !! end of the domain. They will be split at the interface for al.
    !! # 4: al-dirichlet dl-dirichlet
    !! # 7: al-dirichlet dl-robin
    !! #11: dp-dirichlet dl-dirichlet
    !! #13: dp-dirichlet dl-robin
    !! #14: al-rbc       dl-robin
    !! #15: al-rbc       dl-dirichlet
    !! #25: lagrangep    dl-robin
    !! -----------------------------------------------------------------
    ctx_mesh%ctx_media%tag_interface1D = 0
    if(ctx_mesh%ctx_media%n_cell_medium_gb(tag_medium_interior) > 0 .and. &
       ctx_mesh%ctx_media%n_cell_medium_gb(tag_medium_atmo)     > 0) then

      flag_boundary_found = 0
      if(abs(global_boundary_info( 4,1)) > 0) then 
        flag_boundary_found                = flag_boundary_found+1
        ctx_mesh%ctx_media%tag_interface1D = tag_dirichlet
      end if
      if(abs(global_boundary_info( 7,1)) > 0) then 
        flag_boundary_found                = flag_boundary_found+1
        ctx_mesh%ctx_media%tag_interface1D = tag_aldirichlet_dlrobin
      end if
      if(abs(global_boundary_info(11,1)) > 0) then 
        flag_boundary_found                = flag_boundary_found+1
        ctx_mesh%ctx_media%tag_interface1D = tag_deltapdirichlet_dldirichlet
      end if
      if(abs(global_boundary_info(13,1)) > 0) then 
        flag_boundary_found                = flag_boundary_found+1
        ctx_mesh%ctx_media%tag_interface1D = tag_deltapdirichlet_dlrobin
      end if
      if(abs(global_boundary_info(14,1)) > 0) then 
        flag_boundary_found     = flag_boundary_found+1
        ctx_mesh%ctx_media%tag_interface1D = tag_alrbc_dlrobin
      end if
      if(abs(global_boundary_info(15,1)) > 0) then 
        flag_boundary_found                = flag_boundary_found+1
        ctx_mesh%ctx_media%tag_interface1D = tag_alrbc_dldirichlet
      end if
      if(abs(global_boundary_info(25,1)) > 0) then 
        flag_boundary_found                = flag_boundary_found+1
        ctx_mesh%ctx_media%tag_interface1D = tag_lagrangep_dlrobin
      end if

      if(flag_boundary_found > 1) then
        ctx_err%msg   = "** ERROR: There is an ambiguity in the " //    &
                        "condition to be imposed at the stellar " //    &
                        "interface between interior and atmo. "   //    &
                        "[domain_init_media_1D]"
        ctx_err%ierr  = -69
        ctx_err%critic=.true.
      end if
      if(flag_boundary_found < 1) then
        ctx_err%msg   = "** ERROR: There is no condition found "  //    &
                        "to be imposed at the stellar " //              &
                        "interface between interior and atmo. "   //    &
                        "[domain_init_media_1D]"
        ctx_err%ierr  = -69
        ctx_err%critic=.true.
      end if
      if(ctx_err%ierr .ne. 0) call raise_error(ctx_err)

    end if
    !! -----------------------------------------------------------------



    return

  end subroutine domain_init_media_1D
  !----------------------------------------------------------------------------
  
end module m_domain_init_media
