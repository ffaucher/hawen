!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_domain_restriction.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to restrict an array given by cell by putting
!> 0 outside the geometrical box considered
!> 
!> THIS ROUTINE USES OMP PARALLELISM
!
!------------------------------------------------------------------------------
module m_domain_restriction

  use omp_lib
  use m_raise_error,          only : raise_error, t_error
  use m_ctx_domain,           only : t_domain

  implicit none

  private  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the cell that are outside the 
  !>  given box  
  interface domain_restriction_per_cell
     module procedure domain_restriction_array1d_real4
     module procedure domain_restriction_array1d_real8
     module procedure domain_restriction_array2d_real4
     module procedure domain_restriction_array2d_real8
  end interface
  !---------------------------------------------------------------------  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero all points in the cells that are outside the 
  !>  given box  
  interface domain_restriction_per_subcell
     module procedure domain_restriction_subconstant_array1d_real4
     module procedure domain_restriction_subconstant_array1d_real8
     module procedure domain_restriction_subconstant_array2d_real4
     module procedure domain_restriction_subconstant_array2d_real8
  end interface
  !---------------------------------------------------------------------  

  public :: domain_restriction_per_cell, domain_restriction_per_subcell
    
  contains

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the cell that are outside the 
  !>  given box
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_array1d_real4(dim_domain,ctx_mesh,array,&
                                              ncells,xmin,xmax,ymin,    &
                                              ymax,zmin,zmax,rmin,rmax, &
                                              r0,ctx_err)

    implicit none
    integer                   ,intent(in)        :: dim_domain
    type(t_domain)            ,intent(in)        :: ctx_mesh
    real(kind=4),allocatable  ,intent(inout)     :: array(:)
    real(kind=4)              ,intent(in)        :: xmin,xmax
    real(kind=4)              ,intent(in)        :: ymin,ymax
    real(kind=4)              ,intent(in)        :: zmin,zmax
    real(kind=4)              ,intent(in)        :: rmin,rmax
    real(kind=4)              ,intent(in)        :: r0(3)
    integer                   ,intent(in)        :: ncells
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r)
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            array(icell) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(2)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar2d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(3)
        !! loop over all cells    
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar3d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case default
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
    end select

    return
  end subroutine domain_restriction_array1d_real4


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the cell that are outside the 
  !>  given box
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_array1d_real8(dim_domain,ctx_mesh,array,&
                                              ncells,xmin,xmax,ymin,    &
                                              ymax,zmin,zmax,rmin,rmax, &
                                              r0,ctx_err)

    implicit none
    integer                   ,intent(in)        :: dim_domain
    type(t_domain)            ,intent(in)        :: ctx_mesh
    real(kind=8),allocatable  ,intent(inout)     :: array(:)
    real(kind=4)              ,intent(in)        :: xmin,xmax
    real(kind=4)              ,intent(in)        :: ymin,ymax
    real(kind=4)              ,intent(in)        :: zmin,zmax
    real(kind=4)              ,intent(in)        :: rmin,rmax
    real(kind=4)              ,intent(in)        :: r0(3)
    integer                   ,intent(in)        :: ncells
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r)
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            array(icell) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(2)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar2d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(3)
        !! loop over all cells    
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar3d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case default
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
    end select

    return
  end subroutine domain_restriction_array1d_real8

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the cell that are outside the 
  !>  given box
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_array2d_real4(dim_domain,ctx_mesh,array,&
                                              ncells,xmin,xmax,ymin,    &
                                              ymax,zmin,zmax,rmin,rmax, &
                                              r0,ctx_err)

    implicit none
    integer                        ,intent(in)        :: dim_domain
    type(t_domain)                 ,intent(in)        :: ctx_mesh
    real(kind=4),allocatable       ,intent(inout)     :: array(:,:)
    real(kind=4)                   ,intent(in)        :: xmin,xmax
    real(kind=4)                   ,intent(in)        :: ymin,ymax
    real(kind=4)                   ,intent(in)        :: zmin,zmax
    real(kind=4)                   ,intent(in)        :: rmin,rmax
    real(kind=4)                   ,intent(in)        :: r0(3)
    integer                        ,intent(in)        :: ncells
    type(t_error)                  ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r)
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            array(icell,:) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(2)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar2d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell,:) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(3)
        !! loop over all cells    
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar3d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell,:) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case default
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
    end select

    return
  end subroutine domain_restriction_array2d_real4


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the cell that are outside the 
  !>  given box
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_array2d_real8(dim_domain,ctx_mesh,array,&
                                              ncells,xmin,xmax,ymin,    &
                                              ymax,zmin,zmax,rmin,rmax, &
                                              r0,ctx_err)

    implicit none
    integer                        ,intent(in)        :: dim_domain
    type(t_domain)                 ,intent(in)        :: ctx_mesh
    real(kind=8),allocatable       ,intent(inout)     :: array(:,:)
    real(kind=4)                   ,intent(in)        :: xmin,xmax
    real(kind=4)                   ,intent(in)        :: ymin,ymax
    real(kind=4)                   ,intent(in)        :: zmin,zmax
    real(kind=4)                   ,intent(in)        :: rmin,rmax
    real(kind=4)                   ,intent(in)        :: r0(3)
    integer                        ,intent(in)        :: ncells
    type(t_error)                  ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r)
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            array(icell,:) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(2)
        !! loop over all cells
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar2d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell,:) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case(3)
        !! loop over all cells    
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar3d,distr,r)
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            array(icell,:) = 0.0
          end if
        end do
        !$OMP END DO
        !$OMP END PARALLEL
      case default
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
    end select

    return
  end subroutine domain_restriction_array2d_real8


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the points that are outside the 
  !>  given domain.
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_subconstant_array1d_real4(              &
                                dim_domain,ctx_mesh,array,ncells,       &
                                npt_per_cell,                           &
                                xmin,xmax,ymin,ymax,zmin,zmax,rmin,rmax,&
                                r0,ctx_err)

    implicit none

    integer                   ,intent(in)        :: dim_domain
    type(t_domain)            ,intent(in)        :: ctx_mesh
    real(kind=4),allocatable  ,intent(inout)     :: array(:)
    real(kind=4)              ,intent(in)        :: xmin,xmax
    real(kind=4)              ,intent(in)        :: ymin,ymax
    real(kind=4)              ,intent(in)        :: zmin,zmax
    real(kind=4)              ,intent(in)        :: rmin,rmax
    real(kind=4)              ,intent(in)        :: r0(3)
    integer                   ,intent(in)        :: npt_per_cell
    integer                   ,intent(in)        :: ncells
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell,igb1,igb2
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r),     &
    !$OMP&         PRIVATE(coobar2d,coobar3d,igb1,igb2)
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(2)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(3)
      
        !! loop over all cells    
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2) = 0.0
          end if
        end do
        !$OMP END DO
    end select
    !$OMP END PARALLEL
    
    
    if (dim_domain < 1 .or. dim_domain > 3) then
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if

    return

  end subroutine domain_restriction_subconstant_array1d_real4


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the points that are outside the 
  !>  given domain.
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_subconstant_array1d_real8(              &
                                dim_domain,ctx_mesh,array,ncells,       &
                                npt_per_cell,                           &
                                xmin,xmax,ymin,ymax,zmin,zmax,rmin,rmax,&
                                r0,ctx_err)

    implicit none

    integer                   ,intent(in)        :: dim_domain
    type(t_domain)            ,intent(in)        :: ctx_mesh
    real(kind=8),allocatable  ,intent(inout)     :: array(:)
    real(kind=4)              ,intent(in)        :: xmin,xmax
    real(kind=4)              ,intent(in)        :: ymin,ymax
    real(kind=4)              ,intent(in)        :: zmin,zmax
    real(kind=4)              ,intent(in)        :: rmin,rmax
    real(kind=4)              ,intent(in)        :: r0(3)
    integer                   ,intent(in)        :: npt_per_cell
    integer                   ,intent(in)        :: ncells
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell,igb1,igb2
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r),     &
    !$OMP&         PRIVATE(coobar2d,coobar3d,igb1,igb2)
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(2)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(3)
      
        !! loop over all cells    
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2) = 0.0
          end if
        end do
        !$OMP END DO
    end select
    !$OMP END PARALLEL
    
    
    if (dim_domain < 1 .or. dim_domain > 3) then
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if

    return

  end subroutine domain_restriction_subconstant_array1d_real8

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the points that are outside the 
  !>  given domain.
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_subconstant_array2d_real4(              &
                                dim_domain,ctx_mesh,array,ncells,       &
                                npt_per_cell,                           &
                                xmin,xmax,ymin,ymax,zmin,zmax,rmin,rmax,&
                                r0,ctx_err)

    implicit none

    integer                   ,intent(in)        :: dim_domain
    type(t_domain)            ,intent(in)        :: ctx_mesh
    real(kind=4),allocatable  ,intent(inout)     :: array(:,:)
    real(kind=4)              ,intent(in)        :: xmin,xmax
    real(kind=4)              ,intent(in)        :: ymin,ymax
    real(kind=4)              ,intent(in)        :: zmin,zmax
    real(kind=4)              ,intent(in)        :: rmin,rmax
    real(kind=4)              ,intent(in)        :: r0(3)
    integer                   ,intent(in)        :: npt_per_cell
    integer                   ,intent(in)        :: ncells
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell,igb1,igb2
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r),     &
    !$OMP&         PRIVATE(coobar2d,coobar3d,igb1,igb2)
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2,:) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(2)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2,:) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(3)
      
        !! loop over all cells    
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2,:) = 0.0
          end if
        end do
        !$OMP END DO
    end select
    !$OMP END PARALLEL
    
    
    if (dim_domain < 1 .or. dim_domain > 3) then
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if

    return

  end subroutine domain_restriction_subconstant_array2d_real4


  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine sets to zero the points that are outside the 
  !>  given domain.
  !> @details
  !
  !> @param[in]   mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine domain_restriction_subconstant_array2d_real8(              &
                                dim_domain,ctx_mesh,array,ncells,       &
                                npt_per_cell,                           &
                                xmin,xmax,ymin,ymax,zmin,zmax,rmin,rmax,&
                                r0,ctx_err)

    implicit none

    integer                   ,intent(in)        :: dim_domain
    type(t_domain)            ,intent(in)        :: ctx_mesh
    real(kind=8),allocatable  ,intent(inout)     :: array(:,:)
    real(kind=4)              ,intent(in)        :: xmin,xmax
    real(kind=4)              ,intent(in)        :: ymin,ymax
    real(kind=4)              ,intent(in)        :: zmin,zmax
    real(kind=4)              ,intent(in)        :: rmin,rmax
    real(kind=4)              ,intent(in)        :: r0(3)
    integer                   ,intent(in)        :: npt_per_cell
    integer                   ,intent(in)        :: ncells
    type(t_error)             ,intent(out)       :: ctx_err
    !! local 
    integer                     :: icell,igb1,igb2
    real(kind=8)                :: coobar2d(2),coobar3d(3),coobar1d
    real(kind=8)                :: r(3),distr

    ctx_err%ierr=0
    
    !$OMP PARALLEL DEFAULT(shared) PRIVATE(icell,coobar1d,distr,r),     &
    !$OMP&         PRIVATE(coobar2d,coobar3d,igb1,igb2)
    
    select case(dim_domain)
      case(1)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          coobar1d = dble(ctx_mesh%cell_bary(1,icell))
          distr    = abs(coobar1d - r0(1))
          !! set to 0 if outside the box
          if(coobar1d < xmin .or. coobar1d > xmax .or.                  &
             distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2,:) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(2)
        !! loop over all cells
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar2d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r=0.d0
          r(1) = coobar2d(1) - r0(1)
          r(2) = coobar2d(2) - r0(3)
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar2d(1) < xmin .or. coobar2d(1) > xmax .or.           &
              coobar2d(2) < zmin .or. coobar2d(2) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2,:) = 0.0
          end if
        end do
        !$OMP END DO
      
      case(3)
      
        !! loop over all cells    
        !$OMP DO
        do icell=1,ncells
          !! barycentric coordinates
          coobar3d(:) = dble(ctx_mesh%cell_bary(:,icell))
          r    = coobar3d - r0
          distr= norm2(r)
          !! set to 0 if outside the box
          if( coobar3d(1) < xmin .or. coobar3d(1) > xmax .or.           &
              coobar3d(2) < ymin .or. coobar3d(2) > ymax .or.           &
              coobar3d(3) < zmin .or. coobar3d(3) > zmax .or.           &
              distr < rmin .or. distr > rmax) then
            igb1 = (icell-1)*npt_per_cell + 1
            igb2 =    igb1 + npt_per_cell - 1
            array(igb1:igb2,:) = 0.0
          end if
        end do
        !$OMP END DO
    end select
    !$OMP END PARALLEL
    
    
    if (dim_domain < 1 .or. dim_domain > 3) then
        ctx_err%msg   = "** ERROR: Incorrect dimension [domain_restriction]**"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end if

    return

  end subroutine domain_restriction_subconstant_array2d_real8

end module m_domain_restriction
