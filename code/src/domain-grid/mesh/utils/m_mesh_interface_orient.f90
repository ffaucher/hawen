!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_interface.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an api to deal with orientation of cell
!> it depends on the type of cell (i.e. simplex, quadrangles)
!>
!
!------------------------------------------------------------------------------
module m_mesh_interface_orient
  
  use m_raise_error,            only : raise_error, t_error
  use m_define_precision,       only : IKIND_MESH
  use m_mesh_simplex_interface, only : mesh_interface_simplex_orient
  
  implicit none

  private
  public  :: mesh_interface_orient
  
  contains



  !--------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> order and orientation of cell 
  !> it depends on the type of cell (i.e. simplex, quadrangles)
  !
  !> @param[in]    dim_domain      : domain dimension
  !> @param[in]    ncells          : total number of cells
  !> @param[in]    cell_neigh      : array of the cell's neighbor
  !> @param[in]    cell_face       : array of the cell's face ind
  !> @param[in]    index_loctoglob_cell: global cell index
  !> @param[in]    order_cell_array: order per cell
  !> @param[inout] order_face      : face order
  !> @param[inout] orientation     : face orientation
  !> @param[in]    format_cell     : cell format
  !> @param[out]   ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine mesh_interface_orient(dim_domain,ncells,cell_neigh,        &
                                   cell_face,cell_node,ghost_node,      &
                                   index_loctoglob_cell,                &
                                   order_cell_array,order_face,         &
                                   orientation,format_cell,ctx_err)
    
    integer                              ,intent(in)  :: dim_domain
    integer(kind=IKIND_MESH)             ,intent(in)  :: ncells
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_neigh(:,:) 
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_face (:,:) 
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_node (:,:) 
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: ghost_node(:,:) 
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: index_loctoglob_cell(:)
    integer                 ,allocatable ,intent(in)  :: order_cell_array(:)
    integer                 ,allocatable,intent(inout):: order_face (:)
    integer                 ,allocatable,intent(inout):: orientation(:,:)
    character(len=*)                     ,intent(in)  :: format_cell
    type(t_error)                        ,intent(out) :: ctx_err

    ctx_err%ierr=0
    
    !! we search the cell depending on the type of elements
    select case(trim(adjustl(format_cell)))
      !! SIMPLEX cells (triangles or tetrahedron)
      case('simplex','triangle','tetrahedron','segment')
        call mesh_interface_simplex_orient(dim_domain,ncells,cell_neigh,&
                                           cell_face,cell_node,         &
                                           ghost_node,                  &
                                           index_loctoglob_cell,        &
                                           order_cell_array,order_face, &
                                           orientation,ctx_err)
      
      !! unrecognized type of cell
      case default
        ctx_err%msg   = "** ERROR: format of the cell not "   // &
                        "recognized in [mesh_interface_orient] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select  
    !! -----------------------------------------------------------------
    return
  end subroutine mesh_interface_orient
 
end module m_mesh_interface_orient
