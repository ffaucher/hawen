!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_find_cell.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used as an api to detect the cell in which belong the 
!> given point: it depends on the type of cell (i.e. simplex, quadrangles)
!>
!>  ********************************************************************
!>  ********** WARNING: the routine uses OPENMP OMP parallelisme
!>  **********          to loop over all cells
!>  ********************************************************************
!
!------------------------------------------------------------------------------
module m_find_cell
  
  use m_raise_error,        only : raise_error, t_error
  use m_ctx_parallelism,    only : t_parallelism
  use m_allreduce_min,      only : allreduce_min
  use m_allreduce_sum,      only : allreduce_sum
  use m_define_precision,   only : IKIND_MESH,RKIND_MESH
  use m_mesh_simplex_find_cell, only : find_cell_simplex
  
  implicit none

  private
  public  :: find_cell
  
  contains



  !--------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The interface finds the cell in which the given point is
  !>  we need to deal with 2D or 3D mesh, as well as shape of the
  !>  element (simplex, quadrangles)
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[in]   coo_input     : coordinates of the position to look for
  !> @param[out]  cell          : output cell number
  !> @param[in]   x_node        : list of node coordinates for all cells
  !> @param[in]   cell_node     : list of node for all cells
  !> @param[in]   index_glob    : list of global indexes for the cell
  !> @param[in]   n_cell        : number of local cells to search
  !> @param[in]   n_cell        : number of global cell in mesh
  !> @param[in]   dim_domain    : domain dimension
  !> @param[in]   cell_neigh    : 2D array containing the neighbors
  !> @param[in]   format_cell   : format of the cell (e.g. simplex)
  !> @param[in]   mpi_check     : check that we have only one value among
  !>                              the processors
  !> @param[out]  ctx_err       : error context
  !---------------------------------------------------------------------
  subroutine find_cell(ctx_paral,coo_input,cell,x_node,cell_node,       &
                       index_loctoglob_cell,n_cell,n_cell_gb,dim_domain,&
                       cell_neigh,format_cell,flag_mpi_check,ctx_err)
    
    type(t_parallelism)                  ,intent(in)  :: ctx_paral
    real   (kind=8)                      ,intent(in)  :: coo_input(:)
    integer(kind=IKIND_MESH)             ,intent(out) :: cell
    real   (kind=RKIND_MESH),allocatable ,intent(in)  :: x_node   (:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_node(:,:)
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: index_loctoglob_cell(:)
    integer(kind=IKIND_MESH)             ,intent(in)  :: n_cell,n_cell_gb
    integer                              ,intent(in)  :: dim_domain
    integer(kind=IKIND_MESH),allocatable ,intent(in)  :: cell_neigh(:,:)
    character(len=*)                     ,intent(in)  :: format_cell
    logical                              ,intent(in)  :: flag_mpi_check
    type(t_error)                        ,intent(out) :: ctx_err
    !! local 
    integer                   :: counter, count_gb
    integer(kind=IKIND_MESH)  :: index_global, index_min

    !! *****************************************************************
    !! ********** WARNING: the routine uses OPENMP OMP parallelisme
    !! **********          to loop over all cells
    !! *****************************************************************
    !! we search the cell depending on the type of elements
    select case(trim(adjustl(format_cell)))
      !! SIMPLEX cells (triangles or tetrahedron)
      case('simplex')
        cell = 0 
        call find_cell_simplex(coo_input,cell,x_node,cell_node,n_cell,  &
                               dim_domain,cell_neigh,ctx_err)
      
      !! unrecognized type of cell
      case default
        ctx_err%msg   = "** ERROR: format of the cell not "   // &
                        "recognized in [find_cell] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    !! -----------------------------------------------------------------
    !! if the FLAG_MPI_CHECK is on:
    !! >>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !! this should be done when all processors are looking for the same 
    !! positions, it makes sure that there is one and only one cell: 
    !! -> in the case where several processors have found a cell, only 
    !!    the one with the smallest global cell index is kept.
    !! -> If none of the processors have found the cell, it wil rise an
    !!    error.
    !! -----------------------------------------------------------------
    if(flag_mpi_check) then

      if(cell .ne. 0) then
        counter = 1
      else
        counter = 0
      endif
      !! merge in case of multiproc
      call allreduce_sum(counter,count_gb,1,ctx_paral%communicator,ctx_err)

      !! ---------------------------------------------------------------
      !! in case we have many, we need to retrieve the smallest cell
      !! in the global indexing
      if(count_gb > 1) then
        if(counter > 0) then
          index_global = index_loctoglob_cell(cell)
        else 
          index_global = n_cell_gb + 1
        endif
        !! get the minimal of all, only the minimal one is kept 
        call allreduce_min(index_global,index_min,1,                    &
                           ctx_paral%communicator,ctx_err)
        if(index_global .ne. index_min) then
          cell = 0
        endif
      
      !! if the value has not been found, return an error
      else if(count_gb .eq. 0) then
        ctx_err%msg   = "** ERROR: no cell has been found [find_cell] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      endif
      !! ---------------------------------------------------------------
    end if
    
    !! -----------------------------------------------------------------
    return
  end subroutine find_cell
 
end module m_find_cell
