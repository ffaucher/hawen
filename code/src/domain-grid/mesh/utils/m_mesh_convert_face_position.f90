!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_convert_face_position.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to convert the position on the face of the reference
!> element to the global position on the mesh. For instance in dimension 2,
!> the input local position is 1D while the global will be 2D. Similarly, in
!> 3D we start from the 2D position on a face, that we convert to 3D.
!>
!
!------------------------------------------------------------------------------
module m_mesh_convert_face_position
  
  use m_raise_error,            only : t_error
  use m_mesh_simplex_interface, only: interface_2D_triangle,            &
                                      interface_3D_tetrahedron
  
  implicit none

  private
  
  !! Interfaces ________________________________________________________
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine finds the global position on the mesh from the local
  !>  position on a face of an element.
  interface mesh_convert_face_position_2D
     module procedure mesh_convert_face_position_2D_multi
     module procedure mesh_convert_face_position_2D_solo
  end interface mesh_convert_face_position_2D
  interface mesh_convert_face_position_3D
     module procedure mesh_convert_face_position_3D_multi
     module procedure mesh_convert_face_position_3D_solo
  end interface mesh_convert_face_position_3D
  !--------------------------------------------------------------------- 

  public  :: mesh_convert_face_position_2D
  public  :: mesh_convert_face_position_3D
  
  contains



  !--------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> find the global position from the local position on a face 
  !> in 2D.
  !
  !> @param[in]    node_coo         : coordinates of the 3 nodes of the cell size(2,3)
  !> @param[in]    iface            : face index to consider
  !> @param[in]    inv_jac          : determinent of the cell jacobian
  !> @param[in]    xpos_face        : 1D position on the face to convert
  !> @param[in]    xpt_gb           : global 2D position on the mesh
  !> @param[in]    xpt_loc          : local  2D position on the ref. elem.
  !> @param[out]   ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine mesh_convert_face_position_2D_solo(node_coo,iface,inv_jac, &
                                                xpos_face,              &
                                                xpt_gb,xpt_loc,ctx_err)
    
    real(kind=8)            ,intent(in)    :: node_coo(2,3)
    integer(kind=4)         ,intent(in)    :: iface
    real(kind=8)            ,intent(in)    :: inv_jac(2,2)
    real(kind=8)            ,intent(in)    :: xpos_face
    real(kind=8)            ,intent(out)   :: xpt_gb (2)
    real(kind=8)            ,intent(out)   :: xpt_loc(2)
    type(t_error)           ,intent(out)   :: ctx_err
    !! local
    real(kind=8)        :: xnode1(2),xnode2(2)
    real(kind=8)        :: a1,b1,a2,b2

    ctx_err%ierr=0
    xpt_gb  = 0.d0
    xpt_loc = 0.d0

    if(xpos_face > 1 .or. xpos_face < 0) then
      ctx_err%msg   = "** ERROR: The 1D position on reference face is "//&
                      " not between 0 and 1 [find_position_on_face] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return !! non-shared error
    end if

    !! the two nodes that compose the considered face.
    xnode1(:) = node_coo(:,interface_2D_triangle(1,iface))
    xnode2(:) = node_coo(:,interface_2D_triangle(2,iface))

    !! -----------------------------------------------------------------
    !! say xnode1 => (0)
    !! say xnode2 => (1)
    !! we have a1 xhat + b1 = x
    !!         a2 xhat + b2 = y
    !! ----------------------------------
    !! a1 0 + b1 = x1
    !! a1 1 + b1 = x2
    !! a2 0 + b2 = y1
    !! a2 1 + b2 = y2
    !! ----------------------------------
    b1 = xnode1(1) ; b2 = xnode1(2) ; 
    a1 = xnode2(1) - b1 ; 
    a2 = xnode2(2) - b2 ; 
    !! -----------------------------------------------------------------
    
    !! global position along the face.
    xpt_gb(1) = a1*xpos_face + b1
    xpt_gb(2) = a2*xpos_face + b2
    !! local position on the reference element
    xpt_loc = matmul(inv_jac,dble(xpt_gb - node_coo(:,1)))


    return
  end subroutine mesh_convert_face_position_2D_solo


  !--------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> find the global position from the local position on a face 
  !> in 2D.
  !
  !> @param[in]    node_coo         : coordinates of the 3 nodes of the cell size(2,3)
  !> @param[in]    iface            : face index to consider
  !> @param[in]    inv_jac          : determinent of the cell jacobian
  !> @param[in]    xpos_face        : 1D position on the face to convert
  !> @param[in]    npt              : number of points to treat
  !> @param[in]    xpt_gb           : global 2D position on the mesh
  !> @param[in]    xpt_loc          : local  2D position on the ref. elem.
  !> @param[out]   ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine mesh_convert_face_position_2D_multi(node_coo,iface,inv_jac, &
                                                 xpos_face,npt,          &
                                                 xpt_gb,xpt_loc,ctx_err)
    
    real(kind=8)            ,intent(in)    :: node_coo(2,3)
    integer(kind=4)         ,intent(in)    :: iface
    real(kind=8)            ,intent(in)    :: inv_jac(2,2)
    real(kind=8)            ,intent(in)    :: xpos_face(:)
    integer(kind=4)         ,intent(in)    :: npt
    real(kind=8)            ,intent(out)   :: xpt_gb (:,:)
    real(kind=8)            ,intent(out)   :: xpt_loc(:,:)
    type(t_error)           ,intent(out)   :: ctx_err
    !! local
    integer             :: k
    real(kind=8)        :: xnode1(2),xnode2(2)
    real(kind=8)        :: a1,b1,a2,b2

    ctx_err%ierr=0
    xpt_gb  = 0.d0
    xpt_loc = 0.d0

    if(maxval(xpos_face) > 1 .or. minval(xpos_face) < 0) then
      ctx_err%msg   = "** ERROR: The 1D position on reference face is "//&
                      " not between 0 and 1 [find_position_on_face] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return !! non-shared error
    end if

    !! the two nodes that compose the considered face.
    xnode1(:) = node_coo(:,interface_2D_triangle(1,iface))
    xnode2(:) = node_coo(:,interface_2D_triangle(2,iface))

    !! -----------------------------------------------------------------
    !! say xnode1 => (0)
    !! say xnode2 => (1)
    !! we have a1 xhat + b1 = x
    !!         a2 xhat + b2 = y
    !! ----------------------------------
    !! a1 0 + b1 = x1
    !! a1 1 + b1 = x2
    !! a2 0 + b2 = y1
    !! a2 1 + b2 = y2
    !! ----------------------------------
    b1 = xnode1(1) ; b2 = xnode1(2) ; 
    a1 = xnode2(1) - b1 ; 
    a2 = xnode2(2) - b2 ; 
    !! -----------------------------------------------------------------
    
    do k=1,npt
      !! global position along the face.
      xpt_gb(1,k) = a1*xpos_face(k) + b1
      xpt_gb(2,k) = a2*xpos_face(k) + b2
      !! local position on the reference element
      xpt_loc(:,k) = matmul(inv_jac,dble(xpt_gb(:,k) - node_coo(:,1)))
    end do

    return
  end subroutine mesh_convert_face_position_2D_multi



  !--------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> find the global position from the local position on a face in 3D.
  !
  !> @param[in]    node_coo         : coordinates of the 3 nodes of the cell size(2,3)
  !> @param[in]    iface            : face index to consider
  !> @param[in]    inv_jac          : determinent of the cell jacobian
  !> @param[in]    xpos_face        : 2D position on the face to convert
  !> @param[in]    xpt_gb           : global 3D position on the mesh
  !> @param[in]    xpt_loc          : local  3D position on the ref. elem.
  !> @param[out]   ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine mesh_convert_face_position_3D_solo(node_coo,iface,inv_jac, &
                                                xpos_face,              &
                                                xpt_gb,xpt_loc,ctx_err)
    
    real(kind=8)            ,intent(in)    :: node_coo(3,4)
    integer(kind=4)         ,intent(in)    :: iface
    real(kind=8)            ,intent(in)    :: inv_jac(3,3)
    real(kind=8)            ,intent(in)    :: xpos_face(2)
    real(kind=8)            ,intent(out)   :: xpt_gb (3)
    real(kind=8)            ,intent(out)   :: xpt_loc(3)
    type(t_error)           ,intent(out)   :: ctx_err
    !! local
    real(kind=8)        :: xnode1(3),xnode2(3),xnode3(3)
    real(kind=8)        :: a1,b1,c1,a2,b2,c2,a3,b3,c3

    ctx_err%ierr=0
    xpt_gb  = 0.d0
    xpt_loc = 0.d0

    if(maxval(xpos_face) > 1 .or. minval(xpos_face) < 0) then
      ctx_err%msg   = "** ERROR: The 2D position on reference face is "//&
                      " not between 0 and 1 [find_position_on_face] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return !! non-shared error
    end if


    !! the three nodes that compose the considered face.
    xnode1(:) = node_coo(:,interface_3D_tetrahedron(1,iface))
    xnode2(:) = node_coo(:,interface_3D_tetrahedron(2,iface))
    xnode3(:) = node_coo(:,interface_3D_tetrahedron(3,iface))

    !! ----------------------------------
    !! say xnode1 => (0,0)
    !! say xnode2 => (0,1)
    !! say xnode3 => (1,0)
    !! we have a1 xhat + b1 yhat + c1 = x
    !!         a2 xhat + b2 yhat + c2 = y
    !!         a3 xhat + b3 yhat + c3 = z
    !! ----------------------------------
    !! a1 0 + b1 0 + c1 = x1
    !! a1 0 + b1 1 + c1 = x2
    !! a1 1 + b1 0 + c1 = x3
    !! ----------------------------------
    c1 = xnode1(1) ; c2 = xnode1(2) ; c3 = xnode1(3) ; 
    a1 = xnode3(1) - c1 
    b1 = xnode2(1) - c1 
    a2 = xnode3(2) - c2 
    b2 = xnode2(2) - c2 
    a3 = xnode3(3) - c3 
    b3 = xnode2(3) - c3 
    
    !! global positions
    xpt_gb(1) = a1*xpos_face(1) + b1*xpos_face(2) + c1
    xpt_gb(2) = a2*xpos_face(1) + b2*xpos_face(2) + c2
    xpt_gb(3) = a3*xpos_face(1) + b3*xpos_face(2) + c3
    xpt_loc = matmul(inv_jac,dble(xpt_gb - node_coo(:,1)))

    return
  end subroutine mesh_convert_face_position_3D_solo

  !--------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> find the global position from the local position on a face in 3D.
  !
  !> @param[in]    node_coo         : coordinates of the 3 nodes of the cell size(2,3)
  !> @param[in]    iface            : face index to consider
  !> @param[in]    inv_jac          : determinent of the cell jacobian
  !> @param[in]    xpos_face        : 2D position on the face to convert
  !> @param[in]    npt              : number of points to treat
  !> @param[in]    xpt_gb           : global 3D position on the mesh
  !> @param[in]    xpt_loc          : local  3D position on the ref. elem.
  !> @param[out]   ctx_err          : error context
  !----------------------------------------------------------------------------
  subroutine mesh_convert_face_position_3D_multi(node_coo,iface,inv_jac,&
                                                xpos_face,npt,          &
                                                xpt_gb,xpt_loc,ctx_err)
    
    real(kind=8)            ,intent(in)    :: node_coo(3,4)
    integer(kind=4)         ,intent(in)    :: iface
    real(kind=8)            ,intent(in)    :: inv_jac(3,3)
    real(kind=8)            ,intent(in)    :: xpos_face(:,:)
    integer(kind=4)         ,intent(in)    :: npt
    real(kind=8)            ,intent(out)   :: xpt_gb (:,:)
    real(kind=8)            ,intent(out)   :: xpt_loc(:,:)
    type(t_error)           ,intent(out)   :: ctx_err
    !! local
    integer             :: k
    real(kind=8)        :: xnode1(3),xnode2(3),xnode3(3)
    real(kind=8)        :: a1,b1,c1,a2,b2,c2,a3,b3,c3

    ctx_err%ierr=0
    xpt_gb  = 0.d0
    xpt_loc = 0.d0

    if(maxval(xpos_face) > 1 .or. minval(xpos_face) < 0) then
      ctx_err%msg   = "** ERROR: The 2D position on reference face is "//&
                      " not between 0 and 1 [find_position_on_face] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return !! non-shared error
    end if


    !! the three nodes that compose the considered face.
    xnode1(:) = node_coo(:,interface_3D_tetrahedron(1,iface))
    xnode2(:) = node_coo(:,interface_3D_tetrahedron(2,iface))
    xnode3(:) = node_coo(:,interface_3D_tetrahedron(3,iface))

    !! ----------------------------------
    !! we have
    !!    xnode1 => (0,0)
    !!    xnode2 => (1,0)
    !!    xnode3 => (0,1)
    !! we have a1 xhat + b1 yhat + c1 = x
    !!         a2 xhat + b2 yhat + c2 = y
    !!         a3 xhat + b3 yhat + c3 = z
    !! ----------------------------------
    !! for instance; 
    !! a1 0 + b1 0 + c1 = x(node1)
    !! a1 1 + b1 0 + c1 = x(node2)
    !! a1 0 + b1 1 + c1 = x(node3)
    !! ----------------------------------
    !! using node1 = (0,0) gives constants c
    c1 = xnode1(1)
    c2 = xnode1(2)
    c3 = xnode1(3)
    
    !! using node2 = (1,0) gives constants a
    a1 = xnode2(1) - c1 
    a2 = xnode2(2) - c2 
    a3 = xnode2(3) - c3 
    
    !! using node3 = (0,1) gives constants b
    b1 = xnode3(1) - c1 
    b2 = xnode3(2) - c2 
    b3 = xnode3(3) - c3 
    
    !! global positions
    do k=1,npt
      xpt_gb(1,k) = a1*xpos_face(1,k) + b1*xpos_face(2,k) + c1
      xpt_gb(2,k) = a2*xpos_face(1,k) + b2*xpos_face(2,k) + c2
      xpt_gb(3,k) = a3*xpos_face(1,k) + b3*xpos_face(2,k) + c3
      xpt_loc(:,k)= matmul(inv_jac,dble(xpt_gb(:,k) - node_coo(:,1)))
    end do

    return
  end subroutine mesh_convert_face_position_3D_multi
  
end module m_mesh_convert_face_position
