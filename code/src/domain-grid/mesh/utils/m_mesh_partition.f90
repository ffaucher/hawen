!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_mesh_partition.f90
!
!> @author
!> J. Diaz & F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to partition the domain mesh upon all processors
!
!------------------------------------------------------------------------------
module m_mesh_partition

  use omp_lib  
  use m_define_precision,     only : IKIND_MESH, RKIND_MESH,            &
                                     IKIND_METIS, RKIND_METIS
  use m_raise_error,          only : raise_error, t_error
  use m_distribute_error,     only : distribute_error
  use m_time,                 only : time
  use m_broadcast,            only : broadcast
  use m_allreduce_sum,        only : allreduce_sum
  use m_ctx_domain,           only : t_domain
  use m_tag_bc,               only : tag_ghost_cell
  use m_mesh_find_index_cell, only : mesh_find_index_cell
  use m_mesh_interface_orient,only : mesh_interface_orient
  use m_ctx_parallelism,      only : t_parallelism
  use m_receive,              only : receive
  use m_send,                 only : send
  use m_barrier,              only : barrier
  use m_wait,                 only : wait

  implicit none

  private  
  public :: mesh_partition
    
  contains  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine distribute the global mesh to all processors
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_partition(ctx_paral,ctx_mesh,ctx_err)

    implicit none
    type(t_parallelism),intent(in)        :: ctx_paral
    type(t_domain)     ,intent(inout)     :: ctx_mesh
    type(t_error)      ,intent(out)       :: ctx_err
    !! local 
    integer,  allocatable :: proc_cell(:)
    
    !! -----------------------------------------------------------------
    !! we split the mesh only if multi-processors ----------------------
    if(ctx_paral%nproc .ne. 1) then
      !! only master has the mapping, which depends on the partitioner
      if(ctx_paral%master) allocate(proc_cell(ctx_mesh%n_cell_gb))   
      select case(trim(adjustl(ctx_mesh%partitioner)))
        case('metis') !! metis partitioner
          call mesh_partition_metis(ctx_paral,ctx_mesh,proc_cell,ctx_err)

        case default  !! unrecognized partitioner
          ctx_err%msg   = "** MESH PARTITIONER : "                    // &
                          trim(adjustl(ctx_mesh%partitioner))         // &
                          " NOT RECOGNIZED ** " // &
                          "=> ONLY METIS IS SUPPORTED [m_mesh_partition]"
          ctx_err%ierr  = -1
          ctx_err%critic=.true.
          call raise_error(ctx_err)

      end select
    endif
    !! -----------------------------------------------------------------
    !! -- from the cell / procs correspondance we send the infos 
    call mesh_partition_send(ctx_paral,ctx_mesh,proc_cell,ctx_err)
    if(ctx_err%ierr .ne. 0) call raise_error(ctx_err)

    !! clean
    if(allocated(proc_cell)) deallocate(proc_cell)
        
    return
  end subroutine mesh_partition

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine uses metis to assign to each cell the processor number 
  !>  in the array correspondance
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[inout] correspondance: correspondance proc/cells
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_partition_metis(ctx_paral,ctx_mesh,correspondance,ctx_err)

    implicit none
    type(t_parallelism)                  ,intent(in)   :: ctx_paral
    type(t_domain)                       ,intent(inout):: ctx_mesh
    type(t_error)                        ,intent(out)  :: ctx_err
    integer,                  allocatable,intent(inout):: correspondance(:)
    !! local time
    real   (kind=8)      :: time0,time1
    !! local metis
    integer(kind=IKIND_MESH)               :: k
    integer(kind=IKIND_METIS) ,allocatable :: cell_node_counter(:)
    integer(kind=IKIND_METIS) ,allocatable :: connectivity(:)
    integer(kind=IKIND_METIS), allocatable :: ndof_per_c(:)
    integer(kind=IKIND_METIS), allocatable :: npart(:)
    integer                                :: ninter_per_c
    integer(kind=IKIND_METIS)              :: nproc,ncommon,volume
    integer(kind=IKIND_METIS)              :: metis_ncell,metis_nnode
    integer(kind=IKIND_METIS), allocatable :: metis_corres(:)

    integer(kind=IKIND_METIS), pointer    :: moption(:)     !=>null() (40 options from metis.h)
    real   (kind=RKIND_METIS), pointer    :: part_weight(:) !=>null()
    integer(kind=IKIND_METIS), pointer    :: weight(:)      !=>null()

    
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    call time(time0)
    !! npartition for metis
    nproc=int(ctx_paral%nproc,kind=IKIND_METIS)

    !! only master has the global mesh for now, it will call for the 
    !! partitioner, treat the results and send to the concerned processor
    ! -------------------------------------------------------------------
    if(ctx_paral%nproc .ne. 1) then !! useless in monoprocessor
      !! metis partitioner
      if(ctx_paral%master) then !! only the master works for now

        !! Number of common node to have an interface between two elements
        ncommon = int(ctx_mesh%n_node_per_face_shared,kind=IKIND_METIS)
        !! number of interface per cell = number of neighbors
        ninter_per_c=ctx_mesh%n_neigh_per_cell
        !! the number of nodes per cell: cell_node_counter
        allocate(cell_node_counter(ctx_mesh%n_cell_gb+1))
        !! number of interface per cell = number of neighbors
        allocate(connectivity     (ctx_mesh%n_cell_gb*ninter_per_c))
        allocate(ndof_per_c       (ctx_mesh%n_cell_gb))
        allocate(npart            (ctx_mesh%n_node_gb))
        !weight     =1./ctx_paral%nproc ; part_weight=1./ctx_paral%nproc
        
        !! starting with zero for metis
        cell_node_counter=0
        do k=1,ctx_mesh%n_cell_gb+1
          cell_node_counter(k) = int(ninter_per_c*(k-1),kind=IKIND_METIS)
        enddo
        do k=1,ctx_mesh%n_cell_gb
          !! careful: minus 1 because metis starts at 0
          connectivity(ninter_per_c*(k-1)+1:ninter_per_c*k) = &
                         int(ctx_mesh%cell_node(:,k)-1,kind=IKIND_METIS)
          !! number of degrees of freedom per cell
          !! 2 dimensions dof ==> (order + 1)*(order)/2
          !! we do not need to know the number of solution here, it 
          !! is the same for all cell anyway.
          if(ctx_mesh%dim_domain == 1) then
            ndof_per_c(k)=int((ctx_mesh%order(k)+1), kind=IKIND_METIS)
          elseif(ctx_mesh%dim_domain == 2) then
            ndof_per_c(k)=int(((ctx_mesh%order(k)+2)*                   &
                               (ctx_mesh%order(k)+1))/2, kind=IKIND_METIS)
          else
            ndof_per_c(k)=int(((ctx_mesh%order(k)+3)*                   &
                               (ctx_mesh%order(k)+2)*                   &
                               (ctx_mesh%order(k)+1))/6, kind=IKIND_METIS)
          end if
        enddo
        !! multiply by the number of solutions
        ndof_per_c=ndof_per_c
        
        !! initialize the Metis inputs: 
        !!   - the options are by default 
        !!   - the weight is constant
        !!   - the part_weight gives the same weight to all cpus.
        allocate(weight     (ctx_mesh%n_cell_gb))
        allocate(part_weight(0:nproc-1))
        allocate(moption    (40))
        weight     = 1
        part_weight(0:nproc-1)= real(1.d0/dble(nproc), kind=4)
        call METIS_SetDefaultOptions(moption);
        
        !! decomposition of the mesh, using the kind of metis 
        metis_ncell = int(ctx_mesh%n_cell_gb, kind=IKIND_METIS)
        metis_nnode = int(ctx_mesh%n_node_gb, kind=IKIND_METIS)
        
        
        allocate(metis_corres(ctx_mesh%n_cell_gb))
        call METIS_PartMeshDual(metis_ncell,        & !! nelement
                                metis_nnode,        & !! nnodes
                                cell_node_counter,  & !! array nnode per cell
                                connectivity,       & !! cell connectivity
                                ndof_per_c,         & !! weight per cell
                                weight,             & !! weight for communicator
                                ncommon,            & !! ncommon node
                                nproc,              & !! number of parts
                                part_weight,        & !! weigh of partition
                                moption,            & !! metis option
                                volume,             & !! com volume
                                metis_corres,       & !! final correspondance
                                                      !! between cell and rank
                                npart)                !! partition vector
         
        !! deallocate the useless
        deallocate(cell_node_counter)
        deallocate(connectivity)
        deallocate(ndof_per_c)
        deallocate(npart)

        !! clean using Metis wrapper for the arrays.
        ! call METIS_Free(moption)
        if(associated(moption))  then
          deallocate(moption,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(moption)
        end if
        if(associated(weight)) then
          deallocate(weight,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(weight)
        end if
        if(associated(part_weight  )) then
          deallocate(part_weight  ,stat=ctx_err%ierr)
          if(ctx_err%ierr .ne. 0) nullify(part_weight)
        end if
        !! we do not really care for error in deallocation
        ctx_err%ierr = 0


        correspondance = int(metis_corres, kind=4)
        deallocate(metis_corres)
      endif !! only master 

    endif !! only when multi-processors

    call time(time1)
    ctx_mesh%time_partition = (time1-time0)
    return
  end subroutine mesh_partition_metis
  
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine distribute the global mesh to processors followin the 
  !>  mapping contained in proc_cell
  !> @details
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] mesh         : mesh context
  !> @param[in]   proc_cell     : correspondance proc/cells
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_partition_send(ctx_paral,ctx_mesh,proc_cell,ctx_err)

    implicit none
    type(t_parallelism)                  ,intent(in)   :: ctx_paral
    type(t_domain)                       ,intent(inout):: ctx_mesh
    integer,                  allocatable,intent(in)   :: proc_cell(:)
    type(t_error)                        ,intent(out)  :: ctx_err
    
    !! local time
    real   (kind=8)      :: time0,time1
    !! master only
    integer(kind=IKIND_MESH), allocatable :: cells_pproc       (:)
    integer(kind=IKIND_MESH), allocatable :: ghost_pproc       (:)
    integer(kind=IKIND_MESH), allocatable :: offset_cells      (:)
    integer(kind=IKIND_MESH), allocatable :: offset_ghost      (:)
    integer(kind=IKIND_MESH), allocatable :: counter_cells     (:)
    integer(kind=IKIND_MESH), allocatable :: counter_ghost     (:)
    integer(kind=IKIND_MESH), allocatable :: ordered_cellindex (:)
    integer(kind=IKIND_MESH), allocatable :: ordered_ghostindex(:)
    integer(kind=IKIND_MESH), allocatable :: ordered_cellnode  (:,:)
    integer(kind=IKIND_MESH), allocatable :: ordered_cellneigh (:,:)
    integer(kind=IKIND_MESH), allocatable :: ordered_cellface  (:,:)
    real   (kind=RKIND_MESH), allocatable :: ordered_bary      (:,:)
    real   (kind=RKIND_MESH), allocatable :: ordered_ghostbary (:,:)
    integer,                  allocatable :: ordered_cellorder (:)
    integer,                  allocatable :: ordered_ghostorder(:)
    integer(kind=IKIND_MESH), allocatable :: array_itemp(:,:)
    integer(kind=IKIND_MESH), allocatable :: local_cell_face(:,:)
    !!
    integer(kind=IKIND_MESH), allocatable :: index_loctoglob_node(:)
    integer(kind=IKIND_MESH), allocatable :: global_cell_node    (:,:)
    real   (kind=RKIND_MESH), allocatable :: temp_xnode(:,:)
    
    integer                               :: i_proc,tag,k
    integer(kind=IKIND_MESH)              :: i_cell,i_neigh,neigh_cell,i_node
    integer(kind=IKIND_MESH)              :: j_cell,j_neigh,my_ncell,inodegb
    integer(kind=IKIND_MESH)              :: i_ghost,j_node
    integer(kind=IKIND_MESH)              :: nghost_tot,my_nghost,n_node_temp
    integer(kind=IKIND_MESH)              :: icstart,icend,localind,neigh
    integer(kind=IKIND_MESH)              :: count_interface,i_face,i_cellgb
    integer(kind=IKIND_MESH)              :: inodegb_ghost,i_cellgb_ghost

    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    call time(time0)
    !! useless in monoprocessor ----------------------------------------
    if(ctx_paral%nproc .ne. 1) then
      if(ctx_paral%master) then

        !! compute the barycenter for all cells ------------------------
        allocate(ctx_mesh%cell_bary(ctx_mesh%dim_domain,ctx_mesh%n_cell_gb))
        !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,k)
        !$OMP DO
        do i_cell=1,ctx_mesh%n_cell_gb
          do k=1,ctx_mesh%dim_domain
            ctx_mesh%cell_bary(k,i_cell) = sum(ctx_mesh%x_node(k,       &
                                           ctx_mesh%cell_node(:,i_cell)))
          end do
        end do
        !$OMP END DO
        !$OMP END PARALLEL
        ctx_mesh%cell_bary = real(dble(ctx_mesh%cell_bary)/             &
                                  dble(ctx_mesh%n_node_per_cell),kind=RKIND_MESH)
        !! -------------------------------------------------------------

        !! count how many cell per per processors and organize infos in
        !! ordered_cellindex loc to glob 
        !! ordered_cellnode 
        !! ordered_cellneigh
        !! ordered_cellface
        !! ordered_cellorder
        !! ordered_node coordinates
        !! ordered_ghostorder
        !! ordered_ghostindex
        !! ordered_barycenters
        !! -------------------------------------------------------------
        allocate(cells_pproc       (ctx_paral%nproc))
        allocate(ghost_pproc       (ctx_paral%nproc))
        allocate(ordered_cellindex (ctx_mesh%n_cell_gb))
        allocate(ordered_cellnode  (ctx_mesh%n_node_per_cell ,ctx_mesh%n_cell_gb))
        allocate(ordered_cellneigh (ctx_mesh%n_neigh_per_cell,ctx_mesh%n_cell_gb))
        allocate(ordered_cellface  (ctx_mesh%n_neigh_per_cell,ctx_mesh%n_cell_gb))
        allocate(ordered_cellorder (ctx_mesh%n_cell_gb))
        allocate(ordered_bary      (ctx_mesh%dim_domain,ctx_mesh%n_cell_gb))
        allocate(offset_cells      (ctx_paral%nproc+1))
        allocate(offset_ghost      (ctx_paral%nproc+1))
        allocate(counter_cells     (ctx_paral%nproc))
        allocate(counter_ghost     (ctx_paral%nproc))

        cells_pproc=0 ; ghost_pproc=0
        !! -------------------------------------------------------------
        !! count number of cells and ghost per processors
        do i_cell=1,ctx_mesh%n_cell_gb
          i_proc=proc_cell(i_cell)+1 !! +1 because processor rank from 0
          cells_pproc(i_proc)=cells_pproc(i_proc)+1

          !! check ghost and update the tag if ghost
          do i_neigh=1,ctx_mesh%n_neigh_per_cell
            neigh_cell=ctx_mesh%cell_neigh(i_neigh,i_cell)
            if(neigh_cell > 0) then
              if(proc_cell(neigh_cell) .ne. proc_cell(i_cell)) then
                ghost_pproc(i_proc) = ghost_pproc(i_proc)+1
              endif
            endif
          enddo
        enddo
        nghost_tot = sum(ghost_pproc)
        !! offset infos for ordering
        offset_cells(:) = 0
        offset_ghost(:) = 0
        do i_proc=1,ctx_paral%nproc
          offset_cells(i_proc + 1) = offset_cells(i_proc) + cells_pproc(i_proc)
          offset_ghost(i_proc + 1) = offset_ghost(i_proc) + ghost_pproc(i_proc)
        enddo
        !! -------------------------------------------------------------

        !! -------------------------------------------------------------
        !! organize the informations using 
        !! the loop is fast, no need for open mp parallelization ?
        allocate(ordered_ghostorder(nghost_tot))
        allocate(ordered_ghostindex(nghost_tot))
        allocate(ordered_ghostbary (ctx_mesh%dim_domain,nghost_tot))
        counter_cells=0 ; counter_ghost=0
        do i_cell=1,ctx_mesh%n_cell_gb
          i_proc=proc_cell(i_cell)+1
          counter_cells(i_proc) = counter_cells(i_proc) + 1
          !! local cell index for the proc
          j_cell = offset_cells(i_proc) + counter_cells(i_proc)
          !! ordered infos
          ordered_cellindex(j_cell)  = i_cell !! global index of the cell
          ordered_cellorder(j_cell)  = ctx_mesh%order     (i_cell)
          ordered_cellneigh(:,j_cell)= ctx_mesh%cell_neigh(:,i_cell)
          ordered_cellface (:,j_cell)= ctx_mesh%cell_face (:,i_cell)
          ordered_cellnode (:,j_cell)= ctx_mesh%cell_node (:,i_cell)
          ordered_bary     (:,j_cell)= ctx_mesh%cell_bary (:,i_cell)
          
          !! check ghost and update the tag if ghost
          do i_neigh=1,ctx_mesh%n_neigh_per_cell
            neigh_cell=ctx_mesh%cell_neigh(i_neigh,i_cell)
            if(neigh_cell > 0) then
              if(proc_cell(neigh_cell) .ne. proc_cell(i_cell)) then
                counter_ghost(i_proc)=counter_ghost(i_proc) + 1
                j_neigh = offset_ghost(i_proc) + counter_ghost(i_proc)
                ordered_ghostorder(j_neigh)   = ctx_mesh%order(neigh_cell)
                ordered_ghostindex(j_neigh)   = neigh_cell
                ordered_ghostbary (:,j_neigh) = ctx_mesh%cell_bary(:,neigh_cell)
                ordered_cellneigh (i_neigh,j_cell) = tag_ghost_cell
              endif
            endif
          enddo !! end do for ghost neighbor 
        enddo  !! end do for ordering all cells infos

      endif !! only master is ordering the information
      
      !! ---------------------------------------------------------------
      !! we first send the infos regarding the number of cells and ghost
      !! ---------------------------------------------------------------
      if(ctx_paral%master) then
        my_ncell =cells_pproc (1)
        my_nghost=ghost_pproc (1)
        !! send infos to processors: the tag is the processor number
        do i_proc=1,ctx_paral%nproc-1
          tag=i_proc
          call send(cells_pproc(i_proc+1),1,i_proc,tag,   &
                    ctx_paral%communicator,ctx_err)
          tag=ctx_paral%nproc+i_proc
          call send(ghost_pproc(i_proc+1),1,i_proc,tag,   &
                    ctx_paral%communicator,ctx_err)
        enddo
      else
        !! receive infos from master, the tag is myrank
        tag=ctx_paral%myrank
        call receive(my_ncell ,1,0,tag,ctx_paral%communicator,ctx_err)
        tag=ctx_paral%nproc+ctx_paral%myrank
        call receive(my_nghost,1,0,tag,ctx_paral%communicator,ctx_err)
      endif
      !! ---------------------------------------------------------------
      !! wait for all here
      call barrier(ctx_paral%communicator,ctx_err)

      !! ---------------------------------------------------------------
      !! this is only required for ghost node infos, which serves
      !! to find the orientation.
      allocate(global_cell_node(ctx_mesh%n_node_per_cell,ctx_mesh%n_cell_gb))
      if(ctx_paral%master) then
        global_cell_node = ctx_mesh%cell_node
      end if
      call broadcast(global_cell_node,int(ctx_mesh%n_node_per_cell*     &
                     ctx_mesh%n_cell_gb),0,ctx_paral%communicator,ctx_err)
      
      !! allocate the mesh% infos --------------------------------------
      if(ctx_paral%master) then
        deallocate(ctx_mesh%cell_node )
        deallocate(ctx_mesh%cell_neigh)
        deallocate(ctx_mesh%cell_face )
        deallocate(ctx_mesh%cell_bary )
        deallocate(ctx_mesh%order     )
      endif
      allocate(ctx_mesh%cell_node (ctx_mesh%n_node_per_cell, my_ncell))
      allocate(ctx_mesh%cell_neigh(ctx_mesh%n_neigh_per_cell,my_ncell))
      allocate(ctx_mesh%cell_face (ctx_mesh%n_neigh_per_cell,my_ncell))
      allocate(ctx_mesh%cell_bary (ctx_mesh%dim_domain,my_ncell+my_nghost))
      allocate(ctx_mesh%order               (my_ncell+my_nghost))
      allocate(ctx_mesh%index_loctoglob_cell(my_ncell+my_nghost))
      allocate(ctx_mesh%ghost_node(ctx_mesh%n_node_per_cell, my_nghost))      
      
      !! ---------------------------------------------------------------
      !! once the master has ordered the information, we send to other 
      !! - global cells index            ==>  ordered_cellindex
      !! - global cells neighbors index  ==>  ordered_cellneigh
      !! - global cells face      index  ==>  ordered_cellface
      !! - global nodes indexes per cell ==>  ordered_cellnode 
      !! - cells order                   ==>  ordered_cellorder
      !! - ghost order                   ==>  ordered_ghostorder
      !! ---------------------------------------------------------------
      if(ctx_paral%master) then
        !! save infos
        icstart= 1
        icend  = my_ncell
        ctx_mesh%index_loctoglob_cell(1:my_ncell) & 
                                          =ordered_cellindex(icstart:icend)
        ctx_mesh%cell_node (:,:)          =ordered_cellnode (:,icstart:icend)
        ctx_mesh%cell_neigh(:,:)          =ordered_cellneigh(:,icstart:icend)
        ctx_mesh%cell_face (:,:)          =ordered_cellface (:,icstart:icend)
        ctx_mesh%cell_bary (:,1:my_ncell) =ordered_bary     (:,icstart:icend)
        ctx_mesh%order     (1:my_ncell)   =ordered_cellorder(icstart:icend)
        icend  = my_nghost
        ctx_mesh%cell_bary(:,my_ncell+1:my_ncell+my_nghost) = & 
                                          ordered_ghostbary(:,icstart:icend)
        ctx_mesh%order(my_ncell+1:my_ncell+my_nghost) = & 
                                           ordered_ghostorder(icstart:icend)
        ctx_mesh%index_loctoglob_cell(my_ncell+1:my_ncell+my_nghost)= &
                                           ordered_ghostindex(icstart:icend)
        !! send infos to other        
        do i_proc=1,ctx_paral%nproc-1
          icstart=offset_cells(i_proc+1)+1
          icend  =offset_cells(i_proc+2)
          !! temp array for send
          allocate(array_itemp(ctx_mesh%n_node_per_cell,cells_pproc(i_proc+1)))
          
          tag=0*ctx_paral%nproc+i_proc
          !! We must force int4 type for the number of stuff sent 
          call send(ordered_cellindex(icstart:icend),                   &
                    int(cells_pproc(i_proc+1)),i_proc,tag,              &
                    ctx_paral%communicator,ctx_err)
          tag=1*ctx_paral%nproc+i_proc
          array_itemp(:,:) = ordered_cellnode(:,icstart:icend)
          call send(array_itemp,int(cells_pproc(i_proc+1)*              &
                    ctx_mesh%n_node_per_cell),i_proc,tag,               &
                    ctx_paral%communicator,ctx_err)
          tag=2*ctx_paral%nproc+i_proc
          array_itemp(:,:) = ordered_cellneigh(:,icstart:icend)
          call send(array_itemp,int(cells_pproc(i_proc+1)*              &
                    ctx_mesh%n_neigh_per_cell),i_proc,tag,              &
                    ctx_paral%communicator,ctx_err)
          tag=6*ctx_paral%nproc+i_proc
          array_itemp(:,:) = ordered_cellface(:,icstart:icend)
          call send(array_itemp,int(cells_pproc(i_proc+1)*              &
                    ctx_mesh%n_neigh_per_cell),i_proc,tag,              &
                    ctx_paral%communicator,ctx_err)
          tag=3*ctx_paral%nproc+i_proc
          call send(ordered_cellorder(icstart:icend),                   &
                    int(cells_pproc(i_proc+1)),i_proc,tag,              &
                    ctx_paral%communicator,ctx_err)
          tag=7*ctx_paral%nproc+i_proc
          call send(ordered_bary(:,icstart:icend),                      &
                    int(cells_pproc(i_proc+1)*ctx_mesh%dim_domain),     &
                    i_proc,tag,ctx_paral%communicator,ctx_err)

          icstart=offset_ghost(i_proc+1)+1
          icend  =offset_ghost(i_proc+2)
          if(ghost_pproc(i_proc+1) .ne. 0) then
            tag=4*ctx_paral%nproc+i_proc
            call send(ordered_ghostorder(icstart:icend),                &
                      int(ghost_pproc(i_proc+1)),i_proc,tag,            &
                      ctx_paral%communicator,ctx_err)
            tag=5*ctx_paral%nproc+i_proc
            call send(ordered_ghostindex(icstart:icend),                &
                      int(ghost_pproc(i_proc+1)),i_proc,tag,            &
                      ctx_paral%communicator,ctx_err)
            tag=8*ctx_paral%nproc+i_proc
            call send(ordered_ghostbary(:,icstart:icend),               &
                      int(ghost_pproc(i_proc+1)*ctx_mesh%dim_domain),   &
                      i_proc,tag,ctx_paral%communicator,ctx_err)
          endif

          deallocate(array_itemp)
        enddo
        
        !! clean allocated master array 
        deallocate(cells_pproc       )
        deallocate(ghost_pproc       )
        deallocate(ordered_cellindex )
        deallocate(ordered_cellnode  )
        deallocate(ordered_cellneigh )
        deallocate(ordered_cellface  )
        deallocate(ordered_cellorder )
        deallocate(ordered_bary      )
        deallocate(offset_cells      )
        deallocate(offset_ghost      )
        deallocate(counter_cells     )
        deallocate(counter_ghost     )
        deallocate(ordered_ghostorder)
        deallocate(ordered_ghostindex)
        deallocate(ordered_ghostbary )
        
      !! ---------------------------------------------------------------
      ! -----------------------------------------------------
      !! We must force int4 to send because not supported by mpi 
      ! -----------------------------------------------------
      else !! other processors receive informations
        tag=0*ctx_paral%nproc+ctx_paral%myrank
        call receive(ctx_mesh%index_loctoglob_cell(1:my_ncell),        &
                     int(my_ncell),0,tag,ctx_paral%communicator,ctx_err)
        tag=1*ctx_paral%nproc+ctx_paral%myrank
        call receive(ctx_mesh%cell_node,int(my_ncell*                  &
                     ctx_mesh%n_node_per_cell),0,tag,                  &
                     ctx_paral%communicator,ctx_err)
        tag=2*ctx_paral%nproc+ctx_paral%myrank
        call receive(ctx_mesh%cell_neigh,int(my_ncell*                 &
                     ctx_mesh%n_neigh_per_cell),0,tag,                 &
                     ctx_paral%communicator,ctx_err)
        tag=6*ctx_paral%nproc+ctx_paral%myrank
        call receive(ctx_mesh%cell_face,int(my_ncell*                  &
                     ctx_mesh%n_neigh_per_cell),0,tag,                 &
                     ctx_paral%communicator,ctx_err)
        tag=3*ctx_paral%nproc+ctx_paral%myrank
        call receive(ctx_mesh%order(1:my_ncell),int(my_ncell),0,tag,   &
                     ctx_paral%communicator,ctx_err)
        tag=7*ctx_paral%nproc+ctx_paral%myrank
        call receive(ctx_mesh%cell_bary(:,1:my_ncell),                 &
                     int(my_ncell*ctx_mesh%dim_domain),0,tag,          &
                     ctx_paral%communicator,ctx_err)
                     
        if(my_nghost.ne. 0) then
          tag=4*ctx_paral%nproc+ctx_paral%myrank
          call receive(ctx_mesh%order(my_ncell+1:my_ncell+my_nghost),  &
                       int(my_nghost),0,tag,ctx_paral%communicator,ctx_err)
          tag=5*ctx_paral%nproc+ctx_paral%myrank
          call receive(ctx_mesh%index_loctoglob_cell(my_ncell+1:       &
                                                  my_ncell+my_nghost), &
                       int(my_nghost),0,tag,ctx_paral%communicator,ctx_err)
          tag=8*ctx_paral%nproc+ctx_paral%myrank
          call receive(ctx_mesh%cell_bary(:,my_ncell+1:                &
                                            my_ncell+my_nghost),       &
                       int(my_nghost*ctx_mesh%dim_domain),0,           &
                       tag,ctx_paral%communicator,ctx_err)
        endif
      endif
      !! ---------------------------------------------------------------
      !! wait for all here
      call barrier(ctx_paral%communicator,ctx_err)

      !! ---------------------------------------------------------------
      !! convert the global node index to local 
      ctx_mesh%n_cell_loc = my_ncell
      ctx_mesh%n_ghost_loc= my_nghost
      !! we need to broadcast the full node array ?
      if(.not. ctx_paral%master) then 
        allocate(ctx_mesh%x_node(ctx_mesh%dim_domain,ctx_mesh%n_node_gb))
      endif
      !! need to force int for mpi
      call broadcast(ctx_mesh%x_node,int(ctx_mesh%n_node_gb *           &
                     ctx_mesh%dim_domain),0,ctx_paral%communicator,ctx_err)
      n_node_temp = min(ctx_mesh%n_node_gb,my_ncell*ctx_mesh%n_node_per_cell)
      !! temporary nodes stuff
      allocate(temp_xnode          (ctx_mesh%dim_domain,n_node_temp))
      allocate(index_loctoglob_node(n_node_temp))

      !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_neigh,localind)
      !$OMP DO
      do i_cell=1,my_ncell
        do i_neigh=1,ctx_mesh%n_neigh_per_cell
          !! neighbor numbering with adjacent cell ---------------------
          if (ctx_mesh%cell_neigh(i_neigh,i_cell)>0) then
            !! find local index
            call mesh_find_index_cell(my_ncell,                            &
                                      ctx_mesh%cell_neigh(i_neigh,i_cell), &
                                      localind,ctx_mesh%index_loctoglob_cell)
            if(localind > my_ncell) then
              ctx_err%ierr=-1
              ctx_err%msg ="** ERROR: cell not found during local renumbering " //&
                     " [mesh_partition_send] **"
              ctx_err%critic=.true.
            else
              ctx_mesh%cell_neigh(i_neigh,i_cell) = localind !! local index
            endif
          endif
        enddo
      enddo
      !$OMP END DO
      !$OMP END PARALLEL
      !! possibility for non-shared error
      call distribute_error(ctx_err,ctx_paral%communicator) 

      !! ---------------------------------------------------------------
      !! interface local numbering
      allocate(local_cell_face(ctx_mesh%n_neigh_per_cell,my_ncell))
      local_cell_face = 0
      count_interface = 0
      do i_cell=1,my_ncell
        do i_neigh=1,ctx_mesh%n_neigh_per_cell
          if(local_cell_face(i_neigh,i_cell) .eq. 0) then
            count_interface                 = count_interface + 1
            local_cell_face(i_neigh,i_cell) = count_interface
            !! for the opposite cell as well
            neigh = ctx_mesh%cell_neigh(i_neigh,i_cell)
            if(neigh > 0) then
              do j_neigh=1, ctx_mesh%n_neigh_per_cell
                if(ctx_mesh%cell_neigh(j_neigh,neigh) .eq. i_cell) then
                  local_cell_face(j_neigh,neigh) = count_interface
                  exit
                end if
              end do           
            end if
          end if
        end do
      end do
     
      ctx_mesh%n_face_loc = count_interface
      allocate(ctx_mesh%index_loctoglob_face(count_interface))
      !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_neigh,i_face)
      !$OMP DO
      do i_cell=1,my_ncell
        do i_neigh=1,ctx_mesh%n_neigh_per_cell
          i_face = local_cell_face(i_neigh,i_cell)
          ctx_mesh%index_loctoglob_face(i_face) = &
                                      ctx_mesh%cell_face(i_neigh,i_cell)
          ctx_mesh%cell_face(i_neigh,i_cell)    = i_face
        end do
      end do
      !$OMP END DO
      !$OMP END PARALLEL
      deallocate(local_cell_face)

      !! update nodes infos local numbering and coordinates
      !! local numbering for ctx_mesh%cell_node 
      !! associated node coordinates in temp_xnode
      !! ---------------------------------------------------------------
      i_ghost = 0
      ctx_mesh%n_node_loc=0
      do i_cell=1,my_ncell
        do i_node=1,ctx_mesh%n_node_per_cell
          !! check if index already exists
          inodegb = ctx_mesh%cell_node(i_node,i_cell)
          !! the key is only to look the nodes between 1 and inodegb
          call mesh_find_index_cell(ctx_mesh%n_node_loc,inodegb,localind, &
                                    index_loctoglob_node)
          !! if new update quantities
          if(localind > ctx_mesh%n_node_loc) then
            ctx_mesh%n_node_loc                      =ctx_mesh%n_node_loc + 1
            index_loctoglob_node(ctx_mesh%n_node_loc)=inodegb
            temp_xnode(:,ctx_mesh%n_node_loc)        =ctx_mesh%x_node(:,inodegb)
          endif
          !! update local numbering of cell_node
          ctx_mesh%cell_node(i_node,i_cell) = localind
        enddo
        
        !! -------------------------------------------------------------
        !! in case one of the neighbor is a ghost we have to obtain 
        !! the index
        !!
        !! THIS IS CERTAINLY NOT OPTIMAL
        !! -------------------------------------------------------------
        do i_neigh=1,ctx_mesh%n_neigh_per_cell
          neigh = ctx_mesh%cell_neigh(i_neigh,i_cell)
          if(neigh .eq. tag_ghost_cell) then
            !! identify global cell index for the ghost and the cell
            i_ghost = i_ghost + 1 !! initialized with 0
            i_cellgb      = ctx_mesh%index_loctoglob_cell(i_cell)
            i_cellgb_ghost= ctx_mesh%index_loctoglob_cell(my_ncell + i_ghost)
            ctx_mesh%ghost_node(:,i_ghost) = -1
            !! loop over all nodes on the main cell
            do i_node=1,ctx_mesh%n_node_per_cell              
              !! loop over all ghost cell nodes to find the good one
              do j_node = 1,ctx_mesh%n_node_per_cell
                !! get the global node for neighbor and main
                inodegb_ghost = global_cell_node(j_node,i_cellgb_ghost)
                inodegb       = global_cell_node(i_node,i_cellgb)
                !! are they matching ?
                if(inodegb == inodegb_ghost) then
                  !! put the local numbering in the ghost array
                  count_interface = count_interface + 1
                  ctx_mesh%ghost_node(j_node,i_ghost)              =    &
                                      ctx_mesh%cell_node(i_node,i_cell)
                end if
              end do
            end do
          end if !! end if neighbor is a ghost
        end do
        !! -------------------------------------------------------------
      enddo
      deallocate(global_cell_node)

      !! proper allocation for the nodes 
      deallocate(ctx_mesh%x_node)
      allocate  (ctx_mesh%x_node(ctx_mesh%dim_domain,ctx_mesh%n_node_loc))
      ctx_mesh%x_node(:,:) = temp_xnode(:,1:ctx_mesh%n_node_loc)
      deallocate(temp_xnode)
      deallocate(index_loctoglob_node)

    else
      !! if monoprocessor
      ctx_mesh%n_cell_loc = ctx_mesh%n_cell_gb
      ctx_mesh%n_ghost_loc= 0
      ctx_mesh%n_node_loc = ctx_mesh%n_node_gb
      ctx_mesh%n_face_loc = ctx_mesh%n_face_gb
      allocate(ctx_mesh%index_loctoglob_cell(ctx_mesh%n_cell_gb))
      allocate(ctx_mesh%index_loctoglob_face(ctx_mesh%n_face_gb))
      do i_cell = 1,ctx_mesh%n_cell_gb
        ctx_mesh%index_loctoglob_cell(i_cell) = i_cell
      enddo
      do i_face=1,ctx_mesh%n_face_gb
        ctx_mesh%index_loctoglob_face(i_face) = i_face
      end do
      !! compute the barycenter for all cells ------------------------
      allocate(ctx_mesh%cell_bary(ctx_mesh%dim_domain,ctx_mesh%n_cell_gb))
      !$OMP PARALLEL DEFAULT(shared) PRIVATE(i_cell,k)
      !$OMP DO
      do i_cell=1,ctx_mesh%n_cell_gb
        do k=1,ctx_mesh%dim_domain
          ctx_mesh%cell_bary(k,i_cell) = sum(ctx_mesh%x_node(k,       &
                                         ctx_mesh%cell_node(:,i_cell)))
        end do
      end do
      !$OMP END DO
      !$OMP END PARALLEL
      ctx_mesh%cell_bary = real(dble(ctx_mesh%cell_bary)/             &
                                dble(ctx_mesh%n_node_per_cell),kind=RKIND_MESH)
      !! -------------------------------------------------------------
    endif

    !! face order and orientation depends on the type of elements
    allocate(ctx_mesh%order_face           (ctx_mesh%n_face_loc))
    allocate(ctx_mesh%cell_face_orientation(ctx_mesh%n_neigh_per_cell,  &
                                            ctx_mesh%n_cell_loc))
    call mesh_interface_orient(ctx_mesh%dim_domain,                     &
                               ctx_mesh%n_cell_loc,                     &
                               ctx_mesh%cell_neigh,ctx_mesh%cell_face,  &
                               ctx_mesh%cell_node,                      &
                               ctx_mesh%ghost_node,                     &
                               ctx_mesh%index_loctoglob_cell,           &
                               ctx_mesh%order,ctx_mesh%order_face,      &
                               ctx_mesh%cell_face_orientation,          &
                               ctx_mesh%format_element,ctx_err)

   
    !! update timing
    call time(time1)
    ctx_mesh%time_partition = ctx_mesh%time_partition + (time1-time0)

    return
  end subroutine mesh_partition_send

end module m_mesh_partition
