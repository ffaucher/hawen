!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_ctx_domain.f90
!
!> @author 
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION: 
!> @brief
!> the module defines of the context for meshes
!
!------------------------------------------------------------------------------
module m_ctx_domain

  use m_define_precision, only : IKIND_MESH, RKIND_MESH
 
  implicit none

  private
  public :: t_domain,domain_clean

  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_medium contains info on the medium considered.
  !>               It is used to characterized a domain where
  !>               different type of physics interact, e.g.,
  !>               acoustic-elastic problem, multi-physics,
  !>               separation between solar interior and 
  !>               atmosphere.
  !> utils:
  !> @param[integer]    n_media              : number of different media in the domain
  !> @param[integer]    n_cell_medium_loc(:) : number of cells per medium (mpi local)
  !> @param[integer]    n_cell_medium_gb (:) : number of cells per medium (mpi global)
  !> @param[integer]    tag_cell         (:) : cell tag to indicate the medium
  !> @param[real]       xinterface1D     (:) : position of interfaces, for 1D mesh only
  !----------------------------------------------------------------------------
  type t_domain_media
    
    !! file where cells tag can be found, it can be the same as for the mesh.
    character(len=512)                      :: file_medium
    !! number of different media and of zones.
    logical                                 :: flag_multi
    integer(kind=4)                         :: n_media 
    integer(kind=4)                         :: n_zone
    !! memory count
    integer(kind=8)                         :: memory_loc
    
    !! count the number of cells that belong in each of the medium, 
    !! we have a local (per mpi) and a global count, 
    !! n_cell_medium_loc(k) indicates how many cells for the medium
    !!                      of type k.
    integer(kind=IKIND_MESH), allocatable  :: n_cell_medium_loc(:)
    integer(kind=IKIND_MESH), allocatable  :: n_cell_medium_gb (:)
  
    !! Each local cell has a tag that indicates the type of medium.
    integer(kind=4)         , allocatable  :: tag_cell(:)
  
    !! position of the interfaces between media in 1D only
    real   (kind=8)         , allocatable  :: xinterface1D(:)
    integer(kind=4)                        :: tag_interface1D
    
    !! tag on each of the face for interface between medium,
    !! dimension (n_face_per_cell,n_cell_loc)
    integer(kind=4)         , allocatable  :: cell_neigh(:,:)
  
  end type t_domain_media
  !----------------------------------------------------------------------------

  
  !----------------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_mesh contains info on the mesh
  !
  !> utils:
  !> @param[string]  file_mesh   : mesh file base name,
  !> @param[string]  format_mesh : mesh format (e.g., triangle),
  !> @param[string]  format_cell : type of cells (triangle, quadrangle)
  !> @param[string]  partitioner : mesh partitioner (e.g., metis, scotch),
  !> @param[integer] dim_domain  : domain dimension (1,2,3)
  !>
  !> @param[real]    memory_peak_part  : memory peak for partitioning,
  !> @param[real]    memory_peak_other : memory peak on non-master proc,
  !> @param[real]    memory_peak_read  : memory peak for reading,
  !> @param[real]    memory_loc        : local memory used on each processor.
  !> @param[real]    time_init         : time to init the mesh
  !> @param[real]    time_read         : time to read the mesh
  !> @param[real]    time_partition    : time to partition
  !> @param[integer] tag_n             : maximal number of tag for BC
  !> @param[integer] tag_bc_array      : list of BC tag
  !> @param[string]  Zimpedance        : type of Impedance ABC
  !>
  !> @param[real]    bounds_xmin       : bounding box xmin
  !> @param[real]    bounds_xmax       : bounding box xmin
  !> @param[real]    bounds_ymin       : bounding box ymin
  !> @param[real]    bounds_ymax       : bounding box ymax
  !> @param[real]    bounds_zmin       : bounding box zmin
  !> @param[real]    bounds_zmax       : bounding box zmax  
  !>
  !> @param[integer] n_cell_gb         : global number of cells
  !> @param[integer] n_node_gb         : global number of nodes
  !> @param[integer] n_face_gb         : global number of interfaces
  !> @param[integer] n_face_interior_gb: global number of interior interfaces
  !> @param[integer] n_face_boundary_gb: global number of boundary interfaces
  !> @param[integer] n_cell_loc        : local number of cells
  !> @param[integer] n_node_loc        : local number of nodes
  !> @param[integer] n_ghost_loc       : local number of ghost
  !>
  !> @param[integer] n_node_per_cell   : number of node per cell
  !> @param[integer] n_node_per_face   : number of node per interface
  !> @param[integer] n_node_per_face_shared: number of node to share interface
  !> @param[integer] n_neigh_per_cell  : number of neighbor per cell
  !> 
  !> @param[real]    x_node   (:,:)    : node coordinates
  !> @param[integer] cell_node(:,:)    : cell node list  
  !> @param[integer] cell_neigh(:,:)   : cell neighbors  
  !> @param[real]    cell_bary(:,:)    : cell barycenters  
  !> @param[integer] order    (:)      : cells order
  !> @param[integer] order_max         : max order
  !> @param[integer] order_min         : min order
  !> @param[logical] flag_eval_int_quadrature: evaluate integral with quadrature or not
  !> @param[string]  order_method      : order method (constant, automatic)
  !> @param[integer] index_loctoglob_cell(:): local to global cell index
  !> @param[integer] index_loctoglob_face(:): local to global face index
  !> @param[integer] face_orientation    (:): local face orientation
  !----------------------------------------------------------------------------
  type t_domain

    !! file name for the current mesh and its format
    character(len=256) :: file_mesh
    character(len=16)  :: format_mesh
    character(len=16)  :: format_element
    character(len=16)  :: partitioner
    !! domain dimension
    integer            :: dim_domain
    !! required memory for global mesh and locally per processor
    integer(kind=8)    :: memory_peak_part  = 0
    integer(kind=8)    :: memory_peak_other = 0
    integer(kind=8)    :: memory_peak_read  = 0
    integer(kind=8)    :: memory_loc        = 0
    !! time for operations
    real(kind=8)       :: time_init      = 0.
    real(kind=8)       :: time_read      = 0.
    real(kind=8)       :: time_partition = 0.
    !! informations on the type of medium for possibilities of multi-physics
    type(t_domain_media)         :: ctx_media
    !! boundary tag infos
    integer(kind=4)              :: tag_n = 0
    integer(kind=4), allocatable :: tag_bc_array(:,:)
    logical                      :: flag_bc_abc         
    logical                      :: flag_bc_freesurface 
    logical                      :: flag_bc_wallsurface 
    logical                      :: flag_bc_dirichlet   
    logical                      :: flag_bc_neumann     
    logical                      :: flag_bc_planewave   
    logical                      :: flag_bc_planewave_scatt
    logical                      :: flag_bc_planewave_isoP   
    logical                      :: flag_bc_planewave_isoS
    logical                      :: flag_bc_planewave_vtiYSH
    logical                      :: flag_bc_planewave_vtiYSV
    logical                      :: flag_bc_planewave_vtiYqP
    logical                      :: flag_bc_aldir_dlrob   
    logical                      :: flag_bc_centneu   
    logical                      :: flag_bc_deltapdir_dldir 
    logical                      :: flag_bc_analytic_orthorhombic
    logical                      :: flag_bc_analytic_elasticiso
    logical                      :: flag_bc_deltapdir_dlrob 
    logical                      :: flag_bc_alrbc_dlrob 
    logical                      :: flag_bc_alrbc_dldir
    logical                      :: flag_bc_deltapdir
    !! pml is normal to the boundary
    integer(kind=4), allocatable :: tag_bc_pml       (:)
    integer(kind=4), allocatable :: tag_bc_pml_radius(:)
    character(len=32)            :: Zimpedance
    
    !! bounding box
    real(kind=RKIND_MESH)  :: bounds_xmin
    real(kind=RKIND_MESH)  :: bounds_xmax
    real(kind=RKIND_MESH)  :: bounds_ymin
    real(kind=RKIND_MESH)  :: bounds_ymax
    real(kind=RKIND_MESH)  :: bounds_zmin
    real(kind=RKIND_MESH)  :: bounds_zmax

    !! -----------------------------------------------------------------
    !! for the mesh in 2 or 3D we have
    !! - cell  (i.e. the element), e.g. tetrahedra, triangle, rectangles
    !! - inter the interface between cells, e.g. edge in 2d, face in 3d
    !! - node  
    !! -----------------------------------------------------------------
    !! global amounts
    integer(kind=IKIND_MESH)  :: n_cell_gb
    integer(kind=IKIND_MESH)  :: n_node_gb
    integer(kind=IKIND_MESH)  :: n_face_gb
    integer(kind=IKIND_MESH)  :: n_face_interior_gb
    integer(kind=IKIND_MESH)  :: n_face_boundary_gb
    !! local amounts + the ghost number
    integer(kind=IKIND_MESH)  :: n_cell_loc
    integer(kind=IKIND_MESH)  :: n_ghost_loc
    integer(kind=IKIND_MESH)  :: n_node_loc
    integer(kind=IKIND_MESH)  :: n_face_loc

    !! information depending on the geometry of the cell
    !! number of node to define a cell and intercell,
    !! number of neighbor for each cell
    integer                   :: n_node_per_cell
    integer                   :: n_node_per_face
    integer                   :: n_node_per_face_shared
    integer                   :: n_neigh_per_cell
    !! number of possible combination for interface 
    !! between element (i.e. rotation of face)
    integer                   :: n_interface_combi
    !! -----------------------------------------------------------------

    !! -----------------------------------------------------------------
    !! node coordinates: array of size (dim_domain,n_node_loc)
    real   (kind=RKIND_MESH)  , allocatable :: x_node(:,:)
    !! cell information: indicates the n_node_per_cell nodes index of
    !! each cell: array of size (n_node_per_cell,n_cell_loc)
    integer(kind=IKIND_MESH)  , allocatable :: cell_node (:,:)
    !! barycenters for each of the cells
    !! each cell and ghost: array of size (dim_domain,n_cell_loc + nghost_loc)
    real   (kind=RKIND_MESH)  , allocatable :: cell_bary (:,:)
    !! the ghost face node: array of size (n_node_per_cell,n_ghost_loc)
    integer(kind=IKIND_MESH)  , allocatable :: ghost_node(:,:)
    !! neighbor information: indicates the n_neigh_per_cell neighbors of 
    !! each cell: array of size (n_neigh_per_cell,n_cell_loc)
    integer(kind=IKIND_MESH)  , allocatable :: cell_neigh(:,:)
    !! interface information: indicates the n_neigh_per_cell face index of 
    !! each cell: array of size (n_neigh_per_cell,n_cell_loc)
    integer(kind=IKIND_MESH)  , allocatable :: cell_face(:,:)
    !! face orientation: for shared face, we need to know how it is
    !! oriented for the current cell. There is a total of 
    !! n_interface_combi possibilities
    integer                   , allocatable :: cell_face_orientation(:,:)
        
    !! order of the polynomials for each cell: array of size 
    !!                                          (n_cell_loc + nghost)
    integer     , allocatable :: order(:)
    integer                   :: order_max
    integer                   :: order_min
    !! indicate the method (i.e. constant, automatic)
    character(len=16)         :: order_method
    !! order of the polynomials for each interface: it the max between
    !! the two adjacent cells; array of size (n_face_loc)
    integer     , allocatable :: order_face(:)
    !! -----------------------------------------------------------------
    
    !! -----------------------------------------------------------------
    !! partition information size (ncells_loc + nghost)
    integer(kind=IKIND_MESH)  , allocatable :: index_loctoglob_cell(:)
    !! partition information size (n_face_loc)
    integer(kind=IKIND_MESH)  , allocatable :: index_loctoglob_face(:)

    !! possibility to use a constant penalization for DG method
    real                                    :: penalization
    !! possibility to evaluate integrals with quadrature rules
    logical                                 :: flag_eval_int_quadrature

    !! -----------------------------------------------------------------
    !! PML informations
    !! -----------------------------------------------------------------
    logical              :: pml_flag        !! is there pml in the domain
    logical              :: pml_flag_init   !! pml initialized or not
    character(len=512)   :: pml_size_format !! how is the pml size decided

    real(kind=8)         :: pml_size        !! size of the pml (in meters 
                                            !!   or in term of wavelength, 
                                            !!   depending on the format)
    
    !! format of the pml and coefficient (e.g., cosine, constant)
    character(len=512)   :: pml_format
    real(kind=8)         :: pml_coeff

    !! if pml, every cell indicates how, e.g., from x, y z min or max,
    !! or if circular even.
    real(kind=8),allocatable :: pml_normals(:,:) !! normals to the PML surface
    integer                  :: pml_nbc          !! number of boundary that are PML
    !! min and max coo of each pml
    real(kind=8),allocatable :: pml_xmin(:), pml_xmax(:)
    real(kind=8),allocatable :: pml_ymin(:), pml_ymax(:)
    real(kind=8),allocatable :: pml_zmin(:), pml_zmax(:)
    !! -----------------------------------------------------------------

  end type t_domain
  
  contains
  
  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean information and allocation for the type domain_media
  !
  !> @param[inout] ctx_media      : structure to clean
  !----------------------------------------------------------------------------
  subroutine domain_media_clean(ctx_media)
    implicit none
    
    type(t_domain_media), intent(inout) :: ctx_media
    
    !!  ----------------------------------------------------------------
    ctx_media%tag_interface1D   = 0
    ctx_media%memory_loc        = 0
    ctx_media%n_zone            = 0
    ctx_media%flag_multi        = .false. 
    if(allocated(ctx_media%n_cell_medium_loc)) deallocate(ctx_media%n_cell_medium_loc)
    if(allocated(ctx_media%n_cell_medium_gb )) deallocate(ctx_media%n_cell_medium_gb )
    if(allocated(ctx_media%tag_cell         )) deallocate(ctx_media%tag_cell         )
    if(allocated(ctx_media%xinterface1D     )) deallocate(ctx_media%xinterface1D     )
    if(allocated(ctx_media%cell_neigh       )) deallocate(ctx_media%cell_neigh       )
    
    return

  end subroutine domain_media_clean
  !---------------------------------------------------------------------------- 


  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> clean mesh information and allocation
  !
  !> @param[inout] ctx_mesh      : mesh context
  !----------------------------------------------------------------------------
  subroutine domain_clean(ctx_mesh)
    implicit none
    
    type(t_domain)       ,intent(inout)  :: ctx_mesh

    !! -----------------------------------------------------------------

    call domain_media_clean(ctx_mesh%ctx_media)
    ctx_mesh%file_mesh      =''
    ctx_mesh%format_mesh    =''
    ctx_mesh%format_element =''
    ctx_mesh%Zimpedance     =''
    ctx_mesh%partitioner    =''
    ctx_mesh%dim_domain     =0
    ctx_mesh%memory_peak_part  = 0
    ctx_mesh%memory_peak_other = 0
    ctx_mesh%memory_peak_read  = 0
    ctx_mesh%memory_loc        = 0
    ctx_mesh%time_init   =0.0
    ctx_mesh%time_read   =0.0
    ctx_mesh%time_partition=0.0
    ctx_mesh%tag_n       =0
    ctx_mesh%bounds_xmin =0.0
    ctx_mesh%bounds_xmax =0.0
    ctx_mesh%bounds_ymin =0.0
    ctx_mesh%bounds_ymax =0.0
    ctx_mesh%bounds_zmin =0.0
    ctx_mesh%bounds_zmax =0.0
    ctx_mesh%n_neigh_per_cell=0
    ctx_mesh%n_node_gb  =0
    ctx_mesh%n_cell_gb  =0
    ctx_mesh%n_face_gb  =0
    ctx_mesh%n_face_loc =0
    ctx_mesh%n_face_interior_gb =0
    ctx_mesh%n_face_boundary_gb =0
    ctx_mesh%n_node_loc =0
    ctx_mesh%n_cell_loc =0
    ctx_mesh%n_ghost_loc=0
    ctx_mesh%n_node_per_cell        =0
    ctx_mesh%n_node_per_face        =0
    ctx_mesh%n_node_per_face_shared =0
    ctx_mesh%n_neigh_per_cell       =0
    ctx_mesh%penalization           =-1
    ctx_mesh%order_max              =0
    ctx_mesh%order_min              =0
    ctx_mesh%flag_eval_int_quadrature=.false.
    ctx_mesh%order_method           =''
    ctx_mesh%flag_bc_abc            = .false.
    ctx_mesh%flag_bc_freesurface    = .false.
    ctx_mesh%flag_bc_wallsurface    = .false.
    ctx_mesh%flag_bc_dirichlet      = .false.
    ctx_mesh%flag_bc_neumann        = .false.
    ctx_mesh%flag_bc_planewave      = .false.
    ctx_mesh%flag_bc_planewave_scatt= .false.
    ctx_mesh%flag_bc_planewave_isoP = .false.
    ctx_mesh%flag_bc_planewave_isoS = .false.
    ctx_mesh%flag_bc_planewave_vtiYSH = .false.
    ctx_mesh%flag_bc_planewave_vtiYSV = .false.
    ctx_mesh%flag_bc_planewave_vtiYqP = .false.
    ctx_mesh%flag_bc_analytic_elasticiso  =.false.
    ctx_mesh%flag_bc_analytic_orthorhombic=.false.
    ctx_mesh%flag_bc_aldir_dlrob    = .false.
    ctx_mesh%flag_bc_centneu        = .false.
    ctx_mesh%flag_bc_deltapdir_dldir= .false.
    ctx_mesh%flag_bc_deltapdir_dlrob= .false.
    ctx_mesh%flag_bc_alrbc_dldir    = .false.
    ctx_mesh%flag_bc_alrbc_dlrob    = .false.
    ctx_mesh%flag_bc_deltapdir      = .false.
    !! pml infos
    ctx_mesh%pml_flag               = .false.
    ctx_mesh%pml_flag_init          = .false.
    ctx_mesh%pml_size_format        = ''
    ctx_mesh%pml_size               = 0.
    ctx_mesh%pml_format             = ''
    ctx_mesh%pml_coeff              = 0.
    ctx_mesh%pml_nbc                = 0
    if(allocated(ctx_mesh%pml_normals      )) deallocate(ctx_mesh%pml_normals )
    if(allocated(ctx_mesh%pml_xmin         )) deallocate(ctx_mesh%pml_xmin    )
    if(allocated(ctx_mesh%pml_xmax         )) deallocate(ctx_mesh%pml_xmax    )
    if(allocated(ctx_mesh%pml_ymin         )) deallocate(ctx_mesh%pml_ymin    )
    if(allocated(ctx_mesh%pml_ymax         )) deallocate(ctx_mesh%pml_ymax    )
    if(allocated(ctx_mesh%pml_zmin         )) deallocate(ctx_mesh%pml_zmin    )
    if(allocated(ctx_mesh%pml_zmax         )) deallocate(ctx_mesh%pml_zmax    )

    if(allocated(ctx_mesh%x_node)   )        deallocate(ctx_mesh%x_node)
    if(allocated(ctx_mesh%cell_node))        deallocate(ctx_mesh%cell_node)
    if(allocated(ctx_mesh%cell_neigh))       deallocate(ctx_mesh%cell_neigh)
    if(allocated(ctx_mesh%cell_bary))        deallocate(ctx_mesh%cell_bary)
    if(allocated(ctx_mesh%cell_face))        deallocate(ctx_mesh%cell_face)
    if(allocated(ctx_mesh%order))            deallocate(ctx_mesh%order)
    if(allocated(ctx_mesh%order_face))       deallocate(ctx_mesh%order_face)
    if(allocated(ctx_mesh%cell_face_orientation)) &
                               deallocate(ctx_mesh%cell_face_orientation)
    if(allocated(ctx_mesh%index_loctoglob_cell))  &
                               deallocate(ctx_mesh%index_loctoglob_cell)
    if(allocated(ctx_mesh%index_loctoglob_face))  &
                               deallocate(ctx_mesh%index_loctoglob_face)
    if(allocated(ctx_mesh%tag_bc_array)) deallocate(ctx_mesh%tag_bc_array)

    return
  end subroutine domain_clean

end module m_ctx_domain
