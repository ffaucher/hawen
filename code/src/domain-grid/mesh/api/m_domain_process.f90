!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_domain_process.f90
!
!> @author
!> F. Faucher [Faculty of Mathematics, University of Vienna]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the domain mesh (triangles in 2D, tetrahedra in 3D)
!
!------------------------------------------------------------------------------
module m_domain_process

  use m_raise_error,                  only : raise_error, t_error
  use m_distribute_error,             only : distribute_error
  use m_ctx_parallelism,              only : t_parallelism
  use m_broadcast,                    only : broadcast
  use m_ctx_domain,                   only : t_domain
  !> read parameter files
  use m_read_parameters_domain,       only: read_parameters_domain,     &
                                            read_parameters_domain_media
  !> print infos
  use m_print_domain,                 only: print_info_mesh_welcome,    &
                                            print_info_mesh_init,       &
                                            print_info_mesh_bbox,       &
                                            print_info_mesh_end,        &
                                            print_info_media
  !> for coupling of media
  use m_domain_init_media,            only : domain_init_media
  !> read mesh
  use m_define_precision,             only : IKIND_MESH
  use m_mesh_simplex2d_read_triangle, only : mesh_read_triangle,        &
                                             mesh_read_init_triangle
  use m_mesh_simplex3d_read_tetgen,   only : mesh_read_tetgen,          &
                                             mesh_read_init_tetgen
  use m_mesh_simplex3d_read_meshformat,only: mesh_read_meshformat3d,    &
                                             mesh_read_init_meshformat3d
  use m_mesh_simplex2d_read_meshformat,only: mesh_read_meshformat2d,    &
                                             mesh_read_init_meshformat2d
  use m_mesh_simplex1d_read_segment,  only : mesh_read_segment,         &
                                             mesh_read_init_segment
  use m_mesh_partition,               only : mesh_partition
  use m_tag_bc        ,               only : tag_pml,n_boundary_tag

  implicit none

  private
  public  :: domain_process
  private :: domain_init, mesh_read2d, mesh_read3d

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> create mesh domain from parameter file
  !
  !> @param[in]    ctx_paral      : context parallelism
  !> @param[in]    domain_type    : type of domain (= mesh)
  !> @param[in]    dim_domain     : domain dimension
  !> @param[in]    n_media_max    : maximal number of media
  !> @param[in]    parameter_file : parameter file where infos are
  !> @param[in]    ctx_domain     : output domain
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine domain_process(ctx_paral,dim_domain,n_media_max,           &
                            parameter_file,ctx_domain,ctx_err)

    implicit none 

    type(t_parallelism)      ,intent(in)   :: ctx_paral
    character(len=*)         ,intent(in)   :: parameter_file
    integer                  ,intent(in)   :: dim_domain
    integer                  ,intent(in)   :: n_media_max
    type(t_domain)           ,intent(inout):: ctx_domain
    type(t_error)            ,intent(inout):: ctx_err
    !! local for mesh
    character(len=512)   :: meshfile,meshformat,meshpart,order_file,Zimp
    integer              :: gborder,ntag_medium_max
    integer,allocatable  :: tag_boundary(:,:),tag_medium(:,:)
    logical              :: flag_init
    !! -----------------------------------------------------------------
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! ---------------------------------------------------------------
    !! read mesh domain info
    !! ---------------------------------------------------------------
    !! read parameter file
    call read_parameters_domain(ctx_paral,parameter_file,meshfile,      &
                                meshformat,meshpart,tag_boundary,       &
                                gborder,order_file,ctx_domain%order_max,&
                                ctx_domain%order_min,                   &
                                ctx_domain%flag_eval_int_quadrature,    &
                                ctx_domain%penalization,Zimp,           &
                                !! PML informations 
                                ctx_domain%pml_flag,                    &
                                ctx_domain%pml_size_format,             &
                                ctx_domain%pml_size,                    &
                                ctx_domain%pml_format,                  &
                                ctx_domain%pml_coeff,                   &
                                ctx_domain%pml_nbc,                     &
                                ctx_err)

    !! --------------------------------------------------------------------
    !!
    !! Read information in case we have a coupling of media.
    !!
    !! --------------------------------------------------------------------
    if(n_media_max > 1) then
      ctx_domain%ctx_media%flag_multi = .true.
      call read_parameters_domain_media(ctx_paral,parameter_file,         &
                                        dim_domain,                       &
                                        ctx_domain%ctx_media%file_medium, &
                                        ctx_domain%ctx_media%n_media,     &
                                        ctx_domain%ctx_media%n_zone,      &
                                        tag_medium,ntag_medium_max,       &
                                        ctx_domain%ctx_media%xinterface1D,&
                                        ctx_err)
      if(ctx_domain%ctx_media%n_media > n_media_max) then
        ctx_err%msg   = "** ERROR: There are too many different "//     &
                        "types of medium are given for this equations"//&
                        "(in terms of tags) [domain_process] **"
        ctx_err%ierr  = -62
        ctx_err%critic=.true.
        call raise_error(ctx_err) 
      endif
    else
      ctx_domain%ctx_media%n_media    =  1 
      ctx_domain%ctx_media%n_zone     =  1 
      ctx_domain%ctx_media%flag_multi = .false.
    end if
    !! -------------------------------------------------------------------

    ctx_domain%pml_flag_init = .false. !> not initialized yet
    !! print welcome -----------------------------------------------------
    call print_info_mesh_welcome(ctx_paral,meshfile,meshformat,meshpart,&
                                 dim_domain,gborder,ctx_err)

    !! init infos --------------------------------------------------------
    flag_init=.true.
    call domain_init(ctx_paral,ctx_domain,dim_domain,meshfile,          &
                     meshformat,meshpart,tag_boundary,gborder,          &
                     order_file,flag_init,Zimp,ctx_err)
    call print_info_mesh_init(ctx_paral,ctx_domain%n_node_gb,           &
                              ctx_domain%n_cell_gb,                     &
                              ctx_domain%n_face_gb,                     &
                              ctx_domain%memory_peak_read,              &
                              ctx_domain%memory_peak_part,              &
                              ctx_domain%memory_peak_other,             &
                              ctx_domain%format_element,                &
                              ctx_domain%Zimpedance,                    &
                              ctx_err)

    !! read infos and partition among processors -------------------------
    flag_init=.false.
    call domain_init(ctx_paral,ctx_domain,dim_domain,meshfile,          &
                     meshformat,meshpart,tag_boundary,gborder,          &
                     order_file,flag_init,Zimp,ctx_err)

    !! initialize the case of coupling between media ---------------------
    if(ctx_domain%ctx_media%flag_multi) then
      !! in case there is only one zone and one medium here, we need to
      !! update the x1d to the maximal position to be safe.
      if(ctx_domain%ctx_media%n_media == 1 .and.                        &
         ctx_domain%ctx_media%n_zone  == 1 .and.                        &
         dim_domain == 1) then
         ctx_domain%ctx_media%xinterface1D(1) =                         &
                                 dble(ctx_domain%bounds_xmax + tiny(1.0))
      end if
      
      call domain_init_media(ctx_paral,ctx_domain,dim_domain,tag_medium,&
                             ntag_medium_max,tag_boundary,ctx_err)
      call print_info_media(ctx_paral,dim_domain,                       &
                            ctx_domain%ctx_media%n_cell_medium_gb,      &
                            ctx_domain%ctx_media%n_media,               &
                            ctx_domain%ctx_media%n_zone,                &
                            ctx_domain%ctx_media%memory_loc,            &
                            ctx_domain%ctx_media%xinterface1D,          &
                            ctx_err)
    end if
    !! -------------------------------------------------------------------
    
    !! the boundary tags are now saved in the domain context -------------
    if(allocated(tag_boundary)) deallocate(tag_boundary)
    if(allocated(tag_medium))   deallocate(tag_medium)

    !! print infos -------------------------------------------------------
    call print_info_mesh_bbox(ctx_paral,                                &
                              dble(ctx_domain%bounds_xmin),             &
                              dble(ctx_domain%bounds_xmax),             &
                              dble(ctx_domain%bounds_ymin),             &
                              dble(ctx_domain%bounds_ymax),             &
                              dble(ctx_domain%bounds_zmin),             &
                              dble(ctx_domain%bounds_zmax),             &
                              ctx_err)
    !! print timing infos ------------------------------------------------
    call print_info_mesh_end(ctx_paral,ctx_domain%time_init,            &
                             ctx_domain%time_read,                      &
                             ctx_domain%time_partition,ctx_err)
    return
  end subroutine domain_process

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  The subroutine reads the input mesh: it is triangle based in 2D
  !>  and tetrahedral based in 3D
  !> @details
  !>  it uses the structure of t_mesh, which depends on the dimension
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[out]  mesh          : mesh context
  !> @param[in]   dim_domain    : domain dimension
  !> @param[in]   file_mesh     : mesh filename
  !> @param[in]   format_mesh   : mesh format
  !> @param[in]   mesh_part     : mesh partitioner
  !> @param[in]   mesh_tag_bc   : mesh boundary condition tags
  !>                              (1): ABC, (2): Dirichlet, (3): Neumann
  !> @param[in]   global_order  : order of polynomials <= 0 if p-adaptivity
  !> @param[in]   order_file    : file to read multiple order
  !> @param[in]   flag_init     : init only or with allocation
  !> @param[in]   Zimp          : choice of Impedance BC for Absorbing
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine domain_init(ctx_paral,mesh,dim_domain,file_mesh,           &
                         format_mesh,mesh_part,mesh_tag_bc,             &
                         global_order,order_file,flag_init,Zimp,ctx_err)

    implicit none

    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: mesh
    integer            ,intent(in)   :: dim_domain,global_order
    integer,allocatable,intent(in)   :: mesh_tag_bc(:,:)
    character(len=*)   ,intent(in)   :: file_mesh,format_mesh,mesh_part
    character(len=*)   ,intent(in)   :: order_file,Zimp
    logical            ,intent(in)   :: flag_init
    type(t_error)      ,intent(out)  :: ctx_err
    !!
    integer(kind=IKIND_MESH) :: k
    integer                  :: unit_order=104
    logical                  :: file_exists

    ctx_err%ierr  = 0

    !! basic mesh infos
    mesh%dim_domain  = dim_domain
    mesh%file_mesh   = trim(adjustl(file_mesh))
    mesh%format_mesh = trim(adjustl(format_mesh))
    mesh%partitioner = trim(adjustl(mesh_part))
    mesh%Zimpedance  = trim(adjustl(Zimp))
    
    !! the order method depends on the global_order during init
    if(flag_init) then
      if(global_order > 0) then
        mesh%order_method = 'constant'
        mesh%order_max    = global_order
      elseif(global_order == -2) then
        mesh%order_method = 'automatic'
      else
        mesh%order_method = 'read'
      end if
    else !! if not init, we can create the array "order"
      if(ctx_paral%master) then
        !! discretization order ----------------------------------------
        allocate(mesh%order(mesh%n_cell_gb))
        select case(trim(adjustl(mesh%order_method)))
          case('constant')
            mesh%order=global_order
          case('automatic')
            !! TEMPORARY FOR MESH PARTITION
            mesh%order=1
          case('read')
            !! read from file
            inquire(file=trim(adjustl(order_file)),exist=file_exists)
            if(.not. file_exists) then
              ctx_err%ierr=-1
              ctx_err%msg ="** MESH ORDER FILE "//trim(adjustl(order_file))//&
                           " DOEST NOT EXIST [domain_init] "
              ctx_err%critic=.true.
            endif
            !! read file
            open (unit=unit_order,file=trim(adjustl(order_file)))
            read(unit_order,*) k
            if(k .ne. mesh%n_cell_gb) then
              ctx_err%ierr=-1
              ctx_err%msg ="** INCORRECT NUMBER OF CELL IN ORDER FILE "  // &
                            trim(adjustl(order_file)) //" [domain_init]"
              ctx_err%critic=.true.
            end if
            !! read the rest if ok
            do k=1,mesh%n_cell_gb
               read(unit_order,*) mesh%order(k)
            end do
            close(unit_order)
            mesh%order_max    = maxval(mesh%order)
          case default
            ctx_err%ierr=-1
            ctx_err%msg ="** Unrecognized method for order [domain_init] "
            ctx_err%critic=.true.
        end select
      end if
      !! ---------------------------------------------------------------
      !! possibility for non-shared error
      call distribute_error(ctx_err,ctx_paral%communicator) 

      call broadcast(mesh%order_max,1,0,ctx_paral%communicator,ctx_err)
    end if

    !! boundary tag
    if(mesh%tag_n .eq. 0) then
      if(allocated(mesh_tag_bc)) then
        mesh%tag_n = size(mesh_tag_bc,2) !! maximum number of input labels for 1 BC
      else
         ctx_err%msg   = "** ERROR: No BC Tag [domain_process] **"
         ctx_err%ierr  = -1
         ctx_err%critic=.true.
         call raise_error(ctx_err)
      end if

      mesh%flag_bc_abc         = .false.
      mesh%flag_bc_freesurface = .false.
      mesh%flag_bc_wallsurface = .false.
      mesh%flag_bc_dirichlet   = .false.
      mesh%flag_bc_neumann     = .false.
      mesh%flag_bc_planewave   = .false.
      mesh%flag_bc_aldir_dlrob = .false.
      mesh%flag_bc_centneu     = .false.
      mesh%flag_bc_deltapdir_dldir  = .false.
      mesh%flag_bc_deltapdir_dlrob  = .false.
      mesh%flag_bc_alrbc_dlrob      = .false.
      mesh%flag_bc_alrbc_dldir      = .false.
      mesh%flag_bc_deltapdir        = .false.
      mesh%flag_bc_planewave_scatt  = .false.
      mesh%flag_bc_planewave_isoP   = .false.
      mesh%flag_bc_planewave_isoS   = .false.
      mesh%flag_bc_planewave_vtiYSH = .false.
      mesh%flag_bc_planewave_vtiYSV = .false.
      mesh%flag_bc_planewave_vtiYqP = .false.
      mesh%flag_bc_analytic_elasticiso = .false.
      if(abs(mesh_tag_bc( 1,1)) > 0) mesh%flag_bc_abc         = .true.
      if(abs(mesh_tag_bc( 2,1)) > 0) mesh%flag_bc_freesurface = .true.
      if(abs(mesh_tag_bc( 3,1)) > 0) mesh%flag_bc_wallsurface = .true.
      if(abs(mesh_tag_bc( 4,1)) > 0) mesh%flag_bc_dirichlet   = .true.
      if(abs(mesh_tag_bc( 5,1)) > 0) mesh%flag_bc_neumann     = .true.
      if(abs(mesh_tag_bc( 6,1)) > 0) mesh%flag_bc_planewave   = .true.
      if(abs(mesh_tag_bc( 7,1)) > 0) mesh%flag_bc_aldir_dlrob = .true.
      if(abs(mesh_tag_bc( 8,1)) > 0) mesh%flag_bc_centneu     = .true.
      if(abs(mesh_tag_bc(11,1)) > 0) mesh%flag_bc_deltapdir_dldir       = .true.
      if(abs(mesh_tag_bc(12,1)) > 0) mesh%flag_bc_analytic_orthorhombic = .true.
      if(abs(mesh_tag_bc(13,1)) > 0) mesh%flag_bc_deltapdir_dlrob       = .true.
      if(abs(mesh_tag_bc(14,1)) > 0) mesh%flag_bc_alrbc_dlrob           = .true.
      if(abs(mesh_tag_bc(15,1)) > 0) mesh%flag_bc_alrbc_dldir           = .true.
      if(abs(mesh_tag_bc(16,1)) > 0) mesh%flag_bc_deltapdir             = .true.
      if(abs(mesh_tag_bc(17,1)) > 0) mesh%flag_bc_planewave_isoP        = .true.
      if(abs(mesh_tag_bc(18,1)) > 0) mesh%flag_bc_planewave_isoS        = .true.
      if(abs(mesh_tag_bc(19,1)) > 0) mesh%flag_bc_analytic_elasticiso   = .true.
      if(abs(mesh_tag_bc(20,1)) > 0) mesh%flag_bc_planewave_vtiYSH      = .true.
      if(abs(mesh_tag_bc(21,1)) > 0) mesh%flag_bc_planewave_vtiYSV      = .true.
      if(abs(mesh_tag_bc(22,1)) > 0) mesh%flag_bc_planewave_vtiYqP      = .true.
      if(abs(mesh_tag_bc(23,1)) > 0) mesh%flag_bc_planewave_scatt       = .true.
      mesh%tag_bc_pml_radius = mesh_tag_bc(10,:)
      !! adjust to have different, NEGATIVE tags
      mesh%tag_bc_pml        = tag_pml - mesh_tag_bc(9,:)
      
      !! we also generate the array of BC, which is much simpler to work
      !! with later on, even though it is not optimal as not all bc exist,
      !! hence we allocate slightly too much.
      allocate(mesh%tag_bc_array(n_boundary_tag,mesh%tag_n))
      mesh%tag_bc_array = 0
      !! except for PML
      mesh%tag_bc_array( 9,:)= tag_pml - mesh_tag_bc(9,:) ! plm
      do k=1,n_boundary_tag
        if(k .ne. 9) then
          mesh%tag_bc_array(k,:)= mesh_tag_bc(k,:)
        end if
      end do
      
      
      
    endif
    !! -----------------------------------------------------------------
    !! read mesh and partition
    select case(mesh%dim_domain)
      case(1)
        !! mesh read
        call mesh_read1d(ctx_paral,mesh,flag_init,ctx_err)
        !! mesh partitioning
        if(.not. flag_init) then
          call mesh_partition(ctx_paral,mesh,ctx_err)
        endif
      case(2)
        !! mesh read
        call mesh_read2d(ctx_paral,mesh,flag_init,ctx_err)
        !! mesh partitioning
        if(.not. flag_init) then
          call mesh_partition(ctx_paral,mesh,ctx_err)
        endif
      case (3)
        !! mesh read
        call mesh_read3d(ctx_paral,mesh,flag_init,ctx_err)
        !! mesh partitioning
        if(.not. flag_init) then
          call mesh_partition(ctx_paral,mesh,ctx_err)
        endif

      case default
         ctx_err%msg   = "** ERROR: dimension must be 2/3 [domain_process] **"
         ctx_err%ierr  = -1
         ctx_err%critic=.true.
         call raise_error(ctx_err)

    end select

    return
  end subroutine domain_init

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  The subroutine reads the input 1D mesh files
  !> @details
  !>  only supports SEGMENT format for now
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[in]   flag_init     : init only or allocate
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read1d(ctx_paral,ctx_mesh,flag_init,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: ctx_mesh
    logical            ,intent(in)   :: flag_init
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    select case(trim(adjustl(ctx_mesh%format_mesh)))
      case('segment')
        if(flag_init) then
          call mesh_read_init_segment(ctx_paral,ctx_mesh,ctx_err)
        else
          call mesh_read_segment(ctx_paral,ctx_mesh,ctx_err)
        endif
        !! error check
        if(ctx_err%ierr.ne.0)then
          ctx_err%msg   = "** ERROR DURING READ_MESH_FILES1D SEGMENT: "//&
                          "[mesh_read1d] " // trim(adjustl(ctx_err%msg))
          call raise_error(ctx_err)
        endif

      !! unrecognized format
      case default
        ctx_err%msg   = "** INPUT MESH FORMAT: "                    // &
                        trim(adjustl(ctx_mesh%format_mesh))         // &
                        " NOT RECOGNIZED ** " // &
                        "=> ONLY SEGMENT IS SUPPORTED IN 1D FOR NOW "
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine mesh_read1d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  The subroutine reads the input 2D mesh files
  !> @details
  !>  supports TRIANGLE and MESH format for now
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[in]   flag_init     : init only or allocate
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read2d(ctx_paral,ctx_mesh,flag_init,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: ctx_mesh
    logical            ,intent(in)   :: flag_init
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    select case(trim(adjustl(ctx_mesh%format_mesh)))
      case('triangle')
        if(flag_init) then
          call mesh_read_init_triangle(ctx_paral,ctx_mesh,ctx_err)
        else
          call mesh_read_triangle(ctx_paral,ctx_mesh,ctx_err)
        endif
        !! error check
        if(ctx_err%ierr.ne.0)then
          ctx_err%msg   = "** ERROR DURING READ_MESH_FILES2D TRIANGLE: "// &
                          "[mesh_read2d] " // trim(adjustl(ctx_err%msg))
          call raise_error(ctx_err)
        endif
      case('mesh','medit')
        if(flag_init) then
          call mesh_read_init_meshformat2d(ctx_paral,ctx_mesh,ctx_err)
        else
          call mesh_read_meshformat2d(ctx_paral,ctx_mesh,ctx_err)
        endif
        !! error check
        if(ctx_err%ierr.ne.0)then
          ctx_err%msg   = "** ERROR DURING READ_MESH_FILES2D .MESH: "// &
                          "[mesh_read2d] " // trim(adjustl(ctx_err%msg))
          call raise_error(ctx_err)
        endif

      !! unrecognized format
      case default
        ctx_err%msg   = "** INPUT MESH FORMAT: "                    // &
                        trim(adjustl(ctx_mesh%format_mesh))         // &
                        " NOT RECOGNIZED ** " // &
                        "=> ONLY TRIANGLE and MESH SUPPORTED IN 2D FOR NOW "
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine mesh_read2d

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !>  The subroutine reads the input 3D mesh files
  !> @details
  !>  supports TETGEN and MESH format for now
  !
  !> @param[in]   ctx_paral     : parallel context
  !> @param[inout] mesh          : mesh context
  !> @param[in]   flag_init     : init only or allocate
  !> @param[out]  ctx_err       : error context
  !----------------------------------------------------------------------------
  subroutine mesh_read3d(ctx_paral,ctx_mesh,flag_init,ctx_err)

    implicit none
    type(t_parallelism),intent(in)   :: ctx_paral
    type(t_domain)     ,intent(inout):: ctx_mesh
    logical            ,intent(in)   :: flag_init
    type(t_error)      ,intent(out)  :: ctx_err

    ctx_err%ierr  = 0

    select case(trim(adjustl(ctx_mesh%format_mesh)))
      case('tetgen')
        if(flag_init) then
          call mesh_read_init_tetgen(ctx_paral,ctx_mesh,ctx_err)
        else
          call mesh_read_tetgen(ctx_paral,ctx_mesh,ctx_err)
        endif
        !! error check
        if(ctx_err%ierr.ne.0)then
          ctx_err%msg   = "** ERROR DURING READ_MESH_FILES3D TETGEN: "//&
                          "[mesh_read3d] " // trim(adjustl(ctx_err%msg))
          call raise_error(ctx_err)
        endif
      case('mesh','medit')
        if(flag_init) then
          call mesh_read_init_meshformat3d(ctx_paral,ctx_mesh,ctx_err)
        else
          call mesh_read_meshformat3d(ctx_paral,ctx_mesh,ctx_err)
        endif
        !! error check
        if(ctx_err%ierr.ne.0)then
          ctx_err%msg   = "** ERROR DURING READ_MESH_FILES3D .MESH: " //&
                          "[mesh_read3d] " // trim(adjustl(ctx_err%msg))
          call raise_error(ctx_err)
        endif

      !! unrecognized format
      case default
        ctx_err%msg   = "** INPUT MESH FORMAT: "                    //  &
                        trim(adjustl(ctx_mesh%format_mesh))         //  &
                        " NOT RECOGNIZED ** "                       //  &
                        "=> ONLY TETGEN and MESH SUPPORTED IN 3D FOR NOW "
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select

    return
  end subroutine mesh_read3d

end module m_domain_process
