!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_crosscorrelation_init.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to initialize information for 
!> the computation of the the cross correlation.
!
!------------------------------------------------------------------------------
module m_crosscorrelation_init
  
  use m_raise_error,              only : raise_error, t_error
  use m_distribute_error,         only : distribute_error
  !! main
  use m_ctx_domain,               only : t_domain
  use m_ctx_parallelism,          only : t_parallelism
  use m_allreduce_min,            only : allreduce_min
  use m_allreduce_max,            only : allreduce_max
  !! models i/o
  use m_read_parameters_model,    only : read_parameters_model
  use m_ctx_model_representation, only : t_model_param,                 &
                                         model_representation_clean,    &
                                         tag_MODEL_PPOLY, tag_MODEL_SEP,&
                                         tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_model_read,               only : model_read_record_sep,         &
                                         model_read_per_cell,           &
                                         model_read_to_polynomial
  use m_model_init,               only : model_init_representation_format
  use m_model_representation_dof, only : model_representation_dof_init
  use m_read_parameter,           only : read_parameter
  !! cross-correlation
  use m_ctx_crosscorrelation,     only : t_crosscorrelation
  use m_crosscorrelation_printer, only : print_crosscovariance_src_info

  implicit none

  private  
  public :: crosscorrelation_init
    
  contains

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> initialize the context for the cross correlation computation
  !
  !> @param[in]    ctx_paral      : parallel context
  !> @param[inout] parameter_file : parameter file
  !> @param[inout] ctx_cc   : context for the cross-correlation
  !> @param[inout] ctx_err        : error context
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_init(ctx_paral,ctx_domain,parameter_file, &
                                   ctx_cc,ctx_err)
  
    implicit none

    type(t_parallelism)     ,intent(in)   :: ctx_paral
    type(t_domain)          ,intent(in)   :: ctx_domain
    character(len=*)        ,intent(in)   :: parameter_file
    type(t_crosscorrelation),intent(inout):: ctx_cc
    type(t_error)           ,intent(out)  :: ctx_err
    !! local
    character(len=32)          :: mformatR,mformatI,strmodel_real,strmodel_imag
    character(len=512)         :: mpath_real,mpath_imag,str
    integer                    :: ndof,nval
    real(kind=8)               :: mcste_real,mcste_imag,mscale_real,mscale_imag
    real(kind=8)               :: mmin_real,mmin_imag,mmax_real,mmax_imag
    logical                    :: flag_extend0
    real(kind=8),allocatable   :: model_real(:),model_imag(:)

    ctx_err%ierr = 0

    !! -----------------------------------------------------------------
    !! General initialization
    !! -----------------------------------------------------------------
    ctx_cc%dim_domain = ctx_domain%dim_domain
    ctx_cc%n_cell_loc = ctx_domain%n_cell_loc
    ctx_cc%format_disc= ''

    !! -----------------------------------------------------------------
    !! We read the information regarding the cross-covariance
    !! of the source term. This is simply considered as a model
    !! parameter, in the sam way as physical properties are dealt
    !! with.
    !! -----------------------------------------------------------------

    !! model representation --------------------------------------------
    call read_parameter(parameter_file,                                 &
                        'source-cross-covariance_representation',       &
                        'piecewise-constant',str,nval)
    call model_init_representation_format(trim(adjustl(str)),           &
                                          ctx_cc%format_disc,ctx_err)
    
    !! -----------------------------------------------------------------
    !! main array
    ndof = ctx_cc%n_cell_loc
    allocate(ctx_cc%src_crosscovariance_real%pconstant(ndof))
    allocate(ctx_cc%src_crosscovariance_imag%pconstant(ndof))

    allocate(model_real(ndof)) !! working array
    allocate(model_imag(ndof))
    model_real = 0.d0
    model_imag = 0.d0
    strmodel_real = 'source-cross-covariance-real'

    call read_parameters_model(parameter_file,strmodel_real,mformatR,mpath_real, &
                           mcste_real,mscale_real,mmin_real,mmax_real,       &
                           ctx_cc%src_crosscovariance_real%compression_flag, &
                           ctx_cc%src_crosscovariance_real%compression_tol,  &
                           ctx_cc%src_crosscovariance_real%compression_str,  &
                           ctx_cc%src_crosscovariance_real%compression_box_x,&
                           ctx_cc%src_crosscovariance_real%compression_box_y,&
                           ctx_cc%src_crosscovariance_real%compression_box_z,&
                           ctx_cc%src_crosscovariance_real%ppoly_order,      &
                           ctx_err)

    strmodel_imag = 'source-cross-covariance-imag'
    call read_parameters_model(parameter_file,strmodel_imag,mformatI,mpath_imag, &
                            mcste_imag,mscale_imag,mmin_imag,mmax_imag,       &
                            ctx_cc%src_crosscovariance_imag%compression_flag, &
                            ctx_cc%src_crosscovariance_imag%compression_tol,  &
                            ctx_cc%src_crosscovariance_imag%compression_str,  &
                            ctx_cc%src_crosscovariance_imag%compression_box_x,&
                            ctx_cc%src_crosscovariance_imag%compression_box_y,&
                            ctx_cc%src_crosscovariance_imag%compression_box_z,&
                            ctx_cc%src_crosscovariance_imag%ppoly_order,      &
                            ctx_err)
    
    if(ctx_err%ierr .ne. 0) then
      ctx_err%msg ="** Error durring the reading of the cross-covariance "//&
                   "source term. [crosscorrelation_init]"
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    !! initialize the model representation with information found
    flag_extend0 = .true. !! we extend with 0s if the domain read is too small.
    
    !! 1. piecewise-constant -------------------------------------------
    call model_read_per_cell(ctx_domain,mformatR,mpath_real,mcste_real,mscale_real,&
                             model_real,flag_extend0,ctx_err)
    call model_read_per_cell(ctx_domain,mformatI,mpath_imag,mcste_imag,mscale_imag,&
                             model_imag,flag_extend0,ctx_err)

    call distribute_error(ctx_err,ctx_paral%communicator) !! for non-shared error
    !! 2. other format -------------------------------------------------
    select case (trim(adjustl(ctx_cc%format_disc)))
      case(tag_MODEL_PCONSTANT)
        !! nothing to do.
      
      case(tag_MODEL_PPOLY)
        !! compute the piecewise polynomial representation: withouth 
        !! compression, we have one polynomial per cell. 
        !! we generate the polynomials and save them in the 
        !! representation context.
        allocate(ctx_cc%src_crosscovariance_real%ppoly(ndof))
        call model_read_to_polynomial(ctx_domain,mformatR,mpath_real,             &
                                      mscale_real,mmin_real,mmax_real,model_real, &
                                      ctx_cc%src_crosscovariance_real%ppoly_order,&
                                      ctx_cc%src_crosscovariance_real%ppoly,ctx_err)

        allocate(ctx_cc%src_crosscovariance_imag%ppoly(ndof))
        call model_read_to_polynomial(ctx_domain,mformatI,mpath_imag,             &
                                      mscale_imag,mmin_imag,mmax_imag,model_imag, &
                                      ctx_cc%src_crosscovariance_imag%ppoly_order,&
                                      ctx_cc%src_crosscovariance_imag%ppoly,ctx_err)
        !! possibility for non-shared error
        call distribute_error(ctx_err,ctx_paral%communicator)    
      
      !! if we have dof: either we start as a constant, either we
      !! read from the sep file so we should keep it. Therefore, 
      !! let us keep the sep model in all cases.
      case(tag_MODEL_SEP, tag_MODEL_DOF)
        !! we save the complete sep info
        call model_read_record_sep(ctx_domain,mformatR,mpath_real,mscale_real,&
                    mmin_real,mmax_real,model_real,                           &
                    ctx_cc%src_crosscovariance_real%param_sep%str_sep_format, &
                    ctx_cc%src_crosscovariance_real%param_sep%ox,             &
                    ctx_cc%src_crosscovariance_real%param_sep%oy,             &
                    ctx_cc%src_crosscovariance_real%param_sep%oz,             &
                    ctx_cc%src_crosscovariance_real%param_sep%dx,             &
                    ctx_cc%src_crosscovariance_real%param_sep%dy,             &
                    ctx_cc%src_crosscovariance_real%param_sep%dz,             &
                    ctx_cc%src_crosscovariance_real%param_sep%nx,             &
                    ctx_cc%src_crosscovariance_real%param_sep%ny,             &
                    ctx_cc%src_crosscovariance_real%param_sep%nz,             &
                    ctx_cc%src_crosscovariance_real%param_sep%sep_model,      &
                    ctx_cc%src_crosscovariance_real%param_sep%sep_model_spherical,&
                    ctx_err)
        call model_read_record_sep(ctx_domain,mformatI,mpath_imag,mscale_imag,&
                    mmin_imag,mmax_imag,model_imag,                           &
                    ctx_cc%src_crosscovariance_imag%param_sep%str_sep_format, &
                    ctx_cc%src_crosscovariance_imag%param_sep%ox,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%oy,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%oz,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%dx,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%dy,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%dz,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%nx,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%ny,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%nz,             &
                    ctx_cc%src_crosscovariance_imag%param_sep%sep_model,      &
                    ctx_cc%src_crosscovariance_imag%param_sep%sep_model_spherical,&
                    ctx_err)
      case default
        ctx_err%ierr=-201
        ctx_err%msg ="** ERROR: unrecognized model representation '"    &
                     //trim(adjustl(ctx_cc%format_disc))//"' " // &
                     "in [m_cross-correlation_init]"
        ctx_err%critic=.true.
        call raise_error(ctx_err)
    end select
    !! ---------------------------------------------------------------
    !! in the case where we have the model per dof, we need to convert
    !! the sep to the dof values. 
    !! ---------------------------------------------------------------

    ctx_cc%src_crosscovariance_real%param_dof%order = -1
    ctx_cc%src_crosscovariance_real%param_dof%ndof  =  0

    ctx_cc%src_crosscovariance_imag%param_dof%order = -1
    ctx_cc%src_crosscovariance_imag%param_dof%ndof  =  0

    if(trim(adjustl(ctx_cc%format_disc)) == tag_MODEL_DOF) then

      allocate(ctx_cc%src_crosscovariance_real%param_dof%mval(ctx_cc%n_cell_loc))
      allocate(ctx_cc%src_crosscovariance_imag%param_dof%mval(ctx_cc%n_cell_loc))
      !! for now we assume same order for all.........................
      ctx_cc%src_crosscovariance_real%param_dof%order =                      &
             ctx_cc%src_crosscovariance_real%ppoly_order
      ctx_cc%src_crosscovariance_imag%param_dof%order =                      &
             ctx_cc%src_crosscovariance_imag%ppoly_order

      call model_representation_dof_init(ctx_domain,                    &
                                         ctx_cc%src_crosscovariance_real,ctx_err)
      call model_representation_dof_init(ctx_domain,                    &
                                         ctx_cc%src_crosscovariance_imag,ctx_err)
      !! non-shared error
      call distribute_error(ctx_err,ctx_paral%communicator)
    end if
    !! maybe be changed from above operations.
    ctx_cc%src_crosscovariance_real%pconstant(:) = model_real(:)
    ctx_cc%src_crosscovariance_imag%pconstant(:) = model_imag(:)

    call allreduce_min(minval(model_real),mmin_real,1,ctx_paral%communicator,ctx_err)
    call allreduce_min(minval(model_imag),mmin_imag,1,ctx_paral%communicator,ctx_err)
    call allreduce_max(maxval(model_real),mmax_real,1,ctx_paral%communicator,ctx_err)
    call allreduce_max(maxval(model_imag),mmax_imag,1,ctx_paral%communicator,ctx_err)

    !! clean
    deallocate(model_real)
    deallocate(model_imag)

    !! print infos here
    call print_crosscovariance_src_info(ctx_paral,ctx_cc%dim_domain,    &
                                        ctx_cc%format_disc,mmin_real,   &
                                        mmax_real,mmin_imag,mmax_imag)

    return

  end subroutine crosscorrelation_init

end module m_crosscorrelation_init
