!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_crosscorrelation_printer.f90
!
!> @author 
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION: 
!> @brief
!> the module deals with screen info printing for the cross-correlation
!> of signals in the context of passive imaging
!
!------------------------------------------------------------------------------
module m_crosscorrelation_printer

  use m_ctx_parallelism,      only : t_parallelism
  use m_raise_error,          only : t_error, raise_error
  use m_print_basics,         only : separator,indicator
  implicit none
  
  private
  public :: print_crosscovariance_src_info
    
  contains  

  !---------------------------------------------------------------------------- 
  ! DESCRIPTION: 
  !> @brief
  !> print info for the cross-correlation source covariance
  !
  !> @param[in]   ctx_paral       : parallel context
  !> @param[in]   n_data          : number of data
  !> @param[out]  ctx_err         : error context
  !----------------------------------------------------------------------------
  subroutine print_crosscovariance_src_info(ctx_paral,dim_domain,format_disc, &
                                            mmin_real,mmax_real,mmin_imag,    &
                                            mmax_imag)

    type(t_parallelism)           ,intent(in)   :: ctx_paral
    integer                       ,intent(in)   :: dim_domain
    character(len=*)              ,intent(in)   :: format_disc
    real(kind=8)                  ,intent(in)   :: mmin_real,mmax_real
    real(kind=8)                  ,intent(in)   :: mmin_imag,mmax_imag

    
    if(ctx_paral%master) then
      ! write(6,'(a)')    separator
      write(6,'(a,i0)') "- Cross-Correlation of signals in dimension ",dim_domain
      write(6,'(a,a)' ) "   - Source covariance model format: ", trim(adjustl(format_disc))
      write(6,'(a,es10.3,a,es10.3,a)' ) &
                        "   - Interval for the real part: (",mmin_real,", ",mmax_real,")"
      write(6,'(a,es10.3,a,es10.3,a)' ) &
                        "   - Interval for the imag part: (",mmin_imag,", ",mmax_imag,")"
    end if
      
    return
  end subroutine print_crosscovariance_src_info

end module m_crosscorrelation_printer
