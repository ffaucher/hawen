!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_crosscorrelation_evalsrc.f90
!
!> @author
!> F. Faucher [Inria Bordeaux -- Team Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to evaluate the source cross-covariance term,
!> depending on the representation it has, e.g., piecewise-constant
!> or piecewise polynomial.
!>
!
!------------------------------------------------------------------------------
module m_crosscorrelation_evalsrc

  !! module used -------------------------------------------------------
  use m_raise_error,                 only: raise_error, t_error
  use m_define_precision,            only: IKIND_MESH, RKIND_POL
  use m_polynomial,                  only: polynomial_eval
  use m_dg_lagrange_dof_coordinates, only: discretization_dof_coo_ref_elem
  use m_ctx_domain,                  only: t_domain
  use m_ctx_model_representation,    only: t_model_param
  use m_ctx_model_representation,    only: tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                           tag_MODEL_PCONSTANT, tag_MODEL_DOF
  use m_ctx_model_representation,    only: model_representation_eval_sep
  use m_model_representation_dof,    only: model_representation_dof_eval_refelem
  use m_model_parameter_evaluation,  only: model_parameter_evaluation
  !! -------------------------------------------------------------------

  implicit none

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise constant representation
  interface crosscorrelation_evalsrc_pconstant
     module procedure crosscorrelation_evalsrc_pconstant_real4
     module procedure crosscorrelation_evalsrc_pconstant_real8
  end interface crosscorrelation_evalsrc_pconstant
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise polynomials representation
  interface crosscorrelation_evalsrc_ppoly
     module procedure crosscorrelation_evalsrc_ppoly_real4
     module procedure crosscorrelation_evalsrc_ppoly_real8
  end interface crosscorrelation_evalsrc_ppoly
  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !>  The subroutine computes the piecewise lagrange representation
  interface crosscorrelation_evalsrc_pdof
     module procedure crosscorrelation_evalsrc_pdof_real4
     module procedure crosscorrelation_evalsrc_pdof_real8
  end interface crosscorrelation_evalsrc_pdof


  private
  public  :: crosscorrelation_evalsrc
  public  :: crosscorrelation_evalsrc_pconstant
  public  :: crosscorrelation_evalsrc_ppoly
  public  :: crosscorrelation_evalsrc_pdof

  contains


  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> Evaluate the source cross-covariance according to how it was saved.
  !
  !> @param[in]    ctx_model    : model parameter context
  !> @param[in]    format_model : format of the representation
  !> @param[in]    dim_domain   : domain dimension
  !> @param[in]    i_cell       : which cell
  !> @param[in]    xpt_refelem  : position on the reference element
  !> @param[in]    xpt_gb       : position on the global    element
  !> @param[out]   v            : output value at the given position
  !> @param[inout] ctx_err      : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_evalsrc(ctx_model_param,format_model,dim_domain,&
                                      i_cell,xpt_refelem,xpt_gb,v,ctx_err)

    implicit none

    type(t_model_param)     ,intent(in)   :: ctx_model_param
    character(len=*)        ,intent(in)   :: format_model
    integer                 ,intent(in)   :: dim_domain
    integer(kind=IKIND_MESH),intent(in)   :: i_cell
    real   (kind=8)         ,intent(in)   :: xpt_refelem(:)
    real   (kind=8)         ,intent(in)   :: xpt_gb     (:)
    real   (kind=8)         ,intent(out)  :: v
    type(t_error)           ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    
    v = 0.d0
    call model_parameter_evaluation(ctx_model_param,format_model,       &
                                    dim_domain,i_cell,xpt_refelem,      &
                                    xpt_gb,v,ctx_err)
    !! possibility for non-shared error here.
    return
    
  end subroutine crosscorrelation_evalsrc



  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model parameter
  !> @param[in]    model_out      : output model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_evalsrc_pconstant_real4(ctx_model_param,  &
                                                      model_out,ctx_err)
    implicit none

    type(t_model_param)                ,intent(in)   :: ctx_model_param
    real(kind=4), allocatable          ,intent(inout):: model_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out(:) = real(ctx_model_param%pconstant(:),kind=4)
    
    return
  end subroutine crosscorrelation_evalsrc_pconstant_real4
  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> constant model representation
  !
  !> @param[in]    ctx_model      : type model parameter
  !> @param[in]    model_out      : output model
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_evalsrc_pconstant_real8(ctx_model_param,  &
                                                      model_out,ctx_err)
    implicit none

    type(t_model_param)                ,intent(in)   :: ctx_model_param
    real(kind=8), allocatable          ,intent(inout):: model_out(:)
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    model_out(:) = real(ctx_model_param%pconstant(:),kind=8)
    
    return
  end subroutine crosscorrelation_evalsrc_pconstant_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type domain
  !> @param[in]    ctx_model      : type model parameter
  !> @param[in]    model_out      : output model
  !> @param[in]    order          : output order of the polynomial
  !> @param[in]    ndof_gb        : output number of dof global
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_evalsrc_ppoly_real4(ctx_domain,           &
                                   ctx_model_param,model_out,order,     &
                                   ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model_param)                ,intent(in)   :: ctx_model_param
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! unused
    allocate(xpt_gb(ctx_domain%dim_domain))
    xpt_gb = 0.d0

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = ctx_model_param%ppoly_order
    ndof_vol = ctx_model_param%ppoly(1)%size_coeff
    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: the piecewise-polynomial source " //   &
                      "covariance must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo, &
                                         ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call crosscorrelation_evalsrc(ctx_model_param,tag_MODEL_PPOLY,  &
                                      ctx_domain%dim_domain,i_cell,     &
                                      dof_coo(:,k),xpt_gb,tempval,ctx_err)
        model_out(i1 + k) = real(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    deallocate(xpt_gb)
    !! -----------------------------------------------------------------  
    
    return
  end subroutine crosscorrelation_evalsrc_ppoly_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> polynomial model representation
  !
  !> @param[in]    ctx_model      : type domain
  !> @param[in]    ctx_model      : type model parameter
  !> @param[in]    model_out      : output model
  !> @param[in]    order          : output order of the polynomial
  !> @param[in]    ndof_gb        : output number of dof global
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_evalsrc_ppoly_real8(ctx_domain,           &
                                   ctx_model_param,model_out,order,     &
                                   ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model_param)                ,intent(in)   :: ctx_model_param
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i1
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! unused
    allocate(xpt_gb(ctx_domain%dim_domain))
    xpt_gb = 0.d0

    !! -----------------------------------------------------------------
    !! order is the max of all orders of the models 
    order    = ctx_model_param%ppoly_order
    ndof_vol = ctx_model_param%ppoly(1)%size_coeff
    !! error if the max order is <= 0 ----------------------------------
    if(order <= 0) then
      ctx_err%msg   = "** ERROR: the piecewise-polynomial source " //   &
                      "covariance must have order > 0 [model_get] **"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      return
    end if

    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    !! -----------------------------------------------------------------
    !! we compute the models at the dof positions here
    allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
    !! Because we use the reference element, it does not depend 
    !! on the cell
    !! -----------------------------------------------------------------
    call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol,     &
                                         ctx_domain%dim_domain,dof_coo, &
                                         ctx_err)

    !! loop over all cell.
    do i_cell = 1, ctx_domain%n_cell_loc
      if(ctx_err%ierr .ne. 0) return
      i1 = (i_cell-1)*ndof_vol
      !! loop over each dof
      do k=1, ndof_vol
        if(ctx_err%ierr .ne. 0) return
        call crosscorrelation_evalsrc(ctx_model_param,tag_MODEL_PPOLY,  &
                                      ctx_domain%dim_domain,i_cell,     &
                                      dof_coo(:,k),xpt_gb,tempval,ctx_err)
        model_out(i1 + k) = dble(tempval)
      end do
      
    end do !! end loop over cells --------------------------------------
    
    deallocate(dof_coo)
    deallocate(xpt_gb)
    !! -----------------------------------------------------------------  
    
    return
  end subroutine crosscorrelation_evalsrc_ppoly_real8

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> dof model representation
  !
  !> @param[in]    ctx_model      : type domain
  !> @param[in]    ctx_model      : type model parameter
  !> @param[in]    model_out      : output model
  !> @param[in]    order          : output order of the polynomial
  !> @param[in]    ndof_gb        : output number of dof global
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_evalsrc_pdof_real4(ctx_domain,            &
                                      ctx_model_param,model_out,        &
                                      order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model_param)                ,intent(in)   :: ctx_model_param
    real(kind=4)     ,allocatable      ,intent(inout):: model_out(:)
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    real(kind=8)        , allocatable :: xpt_refelem(:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = ctx_model_param%param_dof%ndof
    order    = ctx_model_param%param_dof%order
    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    !! unused we force p-constant
    allocate(xpt_gb     (ctx_domain%dim_domain))
    xpt_gb = 0.d0
    if(ndof_vol == 1) then
      allocate(xpt_refelem(ctx_domain%dim_domain))
      xpt_refelem = 0.d0
      do i_cell = 1, ctx_domain%n_cell_loc
        call crosscorrelation_evalsrc(ctx_model_param,tag_MODEL_PCONSTANT,&
                                      ctx_domain%dim_domain,i_cell,       &
                                      xpt_refelem,xpt_gb,tempval,ctx_err)
        model_out(i_cell) = real(tempval,kind=4)
      end do
      deallocate(xpt_refelem)
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol, &
                                             ctx_domain%dim_domain,     &
                                             dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        do k=1,ndof_vol
          call crosscorrelation_evalsrc(ctx_model_param,tag_MODEL_DOF,  &
                                        ctx_domain%dim_domain,i_cell,   &
                                        dof_coo(:,k),xpt_gb,tempval,ctx_err)
          out_val(k) = tempval
        end do
        
        model_out(i_gb1:i_gb2) = real(out_val(:),kind=4)
      end do
      deallocate(dof_coo)
      deallocate(out_val)
    end if
    
    return
  end subroutine crosscorrelation_evalsrc_pdof_real4

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> get model coefficients for a specific field, for piecewise
  !> dof model representation
  !
  !> @param[in]    ctx_model      : type domain
  !> @param[in]    ctx_model      : type model parameter
  !> @param[in]    model_out      : output model
  !> @param[in]    order          : output order of the polynomial
  !> @param[in]    ndof_gb        : output number of dof global
  !> @param[inout] ctx_err        : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_evalsrc_pdof_real8(ctx_domain,            &
                                      ctx_model_param,model_out,        &
                                      order,ndof_gb,ctx_err)

    implicit none

    type(t_domain)                     ,intent(in)   :: ctx_domain
    type(t_model_param)                ,intent(in)   :: ctx_model_param
    real(kind=8)     ,allocatable      ,intent(inout):: model_out(:)
    integer                            ,intent(out)  :: order
    integer                            ,intent(out)  :: ndof_gb
    type(t_error)                      ,intent(inout):: ctx_err
    !! -----------------------------------------------------------------
    integer(kind=IKIND_MESH)          :: i_cell,i_gb1,i_gb2
    integer                           :: ndof_vol,k
    real(kind=8)                      :: tempval
    real(kind=8)        , allocatable :: dof_coo(:,:)
    real(kind=8)        , allocatable :: out_val(:)
    real(kind=8)        , allocatable :: xpt_refelem(:),xpt_gb(:)
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0

    !! -----------------------------------------------------------------
    !! get the maximal order and corresponding ndof
    ndof_vol = ctx_model_param%param_dof%ndof
    order    = ctx_model_param%param_dof%order
    ndof_gb = int(ndof_vol * ctx_domain%n_cell_loc)
    
    !! allocate with size 
    allocate(model_out(ndof_gb))
    model_out = 0.d0
    
    !! if ndof = 1 ==> piecewise constant
    !! unused we force p-constant
    allocate(xpt_gb     (ctx_domain%dim_domain))
    xpt_gb = 0.d0
    if(ndof_vol == 1) then
      allocate(xpt_refelem(ctx_domain%dim_domain))
      xpt_refelem = 0.d0
      do i_cell = 1, ctx_domain%n_cell_loc
        call crosscorrelation_evalsrc(ctx_model_param,tag_MODEL_PCONSTANT,&
                                      ctx_domain%dim_domain,i_cell,       &
                                      xpt_refelem,xpt_gb,tempval,ctx_err)
        model_out(i_cell) = real(tempval,kind=8)
      end do
      deallocate(xpt_refelem)
    !! -----------------------------------------------------------------
    else     
      allocate(dof_coo(ctx_domain%dim_domain,ndof_vol))
      allocate(out_val(ndof_vol))
      do i_cell = 1, ctx_domain%n_cell_loc
        i_gb1 = (i_cell-1) * ndof_vol + 1
        i_gb2 = (i_cell-1) * ndof_vol + ndof_vol
        !! get the dof position for the order max
        dof_coo=0.d0
        call discretization_dof_coo_ref_elem(ctx_domain,order,ndof_vol, &
                                             ctx_domain%dim_domain,     &
                                             dof_coo,ctx_err)
        !! eval for all these points
        out_val = 0.d0
        do k=1,ndof_vol
          call crosscorrelation_evalsrc(ctx_model_param,tag_MODEL_DOF,  &
                                        ctx_domain%dim_domain,i_cell,   &
                                        dof_coo(:,k),xpt_gb,tempval,ctx_err)
          out_val(k) = tempval
        end do
        
        model_out(i_gb1:i_gb2) = real(out_val(:),kind=8)
      end do
      deallocate(dof_coo)
      deallocate(out_val)
    end if
    
    return
  end subroutine crosscorrelation_evalsrc_pdof_real8

end module m_crosscorrelation_evalsrc
