!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_crosscorrelation_save.f90
!
!> @author
!> F. Faucher [Inria Bordeaux]
!
! DESCRIPTION:
!> @brief
!> the module is used to save cross-correlation context infos,
!> for instance the source covariance term.
!
!------------------------------------------------------------------------------
module m_crosscorrelation_save

  !! module used -------------------------------------------------------
  use m_raise_error,              only: raise_error, t_error
  use m_distribute_error,         only: distribute_error
  use m_ctx_parallelism,          only: t_parallelism
  use m_ctx_domain,               only: t_domain
  use m_ctx_discretization,       only: t_discretization
  use m_ctx_model_representation, only: t_model_param
  use m_crosscorrelation_evalsrc, only: crosscorrelation_evalsrc_pconstant, &
                                        crosscorrelation_evalsrc_ppoly,     &
                                        crosscorrelation_evalsrc_pdof
  use m_ctx_io,                   only: t_io
  use m_array_save,               only: array_save
  use m_io_filename,              only: io_filename
  use m_ctx_model_representation, only: tag_MODEL_PCONSTANT,            &
                                        tag_MODEL_PPOLY, tag_MODEL_SEP, &
                                        tag_MODEL_DOF
  !! -------------------------------------------------------------------
  implicit none

  private
  public  :: crosscorrelation_save_covariance_src

  contains

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> save the cross-covariance source term model
  !
  !> @param[in]    ctx_paral           : type model
  !> @param[in]    ctx_domain          : context domain
  !> @param[in]    ctx_discretization  : context discretization
  !> @param[in]    ctx_model           : context model parameter
  !> @param[in]    ctx_io              : context io
  !> @param[in]    nmodel              : number of models
  !> @param[in]    name_model(:)       : model names
  !> @param[in]    iteration           : current iteration
  !> @param[in]    freq                : current frequency
  !> @param[inout] ctx_err             : context error
  !----------------------------------------------------------------------------
  subroutine crosscorrelation_save_covariance_src(ctx_paral,ctx_domain, &
                                   ctx_discretization,ctx_model_param,  &
                                   ctx_io,format_disc,iteration,freq,   &
                                   ctx_err)

    implicit none

    type(t_parallelism)          ,intent(in)   :: ctx_paral
    type(t_domain)               ,intent(in)   :: ctx_domain
    type(t_discretization)       ,intent(in)   :: ctx_discretization
    type(t_model_param)          ,intent(in)   :: ctx_model_param
    type(t_io)                   ,intent(in)   :: ctx_io
    character(len=*)             ,intent(in)   :: format_disc
    integer                      ,intent(in)   :: iteration
    complex(kind=8)              ,intent(in)   :: freq
    type(t_error)                ,intent(inout):: ctx_err
    !! local
    integer                        :: imodel,phantom_i,cste_order,ndof_gb
    integer                        :: nmodel
    real,allocatable               :: model(:,:),model_loc(:)
    real(kind=8)                   :: phantom_r(2)
    character(len=32)              :: name_loc
    character(len=128),allocatable :: fieldname(:),outname(:),name_model(:)
    character(len=128)             :: outname_gb
    logical                        :: flag_freq,flag_mode
    
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    !! there is only one model: the source covariance term.
    nmodel = 1 
    
    allocate(name_model(nmodel))
    name_model(1) = 'source-covariance'
    !! no frequency, and no mode for the model
    flag_freq = .false. 
    flag_mode = .false. 
    phantom_i = 0 ; phantom_r(:) = 0.0

    !! -----------------------------------------------------------------
    allocate(fieldname (nmodel))
    allocate(outname   (nmodel))
    !! depends on the model representation
    select case(trim(adjustl(format_disc)))
      
      !! ---------------------------------------------------------------
      case(tag_MODEL_PCONSTANT,tag_MODEL_SEP)

        !! evaluate the model per cell.
        allocate(model_loc(ctx_domain%n_cell_loc))
        allocate(model    (ctx_domain%n_cell_loc,nmodel))
        do imodel=1,nmodel
          name_loc =trim(adjustl(name_model(imodel)))
          model_loc= 0.0
          !! eval piecewise constant per cell: 
          !! WE SAVE REAL 4 HERE, BECAUSE OF MODEL_LOC, WE CAN JUST 
          !! CHANGE MODEL_LOC TO REAL 8 TO SAVE IN DOUBLE.
          !! ***********************************************************       
          call crosscorrelation_evalsrc_pconstant(ctx_model_param,      &
                                                  model_loc,ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator) 
          model(:,imodel)  =model_loc
          fieldname(imodel)=trim(adjustl(name_loc))
        end do
        deallocate(model_loc)

        !! filenames >>>>>>>>>>>>>
        outname=''
        call io_filename(outname,outname_gb,fieldname,                  &
                         "source-covariance",nmodel,                    &
                         flag_freq,flag_mode,phantom_r,phantom_i,       &
                         ctx_err,o_iter=iteration)
        call distribute_error(ctx_err,ctx_paral%communicator) 
        
        !! save >>>>>>>>>>>>>>>>>>
        call array_save(ctx_paral,ctx_domain,ctx_discretization,          &
                        ctx_io%save_unstructured,ctx_io%save_structured,  &
                        ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                        model,nmodel,'cell',ctx_io%workpath_model,outname,&
                        outname_gb,ctx_err)
        call distribute_error(ctx_err,ctx_paral%communicator) 
        deallocate(model)


      ! ----------------------------------------------------------------
      case(tag_MODEL_PPOLY)

        ! --------------------------------------------------------------
        !! if we save using a structured grid, it must be done with the
        !! piecewise-constant representation, otherwise, all cartesian
        !! map must be appropriate to each of the models.
        !! -------------------------------------------------------------
        if(ctx_io%save_structured) then
          !! we extract the piecewise-constant version of the model 
          !! evaluate the model per cell.
          allocate(model_loc(ctx_domain%n_cell_loc))
          allocate(model    (ctx_domain%n_cell_loc,nmodel))
          do imodel=1,nmodel
            name_loc =trim(adjustl(name_model(imodel)))
            model_loc= 0.0
            !! eval piecewise constant per cell (can be r4 or r8)      
            call crosscorrelation_evalsrc_pconstant(ctx_model_param,    &
                                                    model_loc,ctx_err)
            call distribute_error(ctx_err,ctx_paral%communicator) 
            model(:,imodel)  =model_loc
            fieldname(imodel)=trim(adjustl(name_loc))
          end do
          deallocate(model_loc)

          outname=''
          call io_filename(outname,outname_gb,fieldname,                &
                           "source-covariance",nmodel,                  &
                           flag_freq,flag_mode,phantom_r,phantom_i,     &
                           ctx_err,o_iter=iteration)
          call distribute_error(ctx_err,ctx_paral%communicator) 

          !! we force false the unstructured part here.
          call array_save(ctx_paral,ctx_domain,ctx_discretization,          &
                          .false.,ctx_io%save_structured,                   &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          model,nmodel,'cell',ctx_io%workpath_model,outname,&
                          outname_gb,ctx_err)
          call distribute_error(ctx_err,ctx_paral%communicator)
          deallocate(model)
        end if
        ! ---------------------------------------------------------

        ! ---------------------------------------------------------
        !!
        !! Now we save the unstructured-grid using the polynomials.
        !!
        !! -------------------------------------------------------------
        if(ctx_io%save_unstructured) then
          do imodel=1,nmodel
            name_loc =trim(adjustl(name_model(imodel)))
            !! eval piecewise linear per cell: 
            !! WE SAVE REAL 4 HERE, BECAUSE OF MODEL_LOC, WE CAN JUST 
            !! CHANGE MODEL_LOC TO REAL 8 TO SAVE IN DOUBLE.
            !! ***********************************************************       
            call crosscorrelation_evalsrc_ppoly(ctx_domain,             &
                                  ctx_model_param,model_loc,cste_order, &
                                  ndof_gb,ctx_err)
            call distribute_error(ctx_err,ctx_paral%communicator) 
            if(imodel == 1) then
              allocate(model(ndof_gb,nmodel))
            end if
  
            model(:,imodel)  =model_loc
            fieldname(imodel)=trim(adjustl(name_loc))
            deallocate(model_loc)
          end do
          
          !! filenames >>>>>>>>>>>>>
          outname=''
          call io_filename(outname,outname_gb,fieldname,                &
                           "source-covariance",nmodel,                  &
                           flag_freq,flag_mode,phantom_r,phantom_i,     &
                           ctx_err,o_iter=iteration)
          call distribute_error(ctx_err,ctx_paral%communicator) 

          !! We force to false the structured part now >>>>>>>>>>>>>>>>>
          call array_save(ctx_paral,ctx_domain,ctx_discretization,      &
                          ctx_io%save_unstructured,.false.,.false.,     &
                          .false.,model,nmodel,'dof',                   &
                          ctx_io%workpath_model,outname,outname_gb,     &
                          ctx_err,o_cste_order=cste_order)
          call distribute_error(ctx_err,ctx_paral%communicator) 
          deallocate(model)
       end if

      ! ----------------------------------------------------------------
      ! Saving using the dof format now.
      ! ----------------------------------------------------------------
      case(tag_MODEL_DOF)
        !! -------------------------------------------------------------
        !! for model per dof.
        !! -------------------------------------------------------------
        
        if(ctx_io%save_structured .or. ctx_io%save_unstructured) then
          do imodel=1,nmodel
            name_loc =trim(adjustl(name_model(imodel)))
            !! eval model: 
            !! WE SAVE REAL 4 HERE, BECAUSE OF MODEL_LOC, WE CAN JUST 
            !! CHANGE MODEL_LOC TO REAL 8 TO SAVE IN DOUBLE.
            !! ***********************************************************   
            call crosscorrelation_evalsrc_pdof(ctx_domain,ctx_model_param, &
                                               model_loc,cste_order,       &
                                               ndof_gb,ctx_err)
            call distribute_error(ctx_err,ctx_paral%communicator) 
            if(imodel == 1) then
              allocate(model(ndof_gb,nmodel))
            end if
            model(:,imodel)  =model_loc
            fieldname(imodel)=trim(adjustl(name_loc))
            deallocate(model_loc)
          end do
        
          !! filenames >>>>>>>>>>>>>
          outname=''
          call io_filename(outname,outname_gb,fieldname,                &
                           "source-covariance",nmodel,                  &
                           flag_freq,flag_mode,phantom_r,phantom_i,     &
                           ctx_err,o_iter=iteration)
          call distribute_error(ctx_err,ctx_paral%communicator) 

          !! we force false the unstructured part here.
          !! we need the order
          call array_save(ctx_paral,ctx_domain,ctx_discretization,          &
                          ctx_io%save_unstructured,ctx_io%save_structured,  &
                          ctx_io%format_struct_vtk,ctx_io%format_struct_sep,&
                          model,nmodel,'dof-orderfix',ctx_io%workpath_model,&
                          outname,outname_gb,ctx_err,o_cste_order=cste_order)
          call distribute_error(ctx_err,ctx_paral%communicator)
          deallocate(model)
        end if
        ! ---------------------------------------------------------
      
      case default 
        ctx_err%msg   = "** ERROR: we cannot save the "//               &
                        trim(adjustl(format_disc))//                    &
                        " model representation [model_crosscorrelation_save] **"
        ctx_err%ierr  = -1
        ctx_err%critic=.true.
        call raise_error(ctx_err)
      
    end select
    !! -----------------------------------------------------------------

    deallocate(fieldname)
    deallocate(outname)
    
    return
  end subroutine crosscorrelation_save_covariance_src
end module m_crosscorrelation_save
