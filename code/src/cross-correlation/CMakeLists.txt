add_subdirectory(api)
add_subdirectory(context)
add_subdirectory(utils)

target_link_libraries( parallelism discretization_operator inversion_gradient raise_error)
install(TARGETS model_update 
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)
add_library(m_read_parameter_crosscov.f90)
