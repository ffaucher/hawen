!------------------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!------------------------------------------------------------------------------
!
! MODULE m_read_parameters_crosscov.f90
!
!> @author
!> F. Faucher [Inria Makutu]
!
! DESCRIPTION:
!> @brief
!> the module is used to read the input parameters for the cross covariance
!> informations. In particular, we need to read the list of data points and
!> of sources involved by each data in separated file.
!
!------------------------------------------------------------------------------
module m_read_parameters_crosscov
  !! module used -------------------------------------------------------
  use m_raise_error,     only: raise_error, t_error
  use m_read_parameter,  only: read_parameter
  !! -------------------------------------------------------------------
  
  implicit none
  private 
  public     :: read_parameters_crosscov
    
  contains  

  !----------------------------------------------------------------------------
  ! DESCRIPTION:
  !> @brief
  !> read parameters to work with the cross covariance.
  !
  !> @param[in]   parameter_file    : parameter file where infos are
  !> @param[out]  str_file_data_src : file to list the src associated to data
  !> @param[out]  str_file_data_pts : file to list the points associated to data
  !> @param[out]  ctx_err           : error context
  !----------------------------------------------------------------------------
  subroutine read_parameters_crosscov(parameter_file,                   &
                                      str_file_data_src,                &
                                      str_file_data_pts,ctx_err)

    implicit none 
    
    character(len=*)             ,intent(in)   :: parameter_file
    character(len=512)           ,intent(out)  :: str_file_data_src
    character(len=512)           ,intent(out)  :: str_file_data_pts
    type(t_error)                ,intent(out)  :: ctx_err
    
    integer              :: nval
    
    !! -----------------------------------------------------------------
    ctx_err%ierr  = 0
    
    !! read the file where to find the sources involved in each data
    str_file_data_src=''
    call read_parameter(parameter_file,'fwi_cross-covariance_data-src', &
                        '',str_file_data_src,nval)
    if(str_file_data_src=='') then
      ctx_err%msg   = "** ERROR: the file to least to convariance " //  &
                      "points is missing [read_crosscov]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if   

    !! read the file where to find the 2-points involved in each data
    str_file_data_pts=''
    call read_parameter(parameter_file,'fwi_cross-covariance_data-pts', &
                        '',str_file_data_pts,nval)
    
    if(str_file_data_pts=='') then
      ctx_err%msg   = "** ERROR: the file to least to convariance " //  &
                      "points is missing [read_crosscov]**"
      ctx_err%ierr  = -1
      ctx_err%critic=.true.
      call raise_error(ctx_err)
    end if
    
    return
  end subroutine read_parameters_crosscov

end module m_read_parameters_crosscov
