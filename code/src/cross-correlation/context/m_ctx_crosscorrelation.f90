!-----------------------------------------------------------------------
! PROJECT: Harmonic Waveform Modeling and Inversion
!-----------------------------------------------------------------------
!
! MODULE m_ctx_crosscorrelation.f90
!
!> @author 
!> F. Faucher   [Inria Makutu]
!
! DESCRIPTION: 
!> @brief
!> the module defines the context for cross-correlation data. In this
!> case, one way need the cross-covariance of the source, which is 
!> treated as any model of properties.
!
!-----------------------------------------------------------------------
module m_ctx_crosscorrelation

  use  m_ctx_model_representation,   only : t_model_param,              &
                                            model_representation_clean
  
  implicit none

  private
  public   ::  t_crosscorrelation, ccor_ctx_clean

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> Type t_ccovar_data contains the information for each of the datum
  !
  !> @param[integer] n_src_gb       : maximal index of the source
  !> @param[integer] n_pts_gb       : maximal index of the points
  !> @param[integer] n_data         : number of data
  !> @param[integer] n_src_loc      : number of source per datum
  !> @param[integer] src_of_data    : list sources in datum
  !> @param[integer] pts_loc        : list of the observation points
  !
  !---------------------------------------------------------------------
  type t_crosscorrelation

    !! the cross-covariance of the source is represented
    !! in the save way as a model parameter.
    character(len=32)   :: format_disc !! representation, e.g., piecewise-constant, sep, etc.
    type(t_model_param) :: src_crosscovariance_real
    type(t_model_param) :: src_crosscovariance_imag

    !! these are general information to be used
    integer             :: dim_domain  !! dimension of the problem.
    integer             :: n_cell_loc  !! local number of cells.

  end type t_crosscorrelation
  !---------------------------------------------------------------------
  
  contains  

  !---------------------------------------------------------------------
  ! DESCRIPTION: 
  !> @brief
  !> clean context
  !
  !> @param[inout] ctx_cc  : cross-correlation  context
  !---------------------------------------------------------------------
  subroutine ccor_ctx_clean(ctx_cc)
    
    implicit none
    type(t_crosscorrelation),   intent(inout)  :: ctx_cc
    
    ctx_cc%n_cell_loc  = 0
    ctx_cc%dim_domain  = 0
    ctx_cc%format_disc = ''
    call model_representation_clean(ctx_cc%src_crosscovariance_real)
    call model_representation_clean(ctx_cc%src_crosscovariance_imag)
    
    return
  
  end subroutine ccor_ctx_clean
  !---------------------------------------------------------------------

end module m_ctx_crosscorrelation
!-----------------------------------------------------------------------
