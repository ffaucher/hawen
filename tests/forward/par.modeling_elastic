########################################################################
## PARAMETER FILE: 2D ELASTIC HDG DISCRETIZATION, MODELING.
########################################################################
## 1) Problem information ##############################################
dimension=2
frequency_fourier=0.2,0.4 # 0.2Hz and 0.4Hz
frequency_laplace=0

## 2) Acquisition ######################################################
file_acquisition_source=./input/source.txt
file_acquisition_receiver=./input//receiver.txt
src_depth=0.           # force here or read in file
rcv_depth=0.           # force here or read in file
source_type=dirac      # dirac point-sources.
source_component=ux,uz # e.g., {p,vx,vy,vz,sxx,...}
source_first=1         # starting from the first source
# source_last=1        # commented: we solve for all sources
source_step=1          # all sources are solved
acquisition_verbose=true

## source characterization from a Ricker wavelet
## or using a constant
source_time0=0.
source_timepeak=0.
source_freqpeak=10.
source_ampli=1.0
source_constant=true    # we use a constant source at 1.0
source_constant_real=1.
source_constant_imag=0.
########################################################################

## 3) model parameters #################################################
# model_sep_vp=     # model_ascii_vp=  # model_constant_vp  # model_scale_vp
model_constant_vp=0.10
model_constant_vs=0.07
model_constant_rho=1.0
viscosity=false # there is no viscosity here.
########################################################################

## 4) mesh information #################################################
mesh_file=./input/mesh/mesh.1
mesh_format=triangle
mesh_partition=metis
# boundary tag infos: flag if any and list of tags
mesh_tag_absorbing=1       # the mesh tag where ABC          is applied
mesh_tag_free-surface=     # the mesh tag where Free-Surface is applied
mesh_tag_dirichlet=        # the mesh tag where Dirichlet    is applied
mesh_tag_neumann=          # the mesh tag where Neumann      is applied
mesh_tag_wall-surface=     # the mesh tag where Wall-Surface is applied

# HDG formulation
evaluate_int_quadrature=true # using quadrature formula for integrals
polynomial_order=4            # force order 4 everywhere
polynomial_order_min=3        # unused
polynomial_order_max=6        # unused

########################################################################

## 5) solver ###########################################################

## number of parallel rhs 
parallel_rhs=128    # maximal number of rhs to solve in parallel

## information for Mumps solver
mumps_analysis=1          #0=AUTO, 1=SEQUENTIAL, 2=PARALLEL
mumps_partitioner_seq=4   #0=AMD, 1=PIVOT, 2=AMF, 3=SCOTCH, 4=PORD, 
                          #5=METIS, 6=MINDEG, 7=AUTO
mumps_partitioner_par=1   #0=AUTO, 1=PT_SCOTCH,  2=PARMETIS
## low-rank options
mumps_lowrank=0           # low-rank is not used
mumps_lowrank_tol=1e-9
mumps_lowrank_variant=0
mumps_advanced_experimental=0
mumps_memory_increase_percent=40
mumps_memory_limit=
mumps_verbose=0
########################################################################

## 6) io ###############################################################
workpath=./results_elastic                  # folder to save results
save_wavefield=true                         # to save wavefields
save_receivers=false                        # to save solution at rcv.
save_structured_vtk=false
save_structured_sep=false
save_unstructured_vtk=true
save_wavefield_component=ux,uz,sxx,szz,sxz  # save velocities and stress
save_receivers_component=ux,uz,sxx,szz,sxz  # save velocities and stress
dx=1e-2 # for structured save, we need the discretization
dy=1e-2 # for structured save, we need the discretization
dz=1e-2 # for structured save, we need the discretization
########################################################################
## END OF PARAMETER FILE 
########################################################################
