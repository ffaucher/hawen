## 1) Problem information ##############################################
dimension=2
frequency_fourier=2,4,10
frequency_laplace=0

## 2) Acquisition ######################################################
file_acquisition_source=acquisition/sources.txt
file_acquisition_receiver=acquisition/receivers.txt
# src_depth= # force here or read in file
# rcv_depth= # force here or read in file
source_type=dirac
source_component=p # {p,vx,vy,vz,sxx,...}
source_first=1
# source_last=
source_step=1
acquisition_verbose=true
## source characterization 
source_time0=0.
source_timepeak=0.
source_freqpeak=10
source_ampli=1e0
# source_constant=true # possibility for constant coeff.
# source_constant_real=
# source_constant_imag=
########################################################################

model_constant_vp=1.0
model_constant_rho=1.0
viscosity=false
viscosity_model=
model_representation=dof
model_representation_polynomial_order_vp=1
model_representation_polynomial_order_rho=1

########################################################################

## 4) grid information #################################################
##    using mesh or cartesian grid 
##    either we read from an input sep or we can specify directly
mesh_file=models/mesh/mesh_5k/mesh.1
mesh_format=triangle
mesh_partition=metis
# boundary tag infos: flag if any and list of tags
mesh_tag_absorbing=1,2,3,4
mesh_tag_free-surface=
mesh_tag_wall-surface=
# polynomial infos, file if order < 0
evaluate_int_quadrature=true
polynomial_order=-2
polynomial_order_min=2
polynomial_order_max=7
polynomial_order_file=
########################################################################

## 5) solver ###########################################################
mumps_analysis=1          #0=AUTO, 1=SEQUENTIAL, 2=PARALLEL
mumps_partitioner_seq=4   #0=AMD, 1=PIVOT, 2=AMF, 3=SCOTCH, 4=PORD, 5=METIS, 6=MINDEG, 7=AUTO
mumps_partitioner_par=1   #0=AUTO, 1=PT_SCOTCH,  2=PARMETIS
mumps_lowrank=0
mumps_lowrank_tol=1e-7
mumps_lowrank_variant=0
mumps_cb_compress=0
mumps_advanced_experimental=0
mumps_memory_increase_percent=50
mumps_multithread_tree=0
mumps_block_structure=0
mumps_verbose=0
mumps_root_parallel=
mumps_crit_pivoting=
mumps_relaxed_pivoting=
mumps_mpi_to_komp=1

parallel_rhs=64
########################################################################

## 6) io ###############################################################
workpath=./results/fwi_01
save_structured_vtk=false
save_structured_sep=true     # we save in sep so that the final model
                             # can be loaded for the set 2 of fwi.
save_unstructured_vtk=false
dx=0.01 # for structured save, we need the discretization
dy=0.01 # for structured save, we need the discretization
dz=0.01 # for structured save, we need the discretization
########################################################################

## 7) inversion ########################################################
## for receivers normal in reciprocity
acquisition_rcv_normal=z # x,y,z,text-file
# file_acquisition_receiver_normal=
fwi_misfit=l2
fwi_frequency_group=1
fwi_path_data=data-sets/modeling_no-noise/data-record_
fwi_component=p # p, vx, vz, normal_v, tangent_v
# fwi_reciprocity_nobs=
fwi_niter=20
fwi_niter_min=15
fwi_stagnation_tol=1E-4
fwi_source_inversion=true
fwi_source_inversion_shared=true
fwi_parameter=bulk-modulus,rho
# list of functions
fwi_parameter_function=inverse,0
fwi_debug_save_gradient=false
fwi_output_clean=2
# search direction
fwi_search_direction=nlcg_FR #nlcg_FR #l-bfgs
fwi_pseudo_hessian_scale=false
fwi_gradient_metric=mass_matrix_inverse
fwi_gradient_model_param=dof #piecewise-constant
fwi_gradient_model_param_doforder=1
# linesearch
fwi_linesearch_method=backtracking
fwi_linesearch_niter=5
fwi_linesearch_step1=1e-2
fwi_linesearch_reset_iter=true
fwi_linesearch_control= # armijo
fwi_linesearch_control_tol=1E-10

# min/max
model_min_vp=0.90
model_max_vp=1.30

# receivers offsets
fwi_offset_rcv=true
fwi_offset_rcv_xmin=
fwi_offset_rcv_xmax=
fwi_offset_rcv_ymin=
fwi_offset_rcv_ymax=
fwi_offset_rcv_zmin=
fwi_offset_rcv_zmax=
fwi_offset_rcv_zmax=
fwi_offset_rcv_radius=0.02

# box restriction
fwi_restrict_box=true
fwi_xmin=0.05
fwi_xmax=0.95
fwi_ymin=
fwi_ymax=
fwi_zmin=0.05
fwi_zmax=0.95
fwi_rmin=
fwi_rmax=
fwi_r0x=
fwi_r0y=
fwi_r0z=


