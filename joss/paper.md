---
title: "`hawen`: time-harmonic wave modeling and inversion using hybridizable discontinuous Galerkin discretization"
tags:
  - Fortran
  - wave equations
  - inverse problems
  - geophysics
  - helioseismology
  - Hybridizable Discontinuous Galerkin method
  - viscoelasticity
  - MPI and OpenMP parallelism 
authors:
  - name: Florian Faucher
    orcid: 0000-0003-4958-7511
    affiliation: "1"
affiliations:
 - name: Faculty of Mathematics, University of Vienna, Oskar-Morgenstern-Platz 1, A-1090 Vienna, Austria.
   index: 1
date: September 2020
bibliography: paper.bib

---

# Summary

Many applications such as seismic and medical imaging, material sciences, 
or helioseismology and planetary science, aim to reconstruct properties 
of a non directly accessible or non-visible interior.
For this purpose, they rely on waves whose propagation through a medium 
interrelates with the physical properties (density, sound speed, etc.) of 
this medium.
The methodology for imaging with waves comprises of two main stages 
illustrated in \autoref{fig:setup}.
In the data acquisition stage (\autoref{fig:setup}a), the 
medium response to probing waves is recorded (e.g., seismic waves 
from Earthquakes recorded by ground network).
In the second stage, we rely on a reconstruction procedure which 
iteratively updates an initial model of physical parameters, 
so that numerical simulations approach the measurements (\autoref{fig:setup}b).
This procedure is employed, for instance, for seismic (reconstruction of 
subsurface layers) and medical (disease diagnostic) imaging,
see the introduction of @Faucher2020adjoint and the references therein.


![Illustration of inverse wave problems. 
\textbf{a)} Acquisition stage: probing waves are sent though the medium, 
and the medium's response is recorded by devices positioned on a portion 
of the domain. 
\textbf{b)} The reconstruction algorithm starts from initial properties 
and compares simulations of wave propagation with the measurements, then
iteratively updates those properties. The green block corresponds to
the forward problem (modeling the propagation of waves) and the orange 
ones to the iterative correction. `hawen` solves both the forward and 
inverse problems associated with time-harmonic waves.
  \label{fig:setup}](figures/setup_global.pdf){width=13cm} 


# Statement of need 

`hawen` is designed to address large-scale inverse wave problems, and includes
\begin{enumerate}
  \item simulating time-harmonic waves in heterogeneous 
        media (e.g., visco-acoustic and visco-elastic 
        propagation, modal equations in helioseismology); 
  \item performing iterative minimization to solve the inverse 
        problem, cf. \autoref{fig:setup}.
\end{enumerate}
It combines MPI and OpenMP parallelism, it is deployed on
supercomputers and has been used in studies on seismic imaging 
[@Faucher2020FRgWIacoustic; @Faucher2020adjoint; @Faucher2020DAS], 
as well as in helioseismology [@Pham2020Siam; @Pham2019Esaim; @Pham2019RR; @Pham2020RRscalar].
The software works with an input \emph{parameter file}: 
a text file listing the problem configuration (dimension, choice of viscous 
model, etc.), such that the computational details are transparent for the users.
`hawen` has its dedicated 
\href{https://ffaucher.gitlab.io/hawen-website/download/hawen_users-guide_documentation.pdf}{User's guide} [@HawenUserGuide] that details
its utilization and all the available functionalities, it is available 
on the software dedicated \href{https://ffaucher.gitlab.io/hawen-website/}{website}, 
which contains illustrations, installation guide and tutorials.


One specificity of `hawen` is to implement the hybridizable 
discontinuous Galerkin (HDG) method [@Arnold2002; @Cockburn2009] 
for the discretization of the wave equations.
It helps reduce the cost of the computations by providing smaller 
linear systems compared to, e.g., finite elements, depending on the 
order of approximation [@Kirby2012; @Faucher2020adjoint].
For seismic applications, current software mostly relies on the
spectral element method [@Komatitsch1998; @Komatitsch1999], such
as \href{https://geodynamics.org/cig/software/specfem3d/}{specfem},
or on finite differences discretization.

The software handles unstructured domains (allowing input meshes 
of different formats) and thus accounts for complex geometry. 
In addition, `hawen` relies on the massively parallel sparse direct 
solver MUMPS [@Amestoy2001; @Amestoy2006] for the matrix factorization,
hence handles problems with many right-hand sides.
Despite the technicality of the underlying methods, the purpose of `hawen` 
is to provide a unified and evolutive framework to address large-scale 
visco-elastic and visco-acoustic inverse problems, with a user-friendly 
interface using the input parameter files.


# Modeling the propagation of waves

The first feature of `hawen` is to simulate the propagation of 
time-harmonic waves in different types of medium: this is the
forward problem in \autoref{fig:setup}. 
The propagation of waves is characterized by a Partial Differential 
Equation which depends on the type of the waves (e.g., mechanical or 
electromagnetic), and on the type of medium considered (e.g., fluid or solid)
[@Carcione2007; @Slawinski2010; @Faucher2017].
`hawen` handles visco-acoustic and visco-elastic propagation,
as well as waves propagation for helioseismology, these are 
described in the \href{https://ffaucher.gitlab.io/hawen-website/download/hawen_users-guide_documentation.pdf}{User's documentation} [@HawenUserGuide].
Our implementation is based upon the HDG method and the computational 
steps are illustrated in \autoref{fig:forward}.

![Illustration of the computational steps for the 
  numerical resolution of the forward problem. 
  For simplicity, we illustrate with an homogeneous 
  medium.
\label{fig:forward}](figures/forward.pdf){width=13cm} 


# Inverse problem via iterative minimization

In the forward problem, the objective is to 
simulate the propagation of waves, given the 
medium physical properties.
Conversely, the objective of the inverse 
problem is to recover the physical properties, 
given some observations of waves, see \autoref{fig:setup}.
To address the inverse problem, `hawen` solves the 
minimization problem:
\begin{equation}\label{eq:misfit_generic}
  \min_{\boldsymbol{m}} \mathcal{J}(\boldsymbol{m}) \, \qquad \text{with} \quad
  \mathcal{J}(\boldsymbol{m}) \,=\, \mathrm{dist}\big(\mathcal{F}(\boldsymbol{m}), \, \boldsymbol{d}\big) \, .
\end{equation}
The misfit function $\mathcal{J}$ is defined to evaluate 
a distance function comparing the observed data $\boldsymbol{d}$ 
with simulations of wave propagation restricted to the position 
of the measurements: $\mathcal{F}(\boldsymbol{m})$, where $\boldsymbol{m}$ represents the 
physical properties. 
The iterative minimization scheme successively updates the 
physical properties used for the simulations (\autoref{fig:setup}b),
and follows a Newton-type method [@Nocedal2006]. In the context of 
seismic imaging, it is the Full Waveform Inversion method, cf. @Virieux2009.

`hawen` offers several options to conduct the 
iterative minimization, such as the choice of misfit function
and method to conduct the minimization.
These are further listed and detailed in 
the \href{https://ffaucher.gitlab.io/hawen-website/download/hawen_users-guide_documentation.pdf}
{software documentation} [@HawenUserGuide].

# Acknowledgements

The research of FF is supported by the Austrian Science 
Fund (FWF) under the Lise Meitner fellowship M 2791-N.
We acknowledge the use of the 
\href{https://vsc.ac.at/home/}{Vienna Scientific Clusters vsc3 and vsc4}
for the numerical applications, as well as with the TGCC cluster via 
the GENCI resource allocation project AP010411013.

# References
