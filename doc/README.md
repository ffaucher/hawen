## this folder contains the documentation for hawen:

### user's guide pdf

### configuration file to generate doxygen documentation:
### it requires doxygen version at least 1.8.18
doxygen doxygen-config-file.txt

creates the doxygen folder which contains the html documentation and 
the latex to generate a pdf file as well
