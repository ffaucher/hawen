#Taken from Bakosi

# Find the Math Kernel Library from Intel
#
#  MKL_FOUND - System has MKL
#  MKL_INCLUDE_DIRS - MKL include files directories
#  MKL_LIBRARIES - The MKL libraries
#  MKL_INTERFACE_LIBRARY - MKL interface library
#  MKL_SEQUENTIAL_LAYER_LIBRARY - MKL sequential layer library
#  MKL_THREADED_LAYER_LIBRARY - MKL threaded layer library
#  MKL_CORE_LIBRARY - MKL core library
#  MKL_SCALAPACK_LIBRARY
#
#  The environment variable MKLROOT is used to find libraries
#
#  Example usage:
#
#  find_package(MKL)
#  if(MKL_FOUND)
#    target_link_libraries(TARGET ${MKL_LIBRARIES})
#  endif()

# If already in cache, be silent
if (MKL_INCLUDE_DIRS AND MKL_LIBRARIES AND MKL_INTERFACE_LIBRARY AND
    MKL_SEQUENTIAL_LAYER_LIBRARY AND MKL_CORE_LIBRARY)
  set (MKL_FIND_QUIETLY TRUE)
endif()

if (${MKL_FIND_COMPONENTS} STREQUAL "INT64")
  message(STATUS "MKL with 64-bit integers")
  set(int_size i)
else()
  message(STATUS "MKL with 32-bit integers")
  set(int_size "")
endif()
if (CMAKE_Fortran_COMPILER_ID STREQUAL "INTEL")
  set(compilo intel)
  set(libmpi intelmpi)
  set(libinter intel)
elseif (CMAKE_Fortran_COMPILER_ID STREQUAL "GNU")
  set(compilo gnu)
  set(libmpi openmpi)
  set(libinter gf)
endif()



set(INT_LIB "libmkl_${libinter}_${int_size}lp64.a")
set(SEQ_LIB "libmkl_sequential.a")
set(THR_LIB "libmkl_${compilo}_thread.a")
set(COR_LIB "libmkl_core.a")
set(SCALAPACK_LIB "libmkl_scalapack_${int_size}lp64.a")
set(BLACS_MPI_LIB "libmkl_blacs_${libmpi}_${int_size}lp64.a")
set(BLACS_IMPT_LIB "libmkl_blacs_sgimpt_lp64.a")
find_path(MKL_INCLUDE_DIR NAMES mkl.h HINTS $ENV{MKLROOT}/include)

find_library(MKL_INTERFACE_LIBRARY
             NAMES ${INT_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
             NO_DEFAULT_PATH)

find_library(MKL_SEQUENTIAL_LAYER_LIBRARY
             NAMES ${SEQ_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
             NO_DEFAULT_PATH)

find_library(MKL_GF_LIBRARY
             NAMES ${GF_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
             NO_DEFAULT_PATH)

find_library(MKL_CORE_LIBRARY
             NAMES ${COR_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
             NO_DEFAULT_PATH)

find_library(MKL_THREADED_LIBRARY
             NAMES ${THR_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
             NO_DEFAULT_PATH)

find_library(MKL_SCALAPACK_LIBRARY
             NAMES ${SCALAPACK_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
             NO_DEFAULT_PATH)

if (MPI_FOUND)
  find_library(MKL_BLACS_MPI_LIB
               NAMES ${BLACS_MPI_LIB}
               PATHS $ENV{MKLROOT}/lib
                     $ENV{MKLROOT}/lib/intel64
               NO_DEFAULT_PATH)
  find_library(MKL_BLACS_IMPT_LIB
               NAMES ${BLACS_IMPT_LIB}
               PATHS $ENV{MKLROOT}/lib
                     $ENV{MKLROOT}/lib/intel64
               NO_DEFAULT_PATH)
  if (MKL_BLACS_MPI_LIB)
    set(MKL_SCALAPACK_LIBRARY ${MKL_SCALAPACK_LIBRARY} ${MKL_BLACS_MPI_LIB})
  endif()
  if (MKL_BLACS_IMPT_LIB)
    set(MKL_SCALAPACK_LIBRARY ${MKL_SCALAPACK_LIBRARY} ${MKL_BLACS_IMPT_LIB})
  endif()
endif(MPI_FOUND)

set(MKL_INCLUDE_DIRS ${MKL_INCLUDE_DIR})
include(CheckFortranFunctionExists)

if (OpenMP_Fortran_FOUND)
  if (CMAKE_Fortran_COMPILER_ID STREQUAL "INTEL")
    set(MKL_LIBRARIES ${MKL_SCALAPACK_LIBRARY} ${MKL_INTERFACE_LIBRARY} ${MKL_THREADED_LIBRARY} ${MKL_CORE_LIBRARY} ${MKL_BLACS_MPI_LIB} gomp iomp5 pthread m dl gfortran)
    set(CMAKE_REQUIRED_LIBRARIES ${MKL_SCALAPACK_LIBRARY} -Wl,--start-group  ${MKL_INTERFACE_LIBRARY} ${MKL_THREADED_LIBRARY} ${MKL_CORE_LIBRARY} ${MKL_BLACS_MPI_LIB} -Wl,--end-group -liomp5 -lgomp -lpthread -lm -ldl -lgfortran )
   elseif (CMAKE_Fortran_COMPILER_ID STREQUAL "GNU")
     set(MKL_LIBRARIES ${MKL_SCALAPACK_LIBRARY} ${MKL_INTERFACE_LIBRARY} ${MKL_THREADED_LIBRARY} ${MKL_CORE_LIBRARY} gomp pthread m dl gfortran)
	 set(CMAKE_REQUIRED_LIBRARIES "${MKL_SCALAPACK_LIBRARY} -Wl,--start-group ${MKL_INTERFACE_LIBRARY} ${MKL_THREADED_LIBRARY} ${MKL_CORE_LIBRARY} ${MKL_BLACS_MPI_LIB} -Wl,--end-group -lgomp -lpthread -lm -ldl") 
  endif()
else()
  set(MKL_LIBRARIES ${MKL_SCALAPACK_LIBRARY} ${MKL_INTERFACE_LIBRARY} ${MKL_SEQUENTIAL_LAYER_LIBRARY} ${MKL_CORE_LIBRARY})
  set(CMAKE_REQUIRED_LIBRARIES -Wl,--start-group ${MKL_SCALAPACK_LIBRARY} ${MKL_INTERFACE_LIBRARY} ${MKL_SEQUENTIAL_LAYER_LIBRARY} ${MKL_CORE_LIBRARY} -Wl,--end-group)
endif()
check_fortran_function_exists(dpotrf HAS_DPOTRF)
 # check for dpotrf
# Handle the QUIETLY and REQUIRED arguments and set MKL_FOUND to TRUE if
# all listed variables are TRUE.
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MKL DEFAULT_MSG MKL_LIBRARIES MKL_INCLUDE_DIRS MKL_SCALAPACK_LIBRARY MKL_INTERFACE_LIBRARY MKL_SEQUENTIAL_LAYER_LIBRARY MKL_CORE_LIBRARY)
MARK_AS_ADVANCED(MKL_INCLUDE_DIRS MKL_LIBRARIES MKL_SCALAPACK_LIBRARY MKL_INTERFACE_LIBRARY MKL_SEQUENTIAL_LAYER_LIBRARY MKL_CORE_LIBRARY)
